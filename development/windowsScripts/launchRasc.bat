@echo off
cd %~dp0
echo.
echo This batch script redirects Rasc's logging to a file (logFile), instead
echo of printing it in real-time to the terminal. This is recommended as it
echo is very helpful for diagnostic purposes.
echo.
echo If you would rather see a real-time log, and have this log thrown away
echo by windows when the program closes (NOT recommended), then run rasc.exe
echo directly.
echo. >> logFile
echo. >> logFile
echo ========================= Game launch ======================= >> logFile
rasc.exe >> logFile 2>&1

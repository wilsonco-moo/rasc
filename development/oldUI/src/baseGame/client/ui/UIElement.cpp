/*
 * UIElement.cpp
 *
 *  Created on: 1 Sep 2018
 *      Author: wilson
 */

#include "UIElement.h"

namespace rasc {

    UIElement::UIElement(RascClientNetIntf * netIntf) :
        netIntf(netIntf) {
    }

    UIElement::~UIElement(void) {
    }

    bool UIElement::onClick(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY) {
        return true;
    }

    void UIElement::onMouseMove(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY) {}

}

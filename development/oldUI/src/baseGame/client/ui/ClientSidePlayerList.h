/*
 * ClientSidePlayerList.h
 *
 *  Created on: 24 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_CLIENTSIDEPLAYERLIST_H_
#define BASEGAME_CLIENT_CLIENTSIDEPLAYERLIST_H_

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

#include "../../../sockets/plus/message/Message.h"
#include "../../../wool/misc/RGBA.h"
#include "UIElement.h"

namespace rasc {

    class CameraProperties;

    class ClientSidePlayerList : public UIElement {

    private:

        int playerCount;
        std::vector<std::string> playerNames;
        std::vector<wool::RGBA> playerColours;
        std::vector<bool> playerTurnStatus;
        std::unordered_map<uint64_t, int> playerIds;

    public:
        ClientSidePlayerList(RascClientNetIntf * netIntf);
        virtual ~ClientSidePlayerList(void);

        void onPlayerListUpdate(simplenetwork::Message & msg);

        void onPlayerTurnStatusChange(uint64_t playerId, bool turnStatus);
        void resetAllTurnStatus(void);

        virtual void draw(CameraProperties & cameraProperties, GLfloat elapsed) override;


    };
}

#endif

/*
 * UIElement.h
 *
 *  Created on: 1 Sep 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_UIELEMENT_H_
#define BASEGAME_CLIENT_UI_UIELEMENT_H_

#include <GL/gl.h>
#include <mutex>


namespace rasc {

    class CameraProperties;
    class RascClientNetIntf;

    class UIElement {
    protected:
        std::mutex dataMutex;
        RascClientNetIntf * netIntf;

    public:
        UIElement(RascClientNetIntf * netIntf);
        virtual ~UIElement(void);

        virtual void draw(CameraProperties & cameraProperties, GLfloat elapsed) = 0;

        /**
         * This is run when the user clicks.
         * This method should return TRUE if nothing in this part of the UI has been clicked on.
         */
        virtual bool onClick(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY);

        /**
         * This should be run when the mouse moves.
         */
        virtual void onMouseMove(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY);
    };

}

#endif

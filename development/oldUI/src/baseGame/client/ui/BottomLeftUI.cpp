/*
 * BottomLeftUi.cpp
 *
 *  Created on: 31 Aug 2018
 *      Author: wilson
 */

#include "BottomLeftUI.h"

#include <GL/gl.h>
#include <iostream>

#include "../../../wool/misc/RGBA.h"
#include "../../common/gameMap/CameraProperties.h"
#include "../../common/Misc.h"
#include "../../common/gameMap/MapModeController.h"
#include "../RascClientNetIntf.h"
#include "../../common/gameMap/CameraProperties.h"
#include "../../common/gameMap/GameMap.h"

namespace rasc {

    BottomLeftUI::BottomLeftUI(RascClientNetIntf * netIntf) : UIElement(netIntf),
        turnString("Turn 0000"),
        nextTurnButtonHighlighted(false),
        mapModeButtonHighlighted(-1),
        finishedTurn(false) {
    }

    BottomLeftUI::~BottomLeftUI(void) {
    }




    #define MAP_MODE_BUTTONS_HEIGHT 28.0f
    #define MAP_MODE_BUTTON_WIDTH 64.0f
    #define MAP_MODE_BUTTON_Y 2.0f
    #define MAP_MODE_BUTTON_INNER_HEIGHT 24.0f



    #define NEXT_TURN_WIDTH 96.0f
    #define NEXT_TURN_HEIGHT 128.0f
    #define NEXT_TURN_INNER_WIDTH 92.0f
    #define NEXT_TURN_INNER_HEIGHT 126.0f
    #define NEXT_TURN_INNER_XOFF 2.0f
    #define NEXT_TURN_INNER_YOFF 2.0f

    #define TURN_COUNT_HEIGHT 28.0f
    #define TURN_COUNT_INNER_HEIGHT 24.0f


    void BottomLeftUI::draw(CameraProperties & cameraProperties, GLfloat elapsed) {

        MISC_SCALE_BOTTOMLEFT(0, MINIMAP_HEIGHT + MAP_MODE_BUTTONS_HEIGHT)

        // Draw the next turn button outline.
        glColor4f(UI_OUTLINE_COLOUR);
        MISC_SCALE_RECT(MINIMAP_WIDTH, 0, NEXT_TURN_WIDTH, NEXT_TURN_HEIGHT)

        bool uiFinishedTurn = false, uiHighlighted = false;
        {
            std::lock_guard<std::mutex> lock(dataMutex);
            if (finishedTurn) {
                uiFinishedTurn = true;
            } else if (nextTurnButtonHighlighted) {
                uiHighlighted = true;
            }
        }

        // Draw the next turn button inner section.
        if (uiFinishedTurn) {
            glColor4f(UI_SELECTED_FILL_COLOUR);
        } else if (uiHighlighted) {
            glColor4f(UI_HIGHLIGHTED_FILL_COLOUR);
        } else {
            glColor4f(UI_FILL_COLOUR);
        }
        MISC_SCALE_RECT(MINIMAP_WIDTH + NEXT_TURN_INNER_XOFF, NEXT_TURN_INNER_YOFF, NEXT_TURN_INNER_WIDTH, NEXT_TURN_INNER_HEIGHT)

        // Draw the next turn button text
        if (uiFinishedTurn) {
            glColor4f(UI_SELECTED_TEXT_COLOUR);
        } else {
            glColor4f(UI_TEXT_COLOUR);
        }
        MISC_SCALE_TEXT("Next", MINIMAP_WIDTH + 3.0f, 28, 2.8f)
        MISC_SCALE_TEXT("turn", MINIMAP_WIDTH + 3.0f, 72, 2.8f)


        // Draw the turn count box
        glColor4f(UI_OUTLINE_COLOUR);
        MISC_SCALE_RECT(MINIMAP_WIDTH, NEXT_TURN_HEIGHT, NEXT_TURN_WIDTH, TURN_COUNT_HEIGHT)
        glColor4f(UI_FILL_COLOUR);
        MISC_SCALE_RECT(MINIMAP_WIDTH + 2.0f, NEXT_TURN_HEIGHT + 2.0f, NEXT_TURN_INNER_WIDTH, TURN_COUNT_INNER_HEIGHT)
        glColor4f(UI_TEXT_COLOUR);
        {
            std::lock_guard<std::mutex> lock(dataMutex);
            MISC_SCALE_WIDE_TEXT(turnString, MINIMAP_WIDTH + 4.0f, NEXT_TURN_HEIGHT + 8.0f, 1.0f)
        }



        // Draw the map mode buttons outline
        glColor4f(UI_OUTLINE_COLOUR);
        MISC_SCALE_RECT(0, 0, MINIMAP_WIDTH, MAP_MODE_BUTTONS_HEIGHT)

        // Draw the map mode buttons
        GLfloat x = 2.0f;
        MapMode mapMode = netIntf->uiGetGameMap()->getMapMode();
        for (int i = 0; i < 4; i++) {

            if (i == mapMode) {
                glColor4f(UI_SELECTED_FILL_COLOUR);
            } else if (i == mapModeButtonHighlighted) {
                glColor4f(UI_HIGHLIGHTED_FILL_COLOUR);
            } else {
                glColor4f(UI_FILL_COLOUR);
            }

            MISC_SCALE_RECT(x, MAP_MODE_BUTTON_Y, MAP_MODE_BUTTON_WIDTH - 2.0f, MAP_MODE_BUTTON_INNER_HEIGHT)
            if (i == mapMode) { glColor4f(UI_SELECTED_TEXT_COLOUR); } else { glColor4f(UI_TEXT_COLOUR); }
            MISC_SCALE_WIDE_TEXT(std::to_string(i + 1), x + 16.0f, UI_TEXT_Y, UI_TEXT_SIZE)
            x += MAP_MODE_BUTTON_WIDTH;
        }
    }


    bool BottomLeftUI::onClick(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY) {
        MISC_SCALE_BOTTOMLEFT(0, MINIMAP_HEIGHT + MAP_MODE_BUTTONS_HEIGHT)

        // If we clicked on the next turn button...
        if (MISC_SCALE_IN_RECT(MINIMAP_WIDTH, 0, NEXT_TURN_WIDTH, NEXT_TURN_HEIGHT, viewX, viewY)) {
            netIntf->finishTurn();
            return false;
        }

        // Check if we clicked on any of the map mode buttons...
        GLfloat x = 0.0f;
        for (int i = 0; i < 4; i++) {
            if (MISC_SCALE_IN_RECT(x, MAP_MODE_BUTTON_Y, MAP_MODE_BUTTON_WIDTH, MAP_MODE_BUTTON_INNER_HEIGHT, viewX, viewY)) {
                netIntf->uiGetGameMap()->setMapMode(i);
                return false;
            }
            x += MAP_MODE_BUTTON_WIDTH;
        }


        return true;
    }

    void BottomLeftUI::onMouseMove(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY) {
        // Check if we mouse-overed the next turn button
        MISC_SCALE_BOTTOMLEFT(0, MINIMAP_HEIGHT + MAP_MODE_BUTTONS_HEIGHT)
        nextTurnButtonHighlighted = MISC_SCALE_IN_RECT(MINIMAP_WIDTH, 0, NEXT_TURN_WIDTH, NEXT_TURN_HEIGHT, viewX, viewY);

        // Check if we mouse-overed any of the map-mode buttons
        GLfloat x = 0.0f;
        mapModeButtonHighlighted = -1;
        for (int i = 0; i < 4; i++) {
            if (MISC_SCALE_IN_RECT(x, MAP_MODE_BUTTON_Y, MAP_MODE_BUTTON_WIDTH, MAP_MODE_BUTTON_INNER_HEIGHT, viewX, viewY)) {
                mapModeButtonHighlighted = i;
                break;
            }
            x += MAP_MODE_BUTTON_WIDTH;
        }
    }






    #define BUFFER_LEN 20

    void BottomLeftUI::updateTurnCount(uint64_t turnCount) {
        std::lock_guard<std::mutex> lock(dataMutex);
        char turnData[BUFFER_LEN];
        #ifdef _WIN32
            std::snprintf(turnData, BUFFER_LEN, "Turn %04lu", turnCount);
        #else
            std::snprintf(turnData, BUFFER_LEN, "Turn %04" PRIu64, turnCount);
        #endif
        turnString = std::string(turnData);
        finishedTurn = false;
    }

    void BottomLeftUI::onFinishTurn(void) {
        std::lock_guard<std::mutex> lock(dataMutex);
        finishedTurn = true;
    }
}

/*
 * TopLeftUI.cpp
 *
 *  Created on: 31 Aug 2018
 *      Author: wilson
 */

#include "TopLeftUI.h"

#include <GL/gl.h>
#include <iostream>
#include <mutex>

#include "../../../rascUI/base/Component.h"
#include "../../../rascUI/util/Location.h"
#include "../../../sockets/plus/message/Message.h"
#include "../../../sockets/plus/util/MutexPtr.h"
#include "../../../wool/misc/RGBA.h"
#include "../../common/gameMap/ArmySelectController.h"
#include "../../common/gameMap/CameraProperties.h"
#include "../../common/gameMap/GameMap.h"
#include "../../common/gameMap/MapModeController.h"
#include "../../common/GameMessages.h"
#include "../../common/Misc.h"
#include "../newUI/NewUIControl.h"
#include "../RascClientNetIntf.h"

namespace rasc {

    TopLeftUI::~TopLeftUI(void) {
    }


    #define UNDERNEATH_RECTANGLE_WIDTH 362.0f
    #define UNDERNEATH_RECTANGLE_HEIGHT 28.0f

    #define MAIN_RECTANGLE_WIDTH 358.0f
    #define MAIN_RECTANGLE_HEIGHT 24.0f


    #define COLOUR_TEXT_X 8

    #define FLAG_X 104
    #define FLAG_Y 4
    #define FLAG_WIDTH 48
    #define FLAG_HEIGHT 20

    #define UNITS_X 176


    #define SPLIT_MERGE_Y 26.0f
    #define SPLIT_MERGE_HEIGHT 28.0f
    #define SPLIT_MERGE_WIDTH 96.0f
    #define SPLIT_MERGE_TEXT_X 8.0f


    void TopLeftUI::draw(CameraProperties & cameraProperties, GLfloat elapsed) {

        MISC_SCALE_TOPLEFT(0, 0)

        glColor4f(UI_OUTLINE_COLOUR);
        MISC_SCALE_RECT(0, 0, UNDERNEATH_RECTANGLE_WIDTH, UNDERNEATH_RECTANGLE_HEIGHT)

        glColor4f(UI_FILL_COLOUR);
        MISC_SCALE_RECT(2.0f, 2.0f, MAIN_RECTANGLE_WIDTH, MAIN_RECTANGLE_HEIGHT)

        glColor4f(UI_TEXT_COLOUR);
        MISC_SCALE_WIDE_TEXT("Colour:", COLOUR_TEXT_X, UI_TEXT_Y, UI_TEXT_SIZE)

        {
            std::lock_guard<std::mutex> lock(dataMutex);

            wool_setColourRGBA(colour);
            MISC_SCALE_RECT(FLAG_X, FLAG_Y, FLAG_WIDTH, FLAG_HEIGHT)

            glColor4f(UI_TEXT_COLOUR);
            MISC_SCALE_WIDE_TEXT(unitsString, UNITS_X, UI_TEXT_Y, UI_TEXT_SIZE)
        }

        // ------------ SPLIT AND MERGE BUTTONS ----------

        simplenetwork::MutexPtr<ArmySelectController> sel = netIntf->uiGetGameMap()->getArmySelectController();

        if (sel->isSplittable()) {
            glColor4f(UI_OUTLINE_COLOUR); MISC_SCALE_RECT(0, SPLIT_MERGE_Y + 2.0f, SPLIT_MERGE_WIDTH + 2.0f, SPLIT_MERGE_HEIGHT - 2.0f);
            if (highlightSplit) { glColor4f(UI_HIGHLIGHTED_FILL_COLOUR); } else { glColor4f(UI_FILL_COLOUR); }
            MISC_SCALE_RECT(2.0f, SPLIT_MERGE_Y + 2.0f, SPLIT_MERGE_WIDTH - 2.0f, SPLIT_MERGE_HEIGHT - 4.0f);
            glColor4f(UI_TEXT_COLOUR); MISC_SCALE_WIDE_TEXT("Split", SPLIT_MERGE_TEXT_X, SPLIT_MERGE_Y + UI_TEXT_Y, UI_TEXT_SIZE);
        }

        if (sel->isMergeable()) {
            glColor4f(UI_OUTLINE_COLOUR); MISC_SCALE_RECT(SPLIT_MERGE_WIDTH, SPLIT_MERGE_Y + 2.0f, SPLIT_MERGE_WIDTH + 2.0f, SPLIT_MERGE_HEIGHT - 2.0f);
            if (highlightMerge) { glColor4f(UI_HIGHLIGHTED_FILL_COLOUR); } else { glColor4f(UI_FILL_COLOUR); }
            MISC_SCALE_RECT(SPLIT_MERGE_WIDTH + 2.0f, SPLIT_MERGE_Y + 2.0f, SPLIT_MERGE_WIDTH - 2.0f, SPLIT_MERGE_HEIGHT - 4.0f);
            glColor4f(UI_TEXT_COLOUR); MISC_SCALE_WIDE_TEXT("Merge", SPLIT_MERGE_WIDTH + SPLIT_MERGE_TEXT_X, SPLIT_MERGE_Y + UI_TEXT_Y, UI_TEXT_SIZE);
        }

        newUIDraw(cameraProperties, elapsed);
    }


    bool TopLeftUI::onClick(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY) {
        GameMap * gameMap = netIntf->uiGetGameMap();

        MISC_SCALE_TOPLEFT(0, 0)
        if (MISC_SCALE_IN_RECT(0, SPLIT_MERGE_Y + 2.0f, SPLIT_MERGE_WIDTH + 2.0f, SPLIT_MERGE_HEIGHT - 2.0f, viewX, viewY)) {
            // Create a checked update message.
            simplenetwork::Message msg(ClientToServer::checkedUpdate);
            {
                simplenetwork::MutexPtr<ArmySelectController> sel = gameMap->getArmySelectController();
                // Run the ArmySelectController splitArmies method
                sel->splitArmies(msg);
            }
            // Then send the message, if anything has actually been written to it.
            // NOTE: This is done outside of the synchronisation of the gameMap. We must never lock the network interface's mutex from
            //       within ONLY the gameMap's mutex, or we have a case for deadlock.
            if (msg.getLength() > 0) netIntf->send(msg);
            return false;
        }


        if (MISC_SCALE_IN_RECT(SPLIT_MERGE_WIDTH, SPLIT_MERGE_Y + 2.0f, SPLIT_MERGE_WIDTH + 2.0f, SPLIT_MERGE_HEIGHT - 2.0f, viewX, viewY)) {
            // Create a checked update message.
            simplenetwork::Message msg(ClientToServer::checkedUpdate);
            {
                simplenetwork::MutexPtr<ArmySelectController> sel = gameMap->getArmySelectController();
                // Run the ArmySelectController mergeArmies method
                sel->mergeArmies(msg);
            }
            // Then send the message, if anything has actually been written to it.
            // NOTE: This is done outside of the synchronisation of the gameMap. We must never lock the network interface's mutex from
            //       within ONLY the gameMap's mutex, or we have a case for deadlock.
            if (msg.getLength() > 0) netIntf->send(msg);
            return false;
        }
        return true;
    }


    void TopLeftUI::onMouseMove(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY) {
        newUIMouseMove(viewX, viewY);
        MISC_SCALE_TOPLEFT(0, 0)
        highlightSplit = MISC_SCALE_IN_RECT(0, SPLIT_MERGE_Y + 2.0f, SPLIT_MERGE_WIDTH + 2.0f, SPLIT_MERGE_HEIGHT - 2.0f, viewX, viewY);
        highlightMerge = MISC_SCALE_IN_RECT(SPLIT_MERGE_WIDTH, SPLIT_MERGE_Y + 2.0f, SPLIT_MERGE_WIDTH + 2.0f, SPLIT_MERGE_HEIGHT - 2.0f, viewX, viewY);
    }

    void TopLeftUI::updateUnits(int units, int maxUnits) {
        std::lock_guard<std::mutex> lock(dataMutex);
        unitsString = "Units: " + std::to_string(units) + "/" + std::to_string(maxUnits);
    }

    void TopLeftUI::updateColour(uint32_t colour) {
        std::lock_guard<std::mutex> lock(dataMutex);
        this->colour = wool::RGBA(colour);
    }


    // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= NEW UI SYSTEM TEST -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

    TopLeftUI::TopLeftUI(RascClientNetIntf * netIntf) : UIElement(netIntf),
        unitsString("Units: 0/0"),
        colour(wool::RGBA::GREEN),
        highlightSplit(false),
        highlightMerge(true),
        setScale(false),

        testTheme(),
        topLevel(&testTheme),

        lCont(rascUI::Location(64, 64, -96, -256, 0, 0, 0.5, 1.0)), lContP(rascUI::Location(64, 64, -96, -256, 0, 0, 0.5, 1.0)),
            lButton0(rascUI::Location(32, 32, -64, 32, 0, 0, 1, 0), "Left button 0"),
            lButton1(rascUI::Location(32, 32 + 32 + 16, -64, 32, 0, 0, 1, 0), "Left button 1"),
            lCont0(rascUI::Location(32, 128, -64, -88, 0, 0, 1, 0.5)), lCont0P(rascUI::Location(32, 128, -64, -88, 0, 0, 1, 0.5)),
                lButton00(rascUI::Location(32, 32, -64, 32, 0, 0, 1, 0), "Lock selection to left",
                    [this](int button, GLfloat viewX, GLfloat viewY) {
                        topLevel.afterEvent.addFunction([this](void) {
                            topLevel.setSelectionBound(&lCont);
                            topLevel.setKeyboardSelected(&lButton00);
                        });
                    }
                ),
                lButton01(rascUI::Location(32, 32 + 32 + 16, -64, 32, 0, 0, 1, 0), "Unlock selection",
                    [this](int button, GLfloat viewX, GLfloat viewY) {
                        topLevel.afterEvent.addFunction([this](void) {
                            topLevel.resetSelectionBound();
                            topLevel.setKeyboardSelected(&lButton01);
                        });
                    }
                ),
            lCont1(rascUI::Location(32, 72, -64, -176, 0, 0.5, 1, 0.5)), lCont1P(rascUI::Location(32, 72, -64, -176, 0, 0.5, 1, 0.5)),
                lButton10(rascUI::Location(32, 32, -64, 32, 0, 0, 1, 0), "Left button 1-0",
                    [this](int button, GLfloat viewX, GLfloat viewY) {
                        lButton11.setActive(!lButton11.isActive());
                    }
                ),
                lButton11(rascUI::Location(32, 32 + 32 + 16, -64, 32, 0, 0, 1, 0), "Left button 1-1",
                    [this](int button, GLfloat viewX, GLfloat viewY) {
                        lButton10.setActive(!lButton10.isActive());
                    }
                ),
            lButton2(rascUI::Location(32, -64, -64, 32, 0, 1, 1, 0), "Left button 1"),

        rCont(rascUI::Location(64, 64, -96, -256, 0.5, 0, 0.5, 1.0)), rContP(rascUI::Location(64, 64, -96, -256, 0.5, 0, 0.5, 1.0)),
            rButton0(rascUI::Location(32, 32, -64, 32, 0, 0, 1, 0), "Right button 0"),
            rButton1(rascUI::Location(32, 32 + 32 + 16, -64, 32, 0, 0, 1, 0), "Right button 1"),
            rCont0(rascUI::Location(32, 128, -64, -88, 0, 0, 1, 0.5)), rCont0P(rascUI::Location(32, 128, -64, -88, 0, 0, 1, 0.5)),
                rButton00(rascUI::Location(32, 32, -64, 32, 0, 0, 1, 0), "Show popup menu",
                    [this](int button, GLfloat x, GLfloat y) {
                        topLevel.afterEvent.addFunction([this](void) {
                            topLevel.add(&popupCont);
                            topLevel.setSelectionBound(&popupCont);
                            topLevel.setKeyboardSelected(&popupButton0);
                        });
                    }
                ),
                rButton01(rascUI::Location(32, 32 + 32 + 16, -64, 32, 0, 0, 1, 0), "Left button 0-1"),
            rCont1(rascUI::Location(32, 72, -64, -176, 0, 0.5, 1, 0.5)), rCont1P(rascUI::Location(32, 72, -64, -176, 0, 0.5, 1, 0.5)),
                rButton10(rascUI::Location(32, 32, -64, 32, 0, 0, 1, 0), "Right button 1-0"),
                rButton11(rascUI::Location(32, 32 + 32 + 16, -64, 32, 0, 0, 1, 0), "Right button 1-1"),
            rButton2(rascUI::Location(32, -64, -64, 32, 0, 1, 1, 0), "Right button 1"),

        popupCont(rascUI::Location(0, 0, 0, 0, 0.25, 0.25, 0.5, 0.5)), popupContPanel(),
            popupLabel(rascUI::Location(32, 32, -64, 32, 0, 0, 1, 0), "Popup menu"),
            popupButton0(rascUI::Location(32, 32 + 48, -64, 32, 0, 0, 1, 0), "Popup button"),
            popupButtonClose(rascUI::Location(32, 32 + 48 + 48, -64, 32, 0, 0, 1, 0), "Close popup menu",
                [this](int button, GLfloat x, GLfloat y) {
                    topLevel.afterEvent.addFunction([this](void) {
                        topLevel.remove(&popupCont);
                        topLevel.setKeyboardSelected(&rButton00);
                    });
                }
            ),
            toggleSeries(true, [this](int button) {
                std::cout << "Change selection: Button " << button << " selected.\n";
                if (button == rascUI::ToggleButtonSeries::NO_TOGGLE_BUTTON) {
                    toggleButton0.setActive(true);
                    toggleButton1.setActive(true);
                    toggleButton2.setActive(false);
                    toggleButton3.setActive(false);
                } else {
                    toggleButton2.setActive(true);
                    toggleButton3.setActive(true);
                }
            }),
            toggleButton0(&toggleSeries, rascUI::Location(32, 176, -16 - 4, 32, 0, 0, 0.25, 0), "Toggle button 0", [](int button, GLfloat x, GLfloat y) {
                std::cout << "Button 0 on selected.\n";
            }),
            toggleButton1(&toggleSeries, rascUI::Location(24, 176, -16 - 4, 32, 0.25, 0, 0.25, 0), "Toggle button 1", [](int button, GLfloat x, GLfloat y) {
                std::cout << "Button 1 on selected.\n";
            }),
            toggleButton2(&toggleSeries, rascUI::Location(16, 176, -16 - 4, 32, 0.5, 0, 0.25, 0), "Toggle button 2", [this](int button, GLfloat x, GLfloat y) {
                std::cout << "Button 2 on selected.\n";
                toggleButton0.setActive(false);
                toggleButton1.setActive(true);
            }),
            toggleButton3(&toggleSeries, rascUI::Location(8, 176, -16 - 4, 32, 0.75, 0, 0.25, 0), "Toggle button 3", [this](int button, GLfloat x, GLfloat y) {
                std::cout << "Button 3 on selected.\n";
                toggleButton0.setActive(true);
                toggleButton1.setActive(false);
            }),
            textEntryBox(rascUI::Location(32, 176 + 48, -64, 32, 0, 0, 1, 0)) {


        toggleButton2.setActive(false);
        toggleButton3.setActive(false);

        lCont.add(&lButton0);
        lCont.add(&lButton1);
        lCont.add(&lCont0P); lCont.add(&lCont0);
            lCont0.add(&lButton00);
            lCont0.add(&lButton01);
        lCont.add(&lCont1P); lCont.add(&lCont1);
            lCont1.add(&lButton10);
            lCont1.add(&lButton11);
        lCont.add(&lButton2);

        rCont.add(&rButton0);
        rCont.add(&rButton1);
        rCont.add(&rCont0P); rCont.add(&rCont0);
            rCont0.add(&rButton00);
            rCont0.add(&rButton01);
        rCont.add(&rCont1P); rCont.add(&rCont1);
            rCont1.add(&rButton10);
            rCont1.add(&rButton11);
        rCont.add(&rButton2);

        popupCont.add(&popupContPanel);
        popupCont.add(&popupLabel);
        popupCont.add(&popupButton0);
        popupCont.add(&popupButtonClose);

        popupCont.add(&toggleButton0);
        popupCont.add(&toggleButton1);
        popupCont.add(&toggleButton2);
        popupCont.add(&toggleButton3);

        popupCont.add(&textEntryBox);

        topLevel.add(&lContP); topLevel.add(&lCont);
        topLevel.add(&rContP); topLevel.add(&rCont);
    }

    void TopLeftUI::newUIDraw(CameraProperties & cameraProperties, GLfloat elapsed) {
        #ifdef DRAW_TESTING_UI
            if (!setScale) { topLevel.setScalePointers(&cameraProperties.uiScale, &cameraProperties.uiScale); setScale = true; }

            rascUI::Rectangle rect(0.0f, 0.0f, cameraProperties.viewWidth, cameraProperties.viewHeight);
            topLevel.draw(rect, elapsed);
        #endif
    }

    void TopLeftUI::newUIMouseEvent(int button, int state, GLfloat viewX, GLfloat viewY) {
        #ifdef DRAW_TESTING_UI
            topLevel.mouseEvent(button, state, viewX, viewY);
        #endif
    }

    void TopLeftUI::newUIMouseMove(GLfloat viewX, GLfloat viewY) {
        #ifdef DRAW_TESTING_UI
            topLevel.mouseMove(viewX, viewY);
        #endif
    }

    bool TopLeftUI::newUIKeyboardPress(int key, bool special) {
        #ifdef DRAW_TESTING_UI
            return topLevel.keyPress(key, special);
        #else
            return true;
        #endif
    }

    bool TopLeftUI::newUIKeyboardRelease(int key, bool special) {
        #ifdef DRAW_TESTING_UI
            return topLevel.keyRelease(key, special);
        #else
            return true;
        #endif
    }

    // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

}























/*
 * TopLeftUI.h
 *
 *  Created on: 31 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_TOPLEFTUI_H_
#define BASEGAME_CLIENT_UI_TOPLEFTUI_H_

#include <GL/gl.h>
#include <cstdint>
#include <string>

#include "../../../rascUI/base/TopLevelContainer.h"
#include "../../../rascUI/components/generic/BackPanel.h"
#include "../../../rascUI/components/generic/Button.h"
#include "../../../rascUI/components/generic/Label.h"
#include "../../../rascUI/components/textEntry/BasicTextEntryBox.h"
#include "../../../rascUI/components/toggleButton/ToggleButton.h"
#include "../../../rascUI/components/toggleButton/ToggleButtonSeries.h"
#include "../../../rascUI/themes/TestTheme.h"
#include "../../../wool/misc/RGBA.h"
#include "UIElement.h"

namespace rasc {

    class CameraProperties;

    class TopLeftUI : public UIElement {
    private:
        std::string unitsString;
        wool::RGBA colour;

        bool highlightSplit, highlightMerge;

    public:
        TopLeftUI(RascClientNetIntf * netIntf);
        virtual ~TopLeftUI(void);

        virtual void draw(CameraProperties & cameraProperties, GLfloat elapsed) override;
        virtual bool onClick(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY) override;
        virtual void onMouseMove(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY) override;


        void updateUnits(int units, int maxUnits);
        void updateColour(uint32_t colour);



    // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= NEW UI SYSTEM TEST -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    private:
        bool setScale;

        rascUI::TestTheme testTheme;
        rascUI::TopLevelContainer topLevel;

        rascUI::Container lCont; rascUI::BackPanel lContP;
            rascUI::Button lButton0;
            rascUI::Button lButton1;
            rascUI::Container lCont0; rascUI::BackPanel lCont0P;
                rascUI::Button lButton00;
                rascUI::Button lButton01;
            rascUI::Container lCont1; rascUI::BackPanel lCont1P;
                rascUI::Button lButton10;
                rascUI::Button lButton11;
            rascUI::Button lButton2;

        rascUI::Container rCont; rascUI::BackPanel rContP;
            rascUI::Button rButton0;
            rascUI::Button rButton1;
            rascUI::Container rCont0; rascUI::BackPanel rCont0P;
                rascUI::Button rButton00;
                rascUI::Button rButton01;
            rascUI::Container rCont1; rascUI::BackPanel rCont1P;
                rascUI::Button rButton10;
                rascUI::Button rButton11;
            rascUI::Button rButton2;

        rascUI::Container popupCont;
            rascUI::BackPanel popupContPanel;
            rascUI::Label popupLabel;
            rascUI::Button popupButton0;
            rascUI::Button popupButtonClose;

            rascUI::ToggleButtonSeries toggleSeries;
            rascUI::ToggleButton toggleButton0;
            rascUI::ToggleButton toggleButton1;
            rascUI::ToggleButton toggleButton2;
            rascUI::ToggleButton toggleButton3;

            rascUI::BasicTextEntryBox textEntryBox;


        void newUIDraw(CameraProperties & cameraProperties, GLfloat elapsed);
        void newUIMouseMove(GLfloat viewX, GLfloat viewY);
    public:
        void newUIMouseEvent(int button, int state, GLfloat viewX, GLfloat viewY);
        bool newUIKeyboardPress(int key, bool special);
        bool newUIKeyboardRelease(int key, bool special);

    // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

    };

}

#endif
































/*
 * ClientSidePlayerList.cpp
 *
 *  Created on: 24 Aug 2018
 *      Author: wilson
 */

#include "ClientSidePlayerList.h"

#include <GL/gl.h>
#include <cmath>
#include <iostream>

#include "../../../wool/font/Font.h"
#include "../../common/gameMap/CameraProperties.h"
#include "../../common/gameMap/MapModeController.h"
#include "../../common/Misc.h"

namespace rasc {


    ClientSidePlayerList::ClientSidePlayerList(RascClientNetIntf * netIntf) : UIElement(netIntf),
        playerCount(0),
        playerNames(),
        playerColours(),
        playerTurnStatus() {
    }

    ClientSidePlayerList::~ClientSidePlayerList() {
    }



    void ClientSidePlayerList::onPlayerListUpdate(simplenetwork::Message & msg) {
        std::lock_guard<std::mutex> lock(dataMutex);
        playerCount = 0;

        playerNames.clear();
        playerColours.clear();
        playerTurnStatus.clear();
        playerIds.clear();

        while(msg.notFinishedReading()) {
            playerIds[msg.read64()] = playerCount;
            playerColours.push_back(wool::RGBA(msg.read32()));
            playerNames.push_back(msg.readString());
            playerTurnStatus.push_back(msg.read8());
            playerCount++;
        }
    }


    void ClientSidePlayerList::onPlayerTurnStatusChange(uint64_t playerId, bool turnStatus) {
        std::lock_guard<std::mutex> lock(dataMutex);
        playerTurnStatus[playerIds[playerId]] = turnStatus;
    }

    void ClientSidePlayerList::resetAllTurnStatus(void) {
        std::lock_guard<std::mutex> lock(dataMutex);
        for (int i = 0; i < playerCount; i++) {
            playerTurnStatus[i] = false;
        }
    }






    #define PLAYERLIST_ROW_WIDTH 192.0f
    #define PLAYERLIST_ROW_HEIGHT 28.0f

    #define PLAYERLIST_RIGHT_MARGIN 8.0f
    #define PLAYERLIST_Y 16.0f
    #define PLAYERLIST_ROWINCREMENT 26.0f

    #define PLAYERLIST_COLOUR_PREVIEW_X 4.0f
    #define PLAYERLIST_COLOUR_PREVIEW_Y 4.0f
    #define PLAYERLIST_COLOUR_PREVIEW_WIDTH 20.0f
    #define PLAYERLIST_COLOUR_PREVIEW_HEIGHT 20.0f


    #define PLAYERLIST_LEFT_INNER_X 2.0f
    #define PLAYERLIST_LEFT_INNER_Y 2.0f
    #define PLAYERLIST_LEFT_INNER_WIDTH 24.0f
    #define PLAYERLIST_LEFT_INNER_HEIGHT 24.0f

    #define PLAYERLIST_RIGHT_INNER_X 28.0f
    #define PLAYERLIST_RIGHT_INNER_Y 2.0f
    #define PLAYERLIST_RIGHT_INNER_WIDTH 162.0f
    #define PLAYERLIST_RIGHT_INNER_HEIGHT 24.0f


    #define PLAYERLIST_TEXT_X 32.0f

    void ClientSidePlayerList::draw(CameraProperties & cameraProperties, GLfloat elapsed) {

        MISC_SCALE_TOPRIGHT(PLAYERLIST_ROW_WIDTH + PLAYERLIST_RIGHT_MARGIN, PLAYERLIST_Y)

        GLfloat y = 0.0f;

        std::lock_guard<std::mutex> lock(dataMutex);

        for (int i = 0; i < playerCount; i++) {

            // Draw the outer rectangle
            glColor4f(UI_OUTLINE_COLOUR);
            MISC_SCALE_RECT(0, y, PLAYERLIST_ROW_WIDTH, PLAYERLIST_ROW_HEIGHT)

            // Draw the inner rectangles, make selected colour if turn is done
            bool turnFinished = playerTurnStatus[i];
            if (turnFinished) {
                glColor4f(UI_SELECTED_FILL_COLOUR);
            } else {
                glColor4f(UI_FILL_COLOUR);
            }
            MISC_SCALE_RECT(PLAYERLIST_LEFT_INNER_X, PLAYERLIST_LEFT_INNER_Y + y, PLAYERLIST_LEFT_INNER_WIDTH, PLAYERLIST_LEFT_INNER_HEIGHT)
            MISC_SCALE_RECT(PLAYERLIST_RIGHT_INNER_X, PLAYERLIST_RIGHT_INNER_Y + y, PLAYERLIST_RIGHT_INNER_WIDTH, PLAYERLIST_RIGHT_INNER_HEIGHT)

            wool_setColourRGBA(playerColours[i]);
            // Draw the colour preview
            MISC_SCALE_RECT(PLAYERLIST_COLOUR_PREVIEW_X, PLAYERLIST_COLOUR_PREVIEW_Y + y, PLAYERLIST_COLOUR_PREVIEW_WIDTH, PLAYERLIST_COLOUR_PREVIEW_HEIGHT)

            // Draw the player name text
            if (turnFinished) {
                glColor4f(UI_SELECTED_TEXT_COLOUR);
            } else {
                glColor4f(UI_TEXT_COLOUR);
            }
            MISC_SCALE_WIDE_TEXT(playerNames[i], PLAYERLIST_TEXT_X, y + UI_TEXT_Y, UI_TEXT_SIZE)

            y += PLAYERLIST_ROWINCREMENT;
        }

    }











    /*

    OLD SYSTEM FOR DRAWING PLAYER LIST

    #define PLAYERLIST_WIDTH 192
    #define PLAYERLIST_RIGHT_MARGIN 8
    #define PLAYERLIST_Y 16
    #define PLAYERLIST_ROWHEIGHT 32
    #define PLAYERLIST_BGCOL 0.2f, 0.2f, 0.2f, 0.6f
    #define PLAYERLIST_FGCOL 1.0f, 1.0f, 1.0f, 1.0f
    #define PLAYERLIST_BGCOL_TURNFINISHED 0.2f, 0.2f, 0.2f, 0.9f
    #define PLAYERLIST_FGCOL_TURNFINISHED 0.6f, 0.6f, 0.6f, 1.0f
    #define PLAYERLIST_ROWGAP 1
    #define PLAYERLIST_TEXT_X 32.0f
    #define PLAYERLIST_TEXT_Y 4.0f
    #define PLAYERLIST_TEXT_SCALE 1.7142857f

    #define PLAYERLIST_COLOUR_PREVIEW_X 4
    #define PLAYERLIST_COLOUR_PREVIEW_Y 4
    #define PLAYERLIST_COLOUR_PREVIEW_WIDTH 24
    #define PLAYERLIST_COLOUR_PREVIEW_HEIGHT 24

    void ClientSidePlayerList::draw(CameraProperties & cameraProperties) {

        GLfloat x = cameraProperties.viewWidth - (PLAYERLIST_WIDTH + PLAYERLIST_RIGHT_MARGIN) * cameraProperties.uiScale,
                y = PLAYERLIST_Y,
                rowHeight = PLAYERLIST_ROWHEIGHT * cameraProperties.uiScale,
                width = PLAYERLIST_WIDTH * cameraProperties.uiScale,
                textX = PLAYERLIST_TEXT_X * cameraProperties.uiScale,
                textY = PLAYERLIST_TEXT_Y * cameraProperties.uiScale,
                textScale = PLAYERLIST_TEXT_SCALE * cameraProperties.uiScale,

                colourX1 = x + PLAYERLIST_COLOUR_PREVIEW_X * cameraProperties.uiScale,
                colourY1 = PLAYERLIST_COLOUR_PREVIEW_Y * cameraProperties.uiScale,
                colourX2 = x + (PLAYERLIST_COLOUR_PREVIEW_X + PLAYERLIST_COLOUR_PREVIEW_WIDTH) * cameraProperties.uiScale,
                colourY2 = (PLAYERLIST_COLOUR_PREVIEW_Y + PLAYERLIST_COLOUR_PREVIEW_HEIGHT) * cameraProperties.uiScale;

        int rowIncrement = rowHeight + (int)ceil(PLAYERLIST_ROWGAP * cameraProperties.uiScale);

        std::lock_guard<std::mutex> lock(dataMutex);

        for (int i = 0; i < playerCount; i++) {

            bool turnFinished = playerTurnStatus[i];

            if (turnFinished) {
                glColor4f(PLAYERLIST_BGCOL_TURNFINISHED);
            } else {
                glColor4f(PLAYERLIST_BGCOL);
            }
            glVertex2f(x, y);
            glVertex2f(x + width, y);
            glVertex2f(x + width, y + rowHeight);
            glVertex2f(x, y + rowHeight);

            wool_setColourRGBA(playerColours[i]);
            glVertex2f(colourX1, y + colourY1);
            glVertex2f(colourX2, y + colourY1);
            glVertex2f(colourX2, y + colourY2);
            glVertex2f(colourX1, y + colourY2);

            if (turnFinished) {
                glColor4f(PLAYERLIST_FGCOL_TURNFINISHED);
            } else {
                glColor4f(PLAYERLIST_FGCOL);
            }
            MapModeController::font.drawStringScale(playerNames[i], x + textX, y + textY, textScale, textScale);

            y += rowIncrement;
        }
    }*/
}





























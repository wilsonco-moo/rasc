/*
 * BottomLeftUi.h
 *
 *  Created on: 31 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_BOTTOMLEFTUI_H_
#define BASEGAME_CLIENT_UI_BOTTOMLEFTUI_H_

#include <cstdint>
#include <string>

#include "UIElement.h"

namespace rasc {

    class CameraProperties;

    class BottomLeftUI : public UIElement {

    private:
        std::string turnString;
        bool nextTurnButtonHighlighted;
        int mapModeButtonHighlighted;
        bool finishedTurn;

    public:
        BottomLeftUI(RascClientNetIntf * netIntf);
        virtual ~BottomLeftUI(void);

        virtual void draw(CameraProperties & cameraProperties, GLfloat elapsed) override;
        virtual bool onClick(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY) override;
        virtual void onMouseMove(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY) override;

        void updateTurnCount(uint64_t turnCount);
        void onFinishTurn(void);
    };

}

#endif

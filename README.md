### The latest stable version of Rasc is tagged as [rasc/2.0](https://gitlab.com/wilsonco-moo/rasc/-/tree/rasc/2.0).

### For more info, and Rasc downloads, see [the official Rasc webpage](http://wilsonco.mooo.com/rasc).

---

# Rasc

Rasc is a multiplayer turn-based strategy game, similar in general concept
to the board game [Risk](https://en.wikipedia.org/wiki/Risk_(game)).
The game is set on a map, which is subdivided into areas and, in turn,
subdivided into provinces. Players compete to own provinces, by placing
and moving armies.

Each province provides *resources* to their owner: manpower and currency.
Currency is used to maintain armies, and to construct buildings. Manpower is
required to place armies, as an upfront cost.

Rasc is written using C++ and OpenGL.

Rasc has the following features:
 * Multiplayer capability, for any (reasonable) number of players.
 * Computer-controlled (AI) opponents, for players to compete against.
 * There are (currently) two complete maps: Iceland and UK.
 * A scalable and resizable UI system [rascUI](https://gitlab.com/wilsonco-moo/rascui),
   making it possible to comfortable play Rasc on high DPI displays.
 * Rasc is cross-platform, and is able to run in Linux and Windows.
 * A fully customisable map system - new maps can be created using textures and
   XML files.

## Acknowledgements

 * Andrew Ackerley, for providing many game concept ideas/designs, and most of
   Rasc's visual and map design.
 * [LodePNG](https://github.com/lvandeve/lodepng) is used to read textures.
   Its source code is included within Rasc's [wool](https://gitlab.com/wilsonco-moo/wool)
   library.
 * [TinyXML-2](https://github.com/leethomason/tinyxml2) is used to read XML
   files. Its source is also included within Rasc (see src/tinyxml2).
 * [The Waf build system](https://waf.io/) is used in Rasc. This makes it easy
   for anyone to build Rasc (see the section below about how to compile Rasc),
   while also allowing easy building during development, without relying on the
   use of a particular editor or IDE.

**Libraries which are also part of Rasc**

The following other git projects are also part of Rasc. When Rasc started using
[The Waf build system](https://waf.io/) in Rasc version 0.7, these were all
moved to separate git repositories. This was done to improve maintainability,
and to allow their use outside of Rasc.
 * [rascUI](https://gitlab.com/wilsonco-moo/rascui): Rasc's UI system.
 * [threading](https://gitlab.com/wilsonco-moo/threading): A library providing, amongst other things, a thread pool implementation used within Rasc.
 * [sockets](https://gitlab.com/wilsonco-moo/sockets): A networking library, used by Rasc for it's multiplayer functionality.
 * [wool](https://gitlab.com/wilsonco-moo/wool): A graphics library of sorts.
 * [rascUItheme](https://gitlab.com/wilsonco-moo/rascuitheme): This provides dynamic theme functionality for the Rasc UI system.

## Running Rasc in Linux

Rasc relies on some shared libraries (`.so` files) in the same directory as
the `rasc` executable. To launch Rasc using these libraries, run
`LD_LIBRARY_PATH=. ./rasc`. If you do not do this, the libraries won't be
found and the game will refuse to launch.

Rasc is written using OpenGL, meaning that the following libraries are required:
(chances are that you probably have these libraries already).
 * FreeGLUT is used by Rasc as a cross platform way to create and manage
   windows. In Debian/Ubuntu this is available as the `freeglut3` package.
   Other linux distributions may have a slightly different name for this.
 * The OpenGL Utility Toolkit is used by Rasc for various things.
   In Debian/Ubuntu this is available as the `libglu1-mesa` package.
   Again, other linux distributions may have a slightly different name for this.

## Compiling Rasc

Note that pre-compiled versions of Rasc for Linux (32 bit and 64 bit), and Rasc
for Windows (64 bit), are provided on
[my website](http://wilsonco.mooo.com/rasc).

**NOTE:** The 32-bit build of Rasc used to be broken due to
[this gcc bug](https://gcc.gnu.org/bugzilla/show_bug.cgi?id=97273). This
affected the version of gcc distributed with Debian 10, and caused the game to
crash. This is now fixed as of the version of gcc distributed with Debian 11,
so builds from [my web server](http://wilsonco.mooo.com/rasc) are no longer
broken.

**Prerequisites for compiling Rasc (in Linux)**

I have listed below all the things I can think of, which might not already be
installed on any Linux install.
 * The FreeGLUT development library, (the package `freeglut3-dev` in
   Debian/Ubuntu).
 * The OpenGL Utility Toolkit development library, (`libglu1-mesa-dev` in
   Debian/Ubuntu).
 * The GNU C++ compiler, (the package `g++` in Debian/Ubuntu). This must be a
   new enough version to support `C++17`.
 * Python is required for Rasc's Waf-based build system. As far as I know, this
   should work with either Python 2 or Python 3, (although it tries to run with
   python3 by default). You can get Python with the (`python3` package in
   Debian/Ubuntu).
 * Rsync is used by the build script for a couple of things, (the package
   `rsync` in Debian/Ubuntu).
 * The command `dpkg --print-architecture` is used by the packaging script to
   detect the architecture (such as `i386` or `amd64`), when compiling for
   linux. Note that this is not essential, and the build script *will* work
   without it, although it will show the wrong architecture.

I know this all works on Debian 12. I'll update these instructions from time
to time when new Debian versions show up.

**Compiling and packaging for Linux**

 * First checkout the appropriate tag. For example, if you want to compile
   Rasc version 2, do `git checkout "rasc/2.0"`. If you want to compile the
   latest development version, use the latest master branch.
 * Run `./package9 linuxNativeBuildSetup`. This will compile the version of
   Rasc appropriate for your architecture, i.e: `i386` or `amd64`, and bundle
   it into a `.tar.xz` archive.

**Additional prerequisites for cross-compiling to Windows**

 * The unix2dos utility is used by the packaging script for converting the
   README and LICENSE file to dos format, for windows exports. In Debian/Ubuntu
   this is provided by the `dos2unix` package.
 * The Mingw-w64 compiler is used to cross-compile Rasc. This is provided by the
   `mingw-w64` package in Debian/Ubuntu.

**Cross-compiling Rasc for Windows**

 * After checking out the right version, run `./package9 windows64BuildSetup`.
   This will compile Rasc for 64 bit windows, and bundle it into a `.zip`
   archive.
 * Zip is used by the build script for packaging the windows version into a
   zip file, (the package `zip` in Debian/Ubuntu).
 * Note that you may run into a problem with missing dll files
   (`libgcc_s_seh-1.dll` and `libstdc++-6.dll`), if you are
   using a version of Mingw-w64 different to the one distributed with Debian 12.
   In that case, find the location of these files on *your* system.
   The build script expects to find these in `/usr/lib/gcc/x86_64-w64-mingw32/12-posix`,
   although the posix version may be different or your system (such as `10-posix`).
   If so, edit Rasc's build script (`wscript`), and find/replace that filename.

**Compiling in development mode**

 * This allows access to the Rasc development console while in-game (accessible
   with the `o` key). This also allows incrementally compiling the game, helpful
   for development.
 * First run `./waf configure` - this will set up all the libraries.
 * Then run `./waf build` - this will compile the game in development mode.
 * To launch the game in development mode, run `./debugLaunch`. To run the game
   in valgrind, run `./memcheckDebugLaunch`. Note that both of these scripts
   use the `xterm` terminal, but can be modified to use any other terminal.

**Other information about compiling**

 * If you find yourself using a 32-bit system, and want to compile 64-bit Rasc
   for linux, this can be done by enabling multi-arch support (see
   [these instructions](https://wiki.debian.org/Multiarch/HOWTO) for Debian).
   Then, install the 64-bit version of each library Rasc depends on (see above),
   and run `./package9 linuxForce64BuildSetup`. This is how my
   [32-bit server](http://wilsonco.mooo.com/aboutserver) compiles all
   [target Rasc architectures](http://wilsonco.mooo.com/rasc/download),
   automatically each night.
 * If you don't specify a build setup for the packaging script - just run
   `./package9`, by default it will compile the native Linux version AND
   the 64 bit windows version. This is helpful to do if both are needed.

## Screenshots

**Rasc version 1**

Here is an example Rasc game, using the UK map. This game started with 30 AI
players, and one human player. By turn 26, there are only 19 AI players, plus
one human player.
![Image](development/screenshots/rasc2.png)

**Before Rasc 0.4**

Here is another example run of Rasc, with the old UI system, (before Rasc 0.4).
This screenshot shows a game of Rasc with initially 30 AI players, on the UK
map.
![Image](development/screenshots/rasc0.png)

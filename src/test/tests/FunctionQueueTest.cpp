/*
 * FunctionQueueTest.cpp
 *
 *  Created on: 5 May 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_functionQueue == 1
    #include <rascUI/util/FunctionQueue.h>
    #include <functional>
    #include <iostream>
    #include <thread>
    #include <chrono>

    namespace rasc {
        bool functionQueueTest(void) {

            // TODO: Make this work with the most recent version.
            rascUI::FunctionQueue queue;

            queue.addFunction([](void) {
                std::cout << "Not timed 0\n";
            });
            queue.addFunction([](void) {
                std::cout << "Not timed 1\n";
            });
            queue.addFunction([](void) {
                std::cout << "Not timed 2\n";
            });

            void * token = queue.createToken();

            int count = 0;
            std::function<void(void)> func = [&queue, &func, token, &count](void) {
                if (count < 10) queue.addTimedFunction(token, 1000, func);
                std::cout << "MAJOR INTERVAL =============== " << count++ << "\n";
            };
            func();

            int count2 = 0;
            std::function<void(void)> func2 = [&queue, &func2, token, &count2](void) {
                if (count2 < 20) queue.addTimedFunction(token, 500, func2);
                std::cout << "minor interval -------" << count2++ << "\n";
            };
            func2();



            void * repeatToken = queue.createToken();
            int count3 = 0;
            queue.addTimedRepeatingFunction(repeatToken, 4000, 2000, [&count3, repeatToken](void) {
                if (count3 > 10) {
                    rascUI::FunctionQueue::deleteToken(repeatToken);
                }
                std::cout << "Timed interval <><><><><><><><><><><><> " << count3++ << "\n";
            });



            while(true) {
                unsigned long long timeUntil = (unsigned long long)-1,
                                   currentTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
                bool hasStuff = false;

                queue.hasWaitingTimedFunctions(currentTime, &timeUntil, &hasStuff);

                if (!hasStuff) {
                    std::cout << "Run out of actions.\n";
                    break;
                }
                std::cout << "Waiting for " << timeUntil << "ms.\n";
                std::this_thread::sleep_for(std::chrono::milliseconds(timeUntil));
                queue.update();
            }

            rascUI::FunctionQueue::deleteToken(token);

            // Add test functionality here.
            return true;
        }
    }
#endif

#endif

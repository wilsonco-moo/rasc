/*
 * BuildingTest.cpp
 *
 *  Created on: 7 Oct 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_building == 1
    #include <baseGame/common/gameMap/provinceUtil/battle/ProvinceBattleController.h>
    #include <baseGame/common/util/templateTypes/staticStorage/StaticVector.h>
    #include <baseGame/common/gameMap/mapElement/types/Province.h>
    #include <baseGame/common/gameMap/properties/trait/Traits.h>
    #include <baseGame/common/gameMap/properties/PropertyDef.h>
    #include <baseGame/common/gameMap/properties/PropertySet.h>
    #include <baseGame/common/gameMap/properties/Properties.h>
    #include <baseGame/common/playerData/PlayerDataSystem.h>
    #include <baseGame/common/stats/types/ProvincesStat.h>
    #include <baseGame/common/stats/types/BuildingStat.h>
    #include <baseGame/common/playerData/PlayerData.h>
    #include <baseGame/common/stats/types/AreasStat.h>
    #include <baseGame/server/RascBaseGameServer.h>
    #include <baseGame/common/gameMap/GameMap.h>
    #include <baseGame/common/util/CommonBox.h>
    #include <baseGame/common/GameMessages.h>
    #include <baseGame/server/ServerBox.h>
    #include <algorithm>
    #include <iostream>
    #include <sstream>
    #include <cstring>

    namespace rasc {

        // For use in test.
        #define AI_PLAYER_COUNT 2

        /**
         * Returns true if the specified trait should be base-available in the province.
         */
        static bool isBaseAvailable(const PropertySet & propertySet, proptype_t trait) {
            // Reject if not a regular building.
            if (!Traits::isTraitRegularBuilding(trait)) return false;
            // Reject if containing any blocking traits.
            for (proptype_t blocking : Traits::getBlockingTraits(trait)) {
                if (propertySet.hasPropertiesOfType(blocking)) return false;
            }
            // Reject if missing any cost-required traits.
            for (proptype_t costRequired : Traits::getCostRequiredTraits(trait)) {
                if (!propertySet.hasPropertiesOfType(costRequired)) return false;
            }
            // Reject if non-stackable trait which province already has.
            if (!Traits::isTraitStackable(trait) && propertySet.hasPropertiesOfType(trait)) return false;
            return true;
        }

        /**
         * Gets all base available buildings in the specified province.
         */
        static std::unordered_set<proptype_t> getBaseAvailable(const PropertySet & propertySet) {
            std::unordered_set<proptype_t> available;
            for (proptype_t trait : Traits::getRegularBuildingTraits()) {
                if (isBaseAvailable(propertySet, trait)) {
                    available.insert(trait);
                }
            }
            return available;
        }

        /**
         * Gets all buildable buildings in the specified province - these are both
         * base available, and CURRENTLY buildable according to Traits::canTraitBeBuilt.
         */
        static std::unordered_set<proptype_t> getBuildable(Province * province) {
            std::unordered_set<proptype_t> buildable;
            for (proptype_t trait : Traits::getRegularBuildingTraits()) {
                if (isBaseAvailable(province->properties.getPropertySet(), trait) &&
                    Traits::canTraitBeBuilt(*province, trait, 1)) {
                    buildable.insert(trait);
                }
            }
            return buildable;
        }

        /**
         * Prints the specified set of property types, prefixed by the specified message.
         */
        static void printTraits(const std::unordered_set<proptype_t> & traits, const std::string & msg) {
            std::vector<std::string> msgs;
            msgs.reserve(traits.size());
            for (proptype_t trait : traits) {
                msgs.push_back(Properties::typeToInternalName(trait));
            }
            std::sort(msgs.begin(), msgs.end());
            std::cout << msg;
            for (size_t i = 0; i < msgs.size(); i++) {
                if (i != 0) std::cout << ", ";
                std::cout << msgs[i];
            }
            std::cout << ".\n";
        }

        /**
         * Checks that the base available buildings are correct in the specified province.
         */
        static bool checkBaseAvailableBuildings(Province * province, BuildingStat * buildingStat) {
            const PropertySet & propertySet = province->properties.getPropertySet();
            std::unordered_set<proptype_t> generatedAvailable = getBaseAvailable(propertySet);
            const std::unordered_set<proptype_t> & statAvailable = buildingStat->getBaseAvailableBuildings(province->getId());
            printTraits(generatedAvailable, "  Gen:  ");
            printTraits(statAvailable, "  Stat: ");
            ASSERT_TEST(statAvailable == generatedAvailable)
            return true;
        }

        /**
         * Converts a list of BuildingStatInfo, to a map of province ID -> property (building) type and value.
         * Returns false if any duplicate buildings were found.
         */
        template<class A>
        bool buildAvailableMap(std::unordered_map<uint32_t, std::unordered_map<proptype_t, float>> & map, const A & list) {
            for (const BuildingStatInfo & building : list) {
                // Make sure insertion succeeded: If it didn't, a duplicate building was listed.
                ASSERT_TEST(map[building.provinceId].emplace(building.building, building.valueForMoney).second)
            }
            return true;
        }

        /**
         * Returns true if the provided BuildingStatInfo lists, are equal in terms
         * of provinces and building IDs. Returns false if either list contains
         * duplicate buildings.
         */
        template<class A, class B>
        static bool compareAvailableBuildingsLists(const A & listA, const B & listB) {
            std::unordered_map<uint32_t, std::unordered_map<proptype_t, float>> availA, availB;
            ASSERT_TEST(buildAvailableMap(availA, listA))
            ASSERT_TEST(buildAvailableMap(availB, listB))
            return availA == availB;
        }

        /**
         * Prints the available buildings for the specified player.
         */
        template<class A>
        static void printAvailableBuildings(const std::array<A, BuildValuedStatIds::COUNT> & buildings, const std::string & playerName, const CommonBox & box) {
            #define MAX_LINE_LEN 120
            std::cout << "    " << playerName << ": \n";
            #define X(statName, calculation) {                                                                                  \
                std::cout << "        By " #statName ": ";                                                                      \
                unsigned int count = 0;                                                                                         \
                for (const BuildingStatInfo & building : buildings[BuildValuedStatIds::statName]) {                             \
                    std::stringstream stream;                                                                                   \
                    stream << '{' << box.map->getProvince(building.provinceId).getName() << "; " <<                             \
                                     building.valueForMoney << "; " <<                                                          \
                                     Properties::typeToInternalName(building.building) << "}, ";                                \
                    std::string name = stream.str();                                                                            \
                    if (count + name.size() > MAX_LINE_LEN) {                                                                   \
                        std::cout << '\n';                                                                                      \
                        unsigned int spaceCount = std::strlen("        By ") + std::strlen(#statName) + std::strlen(": ");      \
                        for (unsigned int i = 0; i < spaceCount; i++) { std::cout << ' '; }                                     \
                        count = 0;                                                                                              \
                    }                                                                                                           \
                    count += name.size();                                                                                       \
                    std::cout << name;                                                                                          \
                }                                                                                                               \
                std::cout << "\n";                                                                                              \
            }
            RASC_BUILDING_VALUED_PROVINCE_STATS
            #undef X
        }

        /**
         * Gets an array of vectors of BuildingStatInfo, representing buildings by "value-for-money"
         * in the province.
         */
        static std::array<std::vector<BuildingStatInfo>, BuildValuedStatIds::COUNT> getBuildableByValueInProvince(Province * province) {
            const AreasStat * areasStat = province->getGameMap().getCommonBox().areasStat;
            bool areaCompletelyOwned = (areasStat->getAreaOwner(province->getAreaId()) != PlayerData::NO_PLAYER);
            #define X(statName, calculation) std::vector<BuildingStatInfo>(),
            std::array<std::vector<BuildingStatInfo>, BuildValuedStatIds::COUNT> arr = { RASC_BUILDING_VALUED_PROVINCE_STATS };
            #undef X
            std::unordered_set<proptype_t> buildable = getBuildable(province);
            for (proptype_t building : buildable) {
                ProvStatModSet modifier = Traits::getConstructAdditionalModifierSet(*province, building);
                std::array<float, BuildValuedStatIds::COUNT> value;
                province->properties.calculateValueForMoney(value, modifier, Traits::getTraitBuildingCost(NULL, building, 1).getOverallCostEstimation(), areaCompletelyOwned);
                for (unsigned int statId = 0; statId < BuildValuedStatIds::COUNT; statId++) {
                    if (value[statId] > 0.0f) {
                        arr[statId].push_back({province->getId(), value[statId], building});
                    }
                }
            }
            return arr;
        }

        /**
         * Gets an array of vectors of BuildingStatInfo, representing buildings by "value-for-money"
         * across all provided provinces.
         */
        static std::array<std::vector<BuildingStatInfo>, BuildValuedStatIds::COUNT> getBuildableByValueInProvinces(const std::vector<Province *> provinces) {
            #define X(statName, calculation) std::vector<BuildingStatInfo>(),
            std::array<std::vector<BuildingStatInfo>, BuildValuedStatIds::COUNT> arr = { RASC_BUILDING_VALUED_PROVINCE_STATS };
            #undef X
            for (Province * province : provinces) {
                std::array<std::vector<BuildingStatInfo>, BuildValuedStatIds::COUNT> provArr = getBuildableByValueInProvince(province);
                for (unsigned int statId = 0; statId < BuildValuedStatIds::COUNT; statId++) {
                    arr[statId].insert(arr[statId].end(), provArr[statId].begin(), provArr[statId].end());
                }
            }
            return arr;
        }

        /**
         * Checks that available buildings by "value-for-money" are correct for the specified player.
         * This is done by generating available buildings by "value-for-money" using
         * getBuildableByValueInProvinces, and comparing it with what BuildingStat using
         * compareAvailableBuildingsLists.
         */
        static bool checkAvailableBuildings(const CommonBox & box, uint64_t playerId) {
            const std::array<std::multiset<BuildingStatInfo>, BuildValuedStatIds::COUNT> & stat = box.buildingStat->getPlayerAvailableBuildings(playerId);
            std::array<std::vector<BuildingStatInfo>, BuildValuedStatIds::COUNT> gen = getBuildableByValueInProvinces(box.provincesStat->getOwnershipInfo(playerId).getOwnedProvinces().vec());
            for (unsigned int statId = 0; statId < BuildValuedStatIds::COUNT; statId++) {
                ASSERT_TEST(compareAvailableBuildingsLists(stat[statId], gen[statId]));
            }
            printAvailableBuildings(stat, box.playerDataSystem->idToData(playerId)->getName(), box);
            return true;
        }


        /**
         * Tests adding and removing buildings from provinces, to check base-availability
         * of buildings specified by BuildingStat is correct.
         */
        static bool runBaseAvailableTest(ServerBox & box) {
            // Get relevant things.
            BuildingStat * buildingStat = box.common.buildingStat;
            Province * province;

            #define SETUP_PROVINCE(id) {                                                                                                                                \
                province = &box.common.map->getProvince(id);                                                                                                            \
                std::unordered_set<proptype_t> existingTraits;                                                                                                          \
                for (const std::pair<const proptype_t, std::unordered_set<uint64_t>> & pair : province->properties.getPropertySet().getPropertiesByTypeMap()) {               \
                    if (Properties::getBaseType(pair.first) == Properties::BaseTypes::trait) {                                                                          \
                        existingTraits.insert(pair.first);                                                                                                              \
                    }                                                                                                                                                   \
                }                                                                                                                                                       \
                printTraits(existingTraits, std::string("\nInitial traits (") + province->getName() + "):\n  ");                                                        \
                std::cout << "\nInitial available:\n";                                                                                                                  \
                ASSERT_TEST(checkBaseAvailableBuildings(province, buildingStat))                                                                                        \
            }
            #define BUILD(building)                                                                                                         \
                province->properties.addTrait(MiscUpdate::serverAuto(box.server, &box), Properties::Types::building);                       \
                std::cout << "\nAfter building " << Properties::typeToInternalName(Properties::Types::building) << ":\n";                   \
                ASSERT_TEST(checkBaseAvailableBuildings(province, buildingStat))
            #define BUILD_STACK(building, count)                                                                                            \
                province->properties.changeTraitCount(MiscUpdate::serverAuto(box.server, &box), Properties::Types::building, count);        \
                std::cout << "\nAfter building " << Properties::typeToInternalName(Properties::Types::building) << " (" << count << "):\n"; \
                ASSERT_TEST(checkBaseAvailableBuildings(province, buildingStat))
            #define DEMOLISH(building)                                                                                                      \
                province->properties.removeTrait(MiscUpdate::serverAuto(box.server, &box), Properties::Types::building);                    \
                std::cout << "\nAfter demolishing " << Properties::typeToInternalName(Properties::Types::building) << ":\n";                \
                ASSERT_TEST(checkBaseAvailableBuildings(province, buildingStat))

            SETUP_PROVINCE(17) // Bijjlunnden
            DEMOLISH(coastal) BUILD(coastal)
            BUILD(bank) BUILD(stockMarket) BUILD(taxOffice) BUILD(fuelDepot) BUILD(footPaths)
            BUILD_STACK(footPaths, 2) DEMOLISH(stockMarket) DEMOLISH(taxOffice) DEMOLISH(bank)
            BUILD(taxOffice) DEMOLISH(fuelDepot) DEMOLISH(footPaths) DEMOLISH(taxOffice)

            SETUP_PROVINCE(24) // Vjollcano
            BUILD(cutDownForest) BUILD(drainSwamp) BUILD(farm) DEMOLISH(farm) BUILD(fort)
            BUILD_STACK(garrison, 1250) DEMOLISH(garrison) BUILD(barracks) BUILD(recruitmentOffice)
            BUILD(farm) BUILD_STACK(farm, 3) DEMOLISH(farm) BUILD(industrialiseFarmland) BUILD(town)
            BUILD(sewagePlant) BUILD(armyBase) BUILD_STACK(highDensityHousing, 4) DEMOLISH(armyBase)
            DEMOLISH(highDensityHousing) DEMOLISH(sewagePlant) DEMOLISH(industrialiseFarmland)
            DEMOLISH(town) DEMOLISH(recruitmentOffice) DEMOLISH(barracks) DEMOLISH(fort)
            DEMOLISH(drainSwamp) DEMOLISH(cutDownForest)

            return true;
        }

        /**
         * Checks that lists of available buildings per player make sense, while simulating
         * a series of real-game actions between two players.
         */
        bool runPlayerBuildingsTest(ServerBox & box) {

            // For convenience
            #define CLAIM_PROVINCE(playerIndex, provinceId)                         \
                box.common.map->getProvince(provinceId).claimByPlayerData(          \
                    MiscUpdate::serverAuto(box.server, &box),                       \
                    box.common.playerDataSystem->idToData(players[playerIndex]));

            #define PLACE_ARMY(playerIndex, provinceId, units) {                            \
                simplenetwork::Message msg(ServerToClient::miscUpdate);                     \
                box.common.map->getProvince(provinceId).armyControl.placeArmyOrAddUnits(    \
                    msg, box.common.turnCount,                                              \
                    box.common.playerDataSystem->idToData(players[playerIndex]), units);    \
                box.receiveMiscUpdate(msg);                                                 \
            }

            #define MOVE_ARMY(playerIndex, oldProvince, newProvince) {                                          \
                ProvinceArmyController & control = box.common.map->getProvince(oldProvince).armyControl;        \
                uint64_t armyId = control.getArmyOwnedByPlayer(players[playerIndex])->getArmyId();              \
                box.common.map->getProvince(newProvince).armyControl                                            \
                    .moveArmy(MiscUpdate::serverAuto(box.server, &box), oldProvince, armyId);                   \
            }

            // Find player IDs and province ownership of players.
            ASSERT_TEST(AI_PLAYER_COUNT >= 2)
            uint64_t players[AI_PLAYER_COUNT];
            const ProvincesStatInfo * ownership[AI_PLAYER_COUNT];
            {
                unsigned int id = 0;
                for (const std::pair<const uint64_t, RascUpdatable *> pair : box.common.playerDataSystem->mapChildren()) {
                    players[id] = pair.first;
                    ownership[id++] = &box.common.provincesStat->getOwnershipInfo(pair.first);
                }
                ASSERT_TEST(id == AI_PLAYER_COUNT)
            }

            // Do initial check.
            std::cout << "\nInitial:\n";
            ASSERT_TEST(checkAvailableBuildings(box.common, players[0]));
            ASSERT_TEST(checkAvailableBuildings(box.common, players[1]));

            // Player 0 starts in thinj, player 1 starts in vjollcano.
            {
                std::vector<Province *> oldOwned = ownership[0]->getOwnedProvinces().vec();
                for (Province * province : oldOwned) { province->claimUnowned(MiscUpdate::serverAuto(box.server, &box)); }
                for (const std::multiset<BuildingStatInfo> & info : box.common.buildingStat->getPlayerAvailableBuildings(players[0])) { ASSERT_TEST(info.empty()) }
                CLAIM_PROVINCE(0, 6) // Thinj

                oldOwned = ownership[1]->getOwnedProvinces().vec();
                for (Province * province : oldOwned) { province->claimUnowned(MiscUpdate::serverAuto(box.server, &box)); }
                for (const std::multiset<BuildingStatInfo> & info : box.common.buildingStat->getPlayerAvailableBuildings(players[1])) { ASSERT_TEST(info.empty()) }
                CLAIM_PROVINCE(1, 24) // Vjollcano
            }

            // Check now starting provinces are correct.
            std::cout << "\nAfter claiming correct start provinces:\n";
            ASSERT_TEST(checkAvailableBuildings(box.common, players[0]));
            ASSERT_TEST(checkAvailableBuildings(box.common, players[1]));

            // Test gaining an additional province.
            std::cout << "\nAfter gaining an additional province each:\n";
            CLAIM_PROVINCE(0, 12) // Bjendy
            ASSERT_TEST(checkAvailableBuildings(box.common, players[0]));
            CLAIM_PROVINCE(1, 22) // Dujck
            ASSERT_TEST(checkAvailableBuildings(box.common, players[1]));

            // Test claiming rest of respective areas.
            std::cout << "\nAfter gaining the rest of respective areas:\n";
            CLAIM_PROVINCE(0, 19) // Rejavstor
            CLAIM_PROVINCE(0, 13) // Jokklesonn
            CLAIM_PROVINCE(0, 8)  // Plumbaerr
            CLAIM_PROVINCE(1, 20) // Chjunguss
            CLAIM_PROVINCE(1, 16) // Llandjokul
            CLAIM_PROVINCE(1, 23) // Reykjavik
            ASSERT_TEST(checkAvailableBuildings(box.common, players[0]));
            ASSERT_TEST(checkAvailableBuildings(box.common, players[1]));

            // Test losing a province (it being made unowned).
            std::cout << "\nAfter losing a province and it becoming unowned:\n";
            // Plumbaerr
            box.common.map->getProvince(8).claimUnowned(MiscUpdate::serverAuto(box.server, &box));
            ASSERT_TEST(checkAvailableBuildings(box.common, players[0]));
            // Vjollcano
            box.common.map->getProvince(24).claimUnowned(MiscUpdate::serverAuto(box.server, &box));
            ASSERT_TEST(checkAvailableBuildings(box.common, players[1]));

            // Test regaining respective areas.
            std::cout << "\nAfter regaining respective areas:\n";
            CLAIM_PROVINCE(0, 8)  // Plumbaerr
            CLAIM_PROVINCE(1, 24) // Vjollcano
            ASSERT_TEST(checkAvailableBuildings(box.common, players[0]));
            ASSERT_TEST(checkAvailableBuildings(box.common, players[1]));

            // Player 0 takes LLandjokul from player 1
            std::cout << "\nAfter " << box.common.playerDataSystem->idToData(players[0])->getName() << " takes " << box.common.map->getProvince(16).getName() << " from " << box.common.playerDataSystem->idToData(players[1])->getName() << ":\n";
            CLAIM_PROVINCE(0, 16)
            ASSERT_TEST(checkAvailableBuildings(box.common, players[0]));
            ASSERT_TEST(checkAvailableBuildings(box.common, players[1]));

            // Player 1 then reclaims Llandjokul, and takes Plumbaerr from player 0.
            std::cout << "\nAfter " << box.common.playerDataSystem->idToData(players[1])->getName() << " reclaims " << box.common.map->getProvince(16).getName() << " from " << box.common.playerDataSystem->idToData(players[0])->getName() << ", then also takes " << box.common.map->getProvince(8).getName() << " from " << box.common.playerDataSystem->idToData(players[0])->getName() << ":\n";
            CLAIM_PROVINCE(1, 16)
            CLAIM_PROVINCE(1, 8)
            ASSERT_TEST(checkAvailableBuildings(box.common, players[0]));
            ASSERT_TEST(checkAvailableBuildings(box.common, players[1]));

            // Artificially start a battle in Plumbaerr.
            std::cout << "\nAfter a battle erupts in " << box.common.map->getProvince(8).getName() << ":\n";
            PLACE_ARMY(0, 13, 8000) // Jokklesonn
            PLACE_ARMY(1, 8,  8000) // Plumbaerr
            MOVE_ARMY(0, 13, 8) // Jokklesonn -> Plumbaerr
            ASSERT_TEST(box.common.map->getProvince(8).battleControl->isBattleOngoing())
            ASSERT_TEST(checkAvailableBuildings(box.common, players[0]));
            ASSERT_TEST(checkAvailableBuildings(box.common, players[1]));

            // Player 0 reclaims plumbaerr, then player 1's army retreats. Battle stops.
            std::cout << "\nAfter " << box.common.playerDataSystem->idToData(players[0])->getName() << " reclaims " << box.common.map->getProvince(8).getName() << " and " << box.common.playerDataSystem->idToData(players[1])->getName() << "'s army retreats, stopping the battle:\n";
            CLAIM_PROVINCE(0, 8)
            MOVE_ARMY(1, 8, 16) // Plumbaerr -> Llandjokul
            ASSERT_TEST(!box.common.map->getProvince(8).battleControl->isBattleOngoing())
            ASSERT_TEST(checkAvailableBuildings(box.common, players[0]));
            ASSERT_TEST(checkAvailableBuildings(box.common, players[1]));

            return true;
        }

        bool buildingTest(void) {
            // Launch an entire Rasc server. Note however we don't need to listen to any ports.
            ServerBox box("iceland", AI_PLAYER_COUNT, true);
            ASSERT_TEST(runBaseAvailableTest(box))
            box.server->startNextTurn(); // Make sure AI players are created.
            ASSERT_TEST(box.common.playerDataSystem->getRascUpdatableChildCount() == AI_PLAYER_COUNT)
            ASSERT_TEST(runPlayerBuildingsTest(box))
            return true;
        }
    }
#endif

#endif

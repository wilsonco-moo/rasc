/*
 * ZeroPrefixedNumberTest.cpp
 *
 *  Created on: 5 May 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_zeroPrefixedNumber == 1
    #include <baseGame/client/ui/customComponents/zeroPrefixedNumber/ZeroPrefixedNumberProvstat.h>
    #include <baseGame/client/ui/customComponents/zeroPrefixedNumber/ZeroPrefixedNumberUnsigned.h>
    #include <baseGame/common/util/numeric/NumberFormat.h>
    #include <iostream>

    namespace rasc {
        
        // To extract protected members from zero prefixed number.
        class ZeroPrefixExtract : public ZeroPrefixedNumberBase {
        private:
            ZeroPrefixExtract(void) :
                ZeroPrefixedNumberBase(10) {
            }
        public:
            const char * getZeroString(void) {
                if (getBufferSize() == 0) {
                    return "";
                } else {
                    return getZeroStringBuffer();
                }
            }
            const char * getNumberString(void) {
                if (getBufferSize() == 0) {
                    return "";
                } else {
                    return getNumberStringBuffer();
                }
            }
        };
        
        static void printZeroPrefix(ZeroPrefixedNumberBase & number) {
            ZeroPrefixExtract & extract = (ZeroPrefixExtract &)number;
            std::cout << '[' << extract.getZeroString() << "] [" << extract.getNumberString() << "]\n";
        }
        
        bool zeroPrefixedNumberTest(void) {
            
            std::cout << "\nGeneric format: (4 digits)\n";
            
            {
                ZeroPrefixedNumberUnsigned num;
                printZeroPrefix(num);
                num = 5; printZeroPrefix(num);
                
                num = ZeroPrefixedNumberUnsigned(5, &NumberFormat::generic<5>);
                printZeroPrefix(num);
                num = 1; printZeroPrefix(num); 
                num = 15; printZeroPrefix(num); 
                num = 250; printZeroPrefix(num); 
                num = 1576; printZeroPrefix(num); 
                num = 16782; printZeroPrefix(num);
            }
            
            std::cout << "\nGeneric format: (6 digits)\n";
            
            {
                ZeroPrefixedNumberUnsigned num(7, &NumberFormat::generic<7>, 12);
                printZeroPrefix(num);
                num = 0; printZeroPrefix(num);
                num = 1500; printZeroPrefix(num);
                num = 68000; printZeroPrefix(num);
                num = 120022; printZeroPrefix(num);
                num = 57145785; printZeroPrefix(num);
                num = 1456453578; printZeroPrefix(num);
            }
            
            std::cout << "\nCurrency format: (5 digits)\n";
            
            {
                ZeroPrefixedNumberProvstat num;
                printZeroPrefix(num);
                num = 5; printZeroPrefix(num);
                
                num = ZeroPrefixedNumberProvstat(6, &NumberFormat::currency<6>, false);
                printZeroPrefix(num);
                num = 1; printZeroPrefix(num);
                num = -1; printZeroPrefix(num);
                num = 15; printZeroPrefix(num);
                num = 226; printZeroPrefix(num);
                num = 6842; printZeroPrefix(num);
                num = 48688; printZeroPrefix(num);
                num = 5757328; printZeroPrefix(num);
                num = 74886311; printZeroPrefix(num);
            }
            
            return true;
        }
    }
#endif

#endif

/*
 * RandomTest.cpp
 *
 *  Created on: 26 Dec 2023
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_random == 1
    #include <baseGame/common/util/numeric/Random.h>
    #include <baseGame/common/util/Misc.h>
    #include <iostream>
    #include <thread>
    #include <chrono>
    #include <cmath>

    namespace rasc {
        
        template<typename Type>
        using RandomFunc = Type (Random::*)(const Type, const Type);
        
        // Test that the specified pointer to Random member function, doesn't
        // generate values outside the range after trying a few times.
        template<typename Type>
        bool testRangeFunc(Random & random, RandomFunc<Type> func, const Type begin, const Type end) {
            for (unsigned int i = 0; i < 10; i++) {
                const Type value = (random.*func)(begin, end);
                
                ASSERT_TEST(begin < end)
                ASSERT_TEST(value >= begin)
                ASSERT_TEST(value < end)
            }
            
            return true;
        }
        
        // Test the sanity of the random range functions!
        bool randomTest(void) {
            
            // Default (re)construct generator with deterministic seed (so this test is deterministic).
            Random random;
            random.generator = std::mt19937();
            
            // Try 10 combinations each of begin and size.
            for (unsigned int begin = 0; begin < 10; begin++) {
                for (unsigned int size = 1; size <= 10; size++) {
                    
                    // Try each range function, (mostly this test is helpful for the int functions).
                    ASSERT_TEST(testRangeFunc(random, &Random::sizetRange, (size_t)begin, (size_t)(begin + size)));
                    ASSERT_TEST(testRangeFunc(random, &Random::uintRange, (unsigned int)begin, (unsigned int)(begin + size)));
                    ASSERT_TEST(testRangeFunc(random, &Random::glfloatRange, (GLfloat)begin, (GLfloat)(begin + size)));
                }
            }
            
            // Specifically check for:
            // https://bugs.llvm.org/show_bug.cgi?id=18767
            // http://web.archive.org/web/20231028175443/https://bugs.llvm.org/show_bug.cgi?id=18767
            // See implementation in Random.cpp.
            ASSERT_TEST(testRangeFunc(random, &Random::glfloatRange, 0.0f, std::nextafter((GLfloat)0.0f, std::numeric_limits<GLfloat>::max())))
            ASSERT_TEST(testRangeFunc(random, &Random::glfloatRange, 1.0f, std::nextafter((GLfloat)1.0f, std::numeric_limits<GLfloat>::max())))
            ASSERT_TEST(testRangeFunc(random, &Random::glfloatRange, 2.0f, std::nextafter((GLfloat)2.0f, std::numeric_limits<GLfloat>::max())))
            ASSERT_TEST(testRangeFunc(random, &Random::glfloatRange, 100.0f, std::nextafter((GLfloat)100.0f, std::numeric_limits<GLfloat>::max())))
            ASSERT_TEST(testRangeFunc(random, &Random::glfloatRange, 100000.0f, std::nextafter((GLfloat)100000.0f, std::numeric_limits<GLfloat>::max())))
            
            // Demonstrate whether timeSeededRandomRange produces acceptable
            // results for consecutive (millisecond) timestamps.
            for (unsigned int i = 0; i < 50; i++) {
                std::cout << Misc::timeSeededRandomRange(0, 100) << ' ';
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
            }
            std::cout << '\n';
            
            // Experiment: Changing one bit in seed_seq doesn't seem to make all that
            // much difference to the first few numbers generated!
            /*
            std::random_device device;
            const unsigned int deviceRandom = device();
            
            const std::chrono::milliseconds::rep time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            
            {
                std::seed_seq seedSeq({ (std::chrono::milliseconds::rep)deviceRandom, time });
                std::mt19937 gen(seedSeq);
                
                // gen.discard(100);
                
                std::uniform_int_distribution distrib(10, 99);
                for (int i = 0; i < 10; i++) {
                    std::cout << distrib(gen) << " ";
                }
                std::cout << "\n";
            }
            
            {
                std::seed_seq seedSeq({ (std::chrono::milliseconds::rep)deviceRandom, time + 1 });
                std::mt19937 gen(seedSeq);
                
                // gen.discard(100);
                
                std::uniform_int_distribution distrib(10, 99);
                for (int i = 0; i < 10; i++) {
                    std::cout << distrib(gen) << " ";
                }
                std::cout << "\n";
            }
            */
            
            return true;
        }
    }
#endif

#endif

/*
 * StaticStorageInitTest.cpp
 *
 *  Created on: 6 Aug 2023
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_staticStorageInit == 1
    // Add #includes here.
    #include <baseGame/common/util/templateTypes/staticStorage/StaticStorageHelpers.h>
    #include <initializer_list>
    #include <iostream>
    
    namespace rasc {
        namespace staticStorageInitTestData {
            // -----------------------------------------------------------------------------------
            // Structures.
            
            // Struct which can't be converted to an int and back.
            struct SomeStruct {
                bool value;
                constexpr SomeStruct(void) :
                    value(false) {
                }
                constexpr explicit SomeStruct(bool value) :
                    value(value) {
                }
                constexpr bool operator == (const SomeStruct & other) const {
                    return value == other.value;
                }
                constexpr bool operator != (const SomeStruct & other) const {
                    return value != other.value;
                }
            };

            // Trait-like structure with vectors and query method.
            template<template<typename, size_t> class VectorType>
            struct Trait {

                VectorType<int, 100> thing0;
                VectorType<SomeStruct, 100> thing1;
                VectorType<int, 100> thing2;

                template<typename Query>
                constexpr void query(Query & query);
            };

            template<template<typename, size_t> class VectorType>
            template<typename Query>
            constexpr void Trait<VectorType>::query(Query & query) {
                query.run(thing0);
                query.run(thing1);
                query.run(thing2);
            }

            // -----------------------------------------------------------------------------------
            // Trait definition function (fixed vectors).
            
            constexpr std::array<Trait<FixedVector>, 4> getTraits(void) {
                std::array<Trait<FixedVector>, 4> traits;

                traits[0].thing0 = { 1, 2, 3, 4 };
                traits[0].thing1 = { SomeStruct(false), SomeStruct(false), SomeStruct(true) };
                traits[0].thing2 = { 1 };

                traits[1].thing0 = { 5 };
                traits[1].thing1 = { SomeStruct(true), SomeStruct(true) };
                traits[1].thing2 = { };

                traits[2].thing0 = { 124, 1234 };
                traits[2].thing1 = { SomeStruct(false) };
                traits[2].thing2 = { 103, 204 };

                traits[3].thing0 = { 9, 8, 7, 6, 5, 1 };
                traits[3].thing1 = { SomeStruct(true), SomeStruct(true), SomeStruct(true), SomeStruct(true), SomeStruct(false) };
                traits[3].thing2 = { 65 };

                return traits;
            }

            // -----------------------------------------------------------------------------------
            // Static storage and populate
            
            STATIC_STORAGE_DEFINE(int, intData, getTraits())
            STATIC_STORAGE_DEFINE(SomeStruct, someStructData, getTraits())

            constexpr std::array<Trait<SizedStaticVector>, 4> getStaticTraits(void) {
                std::array<Trait<SizedStaticVector>, 4> staticTraits;
                
                STATIC_STORAGE_POPULATE(int, staticTraits, intData, getTraits())
                STATIC_STORAGE_POPULATE(SomeStruct, staticTraits, someStructData, getTraits())

                return staticTraits;
            }

            constexpr static std::array<Trait<SizedStaticVector>, 4> staticTraits = getStaticTraits();
            
            // -----------------------------------------------------------------------------------
            // Static asserts
            // Since we're testing constexpr initialisation features, the whole test
            // can just be static asserts!
            
            static_assert(std::size(intData.buffer) == 17, "Int data buffer is the wrong size!");
            static_assert(std::size(someStructData.buffer) == 11, "Some struct buffer is the wrong size!");
            
            // Checks if data range equals the provided one.
            template<typename Type, typename Container>
            constexpr bool dataEquals(const Container & container, std::initializer_list<Type> data) {
                auto iterUpto = std::begin(container);
                
                for (const Type & value : data) {
                    // Run out of input data.
                    if (iterUpto == std::end(container)) {
                        return false;
                    }
                    
                    // Value differs.
                    if (*iterUpto != value) {
                        return false;
                    }
                    
                    // Move to next in input data too.
                    ++iterUpto;
                }
                
                // All are equal only if we read all input data.
                return iterUpto == std::end(container);
            }
            
            // Check whole data buffers.
            static_assert(dataEquals(intData.buffer, {
                    1, 2, 3, 4,
                    1,
                    5,
                    124, 1234,
                    103, 204,
                    9, 8, 7, 6, 5, 1,
                    65
                }), "Int data buffer is wrong!");
            
            static_assert(dataEquals(someStructData.buffer, {
                    SomeStruct(false), SomeStruct(false), SomeStruct(true),
                    SomeStruct(true), SomeStruct(true),
                    SomeStruct(false),
                    SomeStruct(true), SomeStruct(true), SomeStruct(true), SomeStruct(true), SomeStruct(false)
                }), "SomeStruct data buffer is wrong!");
              
            // Test the data inside the traits.
            static_assert(dataEquals(staticTraits[0].thing0, { 1, 2, 3, 4 }), "Trait0 thing0 data is wrong!");
            static_assert(dataEquals(staticTraits[0].thing1, { SomeStruct(false), SomeStruct(false), SomeStruct(true) }), "Trait0 thing1 data is wrong!");
            static_assert(dataEquals(staticTraits[0].thing2, { 1 }), "Trait0 thing2 data is wrong!");
            
            static_assert(dataEquals(staticTraits[1].thing0, { 5 }), "Trait1 thing0 data is wrong!");
            static_assert(dataEquals(staticTraits[1].thing1, { SomeStruct(true), SomeStruct(true) }), "Trait1 thing1 data is wrong!");
            static_assert(dataEquals<int>(staticTraits[1].thing2, {  }), "Trait1 thing2 data is wrong!");
            
            static_assert(dataEquals(staticTraits[2].thing0, { 124, 1234 }), "Trait2 thing0 data is wrong!");
            static_assert(dataEquals(staticTraits[2].thing1, { SomeStruct(false) }), "Trait2 thing1 data is wrong!");
            static_assert(dataEquals(staticTraits[2].thing2, { 103, 204 }), "Trait2 thing2 data is wrong!");
            
            static_assert(dataEquals(staticTraits[3].thing0, { 9, 8, 7, 6, 5, 1 }), "Trait3 thing0 data is wrong!");
            static_assert(dataEquals(staticTraits[3].thing1, { SomeStruct(true), SomeStruct(true), SomeStruct(true), SomeStruct(true), SomeStruct(false) }), "Trait3 thing1 data is wrong!");
            static_assert(dataEquals(staticTraits[3].thing2, { 65 }), "Trait3 thing2 data is wrong!");
            
            // -----------------------------------------------------------------------------------
            // Actual test code (just prints the data!)
            
            bool doTest(void) {
                std::cout << "Int buffer: (size: " << std::size(intData.buffer) << ")\n  ";
                for (int value : intData.buffer) {
                    std::cout << value << ", ";
                }

                std::cout << "\n\nSome struct buffer: (size: " << std::size(someStructData.buffer) << ")\n  ";
                for (const SomeStruct & value : someStructData.buffer) {
                    std::cout << (value.value ? "true" : "false") << ", ";
                }
                
                size_t index = 0;
                for (const Trait<SizedStaticVector> & trait : staticTraits) {
                    std::cout << "\n\nTrait " << index << ":\n";
                    
                    std::cout << "  thing0:\n    ";
                    for (int value : trait.thing0) {
                        std::cout << value << ", ";
                    }
                    
                    std::cout << "\n  thing1:\n    ";
                    for (const SomeStruct & value : trait.thing1) {
                        std::cout << (value.value ? "true" : "false") << ", ";
                    }
                    
                    std::cout << "\n  thing2:\n    ";
                    for (int value : trait.thing2) {
                        std::cout << value << ", ";
                    }
                    
                    index++;
                }
                std::cout << "\n";

                return true;
            }
        }
        
        bool staticStorageInitTest(void) {
            return staticStorageInitTestData::doTest();
        }
    }
#endif

#endif

/*
 * PathTest.cpp
 *
 *  Created on: 1 Oct 2021
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_path == 1
    #include <baseGame/common/config/StaticConfig.h>
    #include <rascUItheme/utils/config/PathUtils.h>
    #include <iostream>
    
    namespace rasc {
        bool pathTest(void) {

            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("") == "")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("/") == "/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("stuff") == "stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("/stuff") == "/stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("thing/stuff") == "thing/stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("/thing/stuff") == "/thing/stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("//") == "/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("///") == "/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("stuff//") == "stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("stuff///") == "stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("/stuff") == "/stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("/stuff//") == "/stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("/stuff///") == "/stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("thing/stuff") == "thing/stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("/thing/stuff") == "/thing/stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("/thing/stuff//") == "/thing/stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("/thing/stuff///") == "/thing/stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("//stuff") == "//stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("//stuff//") == "//stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("//stuff///") == "//stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("/thing//stuff") == "/thing//stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("//thing//stuff//") == "//thing//stuff/")
            ASSERT_TEST(rascUItheme::PathUtils::addTrailingForwardSlash("///thing//stuff///") == "///thing//stuff/")

            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("") == "")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("/") == "/")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("//") == "/")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("///") == "/")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("stuff") == "stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("stuff/") == "stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("stuff//") == "stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("stuff///") == "stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("/stuff") == "/stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("/stuff/") == "/stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("/stuff//") == "/stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("/stuff///") == "/stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("thing/stuff") == "thing/stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("thing/stuff/") == "thing/stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("thing/stuff//") == "thing/stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("thing/stuff///") == "thing/stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("/thing/stuff") == "/thing/stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("/thing/stuff/") == "/thing/stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("/thing/stuff//") == "/thing/stuff")
            ASSERT_TEST(rascUItheme::PathUtils::removeTrailingForwardSlash("/thing/stuff///") == "/thing/stuff")

            rascUItheme::PathUtils::getHomeDirectory();
            std::cout << "Home directory (test): " << rascUItheme::PathUtils::getHomeDirectory() << "\n";
            
            rascUItheme::PathUtils::getConfigDirectory();
            std::cout << "Config directory (test): " << rascUItheme::PathUtils::getConfigDirectory() << "\n";
            
            rascUItheme::PathUtils::getRascUIConfigDirectory();
            std::cout << "RascUI config directory (test): " << rascUItheme::PathUtils::getRascUIConfigDirectory() << "\n";
            
            rasc::StaticConfig::getRascConfigDirectory();
            std::cout << "Rasc config directory (test): " << rasc::StaticConfig::getRascConfigDirectory() << "\n";
            
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("") == "")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("/") == "/")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("//") == "/")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("///") == "/")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("a") == "a")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("/a") == "a")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("a/") == "a")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("/a/") == "a")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("//a//") == "a")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("thing/stuff") == "stuff")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("/thing/stuff") == "stuff")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("/thing/stuff/") == "stuff")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("///thing///stuff///") == "stuff")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("///thing///stuff.png///") == "stuff.png")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("/a/b/c/d/e/f/g/h") == "h")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("/a/b/c/d/e/f/g/h/") == "h")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("a/b/c/d/e/f/g/h/") == "h")
            ASSERT_TEST(rascUItheme::PathUtils::getFilename("a/b/c/d/e/f/g/h") == "h")
            
            // Add test functionality here.
            return true;
        }
    }
#endif

#endif

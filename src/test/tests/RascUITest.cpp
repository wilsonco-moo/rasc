/*
 * RascUITest.cpp
 *
 *  Created on: 5 May 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_rascUI == 1
    #include <rascUItheme/utils/DynamicThemeLoader.h>
    #include <baseGame/common/config/StaticConfig.h>
    #include <rascUI/components/generic/BackPanel.h>
    #include <rascUI/components/generic/Button.h>
    #include <rascUI/base/TopLevelContainer.h>
    #include <rascUI/base/Component.h>
    #include <rascUI/base/Container.h>
    #include <rascUI/util/Bindings.h>
    #include <iostream>
    #include <utility>
    #include <vector>

    // Uncomment to enable interactive mode.
    //#define TEST_INTERACTIVE_MODE

    namespace rasc {
        bool rascUITest(void) {

            // Dynamically load the theme, exit if load failed.
            rascUItheme::DynamicThemeLoader loader(StaticConfig::THEMES_DIRECTORY +
                StaticConfig::themeFriendlyNameToFilename(StaticConfig::DEFAULT_THEME_FRIENDLY_NAME));
            if (!loader.loadedSuccessfully()) return false;
            rascUI::Theme * theme = loader.getTheme();

            // Create a top level component.
            rascUI::TopLevelContainer topLevel(theme, &rascUI::Bindings::defaultOpenGL);
            topLevel.name = "Top level";

            // Create a bunch of other components.
            rascUI::Container cont0; cont0.name = "Container 0";
            rascUI::Container cont1; cont1.name = "Container 1";
            rascUI::Container cont2; cont2.name = "Container 2";
            rascUI::Container cont3; cont3.name = "Container 3";
            rascUI::Container cont4; cont4.name = "Container 4";
            rascUI::Container cont5; cont5.name = "Container 5";
            rascUI::Container cont6; cont6.name = "Container 6";
            rascUI::Container cont7; cont7.name = "Container 7";
            rascUI::BackPanel panel0; panel0.name = "Panel 0";
            rascUI::BackPanel panel1; panel1.name = "Panel 1";
            rascUI::BackPanel panel2; panel2.name = "Panel 2";
            rascUI::BackPanel panel3; panel3.name = "Panel 3";
            rascUI::BackPanel panel4; panel4.name = "Panel 4";
            rascUI::BackPanel panel5; panel5.name = "Panel 5";
            rascUI::Button button0; button0.name = "Button 0";
            rascUI::Button button1; button1.name = "Button 1";
            rascUI::Button button2; button2.name = "Button 2";
            rascUI::Button button3; button3.name = "Button 3";
            rascUI::Button button4; button4.name = "Button 4";
            rascUI::Button button5; button5.name = "Button 5";
            rascUI::Button button6; button6.name = "Button 6";
            rascUI::Button button7; button7.name = "Button 7";
            rascUI::Button button8; button8.name = "Button 8";
            rascUI::Button button9; button9.name = "Button 9";
            rascUI::Button button10; button10.name = "Button 10";
            rascUI::Button button11; button11.name = "Button 11";

            // A pointer used for stuff.
            rascUI::Component * component;


            // ================================ Test getting last keyboard responsive component (non-depth) ================================
            // Build hierarchy.
            topLevel.add(&panel0);
            //topLevel.add(&button0);
            topLevel.add(&panel1);
            topLevel.add(&cont0);
                cont0.add(&button2);
                cont0.add(&panel2);
                cont0.add(&button3);
            //topLevel.add(&button1);
            topLevel.add(&panel3);

            // Print and check.
            topLevel.print();
            component = topLevel.getLastKeyboardResponsiveComponentNonDepth();
            std::cout << "Last keyboard responsive non-depth: " << component->name << "\n\n";
            if (component != &button3) return false;
            topLevel.clearHierarchy();


            // ================================ Test in more detail with bigger hierarchy ==================================================
            // Build hierarchy.
                cont1.add(&panel3);
                cont1.add(&cont4);
                    cont4.add(&button5);
                    cont4.add(&button6);
                cont1.add(&cont5);
                    cont5.add(&button7);
                    cont5.add(&cont6);
                        cont6.add(&cont7);
                            cont7.add(&button9);
                            cont7.add(&button10);
                            cont7.add(&button11);
                    cont5.add(&button8);

            topLevel.add(&panel0);
            topLevel.add(&button0);
            topLevel.add(&cont0);
                cont0.add(&button3);
                cont0.add(&panel2);
                cont0.add(&cont2);
                    cont2.add(&button4);
                cont0.add(&cont3);
            topLevel.add(&panel1);
            topLevel.add(&button1);
            topLevel.add(&cont1);    // Add the container after building it's hierarchy seperately
            topLevel.add(&button2);

            topLevel.remove(&cont1); // Then remove it again
            topLevel.add(&cont1);    // Then add it again - test the add/remove system works

            // Print and check first keyboard responsive component.
            topLevel.print();
            component = topLevel.getFirstKeyboardResponsiveComponentNonDepth();
            std::cout << "First keyboard responsive non-depth: " << component->name << "\n\n";
            if (component != &button0) return false;

            #ifdef TEST_INTERACTIVE_MODE
                // We can switch containers with w and s, and we can switch components with a and d.
                while(true) {
                    std::string oldName = component->name;
                    component->name = std::string(">> ") + component->name + " <<";
                    std::cout << "\n\n";
                    topLevel.print();
                    component->name = oldName;

                    char c;
                    std::cin >> c;

                    switch(c) {
                        case 'd': component = component->nextKeyboardComponent(); break;
                        case 'a': component = component->previousKeyboardComponent(); break;
                        case 's': component = component->nextKeyboardContainer(); break;
                        case 'w': component = component->previousKeyboardContainer(); break;
                    }
                }
            #else

                // For each first component in each container with keyboard responsive components,
                // the order of keyboard selection, all in order of keyboard selection between
                // containers.
                const std::vector<std::pair<rascUI::Component *, std::vector<rascUI::Component *>>> order({
                    {&button0, {&button0, &button1, &button2}},
                    {&button3, {&button3}},
                    {&button4, {&button4}},
                    {&button5, {&button5, &button6}},
                    {&button7, {&button7, &button8}},
                    {&button9, {&button9, &button10, &button11}}
                });

                // Iterate through each first component in containers.
                for (auto firstCompIter = order.begin(); firstCompIter != order.end(); ++firstCompIter) {

                    // Get the first-components which should be previous and next, according to next/previous keyboard containers.
                    auto nextFirstComp = (firstCompIter == --order.end()) ? order.begin() : (firstCompIter + 1);
                    auto previousFirstComp = (firstCompIter == order.begin()) ? --order.end() : (firstCompIter - 1);

                    // Iterate through each inside component.
                    const std::vector<rascUI::Component *> & insideOrder = firstCompIter->second;
                    for (auto insideCompIter = insideOrder.begin(); insideCompIter != insideOrder.end(); ++insideCompIter) {

                        // Get the inside components which should be previous and next, according to next/previous keyboard components.
                        auto previousInsideComp = (insideCompIter == insideOrder.begin()) ? --insideOrder.end() : (insideCompIter - 1);
                        auto nextInsideComp = (insideCompIter == --insideOrder.end()) ? insideOrder.begin() : (insideCompIter + 1);

                        // Check all four keyboard components: next/previous component, next/previous first-container.
                        rascUI::Component * component = *insideCompIter;
                        std::cout << "Checking keyboard selection order for " << component->name << "...\n";
                        if (component->nextKeyboardComponent() != *nextInsideComp ||
                            component->previousKeyboardComponent() != *previousInsideComp ||
                            component->nextKeyboardContainer() != nextFirstComp->first ||
                            component->previousKeyboardContainer() != previousFirstComp->first) {
                            return false;
                        }
                    }
                }

            #endif

            topLevel.clearHierarchy();
            return true;
        }
    }
#endif

#endif

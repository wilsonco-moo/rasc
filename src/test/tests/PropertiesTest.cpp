/*
 * PropertiesTest.cpp
 *
 *  Created on: 13 Jun 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_properties == 1
    #include <baseGame/common/gameMap/properties/ProvinceProperties.h>
    #include <baseGame/common/gameMap/mapElement/types/Province.h>
    #include <baseGame/common/update/updateTypes/CheckedUpdate.h>
    #include <baseGame/common/gameMap/properties/trait/Traits.h>
    #include <baseGame/common/gameMap/properties/Properties.h>
    #include <baseGame/common/update/updateTypes/MiscUpdate.h>
    #include <baseGame/common/util/numeric/UniqueIdAssigner.h>
    #include <baseGame/common/util/numeric/NumberFormat.h>
    #include <baseGame/server/gameMap/ServerGameMap.h>
    #include <baseGame/common/playerData/PlayerData.h>
    #include <baseGame/common/config/StaticConfig.h>
    #include <baseGame/server/RascBaseGameServer.h>
    #include <baseGame/common/gameMap/GameMap.h>
    #include <sockets/plus/message/Message.h>
    #include <baseGame/common/util/XMLUtil.h>
    #include <baseGame/server/ServerBox.h>
    #include <baseGame/common/util/Misc.h>
    #include <tinyxml2/tinyxml2.h>
    #include <iostream>
    #include <cstdlib>
    #include <cstring>
    #include <sstream>

    namespace rasc {

        // This allows us to register additional rasc updatable children
        // to a province, for testing.
        class PropertiesTestProvince : public Province {
        public:
            void publicRegisterChild(RascUpdatable * updatable) {
                registerChild(updatable);
            }
        };

        // This allows us to write initial data ONLY for a single rasc updatable (the province properties).
        class TestProvinceProperties : public ProvinceProperties {
        public:
            void publicGenerateInitialData(simplenetwork::Message & msg, bool isSaving) {
                onGenerateInitialData(msg, isSaving);
            }
        };

        // The province ID of the province to use for testing (within the iceland map).
        // ID 4 is Jarsysla - we can't use a province with sea on it, because the
        // -100% stat modifiers would make some of the test useless.
        #define TEST_PROVINCE_ID 4

        // Use the iceland map for testing.
        #define TEST_MAP_FILENAME "iceland"

        /**
         * Generates a string representation of a ProvStatMod.
         */
        static std::string statModifierToStr(const ProvStatMod & mod, const char * name, ProvStatId id) {
            std::stringstream str;
            str << '[' << name << ": " << mod.getValue();
            if (mod.getPercentage() != 0) {
                NUMBER_FORMAT_DECL(percentageBuffer, percentage, 8, ", ", mod.getPercentage(), false)
                str << percentageBuffer;
            }
            str << ']';
            return str.str();
        }

        /**
         * Generates a string representation of a ProvStatModSet.
         */
        static std::string statModifierSetToStr(const ProvStatModSet & modSet) {
            std::stringstream str;
            str << '{';
            bool notFirst = false;
            #define X(name, isGoodStat, baseProvinceValue) {                            \
                auto iter = modSet.getPerStatModifiers().find(ProvStatIds::name);       \
                if (iter != modSet.getPerStatModifiers().end()) {                       \
                    if (notFirst) {                                                     \
                        str << ", ";                                                    \
                    }                                                                   \
                    str << statModifierToStr(iter->second, #name, ProvStatIds::name);   \
                    notFirst = true;                                                    \
                }                                                                       \
            }
            RASC_PROVINCE_STATS
            #undef X
            str << '}';
            return str.str();
        }

        /**
         * A test helper method: gets a string representation of a properties.
         */
        static std::string propertiesToString(ProvinceProperties * properties) {
            std::stringstream str;
            str << properties->getProvince().getName() << ": (ID: " << properties->getProvince().getId() << ")\n";
            PropertySet & propertySet = properties->getPropertySet();
            for (const std::pair<const uint64_t, PropertySet::TypeAndStorage> & pair : propertySet.getProperties()) {
                str << "    ID: " << pair.first << ", type: " << (unsigned int)pair.second.getType()
                          << " [" << propertySet.getFriendlyName(pair.first) << ']';
                if (Properties::getBaseType(pair.second.getType()) == Properties::BaseTypes::trait) {
                    str << ", (trait: " << Traits::propertyGetTraitCount(pair.second.getStorage()) << "), ";
                    ProvStatModSet mod;
                    propertySet.getStatModifierSet(pair.first, mod);
                    str << statModifierSetToStr(mod);
                }
                str << ".\n";
            }
            str << "    Overall: " << statModifierSetToStr(properties->getAccumulatedStatModifierSet()) << '\n';
            return str.str();
        }


        // Checks that as much as possible is as expected, concerning the specified trait, count and ID.
        // This is used in runStatPropertiesTest.
        // Note that if the count is zero, the unique ID is not used.
        #define CHECK_TRAIT(name, count, uniqueId)                                                                                                          \
            if (count == 0) {                                                                                                                               \
                ASSERT_TEST(properties->getTraitCount(Properties::Types::name) == 0)                                                                        \
                ASSERT_TEST(!propertySet.hasPropertiesOfType(Properties::Types::name))                                                                      \
                ASSERT_TEST(propertySet.getPropertiesByType(Properties::Types::name) == NULL)                                                               \
                ASSERT_TEST(propertySet.getPropertyStorage(UniqueIdAssigner::INVALID_ID) == NULL)                                                           \
                ASSERT_TEST(propertySet.getPropertyType(UniqueIdAssigner::INVALID_ID) == Properties::Types::INVALID)                                        \
            } else {                                                                                                                                        \
                ASSERT_TEST(properties->getTraitCount(Properties::Types::name) == count)                                                                    \
                ASSERT_TEST(propertySet.hasPropertiesOfType(Properties::Types::name))                                                                       \
                ASSERT_TEST(propertySet.getPropertiesByType(Properties::Types::name)->size() == 1)                                                      \
                ASSERT_TEST(*propertySet.getPropertiesByType(Properties::Types::name)->begin() == uniqueId)                                                 \
                { PropertySet::TypeAndStorage * stor = propertySet.getPropertyStorage(uniqueId);                                                            \
                  ASSERT_TEST(stor != NULL && stor->getType() == Properties::Types::name &&                                                                 \
                                  Traits::propertyGetTraitCount(stor->getStorage()) == count)                                                               \
                  ASSERT_TEST(propertySet.getFriendlyName(uniqueId) == Traits::propertyGetFriendlyName(Properties::Types::name, stor->getStorage()))        \
                  ASSERT_TEST(propertySet.getDescription(uniqueId) == Traits::propertyGetDescription(Properties::Types::name, stor->getStorage())) }        \
                ASSERT_TEST(propertySet.getPropertyType(uniqueId) == Properties::Types::name)                                                               \
                { ProvStatModSet s;                                                                                                                         \
                  propertySet.getStatModifierSet(uniqueId, s);                                                                                              \
                  ASSERT_TEST(s == Traits::getProvinceStatModiferSet(Properties::Types::name, count)) }                                                         \
                ASSERT_TEST(!propertySet.needsUpdating(uniqueId))                                                                                           \
            }


        /**
         * This tries to ensure that the province properties works correctly. This is
         * done by adding and removing properties, checking that they are added and removed
         * correctly, and checking that the reported accumulated stat modifier set remains
         * as expected.
         */
        static bool runStatPropertiesTest(ServerBox * box, Province * province, ProvinceProperties * properties) {
            PropertySet & propertySet = properties->getPropertySet();

            // Get the initial status (we will test against this at the end).
            ProvStatModSet initialMod = properties->getAccumulatedStatModifierSet();
            size_t initialPropertyCount = propertySet.getPropertyCount(),
                   initialDistinctCount = propertySet.getDistinctPropertyTypeCount();
            bool initialNeedsPerTurnUpd = propertySet.needsPerTurnUpdate();
            std::cout << "Initial: " << propertiesToString(properties) << "\n";

            // This will be used for testing as we go along.
            ProvStatModSet expectedMod = initialMod;

            // Check that a bank is not already present.
            CHECK_TRAIT(bank, 0, 0)

            // Add a bank to the properties, update expected stat modifier, record it's ID.
            uint64_t bankId = properties->addTrait(MiscUpdate::serverAuto(box->server, box), Properties::Types::bank);
            std::cout << "Bank added: " << propertiesToString(properties) << "\n";
            expectedMod += Traits::getProvinceStatModiferSet(Properties::Types::bank, 1);

            // Check that the bank is now present, and that the stat modifier is as expected.
            CHECK_TRAIT(bank, 1, bankId)
            ASSERT_TEST(properties->getAccumulatedStatModifierSet() == expectedMod)

            // Add the bank again, check that the returned property is the same, check that nothing changed.
            { uint64_t newBankId = properties->addTrait(MiscUpdate::serverAuto(box->server, box), Properties::Types::bank);
              ASSERT_TEST(newBankId == bankId) }
            CHECK_TRAIT(bank, 1, bankId)
            ASSERT_TEST(properties->getAccumulatedStatModifierSet() == expectedMod)

            // Add a fort to the properties, update expected stat modifier, record it's ID.
            uint64_t fortId = properties->addTrait(MiscUpdate::serverAuto(box->server, box), Properties::Types::fort);
            std::cout << "Fort added: " << propertiesToString(properties) << "\n";
            expectedMod += Traits::getProvinceStatModiferSet(Properties::Types::fort, 1);

            // Check both traits and expected stat modifier.
            CHECK_TRAIT(bank, 1, bankId)
            CHECK_TRAIT(fort, 1, fortId)
            ASSERT_TEST(properties->getAccumulatedStatModifierSet() == expectedMod)

            // Add a garrison (a stackable trait).
            uint64_t garrisonId = properties->changeTraitCount(MiscUpdate::serverAuto(box->server, box), Properties::Types::garrison, 650);
            CHECK_TRAIT(bank, 1, bankId)
            CHECK_TRAIT(fort, 1, fortId)
            CHECK_TRAIT(garrison, 650, garrisonId)

            // Change it's trait count a bunch of times to arbitrary values.
            for (int i : {400, 850, 22, 16443, 98422, 103552, 2, 34, 99, 4324, 45, 5, 45, 5, 6564, 234, 342, 16000}) {
                uint64_t newId = properties->changeTraitCount(MiscUpdate::serverAuto(box->server, box), Properties::Types::garrison, i);
                ASSERT_TEST(newId == garrisonId)
                CHECK_TRAIT(bank, 1, bankId)
                CHECK_TRAIT(fort, 1, fortId)
                CHECK_TRAIT(garrison, i, newId)
            }

            // Make sure the accumulated stat modifier is as expected at the end (the last one).
            std::cout << "Garrison added: " << propertiesToString(properties) << "\n";
            expectedMod += Traits::getProvinceStatModiferSet(Properties::Types::garrison, 16000);
            ASSERT_TEST(properties->getAccumulatedStatModifierSet() == expectedMod)
            CHECK_TRAIT(bank, 1, bankId)
            CHECK_TRAIT(fort, 1, fortId)
            CHECK_TRAIT(garrison, 16000, garrisonId)

            // Save the data of the properties to a message (using the casting trick).
            simplenetwork::Message msg;
            ((TestProvinceProperties *)properties)->publicGenerateInitialData(msg, true);

            // Add some other traits (check that they are correct).
            uint64_t barracksId = properties->addTrait(MiscUpdate::serverAuto(box->server, box), Properties::Types::barracks),
                     housingProjectId = properties->changeTraitCount(MiscUpdate::serverAuto(box->server, box), Properties::Types::housingProject, 31);
            std::cout << "Barracks and housing project added: " << propertiesToString(properties) << "\n";
            expectedMod += Traits::getProvinceStatModiferSet(Properties::Types::barracks, 1);
            expectedMod += Traits::getProvinceStatModiferSet(Properties::Types::housingProject, 31);
            ASSERT_TEST(properties->getAccumulatedStatModifierSet() == expectedMod)
            CHECK_TRAIT(bank, 1, bankId)
            CHECK_TRAIT(fort, 1, fortId)
            CHECK_TRAIT(garrison, 16000, garrisonId)
            CHECK_TRAIT(barracks, 1, barracksId)
            CHECK_TRAIT(housingProject, 31, housingProjectId)

            // Reset the properties by reading initial data from the message. Check that this has
            // reset it to the state it was when we saved it.
            box->receiveInitialData(msg);
            std::cout << "Restored to previous: " << propertiesToString(properties) << "\n";
            expectedMod -= Traits::getProvinceStatModiferSet(Properties::Types::barracks, 1);
            expectedMod -= Traits::getProvinceStatModiferSet(Properties::Types::housingProject, 31);
            ASSERT_TEST(properties->getAccumulatedStatModifierSet() == expectedMod)
            CHECK_TRAIT(bank, 1, bankId)
            CHECK_TRAIT(fort, 1, fortId)
            CHECK_TRAIT(garrison, 16000, garrisonId)
            CHECK_TRAIT(barracks, 0, 0)
            CHECK_TRAIT(housingProject, 0, 0)

            // Remove the bank, do checks.
            { uint64_t removedBank = properties->removeTrait(MiscUpdate::serverAuto(box->server, box), Properties::Types::bank);
              ASSERT_TEST(removedBank == bankId) }
            std::cout << "Bank removed: " << propertiesToString(properties) << "\n";
            expectedMod -= Traits::getProvinceStatModiferSet(Properties::Types::bank, 1);
            ASSERT_TEST(properties->getAccumulatedStatModifierSet() == expectedMod)
            CHECK_TRAIT(bank, 0, 0)
            CHECK_TRAIT(fort, 1, fortId)
            CHECK_TRAIT(garrison, 16000, garrisonId)

            // Remove the bank again, make sure nothing changes.
            { uint64_t removedBank = properties->removeTrait(MiscUpdate::serverAuto(box->server, box), Properties::Types::bank);
              ASSERT_TEST(removedBank == UniqueIdAssigner::INVALID_ID) }
            ASSERT_TEST(properties->getAccumulatedStatModifierSet() == expectedMod)
            CHECK_TRAIT(bank, 0, 0)
            CHECK_TRAIT(fort, 1, fortId)
            CHECK_TRAIT(garrison, 16000, garrisonId)

            // Remove the fort and garrison, updating expected stat modifier.
            { uint64_t removedGarrison = properties->removeTrait(MiscUpdate::serverAuto(box->server, box), Properties::Types::garrison);
              ASSERT_TEST(removedGarrison == garrisonId) }
            expectedMod -= Traits::getProvinceStatModiferSet(Properties::Types::garrison, 16000);
            { uint64_t removedFort = properties->removeTrait(MiscUpdate::serverAuto(box->server, box), Properties::Types::fort);
              ASSERT_TEST(removedFort == fortId) }
            expectedMod -= Traits::getProvinceStatModiferSet(Properties::Types::fort, 1);
            std::cout << "Reset to initial: " << propertiesToString(properties) << "\n";

            // Check that everything matches the initial status.
            ASSERT_TEST(properties->getAccumulatedStatModifierSet() == expectedMod)
            ASSERT_TEST(expectedMod == initialMod)
            CHECK_TRAIT(bank, 0, 0)
            CHECK_TRAIT(fort, 0, 0)
            CHECK_TRAIT(garrison, 0, 0)
            ASSERT_TEST(propertySet.getPropertyCount() == initialPropertyCount)
            ASSERT_TEST(propertySet.getDistinctPropertyTypeCount() == initialDistinctCount)
            ASSERT_TEST(propertySet.needsPerTurnUpdate() == initialNeedsPerTurnUpd)

            return true;
        }

        /**
         * The main test method.
         */
        bool propertiesTest(void) {

            // First run the properties test using an artificially created ProvinceProperties.
            // Doing this was very helpful during development of the new properties system,
            // as there was two versions of ProvinceProperties, with the old version still being
            // used internally by the game.
            {
                // Launch an entire Rasc server. Listen to the appropriate
                // port, since we need network operations. Complain if that fails.
                ServerBox box(TEST_MAP_FILENAME, 4, true);
                if (!box.server->listenToPort(TEST_LISTEN_PORT)) {
                    std::cerr << "Failed to listen to port " << TEST_LISTEN_PORT << " in properties test.\n";
                    return false;
                }

                // Find the province in the game map.
                ASSERT_TEST(box.map->getProvinceCount() > TEST_PROVINCE_ID)
                Province & province = box.map->getProvince(TEST_PROVINCE_ID);

                // Find the XML element in the map XML file, for this province.
                tinyxml2::XMLDocument xml;
                std::string testMapConfigFile = StaticConfig::getMapConfigFile(TEST_MAP_FILENAME);
                loadFile(xml, testMapConfigFile.c_str());
                tinyxml2::XMLElement * rootElement = readElement(xml, "gameMap"),
                                     * provincesElement = readElement(rootElement, "provinces"),
                                     * testProvinceElement = NULL;
                loopElements(elem, provincesElement, "province") {
                    if (std::strtoul(readAttribute(elem, "id"), NULL, 0) == TEST_PROVINCE_ID) {
                        testProvinceElement = elem;
                        break;
                    }
                }
                ASSERT_TEST(testProvinceElement != NULL)

                // Create a ProvinceProperties and add it as a child rasc updatable to our province.
                UniqueIdAssigner uniqueIdAssigner;
                ProvinceProperties properties(province, &uniqueIdAssigner);
                properties.loadFromXML(testProvinceElement);
                ((PropertiesTestProvince &)province).publicRegisterChild(&properties);

                // Run the properties test.
                ASSERT_TEST(runStatPropertiesTest(&box, &province, &properties));
            }



            // Then run the properties test again, using an existing ProvinceProperties
            // created by the game server. This is now possible to do, since the new province properties
            // system has been integrated into the game.
            {
                // Launch an entire Rasc server. Listen to the appropriate
                // port, since we need network operations. Complain if that fails.
                ServerBox box(TEST_MAP_FILENAME, 4, true);
                if (!box.server->listenToPort(TEST_LISTEN_PORT)) {
                    std::cerr << "Failed to listen to port " << TEST_LISTEN_PORT << " in properties test.\n";
                    return false;
                }

                // Find the province in the game map.
                ASSERT_TEST(box.map->getProvinceCount() > TEST_PROVINCE_ID)
                Province & province = box.map->getProvince(TEST_PROVINCE_ID);

                // Run the properties test using an existing ProvinceProperties.
                ASSERT_TEST(runStatPropertiesTest(&box, &province, &province.properties));
            }

            return true;
        }
    }
#endif

#endif

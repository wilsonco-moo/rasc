/*
 * ProbabilityArrayTest.cpp
 *
 *  Created on: 1 Jan 2024
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_probabilityArray == 1
    #include <baseGame/common/util/templateTypes/ProbabilityArray.h>
    #include <iostream>
    
    namespace rasc {
        bool probabilityArrayTest(void) {
            
            // The example from the documentation.
            {
                constexpr ProbabilityArray<unsigned int, char, 3> array({{ 1, 'A' }, { 2, 'B' }, { 3, 'C'}});
                
                ASSERT_TEST(array.getTotal() == 6)
                ASSERT_TEST(array[0].first == 1)
                ASSERT_TEST(array[1].first == 3)
                ASSERT_TEST(array[2].first == 6)
                std::cout << "array.getTotal() == " << array.getTotal() << '\n';
                
                ASSERT_TEST(array.getValue(0) == 'A')
                std::cout << "array.getValue(0) == " << array.getValue(0) << '\n';
                ASSERT_TEST(array.getValue(1) == 'B')
                std::cout << "array.getValue(1) == " << array.getValue(1) << '\n';
                ASSERT_TEST(array.getValue(2) == 'B')
                std::cout << "array.getValue(2) == " << array.getValue(2) << '\n';
                ASSERT_TEST(array.getValue(3) == 'C')
                std::cout << "array.getValue(3) == " << array.getValue(3) << '\n';
                ASSERT_TEST(array.getValue(4) == 'C')
                std::cout << "array.getValue(4) == " << array.getValue(4) << '\n';
                ASSERT_TEST(array.getValue(5) == 'C')
                std::cout << "array.getValue(5) == " << array.getValue(5) << '\n';
            }
            
            // Equal probabilities
            //                upper bounds
            // elem  values   0   1   2   3   4   5
            // 2 A    01      0<2 1<2 2<2 3<2 4<2 5<2
            // 4 B    23              2<4 3<4 4<4 5<4
            // 6 C    45                      4<6 5<6
            {
                constexpr ProbabilityArray<unsigned int, char, 3> array({{ 2, 'A' }, { 2, 'B' }, { 2, 'C' }});
                
                ASSERT_TEST(array.getTotal() == 6)
                ASSERT_TEST(array[0].first == 2)
                ASSERT_TEST(array[1].first == 4)
                ASSERT_TEST(array[2].first == 6)
                
                ASSERT_TEST(array.getValue(0) == 'A')
                ASSERT_TEST(array.getValue(1) == 'A')
                ASSERT_TEST(array.getValue(2) == 'B')
                ASSERT_TEST(array.getValue(3) == 'B')
                ASSERT_TEST(array.getValue(4) == 'C')
                ASSERT_TEST(array.getValue(5) == 'C')
            }
            
            // With a zero in there too.
            {
                constexpr ProbabilityArray<unsigned int, char, 4> array({{ 1, 'A' }, { 3, 'B' }, { 0, 'C' }, { 4, 'D'}});
                
                ASSERT_TEST(array.getTotal() == 8)
                ASSERT_TEST(array[0].first == 1)
                ASSERT_TEST(array[1].first == 4)
                ASSERT_TEST(array[2].first == 4)
                ASSERT_TEST(array[3].first == 8)
                
                ASSERT_TEST(array.getValue(0) == 'A')
                ASSERT_TEST(array.getValue(1) == 'B')
                ASSERT_TEST(array.getValue(2) == 'B')
                ASSERT_TEST(array.getValue(3) == 'B')
                ASSERT_TEST(array.getValue(4) == 'D')
                ASSERT_TEST(array.getValue(5) == 'D')
                ASSERT_TEST(array.getValue(6) == 'D')
                ASSERT_TEST(array.getValue(7) == 'D')
            }
            
            return true;
        }
    }
#endif

#endif

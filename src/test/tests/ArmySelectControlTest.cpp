/*
 * ArmySelectControlTest.cpp
 *
 *  Created on: 5 May 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_armySelectControl == 1
    #include <baseGame/common/gameMap/ArmySelectController.h>
    #include <baseGame/common/gameMap/GameMap.h>
    #include <baseGame/server/ServerBox.h>

    namespace rasc {
        bool armySelectControlTest(void) {

            // Launch an entire Rasc server. Note however we don't need to listen to any ports.
            ServerBox box("iceland", 4, true);

            // Note that if this test is enabled, debugPrintStatus becomes available to use.
            rasc::ArmySelectController & sel = box.common.map->getArmySelectController();
            sel.switchSelect(/*province*/3, /*army*/1);
            sel.multipleSelect(/*province*/0, /*army*/2);
            sel.multipleSelect(/*province*/3, /*army*/15);
            sel.debugPrintStatus();
            sel.onArmyMove(/*old province*/0, /*new province*/1, /*army*/2);
            sel.debugPrintStatus();
            sel.onArmyDestroy(/*army*/15);
            sel.debugPrintStatus();
            sel.switchSelect(/*province*/1, /*army*/25);
            sel.debugPrintStatus();
            sel.unselect();
            sel.debugPrintStatus();

            // Add test functionality here.
            return true;
        }
    }
#endif

#endif

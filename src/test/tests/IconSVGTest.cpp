/*
 * IconSVGTest.cpp
 *
 *  Created on: 27 Jun 2021
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_iconSVG == 1
    #include <baseGame/client/ui/customComponents/icon/IconDrawer.h>
    #include <baseGame/common/util/definitions/IconDef.h>
    #include <iostream>
    #include <iomanip>
    #include <fstream>
    
    namespace rasc {
        // Enum of basic incremental IDs, to count icons.
        #define X(previousName, previousShapeCount, name, shapeCount) name,
        class SVGIconTestIds { public: enum { RASC_ICON_LIST COUNT }; };
        #undef X

        // Array of actual icon IDs: converts incremental ID to icon ID.
        #define X(previousName, previousShapeCount, name, shapeCount) IconDef::name,
        static const unsigned int ICON_TEST_IDS[SVGIconTestIds::COUNT] = { RASC_ICON_LIST };
        #undef X
        
        bool iconSVGTest(void) {
            // Column count, row count.
            constexpr unsigned int columns = 5,
                                   rows = (SVGIconTestIds::COUNT + columns - 1) / columns;
            
            // Other derived sizing values.
            const float scale = 4.0f,
                        width = IconDrawer::BASE_ICON_WIDTH * scale,
                        height = IconDrawer::BASE_ICON_HEIGHT * scale,
                        spacing = IconDrawer::BASE_ICON_SPACING * scale,
                        imageWidth = spacing + (width + spacing) * columns,
                        imageHeight = spacing + (height + spacing) * rows;
            
            // Save to file.
            std::ofstream file("allIcons.svg");
            
            // Purposefully set precision slightly below that which perfectly preserves precision, so that
            // we can remove small rounding errors, which cause issues with SVG rendering.
            // https://stackoverflow.com/a/23437425   (http://web.archive.org/web/20170808192037/https://stackoverflow.com/questions/4459987/convert-float-to-string-without-losing-precision)
            file << std::defaultfloat << std::setprecision(7);
            
            // Start SVG file with opening tags.
            file << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<svg version=\"1.1\" width=\"" << imageWidth << "\" height=\"" << imageHeight << "\" viewBox=\"0 0 " << imageWidth << ' ' << imageHeight << "\">\n";
            
            // Current cell position.
            unsigned int cellX = 0, cellY = 0;
            
            // Iterate all incremental icon IDs.
            for (unsigned int incrementalId = 0; incrementalId < SVGIconTestIds::COUNT; incrementalId++) {
                
                // Get actual icon ID, draw SVG data to file.
                const unsigned int iconId = ICON_TEST_IDS[incrementalId];
                IconDrawer::drawSVG(file, iconId, spacing + cellX * (width + spacing), spacing + cellY * (height + spacing), scale, scale);
                
                // Increment cell position.
                cellX++;
                if (cellX >= columns) {
                    cellX = 0;
                    cellY++;
                }
            }
            
            // End SVG file with closing tag.
            file << "</svg>\n";
            return true;
        }
    }
#endif

#endif

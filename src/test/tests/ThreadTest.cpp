/*
 * ThreadTest.cpp
 *
 *  Created on: 5 May 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

// For convenience, since these are threading tests.
#define SLEEP_FOR(secondCount) \
        std::this_thread::sleep_for(std::chrono::seconds(secondCount))


#if TEST_ENABLED_thread1 == 1
    #include <threading/ThreadQueue.h>
    #include <thread>
    #include <chrono>
    #include <iostream>

    namespace rasc {
        bool thread1Test(void) {
            threading::ThreadQueue queue(3);
            SLEEP_FOR(1);
            uint64_t jobId;
            for (int i = 0; i < 5; i++) {
                queue.addBarrier([i](void){std::cout << "!!! BARRIER START " << i << " !!!\n"; SLEEP_FOR(1); std::cout << "!!! BARRIER END " << i << " !!!\n";});
            }
            for (int i = 0; i < 15; i++) {
                queue.add([i](void){std::cout << "!!! JOB START " << i << " !!!\n"; SLEEP_FOR(1); std::cout << "!!! JOB END " << i << " !!!\n";});
            }
            for (int i = 0; i < 5; i++) {
                jobId = queue.addBarrier([i](void){std::cout << "!!! BARRIER START " << i << " !!!\n"; SLEEP_FOR(1); std::cout << "!!! BARRIER END " << i << " !!!\n";});
            }
            queue.waitForJobToComplete(jobId);
            return true;
        }
    }
#endif


#if TEST_ENABLED_thread2 == 1
    #include <threading/ThreadQueue.h>
    #include <iostream>
    #include <thread>
    #include <chrono>

    namespace rasc {
        bool thread2Test(void) {
            threading::ThreadQueue queue(4);
            SLEEP_FOR(1);
            size_t threadId = queue.getExampleThread();
            uint64_t jobId;
            {
                int i = 0;
                queue.addToThread(threadId, [i](void){std::cout << "!!! JOB START " << i << " !!!\n"; SLEEP_FOR(5); std::cout << "!!! JOB END " << i << " !!!\n";});
                i++;
                jobId = queue.addToThread(threadId, [i](void){std::cout << "!!! JOB START " << i << " !!!\n"; SLEEP_FOR(5); std::cout << "!!! JOB END " << i << " !!!\n";});
            }
            for (int i = 0; i < 15; i++) {
                std::cout << "<><><><> Job status: " << threading::jobStatusName(queue.getJobStatus(jobId)) << " <><><><>\n";
                SLEEP_FOR(1);
            }
            return true;
        }
    }
#endif

#endif

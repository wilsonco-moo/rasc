/*
 * StatModifierTest.cpp
 *
 *  Created on: 13 Jun 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_statModifier == 1
    #include <baseGame/common/gameMap/provinceUtil/ProvStatMod.h>
    #include <iostream>

    namespace rasc {
        bool statModifierTest(void) {

            // Check that operator== works for ProvStatMod.
            ASSERT_TEST(ProvStatMod() == ProvStatMod())
            ASSERT_TEST(ProvStatMod(0, 0) == ProvStatMod())
            ASSERT_TEST(ProvStatMod() == ProvStatMod(0, 0))
            ASSERT_TEST(ProvStatMod(0, 0) == ProvStatMod(0, 0))
            ASSERT_TEST(ProvStatMod(20, 0) == ProvStatMod(20, 0))
            ASSERT_TEST(ProvStatMod(0, 20) == ProvStatMod(0, 20))
            ASSERT_TEST(ProvStatMod(30, 20) == ProvStatMod(30, 20))
            ASSERT_TEST(ProvStatMod(-30, 20) == ProvStatMod(-30, 20))
            ASSERT_TEST(ProvStatMod(-30, -20) == ProvStatMod(-30, -20))
            ASSERT_TEST(!(ProvStatMod(20, 0) == ProvStatMod()))
            ASSERT_TEST(!(ProvStatMod() == ProvStatMod(20, 0)))
            ASSERT_TEST(!(ProvStatMod(20, 90) == ProvStatMod(90, 90)))
            ASSERT_TEST(!(ProvStatMod(90, 90) == ProvStatMod(20, 90)))
            ASSERT_TEST(!(ProvStatMod(90, 20) == ProvStatMod(90, 90)))
            ASSERT_TEST(!(ProvStatMod(90, 90) == ProvStatMod(90, 20)))
            ASSERT_TEST(!(ProvStatMod(20, 30) == ProvStatMod(90, 90)))
            ASSERT_TEST(!(ProvStatMod(90, 90) == ProvStatMod(20, 30)))

            // Check that operator!= works for ProvStatMod.
            ASSERT_TEST(!(ProvStatMod() != ProvStatMod()))
            ASSERT_TEST(!(ProvStatMod(0, 0) != ProvStatMod()))
            ASSERT_TEST(!(ProvStatMod() != ProvStatMod(0, 0)))
            ASSERT_TEST(!(ProvStatMod(0, 0) != ProvStatMod(0, 0)))
            ASSERT_TEST(!(ProvStatMod(20, 0) != ProvStatMod(20, 0)))
            ASSERT_TEST(!(ProvStatMod(0, 20) != ProvStatMod(0, 20)))
            ASSERT_TEST(!(ProvStatMod(30, 20) != ProvStatMod(30, 20)))
            ASSERT_TEST(!(ProvStatMod(-30, 20) != ProvStatMod(-30, 20)))
            ASSERT_TEST(!(ProvStatMod(-30, -20) != ProvStatMod(-30, -20)))
            ASSERT_TEST(ProvStatMod(20, 0) != ProvStatMod())
            ASSERT_TEST(ProvStatMod() != ProvStatMod(20, 0))
            ASSERT_TEST(ProvStatMod(20, 90) != ProvStatMod(90, 90))
            ASSERT_TEST(ProvStatMod(90, 90) != ProvStatMod(20, 90))
            ASSERT_TEST(ProvStatMod(90, 20) != ProvStatMod(90, 90))
            ASSERT_TEST(ProvStatMod(90, 90) != ProvStatMod(90, 20))
            ASSERT_TEST(ProvStatMod(20, 30) != ProvStatMod(90, 90))
            ASSERT_TEST(ProvStatMod(90, 90) != ProvStatMod(20, 30))

            // Check that operator+ works for ProvStatMod.
            ASSERT_TEST(ProvStatMod() + ProvStatMod(30, 20) == ProvStatMod(30, 20))
            ASSERT_TEST(ProvStatMod(30, 20) + ProvStatMod() == ProvStatMod(30, 20))
            ASSERT_TEST(ProvStatMod(16, 48) + ProvStatMod(12, 33) == ProvStatMod(28, 81))
            ASSERT_TEST(ProvStatMod(12, 33) + ProvStatMod(16, 48) == ProvStatMod(28, 81))

            // Check that operator+= works for ProvStatMod.
            { ProvStatMod m; m += ProvStatMod(30, 20); ASSERT_TEST(m == ProvStatMod(30, 20)) }
            { ProvStatMod m(30, 20); m += ProvStatMod(); ASSERT_TEST(m == ProvStatMod(30, 20)) }
            { ProvStatMod m(16, 48); m += ProvStatMod(12, 33); ASSERT_TEST(m == ProvStatMod(28, 81)) }
            { ProvStatMod m(12, 33); m += ProvStatMod(16, 48); ASSERT_TEST(m == ProvStatMod(28, 81)) }

            // Check that operator- works for ProvStatMod.
            ASSERT_TEST(ProvStatMod() - ProvStatMod(30, 20) == ProvStatMod(-30, -20))
            ASSERT_TEST(ProvStatMod(30, 20) - ProvStatMod() == ProvStatMod(30, 20))
            ASSERT_TEST(ProvStatMod(16, 48) - ProvStatMod(12, 33) == ProvStatMod(4, 15))
            ASSERT_TEST(ProvStatMod(12, 33) - ProvStatMod(16, 48) == ProvStatMod(-4, -15))

            // Check that operator-= works for ProvStatMod.
            { ProvStatMod m; m -= ProvStatMod(30, 20); ASSERT_TEST(m == ProvStatMod(-30, -20)) }
            { ProvStatMod m(30, 20); m -= ProvStatMod(); ASSERT_TEST(m == ProvStatMod(30, 20)) }
            { ProvStatMod m(16, 48); m -= ProvStatMod(12, 33); ASSERT_TEST(m == ProvStatMod(4, 15)) }
            { ProvStatMod m(12, 33); m -= ProvStatMod(16, 48); ASSERT_TEST(m == ProvStatMod(-4, -15)) }

            // Check that operator* works for ProvStatMod (working out final stat value).
            ASSERT_TEST(ProvStatMod() * 0 == 0)
            ASSERT_TEST(ProvStatMod() * 102 == 102)
            ASSERT_TEST(ProvStatMod() * -38 == -38)
            ASSERT_TEST(ProvStatMod(0, -100) * 40 == 0)
            ASSERT_TEST(ProvStatMod(10, 0) * 40 == 50)
            ASSERT_TEST(ProvStatMod(10, 50) * 40 == 75)
            ASSERT_TEST(ProvStatMod(10, 100) * 40 == 100)

            // ============================= Check that operator== works for ProvStatModSet ======================

            ASSERT_TEST(ProvStatModSet() == ProvStatModSet())

            ASSERT_TEST(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)}
            }) == ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)}
            }))

            ASSERT_TEST(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            }) == ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            }))

            ASSERT_TEST(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            }) == ProvStatModSet({
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)},
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)}
            }))

            ASSERT_TEST(!(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            }) == ProvStatModSet()))

            ASSERT_TEST(!(ProvStatModSet() == ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            })))

            ASSERT_TEST(!(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(10, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            }) == ProvStatModSet({
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)},
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)}
            })))

            ASSERT_TEST(!(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            }) == ProvStatModSet({
                {ProvStatIds::currencyIncome, ProvStatMod(63, 15)},
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)}
            })))

            ASSERT_TEST(!(ProvStatModSet() == ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod()}
            })))

            ASSERT_TEST(!(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod()}
            }) == ProvStatModSet()))

            // ================================== Check that operator!= works for ProvStatModSet =========================

            ASSERT_TEST(!(ProvStatModSet() != ProvStatModSet()))

            ASSERT_TEST(!(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)}
            }) != ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)}
            })))

            ASSERT_TEST(!(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            }) != ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            })))

            ASSERT_TEST(!(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            }) != ProvStatModSet({
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)},
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)}
            })))

            ASSERT_TEST(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            }) != ProvStatModSet())

            ASSERT_TEST(ProvStatModSet() != ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            }))

            ASSERT_TEST(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(10, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            }) != ProvStatModSet({
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)},
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)}
            }))

            ASSERT_TEST(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)},
                {ProvStatIds::currencyIncome, ProvStatMod(62, 15)}
            }) != ProvStatModSet({
                {ProvStatIds::currencyIncome, ProvStatMod(63, 15)},
                {ProvStatIds::manpowerIncome, ProvStatMod(20, 30)}
            }))

            ASSERT_TEST(ProvStatModSet() != ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod()}
            }))

            ASSERT_TEST(ProvStatModSet({
                {ProvStatIds::manpowerIncome, ProvStatMod()}
            }) != ProvStatModSet())

            // ==================== Check that operators +=, -=, +, - work for ProvStatModSet ========================

            // For each one, check multiple variants of both addition and subtraction.
            #define CHECK_ARITHMETIC(inputA, inputB, expectedResult)                                                                   \
                /* Check adding (both ways around)                                                                                  */ \
                CHECK_ADD(ProvStatModSet inputA, ProvStatModSet inputB, ProvStatModSet expectedResult)                                 \
                CHECK_ADD(ProvStatModSet inputB, ProvStatModSet inputA, ProvStatModSet expectedResult)                                 \
                /* Check subtraction (a--b=c, b--a=c, c-b=a, c-a=b)                                                                 */ \
                CHECK_SUBTRACT(ProvStatModSet inputA, (ProvStatModSet() - ProvStatModSet inputB), ProvStatModSet expectedResult)       \
                CHECK_SUBTRACT(ProvStatModSet inputB, (ProvStatModSet() - ProvStatModSet inputA), ProvStatModSet expectedResult)       \
                CHECK_SUBTRACT(ProvStatModSet expectedResult, ProvStatModSet inputB, ProvStatModSet inputA)                            \
                CHECK_SUBTRACT(ProvStatModSet expectedResult, ProvStatModSet inputA, ProvStatModSet inputB)

            // To check addition, check both operator+ and operator+=.
            #define CHECK_ADD(inputA, inputB, expectedResult)              \
                /* Check operator+                                      */ \
                ASSERT_TEST(inputA + inputB == expectedResult)             \
                                                                           \
                /* Check operator+=                                     */ \
                {                                                          \
                    ProvStatModSet s = inputA;                             \
                    s += inputB;                                           \
                    ASSERT_TEST(s == expectedResult)                       \
                }

            // To check subtraction, check both operator- and operator-=.
            #define CHECK_SUBTRACT(inputA, inputB, expectedResult)         \
                /* Check operator-                                      */ \
                ASSERT_TEST(inputA - inputB == expectedResult)             \
                                                                           \
                /* Check operator-=                                     */ \
                {                                                          \
                    ProvStatModSet s = inputA;                             \
                    s -= inputB;                                           \
                    ASSERT_TEST(s == expectedResult)                       \
                }

            // In all of the following cases, it should be true that inputA + inputB == expectedResult.

            CHECK_ARITHMETIC((), (), ())

            CHECK_ARITHMETIC((), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)}
            }))


            CHECK_ARITHMETIC(({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(6, 2)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(21, 63)}
            }))

            CHECK_ARITHMETIC(({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)}
            }), ({
                {ProvStatIds::currencyIncome, ProvStatMod(6, 2)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)},
                {ProvStatIds::currencyIncome, ProvStatMod(6, 2)}
            }))

            CHECK_ARITHMETIC(({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)},
                {ProvStatIds::currencyIncome, ProvStatMod(5, 7)}
            }), ({
                {ProvStatIds::currencyIncome, ProvStatMod(6, 2)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)},
                {ProvStatIds::currencyIncome, ProvStatMod(11, 9)}
            }))

            CHECK_ARITHMETIC(({
                {ProvStatIds::currencyIncome, ProvStatMod(6, 2)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)},
                {ProvStatIds::currencyIncome, ProvStatMod(5, 7)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)},
                {ProvStatIds::currencyIncome, ProvStatMod(11, 9)}
            }))

            CHECK_ARITHMETIC(({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(-15, -61)}
            }), ())

            CHECK_ARITHMETIC(({
                {ProvStatIds::manpowerIncome, ProvStatMod(-15, -61)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)}
            }), ())

            CHECK_ARITHMETIC(({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)},
                {ProvStatIds::currencyIncome, ProvStatMod(80, 92)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(-15, -61)}
            }), ({
                {ProvStatIds::currencyIncome, ProvStatMod(80, 92)}
            }))

            CHECK_ARITHMETIC(({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(-15, -61)},
                {ProvStatIds::currencyIncome, ProvStatMod(80, 92)}
            }), ({
                {ProvStatIds::currencyIncome, ProvStatMod(80, 92)}
            }))

            CHECK_ARITHMETIC(({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)},
                {ProvStatIds::currencyIncome, ProvStatMod(-22, -14)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(-15, -61)},
                {ProvStatIds::currencyIncome, ProvStatMod(22, 14)}
            }), ())

            CHECK_ARITHMETIC(({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)},
                {ProvStatIds::currencyIncome, ProvStatMod(-22, -14)}
            }), ({
                {ProvStatIds::currencyIncome, ProvStatMod(22, 14)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)}
            }))


            CHECK_ARITHMETIC(({
                {ProvStatIds::currencyIncome, ProvStatMod(22, 14)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)},
                {ProvStatIds::currencyIncome, ProvStatMod(-22, -14)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)}
            }))

            CHECK_ARITHMETIC(({
                {ProvStatIds::currencyIncome, ProvStatMod(22, 14)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)},
                {ProvStatIds::currencyIncome, ProvStatMod(10, 11)}
            }), ({
                {ProvStatIds::manpowerIncome, ProvStatMod(15, 61)},
                {ProvStatIds::currencyIncome, ProvStatMod(32, 25)}
            }))

            return true;
        }
    }
#endif

#endif

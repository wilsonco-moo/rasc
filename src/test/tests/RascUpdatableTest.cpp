/*
 * RascUpdatableTest.cpp
 *
 *  Created on: 5 May 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_rascUpdatable == 1
    #include <baseGame/common/update/TopLevelRascUpdatable.h>
    #include <baseGame/common/update/RascUpdatable.h>
    #include <sockets/plus/message/Message.h>
    #include <iostream>
    #include <string>

    namespace rasc {

        /**
         * A parent class.
         */
        class ParentPotato : public rasc::TopLevelRascUpdatable {
        public:
            std::string expectedString;
            ParentPotato(const std::string & expectedString) :
                rasc::TopLevelRascUpdatable(rasc::RascUpdatable::Mode::map, 2),
                expectedString(expectedString) {
            }
            virtual ~ParentPotato(void) {}

        protected:
            virtual bool onReceiveMiscUpdate(simplenetwork::Message & msg) override {
                std::string str = msg.readString();
                std::cout << "Received by parent: ";
                printIdInformation();
                std::cout << ": [" << str << "]\n";
                if (str == expectedString) {
                    return true;
                } else {
                    std::cout << "WRONG MESSAGE: found: [" << str << "], " << "expected: [" << expectedString << "]\n";
                    return false;
                }
            }

        public:
            /**
             * For convenience, allows outside access to child registration.
             */
            inline void doRegisterChild(RascUpdatable * child, uint64_t id) {
                registerChild(child, id);
            }
        };

        /**
         * A child class.
         */
        class ChildPotato : public rasc::RascUpdatable {
        public:
            std::string expectedString;
            ChildPotato(const std::string & expectedString) :
                rasc::RascUpdatable(rasc::RascUpdatable::Mode::map, 8),
                expectedString(expectedString) {
            }
            virtual ~ChildPotato(void) {}

        protected:
            virtual bool onReceiveMiscUpdate(simplenetwork::Message & msg) override {
                std::string str = msg.readString();
                std::cout << "Received by child: ";
                printIdInformation();
                std::cout << ": [" << str << "]\n";
                if (str == expectedString) {
                    return true;
                } else {
                    std::cout << "WRONG MESSAGE: found: [" << str << "], " << "expected: [" << expectedString << "]\n";
                    return false;
                }
            }

        public:
            /**
             * For convenience, allows outside access to child registration.
             */
            inline void doRegisterChild(RascUpdatable * child, uint64_t id) {
                registerChild(child, id);
            }
        };

        bool rascUpdatableTest(void) {

            // Create some RascUpdatables.
            ParentPotato parent("Message sent to parent");
            ChildPotato child0("Message sent to zeroth child"),
                        child1("Message sent to first child"),
                        child2("Message sent to second child"),
                        child3("Message sent to third child"),
                        child4("Message sent to fourth child"),
                        child5("Message sent to fifth child"),
                        child6("Message sent to sixth child"),
                        child7("Message sent to seventh child"),
                        child8("Message sent to eigth child"),
                        child9("Message sent to ninth child"),
                        child10("Message sent to tenth child"),
                        child11("Message sent to eleventh child"),
                        child12("Message sent to twelfth child");

            // Register their hierarchy.
            parent.doRegisterChild(&child0, 123);
                child0.doRegisterChild(&child1, 1);
                child0.doRegisterChild(&child2, 2);
                    child2.doRegisterChild(&child3, 65535);
                        child3.doRegisterChild(&child4, 65535);
                child0.doRegisterChild(&child5, 3);
            parent.doRegisterChild(&child6, 456);
            parent.doRegisterChild(&child7, 789);
                child7.doRegisterChild(&child8, 32768);
                    child8.doRegisterChild(&child9, 156);
                    child8.doRegisterChild(&child10, 231);
                    child8.doRegisterChild(&child11, 12);
                child7.doRegisterChild(&child12, 32767);

            simplenetwork::Message msg;
            bool success = true;

            #define RUN_RASC_UPDATABLE_TEST(rascUpdatable, messageStr)  \
                rascUpdatable.writeUpdateHeader(msg);                   \
                msg.writeCString(messageStr);                           \
                success &= parent.receiveMiscUpdate(msg);               \
                msg.clear();

            RUN_RASC_UPDATABLE_TEST(parent, "Message sent to parent")
            RUN_RASC_UPDATABLE_TEST(child0, "Message sent to zeroth child")
            RUN_RASC_UPDATABLE_TEST(child1, "Message sent to first child")
            RUN_RASC_UPDATABLE_TEST(child2, "Message sent to second child")
            RUN_RASC_UPDATABLE_TEST(child3, "Message sent to third child")
            RUN_RASC_UPDATABLE_TEST(child4, "Message sent to fourth child")
            RUN_RASC_UPDATABLE_TEST(child5, "Message sent to fifth child")
            RUN_RASC_UPDATABLE_TEST(child6, "Message sent to sixth child")
            RUN_RASC_UPDATABLE_TEST(child7, "Message sent to seventh child")
            RUN_RASC_UPDATABLE_TEST(child8, "Message sent to eigth child")
            RUN_RASC_UPDATABLE_TEST(child9, "Message sent to ninth child")
            RUN_RASC_UPDATABLE_TEST(child10, "Message sent to tenth child")
            RUN_RASC_UPDATABLE_TEST(child11, "Message sent to eleventh child")
            RUN_RASC_UPDATABLE_TEST(child12, "Message sent to twelfth child")

            return success;
        }
    }
#endif

#endif

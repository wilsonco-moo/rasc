/*
 * ColourBlindImageGenTest.cpp
 *
 *  Created on: 3 Sep 2023
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_colourBlindImageGen == 1
    #include <baseGame/common/util/numeric/MultiCol.h>
    #include <baseGame/common/util/numeric/Random.h>
    #include <baseGame/server/RascBaseGameServer.h>
    #include <wool/texture/lodepng/lodepng.h>
    #include <iostream>
    #include <cstdlib>
    #include <string>
    #include <cmath>
    
    namespace rasc {
        
        #define COLOUR_IMG_DIR "development/colourBlindImage/"
        
        // Replaces all pixels in the template image with the specified two lab colours,
        // then returns the new image, (or NULL if pixels are out of range for RGB).
        static unsigned char* replaceColours(const unsigned char * const templateImg, const size_t area, const MultiCol & labCol0, const MultiCol & labCol1) {
            // Convert colours to 32 bit RGBA ints.
            MultiCol rgbCol0 = labCol0;
            MultiCol rgbCol1 = labCol1;
            rgbCol0.labToXyz();
            rgbCol0.xyzToRgb();
            rgbCol1.labToXyz();
            rgbCol1.xyzToRgb();
            
            // Complain about out of range colours.
            if (!rgbCol0.isWithinRangeRGB()) {
                std::cerr << "Col 0 out of range: " << rgbCol0.r() << ", " << rgbCol0.g() << ", " << rgbCol0.b() << ".\n";
                return NULL;
            }
            
            if (!rgbCol1.isWithinRangeRGB()) {
                std::cerr << "Col 1 out of range: " << rgbCol1.r() << ", " << rgbCol1.g() << ", " << rgbCol1.b() << ".\n";
                return NULL;
            }
            
            const uint32_t col0Rgba = rgbCol0.toIntRGBA();
            const uint32_t col1Rgba = rgbCol1.toIntRGBA();
            
            // Allocate output image with same size as input image.
            unsigned char * const output = (unsigned char *)malloc(area * 4);
            
            // Substitute output pixels according to template pixels.
            // If template image pixel is black (red less than 128), use col0, else use col1.
            for (size_t index = 0; index < area; index++) {
                unsigned char * const outPix = output + (index * 4);
                const unsigned char * const templatePix = templateImg + (index * 4);
                const uint32_t chosenCol = (templatePix[0] < 128 ? col0Rgba : col1Rgba);
                
                outPix[0] = (chosenCol >> 24) & 255; // r
                outPix[1] = (chosenCol >> 16) & 255; // g
                outPix[2] = (chosenCol >> 8) & 255; // b
                outPix[3] = (chosenCol >> 0) & 255; // b
            }
            
            return output;
        }
        
        // Macro to make exporting images more convenient
        // (generates a new image, replaces colours, exports it).
        #define EXPORT_IMAGE(labCol0, labCol1, filename) { \
            const size_t area = (size_t)width * height; \
            unsigned char * const replacedImg = replaceColours(templateImg, area, labCol0, labCol1); \
            ASSERT_TEST(replacedImg); \
            std::string filenameStr(COLOUR_IMG_DIR); \
            filenameStr += (filename); \
            ASSERT_TEST(!lodepng_encode32_file(filenameStr.c_str(), replacedImg, width, height)); \
            free(replacedImg); \
        }
        
        // Goes in a circle around the specified centre point, at specified luminance.
        // For each one we generate an image for equidistant (equal luminance) colour pairs.
        // In theory, those with lots of variance on 'a' but little on 'b', should be perceptually very similar for those with red-green colour-blindness.
        // See: https://sid.onlinelibrary.wiley.com/doi/full/10.1002/msid.1145
        // Archive: http://web.archive.org/web/20230903154416/https://sid.onlinelibrary.wiley.com/doi/full/10.1002/msid.1145
        bool runCircle(Random & random, const unsigned char * const templateImg, const unsigned int width, const unsigned int height, const float lum, const float x, const float y, const char * filename) {
            // Number of steps in the circle.
            constexpr unsigned int steps = 4;
            
            // COLOUR_CLOSENESS_THRESHOLD is a squared distance, so square root it to find actual distance.
            // Halve it to find radius.
            const float radius = std::sqrt(rasc::RascBaseGameServer::COLOUR_CLOSENESS_THRESHOLD) * 0.5f;
            
            // Shuffle 4 letters (randomise output names for blind trial!)
            std::vector<char> charactersInitial;
            for (size_t i = 0; i < steps; i++) {
                charactersInitial.push_back('a' + i);
            }
            std::array<char, steps> charactersFinal;
            for (size_t i = 0; i < steps; i++) {
                std::vector<char>::iterator iter = charactersInitial.begin() + random.sizetRange(0, charactersInitial.size());
                charactersFinal[i] = *iter;
                charactersInitial.erase(iter);
            }
            
            std::cout << filename << ":\n";
            
            // For each step:
            // Step 0 should be "most similar" for somebody who is red-green colour blind (a differs most, b is equal).   ( . . )
            // Step n should be "least similar" for somebody who is red-green colour blind (a is equal, b differs most).  (  :  )
            for (unsigned int index = 0; index < steps; index++) {
                // Find angle from centre.
                const float angle = M_PI * 0.5f * ((float)index / (float)(steps - 1u));
                
                // Get colours at fixed distance from centre, each opposing centre
                // rotated by angle.
                const MultiCol col0(lum, x - radius * std::cos(angle), y - radius * std::sin(angle));
                const MultiCol col1(lum, x + radius * std::cos(angle), y + radius * std::sin(angle));
                
                // Print name to tell us which one is which, export image.
                std::cout << "  Step " << index << " = " << charactersFinal[index] << ", (Dist: " << col0.distance(col1) << ")\n";                
                EXPORT_IMAGE(col0, col1, std::string(filename) + '-' + charactersFinal[index]);
            }
            
            std::cout << '\n';
            
            return true;
        }
        
        bool colourBlindImageGenTest(void) {
            
            // Load template image.
            unsigned char * templateImg;
            unsigned int width;
            unsigned int height;
            ASSERT_TEST(!lodepng_decode32_file(&templateImg, &width, &height, COLOUR_IMG_DIR "template.png"))
            
            Random random;
            ASSERT_TEST(runCircle(random, templateImg, width, height, 25.0f, 10.0f, 0.0f, "25-lum"))
            ASSERT_TEST(runCircle(random, templateImg, width, height, 50.0f, 0.0f, 0.0f, "50-lum"))
            ASSERT_TEST(runCircle(random, templateImg, width, height, 75.0f, 0.0f, 0.0f, "75-lum"))
            
            free(templateImg);
            return true;
        }
        
        #undef EXPORT_IMAGE
    }
#endif

#endif

/*
 * NumberFormatTest.cpp
 *
 *  Created on: 30 Jan 2021
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_numberFormat == 1
    #include <baseGame/common/gameMap/provinceUtil/ProvStatMod.h>
    #include <baseGame/common/util/numeric/NumberFormat.h>
    #include <baseGame/common/playerData/PlayerData.h>
    #include <baseGame/common/util/numeric/Random.h>
    #include <iostream>
    #include <cstring>
    #include <string>
    
    namespace rasc {

        /**
         * Prints fixed width padded text with checks.
         */
        bool printFixed(const char * text, unsigned int bufferSize, const char * type) {
            ASSERT_TEST(std::strlen(text) < bufferSize)
            char * buffer = new char[bufferSize + 8];
            std::sprintf(buffer, ":    [%*s]\n", bufferSize - 1, text);
            std::cout << type << buffer;
            delete[] buffer;
            return true;
        }
        
        /**
         * Gets random unsigned number of specified digits, or maximum value if not possible.
         */
        static uint64_t randDigits(Random & random, unsigned int digits) {
            if (digits == 0) return 0;
            uint64_t num = 1;
            for (unsigned int i = 1; i < digits; i++) {
                if (num > std::numeric_limits<uint64_t>::max() / 10) {
                    num = std::numeric_limits<uint64_t>::max();
                    break;
                } else {
                    num *= 10;
                    num += random.uintRange(0, 10);
                }
            }
            return num;
        }
        
        /**
         * Gets random signed number of specified digits (not including decimal point),
         * or largest/smallest if not possible.
         */
        static provstat_t randDigitsSigned(Random & random, unsigned int digits, bool positive) {
            uint64_t num = randDigits(random, digits);
            if (positive) {
                return (provstat_t)std::min(num, (uint64_t)std::numeric_limits<provstat_t>::max());
            } else {
                return -((provstat_t)std::min(num, ((uint64_t)std::numeric_limits<provstat_t>::max()) + 1u));
            }
        }
        
        /**
         * Allows access to buffers of various sizes, to avoid excessive dynamic allocation.
         */
        template<unsigned int count>
        class MultiBuffer {
        private:
            char * buffers[count];
        public:
            MultiBuffer(void) {
                for (unsigned int i = 0; i < count; i++) {
                    buffers[i] = new char[i];
                }
            }
            ~MultiBuffer(void) {
                for (unsigned int i = 0; i < count; i++) {
                    delete[] buffers[i];
                }
            }
            bool getBuffer(char *& buffer, unsigned int id) {
                ASSERT_TEST(id < count)
                buffer = buffers[id];
                return true;
            }
        };
        
        
        
        // ======================================== Basic value test =======================================
        
        #define TEST_FORMAT_VALUE_BUFFERSIZE_TYPE_POS_PREFIXPLUS(type, typeCaps, typeFunc, bufferSize, digits, positive, prefixPlus, print) \
            if (bufferSize >= rasc::NumberFormat::typeCaps##_MIN) {                                                                         \
                /* Build format.                                                                                                         */ \
                char * buffer;                                                                                                              \
                ASSERT_TEST(multiBuff.getBuffer(buffer, bufferSize))                                                                        \
                provstat_t value = randDigitsSigned(random, digits, positive);                                                              \
                unsigned int len = typeFunc(type, buffer, bufferSize, value, prefixPlus);                                                   \
                                                                                                                                            \
                /* Checks.                                                                                                               */ \
                ASSERT_TEST(len == std::strlen(buffer))                                                                                     \
                ASSERT_TEST(len > 0)                                                                                                        \
                ASSERT_TEST(len < bufferSize)                                                                                               \
                if (print) {                                                                                                                \
                    ASSERT_TEST(printFixed(buffer, bufferSize, #type))                                                                      \
                }                                                                                                                           \
            }

        #define TEST_FORMAT_VALUE_BUFFERSIZE_TYPE(type, typeCaps, typeFunc, bufferSize, digits, print)                              \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE_POS_PREFIXPLUS(type, typeCaps, typeFunc, bufferSize, digits, true, false, print)      \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE_POS_PREFIXPLUS(type, typeCaps, typeFunc, bufferSize, digits, true, true, print)       \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE_POS_PREFIXPLUS(type, typeCaps, typeFunc, bufferSize, digits, false, false, print)     \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE_POS_PREFIXPLUS(type, typeCaps, typeFunc, bufferSize, digits, false, true, print)

        #define BASIC_FUNC(name, buffer, bufferSize, value, modifier) \
            rasc::NumberFormat::name(buffer, bufferSize, value, modifier)

        #define UNSIGNED_FUNC(name, buffer, bufferSize, value, modifier) \
            rasc::NumberFormat::name(buffer, bufferSize, *((uint64_t *)&(value)))
        
        #define TEST_FORMAT_VALUE_BUFFERSIZE(bufferSize, digits, print)                                             \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE(generic, GENERIC, UNSIGNED_FUNC, bufferSize, digits, print)           \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE(genericSigned, GENERIC_SIGNED, BASIC_FUNC, bufferSize, digits, print) \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE(currency, CURRENCY, BASIC_FUNC, bufferSize, digits, print)            \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE(manpower, MANPOWER, BASIC_FUNC, bufferSize, digits, print)            \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE(provinceValue, PROVINCE_VALUE, BASIC_FUNC, bufferSize, digits, print) \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE(land, LAND, BASIC_FUNC, bufferSize, digits, print)                    \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE(percentage, PERCENTAGE, BASIC_FUNC, bufferSize, digits, print)        \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE(permille, PERMILLE, BASIC_FUNC, bufferSize, digits, print)            \
            TEST_FORMAT_VALUE_BUFFERSIZE_TYPE(armyUnits, ARMY_UNITS, UNSIGNED_FUNC, bufferSize, digits, print)
        
        #define TEST_FORMAT_VALUE(digits, print)                    \
            for (unsigned int size = 1; size < 24; size++) {        \
                TEST_FORMAT_VALUE_BUFFERSIZE(size, digits, print)   \
            }
        
        template<unsigned int multiBuffSize>
        bool testBasicValues(Random & random, MultiBuffer<multiBuffSize> & multiBuff) {
            for (unsigned int digits = 0; digits < 21; digits++) {
                TEST_FORMAT_VALUE(digits, false)
            }
            return true;
        }
        
        // ======================================== MODIFIER TEST =======================================
        
        #define TEST_FORMAT_MODIFIER_BUFFERSIZE_TYPE_POS_PREFIXPLUS(type, typeCaps, typeFunc, bufferSize, digitsVal, digitsPerc, positive, prefixPlus, print) \
            if (bufferSize >= rasc::NumberFormat::typeCaps##_MIN) {                                                                                           \
                /* Build format.                                                                                                                           */ \
                char * buffer;                                                                                                                                \
                ASSERT_TEST(multiBuff.getBuffer(buffer, bufferSize))                                                                                          \
                provstat_t value = randDigitsSigned(random, digitsVal, positive),                                                                             \
                           percentage = randDigitsSigned(random, digitsPerc, positive);                                                                       \
                unsigned int len = typeFunc(type, buffer, bufferSize, value, percentage, prefixPlus);                                                         \
                                                                                                                                                              \
                /* Checks.                                                                                                                                 */ \
                ASSERT_TEST(len == std::strlen(buffer))                                                                                                       \
                ASSERT_TEST(len > 0)                                                                                                                          \
                ASSERT_TEST(len < bufferSize)                                                                                                                 \
                if (print) {                                                                                                                                  \
                    ASSERT_TEST(printFixed(buffer, bufferSize, #type))                                                                                        \
                }                                                                                                                                             \
            }
            
        #define TEST_FORMAT_MODIFIER_BUFFERSIZE_TYPE(type, typeCaps, typeFunc, bufferSize, digitsVal, digitsPerc, print)                          \
            TEST_FORMAT_MODIFIER_BUFFERSIZE_TYPE_POS_PREFIXPLUS(type, typeCaps, typeFunc, bufferSize, digitsVal, digitsPerc, true, false, print)  \
            TEST_FORMAT_MODIFIER_BUFFERSIZE_TYPE_POS_PREFIXPLUS(type, typeCaps, typeFunc, bufferSize, digitsVal, digitsPerc, true, true, print)   \
            TEST_FORMAT_MODIFIER_BUFFERSIZE_TYPE_POS_PREFIXPLUS(type, typeCaps, typeFunc, bufferSize, digitsVal, digitsPerc, false, false, print) \
            TEST_FORMAT_MODIFIER_BUFFERSIZE_TYPE_POS_PREFIXPLUS(type, typeCaps, typeFunc, bufferSize, digitsVal, digitsPerc, false, true, print)
        
        #define STATMOD_FUNC(name, buffer, bufferSize, number1, number2, prefixPlus) \
            rasc::NumberFormat::name(buffer, bufferSize, ProvStatMod(number1, number2), prefixPlus)

        #define TWOVAL_FUNC(name, buffer, bufferSize, number1, number2, prefixPlus) \
            rasc::NumberFormat::name(buffer, bufferSize, number1, number2)
        
        #define TEST_FORMAT_MODIFIER_BUFFERSIZE(bufferSize, digitsVal, digitsPerc, print)                                                                \
            TEST_FORMAT_MODIFIER_BUFFERSIZE_TYPE(genericSignedModifier, GENERIC_SIGNED_MODIFIER, STATMOD_FUNC, bufferSize, digitsVal, digitsPerc, print) \
            TEST_FORMAT_MODIFIER_BUFFERSIZE_TYPE(currencyModifier, CURRENCY_MODIFIER, STATMOD_FUNC, bufferSize, digitsVal, digitsPerc, print)            \
            TEST_FORMAT_MODIFIER_BUFFERSIZE_TYPE(landUseAvailable, LAND_USE_AVAILABLE, TWOVAL_FUNC, bufferSize, digitsVal, digitsPerc, print)
            
        #define TEST_FORMAT_MODIFIER(digitsVal, digitsPerc, print)                    \
            for (unsigned int size = 10; size < 24; size++) {                         \
                TEST_FORMAT_MODIFIER_BUFFERSIZE(size, digitsVal, digitsPerc, print)   \
            }
        
        template<unsigned int multiBuffSize>
        bool testModifiers(Random & random, MultiBuffer<multiBuffSize> & multiBuff) {
            for (unsigned int digitsVal = 0; digitsVal < 21; digitsVal++) {
                for (unsigned int digitsPerc = 0; digitsPerc < 21; digitsPerc++) {
                    TEST_FORMAT_MODIFIER(digitsVal, digitsPerc, false)
                }
            }
            
            return true;
        }
        
        // ==============================================================================================
        
        bool numberFormatTest(void) {
            // Note: Default (re)construct random generator, so this test is deterministic.
            Random random;
            random.generator = std::mt19937();
            
            MultiBuffer<24> buffer;
            ASSERT_TEST(testBasicValues(random, buffer))
            ASSERT_TEST(testModifiers(random, buffer))
            return true;
        }
    }
#endif

#endif

/*
 * SerialNumberArithTest.cpp
 *
 *  Created on: 28 Dec 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_serialNumberArith == 1
    #include <sockets/plus/util/SerialNumberArith.h>
    #include <iostream>
    #include <cstdint>
    #include <limits>
    
    namespace rasc {
        bool serialNumberArithTest(void) {
            // Exhaustively tests simplenetwork::SerialNumberArith functions, for 8 bit integers.
            
            // Check signed convert is correct for positive numbers.
            for (unsigned int i = 0; i < 128u; i++) {
                uint8_t num = i;
                int8_t cmp = num;
                ASSERT_TEST(simplenetwork::SerialNumberArith::signedConvert(num) == cmp)
            }
            
            // Check signed convert is correct for negative numbers.
            {
                int8_t cmp = -128;
                for (unsigned int i = 128; i < 256u; i++) {
                    uint8_t num = i;
                    ASSERT_TEST(simplenetwork::SerialNumberArith::signedConvert(num) == cmp)
                    cmp++;
                }
            }

            // Check that distance returns the correct value for all possible numbers and offsets.
            for (unsigned int baseInt = 0; baseInt < 256u; baseInt++) {
                uint8_t base = baseInt;

                for (int offsetInt = -128; offsetInt < 128; offsetInt++) {
                    int8_t offset = offsetInt;

                    int offsettedBaseInt = ((int)baseInt + offsetInt + 256 * 10) % 256;
                    uint8_t offsettedBase = offsettedBaseInt;

                    ASSERT_TEST(simplenetwork::SerialNumberArith::distance(offsettedBase, base) == offset)
                }
            }

            // Check that less than returns correct value for all possible pairs of numbers.
            for (unsigned int baseInt = 0; baseInt < 256u; baseInt++) {
                uint8_t base = baseInt;

                for (int offsetInt = -128; offsetInt < 128; offsetInt++) {
                    int8_t offset = offsetInt;

                    int offsettedBaseInt = ((int)baseInt + offsetInt + 256 * 10) % 256;
                    uint8_t offsettedBase = offsettedBaseInt;

                    ASSERT_TEST(simplenetwork::SerialNumberArith::lessThan(offsettedBase, base) == (offset < 0))
                }
            }
            
            // Check that less than or equal to returns correct value for all possible pairs of numbers.
            for (unsigned int baseInt = 0; baseInt < 256u; baseInt++) {
                uint8_t base = baseInt;

                for (int offsetInt = -128; offsetInt < 128; offsetInt++) {
                    int8_t offset = offsetInt;

                    int offsettedBaseInt = ((int)baseInt + offsetInt + 256 * 10) % 256;
                    uint8_t offsettedBase = offsettedBaseInt;

                    ASSERT_TEST(simplenetwork::SerialNumberArith::lessThanOrEqual(offsettedBase, base) == (offset <= 0))
                }
            }
            
            // Check that greater than returns correct value for all possible pairs of numbers.
            for (unsigned int baseInt = 0; baseInt < 256u; baseInt++) {
                uint8_t base = baseInt;

                for (int offsetInt = -128; offsetInt < 128; offsetInt++) {
                    int8_t offset = offsetInt;

                    int offsettedBaseInt = ((int)baseInt + offsetInt + 256 * 10) % 256;
                    uint8_t offsettedBase = offsettedBaseInt;

                    ASSERT_TEST(simplenetwork::SerialNumberArith::greaterThan(offsettedBase, base) == (offset > 0))
                }
            }

            // Check that greater than or equal to returns correct value for all possible pairs of numbers.
            for (unsigned int baseInt = 0; baseInt < 256u; baseInt++) {
                uint8_t base = baseInt;

                for (int offsetInt = -128; offsetInt < 128; offsetInt++) {
                    int8_t offset = offsetInt;

                    int offsettedBaseInt = ((int)baseInt + offsetInt + 256 * 10) % 256;
                    uint8_t offsettedBase = offsettedBaseInt;

                    ASSERT_TEST(simplenetwork::SerialNumberArith::greaterThanOrEqual(offsettedBase, base) == (offset >= 0))
                }
            }
            
            return true;
        }
    }
#endif

#endif

/*
 * MessageFileTest.cpp
 *
 *  Created on: 5 May 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_messageFile == 1
    #include <sockets/simplenetwork/simplenetwork.h> // INCLUDE THIS FIRST.
    #include <sockets/plus/message/Message.h>
    #include <iostream>
    #include <cstring>
    #include <string>

    #define MESSAGE_TEST_FILE "/tmp/rascMessageTestFile"
    #define MESSAGE_TEST_STRING "I've got this to do"
    #define MESSAGE_TEST_ID 5

    namespace rasc {
        bool messageFileTest(void) {

            std::cout << "Using the C way to do things:\n";
            {
                struct simplenetwork_Message msg = simplenetwork_newEmptyMessageDefault();
                msg.id = MESSAGE_TEST_ID;
                simplenetwork_writeString(&msg, MESSAGE_TEST_STRING);
                if (simplenetwork_save(&msg, MESSAGE_TEST_FILE)) {
                    std::cout << "Save success (C).\n";
                } else {
                    std::cout << "Failed to save to file (C).\n";
                    return false;
                }
                free(msg.data);
                msg = simplenetwork_load(MESSAGE_TEST_FILE);
                if (simplenetwork_isValid(&msg)) {
                    std::cout << "Load success (C).\nLen: " << msg.length << ", id: " << msg.id << "\nData: ";
                    char * str = simplenetwork_readString(&msg);
                    std::cout << str << "\n";
                    if (strcmp(str, MESSAGE_TEST_STRING) != 0 || msg.id != MESSAGE_TEST_ID) {
                        std::cout << "Read wrong data or ID from file (C).\n";
                        return false;
                    }
                    free(str);
                    free(msg.data);
                } else {
                    std::cout << "Failed to read from file (C).\n";
                    return false;
                }
            }
            std::cout << "Using the C++ way to do things:\n";
            {
                simplenetwork::Message msg(MESSAGE_TEST_ID);
                msg.writeString(MESSAGE_TEST_STRING);
                if (msg.save(MESSAGE_TEST_FILE)) {
                    std::cout << "Save success (C++).\n";
                } else {
                    std::cout << "Failed to save to file (C++).\n";
                    return false;
                }
                if (msg.load(MESSAGE_TEST_FILE)) {
                    std::cout << "Load success (C++).\nLen: " << msg.getLength() << ", id: " << msg.getId() << "\nData: ";
                    std::string str = msg.readString();
                    std::cout << str << "\n";
                    if (str != MESSAGE_TEST_STRING || msg.getId() != MESSAGE_TEST_ID) {
                        std::cout << "Read wrong data or ID from file (C++).\n";
                        return false;
                    }
                } else {
                    std::cout << "Failed to read from file (C++).\n";
                    return false;
                }
            }
            return true;
        }
    }
#endif

#endif

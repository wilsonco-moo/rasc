/*
 * CheckedArithmeticTest.cpp
 *
 *  Created on: 11 May 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_checkedArithmetic == 1
    #include <baseGame/common/util/numeric/ChArith.h>
    #include <iostream>
    #include <cstdint>
    #include <climits>
    #include <limits>

    // The type which we will exhaustively perform arithmetic operations on.
    typedef int8_t test_t;
    // The type used to verify the results of test_t - this must be bigger.
    typedef int32_t verify_t;
    // The type used to print the results.
    typedef int32_t print_t;

    // Whether to print progress while running.
    #define SHOULD_PRINT_PROGRESS false
    // The interval of outer iterations for printing progress.
    #define PROGRESS_INTERVAL 100

    /**
     * Exhaustively tries the operator on all combinations of two numbers
     * of type test_t. Each for each one, the result given by the ChArith
     * function is checked against a result from verify_t.
     * The parameter testZeroForSecondNumber is required as true for division,
     * since dividing by zero is a special case which must be avoided.
     */
    #define RUN_ARITH_TEST(testOperator, testFunction, testZeroForSecondNumber)                                             \
                                                                                                                            \
        std::cout << "Running arithmetic test for " #testFunction "...\n";                                                  \
                                                                                                                            \
        for (verify_t num1Verify = std::numeric_limits<test_t>::min();                                                      \
             num1Verify <= std::numeric_limits<test_t>::max(); num1Verify++) {                                              \
                                                                                                                            \
            /* Print progress if required.                                                                               */ \
            if (SHOULD_PRINT_PROGRESS && num1Verify % PROGRESS_INTERVAL == 0) {                                             \
                std::cout << "    Running " << num1Verify << "...\n";                                                       \
            }                                                                                                               \
                                                                                                                            \
            for (verify_t num2Verify = std::numeric_limits<test_t>::min();                                                  \
                 num2Verify <= std::numeric_limits<test_t>::max(); num2Verify++) {                                          \
                                                                                                                            \
                /* Work out result using function and test type.                                                         */ \
                test_t num1 = (test_t)num1Verify,                                                                           \
                       num2 = (test_t)num2Verify;                                                                           \
                bool error = false;                                                                                         \
                test_t result = ChArith::testFunction(&error, num1, num2);                                                  \
                                                                                                                            \
                /* Handle cases where the second number being zero is wrong (like division).                             */ \
                if (testZeroForSecondNumber && num2 == 0) {                                                                 \
                    if (!error || result != 0) {                                                                            \
                        std::cout << "WRONG: [" << (print_t)num1 << " " #testOperator " " <<                                \
                                     (print_t)num2 << "] should report an error and return zero.\n";                        \
                    }                                                                                                       \
                } else {                                                                                                    \
                    verify_t verifyResult = num1Verify testOperator num2Verify;                                             \
                    /* If when checked using verify_t, the result is out of range                                        */ \
                    /* of test_t, an error should have been reported.                                                    */ \
                    if (verifyResult < std::numeric_limits<test_t>::min() ||                                                \
                        verifyResult > std::numeric_limits<test_t>::max()) {                                                \
                        if (!error || result != 0) {                                                                        \
                            std::cout << "WRONG: [" << (print_t)num1 << " " #testOperator " " <<                            \
                                         (print_t)num2 << "] should report an error and return zero.\n";                    \
                        }                                                                                                   \
                                                                                                                            \
                    /* If when checked using verify_t, the result is NOT out of range                                    */ \
                    /* of test_t, no error should have been reported.                                                    */ \
                    } else {                                                                                                \
                        if (error || (verify_t)result != verifyResult) {                                                    \
                            std::cout << "WRONG: [" << (print_t)num1 << " " #testOperator " " <<                            \
                                         (print_t)num2 << "] should not report an error, and should return " <<             \
                                         verifyResult << ".\n";                                                             \
                        }                                                                                                   \
                    }                                                                                                       \
                }                                                                                                           \
            }                                                                                                               \
        }

    /**
     * Performs the specified operation using ints, and checks the result.
     */
    #define RUN_SIMPLE_INT_CHECK(testOperator, testFunction, num1, num2, expectedResult, expectedError) { \
        /* Store as int variables first, so the compiler gives appropriate overflow warnings.          */ \
        int num1Int = (num1),                                                                             \
            num2Int = (num2),                                                                             \
            expectedResultInt = (expectedResult);                                                         \
        /* Perform the calculation and check it.                                                       */ \
        bool error = false;                                                                               \
        int result = ChArith::testFunction(&error, num1Int, num2Int);                                     \
        if (error != (expectedError) || result != expectedResultInt) {                                    \
            std::cout << "WRONG: [" << (print_t)num1Int << " " #testOperator " " << (print_t)num2Int <<   \
                         "] should be " << (print_t)expectedResultInt <<                                  \
                         ((expectedError) ? " with error.\n" : " without error.\n");                      \
            return false;                                                                                 \
        }                                                                                                 \
    }

    /**
     * Big can be any int number which has the following properties:
     *  > BIG * SMALL, and all negative combinations thereof, must
     *    be representable as an int.
     *  > BIG * BIG, and all negative combinations thereof, must be
     *    too big to be representable as an int.
     *  > BIG and -BIG must be representable as an integer.
     */
    #define BIG   ((INT_MAX / 10) - 1)
    #define SMALL 10

    namespace rasc {
        bool checkedArithmeticTest(void) {

            // Run exhaustive arithmetic tests on all operators.

            RUN_ARITH_TEST(+, add, false)
            RUN_ARITH_TEST(-, subtract, false)
            RUN_ARITH_TEST(*, multiply, false)
            RUN_ARITH_TEST(/, divide, true)

            // Most of the following checks are calculations which, if done normally,
            // would cause integer overflow: they are all the edge cases I could think of.
            // It is important that the following runs properly, even with -ftrapv enabled
            // --> making sure checks are done without *actually* performing the out of
            // range arithmetic in question.

            RUN_SIMPLE_INT_CHECK(+, add, 1,       1,       2,  false) // 1   + 1   = 2
            RUN_SIMPLE_INT_CHECK(+, add, 50,      1,       51, false) // 50  + 1   = 51
            RUN_SIMPLE_INT_CHECK(+, add, -1,      -1,      -2, false) // -1  + -1  = -2
            RUN_SIMPLE_INT_CHECK(+, add, INT_MAX, 1,       0,  true)  // max + 1   = error
            RUN_SIMPLE_INT_CHECK(+, add, 1,       INT_MAX, 0,  true)  // 1   + max = error
            RUN_SIMPLE_INT_CHECK(+, add, INT_MIN, -1,      0,  true)  // min + -1  = error
            RUN_SIMPLE_INT_CHECK(+, add, -1,      INT_MIN, 0,  true)  // -1  + min = error
            RUN_SIMPLE_INT_CHECK(+, add, INT_MAX, INT_MAX, 0,  true)  // max + max = error
            RUN_SIMPLE_INT_CHECK(+, add, INT_MIN, INT_MIN, 0,  true)  // min + min = error

            RUN_SIMPLE_INT_CHECK(-, subtract, 1,       1,       0,        false) // 1   - 1   = 0
            RUN_SIMPLE_INT_CHECK(-, subtract, 50,      1,       49,       false) // 50  - 1   = 49
            RUN_SIMPLE_INT_CHECK(-, subtract, -1,      -1,      0,        false) // -1  - -1  = 0
            RUN_SIMPLE_INT_CHECK(-, subtract, INT_MAX, -1,      0,        true)  // max - -1  = error
            RUN_SIMPLE_INT_CHECK(-, subtract, 0,       INT_MAX, -INT_MAX, false) // 0   - max = -max
            RUN_SIMPLE_INT_CHECK(-, subtract, -1,      INT_MAX, INT_MIN,  false) // -1  - max = min
            RUN_SIMPLE_INT_CHECK(-, subtract, -2,      INT_MAX, 0,        true)  // -2  - max = error
            RUN_SIMPLE_INT_CHECK(-, subtract, INT_MIN, 1,       0,        true)  // min - 1   = error
            RUN_SIMPLE_INT_CHECK(-, subtract, 0,       INT_MIN, 0,        true)  // 0   - min = error
            RUN_SIMPLE_INT_CHECK(-, subtract, -1,      INT_MIN, INT_MAX,  false) // -1  - min = max

            RUN_SIMPLE_INT_CHECK(*, multiply, 1,      1,      1,               false) // 1   * 1   = 1
            RUN_SIMPLE_INT_CHECK(*, multiply, SMALL,  SMALL,  SMALL  * SMALL,  false) // small  * small  = small  * small
            RUN_SIMPLE_INT_CHECK(*, multiply, SMALL,  -SMALL, SMALL  * -SMALL, false) // small  * -small = small  * -small
            RUN_SIMPLE_INT_CHECK(*, multiply, -SMALL, SMALL,  -SMALL * SMALL,  false) // -small * small  = -small * small
            RUN_SIMPLE_INT_CHECK(*, multiply, -SMALL, -SMALL, -SMALL * -SMALL, false) // -small * -small = -small * -small
            RUN_SIMPLE_INT_CHECK(*, multiply, SMALL,  BIG,    SMALL  * BIG,    false) // small  * big    = small  * big
            RUN_SIMPLE_INT_CHECK(*, multiply, SMALL,  -BIG,   SMALL  * -BIG,   false) // small  * -big   = small  * -big
            RUN_SIMPLE_INT_CHECK(*, multiply, -SMALL, BIG,    -SMALL * BIG,    false) // -small * big    = -small * big
            RUN_SIMPLE_INT_CHECK(*, multiply, -SMALL, -BIG,   -SMALL * -BIG,   false) // -small * -big   = -small * -big
            RUN_SIMPLE_INT_CHECK(*, multiply, BIG,    SMALL,  BIG    * SMALL,  false) // big    * small  = small  * small
            RUN_SIMPLE_INT_CHECK(*, multiply, BIG,    -SMALL, BIG    * -SMALL, false) // big    * -small = small  * -small
            RUN_SIMPLE_INT_CHECK(*, multiply, -BIG,   SMALL,  -BIG   * SMALL,  false) // -big   * small  = -small * small
            RUN_SIMPLE_INT_CHECK(*, multiply, -BIG,   -SMALL, -BIG   * -SMALL, false) // -big   * -small = -small * -small
            RUN_SIMPLE_INT_CHECK(*, multiply, BIG,    BIG,    0,               true)  // big    * big    = error
            RUN_SIMPLE_INT_CHECK(*, multiply, BIG,    -BIG,   0,               true)  // big    * -big   = error
            RUN_SIMPLE_INT_CHECK(*, multiply, -BIG,   BIG,    0,               true)  // -big   * big    = error
            RUN_SIMPLE_INT_CHECK(*, multiply, -BIG,   -BIG,   0,               true)  // -big   * -big   = error

            RUN_SIMPLE_INT_CHECK(/, divide, 10,      2,  5,        false) // 10  / 2  = 5
            RUN_SIMPLE_INT_CHECK(/, divide, -10,     2,  -5,       false) // -10 / 2  = -5
            RUN_SIMPLE_INT_CHECK(/, divide, 10,      0,  0,        true)  // 10  / 0  = error
            RUN_SIMPLE_INT_CHECK(/, divide, -10,     0,  0,        true)  // -10 / 0  = error
            RUN_SIMPLE_INT_CHECK(/, divide, INT_MAX, -1, -INT_MAX, false) // max / -1 = -max
            RUN_SIMPLE_INT_CHECK(/, divide, INT_MIN, -1, 0,        true)  // min / -1 = error

            return true;
        }
    }
#endif

#endif

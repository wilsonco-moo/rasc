/*
 * OffValTest.cpp
 *
 *  Created on: 24 May 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_offVal == 1
    #include <rascUI/util/OffVal.h>
    #include <iostream>

    namespace rasc {
        bool offValTest(void) {

            using Offv = rascUI::OffVal<int, float>;

            // Test operator ==
            ASSERT_TEST(Offv(3, 0.2f) == Offv(3, 0.2f))
            ASSERT_TEST(!(Offv(3, 0.2f) == Offv(3, 0.4f)))
            ASSERT_TEST(!(Offv(3, 0.2f) == Offv(4, 0.2f)))
            ASSERT_TEST(Offv(3, 0.0f) == 3)
            ASSERT_TEST(!(Offv(3, 0.5f) == 3))
            ASSERT_TEST(!(Offv(4, 0.0f) == 3))

            // Test operator !=
            ASSERT_TEST(Offv(3, 0.2f) != Offv(3, 0.4f))
            ASSERT_TEST(Offv(3, 0.2f) != Offv(4, 0.2f))
            ASSERT_TEST(!(Offv(3, 0.2f) != Offv(3, 0.2f)))
            ASSERT_TEST(Offv(3, 0.5f) != 3)
            ASSERT_TEST(Offv(4, 0.5f) != 3)
            ASSERT_TEST(!(Offv(3, 0.0f) != 3))

            // Test alternate constructor
            ASSERT_TEST(Offv(2.0f) == Offv(2, 0.0f))
            ASSERT_TEST(Offv(2.25f) == Offv(2, 0.25f))
            ASSERT_TEST(Offv(2.75f) == Offv(2, 0.75f))
            ASSERT_TEST(Offv(0.25f) == Offv(0, 0.25f))
            ASSERT_TEST(Offv(0.75f) == Offv(0, 0.75f))
            ASSERT_TEST(Offv(-0.75f) == Offv(-1, 0.25f))
            ASSERT_TEST(Offv(-0.25f) == Offv(-1, 0.75f))
            ASSERT_TEST(Offv(-3.75f) == Offv(-4, 0.25f))
            ASSERT_TEST(Offv(-3.25f) == Offv(-4, 0.75f))

            // Test operator <
            ASSERT_TEST(Offv(2, 0.4f) < Offv(3, 0.2f))
            ASSERT_TEST(Offv(2, 0.4f) < Offv(2, 0.6f))
            ASSERT_TEST(!(Offv(2, 0.6f) < Offv(2, 0.4f)))
            ASSERT_TEST(!(Offv(3, 0.4f) < Offv(2, 0.6f)))
            ASSERT_TEST(!(Offv(3, 0.2f) < Offv(3, 0.2f)))
            ASSERT_TEST(Offv(3, 0.5f) < 4)
            ASSERT_TEST(!(Offv(3, 0.5f) < 3))
            ASSERT_TEST(!(Offv(4, 0.0f) < 3))
            ASSERT_TEST(!(Offv(3, 0.0f) < 3))

            // Test operator <=
            ASSERT_TEST(Offv(2, 0.4f) <= Offv(3, 0.2f))
            ASSERT_TEST(Offv(2, 0.4f) <= Offv(2, 0.6f))
            ASSERT_TEST(!(Offv(2, 0.6f) <= Offv(2, 0.4f)))
            ASSERT_TEST(!(Offv(3, 0.4f) <= Offv(2, 0.6f)))
            ASSERT_TEST(Offv(3, 0.2f) <= Offv(3, 0.2f))
            ASSERT_TEST(Offv(3, 0.5f) <= 4)
            ASSERT_TEST(!(Offv(3, 0.5f) <= 3))
            ASSERT_TEST(!(Offv(4, 0.0f) <= 3))
            ASSERT_TEST(Offv(3, 0.0f) <= 3)

            // Test operator >
            ASSERT_TEST(Offv(3, 0.2f) > Offv(2, 0.4f))
            ASSERT_TEST(Offv(2, 0.6f) > Offv(2, 0.4f))
            ASSERT_TEST(!(Offv(2, 0.4f) > Offv(2, 0.6f)))
            ASSERT_TEST(!(Offv(2, 0.6f) > Offv(3, 0.4f)))
            ASSERT_TEST(!(Offv(3, 0.2f) > Offv(3, 0.2f)))
            ASSERT_TEST(Offv(5, 0.0f) > 4)
            ASSERT_TEST(Offv(4, 0.5f) > 4)
            ASSERT_TEST(!(Offv(2, 0.5f) > 3))
            ASSERT_TEST(!(Offv(3, 0.0f) > 3))

            // Test operator >=
            ASSERT_TEST(Offv(3, 0.2f) >= Offv(2, 0.4f))
            ASSERT_TEST(Offv(2, 0.6f) >= Offv(2, 0.4f))
            ASSERT_TEST(!(Offv(2, 0.4f) >= Offv(2, 0.6f)))
            ASSERT_TEST(!(Offv(2, 0.6f) >= Offv(3, 0.4f)))
            ASSERT_TEST(Offv(3, 0.2f) >= Offv(3, 0.2f))
            ASSERT_TEST(Offv(5, 0.0f) >= 4)
            ASSERT_TEST(Offv(4, 0.5f) >= 4)
            ASSERT_TEST(!(Offv(2, 0.5f) >= 3))
            ASSERT_TEST(Offv(3, 0.0f) >= 3)

            // Test operator +
            ASSERT_TEST(Offv(2, 0.0f) + Offv(3, 0.0f) == Offv(5, 0.0f))
            ASSERT_TEST(Offv(2, 0.25f) + Offv(3, 0.5f) == Offv(5, 0.75f))
            ASSERT_TEST(Offv(2, 0.5f) + Offv(3, 0.5f) == Offv(6, 0.0f))
            ASSERT_TEST(Offv(2, 0.5f) + Offv(3, 0.75f) == Offv(6, 0.25f))
            ASSERT_TEST(Offv(2, 0.0f) + Offv(-3, 0.0f) == Offv(-1, 0.0f))
            ASSERT_TEST(Offv(2, 0.25f) + Offv(-3, 0.5f) == Offv(-1, 0.75f))
            ASSERT_TEST(Offv(2, 0.5f) + Offv(-3, 0.5f) == Offv(0, 0.0f))
            ASSERT_TEST(Offv(2, 0.5f) + Offv(-3, 0.75f) == Offv(0, 0.25f))

            // Test operator +=
            ASSERT_TEST((Offv(2, 0.0f) += Offv(3, 0.0f)) == Offv(5, 0.0f))
            ASSERT_TEST((Offv(2, 0.25f) += Offv(3, 0.5f)) == Offv(5, 0.75f))
            ASSERT_TEST((Offv(2, 0.5f) += Offv(3, 0.5f)) == Offv(6, 0.0f))
            ASSERT_TEST((Offv(2, 0.5f) += Offv(3, 0.75f)) == Offv(6, 0.25f))
            ASSERT_TEST((Offv(2, 0.0f) += Offv(-3, 0.0f)) == Offv(-1, 0.0f))
            ASSERT_TEST((Offv(2, 0.25f) += Offv(-3, 0.5f)) == Offv(-1, 0.75f))
            ASSERT_TEST((Offv(2, 0.5f) += Offv(-3, 0.5f)) == Offv(0, 0.0f))
            ASSERT_TEST((Offv(2, 0.5f) += Offv(-3, 0.75f)) == Offv(0, 0.25f))

            // Test operator -
            ASSERT_TEST(Offv(5, 0.0f) - Offv(3, 0.0f) == Offv(2, 0.0f))
            ASSERT_TEST(Offv(5, 0.75f) - Offv(3, 0.5f) == Offv(2, 0.25f))
            ASSERT_TEST(Offv(5, 0.0f) - Offv(3, 0.25f) == Offv(1, 0.75f))
            ASSERT_TEST(Offv(5, 0.0f) - Offv(6, 0.875f) == Offv(-2, 0.125f))
            ASSERT_TEST(Offv(5, 0.25f) - Offv(3, 0.25f) == Offv(2, 0.0f))
            ASSERT_TEST(Offv(5, 0.375f) - Offv(3, 0.75f) == Offv(1, 0.625f))

            // Test operator -=
            ASSERT_TEST((Offv(5, 0.0f) -= Offv(3, 0.0f)) == Offv(2, 0.0f))
            ASSERT_TEST((Offv(5, 0.75f) -= Offv(3, 0.5f)) == Offv(2, 0.25f))
            ASSERT_TEST((Offv(5, 0.0f) -= Offv(3, 0.25f)) == Offv(1, 0.75f))
            ASSERT_TEST((Offv(5, 0.0f) -= Offv(6, 0.875f)) == Offv(-2, 0.125f))
            ASSERT_TEST((Offv(5, 0.25f) -= Offv(3, 0.25f)) == Offv(2, 0.0f))
            ASSERT_TEST((Offv(5, 0.375f) -= Offv(3, 0.75f)) == Offv(1, 0.625f))

            // Test zeroRound method.
            { Offv ofv(0, 0.0f); ofv.zeroRound(); ASSERT_TEST(ofv == Offv(0, 0.0f)) }
            { Offv ofv(0, 0.25f); ofv.zeroRound(); ASSERT_TEST(ofv == Offv(0, 0.25f)) }
            { Offv ofv(1, 0.75f); ofv.zeroRound(); ASSERT_TEST(ofv == Offv(1, 0.75f)) }
            { Offv ofv(-1, 0.0f); ofv.zeroRound(); ASSERT_TEST(ofv == Offv(0, 0.0f)) }
            { Offv ofv(-2, 0.25f); ofv.zeroRound(); ASSERT_TEST(ofv == Offv(0, 0.0f)) }
            { Offv ofv(-1, 0.999f); ofv.zeroRound(); ASSERT_TEST(ofv == Offv(0, 0.0f)) }

            // Test zeroRoundSubtract method.
            { Offv ofv(0, 0.0f); ofv.zeroRoundSubtract(Offv(0, 0.0f)); ASSERT_TEST(ofv == Offv(0, 0.0f)) }
            { Offv ofv(0, 0.0f); ofv.zeroRoundSubtract(Offv(0, 0.25f)); ASSERT_TEST(ofv == Offv(0, 0.0f)) }
            { Offv ofv(0, 0.0f); ofv.zeroRoundSubtract(Offv(1, 0.75f)); ASSERT_TEST(ofv == Offv(0, 0.0f)) }
            { Offv ofv(1, 0.25f); ofv.zeroRoundSubtract(Offv(1, 0.25f)); ASSERT_TEST(ofv == Offv(0, 0.0f)) }
            { Offv ofv(2, 0.0f); ofv.zeroRoundSubtract(Offv(2, 0.0f)); ASSERT_TEST(ofv == Offv(0, 0.0f)) }
            { Offv ofv(3, 0.25f); ofv.zeroRoundSubtract(Offv(2, 0.75f)); ASSERT_TEST(ofv == Offv(0, 0.5f)) }
            { Offv ofv(3, 0.5f); ofv.zeroRoundSubtract(Offv(1, 0.75f)); ASSERT_TEST(ofv == Offv(1, 0.75f)) }

            // Test the ceilBase method.
            ASSERT_TEST(Offv(-2, 0.0f).ceilBase() == -2)
            ASSERT_TEST(Offv(3, 0.0f).ceilBase() == 3)
            ASSERT_TEST(Offv(-2, 0.125f).ceilBase() == -1)
            ASSERT_TEST(Offv(3, 0.75f).ceilBase() == 4)

            // Test the toOffType method.
            ASSERT_TEST(Offv(-2, 0.0f).toOffType() == -2.0f)
            ASSERT_TEST(Offv(3, 0.0f).toOffType() == 3.0f)
            ASSERT_TEST(Offv(-2, 0.125f).toOffType() == -1.875f)
            ASSERT_TEST(Offv(3, 0.75f).toOffType() == 3.75f)

            // Test the toSingleValue method.
            ASSERT_TEST(Offv(-2, 0.0f).toSingleValue<double>() == (double)-2.0f)
            ASSERT_TEST(Offv(3, 0.0f).toSingleValue<double>() == (double)3.0f)
            ASSERT_TEST(Offv(-2, 0.125f).toSingleValue<double>() == (double)-1.875f)
            ASSERT_TEST(Offv(3, 0.75f).toSingleValue<double>() == (double)3.75f)

            // Test fromSingleValue method.
            ASSERT_TEST(Offv::fromSingleValue<long double>(2.0f) == Offv(2, 0.0f))
            ASSERT_TEST(Offv::fromSingleValue<long double>(2.25f) == Offv(2, 0.25f))
            ASSERT_TEST(Offv::fromSingleValue<long double>(2.75f) == Offv(2, 0.75f))
            ASSERT_TEST(Offv::fromSingleValue<long double>(0.25f) == Offv(0, 0.25f))
            ASSERT_TEST(Offv::fromSingleValue<long double>(0.75f) == Offv(0, 0.75f))
            ASSERT_TEST(Offv::fromSingleValue<long double>(-0.75f) == Offv(-1, 0.25f))
            ASSERT_TEST(Offv::fromSingleValue<long double>(-0.25f) == Offv(-1, 0.75f))
            ASSERT_TEST(Offv::fromSingleValue<long double>(-3.75f) == Offv(-4, 0.25f))
            ASSERT_TEST(Offv::fromSingleValue<long double>(-3.25f) == Offv(-4, 0.75f))

            // Test the multiplyByFloat method.
            { Offv ofv(4, 0.0f); ofv.multiplyByFloat<double>(1.0f); ASSERT_TEST(ofv == Offv(4, 0.0f)) }
            { Offv ofv(16, 0.5f); ofv.multiplyByFloat<double>(0.5f); ASSERT_TEST(ofv == Offv(8, 0.25f)) }
            { Offv ofv(3, 0.75f); ofv.multiplyByFloat<double>(2.0f); ASSERT_TEST(ofv == Offv(7, 0.5f)) }
            { Offv ofv(3, 0.75f); ofv.multiplyByFloat<double>(15869.0f); ASSERT_TEST(ofv == Offv(59508, 0.75f)) }
            { Offv ofv(3, 0.75f); ofv.multiplyByFloat<double>(0.125f); ASSERT_TEST(ofv == Offv(0, 0.46875f)) }
            { Offv ofv(-5, 0.75f); ofv.multiplyByFloat<double>(0.5f); ASSERT_TEST(ofv == Offv(-3, 0.875f)) }
            { Offv ofv(-5, 0.75f); ofv.multiplyByFloat<double>(-0.5f); ASSERT_TEST(ofv == Offv(2, 0.125f)) }
            { Offv ofv(4, 0.25f); ofv.multiplyByFloat<double>(0.5f); ASSERT_TEST(ofv == Offv(2, 0.125f)) }
            { Offv ofv(4, 0.25f); ofv.multiplyByFloat<double>(-0.5f); ASSERT_TEST(ofv == Offv(-3, 0.875f)) }
            { Offv ofv(101, 0.0f); ofv.multiplyByFloat<double>(0.75f); ASSERT_TEST(ofv == Offv(75, 0.75f)) }

            return true;
        }
    }
#endif

#endif

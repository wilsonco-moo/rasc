/*
 * TerrainDisplayTest.cpp
 *
 *  Created on: 23 Jun 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_terrainDisplay == 1
    #include <baseGame/common/util/definitions/TerrainDisplayDef.h>
    #include <baseGame/client/config/TerrainDisplayManager.h>
    #include <wool/texture/lodepng/lodepng.h>
    #include <threading/ThreadQueue.h>
    #include <iostream>
    #include <cstddef>
    #include <utility>
    #include <vector>

    namespace rasc {
        bool terrainDisplayTest(void) {

            // Create a terrain display manager, wait for it to load, assert that it loaded successfully.
            threading::ThreadQueue threadQueue;
            TerrainDisplayManager manager(&threadQueue);
            threadQueue.waitForJobToComplete(threadQueue.addBarrier());
            ASSERT_TEST(manager.hasLoadedSuccessfully())

            // Layers to generate a texture from.
            std::vector<TerDispLayer> layers = {
                {0, TerDispTex::mountainousBack},
                {1, TerDispTex::hillyBack},
                {2, TerDispTex::plains},
                {3, TerDispTex::mountainousFront},
                {4, TerDispTex::hillyFront},
                {5, TerDispTex::boggy},
                {6, TerDispTex::forested}
            };

            // Generate a texture.
            unsigned char * image = (unsigned char *)malloc(manager.getTextureSizeBytes());
            ASSERT_TEST(image != NULL)
            manager.buildTerrainTexture(std::move(layers), image);

            // Save the generated texture (assert that it succeeded).
            ASSERT_TEST(!lodepng_encode32_file("terrainDisplayTestOutput.png",
                                        image, manager.getWidth(), manager.getHeight()));
            free(image);
            return true;
        }
    }
#endif

#endif


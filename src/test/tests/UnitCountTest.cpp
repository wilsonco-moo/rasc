/*
 * UnitCountTest.cpp
 *
 *  Created on: 5 May 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_unitCount == 1
    #include <baseGame/common/util/numeric/KShortenedNumber.h>
    #include <baseGame/common/util/numeric/Random.h>
    #include <iostream>
    #include <cstring>
    #include <string>
    #include <limits>

    namespace rasc {
        /**
         * Gets random unsigned number of specified digits, or maximum value if not possible.
         */
        static uint64_t randDigits(Random & random, unsigned int digits) {
            uint64_t num = 1;
            for (unsigned int i = 1; i < digits; i++) {
                if (num > std::numeric_limits<uint64_t>::max() / 10) {
                    num = std::numeric_limits<uint64_t>::max();
                    break;
                } else {
                    num *= 10;
                    num += random.uintRange(0, 10);
                }
            }
            return num;
        }
        
        /**
         * Gets random signed number of specified digits (not including decimal point),
         * or largest/smallest if not possible.
         */
        static int64_t randDigitsSigned(Random & random, unsigned int digits, bool positive) {
            uint64_t num = randDigits(random, digits);
            if (positive) {
                return (int64_t)std::min(num, (uint64_t)std::numeric_limits<int64_t>::max());
            } else {
                return -((int64_t)std::min(num, ((uint64_t)std::numeric_limits<int64_t>::max()) + 1u));
            }
        }
        
        // Number of digits to check.
        #define COUNT 22
        
        #define RUN_KSHORTENED_TEST(charCount, type, func, randExpr) {                                  \
                                                                                                        \
            /* Make this dynamically allocated so valgrind spots reads/writes to outside of it,      */ \
            /* rather than just screwing up the stack.                                               */ \
            char * str = new char[charCount];                                                           \
                                                                                                        \
            for (int i = 1; i < COUNT; i++) {                                                           \
                                                                                                        \
                /* Generate k shortened number.                                                      */ \
                type num = randExpr;                                                                    \
                unsigned int len = func(str, num);                                                      \
                                                                                                        \
                /* Check lengths.                                                                    */ \
                ASSERT_TEST(strlen(str) == len)                                                         \
                ASSERT_TEST(len >= 1)                                                                   \
                ASSERT_TEST(len < (charCount))                                                          \
                                                                                                        \
                /* Print.                                                                            */ \
                for (int e = 0; e < COUNT - i; e++) {                                                   \
                    std::cout << ' ';                                                                   \
                }                                                                                       \
                std::cout << num << " -> " << str << "\n";                                              \
            }                                                                                           \
            std::cout << "\n";                                                                          \
            delete[] str;                                                                               \
        }
        
        bool unitCountTest(void) {
            // Note: Default (re)construct random generator, so this test is deterministic.
            Random random;
            random.generator = std::mt19937();
            
            RUN_KSHORTENED_TEST(rasc::KShortenedNumber::SIZE, uint64_t, rasc::KShortenedNumber::format, randDigits(random, i))
            RUN_KSHORTENED_TEST(rasc::KShortenedNumber::SIZE_SIGNED, int64_t, rasc::KShortenedNumber::formatSigned, randDigitsSigned(random, i, true))
            RUN_KSHORTENED_TEST(rasc::KShortenedNumber::SIZE_SIGNED, int64_t, rasc::KShortenedNumber::formatSigned, randDigitsSigned(random, i, false))
            RUN_KSHORTENED_TEST(rasc::KMinimisedNumber::SIZE, uint64_t, rasc::KMinimisedNumber::format, randDigits(random, i))
            RUN_KSHORTENED_TEST(rasc::KMinimisedNumber::SIZE_SIGNED, int64_t, rasc::KMinimisedNumber::formatSigned, randDigitsSigned(random, i, true))
            RUN_KSHORTENED_TEST(rasc::KMinimisedNumber::SIZE_SIGNED, int64_t, rasc::KMinimisedNumber::formatSigned, randDigitsSigned(random, i, false))
            return true;
        }
    }
#endif

#endif

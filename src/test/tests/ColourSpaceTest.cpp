/*
 * ColourSpaceTest.cpp
 *
 *  Created on: 5 May 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_colourSpace == 1
    #include <baseGame/common/util/numeric/MultiCol.h>
    #include <baseGame/common/util/numeric/Random.h>
    #include <iostream>
    #include <cmath>
    #include <array>

    namespace rasc {
        bool colourSpaceTest(void) {

            // Note: Default (re)construct random generator, so this test is deterministic.
            Random random;
            random.generator = std::mt19937();
            
            std::cout << "CIELAB\n";
            #define COUNT 5000lu
            #define DEPTH 257u
            #define INTERVAL 100lu

            std::array<unsigned long long, DEPTH> multiCount;
            multiCount.fill(0);

            for (unsigned long long i = 0; i < COUNT; i++) {

                uint32_t col1 = rasc::MultiCol::perceptUniformRandomColInt(random),
                         col2 = rasc::MultiCol::perceptUniformRandomColInt(random);

                //uint32_t col1 = rasc::MultiCol::randomColRGBInt(),
                //         col2 = rasc::MultiCol::randomColRGBInt();

                rasc::MultiCol mcol1(col1), mcol2(col2);

                mcol1.rgbToXyz(); mcol1.xyzToLab();
                mcol2.rgbToXyz(); mcol2.xyzToLab();

                unsigned int multiDiff = (unsigned int)sqrt(mcol1.distanceSquared(mcol2, MultiCol::ColourBlindMode::none));

                if (multiDiff > DEPTH) {
                    std::cerr << "ERROR TOO BIG MULTI DIFF " << multiDiff << "\n";
                    return false;
                }
                multiCount[multiDiff]++;

                if (i % INTERVAL == 0) {
                    std::cerr << (i/INTERVAL) << " of " << (COUNT/INTERVAL) << "\n";
                }
            }

            for (unsigned int i = 0; i < DEPTH; i++) {
                std::cout << multiCount[i] << " ";
            }
            std::cout << "\n";

            return true;
        }
    }
#endif

#endif

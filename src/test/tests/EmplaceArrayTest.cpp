/*
 * EmplaceArrayTest.cpp
 *
 *  Created on: 17 Jul 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_emplaceArray == 1
    #include <rascUI/util/EmplaceVector.h>
    #include <rascUI/util/EmplaceArray.h>
    #include <iostream>

    namespace rasc {

        /**
         * A simple class for testing emplace containers. This class disallows
         * copying and moving. When created, it adds the provided value to the
         * counter. When destroyed, it subtracts the provided value from the
         * counter.
         */
        class NoCopyMove {
        private:
            int * counter,
                value;

            NoCopyMove(const NoCopyMove & other);
            NoCopyMove & operator = (const NoCopyMove & other);

        public:
            NoCopyMove(int * counter, int value) :
                counter(counter),
                value(value) {
                *counter += value;
            }
            ~NoCopyMove(void) {
                *counter -= value;
            }
            inline int getValue(void) const {
                return value;
            }
        };


        bool emplaceArrayTest(void) {

            // By adding and removing elements with a sequence of 2 to the
            // power of index, (and keeping track of their sum with this counter),
            // we can make sure that all elements are created and destroyed at the
            // right time.
            int counter = 0;

            // Checks size of array, adds an element with value 1<<val,
            // checks counter value, new size and element access.
            #define TEST_ADD(container, val)                                             \
                if (val == 0) {                                                          \
                    ASSERT_TEST((container).empty())                                     \
                }                                                                        \
                ASSERT_TEST((container).size() == (val))                                 \
                (container).emplace(&counter, 1 << (val));                               \
                ASSERT_TEST((container).size() == (val) + 1)                             \
                ASSERT_TEST(counter == (1 << ((val) + 1)) - 1)                           \
                ASSERT_TEST((container)[val].getValue() == (1 << (val)))                 \
                ASSERT_TEST((container).back().getValue() == (1 << (val)))               \
                ASSERT_TEST(((container).end() - 1)->getValue() == (1 << (val)))

            // Checks size of array, removes an element with value 1<<val,
            // checks counter value, new size and element access.
            #define TEST_REMOVE(container, val)                                          \
                ASSERT_TEST((container).size() == (val + 1))                             \
                (container).pop_back();                                                  \
                ASSERT_TEST((container).size() == (val))                                 \
                ASSERT_TEST(counter == (1 << (val)) - 1)                                 \
                if (val == 0) {                                                          \
                    ASSERT_TEST((container).empty())                                     \
                } else {                                                                 \
                    ASSERT_TEST((container)[val - 1].getValue() == (1 << (val - 1)))     \
                    ASSERT_TEST((container).back().getValue() == (1 << (val - 1)))       \
                    ASSERT_TEST(((container).end() - 1)->getValue() == (1 << (val - 1))) \
                }

            // Adds a value, removes it, then adds it again. See above.
            #define TEST_ADD_REMOVE_ADD(container, val)                                  \
                TEST_ADD(container, val)                                                 \
                TEST_REMOVE(container, val)                                              \
                TEST_ADD(container, val)


            // ========================= EmplaceArray ===============================

            // Test filling an emplace array completely, then check
            // everything was destroyed when it goes out of scope.
            // Clear it and re-fill it also.
            {
                rascUI::EmplaceArray<NoCopyMove, 8> arr;
                for (unsigned int i = 0; i < 8; i++) {
                    TEST_ADD_REMOVE_ADD(arr, i)
                }
                arr.clear();
                ASSERT_TEST(counter == 0)
                for (unsigned int i = 0; i < 8; i++) {
                    TEST_ADD_REMOVE_ADD(arr, i)
                }
            }
            ASSERT_TEST(counter == 0)

            // Test only partially filling an emplace array, then check
            // everything was destroyed when it goes out of scope.
            // Clear it and re-fill it also.
            {
                rascUI::EmplaceArray<NoCopyMove, 16> arr;
                for (unsigned int i = 0; i < 12; i++) {
                    TEST_ADD_REMOVE_ADD(arr, i)
                }
                arr.clear();
                ASSERT_TEST(counter == 0)
                for (unsigned int i = 0; i < 10; i++) {
                    TEST_ADD_REMOVE_ADD(arr, i)
                }
            }
            ASSERT_TEST(counter == 0)

            // ========================= EmplaceVector ==============================

            // Test filling an emplace array completely, then check
            // everything was destroyed when it goes out of scope.
            // Clear it and re-fill it also.
            {
                rascUI::EmplaceVector<NoCopyMove> vec(8);
                for (unsigned int i = 0; i < 8; i++) {
                    TEST_ADD_REMOVE_ADD(vec, i)
                }
                vec.clear();
                ASSERT_TEST(counter == 0)
                for (unsigned int i = 0; i < 8; i++) {
                    TEST_ADD_REMOVE_ADD(vec, i)
                }
            }
            ASSERT_TEST(counter == 0)

            // Test only partially filling an emplace array, then check
            // everything was destroyed when it goes out of scope.
            // Clear it and re-fill it also.
            {
                rascUI::EmplaceArray<NoCopyMove, 16> vec;
                for (unsigned int i = 0; i < 12; i++) {
                    TEST_ADD_REMOVE_ADD(vec, i)
                }
                vec.clear();
                ASSERT_TEST(counter == 0)
                for (unsigned int i = 0; i < 10; i++) {
                    TEST_ADD_REMOVE_ADD(vec, i)
                }
            }
            ASSERT_TEST(counter == 0)

            // Do the same as before, but this time clear it by resizing it.
            {
                rascUI::EmplaceVector<NoCopyMove> vec(8);
                for (unsigned int i = 0; i < 8; i++) {
                    TEST_ADD_REMOVE_ADD(vec, i)
                }
                vec.resize(12);
                ASSERT_TEST(counter == 0)
                for (unsigned int i = 0; i < 12; i++) {
                    TEST_ADD_REMOVE_ADD(vec, i)
                }
            }
            ASSERT_TEST(counter == 0)

            // Do the same as last time, but start with a default constructed vector
            // and set its initial size by resizing.
            {
                rascUI::EmplaceVector<NoCopyMove> vec;
                vec.resize(8);
                for (unsigned int i = 0; i < 8; i++) {
                    TEST_ADD_REMOVE_ADD(vec, i)
                }
                vec.resize(12);
                ASSERT_TEST(counter == 0)
                for (unsigned int i = 0; i < 12; i++) {
                    TEST_ADD_REMOVE_ADD(vec, i)
                }
            }
            ASSERT_TEST(counter == 0)

            // Test partially filling it again, but clear by resizing.
            {
                rascUI::EmplaceVector<NoCopyMove> vec(16);
                for (unsigned int i = 0; i < 12; i++) {
                    TEST_ADD_REMOVE_ADD(vec, i)
                }
                vec.resize(11);
                ASSERT_TEST(counter == 0)
                for (unsigned int i = 0; i < 10; i++) {
                    TEST_ADD_REMOVE_ADD(vec, i)
                }
            }
            ASSERT_TEST(counter == 0)

            return true;
        }
    }
#endif

#endif

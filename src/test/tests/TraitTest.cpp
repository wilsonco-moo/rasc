/*
 * TraitTest.cpp
 *
 *  Created on: 5 Oct 2020
 *      Author: wilson
 */

#include "../TestControl.h"
#ifdef TESTING_MODE_ENABLED

#if TEST_ENABLED_trait == 1
    #include <baseGame/common/util/templateTypes/staticStorage/StaticVector.h>
    #include <baseGame/common/gameMap/properties/trait/TraitRange.h>
    #include <baseGame/common/gameMap/properties/trait/TraitDef.h>
    #include <baseGame/common/gameMap/properties/trait/Traits.h>
    #include <baseGame/common/gameMap/properties/Properties.h>
    #include <unordered_set>
    #include <unordered_map>
    #include <algorithm>
    #include <iostream>

    namespace rasc {

        #define ASSERT_VALID_TRAIT(traitId) \
            ASSERT_TEST(Properties::isValidProperty(traitId) && Properties::getBaseType(traitId) == Properties::BaseTypes::trait)

        /**
         * Returns true if "subsetVecA" is a subset of "vecB".
         */
        static bool isASubsetOfB(const StaticVector<proptype_t> & subsetVecA, const StaticVector<proptype_t> & vecB) {
            std::unordered_set<proptype_t> superset(vecB.begin(), vecB.end());
            for (proptype_t value : subsetVecA) {
                if (superset.find(value) == superset.end()) return false;
            }
            return true;
        }

        /**
         * Returns true if vec has duplicate values.
         */
        static bool hasDuplicates(const StaticVector<proptype_t> & vec) {
            std::unordered_set<proptype_t> set;
            for (proptype_t value : vec) {
                if (set.find(value) != set.end()) {
                    return true;
                }
                set.insert(value);
            }
            return false;
        }

        /**
         * Performs a depth-first search on the directed graph described by an unordered map of unordered sets.
         * Each entry in the map is a node, each entry in the associated set is a connected node.
         * "output" is populated with all connecting nodes to "node", NOT including "node".
         */
        static void dfs(std::unordered_set<proptype_t> & output, const std::unordered_map<proptype_t, std::unordered_set<proptype_t>> & graph, proptype_t node) {
            auto iter = graph.find(node);
            if (iter == graph.end()) return;
            for (proptype_t connectingNode : iter->second) {
                if (output.insert(connectingNode).second) { // Don't search node twice.
                    dfs(output, graph, connectingNode);
                }
            }
        }

        /**
         * Removes unnecessary connections from a (transitive) directed graph - transitive reduction.
         * The removed pairs (if any) are added to removedPairs. If by the end of this operation
         * removedPairs is empty, the original graph had no redundancy.
         */
        static void transitiveReduce(std::unordered_map<proptype_t, std::unordered_set<proptype_t>> & graph, std::vector<std::pair<proptype_t, proptype_t>> & removedPairs) {
            std::unordered_set<proptype_t> redundantConnections;
            for (std::pair<const proptype_t, std::unordered_set<proptype_t>> & node : graph) {
                redundantConnections.clear();
                for (proptype_t connectingNode : node.second) {
                    dfs(redundantConnections, graph, connectingNode);
                }
                for (proptype_t redundantNode : redundantConnections) {
                    if (node.second.erase(redundantNode) != 0) {
                        removedPairs.emplace_back(node.first, redundantNode);
                    }
                }
            }
        }

        /**
         * If one of the traits has a non-zero modifier, statsInCommon is incremented.
         * If that is the case, then additionally if costReqTrait is better value than
         * sourceTrait, reqIsBetterValue is incremented.
         */
        static void testBetterValue(unsigned int & statsInCommon, unsigned int & reqIsBetterValue,
                                    proptype_t sourceTrait,  provstat_t sourceCost, provstat_t sourceMod,
                                    proptype_t costReqTrait, provstat_t reqCost,    provstat_t reqMod) {

            if (sourceMod == 0 && reqMod == 0) return;
            statsInCommon++;

            if ((reqMod * sourceCost) > (sourceMod * reqCost)) {
                reqIsBetterValue++;
            }
        }

        /**
         * Returns true if "sourceTrait" cost-requires "costReqTrait".
         * This is true if the cost-required trait is better value in all circumstances
         * than the source trait, if the cost required trait does not require or
         * block any additional traits, and if neither trait is either a non-building,
         * combat building or "none" building. This is also true if the source trait
         * requires the cost required trait.
         */
        static bool doesCostRequire(proptype_t sourceTrait, proptype_t costReqTrait) {

            // Traits cannot cost require themselves.
            if (sourceTrait == costReqTrait) {
                return false;
            }

            // If source requires the trait, it must also cost require it.
            if (Traits::hasRequiredTrait(sourceTrait, costReqTrait)) {
                return true;
            }

            // There is no point additionally adding a cost requirement if one of the traits is not
            // a regular building.
            if (!Traits::isTraitRegularBuilding(sourceTrait) || !Traits::isTraitRegularBuilding(costReqTrait)) {
                return false;
            }

            // The cost-required trait cannot block things which the source trait does not,
            // or require things which the source trait does not.
            // i.e: cost-required trait's blocking traits must be a subset of the source trait's blocking traits,
            //  and cost-required trait's required traits must be a subset of the source trait's required traits.
            if (!isASubsetOfB(Traits::getBlockingTraits(costReqTrait), Traits::getBlockingTraits(sourceTrait)) ||
                !isASubsetOfB(Traits::getRequiredTraits(costReqTrait), Traits::getRequiredTraits(sourceTrait))) {
                return false;
            }

            provstat_t sourceCost = Traits::getTraitBuildingCost(NULL, sourceTrait, 1).getOverallCostEstimation(),
                       reqCost    = Traits::getTraitBuildingCost(NULL, costReqTrait, 1).getOverallCostEstimation();

            // We cannot figure out value for money if one of the traits costs nothing.
            if (sourceCost == 0 || reqCost == 0) {
                return false;
            }

            ProvStatModSet sourceMod = Traits::getProvinceStatModiferSet(sourceTrait, 1),
                              reqMod = Traits::getProvinceStatModiferSet(costReqTrait, 1);

            unsigned int statsInCommon = 0,
                         reqIsBetterValue = 0;

            // For convenience: testBetterValue for specified stat, value type and sign.
            #define TEST_VALUE(stat, valueType, sign)                                                                   \
                testBetterValue(statsInCommon, reqIsBetterValue,                                                        \
                                sourceTrait,  sourceCost, sign sourceMod.getProvStatMod##valueType(ProvStatIds::stat),  \
                                costReqTrait, reqCost,    sign    reqMod.getProvStatMod##valueType(ProvStatIds::stat));

            // Runs TEST_VALUE for both Value and Percentage.
            #define TEST_VALUE_AND_PERC(stat, sign) \
                TEST_VALUE(stat, Value,      sign)  \
                TEST_VALUE(stat, Percentage, sign)

            // Test each relevant stat. Note: Don't bother checking land use.
            TEST_VALUE_AND_PERC(manpowerIncome, +)
            TEST_VALUE_AND_PERC(currencyIncome, +)
            TEST_VALUE_AND_PERC(landAvailable,  +)
            TEST_VALUE_AND_PERC(manpowerExpenditure, -)
            TEST_VALUE_AND_PERC(currencyExpenditure, -)

            // We cost require if there is at least one stat in common, and req is better value
            // for all of the stats we have in common.
            return statsInCommon != 0 && reqIsBetterValue == statsInCommon;
        }


        /**
         * Makes sure required/allowing and blocking trait relations are symmetric.
         * Also makes sure all mentioned trait relations are valid traits, and that
         * no duplicates are found.
         */
        static bool checkSymmetry(void) {
            for (proptype_t trait = TraitRange::MIN; trait <= TraitRange::MAX; trait++) {

                const StaticVector<proptype_t> & required = Traits::getRequiredTraits(trait),
                                               & blocking = Traits::getBlockingTraits(trait),
                                               & allowing = Traits::getAllowingTraits(trait),
                                               & costRequired = Traits::getCostRequiredTraits(trait),
                                               & costAllowing = Traits::getCostAllowingTraits(trait);

                // If the trait is not buildable, it must not require other traits, since that would be redundant.
                ASSERT_TEST(Traits::isTraitBuildable(trait) || required.empty())
                // Duplicates are not allowed.
                ASSERT_TEST(!hasDuplicates(required))
                ASSERT_TEST(!hasDuplicates(blocking))
                ASSERT_TEST(!hasDuplicates(allowing))
                ASSERT_TEST(!hasDuplicates(costRequired))
                ASSERT_TEST(!hasDuplicates(costAllowing))
                // Traits cannot require, block, allow, cost-require or cost-allow themself.
                ASSERT_TEST(std::find(required.begin(), required.end(), trait) == required.end())
                ASSERT_TEST(std::find(blocking.begin(), blocking.end(), trait) == blocking.end())
                ASSERT_TEST(std::find(allowing.begin(), allowing.end(), trait) == allowing.end())
                ASSERT_TEST(std::find(costRequired.begin(), costRequired.end(), trait) == costRequired.end())
                ASSERT_TEST(std::find(costAllowing.begin(), costAllowing.end(), trait) == costAllowing.end())
                // Check symmetry of blocking traits.
                for (proptype_t blockingTraitId : blocking) {
                    ASSERT_VALID_TRAIT(blockingTraitId)
                    ASSERT_TEST(Traits::hasBlockingTrait(trait, blockingTraitId))
                    ASSERT_TEST(Traits::hasBlockingTrait(blockingTraitId, trait))
                }
                // Check symmetry of required traits.
                for (proptype_t requiredTraitId : required) {
                    ASSERT_VALID_TRAIT(requiredTraitId)
                    ASSERT_TEST(Traits::hasRequiredTrait(trait, requiredTraitId))
                    ASSERT_TEST(Traits::hasAllowingTrait(requiredTraitId, trait))
                }
                // Check symmetry of allowing traits.
                for (proptype_t allowingTraitId : allowing) {
                    ASSERT_VALID_TRAIT(allowingTraitId)
                    ASSERT_TEST(Traits::hasAllowingTrait(trait, allowingTraitId))
                    ASSERT_TEST(Traits::hasRequiredTrait(allowingTraitId, trait))
                }
                // Check symmetry of costRequired traits.
                for (proptype_t costRequiredTraitId : costRequired) {
                    ASSERT_VALID_TRAIT(costRequiredTraitId)
                    ASSERT_TEST(Traits::hasCostRequiredTrait(trait, costRequiredTraitId))
                    ASSERT_TEST(Traits::hasCostAllowingTrait(costRequiredTraitId, trait))
                }
                // Check symmetry of costAllowing traits.
                for (proptype_t costAllowingTraitId : costAllowing) {
                    ASSERT_VALID_TRAIT(costAllowingTraitId)
                    ASSERT_TEST(Traits::hasCostAllowingTrait(trait, costAllowingTraitId))
                    ASSERT_TEST(Traits::hasCostRequiredTrait(costAllowingTraitId, trait))
                }
                // Traits cannot require a trait which they also block.
                for (proptype_t requiredTrait : required) {
                    ASSERT_TEST(std::find(blocking.begin(), blocking.end(), requiredTrait) == blocking.end())
                }
                // Traits cannot allow a trait which they also block.
                for (proptype_t allowingTrait : allowing) {
                    ASSERT_TEST(std::find(blocking.begin(), blocking.end(), allowingTrait) == blocking.end())
                }
                // Traits cannot cost-require a trait which they also block.
                for (proptype_t costRequiredTrait : costRequired) {
                    ASSERT_TEST(std::find(blocking.begin(), blocking.end(), costRequiredTrait) == blocking.end())
                }
                // Traits cannot cost-allow a trait which they also block.
                for (proptype_t costAllowingTrait : costAllowing) {
                    ASSERT_TEST(std::find(blocking.begin(), blocking.end(), costAllowingTrait) == blocking.end())
                }
            }
            return true;
        }


        /**
         * Builds graphs of trait requiring, blocking, allowing, cost-requiring and cost-allowing,
         * then transitively reduces them, and makes sure the originals had no redundant
         * connections.
         */
        static bool checkTransitivity(void) {

            std::unordered_map<proptype_t, std::unordered_set<proptype_t>> graph;
            std::vector<std::pair<proptype_t, proptype_t>> removedPairs;

            // Checks that the graph generated from the provided relation between traits, does
            // not have any redundant connections.
            #define DO_TRANSITIVITY_TEST(traitFunc, relationName)                                           \
                graph.clear();                                                                              \
                for (proptype_t trait = TraitRange::MIN; trait <= TraitRange::MAX; trait++) {               \
                    for (proptype_t related : traitFunc(trait)) {                                           \
                        graph[trait].insert(related);                                                       \
                    }                                                                                       \
                }                                                                                           \
                removedPairs.clear();                                                                       \
                transitiveReduce(graph, removedPairs);                                                      \
                if (!removedPairs.empty()) {                                                                \
                    std::cerr << "FAILED TRANSITIVITY TEST: \n";                                            \
                    for (const std::pair<proptype_t, proptype_t> & pair : removedPairs) {                   \
                        std::cerr << "    Trait " << Properties::typeToInternalName(pair.first)             \
                                  << " " relationName " " << Properties::typeToInternalName(pair.second)    \
                                  << " is redundant.\n";                                                    \
                    }                                                                                       \
                    return false;                                                                           \
                }

            DO_TRANSITIVITY_TEST(Traits::getRequiredTraits, "requiring")
            DO_TRANSITIVITY_TEST(Traits::getAllowingTraits, "allowing")
            DO_TRANSITIVITY_TEST(Traits::getCostRequiredTraits, "cost-requiring")
            DO_TRANSITIVITY_TEST(Traits::getCostAllowingTraits, "cost-allowing")
            return true;
        }

        /**
         * Checks that all cost requiring traits match up with those generated
         * with doesCostRequire, and that there is no redundancy.
         */
        static bool checkCostRequiring(void) {

            // Pass every combination of pairs of traits through doesCostRequire, generate
            // graph of prospective cost-requiring, then transitively reduce it.
            std::unordered_map<proptype_t, std::unordered_set<proptype_t>> genCostReqGraph;
            for (proptype_t sourceTrait = TraitRange::MIN; sourceTrait <= TraitRange::MAX; sourceTrait++) {
                for (proptype_t costReqTrait = TraitRange::MIN; costReqTrait <= TraitRange::MAX; costReqTrait++) {
                    if (doesCostRequire(sourceTrait, costReqTrait)) {
                        genCostReqGraph[sourceTrait].insert(costReqTrait);
                    }
                }
            }
            std::vector<std::pair<proptype_t, proptype_t>> removedPairs;
            transitiveReduce(genCostReqGraph, removedPairs);

            // Build a graph of the *real* cost requiring traits, according to Traits.
            std::unordered_map<proptype_t, std::unordered_set<proptype_t>> realCostReqGraph;
            for (proptype_t trait = TraitRange::MIN; trait <= TraitRange::MAX; trait++) {
                for (proptype_t costReqTrait : Traits::getCostRequiredTraits(trait)) {
                    realCostReqGraph[trait].insert(costReqTrait);
                }
            }

            // Complain about cost-requires which exist but should not.
            bool success = true;
            for (const std::pair<const proptype_t, std::unordered_set<proptype_t>> & pair : realCostReqGraph) {
                for (proptype_t costReqTrait : pair.second) {
                    auto iter = genCostReqGraph.find(pair.first);
                    if (iter == genCostReqGraph.end() || iter->second.find(costReqTrait) == iter->second.end()) {
                        std::cerr << "REDUNDANT COST-REQUIRE: Trait " << Properties::typeToInternalName(pair.first) <<
                                     " marked as cost-requiring " << Properties::typeToInternalName(costReqTrait) <<
                                     ", but either not cost-requiring or redundant.\n";
                        success = false;
                    }
                }
            }

            // Complain about cost-requires which are missing.
            for (const std::pair<const proptype_t, std::unordered_set<proptype_t>> & pair : genCostReqGraph) {
                for (proptype_t costReqTrait : pair.second) {
                    auto iter = realCostReqGraph.find(pair.first);
                    if (iter == realCostReqGraph.end() || iter->second.find(costReqTrait) == iter->second.end()) {
                        std::cerr << "MISSING COST-REQUIRE: Trait " << Properties::typeToInternalName(pair.first) <<
                                     " should be marked as cost-requiring " << Properties::typeToInternalName(costReqTrait) <<
                                     ", but this relation is missing.\n";
                        success = false;
                    }
                }
            }

            return success;
        }

        bool traitTest(void) {
            ASSERT_TEST(checkSymmetry())
            ASSERT_TEST(checkTransitivity())
            ASSERT_TEST(checkCostRequiring())
            return true;
        }
    }
#endif

#endif

/*
 * TestControl.h
 *
 *  Created on: 5 May 2020
 *      Author: wilson
 */

#ifndef TEST_TESTCONTROL_H_
#define TEST_TESTCONTROL_H_

/**
 * Whether to enable testing mode globally or not.
 */
//#define TESTING_MODE_ENABLED

/**
 * If this is defined then ALL tests are enabled, (except those declared
 * as TEST_DISABLE_FORCE).
 *
 * Note that this must not be defined at the same time as
 * TESTING_FORCE_DISABLE_ALL.
 */
//#define TESTING_FORCE_ENABLE_ALL

/**
 * If this is defined then ALL tests are disabled, (except those declared
 * as TEST_ENABLE_FORCE).
 *
 * Note that this must not be defined at the same time as
 * TESTING_FORCE_ENABLE_ALL.
 */
//#define TESTING_FORCE_DISABLE_ALL



#ifdef TESTING_MODE_ENABLED
namespace rasc {

    /**
     * This runs all tests, returning true if all were successful.
     * This should be run instead of the main game, by the main method,
     * if TESTING_MODE_ENABLED is set.
     */
    bool runAllTests(void);

    // If not running in TestControl.cpp, use zero or one values for
    // enable or disable, (depending on status of force test macros).
    #ifndef RUNNING_WITHIN_TESTCONTROL_CPP
        #ifdef TESTING_FORCE_ENABLE_ALL
            #define TEST_DISABLE_FORCE 0
            #define TEST_DISABLE       1
            #define TEST_ENABLE        1
            #define TEST_ENABLE_FORCE  1
        #else
            #ifdef TESTING_FORCE_DISABLE_ALL
                #define TEST_DISABLE_FORCE 0
                #define TEST_DISABLE       0
                #define TEST_ENABLE        0
                #define TEST_ENABLE_FORCE  1
            #else
                #define TEST_DISABLE_FORCE 0
                #define TEST_DISABLE       0
                #define TEST_ENABLE        1
                #define TEST_ENABLE_FORCE  1
            #endif
        #endif
        #include "TestList.h"
    #endif

    // --------------- Utility macros for testing ----------------

    /**
     * This macro provides a convenient assertion mechanism. If the
     * provided expression is false, a complaint is printed to the log
     * and false is returned.
     */
    #define ASSERT_TEST(test)                                   \
        if (!(test)) {                                          \
            std::cerr << "Assertion failed: " #test ".\n";      \
            return false;                                       \
        }

    /**
     * The port to listen to, when tests require network operations.
     */
    #define TEST_LISTEN_PORT 64000
}
#endif
#endif

/*
 * TestControl.cpp
 *
 *  Created on: 5 May 2020
 *      Author: wilson
 */

// Notify that we are TestControl.cpp then include the header.
#define RUNNING_WITHIN_TESTCONTROL_CPP
#include "TestControl.h"

#ifdef TESTING_MODE_ENABLED
#include <iostream>
#include <cstddef>
#include <vector>

// Make sure that TESTING_FORCE_ENABLE_ALL and TESTING_FORCE_DISABLE_ALL
// are not BOTH defined at the same time.
#ifdef TESTING_FORCE_ENABLE_ALL
    #ifdef TESTING_FORCE_DISABLE_ALL
        #error "TESTING_FORCE_ENABLE_ALL and TESTING_FORCE_DISABLE_ALL must not both be defined at the same time."
    #endif
#endif

// Use function like macros for enabled or disabled, then include the test list.
#ifdef TESTING_FORCE_ENABLE_ALL
    #define TEST_DISABLE_FORCE(x)
    #define TEST_DISABLE(x)       x
    #define TEST_ENABLE(x)        x
    #define TEST_ENABLE_FORCE(x)  x
#else
    #ifdef TESTING_FORCE_DISABLE_ALL
        #define TEST_DISABLE_FORCE(x)
        #define TEST_DISABLE(x)
        #define TEST_ENABLE(x)
        #define TEST_ENABLE_FORCE(x)  x
    #else
        #define TEST_DISABLE_FORCE(x)
        #define TEST_DISABLE(x)
        #define TEST_ENABLE(x)        x
        #define TEST_ENABLE_FORCE(x)  x
    #endif
#endif
#include "TestList.h"

namespace rasc {

    // Define all test functions which happen to be enabled.
    #define X(name) \
        TEST_ENABLED_##name(bool name##Test(void);)
    LIST_OF_TESTS
    #undef X

    // An enum of the test names, so we know how many there are.
    #define COMMA ,
    #define X(name) \
        TEST_ENABLED_##name(name COMMA)
    class TestList {
    public:
        enum {
            LIST_OF_TESTS
            COUNT
        };
    };
    #undef X

    bool runAllTests(void) {

        std::vector<const char *> failedTests;
        size_t successCount = 0;

        // Run all tests, count successes and failures.
        #define X(name)                                                        \
            TEST_ENABLED_##name({                                              \
                std::cerr << "Running test " << TestList::name + 1 <<          \
                             " of " << TestList::COUNT << ": " #name "...\n";  \
                bool result = name##Test();                                    \
                if (result) {                                                  \
                    successCount++;                                            \
                } else {                                                       \
                    std::cerr << "FAILED TEST " #name ".\n";                   \
                    failedTests.push_back(#name);                              \
                }                                                              \
            })
        LIST_OF_TESTS
        #undef X

        // Print results.
        std::cerr << "\n\nTests complete: " << successCount << " of " << TestList::COUNT << " successful.\n";

        if (!failedTests.empty()) {
            std::cerr << "Failed tests: " << failedTests.front();
            for (size_t i = 1; i < failedTests.size(); i++) {
                std::cerr << ", " << failedTests[i];
            }
            std::cerr << ".\n";
        }

        // If none failed return true.
        return failedTests.empty();
    }
}

#endif

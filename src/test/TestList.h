/*
 * TestList.h
 *
 *  Created on: 5 May 2020
 *      Author: wilson
 */

/**
 * Put here the name of any tests, along with TEST_ENABLE or TEST_DISABLE
 * which control whether they should be run or not.
 * Tests should return true on success, and false on failure.
 *
 * Note: Do not include this file directly, always only include TestControl.h.
 * Note: Do not edit the end comment, and ensure that ALL lines
 *       (even the last one) have backslashes, as these are required by the
 *       newTest bash script.
 */
// Start of test list.
#define LIST_OF_TESTS                             \
    X(armySelectControl)                          \
    X(building)                                   \
    X(checkedArithmetic)                          \
    X(colourBlindImageGen)                        \
    X(colourSpace)                                \
    X(emplaceArray)                               \
    X(functionQueue)                              \
    X(iconSVG)                                    \
    X(messageFile)                                \
    X(numberFormat)                               \
    X(offVal)                                     \
    X(path)                                       \
    X(probabilityArray)                           \
    X(properties)                                 \
    X(random)                                     \
    X(rascUI)                                     \
    X(rascUpdatable)                              \
    X(serialNumberArith)                          \
    X(staticStorageInit)                          \
    X(statModifier)                               \
    X(terrainDisplay)                             \
    X(thread1)                                    \
    X(thread2)                                    \
    X(trait)                                      \
    X(unitCount)                                  \
    X(zeroPrefixedNumber)                         \
// End of test list.

/**
 * Whether each of the tests listed above are enabled or disabled,
 * so whether they will run or not.
 * Note: Do not add blank lines or edit the start or end comments, as these
 *       are required by the newTest bash script.
 */
// Start of test enabled list.
#define TEST_ENABLED_armySelectControl                          TEST_ENABLE
#define TEST_ENABLED_building                                   TEST_ENABLE
#define TEST_ENABLED_checkedArithmetic                          TEST_ENABLE
#define TEST_ENABLED_colourBlindImageGen                        TEST_DISABLE
#define TEST_ENABLED_colourSpace                                TEST_ENABLE
#define TEST_ENABLED_emplaceArray                               TEST_ENABLE
#define TEST_ENABLED_functionQueue                              TEST_DISABLE
#define TEST_ENABLED_iconSVG                                    TEST_ENABLE
#define TEST_ENABLED_messageFile                                TEST_ENABLE
#define TEST_ENABLED_numberFormat                               TEST_ENABLE
#define TEST_ENABLED_offVal                                     TEST_ENABLE
#define TEST_ENABLED_path                                       TEST_ENABLE
#define TEST_ENABLED_probabilityArray                           TEST_ENABLE
#define TEST_ENABLED_properties                                 TEST_ENABLE
#define TEST_ENABLED_random                                     TEST_ENABLE
#define TEST_ENABLED_rascUI                                     TEST_ENABLE
#define TEST_ENABLED_rascUpdatable                              TEST_ENABLE
#define TEST_ENABLED_serialNumberArith                          TEST_ENABLE
#define TEST_ENABLED_staticStorageInit                          TEST_ENABLE
#define TEST_ENABLED_statModifier                               TEST_ENABLE
#define TEST_ENABLED_terrainDisplay                             TEST_ENABLE
#define TEST_ENABLED_thread1                                    TEST_DISABLE
#define TEST_ENABLED_thread2                                    TEST_DISABLE
#define TEST_ENABLED_trait                                      TEST_ENABLE
#define TEST_ENABLED_unitCount                                  TEST_ENABLE
#define TEST_ENABLED_zeroPrefixedNumber                         TEST_ENABLE
// End of test enabled list.

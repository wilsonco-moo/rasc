#include <baseGame/common/config/StaticConfig.h>
#include <baseGame/client/mainMenu/TitleRoom.h>
#include <baseGame/client/rooms/LoadingRoom.h>
#include <baseGame/client/GlobalProperties.h>
#include <wool/window/config/WindowConfig.h>
#include <baseGame/server/ServerConsole.h>
#include <wool/window/base/WindowBase.h>
#include <baseGame/common/util/Misc.h>
#include <wool/room/base/RoomBase.h>
#include <threading/ThreadQueue.h>
#include <sockets/plus/misc.h>
#include <test/TestControl.h>
#include <iostream>
#include <cstddef>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <string>
#include <ctime>

#define WINDOW_TITLE ((char *)"Rasc: The ultimate strategy game")
#define FRAME_RATE_LOGGING false

// Defines how Rasc is launched, in both command line and normal modes.
#define LAUNCH_RASC                                     \
    wool::WindowConfig windowConfig;                    \
    windowConfig.windowTitle = WINDOW_TITLE;            \
    wool::WindowBase window(room, windowConfig);        \
    window.enableFrameRateLogging = FRAME_RATE_LOGGING; \
    window.start(&argc, argv);

int main(int argc, char ** argv) {
    rasc::Misc::setupSignalAndTerminateHandlers();
    simplenetwork::socketsInit();
    #ifdef TESTING_MODE_ENABLED
        rasc::runAllTests();
    #else
        if (argc >= 2 && strcmp("cmd", argv[1]) == 0) {
            // ========================== COMMAND LINE LAUNCH MODE =======================================================

            if (rasc::Misc::getYN("Start server? Y/N: ")) {
                // ------------------------- For starting the server --------------------
                bool restartServer;
                do {
                    restartServer = false;
                    printf("Enter port number for server: ");
                    uint16_t port = rasc::Misc::readNumberRange(0, (uint16_t)-1);

                    printf("Enter number of AI players to place on map: ");
                    int aisToPlace = rasc::Misc::readNumberRange(0, INT_MAX, true, false);

                    std::string map;
                    {
                        std::vector<std::string> maps = rasc::StaticConfig::getMaps(false);
                        if (maps.empty()) {
                            std::cout << "ERROR: There are no maps available for the server to use.\n";
                            break;
                        }
                        map = maps[rasc::Misc::selectFromOptions(maps, "\nAvailable maps:\n", "Please select a map to use for the server: ")];
                    }

                    rasc::ServerConsole console(&restartServer, port, aisToPlace, map);
                } while(restartServer);
            } else {
                // ------------------------- For starting the client --------------------
                printf("Enter address of server to connect to: ");
                std::string address;
                std::getline(std::cin, address);

                printf("Enter port number of server to connect to: ");
                uint16_t port = rasc::Misc::readNumberRange(0, (uint16_t)-1);

                printf("Enter player name: ");
                std::string playerName;
                std::getline(std::cin, playerName);

                while(playerName.length() > 12) {
                    printf("The player name cannot be more 12 characters. Please type it again:\n> ");
                    std::getline(std::cin, playerName);
                }

                wool::RoomBase * room = NULL;

                // Set up some connection info. We will pass the rooms a pointer to this, which they will use.
                // This is safe since this will not be destroyed until after the client exits, so avoids unnecessary copying.
                rasc::GlobalProperties globalProperties(address, playerName, port);

                // If the user wants us to skip the main menu and launch the client immediately...
                if (argc > 2 && strcmp(argv[2], "client") == 0) {
                    // Initialise the game here. Call title room and main menu room initialisation since we bypass the title room.
                    globalProperties.titleRoomInitialisation();
                    globalProperties.mainMenuRoomInitialisation();
                    // The above two functions load textures etc on background threads. Since we bypass the
                    // main menu, wait for those to finish before the next step.
                    globalProperties.threadQueue->waitForJobToComplete(globalProperties.threadQueue->addBarrier());
                    std::string failureReason;
                    globalProperties.startInternalServer = false; // We don't want to start an internal server, as we are connecting to a server.
                    room = rasc::LoadingRoom::initialiseGameClient(&globalProperties, failureReason);
                } else {
                    room = new rasc::TitleRoom(&globalProperties); // Otherwise, start with the title room.
                }

                // If the room is not NULL: (The room will be NULL if the user decides to go straight to the game client, but the connection fails)...
                // .. Then launch the game with that room.
                if (room != NULL) {
                    LAUNCH_RASC
                }
            }
        } else {
            // -------------------------- GUI LAUNCH MODE --------------------------
            rasc::GlobalProperties globalProperties;
            wool::RoomBase * room = new rasc::TitleRoom(&globalProperties);
            LAUNCH_RASC
            // ---------------------------------------------------------------------
        }
        printf("Exiting Rasc.\n");
        // Traits used to be freed here.
    #endif
    // Ensure networking stuff is tidied up at the end.
    simplenetwork::socketsQuit();
    return 0;
}

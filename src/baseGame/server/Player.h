/*
 * Player.h
 *
 * The Player class represents a game player, on the client and the server.
 * This class contains a 64-bit player Id and a colour.
 *
 *  Created on: 21 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_PLAYER_H_
#define BASEGAME_SERVER_PLAYER_H_

#include <cstdint>

namespace simplenetwork {
    class Message;
}

namespace rasc {
    class PlayerData;

    class Player {

    private:
        PlayerData * data;

    public:
        Player(PlayerData * data);
        virtual ~Player(void);

        inline void setData(PlayerData * data) {
            this->data = data;
        }

        inline PlayerData * getData(void) const {
            return data;
        }

        // -------------- TURN SYSTEM --------------------

        /**
         * This must be called from within the server's mutex.
         *
         * For human players:
         *  > This method should return the turn status from their player data.
         *
         * For AI players:
         *  > This should always return true.
         */
        virtual bool haveFinishedTurn(void) = 0;

        /**
         * This must be called from within the server's mutex.
         *
         * For human players:
         *  > This method should notify the human player that they can start their turn.
         *    The player should write a misc update which resets their turn status to the supplied message.
         *
         * For AI players:
         *  > This method should carry out all of the AI player's turn calculations. All map updates
         *    done should be written to the given simplenetwork::Message, as the supplied message
         *    will be sent to all clients as a misc update.
         *    We should return true here if we have no provinces and are out of the game.
         *
         * Note: This method is called from within the server's main mutex.
         */
        virtual bool startTurn(simplenetwork::Message & msg) = 0;

        /**
         * Here, AI players should return false, and human players should return true.
         */
        virtual bool isHumanPlayer(void) const = 0;

    };

}

#endif

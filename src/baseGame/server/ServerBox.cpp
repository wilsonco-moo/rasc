/*
 * ServerBox.cpp
 *
 *  Created on: 21 Feb 2019
 *      Author: wilson
 */

#include "ServerBox.h"

#include <iostream>
#include <cstddef>

#include <sockets/plus/message/Message.h>
#include <tinyxml2/tinyxml2.h>

#include "../common/stats/types/UnitsDeployedStat.h"
#include "../common/util/numeric/UniqueIdAssigner.h"
#include "../common/playerData/PlayerDataSystem.h"
#include "../common/stats/types/ContinentsStat.h"
#include "../common/stats/types/ProvincesStat.h"
#include "../common/stats/types/AdjacencyStat.h"
#include "../common/stats/types/BuildingStat.h"
#include "../common/stats/UnifiedStatSystem.h"
#include "../common/stats/types/AreasStat.h"
#include "../common/update/RascUpdatable.h"
#include "../common/RascUpdatableBoxIds.h"
#include "../common/util/numeric/Random.h"
#include "../common/config/StaticConfig.h"
#include "battle/ServerBattleScheduler.h"
#include "../common/config/RascConfig.h"
#include "metrics/ServerMetricsStat.h"
#include "../common/util/XMLUtil.h"
#include "gameMap/ServerGameMap.h"
#include "../common/util/Misc.h"
#include "RascBaseGameServer.h"

namespace rasc {

    ServerBox::ServerBox(const std::string & mapFilename, int aisToPlace, bool isDedicatedServer) :
        TopLevelRascUpdatable(RascUpdatable::Mode::array, BOX_IDENTIFIER_BYTES),

        // ======================== Global server properties ======================
        // These must be set before creating
        // any of the main server classes.
        mapFilename(mapFilename),
        aisToPlace(aisToPlace),
        isDedicatedServer(isDedicatedServer),
        metricsStat(NULL) {

        // Set the initial turn count.
        common.turnCount = 0;

        // Note: Don't use an initialiser list for creating stuff, since we must
        // initialise stuff in an order not possible to use an initialiser list for.

        // <><><><><><><><><><><><> Stats <><><><><><><><><><><><><><>
        // Note: Create stats and stat system BEFORE GameMap.
        // Note: Don't create or register metrics stat - that is done only on request of server console.
        common.statSystem = new UnifiedStatSystem();
        common.areasStat = new AreasStat();
        common.continentsStat = new ContinentsStat();
        common.provincesStat = new ProvincesStat();
        common.unitsDeployedStat = new UnitsDeployedStat();
        common.adjacencyStat = new AdjacencyStat();
        common.buildingStat = new BuildingStat();
        // <><><><><><><><> Register stats <><><><><><><><><><><><><><>
        common.statSystem->registerStat(common.areasStat);
        common.statSystem->registerStat(common.continentsStat);
        common.statSystem->registerStat(common.provincesStat);
        common.statSystem->registerStat(common.unitsDeployedStat);
        common.statSystem->registerStat(common.adjacencyStat);
        common.statSystem->registerStat(common.buildingStat);
        // <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

        // ========================== Main server classes =========================
        playerIdAssigner = new UniqueIdAssigner();
        armyIdAssigner = new UniqueIdAssigner();
        propertiesIdAssigner = new UniqueIdAssigner();
        
        common.random = new Random();
        
        // Note: Pass null for RascConfig's thread queue - server never saves config file!
        common.rascConfig = new RascConfig(NULL);

        map = new ServerGameMap(*this, propertiesIdAssigner);
        common.map = map;
        server = new RascBaseGameServer(*this);

        common.playerDataSystem = new PlayerDataSystem(common);
        battleScheduler = new ServerBattleScheduler(*this);

        // Traits used to be loaded here.

        // Next load the map from the map's XML file.
        {
            tinyxml2::XMLDocument xml;
            std::string mapConfigFilename = StaticConfig::getMapConfigFile(mapFilename);
            loadFile(xml, mapConfigFilename.c_str());
            map->loadFromXML(&xml);
        }

        // Now the map is loaded, initialise the stat system.
        common.statSystem->initialise(common);

        // ------------------------------ Register rasc updatable children ------------------------------------
        // We MUST ENSURE that the order is consistent with RascBox and RascUpdatableBoxIds.
        registerChild(map);
        registerChild(common.playerDataSystem);
        // Classes only present on the server, (these must be defined last):
        registerChild(playerIdAssigner);
        registerChild(armyIdAssigner);
        registerChild(battleScheduler);
        registerChild(propertiesIdAssigner);
    }

    ServerBox::~ServerBox(void) {
        delete server; // Delete the server before anything else. Once the server is gone, there
                       // cannot any longer be running threads.

        delete map;

        delete playerIdAssigner;
        delete armyIdAssigner;
        delete propertiesIdAssigner;
        delete common.playerDataSystem;
        delete battleScheduler;
        delete common.rascConfig;
        delete common.random;

        delete common.areasStat;
        delete common.continentsStat;
        delete common.provincesStat;
        delete common.unitsDeployedStat;
        delete common.adjacencyStat;
        delete common.buildingStat;
        delete metricsStat; // Still must delete metrics stat, it may have been created.
        delete common.statSystem; // Delete the stat system AFTER ALL STATS.
    }

    // -------------------------- For loading and saving server-side data, such as turn count -------------------------

    void ServerBox::onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) {
        // The client does not get it's turn count through the rasc updatable system, so we only need to write the
        // turn count if we are saving.
        if (isSaving) {
            writeUpdateHeader(msg);
            msg.writeString(mapFilename);
            msg.writeString(StaticConfig::RASC_VERSION_WITHOUT_PLATFORM);
            msg.write64(common.turnCount);
        }
    }
    bool ServerBox::onReceiveInitialData(simplenetwork::Message & msg) {
        std::string savefileMap = msg.readString();
        if (savefileMap != mapFilename) {
            std::cout << "WARNING: This savefile was made using the map: " << savefileMap << ".\n         The currently loaded map is: " << mapFilename << ".\n";
            if (!isDedicatedServer || !Misc::getYN("Load this savefile anyway? Y/N: ")) {
                return false;
            }
        }

        std::string savefileVersion = msg.readString();
        if (savefileVersion != StaticConfig::RASC_VERSION_WITHOUT_PLATFORM) {
            std::cout << "WARNING: This savefile was made using rasc version: " << savefileVersion << ",\n         but the current game version is " << StaticConfig::RASC_VERSION_WITHOUT_PLATFORM << ".\n";
            if (!Misc::getYN("Load this savefile anyway? Y/N: ")) {
                return false;
            }
        }

        common.turnCount = msg.read64();
        return true;
    }
}

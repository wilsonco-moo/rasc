/*
 * ServerGameMap.cpp
 *
 *  Created on: 13 Oct 2019
 *      Author: wilson
 */

#include "ServerGameMap.h"

#include "ServerProvince.h"
#include "../ServerBox.h"

namespace rasc {

    ServerGameMap::ServerGameMap(ServerBox & serverBox, UniqueIdAssigner * propertiesUniqueIdAssigner) :
        GameMap(serverBox.common, propertiesUniqueIdAssigner),
        serverBox(serverBox) {
    }

    ServerGameMap::~ServerGameMap(void) {
    }
    
    Province * ServerGameMap::createNewProvince(void) {
        return new ServerProvince(*this, &getArmySelectController());
    }
}

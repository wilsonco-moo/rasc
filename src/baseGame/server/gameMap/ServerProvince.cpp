/*
 * ServerProvince.cpp
 *
 *  Created on: 3 May 2021
 *      Author: wilson
 */

#include "ServerProvince.h"

#include "../battle/ServerProvinceBattleController.h"
#include "ServerGameMap.h"

namespace rasc {

    ServerProvince::ServerProvince(GameMap & gameMap, ArmySelectController * armySelectController) :
        Province(gameMap, armySelectController) {
    }
    
    ServerProvince::~ServerProvince(void) {
    }
    
    ProvinceBattleController * ServerProvince::createNewBattleController(void) {
        return new ServerProvinceBattleController(*this, ((ServerGameMap &)gameMap).getServerBox());
    }
}

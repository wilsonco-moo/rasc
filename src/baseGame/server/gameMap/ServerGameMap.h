/*
 * ServerGameMap.h
 *
 *  Created on: 13 Oct 2019
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_GAMEMAP_SERVERGAMEMAP_H_
#define BASEGAME_SERVER_GAMEMAP_SERVERGAMEMAP_H_

#include "../../common/gameMap/GameMap.h"

namespace rasc {

    class ServerBox;

    /**
     * This is a subclass of GameMap, used only on the server-side.
     * Here, any server-only functionality in the GameMap should be added.
     */
    class ServerGameMap : public GameMap {
    private:
        ServerBox & serverBox;

    public:
        ServerGameMap(ServerBox & serverBox, UniqueIdAssigner * propertiesUniqueIdAssigner);
        virtual ~ServerGameMap(void);
        
        /**
         * To allow server provinces access to server box.
         */
        inline ServerBox & getServerBox(void) const {
            return serverBox;
        }
        
    protected:
        // Override to return a ServerProvince.
        virtual Province * createNewProvince(void) override;
    };
}

#endif

/*
 * ServerProvince.h
 *
 *  Created on: 3 May 2021
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_GAMEMAP_SERVERPROVINCE_H_
#define BASEGAME_SERVER_GAMEMAP_SERVERPROVINCE_H_

#include "../../common/gameMap/mapElement/types/Province.h"

namespace rasc {

    /**
     * ServerProvince is a subclass of Province, which is the class responsible for holding
     * all province data, (various stats, armies, etc). See Province.h for more information.
     * On the game server, all provinces are ServerProvince.
     * Here, any server-only Province functionality should be added.
     */
    class ServerProvince : public Province {
    private:

    public:
        ServerProvince(GameMap & gameMap, ArmySelectController * armySelectController);
        virtual ~ServerProvince(void);
        
    protected:
        // Override to return a ServerProvinceBattleController.
        virtual ProvinceBattleController * createNewBattleController(void) override;
    };
}

#endif

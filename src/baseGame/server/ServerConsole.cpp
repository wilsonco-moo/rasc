/*
 * ServerConsole.cpp
 *
 *  Created on: 18 Mar 2019
 *      Author: wilson
 */

#include "ServerConsole.h"

#include <sockets/plus/simpleNetworkServer/SimpleNetworkServer.h>
#include <stddef.h>
#include <iostream>
#include <vector>
#include <mutex>

#include "../common/stats/UnifiedStatSystem.h"
#include "../common/config/StaticConfig.h"
#include "../client/GlobalProperties.h"
#include "metrics/ServerMetricsStat.h"
#include "../common/util/CommonBox.h"
#include "../common/util/Misc.h"
#include "RascBaseGameServer.h"

namespace rasc {

    const std::unordered_map<std::string, std::string> ServerConsole::COMMAND_HELP({
        
        {"help",
         "\n[help (no args)]\n"
         "  Shows a list of available commands.\n"
         "[help command]\n"
         "  Shows the help information about the specified command.\n\n"
        },
        
        {"exit",
         "\n[exit (no args)]\n"
         "  Exits the server cleanly.\n\n"
        },
        
        {"restart",
         "\n[restart (no args)]\n"
         "  Exits the server cleanly, then immediately restarts it.\n\n"
        },

        {"save",
         "\n[save (no args)]\n"
         "  Saves the server's current map, allowing the user to interactively choose\n"
         "  a savefile.\n\n"
        },

        {"load",
         "\n[load (no args)]\n"
         "  Loads the server's map, allowing the user to interactively choose a\n"
         "  savefile to load from.\n\n"
        },

        {"metrics-enable",
         "\n[metrics-enable (no args)]\n"
         "  Creates server metrics stat to enable metrics. If that has been done\n"
         "  already, this command does nothing.\n\n"
        },
                
        {"metrics",
         "\n[metrics args...]\n"
         "  Query metrics using specified operation, see help inside metrics command.\n\n"
        }
    });
    
    ServerConsole::ServerConsole(bool * restartServer, uint16_t port, int aisToPlace, const std::string & mapFilename) :
        box(mapFilename, aisToPlace, true),
        continueRunning(true),
        shouldRestartServer(restartServer),
        commands({

            {"help", [this](const std::vector<std::string> & args) {
                if (args.size() > 2) {
                    std::cout << "\nThe help command requires either no arguments, or a single argument.\n\n";
                } else if (args.size() == 2) {
                    std::unordered_map<std::string, std::string>::const_iterator iter = COMMAND_HELP.find(args[1]);
                    if (iter == COMMAND_HELP.end()) {
                        std::cout << "\nCannot find help about command \"" << args[1] << "\".\n\n";
                    } else {
                        std::cout << iter->second;
                    }
                } else {
                    std::cout << "\nFor help about a specific command, type \"help command\".\nAvailable commands:\n";
                    bool first = true;
                    for (const std::pair<const std::string, std::function<void(const std::vector<std::string> &)>> & pair : commands) {
                        if (!first) std::cout << ", ";
                        first = false;
                        std::cout << pair.first;
                    }
                    std::cout << ".\n\n";
                }
            }},
        
            {"exit", [this](const std::vector<std::string> & args) {
                continueRunning = false;
            }},


            {"restart", [this](const std::vector<std::string> & args) {
                continueRunning = false;
                *shouldRestartServer = true;
            }},


            {"save", [this](const std::vector<std::string> & args) {
                std::vector<std::string> files = StaticConfig::getSaves();
                std::string savefile;
                if (files.empty()) {
                    savefile = Misc::readNonEmpty("\nThere are no existing rasc save files.\nPlease enter a name for this save file: ", "Please enter a non-empty filename.\n> ");
                } else {
                    files.push_back("[new save]");
                    size_t fileId = Misc::selectFromOptions(files, "\nExisting rasc save files:\n(Use option " + std::to_string(files.size() - 1) + " to create a new save file)\n", "Please select one, by its number: ");
                    if (fileId == files.size() - 1) {
                        savefile = Misc::readNonEmpty("Please enter a name for this save file: ", "Please enter a non-empty filename.\n> ");
                    } else {
                        savefile = files[fileId];
                    }
                }
                savefile = StaticConfig::getSavefileDirectory() + savefile + StaticConfig::SAVEFILE_EXTENSION;
                std::cout << "\nSaving to " << savefile << " ...\n";
                if (box.server->saveGame(savefile)) {
                    std::cout << "Save successful.\n\n";
                } else {
                    std::cout << "Save failed.\n\n";
                }
            }},


            {"load", [this](const std::vector<std::string> & args) {
                std::vector<std::string> files = StaticConfig::getSaves();
                if (files.empty()) {
                    std::cout << "\nThere are no existing rasc save files.\n\n";
                    return;
                }
                size_t fileId = Misc::selectFromOptions(files, "\nAvailable rasc save files:\n", "Please select one, by its number: ");
                if (box.server->loadGame(StaticConfig::getSavefileDirectory() + files[fileId] + StaticConfig::SAVEFILE_EXTENSION)) {
                    std::cout << "Load successful.\n\n";
                } else {
                    std::cout << "Load failed.\n\n";
                }
            }},

            {"metrics-enable", [this](const std::vector<std::string> & args) {
                // Accessing server internals from here requires lock.
                std::lock_guard<std::recursive_mutex> lock(box.server->getKillableMutex());
                
                // If not done already, create metrics stat, register with stat system,
                // then re-initialise stat system.
                if (box.metricsStat == NULL) {
                    box.metricsStat = new ServerMetricsStat();
                    box.common.statSystem->registerStat(box.metricsStat);
                    box.common.statSystem->initialise(box.common);
                    std::cout << "Enabled server metrics.\n\n";
                } else {
                    std::cout << "Server metrics has already been enabled, doing nothing.\n\n";
                }
            }},
                    
            {"metrics", [this](const std::vector<std::string> & args) {
                // Accessing server internals from here requires lock.
                std::lock_guard<std::recursive_mutex> lock(box.server->getKillableMutex());
                
                if (box.metricsStat == NULL) {
                    std::cout << "Cannot query metrics, metrics are not enabled!\n\n";
                } else {
                    box.metricsStat->consoleQueryMetrics(args);
                }
            }}
        }) {

        if (box.server->listenToPort(port)) {
            std::cout << "Server started successfully.\n\n";
            while(box.server->isRunning() && continueRunning) {
                runServerCommand();
            }
        } else {
            std::cout << "Failed to bind/listen to socket, is it already in use?\n";
        }
    }

    ServerConsole::~ServerConsole(void) {
        std::cout << "Server closing.\n";
    }

    void ServerConsole::runServerCommand(void) {
        // Get server command from console.
        std::cout << "Enter server command:\n";
        std::string command;
        std::getline(std::cin, command);
        
        // Split command into args.
        const std::vector<std::string> args = Misc::splitString(command, ' ');
        
        // Find it in command map, run if possible.
        std::map<std::string, std::function<void(const std::vector<std::string> &)>>::iterator iter = commands.find(args.front());
        if (iter == commands.end()) {
            std::cout << "\nUnknown command \"" << args.front() << "\".\nFor a list of commands, type help.\n\n";
        } else {
            iter->second(args);
        }
    }
}

/*
 * ServerMetricsStat.h
 *
 *  Created on: 20 Jun 2021
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_METRICS_SERVERMETRICSSTAT_H_
#define BASEGAME_SERVER_METRICS_SERVERMETRICSSTAT_H_

#include <unordered_map>
#include <string>
#include <vector>
#include <array>

#include "../../common/stats/Stat.h"

namespace rasc {
    class PlayerData;
    class Province;
    
    /**
     * ServerMetricsStat is a specialised stat, only available on the server, which
     * only exists if explicitly enabled by the server console. Compared to other
     * stats, server metrics stat is a little different:
     *  > Most stats generate all of their internal state from the *current* state
     *    of the map, and are able to re-build it completely when re-initialised.
     *    ServerMetricsStat instead stores a "history" of what players have done
     *    over this run of the server. This means its data is wiped when re-loading
     *    the server.
     *  > Most stats collect data which is either displayed to users in the UI,
     *    or is used by the AI to make informed decisions. ServerMetricsStat is
     *    purely a development tool to compare the overall performance and decisions
     *    made by human players and AI players.
     * 
     * Data collected by ServerMetricsStat:
     *  > todo
     */
    class ServerMetricsStat : public Stat {
    private:
        // ------------ Macro lists/enums ---------------
        
        // Macro list of player metrics,
        // Format: Type, name, column width.
        #define RASC_SERVER_PLAYER_METRICS_LIST    \
            X(UnsignedIntMetric, id, 6)            \
            X(PlayerNameMetric, name, 12)          \
            X(UnsignedIntMetric, provCount, 9)     \
            X(UnsignedIntMetric, maxProvCount, 12) \
            X(UnsignedIntMetric, provGained, 10)   \
            X(UnsignedIntMetric, provLost, 8)      \
            X(CurrencyMetric, totalArmyM, 10)      \
            X(CurrencyMetric, totalBuildC, 11)     \
            X(CurrencyMetric, totalIncome, 11)
        
        // List of metric operation types.
        #define RASC_SERVER_METRICS_OPERATION_TYPES \
            X(query)                                \
            X(show)
        
        // Enum of player metrics.
        #define X(type, name, ...) name,
        class PlayerMetrics { public: enum { RASC_SERVER_PLAYER_METRICS_LIST COUNT }; };
        #undef X
        
        // ---------- Static data -----------------------
        
        // Vector of update types, for registering with rascographics.
        const static std::vector<Stat::StatUpdateType> UPDATE_TYPES;
        
        // Map of player metric name to ID.
        const static std::unordered_map<std::string, unsigned int> METRIC_NAME_TO_ID;
        
        // Array of metric names by ID.
        const static std::string METRIC_ID_TO_NAME[PlayerMetrics::COUNT];
        
        // Array of column widths for each metric.
        const static unsigned int METRIC_COLUMN_WIDTHS[PlayerMetrics::COUNT];
        
        // String listing all available metrics.
        static const char * const AVAILABLE_METRICS_STRING;
        
        // Map of query type name to function pointer (see below).
        const static std::unordered_map<std::string, void(ServerMetricsStat::*)(const std::vector<std::string> &) const> QUERY_FUNCTIONS;
        
        // ---------- Metric types ----------------------
        
        // Base type for metric.
        class Metric {
        public:
            Metric(void);
            virtual std::string toString(void) const = 0;
            virtual bool operator < (const Metric & other) const = 0;
        };
        
        // Metrics for integer types.
        class SignedIntMetric : public Metric {
        public:
            provstat_t value;
            SignedIntMetric(provstat_t value = 0);
            virtual std::string toString(void) const override;
            virtual bool operator < (const Metric & other) const override;
        };
        class UnsignedIntMetric : public Metric {
        public:
            uint64_t value;
            UnsignedIntMetric(uint64_t value = 0);
            virtual std::string toString(void) const override;
            virtual bool operator < (const Metric & other) const override;
        };
        class CurrencyMetric : public Metric {
        public:
            uint64_t value;
            CurrencyMetric(uint64_t value = 0);
            virtual std::string toString(void) const override;
            virtual bool operator < (const Metric & other) const override;
        };
        
        // Keeps a PlayerData, to get player name.
        class PlayerNameMetric : public Metric {
        private:
            PlayerData * data;
        public:
            PlayerNameMetric(PlayerData * data);
            virtual std::string toString(void) const override;
            virtual bool operator < (const Metric & other) const override;
        };
        
        // ----------------------------------------------
        
        // Stores all the metrics data for a player.
        class PlayerMetricData {
        private:
            // Array of member pointers to access below metrics by enum.
            static const Metric PlayerMetricData::* METRICS_MEMBERS[PlayerMetrics::COUNT];
            
            // Define each metric.
            #define X(type, name, ...) type name;
            RASC_SERVER_PLAYER_METRICS_LIST
            #undef X
            
        public:
            PlayerMetricData(PlayerData * data);
            
            void onGainProvince(Province & province);
            void onLoseProvince(Province & province);
            void onChangePlayerStats(const PlayerStatArr & statChange, PlayerStatChangeReason reason);
            
            // Gets metric by ID.
            const Metric & getMetric(unsigned int metricId) const;
        };
        
        // Comparator to compare pointers to metrics data by metric type.
        class MetricComparator {
        private:
            unsigned int metricId;
        public:
            MetricComparator(unsigned int metricId);
            bool operator()(const PlayerMetricData * metricData0, const PlayerMetricData * metricData1) const;
        };
        
        CommonBox * box;
        std::unordered_map<uint64_t, PlayerMetricData> playerMetrics;
        
    public:
        ServerMetricsStat(void);
        virtual ~ServerMetricsStat(void);
        
    private:
        // Helper function: draws table divider for specified metrics.
        void drawDivider(const std::vector<unsigned int> metrics) const;
        
        // Define functions for each metric operation.
        #define X(operationType) \
            void operationType(const std::vector<std::string> & args) const;
        RASC_SERVER_METRICS_OPERATION_TYPES
        #undef X
        
    protected:
        // Misc methods
        virtual void initialise(CommonBox & box) override;
        virtual const std::vector<Stat::StatUpdateType> & getUpdateTypes(void) const override;

        // Stat update methods
        virtual void onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) override;
        virtual void onAddPlayer(uint64_t playerId) override;
        virtual void onRemovePlayer(uint64_t playerId) override;
        virtual void onChangePlayerStats(uint64_t playerId, const PlayerStatArr & statChange, PlayerStatChangeReason reason) override;
        
    public:
        /**
         * Allows interactively exploring metrics via the server console.
         */
        void consoleQueryMetrics(const std::vector<std::string> & args) const;
    };
}

#endif

/*
 * ServerMetricsStat.cpp
 *
 *  Created on: 20 Jun 2021
 *      Author: wilson
 */

#include "ServerMetricsStat.h"

#include <algorithm>
#include <iostream>
#include <cstddef>
#include <cstdlib>
#include <sstream>

#include "../../common/gameMap/mapElement/types/Province.h"
#include "../../common/playerData/PlayerDataSystem.h"
#include "../../common/util/numeric/NumberFormat.h"
#include "../../common/playerData/PlayerData.h"
#include "../../common/gameMap/GameMap.h"
#include "../../common/util/CommonBox.h"

namespace rasc {

    const std::vector<Stat::StatUpdateType> ServerMetricsStat::UPDATE_TYPES = {
        Stat::StatUpdateTypes::changeProvinceOwnership,
        Stat::StatUpdateTypes::addPlayer,
        Stat::StatUpdateTypes::removePlayer,
        Stat::StatUpdateTypes::changePlayerStats
    };
    
    #define X(type, name, ...) { #name, ServerMetricsStat::PlayerMetrics::name },
    const std::unordered_map<std::string, unsigned int> ServerMetricsStat::METRIC_NAME_TO_ID = { RASC_SERVER_PLAYER_METRICS_LIST };
    #undef X
    
    #define X(type, name, ...) #name,
    const std::string ServerMetricsStat::METRIC_ID_TO_NAME[PlayerMetrics::COUNT] = { RASC_SERVER_PLAYER_METRICS_LIST };
    #undef X

    #define X(type, name, columnWidth, ...) columnWidth,
    const unsigned int ServerMetricsStat::METRIC_COLUMN_WIDTHS[PlayerMetrics::COUNT] = { RASC_SERVER_PLAYER_METRICS_LIST };
    #undef X
    
    #define X(type, name, ...) #name ", "
    const char * const ServerMetricsStat::AVAILABLE_METRICS_STRING = "Available metrics: " RASC_SERVER_PLAYER_METRICS_LIST;
    #undef X
    
    #define X(queryType) { #queryType, &ServerMetricsStat::queryType },
    const std::unordered_map<std::string, void(ServerMetricsStat::*)(const std::vector<std::string> &) const> ServerMetricsStat::QUERY_FUNCTIONS = { RASC_SERVER_METRICS_OPERATION_TYPES };
    #undef X

    // ---------- Metric types ----------------------
    
    ServerMetricsStat::Metric::Metric(void) {
    }
    
    ServerMetricsStat::SignedIntMetric::SignedIntMetric(provstat_t value) :
        Metric(),
        value(value) {
    }
    std::string ServerMetricsStat::SignedIntMetric::toString(void) const {
        char buffer[8];
        NumberFormat::genericSigned<sizeof(buffer)>(buffer, value, false);
        return std::string(buffer);
    }
    bool ServerMetricsStat::SignedIntMetric::operator < (const Metric & other) const {
        return value < ((const SignedIntMetric &)other).value;
    }
    
    ServerMetricsStat::UnsignedIntMetric::UnsignedIntMetric(uint64_t value) :
        Metric(),
        value(value) {
    }
    std::string ServerMetricsStat::UnsignedIntMetric::toString(void) const {
        char buffer[8];
        NumberFormat::generic<sizeof(buffer)>(buffer, value);
        return std::string(buffer);
    }
    bool ServerMetricsStat::UnsignedIntMetric::operator < (const Metric & other) const {
        return value < ((const UnsignedIntMetric &)other).value;
    }
    
    ServerMetricsStat::CurrencyMetric::CurrencyMetric(uint64_t value) :
        Metric(),
        value(value) {
    }
    std::string ServerMetricsStat::CurrencyMetric::toString(void) const {
        char buffer[8];
        NumberFormat::currency<sizeof(buffer)>(buffer, value, false);
        return std::string(buffer);
    }
    bool ServerMetricsStat::CurrencyMetric::operator < (const Metric & other) const {
        return value < ((const UnsignedIntMetric &)other).value;
    }
    
    ServerMetricsStat::PlayerNameMetric::PlayerNameMetric(PlayerData * data) :
        Metric(),
        data(data) {
    }
    std::string ServerMetricsStat::PlayerNameMetric::toString(void) const {
        return data->getName();
    }
    bool ServerMetricsStat::PlayerNameMetric::operator < (const Metric & other) const {
        return data->getName() < ((const PlayerNameMetric &)other).data->getName();
    }

    // ---------------- Player metric data ----------
    
    #define X(type, name, ...) (ServerMetricsStat::Metric ServerMetricsStat::PlayerMetricData::*)&ServerMetricsStat::PlayerMetricData::name,
    const ServerMetricsStat::Metric ServerMetricsStat::PlayerMetricData::* ServerMetricsStat::PlayerMetricData::METRICS_MEMBERS[ServerMetricsStat::PlayerMetrics::COUNT] = { RASC_SERVER_PLAYER_METRICS_LIST };
    #undef X
    
    ServerMetricsStat::PlayerMetricData::PlayerMetricData(PlayerData * data) :
        id(data->getId()),
        name(data) {
    }
    
    void ServerMetricsStat::PlayerMetricData::onGainProvince(Province & province) {
        provGained.value++;
        provCount.value++;
        maxProvCount.value = std::max(maxProvCount.value, provCount.value);
    }
    
    void ServerMetricsStat::PlayerMetricData::onLoseProvince(Province & province) {
        provLost.value++;
        provCount.value--;
    }
    
    void ServerMetricsStat::PlayerMetricData::onChangePlayerStats(const PlayerStatArr & statChange, PlayerStatChangeReason reason) {
        switch(reason) {
        case PlayerStatChangeReasons::armyMaintenance:
            totalArmyM.value -= statChange[PlayerStatIds::currency];
            break;
        case PlayerStatChangeReasons::constructBuilding:
            totalBuildC.value -= statChange[PlayerStatIds::currency];
            break;
        case PlayerStatChangeReasons::incomeFromProvinces:
            totalIncome.value += statChange[PlayerStatIds::currency];
            break;
        }
    }
    
    const ServerMetricsStat::Metric & ServerMetricsStat::PlayerMetricData::getMetric(unsigned int metricId) const {
        return this->*METRICS_MEMBERS[metricId];
    }
    
    ServerMetricsStat::MetricComparator::MetricComparator(unsigned int metricId) :
        metricId(metricId) {
    }
    bool ServerMetricsStat::MetricComparator::operator()(const PlayerMetricData * metricData0, const PlayerMetricData * metricData1) const {
        return metricData0->getMetric(metricId) < metricData1->getMetric(metricId);
    }
    
    // -----------------------------------------------------------------------
    
    ServerMetricsStat::ServerMetricsStat(void) :
        box(NULL),
        playerMetrics() {
    }
    
    ServerMetricsStat::~ServerMetricsStat(void) {
    }
    
    void ServerMetricsStat::drawDivider(const std::vector<unsigned int> metrics) const {
        std::cout << "|";
        for (unsigned int metricId : metrics) {
            const unsigned int columnWidth = METRIC_COLUMN_WIDTHS[metricId];
            for (unsigned int i = 0; i < columnWidth; i++) { std::cout << '-'; }
            std::cout << '|';
        }
        std::cout << '\n';
    }
    
    // ---------------------------------- Metric queries ---------------------

    void ServerMetricsStat::query(const std::vector<std::string> & args) const {
        // Give help text when no args.
        if (args.size() <= 2) {
            std::cout << "Format:\nmetrics query metric0, (metric1, ...) (sortBy metric (asc/desc))\n" <<  AVAILABLE_METRICS_STRING << ".\n\n";
            return;
        }
            
        // Index up to in search, whether we found a sort by.
        size_t index = 2;
        bool foundSortBy = false;

        // Build vector of metric IDs to get. Complain if any are nonsense, (except for sortBy).
        std::vector<unsigned int> metricsToGet;
        for (; index < args.size(); index++) {
            const std::string & arg = args[index];
            auto iter = METRIC_NAME_TO_ID.find(arg);
            if (iter == METRIC_NAME_TO_ID.end()) {
                // If "sortBy" is found, increment index to next, set flag and break.
                if (arg == "sortBy") {
                    foundSortBy = true;
                    index++;
                    break;
                // Otherwise this is an invalid metric name, so print list of metric names.
                } else {
                    std::cerr << "Unknown metric \"" << arg << "\".\n" << AVAILABLE_METRICS_STRING << ".\n\n";
                    return;
                }
            } else {
                metricsToGet.push_back(iter->second);
            }
        }

        // We can't get nothing!
        if (metricsToGet.empty()) {
            std::cout << "Cannot query no metrics.\n" << AVAILABLE_METRICS_STRING << ".\n\n";
            return;
        }

        unsigned int sortBy;
        bool isAscending;
        if (foundSortBy) {
            // Complain if nothing follows "sortBy".
            if (index >= args.size()) {
                std::cerr << "\"sortBy\" must be followed by a metric to sort by, rather than nothing.\n" << AVAILABLE_METRICS_STRING << ".\n\n";
                return;
            }
            // Otherwise figure out which metric the user wants to sort by. Complain if that metric cannot be found.
            auto sortByIter = METRIC_NAME_TO_ID.find(args[index]);
            if (sortByIter == METRIC_NAME_TO_ID.end()) {
                std::cerr << "Unknown sortBy metric: \"" << args[index] << "\".\n" << AVAILABLE_METRICS_STRING << ".\n\n";
                return;
            }
            sortBy = sortByIter->second;
            index++;
                
            // Find sort order, defaulting to ascending if no more args.
            if (index >= args.size()) {
                isAscending = true;
            } else if (args[index] == "asc") {
                isAscending = true;
                index++;
            } else if (args[index] == "desc") {
                isAscending = false;
                index++;
            } else {
                std::cerr << "Unknown sort type: \"" << args[index] << "\", available sort types are \"asc\" and \"desc\".\n\n";
                return;
            }
            // Complain if there is junk at the end.
            if (index < args.size()) {
                std::cerr << "Nonsense at the end of metrics query: \"" << args[index] << "\".\n\n";
                return;
            }
        } else {
            // Without specified sort by, sort by player ID in ascending order.
            sortBy = PlayerMetrics::id;
            isAscending = true;
        }

        // Print divider, titles then divider.
        drawDivider(metricsToGet);
        std::cout << '|';
        for (unsigned int metricId : metricsToGet) {
            const std::string & metricName = METRIC_ID_TO_NAME[metricId];
            std::cout << metricName;
            const unsigned int spaceCount = METRIC_COLUMN_WIDTHS[metricId] - std::min(metricName.length(), (size_t)METRIC_COLUMN_WIDTHS[metricId]);
            for (unsigned int i = 0; i < spaceCount; i++) { std::cout << ' '; }
            std::cout << '|';
        }
        std::cout << '\n';
        drawDivider(metricsToGet);

        // Get vector of pointers to all metrics, sort by metric id, then reverse if descending.
        std::vector<const PlayerMetricData *> metricQueryData;
        metricQueryData.reserve(playerMetrics.size());
        for (const std::pair<const uint64_t, PlayerMetricData> & pair : playerMetrics) {
            metricQueryData.push_back(&pair.second);
        }
        std::sort(metricQueryData.begin(), metricQueryData.end(), MetricComparator(sortBy));
        if (!isAscending) std::reverse(metricQueryData.begin(), metricQueryData.end());

        // For each metric data in query, print value in each column.
        for (const PlayerMetricData * metricData : metricQueryData) {
            std::cout << '|';
            for (unsigned int metricId : metricsToGet) {
                const std::string valueStr = metricData->getMetric(metricId).toString();
                const unsigned int spaceCount = METRIC_COLUMN_WIDTHS[metricId] - std::min(valueStr.length(), (size_t)METRIC_COLUMN_WIDTHS[metricId]);
                std::cout << valueStr;                    
                for (unsigned int i = 0; i < spaceCount; i++) { std::cout << ' '; }
                std::cout << '|';
            }
            std::cout << '\n';
        }

        // Divider at the bottom.
        drawDivider(metricsToGet);
        std::cout << '\n';
    }
    
    void ServerMetricsStat::show(const std::vector<std::string> & args) const {
        // Give help text when no args.
        if (args.size() != 3) {
            std::cout << "Format:\nmetrics show playerId.\n\n";
            return;
        }
        
        // Find player from arg, complain if not existing.
        const uint64_t playerId = std::strtoull(args[2].c_str(), NULL, 0);
        auto iter = playerMetrics.find(playerId);
        if (iter == playerMetrics.end()) {
            std::cout << "Cannot find player ID \"" << playerId << "\".\n\n";
            return;
        }
        const PlayerMetricData & metricData = iter->second;
        
        // Print title.
        std::cout << "Showing metrics data for player: "
                  << metricData.getMetric(PlayerMetrics::name).toString() << " (id: "
                  << metricData.getMetric(PlayerMetrics::id).toString() << ")\n";
        
        // Print each metric for that player.
        for (unsigned int metricId = 0; metricId < PlayerMetrics::COUNT; metricId++) {
            std::cout << "  " << METRIC_ID_TO_NAME[metricId] << ": "
                      << metricData.getMetric(metricId).toString() << '\n';
        }
        std::cout << '\n';
    }
    
    // -----------------------------------------------------------------------
    
    void ServerMetricsStat::initialise(CommonBox & box) {
        // Clear data structures.
        playerMetrics.clear();
        
        // Keep a pointer to box.
        this->box = &box;
        
        // Add each player id listed in the player data system.
        for (const std::pair<const uint64_t, RascUpdatable *> pair : box.playerDataSystem->mapChildren()) {
            onAddPlayer(pair.first);
        }
        
        // Add all existing province ownership.
        for (Province * province : box.map->getProvinces()) {
            onChangeProvinceOwnership(*province, PlayerData::NO_PLAYER, province->getProvinceOwner());
        }
    }
    
    const std::vector<Stat::StatUpdateType> & ServerMetricsStat::getUpdateTypes(void) const {
        return UPDATE_TYPES;
    }

    void ServerMetricsStat::onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) {
        if (oldOwner != PlayerData::NO_PLAYER) {
            auto iter = playerMetrics.find(oldOwner);
            if (iter == playerMetrics.end()) {
                std::cerr << "WARNING: ServerMetricsStat: " << "Seemingly unknown player " << oldOwner << " lost a province.\n";
            } else {
                iter->second.onLoseProvince(province);
            }
        }
        if (newOwner != PlayerData::NO_PLAYER) {
            auto iter = playerMetrics.find(newOwner);
            if (iter == playerMetrics.end()) {
                std::cerr << "WARNING: ServerMetricsStat: " << "Seemingly unknown player " << oldOwner << " gained a province.\n";
            } else {
                iter->second.onGainProvince(province);
            }
        }
    }
    
    void ServerMetricsStat::onAddPlayer(uint64_t playerId) {
        // Get player data, check existence.
        PlayerData * data = box->playerDataSystem->idToDataChecked(playerId);
        if (data == NULL) {
            std::cerr << "WARNING: ServerMetricsStat: " << "Non existent player ID " << playerId << " added.\n";
            return;
        }
        // Add player metrics, check success.
        if (!playerMetrics.emplace(playerId, PlayerMetricData(data)).second) {
            std::cerr << "WARNING: ServerMetricsStat: " << "Player id " << playerId << " added when seemingly already existing.\n";
        }
    }
    
    void ServerMetricsStat::onRemovePlayer(uint64_t playerId) {
        auto iter = playerMetrics.find(playerId);
        if (iter == playerMetrics.end()) {
            std::cerr << "WARNING: ServerMetricsStat: " << "Seemingly unknown player " << playerId << " removed.\n";
        } else {
            playerMetrics.erase(iter);
        }
    }
    
    void ServerMetricsStat::onChangePlayerStats(uint64_t playerId, const PlayerStatArr & statChange, PlayerStatChangeReason reason) {
        auto iter = playerMetrics.find(playerId);
        if (iter == playerMetrics.end()) {
            std::cerr << "WARNING: ServerMetricsStat: " << "Unknown player " << playerId << " had stats change.\n";
        } else {
            iter->second.onChangePlayerStats(statChange, reason);
        }
    }
    
    void ServerMetricsStat::consoleQueryMetrics(const std::vector<std::string> & args) const {
        if (args.size() <= 1) {
            // List metric queries when no args.
            #define X(queryType) #queryType ", "
            std::cout << "Available metrics operations: " RASC_SERVER_METRICS_OPERATION_TYPES "\n\n";
            #undef X
        } else {
            // Otherwise run appropriate query function.
            auto iter = QUERY_FUNCTIONS.find(args[1]);
            if (iter == QUERY_FUNCTIONS.end()) {
                std::cout << "Unknown operation type: " << args[1] << ".\n\n";
            } else {
                (this->*(iter->second))(args);
            }
        }
    }
}

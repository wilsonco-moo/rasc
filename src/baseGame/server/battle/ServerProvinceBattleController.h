/*
 * ServerProvinceBattleController.h
 *
 *  Created on: 13 Oct 2019
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_BATTLE_SERVERPROVINCEBATTLECONTROLLER_H_
#define BASEGAME_SERVER_BATTLE_SERVERPROVINCEBATTLECONTROLLER_H_

#include <unordered_map>
#include <vector>

#include "../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "ServerBattleScheduler.h"

namespace rasc {

    class ServerProvinceBattleController : public ProvinceBattleController {
    private:

        // ---------------------- Battle phase control ---------------------------

        /**
         * The 5 battle types. Depending on which one is used, battle phases
         * must happen in different orders.
         * Note: Do not change the order of this, the battle order tables rely on it.
         * Note: This is not actually stored anywhere. It is worked out each phase
         *       from the lists of attackers and defenders.
         */
        class BattleTypes {
        public:
            enum {
                aiAttackHuman,
                humanAttackAi,
                firstAiAttackSecondAi,
                secondAiAttackFirstAi,
                humanAttackHuman,
                siege,
                COUNT
            };
        };
        using BattleType = uint8_t;

        /**
         * This stores, for each time point, for each battle type, the
         * required battle phase.
         */
        const static BattlePhase battlePhaseTable[ServerBattleScheduler::TimePoints::COUNT][BattleTypes::COUNT];

        /**
         * This stores, for each time point, for each battle type, the
         * required next time point at which a battle phase must happen.
         */
        const static ServerBattleScheduler::TimePoint timePointTable[ServerBattleScheduler::TimePoints::COUNT][BattleTypes::COUNT];


        // -----------------------------------------------------------------------

        // For convenience.
        ServerBox & box;

        // This is set by stuff running during a battle phase,
        // if a change which requires a rebalance happens.
        bool battleRebalanceNeeded;

        // These are used during battle phases for temporary storage.
        // They are stored here to avoid excessive unnecessary reallocation.
        std::vector<uint64_t> weCanAttack;
        std::unordered_map<uint64_t, uint64_t> lossMap;

    public:
        ServerProvinceBattleController(Province & province, ServerBox & box);
        virtual ~ServerProvinceBattleController(void);

    private:

        /**
         * Works out the battle type which should currently be used.
         * This is done from the current list of attackers and defenders.
         * If there are not enough attackers and/or defenders for them to
         * attack each other, this will always return a siege type.
         */
        BattleType getBattleType(void) const;

        /**
         * Looks up the next required battle phase, given the specified current
         * time point and battle type.
         */
        BattlePhase getNextBattlePhase(ServerBattleScheduler::TimePointDef timePoint, BattleType battleType) const;

        /**
         * Works out the next time point at which we should run a battle phase,
         * given the specified current time point and battle type.
         */
        ServerBattleScheduler::TimePointDef getNextTimePoint(ServerBattleScheduler::TimePointDef timePoint, BattleType battleType) const;

        // -------------------------- Battle ticks --------------------------

        // All of these methods are called at the appropriate times by performBattleTick.
        void doStrikePhase(simplenetwork::Message & msg, uint64_t referencePlayerAttacker, uint64_t units);
        void doDefensePhase(simplenetwork::Message & msg);
        void doAttackPhase(simplenetwork::Message & msg);
        void doSiegePhase(simplenetwork::Message & msg);

        // ----------------- Internal battle control (utility) ---------------

        /**
         * The specified player will lose the specified number of armies.
         * If this kills their army, they will be removed from the battle.
         *
         * If the army size is smaller than "minSize", it's size decreased by "offset",
         * otherwise it decreased by "percentage" percent.
         */
        void takeLosses(simplenetwork::Message & msg, uint64_t playerId, unsigned int minSize, unsigned int offset, unsigned int percentage);

        /**
         * This version of takeLosses only allows subtracting a plain number of men.
         * Returns the number of losses actually taken. This will be less than losses, if
         * the original army was too small.
         */
        uint64_t takeLossesAbsolute(simplenetwork::Message & msg, uint64_t playerId, uint64_t losses);

        // -------------------------- Battle control ----------------------

        /**
         * Sends a notification to say the battle is starting.
         */
        void sendOnStartBattle(simplenetwork::Message & msg);

        /**
         * Sends a notification to say the battle is ending.
         * This also ensures the removal of any upcoming scheduled phases from the battle scheduler.
         */
        void sendOnEndBattle(simplenetwork::Message & msg);

        /**
         * Notifies that we have done the specified battle phase.
         * This must always be run by performBattleTick or tryToStartBattle after calling
         * one of the battle phase methods.
         * This is done by writing and applying the onBattlePhase misc update.
         */
        void sendOnDoBattlePhase(simplenetwork::Message & msg, BattlePhase phase);

    public:

        /**
         * This must be called each time an army moves into our province.
         * This method assumes that at least one army belonging to the specified attacker ID
         * *does* exist within the province.
         * This method will start a battle if appropriate, otherwise the specified attacker's army
         * will join a battle.
         */
        virtual void onMoveArmyIntoProvince(simplenetwork::Message & msg, uint64_t attackerId, uint64_t unitCount) override;
        virtual void onMoveArmyOutOfProvince(simplenetwork::Message & msg) override;

        /**
         * This is called by ServerBattleScheduler when the time point we scheduled as the
         * next battle tick, is reached.
         */
        void performBattleTick(simplenetwork::Message & msg, ServerBattleScheduler::TimePointDef timePoint);

        /**
         * This re-sends the colour of the main attacker player, if one exists (for the siege display).
         * This should be called whenever the colour of a player who has deployed units in
         * this province changes, if there is a battle ongoing.
         */
        void updateAttackerColour(simplenetwork::Message & msg);

    // -------------------------------- ARMY REBALANCING -------------------------------------

    private:
        void sendUpdateInvolvedPlayers(simplenetwork::Message & msg, const std::vector<uint64_t> & newAttackers, const std::vector<uint64_t> & newDefenders);

    public:
        void rebalanceBattle(simplenetwork::Message & msg, uint64_t referencePlayerAttacker, uint64_t units);
    };
}

#endif

/*
 * ServerProvinceBattleController.cpp
 *
 *  Created on: 13 Oct 2019
 *      Author: wilson
 */

#include "ServerProvinceBattleController.h"

#include <algorithm>
#include <iostream>

#include "../../common/gameMap/mapElement/types/Province.h"
#include "../../common/gameMap/properties/trait/TraitDef.h"
#include "../../common/gameMap/properties/trait/Traits.h"
#include "../../common/gameMap/properties/Properties.h"
#include "../../common/update/TopLevelRascUpdatable.h"
#include "../../common/playerData/PlayerData.h"
#include "../../common/gameMap/units/Army.h"
#include "../../common/util/Misc.h"
#include "../RascBaseGameServer.h"
#include "../ServerBox.h"

namespace rasc {

    // ============================== Battle order tables ============================================

    /**
     * ORDER SUMMARY:
     *                     AI A attacks Human B:   Immediately:              Strike phase
     *                                             Next afterHuman:          Defense phase <<<\
     *                                             Next afterAI:             Attack phase  >>>/
     *
     *                     Human A attacks AI B:   Immediately:              Strike phase
     *                                             Next afterAI:             Defense phase <<<\
     *                                             Next afterHuman:          Attack phase  >>>/
     *
     *  AI A attacks AI B, (A turn then B turn):   Immediately:              Strike phase
     *                                             Next afterAI:             Defense phase <<<\
     *                                             Next betweenAI[attacker]: Attack phase  >>>/
     *
     *  AI A attacks AI B, (B turn then A turn):   Immediately:              Strike phase
     *                                             Next betweenAI[defender]: Defense phase <<<\
     *                                             Next afterAI:             Attack phase  >>>/
     *
     *                  Human A attacks Human B:   Immediately:              Strike phase
     *                                             Next afterHuman:          Defense phase <<<\
     *                                             Next afterAI:             Attack phase  >>>/
     */


    /**
     * Note that battle phases marked with  "INV"  (invalid)
     * are TimePoint/BattleType combinations which should not normally happen, unless a
     * human player involved in a battle has just left/joined, during the battle.
     */
    const ServerProvinceBattleController::BattlePhase ServerProvinceBattleController::battlePhaseTable[ServerBattleScheduler::TimePoints::COUNT][BattleTypes::COUNT] = {

        { // Immediate time point, (always strike phase).
            BattlePhases::strike,  // AI attack Human.
            BattlePhases::strike,  // Human attack AI.
            BattlePhases::strike,  // First AI attack second AI.
            BattlePhases::strike,  // Second AI attack first AI.
            BattlePhases::strike,  // Human attack human.
            BattlePhases::siege    // Siege battle type
        },

        { // Between AI time point
  /* INV */ BattlePhases::attack,  // AI attack Human. (AI replaced by human) (do attack since a defense phase will happen next afterHuman)
  /* INV */ BattlePhases::defense, // Human attack AI. (AI replaced by human) (do defense since an attack phase will happen next afterHuman)
            BattlePhases::attack,  // First AI attack second AI.
            BattlePhases::defense, // Second AI attack first AI.
  /* INV */ BattlePhases::attack,  // Human attack human. (Both AI players replaced by human players) (do attack since a defense phase will happen next afterHuman)
            BattlePhases::siege    // Siege battle type
        },

        { // After AI time point
            BattlePhases::attack,  // AI attack Human.
            BattlePhases::defense, // Human attack AI.
            BattlePhases::defense, // First AI attack second AI.
            BattlePhases::attack,  // Second AI attack first AI.
            BattlePhases::attack,  // Human attack human.
            BattlePhases::siege    // Siege battle type
        },

        { // After human time point
            BattlePhases::defense, // AI attack Human.
            BattlePhases::attack,  // Human attack AI.
  /* INV */ BattlePhases::defense, // First AI attack second AI.  (Human left during battle with AI) (do defense phase since attack phase will happen next betweenAi)
  /* INV */ BattlePhases::attack,  // Second AI attack first AI.  (Human left during battle with AI) (do attack phase since defense phase will happen next betweenAi)
            BattlePhases::defense, // Human attack human.
            BattlePhases::siege    // Siege battle type
        }
    };

    /**
     * Note that battle phases marked with  "INV"  (invalid)
     * are TimePoint/BattleType combinations which should not normally happen, unless a
     * human player involved in a battle has just left/joined, during the battle.
     */
    const ServerBattleScheduler::TimePoint ServerProvinceBattleController::timePointTable[ServerBattleScheduler::TimePoints::COUNT][BattleTypes::COUNT] = {

        { // Immediate time point, will do defense phase next time always.
            ServerBattleScheduler::TimePoints::afterHuman, // AI attack Human.
            ServerBattleScheduler::TimePoints::afterAI,    // Human attack AI.
            ServerBattleScheduler::TimePoints::afterAI,    // First AI attack second AI.
            ServerBattleScheduler::TimePoints::betweenAI,  // Second AI attack first AI.
            ServerBattleScheduler::TimePoints::afterHuman, // Human attack human.
            ServerBattleScheduler::TimePoints::afterHuman  // Siege battle type (siege tick always done afterHuman).
        },

        { // Between AI time point
  /* INV */ ServerBattleScheduler::TimePoints::afterHuman, // AI attack Human. (AI replaced by human) (Go to afterHuman, as that is valid for ai attack human).
  /* INV */ ServerBattleScheduler::TimePoints::afterHuman, // Human attack AI. (AI replaced by human) (Also go to afterHuman. Don't go to afterAI, since that would be a second phase between the two players).
            ServerBattleScheduler::TimePoints::afterAI,    // First AI attack second AI.
            ServerBattleScheduler::TimePoints::afterAI,    // Second AI attack first AI.
  /* INV */ ServerBattleScheduler::TimePoints::afterHuman, // Human attack human. (Both AI players replaced by human players) (Also go to afterHuman, to avoid duplicating a phase).
            ServerBattleScheduler::TimePoints::afterHuman  // Siege battle type (siege tick always done afterHuman).
        },

        { // After AI time point
            ServerBattleScheduler::TimePoints::afterHuman, // AI attack Human.
            ServerBattleScheduler::TimePoints::afterHuman, // Human attack AI.
            ServerBattleScheduler::TimePoints::betweenAI,  // First AI attack second AI.
            ServerBattleScheduler::TimePoints::betweenAI,  // Second AI attack first AI.
            ServerBattleScheduler::TimePoints::afterHuman, // Human attack human.
            ServerBattleScheduler::TimePoints::afterHuman  // Siege battle type (siege tick always done afterHuman).
        },

        { // After human time point
            ServerBattleScheduler::TimePoints::afterAI,    // AI attack Human.
            ServerBattleScheduler::TimePoints::afterAI,    // Human attack AI.
  /* INV */ ServerBattleScheduler::TimePoints::betweenAI,  // First AI attack second AI.  (Human left during battle with AI) (Use betweenAI since that is the next valid phase).
  /* INV */ ServerBattleScheduler::TimePoints::betweenAI,  // Second AI attack first AI.  (Human left during battle with AI) (Use betweenAI since that is the next valid phase).
            ServerBattleScheduler::TimePoints::afterAI,    // Human attack human.
            ServerBattleScheduler::TimePoints::afterHuman  // Siege battle type
        }
    };

    // ================================= Battle order control methods ================================

    ServerProvinceBattleController::BattleType ServerProvinceBattleController::getBattleType(void) const {

        // A battle with either no attackers or no defenders must be a siege.
        if (getAttackers().size() == 0 || getDefenders().size() == 0) {
            return BattleTypes::siege;
        }

        // Use the first attacker and first defender, ask server whether they are human or AI.
        uint64_t attacker = getAttackers()[0],
                 defender = getDefenders()[0];
        RascBaseGameServer & server = *box.server;
        bool attackerHuman = server.isHumanPlayer(attacker),
             defenderHuman = server.isHumanPlayer(defender);

        // Return the appropriate battle type. In the case of AI's attacking each other,
        // ask the server what order their turns happen in.
        if (attackerHuman) {
            if (defenderHuman) {
                return BattleTypes::humanAttackHuman;
            } else {
                return BattleTypes::humanAttackAi;
            }
        } else {
            if (defenderHuman) {
                return BattleTypes::aiAttackHuman;
            } else if (RascBaseGameServer::aiTurnOrder(attacker, defender)) {
                return BattleTypes::firstAiAttackSecondAi;
            } else {
                return BattleTypes::secondAiAttackFirstAi;
            }
        }
    }

    ServerProvinceBattleController::BattlePhase ServerProvinceBattleController::getNextBattlePhase(ServerBattleScheduler::TimePointDef timePoint, BattleType battleType) const {
        // Simply return the appropriate battle phase, from the table.
        return battlePhaseTable[timePoint.timePoint][battleType];
    }

    ServerBattleScheduler::TimePointDef ServerProvinceBattleController::getNextTimePoint(ServerBattleScheduler::TimePointDef timePoint, BattleType battleType) const {
        // Find the appropriate time point from the table.
        ServerBattleScheduler::TimePoint point = timePointTable[timePoint.timePoint][battleType];
        if (point == ServerBattleScheduler::TimePoints::betweenAI) {
            // If it's a between AI time point, then we need to pair it with a player id.
            // Pick whether to use attacker or defender id based on AI turn order.
            if (battleType == BattleTypes::firstAiAttackSecondAi) {
                return {point, getAttackers()[0]}; // Attackers and defenders guaranteed to exist here,
            } else {                               // due to the battle types which produce a betweenAI time point.
                return {point, getDefenders()[0]};
            }
        } else {
            // For any other time point type, a player id is not needed, so use PlayerData::NO_PLAYER.
            return {point, PlayerData::NO_PLAYER};
        }
    }

    // ===============================================================================================











    ServerProvinceBattleController::ServerProvinceBattleController(Province & province, ServerBox & box) :
        ProvinceBattleController(province),
        box(box),
        battleRebalanceNeeded(false),
        weCanAttack(),
        lossMap() {
    }

    ServerProvinceBattleController::~ServerProvinceBattleController(void) {
    }

    // --------------------------------- Battle ticks --------------------------------

    /**
     * Each battle phase, armies deal an amount of damage to opposing armies, equal to
     * the size of their army, divided by FRACTION_DAMAGE_BASE.
     * The max operation is used to make sure that this value is NOT smaller than MIN_DAMAGE_OVERALL.
     */
    #define FRACTION_DAMAGE_BASE 5
    #define MIN_DAMAGE_OVERALL ((uint64_t)1000)

    /**
     * The base damage fraction, and minimum damage, taken during strike phase.
     * The damage done to the defending army in the strike phase must be large enough to avoid
     * the exploit of retreating without damage, but not too large that a large amount of the battle
     * happens in the strike phase.
     */
    #define STRIKE_PHASE_DAMAGE_BASE 7
    #define STRIKE_MIN_DAMAGE ((uint64_t)1000)

    /**
     * The base damage fraction, and minimum damage, that the garrison deals to attacking armies, each siege phase.
     * Garrisons attack with the strength of regular armies. The minimum damage must be larger than the garrison
     * defend minimum damage, in order to make small armies ineffective against garrisons.
     */
    #define GARRISON_ATTACK_DAMAGE_BASE 5
    #define GARRISON_ATTACK_MIN_DAMAGE ((uint64_t)900)

    /**
     * The base damage fraction, and minimum damage, that the attacking armies in a siege deal to the garrison each turn,
     * after they are attacked *by* the garrison.
     * Garrisons should take damage slower than regular armies, because they are sat in a fort.
     * The minimum damage dealt to garrisons must be low, in order to make small armies ineffective against garrisons.
     */
    #define GARRISON_DEFEND_DAMAGE_BASE 8
    #define GARRISON_DEFEND_MIN_DAMAGE ((uint64_t)200)


    void ServerProvinceBattleController::doStrikePhase(simplenetwork::Message & msg, uint64_t rpa, uint64_t units) {
        // rpa = reference player attacker.
        ProvinceArmyController & armyControl = province.armyControl;

        // FIRST, rpa is attacked by existing armies. So count the number of existing opposing units in the province.
        // NOTE: At this point we can assume that all attacker/defenders have EXACTLY ONE army in the province.
        // Also record which players we can attack, for convenience, for use later. Note this will never record us,
        // since canAttackEachOther will never let us attack ourselves.
        uint64_t opposingUnitCount = 0;
        for (uint64_t attackerId : getAttackers()) {
            if (box.server->canAttackEachOther(rpa, attackerId)) {
                opposingUnitCount += armyControl.getArmyOwnedByPlayer(attackerId)->getUnitCount();
                weCanAttack.push_back(attackerId);
            }
        }
        for (uint64_t defenderId : getDefenders()) {
            if (box.server->canAttackEachOther(rpa, defenderId)) {
                opposingUnitCount += armyControl.getArmyOwnedByPlayer(defenderId)->getUnitCount();
                weCanAttack.push_back(defenderId);
            }
        }

        // Take appropriate damage from all the opposing armies. Make sure we can't take more damage than the number of units that just joined,
        // as otherwise, (counterintuitively), other armies owned by us also in the province could be damaged by reinforcing with a small army.
        uint64_t damageToUs = Misc::minmax(opposingUnitCount / STRIKE_PHASE_DAMAGE_BASE, STRIKE_MIN_DAMAGE, units);
        takeLossesAbsolute(msg, rpa, damageToUs);

        // If at this point, all the units we just added have been killed, we cannot deal any opposing damage.
        if (damageToUs >= units) return;

        // Work out how much damage the REMAINING number of units that we added can do to the other armies in this province.
        // Deal that much damage to all other players that we can attack, (we recorded this earlier).
        uint64_t damageFromRemaining = (units - damageToUs) / STRIKE_PHASE_DAMAGE_BASE;
        for (uint64_t opposingPlayerId : weCanAttack) {
            takeLossesAbsolute(msg, opposingPlayerId, damageFromRemaining);
        }
        weCanAttack.clear(); // Always leave this empty at the end.
    }

    void ServerProvinceBattleController::doDefensePhase(simplenetwork::Message & msg) {
        ProvinceArmyController & armyControl = province.armyControl;

        for (uint64_t ourId : getDefenders()) {
            for (uint64_t attackerId : getAttackers()) {
                if (box.server->canAttackEachOther(ourId, attackerId)) {
                    weCanAttack.push_back(attackerId);
                }
            }

            std::list<Army>::const_iterator ourArmy = armyControl.getArmyOwnedByPlayer(ourId);
            uint64_t ourDamage = std::max(
                ourArmy->getUnitCount() / FRACTION_DAMAGE_BASE,
                MIN_DAMAGE_OVERALL
            ) / weCanAttack.size();

            for (uint64_t playerToAttack : weCanAttack) {
                lossMap[playerToAttack] += ourDamage;
            }
            weCanAttack.clear();
        }

        for (const std::pair<const uint64_t, uint64_t> & pair : lossMap) {
            takeLossesAbsolute(msg, pair.first, pair.second);
        }
        lossMap.clear();
    }

    void ServerProvinceBattleController::doAttackPhase(simplenetwork::Message & msg) {
        ProvinceArmyController & armyControl = province.armyControl;

        for (uint64_t ourId : getAttackers()) {
            for (uint64_t attackerId : getAttackers()) {
                if (box.server->canAttackEachOther(ourId, attackerId)) {
                    weCanAttack.push_back(attackerId);
                }
            }
            for (uint64_t defenderId : getDefenders()) {
                if (box.server->canAttackEachOther(ourId, defenderId)) {
                    weCanAttack.push_back(defenderId);
                }
            }

            std::list<Army>::const_iterator ourArmy = armyControl.getArmyOwnedByPlayer(ourId);
            uint64_t ourDamage = std::max(
                ourArmy->getUnitCount() / FRACTION_DAMAGE_BASE,
                MIN_DAMAGE_OVERALL
            ) / weCanAttack.size();

            for (uint64_t playerToAttack : weCanAttack) {
                lossMap[playerToAttack] += ourDamage;
            }
            weCanAttack.clear();
        }

        for (const std::pair<const uint64_t, uint64_t> & pair : lossMap) {
            takeLossesAbsolute(msg, pair.first, pair.second);
        }
        lossMap.clear();
    }



    void ServerProvinceBattleController::doSiegePhase(simplenetwork::Message & msg) {
        ProvinceArmyController & armyControl = province.armyControl;

        // If the province has a garrison, do battle with the garrison.
        // Because this is a siege phase, we can assume that we are allowed to attack all of the attackers.
        provstat_t garrisonSize = GET_GARRISON_SIZE(province.properties);
        if (garrisonSize > 0) {
            // Work out the damage that the garrison deals to the attacking armies, then
            // deal that damage to each attacking player. While doing that, count the number
            // of attacker units left afterwards, (their original count, minus their losses.
            uint64_t garrisonAttackDamage = std::max(GARRISON_ATTACK_MIN_DAMAGE, ((uint64_t)garrisonSize) / GARRISON_ATTACK_DAMAGE_BASE);
            uint64_t attackerUnitCount = 0;
            for (uint64_t attackerId : getAttackers()) {
                attackerUnitCount += armyControl.getArmyOwnedByPlayer(attackerId)->getUnitCount(); // add original unit count.
                attackerUnitCount -= takeLossesAbsolute(msg, attackerId, garrisonAttackDamage);     // subtract losses.
            }

            if (attackerUnitCount > 0) {
                // Work out the damage that (what is left of) the attacking force should does to the garrison, and subtract it from the
                // garrison size. Make sure the size of the garrison never drops below zero.
                provstat_t garrisonDefendDamage = (provstat_t)std::max(GARRISON_DEFEND_MIN_DAMAGE, attackerUnitCount / GARRISON_DEFEND_DAMAGE_BASE);
                province.properties.changeTraitCount(MiscUpdate::server(msg, getRascUpdatableRoot()), Properties::Types::garrison, std::max((provstat_t)0, garrisonSize - garrisonDefendDamage));
            }

        // If the province does not have a garrison, perform normal siege phases.
        } else {
            // Take a count of total losses across all attackers.
            uint64_t totalLosses = 0;
            for (uint64_t playerId : getAttackers()) {
                totalLosses += takeLossesAbsolute(msg, playerId, ProvinceBattleController::SIEGE_PHASE_LOSSES);
            }
            if (totalLosses >= ProvinceBattleController::SIEGE_PHASE_LOSSES &&
                getSiegePhaseCount() >= (ProvinceBattleController::TOTAL_SIEGE_PHASES - 1)) {
                // Claim the province when we get to the end of the siege, but only if
                // on the last siege phase there were enough losses to constitute a full siege phase.
                province.claimByPlayer(MiscUpdate::server(msg, getRascUpdatableRoot()), box.server->getPlayerById(getAttackers()[0]));
                battleRebalanceNeeded = true;
            } else {
                writeUpdateHeader(msg);
                writeMiscUpdateType(msg, MiscUpdateTypes::incrementSiegePhaseCount);
                getRascUpdatableRoot()->receiveMiscUpdate(msg);
            }
        }
    }

    // -------------------------------------------------------------------------------

    void ServerProvinceBattleController::takeLosses(simplenetwork::Message & msg, uint64_t playerId, unsigned int minSize, unsigned int offset, unsigned int percentage) {
        std::list<Army>::const_iterator army = province.armyControl.getArmyOwnedByPlayer(playerId);
        uint64_t unitCount = army->getUnitCount();
        if (unitCount >= minSize) {
            // If the army size is greater than minimum, subtract a percentage.
            province.armyControl.addUnitsNoTurn(MiscUpdate::server(msg, getRascUpdatableRoot()), army->getArmyId(), -((unitCount * percentage)/100));
        } else if (unitCount > offset) {
            // If army size is less than minimum, but greater than the offset imposed, subtract it.
            province.armyControl.addUnitsNoTurn(MiscUpdate::server(msg, getRascUpdatableRoot()), army->getArmyId(), -((int64_t)offset));
        } else {
            // .. Otherwise remove the army from the battle and set battleRebalanceNeeded to true.
            province.armyControl.removeArmy(MiscUpdate::server(msg, getRascUpdatableRoot()), army->getArmyId());
            battleRebalanceNeeded = true;
        }
    }

    uint64_t ServerProvinceBattleController::takeLossesAbsolute(simplenetwork::Message & msg, uint64_t playerId, uint64_t losses) {
        std::list<Army>::const_iterator army = province.armyControl.getArmyOwnedByPlayer(playerId);
        uint64_t unitCount = army->getUnitCount();
        if (unitCount > losses) {
            // If army size is less than minimum, but greater than the offset imposed, subtract it.
            province.armyControl.addUnitsNoTurn(MiscUpdate::server(msg, getRascUpdatableRoot()), army->getArmyId(), -((int64_t)losses));
            // If losses was smaller than the original army, return losses.
            return losses;
        } else {
            // .. Otherwise remove the army from the battle and set battleRebalanceNeeded to true.
            province.armyControl.removeArmy(MiscUpdate::server(msg, getRascUpdatableRoot()), army->getArmyId());
            battleRebalanceNeeded = true;
            // If there are now no units left, return the original number of units.
            return unitCount;
        }
    }

    void ServerProvinceBattleController::sendOnStartBattle(simplenetwork::Message & msg) {
        writeUpdateHeader(msg);
        writeMiscUpdateType(msg, MiscUpdateTypes::startBattle);
        getRascUpdatableRoot()->receiveMiscUpdate(msg);
    }

    void ServerProvinceBattleController::sendOnEndBattle(simplenetwork::Message & msg) {
        writeUpdateHeader(msg);
        writeMiscUpdateType(msg, MiscUpdateTypes::onEndBattle);
        getRascUpdatableRoot()->receiveMiscUpdate(msg);
        box.battleScheduler->removeFromSchedule(province.getId());
    }

    void ServerProvinceBattleController::sendOnDoBattlePhase(simplenetwork::Message & msg, BattlePhase phase) {
        writeUpdateHeader(msg);
        writeMiscUpdateType(msg, MiscUpdateTypes::onBattlePhase);
        msg.write8(phase);
        getRascUpdatableRoot()->receiveMiscUpdate(msg);
    }

    // -------------------------------------------------------------------------------

    void ServerProvinceBattleController::onMoveArmyIntoProvince(simplenetwork::Message & msg, uint64_t attackerId, uint64_t unitCount) {
        rebalanceBattle(msg, attackerId, unitCount);
    }

    void ServerProvinceBattleController::onMoveArmyOutOfProvince(simplenetwork::Message & msg) {
        rebalanceBattle(msg, PlayerData::NO_PLAYER, 0);
    }


    void ServerProvinceBattleController::performBattleTick(simplenetwork::Message & msg, ServerBattleScheduler::TimePointDef timePoint) {

        // If the battle ended before running a battle tick, print a warning and do nothing.
        if (!isBattleOngoing()) {
            std::cerr << "WARNING: ServerProvinceBattleController: Tried to run battle tick for battle which already ended.\n";
            return;
        }

        // Request army merges, since we are about to run a battle phase.
        province.armyControl.mergeAllPlayerArmies(MiscUpdate::server(msg, getRascUpdatableRoot()));

        // Get the battle type, and the phase which needs to happen.
        BattleType battleType = getBattleType();
        BattlePhase nextPhase = getNextBattlePhase(timePoint, battleType);

        // Run the appropriate method for this required phase.
        switch(nextPhase) {
        case BattlePhases::strike:
            std::cerr << "WARNING: ServerProvinceBattleController: Strike phases should NEVER be scheduled to happen later.\n";
            break;
        case BattlePhases::defense:
            doDefensePhase(msg);
            break;
        case BattlePhases::attack:
            doAttackPhase(msg);
            break;
        case BattlePhases::siege:
            doSiegePhase(msg);
            break;
        default:
            std::cerr << "WARNING: Invalid battle phase encountered during ServerProvinceBattleController::performBattleTick.\n";
            break;
        }

        // Notify that we just did a battle phase.
        sendOnDoBattlePhase(msg, nextPhase);

        // If that battle phase requested a rebalance, then rebalance.
        // Rebalancing might have changed the battle type, so recalculate it.
        if (battleRebalanceNeeded) {
            battleRebalanceNeeded = false;
            rebalanceBattle(msg, PlayerData::NO_PLAYER, 0);
            battleType = getBattleType();
        }

        // If after possibly rebalancing, the battle is still ongoing, schedule the next phase.
        if (isBattleOngoing()) {
            box.battleScheduler->addToSchedule(getNextTimePoint(timePoint, battleType), province.getId());
        }
    }

    void ServerProvinceBattleController::updateAttackerColour(simplenetwork::Message & msg) {
        writeUpdateHeader(msg);
        writeMiscUpdateType(msg, MiscUpdateTypes::updateAttackerColour);
        if (getAttackerCount() == 0) {
            msg.write32(0);
        } else {
            msg.write32(box.server->getPlayerById(getAttackers()[0])->getData()->getColourInt());
        }
    }




    // -------------------------------- ARMY REBALANCING -------------------------------------


    void ServerProvinceBattleController::sendUpdateInvolvedPlayers(simplenetwork::Message & msg, const std::vector<uint64_t> & newAttackers, const std::vector<uint64_t> & newDefenders) {
        if (newAttackers != getAttackers() || newDefenders != getDefenders()) {
            writeUpdateHeader(msg);
            writeMiscUpdateType(msg, MiscUpdateTypes::updateInvolvedPlayers);
            msg.write32(newAttackers.size());
            msg.write32(newDefenders.size());
            for (uint64_t attacker : newAttackers) {
                msg.write64(attacker);
            }
            for (uint64_t defender : newDefenders) {
                msg.write64(defender);
            }

            if (newAttackers.size() == 0) {
                msg.write32(0);
            } else {
                msg.write32(box.server->getPlayerById(newAttackers[0])->getData()->getColourInt());
            }

            getRascUpdatableRoot()->receiveMiscUpdate(msg);
        }
    }



    void ServerProvinceBattleController::rebalanceBattle(simplenetwork::Message & msg, uint64_t rpa, uint64_t units) {

        ProvinceArmyController & armyControl = province.armyControl;
        uint64_t rpd; // rpd = Reference player defender,   rpa = reference player attacker.

        // Define a new vector of attackers and defenders, make rpd a defender by default.
        std::vector<uint64_t> newAttackers, newDefenders;

        // Find rpd: Mark battle end if there are no armies.
        // Otherwise set rpd to first defender who owns an army in the province.
        // Otherwise (if none found), use the owner of the first army in province.
        if (armyControl.getArmies().empty()) {
            goto runEndBattle; // [ MARK END BATTLE ]
        } else {
            bool foundRpd = false;
            for (uint64_t defender : getDefenders()) {
                if (armyControl.getArmyOwnedByPlayer(defender) != armyControl.getArmies().cend()) {
                    foundRpd = true;
                    rpd = defender;
                    break;
                }
            }
            if (!foundRpd) {
                rpd = armyControl.getArmies().front().getArmyOwner();
            }
        }
        newDefenders.push_back(rpd);

        // Now go through each army. For each army owner, add them as attacker or defender respectively.
        for (const Army & army : armyControl.getArmies()) {
            uint64_t armyOwner = army.getArmyOwner();

            // Do nothing if this player is already involved.
            if (std::find(newDefenders.begin(), newDefenders.end(), armyOwner) != newDefenders.end() || std::find(newAttackers.begin(), newAttackers.end(), armyOwner) != newAttackers.end()) {
                continue;
            }

            // If not allowed to attack any defenders, add to new defenders. Otherwise add to new attackers.
            bool attackAllowed = false;
            for (uint64_t defender : newDefenders) {
                if (box.server->canAttackEachOther(defender, armyOwner)) {
                    attackAllowed = true;
                    break;
                }
            }
            (attackAllowed ? newAttackers : newDefenders).push_back(armyOwner);
        }

        // If there are no attackers, mark as battle if able to attack province, mark end battle otherwise.
        // If there are some attackers, mark as battle.
        if (newAttackers.empty()) {
            if (box.server->canAttackEachOther(rpd, province.getProvinceOwner())) {
                goto runSiege; // [ MARK AS SIEGE ]
            } else {
                goto runEndBattle; // [ MARK END BATTLE ]
            }
        } else {
            goto runBattle; // [ MARK AS BATTLE ]
        }

        runBattle:
            {
                // Start battle if it is not already ongoing.
                bool haveStartedBattle = !isBattleOngoing();
                if (haveStartedBattle) {
                    sendOnStartBattle(msg);
                }
                // Send an update of involved players.
                sendUpdateInvolvedPlayers(msg, newAttackers, newDefenders);
                // Run strike phase on rpa if present, (player joining battle).
                if (rpa != PlayerData::NO_PLAYER) {
                    // Request army merges, since we are about to run a battle phase.
                    armyControl.mergeAllPlayerArmies(MiscUpdate::server(msg, getRascUpdatableRoot()));
                    // Run the strike phase, and notify that it happened.
                    doStrikePhase(msg, rpa, units);
                    sendOnDoBattlePhase(msg, BattlePhases::strike);
                    // If that battle phase requested a rebalance, then do a rebalance.
                    if (battleRebalanceNeeded) {
                        battleRebalanceNeeded = false;
                        rebalanceBattle(msg, PlayerData::NO_PLAYER, 0);
                    }
                    // If after possibly rebalancing again, the battle is still ongoing, schedule the next phase.
                    // Only schedule the next phase if we *just* started the battle. Otherwise there should
                    // be phases already scheduled.
                    if (isBattleOngoing() && haveStartedBattle) {
                        BattleType battleType = getBattleType();
                        ServerBattleScheduler::TimePointDef currentTimePoint = {ServerBattleScheduler::TimePoints::immediate, PlayerData::NO_PLAYER};
                        box.battleScheduler->addToSchedule(getNextTimePoint(currentTimePoint, battleType), province.getId());
                    }
                }
            }
        return;

        runSiege:
            {
                // Start battle if it is not already ongoing.
                bool haveStartedBattle = !isBattleOngoing();
                if (haveStartedBattle) {
                    sendOnStartBattle(msg);
                }
                // Send an update of involved players.
                sendUpdateInvolvedPlayers(msg, newDefenders, newAttackers); // Note opposite way round: In siege all players are attackers.
                // Don't run any immediate phases - there is no immediate phase for a siege.
                // Schedule the next siege phase if we just started the battle.
                if (haveStartedBattle) {
                    BattleType battleType = getBattleType();
                    ServerBattleScheduler::TimePointDef currentTimePoint = {ServerBattleScheduler::TimePoints::immediate, PlayerData::NO_PLAYER};
                    box.battleScheduler->addToSchedule(getNextTimePoint(currentTimePoint, battleType), province.getId());
                }
            }
        return;

        runEndBattle:
            // End battle if it is ongoing. Do nothing else.
            if (isBattleOngoing()) {
                sendOnEndBattle(msg);
            }
        return;
    }
}

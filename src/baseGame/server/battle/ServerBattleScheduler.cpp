/*
 * ServerBattleScheduler.cpp
 *
 *  Created on: 13 Oct 2019
 *      Author: wilson
 */

#include "ServerBattleScheduler.h"

#include <algorithm>
#include <iostream>

#include "../../common/gameMap/mapElement/types/Province.h"
#include "ServerProvinceBattleController.h"
#include "../gameMap/ServerGameMap.h"
#include "../RascBaseGameServer.h"
#include "../ServerBox.h"

 namespace rasc {

    /**
     * Runs a battle tick, on the specified province ID, using the specified time point.
     * This also checks to see if the provided province actually exists. If it doesn't,
     * nothing is done. This allows us to cope with invalid provinces in the schedule
     * maps. These would result only from a damaged or modified savefile.
     */
    #define DO_BATTLE_TICK(msg, provinceId, timePoint)                                                                                               \
        if (box.map->provinceExists(provinceId)) {                                                                                                   \
            ((ServerProvinceBattleController *)box.map->getProvince(provinceId).battleControl)->performBattleTick((msg), (timePoint));               \
        } else {                                                                                                                                     \
            std::cerr << "WARNING: ServerBattleScheduler: Invalid province ID encountered when trying to run battle tick: " << (provinceId) << "\n"; \
        }

    ServerBattleScheduler::ServerBattleScheduler(ServerBox & box) :
        box(box),
        betweenAiSchedule0(),
        betweenAiSchedule1(),
        betweenAiScheduleRead(&betweenAiSchedule0),
        betweenAiScheduleWrite(&betweenAiSchedule1),
        betweenAiReverse0(),
        betweenAiReverse1(),
        betweenAiReverseRead(&betweenAiReverse0),
        betweenAiReverseWrite(&betweenAiReverse1),
        afterAiSchedule0(),
        afterAiSchedule1(),
        afterAiScheduleRead(&afterAiSchedule0),
        afterAiScheduleWrite(&afterAiSchedule1),
        afterHumanSchedule0(),
        afterHumanSchedule1(),
        afterHumanScheduleRead(&afterHumanSchedule0),
        afterHumanScheduleWrite(&afterHumanSchedule1),
        haveSwappedBetweenAi(false) {
    }

    ServerBattleScheduler::~ServerBattleScheduler(void) {
    }

    void ServerBattleScheduler::addToSchedule(TimePointDef timePoint, uint32_t provinceId) {
        switch(timePoint.timePoint) {
        case TimePoints::immediate:
            std::cerr << "WARNING: Tried to add immediate time point to ServerBattleScheduler: this will do nothing.\n";
            break;
        case TimePoints::betweenAI:
            if (betweenAiReverseWrite->find(provinceId) == betweenAiReverseWrite->end()) {
                (*betweenAiScheduleWrite)[timePoint.aiPlayerId].insert(provinceId);
                (*betweenAiReverseWrite)[provinceId] = timePoint.aiPlayerId;
            } else {
                std::cerr << "WARNING: ServerBattleScheduler: Tried to schedule the same province to run multiple betweenAI phases.\n";
            }
            break;
        case TimePoints::afterAI:
            if (afterAiScheduleWrite->find(provinceId) == afterAiScheduleWrite->end()) {
                afterAiScheduleWrite->insert(provinceId);
            } else {
                std::cerr << "WARNING: ServerBattleScheduler: Tried to schedule the same province to run multiple afterAI phases.\n";
            }
            break;
        case TimePoints::afterHuman:
            if (afterHumanScheduleWrite->find(provinceId) == afterHumanScheduleWrite->end()) {
                afterHumanScheduleWrite->insert(provinceId);
            } else {
                std::cerr << "WARNING: ServerBattleScheduler: Tried to schedule the same province to run multiple afterHuman phases.\n";
            }
            break;
        }
    }

    void ServerBattleScheduler::removeFromSchedule(uint32_t provinceId) {
        {
            std::unordered_map<uint32_t, uint64_t>::iterator iter = betweenAiReverse0.find(provinceId);
            if (iter != betweenAiReverse0.end()) {
                betweenAiSchedule0[iter->second].erase(iter->first);
                betweenAiReverse0.erase(iter);
            }
        }
        {
            std::unordered_map<uint32_t, uint64_t>::iterator iter = betweenAiReverse1.find(provinceId);
            if (iter != betweenAiReverse1.end()) {
                betweenAiSchedule1[iter->second].erase(iter->first);
                betweenAiReverse1.erase(iter);
            }
        }
        afterAiScheduleWrite->erase(provinceId);
        afterHumanScheduleWrite->erase(provinceId);
    }

    void ServerBattleScheduler::onReachTimePoint(simplenetwork::Message & msg, TimePointDef timePoint) {
        switch(timePoint.timePoint) {
        case TimePoints::immediate:
            std::cerr << "WARNING: Called ServerBattleScheduler::onReachTimePoint with immediate time point: this will do nothing.\n";
            break;

        case TimePoints::betweenAI: {

            // Note: Only swap between AI maps on the first call of betweenAI each turn.
            if (!haveSwappedBetweenAi) {
                haveSwappedBetweenAi = true;
                std::swap(betweenAiScheduleRead, betweenAiScheduleWrite);
                std::swap(betweenAiReverseRead, betweenAiReverseWrite);
            }

            auto iter = betweenAiScheduleRead->find(timePoint.aiPlayerId);
            if (iter != betweenAiScheduleRead->end()) {
                // Get the set of province ids, and erase it from the map, BEFORE iterating through
                // it. This allows removeFromSchedule to be used by betweenAI battle phases,
                // without affecting what we are currently iterating through.
                std::unordered_set<uint32_t> provinceIds = std::move(iter->second);
                betweenAiScheduleRead->erase(iter);
                // Go through the province ids associated with this player.
                for (uint32_t provinceId : provinceIds) {
                    // Remove provinceId>>playerId from reverse map. Do this BEFORE running
                    // the betweenAI battle phase.
                    betweenAiReverseRead->erase(provinceId);
                    DO_BATTLE_TICK(msg, provinceId, timePoint)
                }
            }
            break;
        }

        case TimePoints::afterAI:

            // Run the battle tick for any between AI battles which have not been run yet.
            // This is just in case a player has been removed, while a battle is ongoing.
            // Then clear and swap between AI schedules.
            while(!betweenAiScheduleRead->empty()) {
                // Loop while not empty, (i.e: Not a range-based for loop), get first pair,
                // erase it, THEN iterate through it. This allows removeFromSchedule to be used.
                auto iter = betweenAiScheduleRead->begin();
                uint64_t aiPlayerId = iter->first;
                std::unordered_set<uint32_t> provinceIds = std::move(iter->second);
                betweenAiScheduleRead->erase(iter);
                // Run the battle tick for each province id.
                for (uint32_t provinceId : provinceIds) {
                    // Remove provinceId>>playerId from reverse map. Do this BEFORE running
                    // the betweenAI battle phase.
                    betweenAiReverseRead->erase(provinceId);
                    TimePointDef prevTimePoint = {TimePoints::betweenAI, aiPlayerId};
                    DO_BATTLE_TICK(msg, provinceId, prevTimePoint)
                }
            }
            // Note: Neither betweenAiScheduleRead nor betweenAiReverseRead need clearing, since
            //       we erased everything as we went along.
            haveSwappedBetweenAi = false; // Set this to false so betweenAI maps get swapped next time.

            // Next run all afterAI battle ticks, and clear and swap the schedules.
            std::swap(afterAiScheduleRead, afterAiScheduleWrite);
            for (uint32_t provinceId : *afterAiScheduleRead) {
                DO_BATTLE_TICK(msg, provinceId, timePoint)
            }
            afterAiScheduleRead->clear();
            break;

        case TimePoints::afterHuman:
            // Swap schedules, then run all afterHuman battle ticks.
            std::swap(afterHumanScheduleRead, afterHumanScheduleWrite);
            for (uint32_t provinceId : *afterHumanScheduleRead) {
                DO_BATTLE_TICK(msg, provinceId, timePoint)
            }
            afterHumanScheduleRead->clear();
            break;
        }
    }


    // ------------------------------------- RascUpdatable inherited ----------------------------------------

    void ServerBattleScheduler::onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) {
        // Only write any initial data if we are saving, since the client
        // side does not actually have a battle scheduler.
        if (isSaving) {
            writeUpdateHeader(msg);

            // Note: We only have to save write schedules, since the read schedules should contain nothing,
            //       as we can safely assume that no save operations will havppen during the turn change.

            // Write pairs in betweenAI schedule. Since the reverse map stores these pairs more conveniently,
            // iterate through that, (write AI id then province ID for easy reading).
            msg.write64(betweenAiReverseWrite->size());
            for (const std::pair<const uint32_t, uint64_t> & pair : *betweenAiReverseWrite) {
                msg.write64(pair.second);
                msg.write32(pair.first);
            }
            // Write province ids in afterAI schedule.
            msg.write64(afterAiScheduleWrite->size());
            for (uint32_t provinceId : *afterAiScheduleWrite) {
                msg.write32(provinceId);
            }
            // Write province ids in afterHuman schedule.
            msg.write64(afterHumanScheduleWrite->size());
            for (uint32_t provinceId : *afterHumanScheduleWrite) {
                msg.write32(provinceId);
            }
        }
    }

    bool ServerBattleScheduler::onReceiveInitialData(simplenetwork::Message & msg) {

        // Read pairs from betweenAI schedule multimap.
        uint64_t size = msg.read64();
        betweenAiScheduleWrite->clear();
        betweenAiReverseWrite->clear();
        for (uint64_t i = 0; i < size; i++) {
            // Complain and give up if the message is too short.
            if (msg.finishedReading()) {
                std::cerr << "WARNING: ServerBattleScheduler::onReceiveInitialData: Message too short: between AI.\n";
                return false;
            }
            uint64_t aiPlayerId = msg.read64();
            uint32_t provinceId = msg.read32();
            (*betweenAiScheduleWrite)[aiPlayerId].insert(provinceId);
            (*betweenAiReverseWrite)[provinceId] = aiPlayerId;
        }

        // Read province ids from afterAI schedule.
        size = msg.read64();
        afterAiScheduleWrite->clear();
        for (uint64_t i = 0; i < size; i++) {
            // Complain and give up if the message is too short.
            if (msg.finishedReading()) {
                std::cerr << "WARNING: ServerBattleScheduler::onReceiveInitialData: Message too short: after AI.\n";
                return false;
            }
            afterAiScheduleWrite->insert(msg.read32());
        }

        // Read province ids from afterAI schedule.
        size = msg.read64();
        afterHumanScheduleWrite->clear();
        for (uint64_t i = 0; i < size; i++) {
            // Complain and give up if the message is too short.
            if (msg.finishedReading()) {
                std::cerr << "WARNING: ServerBattleScheduler::onReceiveInitialData: Message too short: after human.\n";
                return false;
            }
            afterHumanScheduleWrite->insert(msg.read32());
        }

        return true;
    }
}

/*
 * ServerBattleScheduler.h
 *
 *  Created on: 13 Oct 2019
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_BATTLE_SERVERBATTLESCHEDULER_H_
#define BASEGAME_SERVER_BATTLE_SERVERBATTLESCHEDULER_H_

#include <unordered_map>
#include <unordered_set>

#include "../../common/update/RascUpdatable.h"

namespace simplenetwork {
    class Message;
}

namespace rasc {

    class ServerBox;

    /**
     * ServerBattleScheduler is the class in charge of running battle phases at the right
     * time each turn.
     * Battles which want to be scheduled to run, should use the addToSchedule method, with
     * an appropriate TimePointDef. A phase will then be run for that battle next time
     * ServerBattleScheduler::onReachTimePoint is called with the correct time point.
     * If the battle wants to remain ongoing, it should then schedule itself again.
     */
    class ServerBattleScheduler : public RascUpdatable {
    public:
        // -------------------- Time points ------------------------

        /**
         * This defines a time point within the turn.
         * Battle phases are run at one of these points in time.
         * Note: Do not change the order of this, the battle order tables
         *       in ServerProvinceBattleController rely on it.
         */
        class TimePoints {
        public:
            enum {
                immediate,  // Only for defining when strike phases should run.
                betweenAI,  // After the specified AI player.
                afterAI,    // After all AI players.
                afterHuman, // After all human players.
                COUNT
            };
        };
        using TimePoint = uint8_t;

        /**
         * This represents a TimePoint, with an optional AI player ID, (for betweenAI).
         * For other TimePoints, the AI player ID is not needed.
         */
        class TimePointDef {
        public:
            TimePoint timePoint;
            uint64_t aiPlayerId;
        };

        // ---------------------------------------------------------

    private:
        ServerBox & box;

        // Key: AI player ID, value: province ID.
        std::unordered_map<uint64_t, std::unordered_set<uint32_t>> betweenAiSchedule0, betweenAiSchedule1;
        std::unordered_map<uint64_t, std::unordered_set<uint32_t>> * betweenAiScheduleRead,
                                                                   * betweenAiScheduleWrite;
        std::unordered_map<uint32_t, uint64_t> betweenAiReverse0, betweenAiReverse1;
        std::unordered_map<uint32_t, uint64_t> * betweenAiReverseRead,
                                               * betweenAiReverseWrite;

        std::unordered_set<uint32_t> afterAiSchedule0, afterAiSchedule1;
        std::unordered_set<uint32_t> * afterAiScheduleRead,
                                     * afterAiScheduleWrite;

        std::unordered_set<uint32_t> afterHumanSchedule0, afterHumanSchedule1;
        std::unordered_set<uint32_t> * afterHumanScheduleRead,
                                     * afterHumanScheduleWrite;

        // This stops us swapping the maps for betweenAI every time. This is set to true
        // on the first betweenAI call, (when the maps are swapped), and set to false
        // in afterAI.
        bool haveSwappedBetweenAi;

        // Don't allow copying: We hold raw pointers.
        ServerBattleScheduler(const ServerBattleScheduler & other);
        ServerBattleScheduler & operator = (const ServerBattleScheduler & other);

    public:
        ServerBattleScheduler(ServerBox & box);
        virtual ~ServerBattleScheduler(void);

        /**
         * Schedules a battle tick for the specified province to happen the next time
         * the specified time point is reached.
         * This is an average case constant time operation.
         */
        void addToSchedule(TimePointDef timePoint, uint32_t provinceId);

        /**
         * Ensures that no further battle ticks are called for the specified province
         * ID, at least until it is added back to the schedule.
         * This is an average case constant time operation.
         */
        void removeFromSchedule(uint32_t provinceId);

        /**
         * This must be called each time a time point is reached.
         * This will run all of the scheduled battle ticks for this time point.
         */
        void onReachTimePoint(simplenetwork::Message & msg, TimePointDef timePoint);

        // ---------------- RascUpdatable inherited --------------------------

        virtual void onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) override;
        virtual bool onReceiveInitialData(simplenetwork::Message & msg) override;
    };
}

#endif

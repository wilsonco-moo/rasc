/*
 * RascBaseGameServer.cpp
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#include "RascBaseGameServer.h"

#include <unordered_set>
#include <iostream>
#include <utility>
#include <cstdlib>
#include <chrono>
#include <mutex>


#include <sockets/plus/message/Message.h>
#include <sockets/plus/networkinterface/NetworkInterface.h>
#include <sockets/plus/util/LockedIterable.h>

#include "playerTypes/threatManagedAI/ThreatManagedAI.h"
#include "../common/gameMap/mapElement/types/Province.h"
#include "../common/stats/types/UnitsDeployedStat.h"
#include "../common/playerData/PlayerDataSystem.h"
#include "battle/ServerProvinceBattleController.h"
#include "../common/stats/types/ProvincesStat.h"
#include "../common/stats/types/AdjacencyStat.h"
#include "../common/stats/UnifiedStatSystem.h"
#include "../common/stats/types/AreasStat.h"
#include "../common/playerData/PlayerData.h"
#include "playerTypes/oldAI/PrioritisedAI.h"
#include "../common/util/numeric/MultiCol.h"
#include "../common/util/numeric/Random.h"
#include "battle/ServerBattleScheduler.h"
#include "playerTypes/HumanPlayer.h"
#include "../common/GameMessages.h"
#include "gameMap/ServerGameMap.h"
#include "ServerBox.h"

namespace rasc {


    // Defines which AI player type we are using. This can be changed to create an instance of any
    // AIPlayer.
    #define NEW_AI_PLAYER(box, data) new ThreatManagedAI((box), (data))

    // 2116 is chosen, because it filters out around 1/4 of the closest colour
    // pairs, when picked with MultiCol::perceptUniformRandomColInt.
    const unsigned int RascBaseGameServer::COLOUR_CLOSENESS_THRESHOLD = 2116;


    RascBaseGameServer::RascBaseGameServer(ServerBox & box) :
        players(),
        playersTable(),
        box(box) {

        // Note: No textures are loaded by the mapModeController, as it's
        // loadTextures method is never called. This way it operates in "server mode."
    }

    RascBaseGameServer::~RascBaseGameServer(void) {
        std::cout << "Destroying...\n";
        killableDestroy();
        // Then destroy any remaining (AI) players
        for (std::pair<const uint64_t, Player *> & playerPair : players) {
            delete playerPair.second;
        }

        std::cout << "Done...\n";
    }

    simplenetwork::NetworkInterface * RascBaseGameServer::createInterface(void) {
        return new HumanPlayer(box);
    }





    // ---------------------- TURN STUFF ---------------------------------

    void RascBaseGameServer::tryToMoveToNextTurn(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        std::cout << "--- TURN: Server trying to move to next turn.\n";
        if (players.size() == 0) return;

        bool canMoveToNextTurn = true;
        for (std::pair<const uint64_t, Player *> & playerPair : players) {
            if (!playerPair.second->haveFinishedTurn()) {
                canMoveToNextTurn = false;
                std::cout << "--- TURN: Cannot move to next turn, as player: " << playerPair.second->getData()->getName() << " has not completed their turn.\n";
            }
        }

        if (canMoveToNextTurn) {
            startNextTurn();
        }
    }


    void RascBaseGameServer::startNextTurn(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);

        // Get the current time, so we can figure out how long the server took to do the turn calculation.
        std::chrono::time_point<std::chrono::steady_clock> turnStartTime = std::chrono::steady_clock::now();

        // Define a misc update message. This SINGLE message will be used for EVERYTHING
        // that happens in the turn boundary: AI movement, stat updates of various kinds, battles, etc.
        simplenetwork::Message msg(ServerToClient::miscUpdate);

        // Since it is now the end of the turn, we have reached the afterHuman time point.
        // So notify the battle scheduler.
        box.battleScheduler->onReachTimePoint(msg, {ServerBattleScheduler::TimePoints::afterHuman, PlayerData::NO_PLAYER});

        // ><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
        box.common.turnCount++; // ~~~ The next turn officially starts here ~~~
        // ><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

        std::cout << "--- <><><><><><><><><><> TURN: Moving to next turn: " << box.common.turnCount << ".\n    Processing AI turns...\n";

        // If it's turn one, place some AI players.
        if (box.common.turnCount == 1) {
            placeAIs(box.aisToPlace, msg);
        }

        // TODO: Update province traits and other stats here.

        // First process AI turns.
        std::list<uint64_t> aiPlayersToRemove;
        int aiTurnCount = 0;
        // Call the start turn message on all players which return false from isHumanPlayer().
        for (std::pair<const uint64_t, Player *> & playerPair : players) {
            if (!playerPair.second->isHumanPlayer()) {
                // Give them a turn misc update, IMMEDIATELY BEFORE THEIR TURN, to update their PlayerData stats.
                playerPair.second->getData()->onGenerateTurnMiscUpdate(MiscUpdate::server(msg, &box));
                // Run each AI turn
                if (playerPair.second->startTurn(msg)) {
                    // If they are out of the game, add them to the list of AI players to remove.
                    aiPlayersToRemove.push_back(playerPair.first);
                }
                // We just did an AI turn, so we are at betweenAI time point. Notify the battle scheduler.
                box.battleScheduler->onReachTimePoint(msg, {ServerBattleScheduler::TimePoints::betweenAI, playerPair.first});
                aiTurnCount++;
            }
        }

        // Next, if there are any AI players to remove this turn, remove them and send the updated player list.
        if (!aiPlayersToRemove.empty()) {
            while(!aiPlayersToRemove.empty()) {
                // Remove them from the players map.
                uint64_t toRemove = aiPlayersToRemove.front(); aiPlayersToRemove.pop_front();
                Player * player = playersTable[toRemove];
                std::cout << "AI player, id: " << toRemove << ", has lost all of their land, and will be removed from the game.\n";
                players.erase(toRemove);
                playersTable.erase(toRemove);
                // Free them from memory as well, since a player is dynamically allocated.
                delete player;
                // ... and delete them from the player data system.
                box.common.playerDataSystem->deletePlayerData(MiscUpdate::server(msg, &box), toRemove);
            }
        }

        // All AI turns are done, so we are at afterAI time point. Notify the battle scheduler.
        box.battleScheduler->onReachTimePoint(msg, {ServerBattleScheduler::TimePoints::afterAI, PlayerData::NO_PLAYER});

        // Now all the things which could cause province ownership to change have been done,
        // run the thing to remove adjacent colours which are too similar.
        removeClosePlayerColours(MiscUpdate::server(msg, &box));

        // Now all the AI turns and relevant battle phases are done, update the stats for all human players at once.
        for (std::pair<const uint64_t, Player *> & playerPair : players) {
            if (playerPair.second->isHumanPlayer()) {
                playerPair.second->getData()->onGenerateTurnMiscUpdate(MiscUpdate::server(msg, &box));
            }
        }

        // Now we've done all AI turns and updates, print the time since the turns started.
        // Note that this MUST NOT include the time taken to send stuff to the clients - so do this here.
        std::chrono::time_point<std::chrono::steady_clock> turnEndTime = std::chrono::steady_clock::now();
        std::cout << "Server turn update time: " << std::chrono::duration_cast<std::chrono::milliseconds>(turnEndTime - turnStartTime).count() << " ms.\n";

        // If we have actually written anything to the message, then send it.
        std::cout << "Main turn data broadcast message size: " << (msg.getLength() + 12) << " bytes.\n";
        if (msg.getLength() > 0) {
            for (simplenetwork::NetworkInterface * netIntf : clientsRaw()) {
                netIntf->send(msg);
            }
        }

        // Now start the turn for all human players.
        msg.clear();
        msg.setId(ServerToClient::miscUpdate);
        std::cout << "--- TURN: Notifying human players to start their turns.\n";
        // Again, call startTurn on all players, but this time only on ones that return true from isHumanPlayer().
        for (std::pair<const uint64_t, Player *> & playerPair : players) {
            if (playerPair.second->isHumanPlayer()) {
                playerPair.second->startTurn(msg);
            }
        }
        for (simplenetwork::NetworkInterface * netIntf : clientsRaw()) {
            netIntf->send(msg);
        }
    }


    void RascBaseGameServer::placeAIs(int count, simplenetwork::Message & msg) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);

        // Find all the provinces which are unowned and accessible.
        std::vector<Province *> provinces;
        for (Province * prov : box.map->getProvinces()) {
            if (prov->getProvinceOwner() == PlayerData::NO_PLAYER && prov->isProvinceAccessible()) {
                provinces.push_back(prov);
            }
        }

        for (int playersPlaced = 0; playersPlaced < count; playersPlaced++) {

            // Pick a random province, complain and break the loop if we run out.
            Province * prov;
            if (provinces.empty()) {
                std::cerr << "WARNING: Could only place " << playersPlaced << " out of the requested " << count << " AI players, since there are too few available provinces.\n";
                break;
            } else {
                // Pick a random province, remove it / replace with back.
                const size_t id = box.common.random->sizetRange(0, provinces.size());
                prov = provinces[id];
                if (provinces.size() > 1) provinces[id] = provinces.back();
                provinces.pop_back();
            }

            // Create an AI player and claim the province.
            uint64_t playerId = box.common.playerDataSystem->newAIPlayerData(MiscUpdate::server(msg, &box), box.playerIdAssigner);
            AIPlayer * aiPlayer = NEW_AI_PLAYER(box, box.common.playerDataSystem->idToData(playerId));
            players[playerId] = aiPlayer;
            playersTable[playerId] = aiPlayer;
            prov->claimByPlayer(MiscUpdate::server(msg, &box), aiPlayer);
        }
    }

    void RascBaseGameServer::onAddPlayer(Player * player) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        std::cout << "Adding player...\n";
        uint64_t playerId = player->getData()->getId();
        players[playerId] = player;
        playersTable[playerId] = player;
    }



    void RascBaseGameServer::onRemovePlayer(Player * player) {
        // Lock to the mutex, print a log message.
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        std::cout << "Human player " << player->getData()->getName() << " has left, an AI player will assume control.\n";

        // Get the player's data.
        PlayerData * data = player->getData();

        // Create a new AI player, use it to replace the old player in the players map.
        AIPlayer * aiPlayer = NEW_AI_PLAYER(box, data);
        uint64_t playerId = data->getId();
        players[playerId] = aiPlayer;
        playersTable[playerId] = aiPlayer;

        // Try to move to the next turn when a player leaves.
        tryToMoveToNextTurn();
    }

    // -------------------------- MISC UPDATE ------------------------------------------

    void RascBaseGameServer::onReceiveMiscUpdate(simplenetwork::Message & msg, simplenetwork::NetworkInterface * source) {

        // Get a locked iterable, so we can loop through our clients.
        // Store it in a variable, so all operations in this method are synchronised to our
        // server killableMutex.
        // This makes the entire thing an atomic operation, without locking unnecessary numbers of times.
        simplenetwork::LockedIterable<std::unordered_set<simplenetwork::NetworkInterface *>> iterable = clientsRaw();

        // Now we are synchronised, first we should send the update to all clients.
        // Since this operation is atomic anyway, it does not matter which order we
        // do this in, (Send and update server), but this way means the server sends back the
        // misc update as soon as possible.
        msg.setId(ServerToClient::miscUpdate);

        // Send back to the source player FIRST, to avoid as much latency between the user doing something,
        // and getting a response as possible.
        source->send(msg);

        // Then also send to all players other than the source network interface.
        for (simplenetwork::NetworkInterface * netIntf : iterable) {
            if (netIntf != source) {
                netIntf->send(msg);
            }
        }

        // Finally, the game map needs updating.
        box.receiveMiscUpdate(msg);
    }

    // --------------------------- CHECKED UPDATES ----------------------------------------

    void RascBaseGameServer::onReceiveCheckedUpdate(simplenetwork::Message & msg, HumanPlayer * source) {
        // Lock to make this an atomic operation.
        std::lock_guard<std::recursive_mutex> lock(killableMutex);

        // Create a Message for generating the misc update. The message id does not matter here, as it will be set in onReceiveMiscUpdate().
        simplenetwork::Message miscUpdate;

        // Process the checked update using the top level rasc updatable.
        // Note that this will process the generated misc update after each checked update.
        box.receiveCheckedUpdate(msg, miscUpdate, source->getData());

        // Then treat the generated message as a misc update, (if it isn't empty).
        if (miscUpdate.getLength() > 0) {
            onReceiveMiscUpdate(miscUpdate, source);
        }
    }




    // ------------------------- LOBBY ------------------------------------


    bool RascBaseGameServer::onLobbyProvinceRequest(uint32_t provinceId, HumanPlayer * targetPlayer) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex); // Lock to make this an atomic operation.
        simplenetwork::Message msg(ServerToClient::lobbyProvinceResponse);

        // If the province does not exist, reject the request.
        if (!box.map->provinceExists(provinceId)) {
            msg.writeBool(false);    // Send a false, this means request unsuccessful.
            msg.writeString("Requested province does not exist");
            targetPlayer->send(msg); // Send this to the player
            std::cout << "Lobby province request, for province ID " << provinceId <<", from player " << targetPlayer->getData()->getName() << ":\n    Province does not exist, rejecting request.\n";
            // Return false because we were blocked from choosing this province, as somebody owns it.
            return false;
        }

        // Get the province, old owner, and target player's ID.
        Province & province = box.map->getProvince(provinceId);
        uint64_t oldOwner = province.getProvinceOwner();
        uint64_t targetPlayerId = targetPlayer->getData()->getId();
        std::cout << "Lobby province request, for province " << province.getName() << " (id = " << provinceId <<"), from player " << targetPlayer->getData()->getName() << ":\n";

        // Reject the request immediately if it is not an accessible province.
        if (!province.isProvinceAccessible()) {
            msg.writeBool(false);    // Send a false, this means request unsuccessful.
            msg.writeString("Requested province is not accessible");
            targetPlayer->send(msg); // Send this to the player
            std::cout << "    Province is not accessible, rejecting request.\n";
            // Return false because we were blocked from choosing this province, as somebody owns it.
            return false;
        }

        // IF THE PROVINCE IS NOT OWNED BY ANY PLAYER:
        if (oldOwner == PlayerData::NO_PLAYER) {
            // Claim the province, AND update the player's PlayerData,
            // then send as a misc update. This claims the province, AND provides the player with
            // one turn's worth of manpower/currency.
            {
                simplenetwork::Message miscUpdateMsg(ServerToClient::miscUpdate);
                province.claimByPlayer(MiscUpdate::serverAuto(this, &box), targetPlayer);
                targetPlayer->getData()->onGenerateTurnMiscUpdate(MiscUpdate::server(miscUpdateMsg, &box));
                for (simplenetwork::NetworkInterface * netIntf : clientsRaw()) netIntf->send(miscUpdateMsg);
            }
            // Send a success message
            msg.writeBool(true);     // Send a true, this means request successful.
            msg.writeBool(false);    // Send a false, this means they do not need to change their id and colour.
            targetPlayer->send(msg); // And send the message.
            std::cout << "    Province un-owned, claiming and sending response.\n";
            // Return true because we successfully chose a starting province.
            return true;

        // IF THE PROVINCE IS OWNED BY SOMEONE...
        } else {

            // Get an iterator to the player that owns it.
            std::unordered_map<uint64_t, Player *>::iterator oldPlayerIter = playersTable.find(oldOwner);

            // IF THE PROVINCE IS OWNED BY A HUMAN PLAYER:
            if (oldPlayerIter != playersTable.end() && oldPlayerIter->second->isHumanPlayer()) {

                // Then send a failure message, we cannot take over another human player directly.
                msg.writeBool(false);    // Send a false, this means request unsuccessful.
                msg.writeString("Province owned by another active player");
                targetPlayer->send(msg); // Send this to the player
                std::cout << "    Province owned by another active player, rejecting request.\n";
                // Return false because we were blocked from choosing this province, as somebody owns it.
                return false;

            // IF THE PROVINCE IS OWNED BY A VACANT PLAYER, OR AN AI PLAYER:
            } else {

                // IF THE OWNER PLAYER EXISTS, I.E IS NOT A VACANT PLAYER, (then it must be an AI player)
                if (oldPlayerIter != playersTable.end()) {
                    std::cout << "    Province owned by AI player (id = " << oldPlayerIter->second->getData()->getId() << "), player taking over control, changed player id to: " << targetPlayerId << ".\n";
                    // Then we must remove the AI player from the players map, since we are taking over from them.
                    // Delete the player, pointer since the player is dynamically allocated.
                    delete oldPlayerIter->second;
                    // Then remove them from the map.
                    playersTable.erase(oldPlayerIter);
                    players.erase(oldOwner);
                } else {
                    // If there is no player controlling this land, then print a warning and return. This should never happen.
                    std::cout << "    WARNING: Vacant player found in lobby province request.\n";
                    msg.writeBool(false);    // Send a false, this means request unsuccessful.
                    msg.writeString("Province owned by another vacant player");
                    targetPlayer->send(msg); // Send this to the player
                    // Return false because we were blocked from choosing this province, as a vacant player owns it.
                    return false;
                }

                // Now our target player needs to take over ownership of the province, so:
                // Reassign the player in the players map.
                players.erase(targetPlayerId);
                playersTable.erase(targetPlayerId);
                players[province.getProvinceOwner()] = targetPlayer;
                playersTable[province.getProvinceOwner()] = targetPlayer;

                // Copy the player name to this AI player's player data. This should be done first, or the player
                // name will be inconsistent after they receive the lobbyProvinceResponse.
                box.common.playerDataSystem->idToData(oldOwner)->changeName(MiscUpdate::serverAuto(this, &box), targetPlayer->getData()->getName());

                // Send them a success message. The client will reassign their id here, and take over
                // the player data once used by that AI player.
                msg.writeBool(true); // Send a true, this means request successful
                msg.writeBool(true); // Send a true, this means they need to change their id and colour
                msg.write64(oldOwner); // Then send the new id of our target player
                targetPlayer->send(msg); // Finally send the message to the player

                // Now we can point our local HumanPlayer at this new PlayerData, and delete the old one.
                targetPlayer->setData(box.common.playerDataSystem->idToData(oldOwner));
                box.common.playerDataSystem->deletePlayerData(MiscUpdate::serverAuto(this, &box), targetPlayerId);

                // Return true because we took over this land successfully.
                return true;
            }
        }
    }

    simplenetwork::LockedIterable<std::map<uint64_t, Player *>> RascBaseGameServer::playersRaw(void) {
        killableMutex.lock();
        return simplenetwork::LockedIterable<std::map<uint64_t, Player *>>(&killableMutex, players.begin(), players.end());
    }

    size_t RascBaseGameServer::getPlayerCount(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        return players.size();
    }

    void RascBaseGameServer::changePlayerColour(MiscUpdate update, uint64_t playerId, uint32_t newColour) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);

        // Find the player data, complain if it doesn't exist.
        PlayerData * data = box.common.playerDataSystem->idToDataChecked(playerId);
        if (data == NULL) {
            std::cerr << "WARNING: RascBaseGameServer: Colour change request for player ID " << playerId << ", which does not exist.\n";
            return;
        }

        // Go through each of the provinces owned by that player, change their colour.
        const ProvincesStatInfo & info = box.common.provincesStat->getOwnershipInfo(playerId);
        for (Province * province : info.getOwnedProvinces().vec()) {
            province->changeColour(MiscUpdate::server(update.msg(), &box), newColour);
        }

        // Using UnitsDeployedStat, go through all the provinces in which they have deployed armies
        // and change the colours of those armies.
        FOR_EACH_DEPL_PROV(box.common, playerId, province)
            for (const Army & army : province.armyControl.getArmies()) {
                if (army.getArmyOwner() == playerId) {
                    // It is safe to apply this while iterating since this misc update
                    // will never add or remove armies.
                    province.armyControl.changeArmyColour(MiscUpdate::server(update.msg(), &box), army.getArmyId(), newColour);
                }
            }
        FOR_EACH_DEPL_PROV_END

        // Change the colour of the PlayerData instance itself. When this is applied,
        // it will notify the stat system.
        data->changeColour(MiscUpdate::server(update.msg(), &box), newColour);

        // Go through each province in which they have deployed units again (using UnitsDeployedStat)...
        FOR_EACH_DEPL_PROV(box.common, playerId, province)
            // Make sure that if there is a battle ongoing, the battle controller re-sends it's main attacker colour.
            // This can only be done AFTER the update has been applied to the PlayerData, since the battle
            // controller will lookup the player colour in the player data system.
            if (province.battleControl->isBattleOngoing()) {
                ((ServerProvinceBattleController *)province.battleControl)->updateAttackerColour(update.msg());
            }
        FOR_EACH_DEPL_PROV_END
    }

    void RascBaseGameServer::removeClosePlayerColours(MiscUpdate update) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        const std::chrono::steady_clock::time_point startTime = std::chrono::steady_clock::now();
        
        // Don't change the colour of more players than exist (x2). Must be a limit otherwise
        // we'll loop forever in cases which cannot be resolved!
        const size_t maxChanges = box.common.playerDataSystem->getRascUpdatableChildCount() * 2;
        // Colour cache size doesn't need to increase with map size: It just needs to be large
        // enough to accommodate a reasonable selection of colours to pick per player.
        constexpr size_t maxCachedColours = 2000;
        // Difference threshold above which we early out of colour selection. Must be a bit bigger
        // than COLOUR_CLOSENESS_THRESHOLD, otherwise you get too many locally similar colours,
        // making future colour pairs harder to resolve!
        constexpr unsigned int minEarlyOutThreshold = (3 * COLOUR_CLOSENESS_THRESHOLD) / 2;
        // Maximum number of colour choices to try per player. Too small and colour choices are
        // too poor (we end up failing to resolve colours and actually trying more combinations),
        // too large and it takes too long!
        constexpr unsigned int maxChoicesPerPlayer = 70;
        
        // Cache colours as picking random colours is fairly expensive, looking up previously
        // picked colours is about twice as fast.
        // Also here is a vector to cache adjacent colours (allocation is re-used each cycle).
        std::vector<MultiCol> cachedColours, cachedAdjacentColours;
        
        // Ask adjacency stat which colour-blindness mode is in use currently.
        const unsigned int colourBlindMode = box.common.adjacencyStat->getColourBlindMode();
        // Count the total number of colour choices (for logging).
        unsigned int totalColourChoiceCount = 0;
        
        // Only loop while there are adjacent players: Otherwise there is nothing for us to do.
        size_t count = 0;
        while(!box.common.adjacencyStat->adjacentEmpty()) {

            // Find the pair of adjacent players with the closest colours.
            const AdjacencyStat::PlayerColourPair & closest = box.common.adjacencyStat->getClosestPlayerColourPair();

            // If their colours are further apart than the threshold, then there are no
            // more close colours on the map, so we can finish.
            if (closest.getColourDiff() > COLOUR_CLOSENESS_THRESHOLD) break;

            // If we change the colours of too many players, give up.
            if (count > maxChanges) {
                std::cout << "WARNING: Failed to remove similar adjacent players, even after changing the colours of " << count << " players.\n";
                break;
            }
            count++;
            
            // Figure out which player's colour to swap.
            // Get the Player and adjacency info for each.
            unsigned int chosenPlayer;
            const std::array<const Player *, 2> players = {
                getPlayerById(closest.getFirstPlayerId()),
                getPlayerById(closest.getSecondPlayerId())
            };
            const std::array<const AdjacencyStat::PlayerAdjacencyInfo *, 2> adjacencyInfos = {
                &box.common.adjacencyStat->getPlayerAdjacencyInfo(closest.getFirstPlayerId()),
                &box.common.adjacencyStat->getPlayerAdjacencyInfo(closest.getSecondPlayerId())
            };
            
            const bool inFirstHalf = (count * 2u < maxChanges);
            const bool inFirstQuarter = (count * 4u < maxChanges);
            // In first half, bias towards changing the colour of a non-human player
            // if possible. If we've changed colours lots already, don't bother: Avoid cases
            // where *not* changing a human player's colour makes resolving colours impossible.
            if (inFirstHalf && players[0]->isHumanPlayer() && !players[1]->isHumanPlayer()) {
                chosenPlayer = 1;
            } else if (inFirstHalf && !players[0]->isHumanPlayer() && players[1]->isHumanPlayer()) {
                chosenPlayer = 0;
            // Otherwise, bias towards whichever player has fewest other adjacent players. (For large
            // nations bordering lots of little ones, we want the little ones to change away from
            // the colour of the large one next to them - changing the large nation will create more
            // colour collisions!). Only do this at the start, to avoid cases where *not* changing
            // a less adjacent player makes resolving colours impossible.
            } else if (inFirstQuarter && adjacencyInfos[0]->getAdjacentPlayerCount() > adjacencyInfos[1]->getAdjacentPlayerCount()) {
                chosenPlayer = 1;
            } else if (inFirstQuarter && adjacencyInfos[0]->getAdjacentPlayerCount() < adjacencyInfos[1]->getAdjacentPlayerCount()) {
                chosenPlayer = 0;
            // Otherwise, pick at random between them. Usually both players are AI at this point.
            } else {
                chosenPlayer = box.common.random->uintRange(0, 2);
            }
            
            // Now we chose the player to change colours of, grab the adjacency info and player id.
            const AdjacencyStat::PlayerAdjacencyInfo & chosenAdjacencyInfo = *adjacencyInfos[chosenPlayer];
            const uint64_t chosenPlayerId = (chosenPlayer == 0 ? closest.getFirstPlayerId() : closest.getSecondPlayerId());
            
            // Cache the colours of all adjacent colours around us, so we can easily
            // iterate them below. Iterating the "borders per player" map and looking up the
            // lab colour in player data, takes about twice as long as iterating a vector
            // (which we do a *lot* below, so this is worth the cost).
            for (const std::pair<const uint64_t, size_t> & borders : chosenAdjacencyInfo.getBordersPerPlayer()) {
                cachedAdjacentColours.push_back(getPlayerById(borders.first)->getData()->getColourLab());
            }
            
            // Pick several colours, pick whichever one is furthest away from our surrounding players' colours.
            // A maximum of maxChoicesPerPlayer colours are tried.
            MultiCol furthestColourLab;
            unsigned int furthestDistance = 0;
            unsigned int colourChoiceId = 0;
            for (; colourChoiceId < maxChoicesPerPlayer; colourChoiceId++) {
                
                // Pick a random colour
                MultiCol randomColourLab;
                const size_t colourCacheId = box.common.random->sizetRange(0, maxCachedColours);
                if (colourCacheId < cachedColours.size()) {
                    randomColourLab = cachedColours[colourCacheId];
                } else {
                    randomColourLab = MultiCol::randomLabColourWithValidRGB(*box.common.random);
                    cachedColours.push_back(randomColourLab);
                }
                
                // Find the minimum distance from it, to the colours of all surrounding adjacent players.
                unsigned int foundDistance = std::numeric_limits<unsigned int>::max();
                for (const MultiCol & adjacentColour : cachedAdjacentColours) {
                    foundDistance = std::min(foundDistance, adjacentColour.distanceSquared(randomColourLab, colourBlindMode));
                }
                
                // If found distance is larger than one we've found so far, use it.
                // Note: Use >= so the first one is always picked even if distance is zero!
                if (foundDistance >= furthestDistance) {
                    furthestColourLab = randomColourLab;
                    furthestDistance = foundDistance;
                    
                    // Early out if we find something further away than the threshold.
                    // There's no point trying to pick more colours now, we meet the minimum
                    // requirements! Otherwise, (near) optimally picking furthest colours
                    // results in choices right at the edge of the colour space! You get a
                    // lot of similar pure yellows, blues etc, for players one-but adjacent.
                    if (furthestDistance > minEarlyOutThreshold) {
                        break;
                    }
                }
            }
            totalColourChoiceCount += colourChoiceId;
            
            // Clear cached adjacent colours now we're done with them.
            cachedAdjacentColours.clear();
            
            // We've ended up picking a colour which is even closer or no better than the current colour pair!
            // Just give up, there's no point making the colour difference worse!
            if (furthestDistance <= closest.getColourDiff()) {
                continue;
            }
            
            MultiCol furthestColourRgb = furthestColourLab;
            furthestColourRgb.labToXyz();
            furthestColourRgb.xyzToRgb();
            
            // Change their colour to one which is randomly picked.
            // Make sure it is a perceptually uniform randomly picked colour.
            changePlayerColour(MiscUpdate::server(update.msg(), &box), chosenPlayerId, furthestColourRgb.toIntRGBA());
        }

        if (count > 0) {
            const std::chrono::steady_clock::time_point finishTime = std::chrono::steady_clock::now();
            const std::chrono::milliseconds timeSpent = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime - startTime);            
            std::cout << "Changed the colour of " << count << " players in " << timeSpent.count() << " ms, to avoid similar adjacent colours. On average, we tried " << (totalColourChoiceCount / count) << " colour combinations per player.\n";
        }
    }

    bool RascBaseGameServer::doesPlayerExist(uint64_t playerId) const {
        return playersTable.find(playerId) != playersTable.end();
    }

    bool RascBaseGameServer::isHumanPlayer(uint64_t playerId) const {
        std::unordered_map<uint64_t, Player *>::const_iterator iter = playersTable.find(playerId);
        if (iter == playersTable.end()) {
            return false;
        } else {
            return iter->second->isHumanPlayer();
        }
    }

    bool RascBaseGameServer::canAttackCxt(void * context, uint64_t firstPlayerId, uint64_t secondPlayerId) {
        return firstPlayerId != secondPlayerId;
    }

    bool RascBaseGameServer::loadGame(const std::string & filename) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);

        // First, if we have any active human or AI players, we cannot load a game, since we are already in one.
        if (!players.empty()) {
            std::cout << "ERROR: Loading can only be done with a cleanly started server,\n       with no active human or AI players. Try restarting the server.\n";
            return false;
        }

        // Next actually do the loading, give up if this fails.
        simplenetwork::Message msg;
        if (!msg.load(filename)) {
            return false;
        }

        // Now everything *should* be reset to default state, so apply the savegame to the box.
        // If that fails for whatever reason, return false.
        if (!box.receiveInitialData(msg)) {
            return false;
        }

        // Finally, re-create the AI players from the player data.
        for (std::pair<uint64_t, RascUpdatable *> updatable : box.common.playerDataSystem->mapChildren()) {
            PlayerData * data = (PlayerData *)updatable.second;
            AIPlayer * player = NEW_AI_PLAYER(box, data);
            players[updatable.first] = player;
            playersTable[updatable.first] = player;
        }

        // Once all game data and players are loaded, initialise the stat system,
        // since we just received a load if initial data.
        box.common.statSystem->initialise(box.common);

        std::cout << "Loaded save from " << filename << ", there are now " << players.size() << " players.\n";
        return true;
    }

    bool RascBaseGameServer::saveGame(const std::string & filename) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        simplenetwork::Message msg;
        box.generateInitialData(msg, true);
        return msg.save(filename);
    }
}

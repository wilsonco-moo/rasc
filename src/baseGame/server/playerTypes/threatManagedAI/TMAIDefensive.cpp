/*
 * TMAIDefensive.cpp
 *
 *  Created on: 3 Dec 2019
 *      Author: wilson
 *
 * This contains the implementation for AI phases which carry out
 * defensive AI behaviour, within the ThreatManagedAI class.
 * See ThreatManagedAI.h.
 */

#include "ThreatManagedAI.h"

#include <map>

#include "../../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../../common/gameMap/mapElement/types/Province.h"
#include "../../../common/gameMap/properties/trait/Traits.h"
#include "../../../common/gameMap/properties/PropertySet.h"
#include "../../../common/gameMap/properties/Properties.h"
#include "../../../common/stats/types/ProvincesStat.h"
#include "../../../common/playerData/PlayerData.h"
#include "../../../common/gameMap/GameMap.h"
#include "../../../common/util/Misc.h"
#include "../../RascBaseGameServer.h"
#include "../../ServerBox.h"

namespace rasc {

    void ThreatManagedAI::doDefensiveArmyMovement(simplenetwork::Message & msg) {

        // Build a vector of army IDs to move, instead of moving them right away.
        // We can't move armies while we are iterating through them.
        std::vector<uint64_t> armiesToMove;
        uint64_t ourId = getData()->getId();

        // For each army in a centre province, which is owned by us and not moved
        // yet this turn...
        for (Province * province : info->getCentreProvinces().vec()) {
            for (const Army & army : province->armyControl.getArmies()) {
                if (army.getArmyOwner() == ourId && army.getTurnLastMoved() < box.common.turnCount) {
                    // ... add it to the list of armies to move.
                    armiesToMove.push_back(army.getArmyId());
                }
            }
            // If there are any friendly armies in this centre province, move them
            // to the most threatened adjacent province, if necessary.
            if (!armiesToMove.empty()) {
                Province * moveTo = findMostThreatenedAdjacentFromCentre(*province);
                if (moveTo != NULL) {
                    for (uint64_t armyId : armiesToMove) {
                        moveTo->armyControl.moveArmy(MiscUpdate::server(msg, &box), province->getId(), armyId);
                    }
                }
                armiesToMove.clear();
            }
        }

        // For each army in an edge province, which is owned by us and not moved
        // yet this turn...
        for (Province * province : info->getEdgeProvinces().vec()) {
            for (const Army & army : province->armyControl.getArmies()) {
                if (army.getArmyOwner() == ourId && army.getTurnLastMoved() < box.common.turnCount) {
                    // ... add it to the list of armies to move.
                    armiesToMove.push_back(army.getArmyId());
                }
            }
            // If there are any friendly armies in this edge province, move them to the most
            // threatened adjacent province with an ongoing battle, if necessary.
            if (!armiesToMove.empty()) {
                Province * moveTo = findBattleToReinforceFromEdge(*province);
                if (moveTo != NULL) {
                    for (uint64_t armyId : armiesToMove) {
                        moveTo->armyControl.moveArmy(MiscUpdate::server(msg, &box), province->getId(), armyId);
                    }
                }
                armiesToMove.clear();
            }
        }
    }

    void ThreatManagedAI::retreatBattlesIntoGarrisons(simplenetwork::Message & msg) {
        const ThreatManagedWeights & weights = getData()->getWeights();
        uint64_t ourId = getData()->getId();

        // Iterate through each province with a fort, where there is an ongoing battle.
        for (Province * province : info->getOwnedProvinces().vec()) {
            if (province->properties.getPropertySet().hasPropertiesOfType(Properties::Types::fort) &&
                province->battleControl->isBattleOngoing()) {
                // There is only something to do, if we have some units in the province.
                uint64_t ownedCount = province->armyControl.countUnitsOwnedBy(ourId);
                if (ownedCount > 0) {
                    // Count friendly and enemy units in province.
                    uint64_t totalFriendly, totalEnemy;
                    province->armyControl.countUnitsFriendlyEnemy(ourId, &totalFriendly, &totalEnemy, &box.server, &RascBaseGameServer::canAttackCxt);
                    // If the total number of friendly units, is less than the enemy units plus battle
                    // safety margin, then absorb all our armies into the garrison.
                    if (totalFriendly < (totalEnemy * weights.garrisonRetreatBattleSafety()) / 100) {
                        absorbArmiesIntoGarrison(msg, *province);
                    }
                }
            }
        }
    }

    uint64_t ThreatManagedAI::doDefensiveArmyPlacement(simplenetwork::Message & msg, uint64_t maxPlaceable) {
        const ThreatManagedWeights & weights = getData()->getWeights();

        // Build a sorted map of the most threatened provinces.
        // Ignore any provinces with negative or zero threat.
        std::multimap<provvalue_t, Province *, std::greater<provvalue_t>> provincesByThreat;
        for (Province * province : info->getEdgeProvinces().vec()) {
            provvalue_t threat = getProvinceThreatBalance(*province);
            if (threat > 0) provincesByThreat.emplace(threat, province);
        }

        // We can't place units if there are no provinces with positive threat.
        if (provincesByThreat.empty()) return 0;

        // Work out how many provinces are the "threatPercentageUnitPlacement" percent most
        // threatened. Do this from total edge province number, make sure it is at least one,
        // and make sure it is not more than the length of provincesByThreat.
        // NOTE: This is the same calculation as is used for attack placement in doAttackArmyPlacement.
        unsigned int mostThreatenedCount = (info->getEdgeProvinces().size() * weights.threatPercentageUnitPlacement()) / 100;
        mostThreatenedCount = Misc::minmax(mostThreatenedCount, 1u, (unsigned int)provincesByThreat.size());

        // Work out the total threat in the most threatened provinces.
        provstat_t totalThreat = 0;
        auto iter = provincesByThreat.begin();
        for (unsigned int i = 0; i < mostThreatenedCount; i++) {
            totalThreat += iter->first;
            ++iter;
        }

        // From this threat, work out the number of men we should place.
        // Seperately work out the number we *actually* placed. This will be slightly
        // different because of rounding.
        uint64_t defensiveToPlace = getDefensivePlaceable(maxPlaceable, totalThreat),
                 defensivePlaced = 0;

        // Place units in these most threatened provinces, proportionally by the amount of threat.
        iter = provincesByThreat.begin();
        for (int unsigned i = 0; i < mostThreatenedCount; i++) {
            uint64_t toPlace = (iter->first * defensiveToPlace) / totalThreat;
            defensivePlaced += placeUnitsAvoidingBattle(msg, *iter->second, toPlace, true);
            ++iter;
        }

        // Return the number of units we *actually* placed in this phase.
        return defensivePlaced;
    }
}

/*
 * ThreatManagedAI.h
 *
 *  Created on: 2 Dec 2019
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_PLAYERTYPES_THREATMANAGEDAI_THREATMANAGEDAI_H_
#define BASEGAME_SERVER_PLAYERTYPES_THREATMANAGEDAI_THREATMANAGEDAI_H_

#include <utility>
#include <vector>

#include "../../../common/gameMap/provinceUtil/ProvinceStatTypes.h"
#include "../../../common/playerData/info/ThreatManagedWeights.h"
#include "../AIPlayer.h"

namespace rasc {

    class ProvincesStatInfo;
    class PlayerStatArr;
    class Province;
    class Army;

    /**
     * This class is implemented across the following source files:
     *  > TMAICore.cpp:
     *      Contains constructor and destructor, and other core functionality,
     *      like the aiTurn methods themselves.
     *  > TMAIUtility.cpp:
     *      Contains general purpose utility methods for use elsewhere within
     *      this class.
     *  > TMAIThreat.cpp:
     *      Contains methods for evaluating the threat of provinces.
     *      This is used for defensive placement and movement.
     *  > TMAIValue.cpp:
     *      Contains methods for evaluating the value of provinces.
     *      This is used for attack placement and movement.
     *  > TMAIUnitPlacement.cpp
     *      Includes methods for dealing with unit placement, for
     *      both attack and defensive behaviour.
     *  > TMAIDefensive.cpp
     *      Contains implementation for AI phases which carry out defensive AI behaviour.
     *  > TMAIAttack.cpp
     *      Contains implementation for AI phases which carry out attacking AI behaviour.
     */
    class ThreatManagedAI : public AIPlayer {
    private:
        // Keep a copy of our provinces stat info, update this at the start of our turn.
        // This is for convenience, so we don't have to look this up in almost every method.
        const ProvincesStatInfo * info;

    public:
        ThreatManagedAI(ServerBox & box, PlayerData * data);
        virtual ~ThreatManagedAI(void);

    protected:
        /**
         * The AI turn. Note that this is core functionality, so is implemented
         * in TMAICore.cpp.
         */
        virtual void aiTurn(simplenetwork::Message & msg) override;

    private:

        // ====================== General utility methods (TMAIUtility.cpp) =========================

        /**
         * Fills the specified vector with a set of pairs of province and army, representing the
         * armies which we own, within enemy land, which we can move this turn.
         * This is useful because we cannot move armies while iterating through them.
         */
        void findMoveableArmiesInEnemyLand(std::vector<std::pair<Province *, const Army *>> & vec);

        // ===================== Control of buildings (TMAIBuilding.cpp) ============================

        /**
         * AI Phase 1:
         * Performs the building placement, management and removal.
         * This first does fort removal, then either does fort placement, or improvement
         * placement, depending on the maximum fort desirability.
         * Returns the number of units placed. We can place units here, if a fort is placed
         * without standing armies in the province. Returns placed units.
         * limitedByManpower should be set to whether we are limited by manpower or currency.
         */
        uint64_t doBuildingManagementPhase(simplenetwork::Message & msg, uint64_t maxPlaceable, bool limitedByManpower);

        /**
         * Progressively removes forts which have become "obsolete".
         * These are forts (and associated garrisons), in centre provinces which make a
         * profit of less than fortRemovalCurrencyProfit, (with probability 1 in fortRemovalCurrencyProfit).
         * Currently, removing a fort costs *the same* as building a new fort.
         *
         * Called by doBuildingManagementPhase.
         */
        void doFortRemoval(simplenetwork::Message & msg);

        /**
         * If we have enough cash, places a fort in specified province, and
         * takes care of garrisons appropriately. Returns true if we did actually place a fort there.
         * We can place units here, if a fort is placed without standing armies in
         * the province. Returns placed units.
         * Sets placedFort to true if we placed a fort, or false otherwise.
         *
         * Called by doBuildingManagementPhase.
         */
        uint64_t doFortPlacement(simplenetwork::Message & msg, Province & province, bool & placedFort, uint64_t maxPlaceable);

        /**
         * Tries to demolish buildings, in order to free up enough available land for
         * construction of a fort.
         * If we cannot affort to do the demolition (plus the specified reserve cost also),
         * then this fails, false is returned and nothing is done.
         * If it is not possible to free up enough land, false is returned and nothing is done.
         * Otherwise, true is returned on success.
         */
        bool tryDemolishToMakeWayForFort(simplenetwork::Message & msg, Province & province, const PlayerStatArr & reserveCost);

        /**
         * Constructs as many buildings as are appropriate to build, given our current situation.
         * This should be called each turn by doBuildingManagementPhase, when there is not enough
         * desirability to build forts.
         * limitedByManpower should be set to whether our unit placement is limited by manpower or currency.
         */
        void doImprovementBuildingPlacement(simplenetwork::Message & msg, bool limitedByManpower);

        /**
         * Returns, in general, how desirable it would be to construct a new fort in the specified province.
         * The province is assumed to be an edge province, owned by us.
         *
         * This usually returns values between around 90 and 300, averaging about 175.
         * The worst value is zero, values above 600-1000 (up to 8000) are occasionally encountered.
         *
         * Called by doBuildingManagementPhase.
         */
        provvalue_t getFortBuildDesirability(const Province & province) const;

        /**
         * Returns the amount of currency per turn that we should reserve for building buildings.
         * This is the minimum between:
         *  - A proportion of our total income, so that larger players can afford
         *    more and better buildings - (weights.buildingReservedIncome1024).
         *  - A proportion of the currency income of our best province, so small players who
         *    invest heavily in a few provinces are rewarded - (weights.buildingReservedProvinceValuePercent).
         */
        provstat_t getBuildingReservedIncome(void) const;

        // ======================= Threat evalaution (TMAIThreat.cpp) ===============================

        /**
         * Gets the threat contribution to the player, of the specified province,
         * assuming it is adjacent to our territory, and owned by an enemy player.
         */
        provvalue_t getLocalEnemyThreat(const Province & province) const;

        /**
         * Returns sum of the contributed threats of all adjacent enemy provinces.
         */
        provvalue_t getSurroundingEnemyThreat(const Province & province) const;

        /**
         * Returns generally how threatened the provided province is, assuming that
         * it is a friendly province. AI players should aim to make this function return
         * zero (or less) by placing armies.
         *
         * If there is no battle going on:
         *  > Returns the sum of the contributed threats of all adjacent enemy provinces,
         *    modified by the number of friendly enemy units in the province.
         *    Friendly enemy units decrease the threat, possibly to a value below zero.
         *
         * If there *is* a battle going on:
         *  > Returns a very high threat if we are losing the battle, or there is a siege
         *    going on.
         *  > Returns a low threat if we are winning the battle.
         *  > Returns a negative threat only if we are overwhelmingly winning the battle
         *    so should probably move some armies elsewhere.
         */
        provvalue_t getProvinceThreatBalance(const Province & province) const;

        /**
         * Assuming the provided province is a centre province, finds the most
         * appropriate province to move armies into, or NULL if we shouldn't move.
         *
         *  > If there is a battle happening in this province, NULL is returned.
         *  > If an adjacent province can be found, which has more threat, that is returned.
         *  > Otherwise, if we are adjacent to some edge province, one of these is picked
         *    at random.
         *  > If we are only adjacent to centre provinces, which are all under no threat,
         *    then one of these is picked at random.
         */
        Province * findMostThreatenedAdjacentFromCentre(const Province & province) const;

        /**
         * Assuming the provided province is an edge province, finds the most
         * appropriate province to move armies into, or NULL if we shouldn't move.
         *
         *  > If there is an adjacent province owned by us, where there is a battle ongoing,
         *    which has positive threat because it needs reinforcement, returns that
         *    province to move to.
         *  > Otherwise returns NULL: Don't move.
         */
        Province * findBattleToReinforceFromEdge(const Province & province) const;

        // ======================= Value evaulation (TMAIValue.cpp) =================================

        /**
         * Assuming the provided province is an enemy province,
         * this works out how worthwhile it would be to own the specified province.
         * Values for this are in the range of about 75-500, but are usually around 130-280.
         */
        provvalue_t getProvinceAttackValue(const Province & province) const;

        /**
         * Assuming the provided province is an enemy province, which contains a battle,
         * (either normal battle or siege-battle with garrison), which friendly units are involved in:
         *
         * > Return a very low or negative province value if we are winning the
         *   battle.
         * > Return a very high value if we are losing the battle. This ought to
         *   be much higher than is gainable just from province value, so we try
         *   to prioritise battle reinforcing.
         */
        provvalue_t getBattleAttackReinforceValue(const Province & province) const;

        /**
         * Assuming the provided province is an enemy province, adjacent to our territory,
         * this works out how desirable it would be for us, currently, to move units from our
         * border to attack this province, (or join in the existing battle which may be happening).
         *
         * > If there is either no ongoing battle, or an ongoing true siege by a friendly player:
         *   - Gets a base value using the getProvinceAttackValue method.
         *   - Then, adds a positive or negative modifier for the number of enemy units
         *     already in this province.
         *   - And subtracts a very large value if when attacking we would always be
         *     seriously outnumbered, (don't *start* battles we obviously can't win).
         * > If there IS an ongoing battle, (or siege-battle against garrison), already:
         *   - Returns the province value using getBattleAttackReinforceValue.
         */
        provvalue_t getProvinceAttackFromBorderDesirabilityBalance(const Province & province) const;

        /**
         * Assuming the provided province is an enemy province, this works out how worthwhile
         * it would be for us to move attacking forces, which are already in enemy territory,
         * into this province to either siege or join in a battle.
         *
         * > If there is either no ongoing battle, or an ongoing true siege by a friendly player:
         *   - Gets a base value using the provinceAttackValue method.
         *   - If there is already an ongoing siege here, where we have enough men to
         *     complete it, subtract a large negative value modifier.
         *   - If there is an ongoing siege where we don't have enough men to complete the
         *     siege, add a bonus modifier.
         *   - Then, adds a positive or negative modifier for the number of enemy units
         *     already in this province.
         * > If there IS an ongoing battle, (or siege-battle against garrison), already:
         *   - Returns the province value using getBattleAttackReinforceValue.
         */
        provvalue_t getProvinceMoveAttackingForceValue(const Province & province) const;

        /**
         * Returns how desirable it is for us to place attack forces on our border,
         * adjacent to the specified province.
         * Currently this just uses getProvinceMoveAttackingForceValue.
         */
        inline provvalue_t getProvinceAttackBorderPlacementDesirabilityBalance(const Province & province) const {
            return getProvinceMoveAttackingForceValue(province);
        }

        /**
         * Returns the number of units that should be needed to attack the specified province
         * across the border. This is the number required to siege the province, plus
         * the number required to outnumber the enemy by attackBattleSafety, (if there is
         * a battle going on.
         */
        uint64_t getUnitsRequiredToAttackAcrossBorder(const Province & province) const;

        // ========================= Unit placement (TMAIUnitPlacement.cpp) =========================

        /**
         * Gets the maximum number of men that we are willing to place
         * this current turn.
         * This is based on the amount of manpower and currency that we have, taking
         * into account the reserved building income.
         *
         * limitedByManpower is set to true if the number of units we can place
         * is limited by manpower, and false if it is limited by currency.
         */
        uint64_t getMenPlaceableThisTurn(bool * limitedByManpower) const;

        /**
         * Work out the maximum number of units which should be placed during
         * the defensive army placement phase, considering the maximum we should place
         * this turn, and the overall threat in relevant provinces.
         * This should also be used when building forts, to figure out the initial
         * garrison.
         * The overall threat is treated as unsigned, so must be positive.
         */
        uint64_t getDefensivePlaceable(uint64_t maxPlaceable, uint64_t overallThreat) const;

        /**
         * Places the specified number of men in the specified province.
         * These are placed as standing armies.
         */
        void placeUnits(simplenetwork::Message & msg, Province & province, uint64_t unitCount);

        /**
         * Places the specified number of men into the garrison of the specified province.
         * The province is assumed to have a fort.
         */
        void placeUnitsIntoGarrison(simplenetwork::Message & msg, Province & province, uint64_t unitCount);

        /**
         * Assuming the province has a fort, this method absorbs all of our standing
         * armies (within that province) into the garrison.
         */
        void absorbArmiesIntoGarrison(simplenetwork::Message & msg, Province & province);

        /**
         * Ensures that unitsToPlace units are added to the garrison. This is achieved by
         * absorbing standing armies, owned by us, into the garrison. If there are too
         * few units to absorb into the garrison, additional units are placed.
         * Returns the number of NEW units placed.
         */
        uint64_t absorbAndPlaceIntoGarrison(simplenetwork::Message & msg, Province & province, uint64_t unitsToPlace);

        /**
         * Absorb/places at least unitsToPlace. If less than minimumStandingArmy units
         * would be left over in the province, more are placed in the garrison.
         */
        uint64_t absorbAndPlaceComparativelyIntoGarrison(simplenetwork::Message & msg, Province & province, uint64_t unitsToPlace, uint64_t minimumStandingArmy);

        /**
         * Assuming the specified province is an edge province, places units in such a way as to
         * avoid putting them straight into a battle.
         * So:
         * If there is no battle going on:
         *   Places units in the specified edge province. If placeInGarrison is set to true, and the
         *   province has a fort, the units are placed as a garrison. Otherwise, units are placed as
         *   standing armies.
         * Otherwise:
         *   This will place the units, as standing armies, in a random adjacent CENTRE PROVINCE, (without
         *   a battle ongoing). If there is no adjacent centre province, puts units (as standing armies) in
         *   any adjacent province owned by us, (without a battle ongoing). Otherwise: Places no units.
         *   When placing in adjacent provinces, garrisons are never used, as otherwise we wouldn't be able
         *   to reinforce a battle next turn. Note that this never places units into an ongoing battle.
         *
         * Returns the number of units actually placed. This might be zero if there is nowhere to put them.
         */
        uint64_t placeUnitsAvoidingBattle(simplenetwork::Message & msg, Province & province, uint64_t unitCount, bool placeInGarrison);

        /**
         * Assuming the specified province is an adjacent enemy province, places units in
         * an (non battle ongoing) edge province, owned by us, adjacent to it.
         * The province picked is randomly from available provinces.
         * Returns the number of units placed, which could be zero if not province is found.
         */
        uint64_t placeUnitsNextToEnemy(simplenetwork::Message & msg, Province & province, uint64_t unitCount);

        // ==================== Defensive behaviour (TMAIDefensive.cpp) =============================

        /**
         * AI Phase 2: Move around the armies in all of our provinces, to
         * better try to balance out threat.
         */
        void doDefensiveArmyMovement(simplenetwork::Message & msg);

        /**
         * AI Phase 3: For each province, which we own, where there is an ongoing
         * battle, which we are involved in, and there is a fort, unless our standing
         * armies largely outnumber the enemy, absorb them into the garrison as they
         * are stronger that way.
         */
        void retreatBattlesIntoGarrisons(simplenetwork::Message & msg);

        /**
         * AI Phase 4: Look at the threat in all of our provinces, and place
         * down armies accordingly.
         * Returns the number of men actually placed down.
         * Note: This works in almost the same way as doAttackArmyPlacement, except that
         *       armies are proportionally split by threat balance,
         *       and we will stop placing units once the threat decreases.
         */
        uint64_t doDefensiveArmyPlacement(simplenetwork::Message & msg, uint64_t maxPlaceable);

        // ===================== Attacking behaviour (TMAIAttack.cpp) ===============================

        /**
         * AI Phase 5: Move around armies with view to attack.
         */
        void doAttackArmyMovement(simplenetwork::Message & msg);

        /**
         * The first bit of AI phase 5: Move our border forces forward.
         */
        void doAttackMovePushBorder(simplenetwork::Message & msg);

        /**
         * The second bit of AI phase 5: Move around armies which are already in enemy
         * territory.
         */
        void doAttackMoveExisting(simplenetwork::Message & msg);
        // Assuming the specified army is too small to complete the siege, redistributes it to a useful
        // adjacent province.
        void redistributeTooSmallArmy(simplenetwork::Message & msg, Province & prov, const Army & army);
        // Redistributes up to the specified number of units out of the province, if useful.
        // The specified number of units must be at least ProvinceBattleController::SIEGE_OVERALL_LOSSES,
        // and MUST be smaller than the size of the army.
        void redistributeAwayFromSiege(simplenetwork::Message & msg, Province & prov, const Army & army, uint64_t toMove);

        /**
         * AI Phase 6: Place armies in the most strategially useful places on our border,
         * with view of attacking.
         * Returns the number of men actually placed down.
         * Note: This works in almost the same way as doDefensiveArmyPlacement, except that
         *       armies are proportionally split by attack value,
         *       and we never stop placing units, relying on the attack movement pushing
         *       units forward.
         */
        uint64_t doAttackArmyPlacement(simplenetwork::Message & msg, uint64_t maxPlaceable);
    };
}

#endif

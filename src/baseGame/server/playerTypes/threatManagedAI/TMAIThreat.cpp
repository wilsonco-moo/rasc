/*
 * TMAIThreat.cpp
 *
 *  Created on: 3 Dec 2019
 *      Author: wilson
 *
 * This file contains the methods for evaluating the
 * threat of provinces, within the ThreatManagedAI class.
 * See ThreatManagedAI.h.
 */

#include "ThreatManagedAI.h"

#include <cstdlib>
#include <cstddef>

#include "../../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../../common/gameMap/mapElement/types/Province.h"
#include "../../../common/gameMap/properties/trait/Traits.h"
#include "../../../common/gameMap/properties/PropertySet.h"
#include "../../../common/gameMap/properties/Properties.h"
#include "../../../common/stats/types/ProvincesStat.h"
#include "../../../common/playerData/PlayerData.h"
#include "../../../common/util/numeric/Random.h"
#include "../../RascBaseGameServer.h"
#include "../../ServerBox.h"

namespace rasc {

    provvalue_t ThreatManagedAI::getLocalEnemyThreat(const Province & province) const {
        uint64_t ourId = info->getPlayerId(),
                 theirId  = province.getProvinceOwner();
        const ThreatManagedWeights & weights = getData()->getWeights();
        RascBaseGameServer * server = box.server;
        ProvincesStat * provincesStat = box.common.provincesStat;
        provvalue_t threat = 0;

        // Count the enemy units in the province
        uint64_t enemyUnits = province.armyControl.countUnitsEnemy(ourId, server, &RascBaseGameServer::canAttackCxt);
        // Add threat contribution from garrisoned units in this province (less than regular units).
        threat += (provvalue_t)((GET_GARRISON_SIZE(province.properties) * weights.garrisonThreatPer4096())/4096);
        // Add threat contribution from enemy units in this province.
        threat += (provvalue_t)((enemyUnits * weights.enemyThreatPer4096())/4096);

        // Work out how many provinces they have, and how many we have. Multiply both by
        // modifiers from our weights. If they have more provinces than us, multiply up the
        // threat by the ratio.
        unsigned int enemyProvinces = provincesStat->getProvinceCount(theirId) * weights.bignessEnemyMultiplier(),
                       ourProvinces = provincesStat->getProvinceCount(ourId)   * weights.bignessOurMultiplier();
        if (enemyProvinces > ourProvinces) {
            // Avoid dividing by zero.
            if (ourProvinces == 0) ourProvinces = 1;
            // Cast to a long, to avoid potential for integer overflow.
            threat = (provvalue_t)((((long)threat) * enemyProvinces) / ourProvinces);
        }

        return threat;
    }

    provvalue_t ThreatManagedAI::getSurroundingEnemyThreat(const Province & province) const {
        provvalue_t localThreat = 0;
        uint64_t playerId = info->getPlayerId();
        GameMap & map = *box.common.map;
        RascBaseGameServer & server = *box.server;

        // Simply sum up threat contributed from all adjacent enemy provinces.
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = map.getProvince(adjProvId);
            if (server.canAttackEachOther(playerId, adjProv.getProvinceOwner())) {
                localThreat += getLocalEnemyThreat(adjProv);
            }
        }
        return localThreat;
    }

    provvalue_t ThreatManagedAI::getProvinceThreatBalance(const Province & province) const {
        ProvinceBattleController * battle = province.battleControl;
        RascBaseGameServer * server = box.server;
        uint64_t ourId = info->getPlayerId();
        const ThreatManagedWeights & weights = getData()->getWeights();

        // Work out the number of enemy AND friendly units.
        uint64_t enemyUnits, friendlyUnits;
        province.armyControl.countUnitsFriendlyEnemy(ourId, &friendlyUnits, &enemyUnits, server, &RascBaseGameServer::canAttackCxt);
        // Add on the appropriately scaled number of garrisoned units. Since this is a friendly province, they count towards friendly units.
        friendlyUnits += ((GET_GARRISON_SIZE(province.properties) * weights.garrisonBattleSizeMultiplier()) / 100);

        if (battle->isBattleOngoing()) {
            // Note that if there are fewer than battleDivisor friendly units, then we
            // can assume there is no battle going on, as otherwise it makes threat calculation
            // impossible, (due to a division by zero).
            if (friendlyUnits < (uint64_t)weights.battleDivisor()) {
                // If there are no friendly units, in a battle in our friendly province, then there
                // is either a battle between two enemy armies, or a siege against us.
                // Ignore all outside threats in this province, and return a threat based only
                // on the siege.
                return (enemyUnits * weights.siegeThreatPerEnemy4096())/4096;

            } else {

                // If there does exist friendly units in the battle, it is a battle
                // that *we* are involved in. So work out the threat in this province
                // using the battle safety and divisor.
                uint64_t enemyWithMargin = (enemyUnits * weights.defenseBattleSafety()) / 100;
                return (provvalue_t)(((int64_t)enemyWithMargin - (int64_t)friendlyUnits) / ((int64_t)friendlyUnits / weights.battleDivisor()));
            }

        } else {
            // If there is no ongoing battle, we can assume that all units in the province are
            // friendly. So return the surrounding threat, minus the contribution from friendly units.
            return getSurroundingEnemyThreat(province) - (provvalue_t)((friendlyUnits * weights.friendlyThreatCounteractPer4096())/4096);
        }
    }

    Province * ThreatManagedAI::findMostThreatenedAdjacentFromCentre(const Province & province) const {

        GameMap & map = *box.common.map;
        Random & random = *box.common.random;
        uint64_t ourId = getData()->getId();

        // Don't move out of a province if a battle is ongoing.
        if (province.battleControl->isBattleOngoing()) {
            return NULL;
        }

        // Assess the threat in the province containing the army.
        provvalue_t threat = getProvinceThreatBalance(province);

        // Find the adjacent province, owned by us, with the largest positive threat
        // which is larger than this province's threat.
        provvalue_t mostThreat = 0;
        Province * mostThreatProv = NULL;
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = map.getProvince(adjProvId);
            if (adjProv.getProvinceOwner() == ourId) {
                provvalue_t adjThreat = getProvinceThreatBalance(adjProv);
                if (adjThreat > mostThreat) {
                    mostThreatProv = &adjProv;
                    mostThreat = adjThreat;
                }
            }
        }

        // If a province was actually found, which is more threatening than this province,
        // then use it.
        if (mostThreat > 0 && mostThreat > threat) {
            return mostThreatProv;
        }

        // Otherwise, look around for edge provinces and adjacent other owned provinces.
        std::vector<Province *> adjEdgeProvs,
                                adjOtherOwnedProvs;
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = map.getProvince(adjProvId);
            if (info->isEdgeProvince(&adjProv)) {
                adjEdgeProvs.push_back(&adjProv);
            } else if (adjProv.getProvinceOwner() == ourId) {
                adjOtherOwnedProvs.push_back(&adjProv);
            }
        }

        // If there are some adjacent edge provinces, pick one of them.
        if (!adjEdgeProvs.empty()) {
            return adjEdgeProvs[random.sizetRange(0, adjEdgeProvs.size())];
        }

        // Otherwise, pick an adjacent other (centre) owned province.
        if (!adjOtherOwnedProvs.empty()) {
            return adjOtherOwnedProvs[random.sizetRange(0, adjOtherOwnedProvs.size())];
        }

        // If by some wierd situation there really *isn't* any adjacent owned centre
        // provinces, then either the stat system is broken, or the map has an inaccessible
        // province, then just return NULL to not move the army.
        return NULL;
    }

    Province * ThreatManagedAI::findBattleToReinforceFromEdge(const Province & province) const {

        GameMap & map = *box.common.map;
        uint64_t ourId = getData()->getId();

        // Don't move out of a province if a battle is ongoing.
        if (province.battleControl->isBattleOngoing()) {
            return NULL;
        }

        // Find the adjacent province, owned by us, with the largest positive threat,
        // where there is also an ongoing battle.
        provvalue_t mostThreat = 0;
        Province * mostThreatProv;
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = map.getProvince(adjProvId);
            if (adjProv.getProvinceOwner() == ourId && adjProv.battleControl->isBattleOngoing()) {
                provvalue_t adjThreat = getProvinceThreatBalance(adjProv);
                if (adjThreat > mostThreat) {
                    mostThreatProv = &adjProv;
                    mostThreat = adjThreat;
                }
            }
        }

        // If we found such a province, return it.
        if (mostThreat > 0) {
            return mostThreatProv;
        }

        // If there is no adjacent battles ongoing with positive threat, return NULL (don't move).
        return NULL;
    }
}

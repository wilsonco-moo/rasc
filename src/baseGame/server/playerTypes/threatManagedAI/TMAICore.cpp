/*
 * TMAICore.cpp
 *
 *  Created on: 2 Dec 2019
 *      Author: wilson
 *
 * This file contains the constructor and destructor, and other core functionality,
 * of the ThreatManagedAI class.
 * See ThreatManagedAI.h.
 */

#include "ThreatManagedAI.h"

#include "../../../common/stats/types/ProvincesStat.h"
#include "../../../common/playerData/PlayerData.h"
#include "../../ServerBox.h"

namespace rasc {

    ThreatManagedAI::ThreatManagedAI(ServerBox & box, PlayerData * data) :
        AIPlayer(box, data),
        info(NULL) {
    }

    ThreatManagedAI::~ThreatManagedAI(void) {
    }

    void ThreatManagedAI::aiTurn(simplenetwork::Message & msg) {
        info = &box.common.provincesStat->getOwnershipInfo(getData()->getId());
        bool limitedByManpower;
        uint64_t maxPlaceable = getMenPlaceableThisTurn(&limitedByManpower);

        // Phase 1: Building management.
        maxPlaceable -= doBuildingManagementPhase(msg, maxPlaceable, limitedByManpower);

        // Phase 2: Defensive army movement.
        doDefensiveArmyMovement(msg);

        // Phase 3: Retreat into garrisons.
        retreatBattlesIntoGarrisons(msg);

        // Phase 4: Defensive army placement.
        if (maxPlaceable > 0) {
            maxPlaceable -= doDefensiveArmyPlacement(msg, maxPlaceable);
        }

        // Phase 5: Attack army movement.
        doAttackArmyMovement(msg);

        // Phase 6: Attack army placement.
        if (maxPlaceable > 0) {
            maxPlaceable -= doAttackArmyPlacement(msg, maxPlaceable);
        }
    }
}

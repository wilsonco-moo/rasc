/*
 * TMAIUnitPlacement.cpp
 *
 *  Created on: 3 Dec 2019
 *      Author: wilson
 *
 * This class contains the methods for dealing with
 * unit placement, within the ThreatManagedAI class.
 * See ThreatManagedAI.h.
 */

#include "ThreatManagedAI.h"

#include <algorithm>
#include <cstdlib>
#include <cstddef>

#include "../../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../../common/gameMap/mapElement/types/Province.h"
#include "../../../common/gameMap/properties/trait/Traits.h"
#include "../../../common/gameMap/properties/PropertySet.h"
#include "../../../common/stats/types/UnitsDeployedStat.h"
#include "../../../common/gameMap/properties/Properties.h"
#include "../../../common/stats/types/ProvincesStat.h"
#include "../../../common/playerData/PlayerData.h"
#include "../../../common/stats/types/AreasStat.h"
#include "../../../common/util/numeric/Random.h"
#include "../../../common/gameMap/GameMap.h"
#include "../../ServerBox.h"

namespace rasc {

    uint64_t ThreatManagedAI::getMenPlaceableThisTurn(bool * limitedByManpower) const {
        const ThreatManagedWeights & weights = getData()->getWeights();
        int lookahead = weights.economyLookaheadTurns();

        provstat_t reservedIncome  = getBuildingReservedIncome(),
                   currencyPerTurn = getData()->getCurrencyBalancePerTurn() - reservedIncome,
                   totalCurrency   = getData()->getTotalCurrency() - (reservedIncome * lookahead),
                   totalManpower   = getData()->getTotalManpower();

        // If we are in debt, we are not allowed to place men.
        // If we are losing money, don't place units as that would end up putting us in debt.
        if (totalCurrency < 0 || currencyPerTurn < 0) {
            // If in debt or losing money, we are limited by currency.
            *limitedByManpower = false;
            return 0;
        }

        // If we have negative manpower, placing men is also not possible.
        if (totalManpower < 0) {
            *limitedByManpower = true;
            return 0;
        }

        // The maximum we can place, is the minimum between our total manpower,
        // and the armies we can afford.
        // We can afford a number of units if we can pay for them for at least "lookahead" turns.
        uint64_t manpowerLimit = totalManpower / AreasStat::MANPOWER_PER_UNIT,
                 currencyLimit = ((currencyPerTurn * lookahead + totalCurrency) * UnitsDeployedStat::CURRENCY_COST_UNIT_SET_SIZE) / (UnitsDeployedStat::CURRENCY_COST_PER_UNIT_SET * lookahead);

        if (manpowerLimit < currencyLimit) {
            *limitedByManpower = true;
            return manpowerLimit;
        } else {
            *limitedByManpower = false;
            return currencyLimit;
        }
    }

    uint64_t ThreatManagedAI::getDefensivePlaceable(uint64_t maxPlaceable, uint64_t overallThreat) const {
        const ThreatManagedWeights & weights = getData()->getWeights();
        // Work out using defensive units per overall threat, but don't place more
        // than the maximum defensive placement percentage of the maximum units placeable.
        return std::min(
            (overallThreat * 4096) / weights.threatPer4096DefensivePlacement(),
            (maxPlaceable * weights.maximumDefensivePlacementPercentage()) / 100
        );
    }

    void ThreatManagedAI::placeUnits(simplenetwork::Message & msg, Province & province, uint64_t unitCount) {
        getData()->addTotalManpower(MiscUpdate::server(msg, &box), -AreasStat::MANPOWER_PER_UNIT * unitCount, PlayerStatChangeReasons::armyBuild);
        province.armyControl.placeArmyOrAddUnits(msg, box.common.turnCount, getData(), unitCount);
    }

    void ThreatManagedAI::placeUnitsIntoGarrison(simplenetwork::Message & msg, Province & province, uint64_t unitCount) {
        // Note: Stat change reason is actually constructing a building, NOT placing an army.
        // Garrisons are in fact buildings, not armies (oddly).
        getData()->addTotalManpower(MiscUpdate::server(msg, &box), -AreasStat::MANPOWER_PER_UNIT * unitCount, PlayerStatChangeReasons::constructBuilding);
        province.properties.changeTraitCount(MiscUpdate::server(msg, &box), Properties::Types::garrison, GET_GARRISON_SIZE(province.properties) + unitCount);
    }

    void ThreatManagedAI::absorbArmiesIntoGarrison(simplenetwork::Message & msg, Province & province) {
        ProvinceArmyController & armyControl = province.armyControl;
        uint64_t ourId = getData()->getId();
        // Get our number of units. If this is zero, we don't need to do anything.
        uint64_t ourUnits = armyControl.countUnitsOwnedBy(ourId);
        if (ourUnits == 0) return;
        // Remove all the province armies.
        std::list<Army>::const_iterator armyIter;
        while((armyIter = armyControl.getArmyOwnedByPlayer(ourId)) != armyControl.getArmies().cend()) {
            armyControl.removeArmy(MiscUpdate::server(msg, &box), armyIter->getArmyId());
        }
        // Add it as a garrison.
        province.properties.changeTraitCount(MiscUpdate::server(msg, &box), Properties::Types::garrison, GET_GARRISON_SIZE(province.properties) + ourUnits);
    }

    uint64_t ThreatManagedAI::absorbAndPlaceIntoGarrison(simplenetwork::Message & msg, Province & province, uint64_t unitsToPlace) {
        ProvinceArmyController & armyControl = province.armyControl;
        uint64_t ourId = getData()->getId();

        // First merge all armies to make this easier, then count our units.
        armyControl.mergeAllPlayerArmies(MiscUpdate::server(msg, &box));
        uint64_t ourUnits = armyControl.countUnitsOwnedBy(ourId);

        // Add on the requested number of units to the garrison.
        province.properties.changeTraitCount(MiscUpdate::server(msg, &box), Properties::Types::garrison, GET_GARRISON_SIZE(province.properties) + unitsToPlace);

        // If we have an army in the province, which is larger than the number of units we are required
        // to absorb into the garrison, subtract units from it. Otherwise remove it.
        if (ourUnits > 0) {
            std::list<Army>::const_iterator armyIter = armyControl.getArmyOwnedByPlayer(ourId);
            if (ourUnits > unitsToPlace) {
                armyControl.addUnitsNoTurn(MiscUpdate::server(msg, &box), armyIter->getArmyId(), -((int64_t)unitsToPlace));
            } else {
                armyControl.removeArmy(MiscUpdate::server(msg, &box), armyIter->getArmyId());
            }
        }

        // If the original army was big enough, just return zero.
        if (ourUnits >= unitsToPlace) {
            return 0;

        // If the original army was NOT big enough, work out how many we actually
        // placed, subtract it from our manpower, and return it.
        // Note that stat change reason is actually constructBuilding, as
        // garrisons are buildings not armies.
        } else {
            uint64_t actuallyPlaced = unitsToPlace - ourUnits;
            getData()->addTotalManpower(MiscUpdate::server(msg, &box), -actuallyPlaced * AreasStat::MANPOWER_PER_UNIT, PlayerStatChangeReasons::constructBuilding);
            return actuallyPlaced;
        }
    }

    uint64_t ThreatManagedAI::absorbAndPlaceComparativelyIntoGarrison(simplenetwork::Message & msg, Province & province, uint64_t unitsToPlace, uint64_t minimumStandingArmy) {
        uint64_t ourId = getData()->getId();
        uint64_t ourUnits = province.armyControl.countUnitsOwnedBy(ourId);

        if (ourUnits <= unitsToPlace + minimumStandingArmy) {
            return absorbAndPlaceIntoGarrison(msg, province, std::max(ourUnits, unitsToPlace));
        } else {
            return absorbAndPlaceIntoGarrison(msg, province, unitsToPlace);
        }
    }

    uint64_t ThreatManagedAI::placeUnitsAvoidingBattle(simplenetwork::Message & msg, Province & province, uint64_t unitCount, bool placeInGarrison) {
        if (province.battleControl->isBattleOngoing()) {

            // If there is an ongoing battle:
            // Find adjacent centre provinces, and adjacent non-centre (other) provinces owned by us.
            // Ignore any adjacent provinces with battles going on.
            GameMap & map = *box.common.map;
            Random & random = *box.common.random;
            uint64_t ourId = getData()->getId();
            std::vector<Province *> adjCentreProvs,
                                    adjOtherOwnedProvs;

            for (uint32_t adjProvId : province.getAdjacent()) {
                Province & adjProv = map.getProvince(adjProvId);
                if (!adjProv.battleControl->isBattleOngoing()) {
                    if (info->isCentreProvince(&adjProv)) {
                        adjCentreProvs.push_back(&adjProv);
                    } else if (adjProv.getProvinceOwner() == ourId) {
                        adjOtherOwnedProvs.push_back(&adjProv);
                    }
                }
            }

            // If we do have some adjacent centre provinces, pick one at random for placing units.
            if (!adjCentreProvs.empty()) {
                placeUnits(msg, *adjCentreProvs[random.sizetRange(0, adjCentreProvs.size())], unitCount);
                return unitCount;
            }

            // If we don't have adjacent centre provinces, but do have adjacent other provinces, pick one of *these* at random.
            // (So fall back to putting extra armies in one of our edge provinces).
            if (!adjOtherOwnedProvs.empty()) {
                placeUnits(msg, *adjOtherOwnedProvs[random.sizetRange(0, adjOtherOwnedProvs.size())], unitCount);
                return unitCount;
            }

            // Otherwise, if there are no adjacent centre or edge provinces, without battles already happening in them,
            // place no units.
            // placeUnits(msg, province, unitCount);
            return 0;

        } else {
            // If there is no battle ongoing, place units normally. If placeInGarrison is set, and we
            // have a fort, place them into the garrison. Otherwise place standing armies.
            if (placeInGarrison && province.properties.getPropertySet().hasPropertiesOfType(Properties::Types::fort)) {
                placeUnitsIntoGarrison(msg, province, unitCount);
            } else {
                placeUnits(msg, province, unitCount);
            }
            return unitCount;
        }
    }

    uint64_t ThreatManagedAI::placeUnitsNextToEnemy(simplenetwork::Message & msg, Province & province, uint64_t unitCount) {
        GameMap & map = *box.common.map;
        Random & random = *box.common.random;
        uint64_t ourId = getData()->getId();
        std::vector<Province *> availableProvinces;

        // Find all adjacent provinces, owned by us, without battles.
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = map.getProvince(adjProvId);
            if (adjProv.getProvinceOwner() == ourId && !adjProv.battleControl->isBattleOngoing()) {
                availableProvinces.push_back(&adjProv);
            }
        }

        // If there are no available provinces, return 0 and place no units.
        if (availableProvinces.empty()) return 0;

        // If there are provinces available, place units in one which is randomly picked.
        placeUnits(msg, *availableProvinces[random.sizetRange(0, availableProvinces.size())], unitCount);
        return unitCount;
    }
}

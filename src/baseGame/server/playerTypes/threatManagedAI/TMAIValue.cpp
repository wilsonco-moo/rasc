/*
 * TMAIValue.cpp
 *
 *  Created on: 6 Dec 2019
 *      Author: wilson
 *
 * This file contains the methods for evaluating the
 * value of provinces, within the ThreatManagedAI class.
 * See ThreatManagedAI.h.
 */

#include "ThreatManagedAI.h"

#include <algorithm>
#include <iostream>
#include <cstdlib>

#include "../../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../../common/gameMap/mapElement/types/Province.h"
#include "../../../common/gameMap/properties/trait/Traits.h"
#include "../../../common/gameMap/properties/PropertySet.h"
#include "../../../common/gameMap/properties/Properties.h"
#include "../../../common/stats/types/ProvincesStat.h"
#include "../../../common/stats/types/AreasStat.h"
#include "../../../common/playerData/PlayerData.h"
#include "../../../common/util/numeric/Random.h"
#include "../../../common/gameMap/GameMap.h"
#include "../../RascBaseGameServer.h"
#include "../../ServerBox.h"

namespace rasc {

    /**
     * If there is a battle going on in the specified province:
     *  > If it is a siege, and enemy players are involved, return the value
     *    of the province with getBattleAttackReinforceValue.
     *  > If is a real (non-siege) battle, return the value of the province
     *    with getBattleAttackReinforceValue.
     */
    #define RETURN_REINFORCE_VAL_IF_BATTLE(prov, ourPlayerId, rascServer)                                               \
        if ((prov).battleControl->isBattleOngoing()) {                                                                  \
            if ((prov).battleControl->getDefenderCount() == 0 && GET_GARRISON_SIZE((prov).properties) == 0) {           \
                /* If there is a true siege done by a non-friendly player, (no defenders and no garrison), use the   */ \
                /* battle method.                                                                                    */ \
                for (uint64_t attacker : (prov).battleControl->getAttackers()) {                                        \
                    if ((rascServer)->canAttackEachOther((ourPlayerId), attacker)) {                                    \
                        return getBattleAttackReinforceValue(prov);                                                     \
                    }                                                                                                   \
                }                                                                                                       \
            } else {                                                                                                    \
                /* If there is a proper battle (or siege-battle against garrison) going on, (there are some          */ \
                /* defenders, or garrison is non-zero), use the battle method.                                       */ \
                return getBattleAttackReinforceValue(prov);                                                             \
            }                                                                                                           \
        }

    provvalue_t ThreatManagedAI::getProvinceAttackValue(const Province & province) const {

        uint64_t ourId = getData()->getId();
        const ThreatManagedWeights & weights = getData()->getWeights();
        Random & random = *box.common.random;
        
        // 0: Return -100000 if the province is owned by us - we cannot attack ourself.
        if (province.getProvinceOwner() == ourId) return -100000;
        
        provvalue_t value = 0;

        // 1: Increase value if the province is unowned.
        if (province.getProvinceOwner() == PlayerData::NO_PLAYER) {
            value += weights.provValueUnownedPlus();

        // 2: Apply various sizing factors.
        } else {
            provvalue_t ourSize   = info->getOwnedProvinces().size(),
                        theirSize = (GLfloat)box.common.provincesStat->getProvinceCount(province.getProvinceOwner());

            if (theirSize <= weights.provValueTinyNationMaxSize() &&
                ourSize >= weights.provValueTinyNationOppressorMinSize()) {
                value += weights.provValueTinyNationPlus();
            }

            provvalue_t sizeMult = (theirSize * 100) / ourSize;
            if (ourSize >= weights.provValueLargerNationMinOurSize() &&
                theirSize >= weights.provValueLargerNationMinSize() &&
                sizeMult >= weights.provValueLargerPercentageLargerFactor()) {
                value += ((sizeMult * weights.provValueLargerNationPlus()) / weights.provValueLargerPercentageLargerFactor());
            }
        }

        // 2: Add on a random value
        value += random.uintRange(0, weights.provValueRandomPlus());

        // 3: Add the area modifier. The fewer provinces left to complete the area, the higher the value.
        {
            std::pair<unsigned int, unsigned int> pair = box.common.areasStat->getOwnedAndTotalWithinArea(ourId, province.getAreaId());

            provvalue_t unOwned = pair.second - pair.first,
                          total = pair.second;

            value += std::min(
                weights.provValueMaxAreaModifier(),
                (total * weights.provValueAreaModifierMultiplier() / unOwned) - weights.provValueAreaModifierMultiplier()
            );

            if (unOwned == total) value += weights.provValueFootholdPlus();
        }

        // 4. Add a bonus for the general province value.
        value += ((((provvalue_t)province.properties[ProvStatIds::provinceValue]) * weights.provValueStatModifierPlus()) / 100);

        return value;
    }

    provvalue_t ThreatManagedAI::getBattleAttackReinforceValue(const Province & province) const {
        RascBaseGameServer * server = box.server;
        uint64_t ourId = getData()->getId();
        const ThreatManagedWeights & weights = getData()->getWeights();

        // Count the number of friendly and enemy units.
        uint64_t friendlyUnits, enemyUnits;
        province.armyControl.countUnitsFriendlyEnemy(ourId, &friendlyUnits, &enemyUnits, server, &RascBaseGameServer::canAttackCxt);
        // Add on the appropriately scaled number of garrisoned units. Since this is an enemy province, they count towards enemy units.
        enemyUnits += ((GET_GARRISON_SIZE(province.properties) * weights.garrisonBattleSizeMultiplier()) / 100);

        // Don't bother reinforcing armies which are too small (or zero for that matter) - it's not worth it.
        if (friendlyUnits < weights.attackValueMinReinforce()) return 0;

        // Use the same calculation as is used in getProvinceThreatBalance.
        uint64_t enemyWithMargin = (enemyUnits * weights.attackBattleSafety()) / 100;
        return (provvalue_t)(((int64_t)enemyWithMargin - (int64_t)friendlyUnits) / ((int64_t)friendlyUnits / weights.attackValueBattleDivisor()));
    }

    provvalue_t ThreatManagedAI::getProvinceAttackFromBorderDesirabilityBalance(const Province & province) const {
        RascBaseGameServer * server = box.server;
        GameMap & map = *box.common.map;
        const ThreatManagedWeights & weights = getData()->getWeights();
        uint64_t ourId = getData()->getId();

        // If there is a battle going on, return using the getBattleAttackReinforceValue method instead.
        RETURN_REINFORCE_VAL_IF_BATTLE(province, ourId, server)

        // 1: Get the base value using the normal province attack value method.
        provvalue_t value = getProvinceAttackValue(province);

        // 2: Work out the number of enemy men in this province, and add these to province
        //    value with the appropriate modifier.
        uint64_t enemyUnits = province.armyControl.countUnitsEnemy(ourId, server, &RascBaseGameServer::canAttackCxt);
        // Add on the appropriately scaled number of garrisoned units. Since this is an enemy province, they count towards enemy units.
        enemyUnits += ((GET_GARRISON_SIZE(province.properties) * weights.garrisonBattleSizeMultiplier()) / 100);
        value += (provvalue_t)((((int64_t)enemyUnits) * weights.attackDesirabilityPer4096())/4096);

        // 3: Look around all the adjacent provinces, and find the armies OWNED BY US,
        //    WHICH CAN MOVE, and whether they are engaged in a siege or not.
        //    Do not count units owned by us who are engaged in regular battles.
        uint64_t adjUnits = 0,
                 adjSiegeEngaged = 0;
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = map.getProvince(adjProvId);
            if (adjProv.battleControl->isBattleOngoing()) {
                if (adjProv.battleControl->getDefenderCount() == 0) {
                    adjSiegeEngaged += adjProv.armyControl.countMoveableUnitsOwnedBy(ourId, box.common.turnCount);
                }
            } else {
                adjUnits += adjProv.armyControl.countMoveableUnitsOwnedBy(ourId, box.common.turnCount);
            }
        }

        // 4: If we don't outnumber the enemy adjusted for battle safety,
        //    then add appropriate modifiers.
        uint64_t enemyWithMargin = (enemyUnits * weights.attackBattleSafety()) / 100;
        if (adjUnits <= enemyWithMargin) {
            if (adjUnits + adjSiegeEngaged > enemyWithMargin) {
                value += weights.attackValueSiegeBreakModifier();
            } else {
                value += weights.attackValueOutnumbermentModifier();
            }
        }

        return value;
    }

    provvalue_t ThreatManagedAI::getProvinceMoveAttackingForceValue(const Province & province) const {
        RascBaseGameServer * server = box.server;
        uint64_t ourId = getData()->getId();
        const ThreatManagedWeights & weights = getData()->getWeights();

        // If there is a battle going on, return using the getBattleAttackReinforceValue method instead.
        RETURN_REINFORCE_VAL_IF_BATTLE(province, ourId, server)

        // 1: Get the base value using the normal province attack value method.
        provvalue_t value = getProvinceAttackValue(province);

        // 2: Count the number of friendly and enemy units. These will usually be zero.
        uint64_t friendlyUnits, enemyUnits;
        province.armyControl.countUnitsFriendlyEnemy(ourId, &friendlyUnits, &enemyUnits, server, &RascBaseGameServer::canAttackCxt);
        // Add on the appropriately scaled number of garrisoned units. Since this is an enemy province, they count towards enemy units.
        enemyUnits += ((GET_GARRISON_SIZE(province.properties) * weights.garrisonBattleSizeMultiplier()) / 100);

        // 3: Add modifiers from sieges.
        if (province.battleControl->isBattleOngoing()) {
            // Note: If a battle is ongoing, we can assume there is a true siege (not garrison battle) that we are involved in, and
            //       no enemy units are present. Otherwise RETURN_REINFORCE_VAL_IF_BATTLE would have returned.
            // So add the appropriate siege modifier based on whether we have enough units in the siege or not.
            if (friendlyUnits < province.battleControl->getUnitsLeftToCompleteSiege()) {
                value += weights.attackValueNotEnoughSiegeModifier();
            } else {
                value += weights.attackValueEnoughSiegeModifier();
            }
        } else {
            // If there is no siege or battle in this ENEMY province, there is either nobody,
            // or some enemy units in the province. So add the modifier for enemy units.
            value += (provvalue_t)((((int64_t)enemyUnits) * weights.attackDesirabilityPer4096())/4096);
        }
        return value;
    }

    uint64_t ThreatManagedAI::getUnitsRequiredToAttackAcrossBorder(const Province & province) const {
        RascBaseGameServer * server = box.server;
        const ThreatManagedWeights & weights = getData()->getWeights();
        uint64_t ourId = getData()->getId();

        // 1: Count the number of friendly and enemy units.
        uint64_t friendlyUnits, enemyUnits;
        province.armyControl.countUnitsFriendlyEnemy(ourId, &friendlyUnits, &enemyUnits, server, &RascBaseGameServer::canAttackCxt);
        // Add on the appropriately scaled number of garrisoned units. Since this is an enemy province, they count towards enemy units.
        enemyUnits += ((GET_GARRISON_SIZE(province.properties) * weights.garrisonBattleSizeMultiplier()) / 100);

        uint64_t value = 0;

        // 2: Work out base value for siege, (by looking at the number of friendly men already there).
        if (friendlyUnits < ProvinceBattleController::SIEGE_OVERALL_LOSSES) {
            value += (ProvinceBattleController::SIEGE_OVERALL_LOSSES - friendlyUnits);
        }

        // 3: Add on number required to outnumber the enemy.
        if (enemyUnits != 0) {
            uint64_t enemyWithMargin = (enemyUnits * weights.attackBattleSafety()) / 100;
            if (friendlyUnits < enemyWithMargin) {
                value += (enemyWithMargin - friendlyUnits);
            }
        }

        return value;
    }
}

/*
 * TMAIBuilding.cpp
 *
 *  Created on: 20 Feb 2020
 *      Author: wilson
 *
 * This file contains the methods for controlling the
 * placement of buildings, within the ThreatManagedAI class.
 * See ThreatManagedAI.h.
 */

#include "ThreatManagedAI.h"

#include <algorithm>
#include <cstdlib>
#include <array>

#include "../../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../../common/util/templateTypes/staticStorage/StaticVector.h"
#include "../../../common/gameMap/mapElement/types/Province.h"
#include "../../../common/gameMap/properties/trait/Traits.h"
#include "../../../common/gameMap/properties/PropertySet.h"
#include "../../../common/gameMap/properties/Properties.h"
#include "../../../common/stats/types/ProvincesStat.h"
#include "../../../common/playerData/PlayerStatArr.h"
#include "../../../common/stats/types/BuildingStat.h"
#include "../../../common/stats/types/AreasStat.h"
#include "../../../common/playerData/PlayerData.h"
#include "../../../common/util/numeric/Random.h"
#include "../../../common/gameMap/GameMap.h"
#include "../../RascBaseGameServer.h"
#include "../../ServerBox.h"

namespace rasc {

    uint64_t ThreatManagedAI::doBuildingManagementPhase(simplenetwork::Message & msg, uint64_t maxPlaceable, bool limitedByManpower) {
        const ThreatManagedWeights & weights = getData()->getWeights();
        uint64_t unitsPlacedOverall = 0;

        // First do fort removal. This looks only at centre provinces, so can never
        // conflict with the fort addition.
        doFortRemoval(msg);

        // Stores true if we tried to build a fort.
        bool wantedFort = false;

        // Loop to allow building of multiple forts in one turn.
        while(true) {
            // Find the value and province with best fort desirability.
            provvalue_t maxFortDes = 0;
            Province * bestProvince = NULL;
            for (Province * province : info->getEdgeProvinces().vec()) {
                provvalue_t fortDes = getFortBuildDesirability(*province);
                if (fortDes > maxFortDes) {
                    maxFortDes = fortDes;
                    bestProvince = province;
                }
            }

            // If desirability is over the threshold, build a fort. If a fort
            // actually gets built, loop again, otherwise break.
            // If desirability is under the threshold, give up and break.
            if (maxFortDes >= weights.fortDesirabilityBuildThreshold()) {
                wantedFort = true;
                bool placedFort;
                uint64_t placed = doFortPlacement(msg, *bestProvince, placedFort, maxPlaceable);
                unitsPlacedOverall += placed;
                maxPlaceable -= placed;
                if (!placedFort) break;
            } else {
                break;
            }

        }

        // If we didn't try to build a fort, do improvement building placement.
        if (!wantedFort) {
            doImprovementBuildingPlacement(msg, limitedByManpower);
        }

        return unitsPlacedOverall;
    }

    void ThreatManagedAI::doFortRemoval(simplenetwork::Message & msg) {
        const ThreatManagedWeights & weights = getData()->getWeights();
        Random & random = *box.common.random;
        PlayerStatArr cost = Traits::getTraitBuildingCost(NULL, Properties::Types::fort, -1);
        // We can't remove forts if we can't afford to. Ignore overflow errors since we are querying a fixed number of buildings.
        if (!getData()->canAffordBuilding(cost)) return;

        // Iterate through each centre province...
        for (Province * centreProvince : info->getCentreProvinces().vec()) {
            ProvinceProperties & properties = centreProvince->properties;

            // If it has no ongoing battle, has a fort, is making a large enough profit, a random probability...
            if (!centreProvince->battleControl->isBattleOngoing() &&
                properties.getPropertySet().hasPropertiesOfType(Properties::Types::fort) &&
                properties[ProvStatIds::currencyIncome] - properties[ProvStatIds::currencyExpenditure] < weights.fortRemovalCurrencyProfit() &&
                random.uintRange(0, weights.fortRemovalProbability()) == 0) {

                // Remove the garrison, and the fort. We get no direct reward for doing this,
                // but now no longer have to pay the maintenence for it.
                properties.removeTrait(MiscUpdate::server(msg, &box), Properties::Types::fort);
                properties.removeTrait(MiscUpdate::server(msg, &box), Properties::Types::garrison);
                // Demolishing forts costs the same as building them. If we can't afford to do it again, give up.
                getData()->spendBuildingCost(MiscUpdate::server(msg, &box), cost);
                if (!getData()->canAffordBuilding(cost)) return;
            }
        }
    }

    uint64_t ThreatManagedAI::doFortPlacement(simplenetwork::Message & msg, Province & province, bool & placedFort, uint64_t maxPlaceable) {
        const ThreatManagedWeights & weights = getData()->getWeights();

        // Get building cost of fort, ignoring overflow. If we can't afford it give up.
        PlayerStatArr cost = Traits::getTraitBuildingCost(NULL, Properties::Types::fort, 1);
        if (!getData()->canAffordBuilding(cost)) {
            placedFort = false;
            return 0;
        }
        // If building a fort exceeds land use, and demolition to make way for the fort fails, give up.
        if (province.properties.doesLandUseModifierExceedLand(Traits::FORT_LAND_USE) &&
            !tryDemolishToMakeWayForFort(msg, province, cost)) {
            placedFort = false;
            return 0;
        }

        // Subtract the manpower and currency cost of the fort, since we want to build it.
        getData()->spendBuildingCost(MiscUpdate::server(msg, &box), cost);

        // Build a fort. Set placedFort to true because we *did* build a fort.
        province.properties.addTrait(MiscUpdate::server(msg, &box), Properties::Types::fort);
        placedFort = true;

        // Absorb and place the appropriate number of units, according to surrounding threat, into the province.
        // Return the number of units actually placed.
        return absorbAndPlaceComparativelyIntoGarrison(
            msg, province,
            getDefensivePlaceable(maxPlaceable, getSurroundingEnemyThreat(province)),
            weights.fortMinStandingArmyAfterBuild()
        );
    }

    bool ThreatManagedAI::tryDemolishToMakeWayForFort(simplenetwork::Message & msg, Province & province, const PlayerStatArr & reserveCost) {
        const PropertySet & propertySet = province.properties.getPropertySet();

        // Change in land use from demolition, and cost to do demolition.
        ProvStatMod landUseChange;
        PlayerStatArr totalDemolishCost;

        // Set to true when cumulative change in and use is enough to allow construction of a fort.
        bool savedEnoughLand = false;

        // Iterate through all regular building traits, in the somewhat unpredictable order of the unordered map.
        std::vector<proptype_t> toDemolish;
        for (const std::pair<const proptype_t, std::unordered_set<uint64_t>> & pair : propertySet.getPropertiesByTypeMap()) {
            if (Properties::getBaseType(pair.first) == Properties::BaseTypes::trait && Traits::isTraitRegularBuilding(pair.first)) {

                // For simplicity, don't bother considering buildings which cannot be immediately demolished
                // because they allow other buildings in the province.
                for (proptype_t allowing : Traits::getAllowingTraits(pair.first)) {
                    if (propertySet.hasPropertiesOfType(allowing)) goto skipBuilding;
                }

                // Get count and stat modifier set. For simplicity, demolish entire stacks of stackable traits at once.
                provstat_t traitCount = Traits::propertyGetTraitCount(propertySet.getPropertyStorageUnchecked(*pair.second.begin()).getStorage());
                ProvStatModSet modifier = Traits::getProvinceStatModiferSet(pair.first, traitCount);

                // Ignore buildings which do not COST positive land, or buildings which PROVIDE land.
                if (modifier.getProvStatModValue(ProvStatIds::landUsed) <= 0 ||
                    modifier.doesModify(ProvStatIds::landAvailable)) {
                    goto skipBuilding;
                }

                // If demolition is possible, update cumulative land use and cost, add to list of buildings.
                landUseChange -= modifier.getProvStatMod(ProvStatIds::landUsed);
                totalDemolishCost += Traits::getTraitBuildingCost(NULL, pair.first, -traitCount);
                toDemolish.push_back(pair.first);

                // If when considering the cumulative land use change, building a fort is possible, stop searching buildings.
                if (!province.properties.doesLandUseModifierExceedLand(Traits::FORT_LAND_USE + landUseChange)) {
                    savedEnoughLand = true;
                    break;
                }
            }
            skipBuilding:;
        }

        // If we either failed to gain enough land, or we cannot afford to
        // demolish (PLUS THE RESERVE COST), give up.
        if (!savedEnoughLand || !getData()->canAffordBuilding(totalDemolishCost + reserveCost)) return false;

        // Otherwise, spend the demolition cost, remove all the buildings, return true because of success.
        getData()->spendBuildingCost(MiscUpdate::server(msg, &box), totalDemolishCost);
        for (proptype_t building : toDemolish) {
            province.properties.removeTrait(MiscUpdate::server(msg, &box), building);
        }
        return true;
    }

    void ThreatManagedAI::doImprovementBuildingPlacement(simplenetwork::Message & msg, bool limitedByManpower) {
        const AreasStat    * areasStat    = box.common.areasStat;
        const BuildingStat * buildingStat = box.common.buildingStat;
        const ThreatManagedWeights & weights = getData()->getWeights();

        // Find total land usage and availability from areas stat.
        provstat_t landUsed  = areasStat->getPlayerLandUsed(getData()->getId()),
                   landAvail = areasStat->getPlayerLandAvailable(getData()->getId());

        // Select appropriate regular building category depending on whether we are limited by manpower.
        unsigned int regularBuilding = (limitedByManpower ? BuildValuedStatIds::manpower : BuildValuedStatIds::currency);

        // If either no available land, or percentage usage is above threshold, default to land improvement.
        std::array<unsigned int, 2> buildCategories;
        if (landAvail <= 0 || ((landUsed * 100) / landAvail) >= weights.maximumUsedLandPercentageThreshold()) {
            buildCategories = { BuildValuedStatIds::land, regularBuilding };
        } else {
            buildCategories = { regularBuilding, BuildValuedStatIds::land };
        }


        while(true) {
            // Go through each build category, until we find one which has available buildings.
            // Give up if none are available.
            const std::multiset<BuildingStatInfo> * buildings = NULL;
            for (unsigned int category : buildCategories) {
                const std::multiset<BuildingStatInfo> & byCategory = buildingStat->getPlayerAvailableBuildingsByStat(getData()->getId(), category);
                if (!byCategory.empty()) {
                    buildings = &byCategory;
                    break;
                }
            }
            if (buildings == NULL) break;

            // If we cannot afford the building, give up.
            const BuildingStatInfo & buildingInfo = *buildings->begin();
            PlayerStatArr cost = Traits::getTraitBuildingCost(NULL, buildingInfo.building, 1);
            if (!getData()->canAffordBuilding(cost)) break;

            // Build the building.
            Province & province = box.common.map->getProvince(buildingInfo.provinceId);
            province.properties.changeTraitCount(MiscUpdate::server(msg, &box), buildingInfo.building, province.properties.getTraitCount(buildingInfo.building) + 1);
            getData()->spendBuildingCost(MiscUpdate::server(msg, &box), cost);
        }
    }

    provvalue_t ThreatManagedAI::getFortBuildDesirability(const Province & province) const {
        uint64_t ourId = getData()->getId();
        const ThreatManagedWeights & weights = getData()->getWeights();

        // We can't build a fort if there is a fort here already.
        if (province.properties.getPropertySet().hasPropertiesOfType(Properties::Types::fort)) return 0;

        // We cannnot ever build a fort, if the province has less available land than the size
        // of a fort, even if buildings are demolished.
        if (province.properties[ProvStatIds::landAvailable] < Traits::FORT_LAND_USE.getValue()) return 0;

        // We can't build a fort if there is an ongoing battle.
        if (province.battleControl->isBattleOngoing()) return 0;

        // As a base value for fort desirability, use the surrounding enemy threat. This is the most important value.
        provvalue_t surroundingThreat = getSurroundingEnemyThreat(province),
                    desirability = surroundingThreat;

        // A large standing army, up to the size required to counteract threat, contributes toward
        // desirability, because if a fort is built, these can be immediately garrisoned. This only
        // makes sense to do if the surrounding threat, (NOTE: Not threat balance), is above zero.
        if (surroundingThreat > 0) {
            // Work out the actual number of men needed to counteract the threat.
            // Any number of men above this, does not add additional desirability, because they would
            // be an attacking army.
            uint64_t requiredMenFromThreat = ((surroundingThreat * 4096) / weights.friendlyThreatCounteractPer4096()),
                     menUptoThreatCounteract = std::min(requiredMenFromThreat, province.armyControl.countUnitsOwnedBy(ourId));
            desirability += ((menUptoThreatCounteract * weights.fortDesirabilityPer4096Standing()) / 4096);
        }

        // A large province value in our edge province contributes to desirability.
        desirability += ((province.properties[ProvStatIds::provinceValue] * weights.fortDesirabilityFromProvinceValue()) / 100);

        // If we own the whole area which the province is in, (i.e: Owned count equals total count of provinces
        // within area), then add a bonus to desirability.
        std::pair<unsigned int, unsigned int> ownedAndTotal = box.common.areasStat->getOwnedAndTotalWithinArea(ourId, province.getAreaId());
        if (ownedAndTotal.first == ownedAndTotal.second) {
            desirability += weights.fortDesirabilityFromAreaOwnership();
        }

        // Find, for adjacent enemy provinces, the maximum attack value, and whether there is a nearby fort.
        // Make sure maxAttackValue is never below zero, (this could be encountered if enemy has really bad provinces).
        provvalue_t maxAttackValue = 0;
        bool hasNearbyFort = false;
        for (uint32_t adjProvId : province.getAdjacent()) {
            const Province & adjProv = box.common.map->getProvince(adjProvId);
            if (box.server->canAttackEachOther(ourId, adjProv.getProvinceOwner())) {
                maxAttackValue = std::max(maxAttackValue, getProvinceAttackValue(adjProv));
                hasNearbyFort |= adjProv.properties.getPropertySet().hasPropertiesOfType(Properties::Types::fort);
            }
        }
        // Add desirability for maximum attack value, and if we are nearby to a fort.
        desirability += ((maxAttackValue * weights.fortDesirabilityPer4096AttackValue()) / 4096);
        if (hasNearbyFort) desirability += weights.fortDesirabilityFromNearbyFort();

        // If the province does not have enough available land for a fort (because of buildings),
        // add appropriate (negative!) modifier since buildings will need to be demolished.
        if (province.properties.doesLandUseModifierExceedLand(Traits::FORT_LAND_USE)) {
            desirability += weights.fortDesirabilityIfNotEnoughLand();
        }

        return desirability;
    }

    provstat_t ThreatManagedAI::getBuildingReservedIncome(void) const {
        const ThreatManagedWeights & weights = getData()->getWeights();
        Province * bestProvince = info->getOwnedProvincesByValue().rbegin()->second;
        return std::min(
            (getData()->getCurrencyIncomePerTurn() * weights.buildingReservedIncome1024()) / 1024,
            (bestProvince->properties[ProvStatIds::currencyIncome] * weights.buildingReservedProvinceValuePercent()) / 100
        );
    }
}

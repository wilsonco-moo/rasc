/*
 * TMAIAttack.cpp
 *
 *  Created on: 5 Dec 2019
 *      Author: wilson
 *
 * This contains the implementation for AI phases which carry out
 * attacking AI behaviour, within the ThreatManagedAI class.
 * See ThreatManagedAI.h.
 */

#include "ThreatManagedAI.h"

#include <iostream>
#include <map>

#include "../../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../../common/gameMap/mapElement/types/Province.h"
#include "../../../common/gameMap/properties/trait/Traits.h"
#include "../../../common/gameMap/properties/PropertySet.h"
#include "../../../common/stats/types/UnitsDeployedStat.h"
#include "../../../common/gameMap/properties/Properties.h"
#include "../../../common/stats/types/ProvincesStat.h"
#include "../../../common/playerData/PlayerData.h"
#include "../../../common/util/numeric/Random.h"
#include "../../../common/gameMap/GameMap.h"
#include "../../../common/util/Misc.h"
#include "../../RascBaseGameServer.h"
#include "../../ServerBox.h"

namespace rasc {

    void ThreatManagedAI::doAttackArmyMovement(simplenetwork::Message & msg) {
        doAttackMovePushBorder(msg);
        doAttackMoveExisting(msg);
    }

    void ThreatManagedAI::doAttackMovePushBorder(simplenetwork::Message & msg) {
        const ThreatManagedWeights & weights = getData()->getWeights();
        GameMap & map = *box.common.map;
        Random & random = *box.common.random;
        uint64_t ourId = getData()->getId();

        // Value all provinces by desirability to move armies from across border.
        // Only pick positive value.
        std::multimap<provvalue_t, Province *, std::greater<provvalue_t>> provincesByValue;
        for (Province * adjProv : info->getAdjacentProvinces().vec()) {
            provvalue_t attackValue = getProvinceAttackFromBorderDesirabilityBalance(*adjProv);
            if (attackValue > 0) {
                provincesByValue.emplace(attackValue, adjProv);
            }
        }

        // If there are no worthwhile provinces for moving attacking armies, return and do nothing.
        if (provincesByValue.empty()) return;

        unsigned int mostValueCount = (info->getAdjacentProvinces().size() * weights.attackBorderPushPercentage()) / 100;
        mostValueCount = Misc::minmax(mostValueCount, 1u, (unsigned int)provincesByValue.size());


        std::vector<Province *> toMoveFrom;
        std::vector<uint64_t> armyIds;

        unsigned int idUpTo = 0;
        for (const std::pair<const provvalue_t, Province *> pair : provincesByValue) {
            if (idUpTo >= mostValueCount) break;

            Province & province = *pair.second;

            // Work out how many men need moving into this province. If it is too few, don't bother.
            uint64_t unitsToMoveIn = getUnitsRequiredToAttackAcrossBorder(province);
            if (unitsToMoveIn < weights.attackMinMoveSize()) {
                continue;
            }

            // Work out how many moveable units we have available in adjacent non-battle provinces.
            toMoveFrom.clear();
            uint64_t unitsAvailable = 0;
            for (uint32_t adjProvId : province.getAdjacent()) {
                Province & adjProv = map.getProvince(adjProvId);
                if (!adjProv.battleControl->isBattleOngoing()) {
                    uint64_t unitCount = adjProv.armyControl.countMoveableUnitsOwnedBy(ourId, box.common.turnCount);
                    if (unitCount > 0) {
                        toMoveFrom.push_back(&adjProv);
                        unitsAvailable += unitCount;
                    }
                }
            }

            // Ignore this province if it is too few.
            if (unitsAvailable < weights.attackMinMoveSize()) {
                continue;
            }

            // Otherwise, keep moving in armies from our random adjacent provinces
            // until we run out, or surpass the requirement.
            uint64_t actuallyMovedIn = 0;
            while(!toMoveFrom.empty() && actuallyMovedIn < unitsToMoveIn) {
                // Pick a random province from toMoveFrom, (get random index, swap with back, pop back).
                const size_t ind = random.sizetRange(0, toMoveFrom.size());
                Province * adjProv = toMoveFrom[ind];
                toMoveFrom[ind] = toMoveFrom.back();
                toMoveFrom.pop_back();

                // Move all armies owned by us, counting their units as we go.
                // Do this by recording army ids in a vector then moving. We can't move them
                // while iterating through them.
                armyIds.clear();
                for (const Army & army : adjProv->armyControl.getArmies()) {
                    if (army.getArmyOwner() == ourId && army.getTurnLastMoved() < box.common.turnCount) {
                        armyIds.push_back(army.getArmyId());
                        actuallyMovedIn += army.getUnitCount();
                    }
                }
                for (uint32_t armyId : armyIds) {
                    province.armyControl.moveArmy(MiscUpdate::server(msg, &box), adjProv->getId(), armyId);
                }
            }

            // If we got this far, we must have done something, so increment the counter.
            idUpTo++;
        }

    }

    void ThreatManagedAI::doAttackMoveExisting(simplenetwork::Message & msg) {
        //uint64_t ourId = getData()->getId();

        // Get a list of armies in enemy territory that we can move.
        // Do this because we can't move them while iterating through provinces
        // that contain armies.
        std::vector<std::pair<Province *, const Army *>> moveableArmies;
        findMoveableArmiesInEnemyLand(moveableArmies);

        // Iterate through all the moveable ones.
        // Assume that armies are not split - rely on auto-merging in battles.
        for (const std::pair<Province *, const Army *> & pair : moveableArmies) {
            Province & province = *pair.first;
            const Army & army = *pair.second;
            const ProvinceBattleController & battleControl = *province.battleControl;

            // This should not happen, but if it does we can't get any useful info about what is happening,
            // so ignore the province.
            if (!battleControl.isBattleOngoing()) continue;

            // If there is a proper (non-siege) battle going on, (or siege-battle against garrison), don't move out of it.
            if (battleControl.getDefenderCount() > 0 || GET_GARRISON_SIZE(province.properties) > 0) continue;

            unsigned int neededToComplete = battleControl.getUnitsLeftToCompleteSiege();
            if (army.getUnitCount() < neededToComplete) {
                // If there are too few men left to complete the siege, redistribute too small army.
                redistributeTooSmallArmy(msg, province, army);

            } else if (army.getUnitCount() - neededToComplete >= ProvinceBattleController::SIEGE_OVERALL_LOSSES) {
                // If there SIEGE_OVERALL_LOSSES more men than necessary, redistribute out of province.
                redistributeAwayFromSiege(msg, province, army, army.getUnitCount() - neededToComplete);
            }
        }
    }

    void ThreatManagedAI::redistributeTooSmallArmy(simplenetwork::Message & msg, Province & province, const Army & army) {
        GameMap & map = *box.common.map;
        Random & random = *box.common.random;
        RascBaseGameServer * server = box.server;
        uint64_t ourId = getData()->getId();
        std::vector<Province *> moveTo;

        // Find adjacent provinces containing friendly units, or owned by us.
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = map.getProvince(adjProvId);
            if (adjProv.getProvinceOwner() == ourId || adjProv.armyControl.countUnitsFriendly(ourId, server, &RascBaseGameServer::canAttackCxt) > 0) {
                moveTo.push_back(&adjProv);
            }
        }

        // If there is one pick it at random.
        if (!moveTo.empty()) {
            Province * moveToProv = moveTo[random.sizetRange(0, moveTo.size())];
            moveToProv->armyControl.moveArmy(MiscUpdate::server(msg, &box), province.getId(), army.getArmyId());
            return;
        }

        // Otherwise just look for any accessible adjacent province and move to it.
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = map.getProvince(adjProvId);
            if (adjProv.isProvinceAccessible()) {
                moveTo.push_back(&adjProv);
            }
        }

        // Again, if there is one move to it.
        if (!moveTo.empty()) {
            Province * moveToProv = moveTo[random.sizetRange(0, moveTo.size())];
            moveToProv->armyControl.moveArmy(MiscUpdate::server(msg, &box), province.getId(), army.getArmyId());
        }
    }

    void ThreatManagedAI::redistributeAwayFromSiege(simplenetwork::Message & msg, Province & province, const Army & army, uint64_t toMove) {
        GameMap & map = *box.common.map;
        std::multimap<provvalue_t, Province *, std::greater<provvalue_t>> provincesByValue;

        // Build a map of the value (of moving in attacking forces) of adjacent accessible provinces.
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = map.getProvince(adjProvId);
            if (adjProv.isProvinceAccessible()) {
                provvalue_t value = getProvinceMoveAttackingForceValue(adjProv);
                if (value > 0) provincesByValue.emplace(value, &adjProv);
            }
        }

        // Work out how many of these we can move armies into, if that is none give up.
        // Each province will get ProvinceBattleController::SIEGE_OVERALL_LOSSES units, except the first
        // one which will get the difference.
        unsigned int moveableInto = std::min((uint64_t)provincesByValue.size(), toMove / ProvinceBattleController::SIEGE_OVERALL_LOSSES);
        if (moveableInto == 0) return;
        uint64_t firstSize = toMove - (moveableInto - 1) * ProvinceBattleController::SIEGE_OVERALL_LOSSES;

        // Iterate through the map, at most moveableInto times.
        unsigned int idUpTo = 0;
        bool first = true;
        for (const std::pair<const provvalue_t, Province *> pair : provincesByValue) {
            if (idUpTo >= moveableInto) break;

            // Get number to move, using firstSize for the first one.
            uint64_t toMoveThisTime;
            if (first) {
                toMoveThisTime = firstSize;
                first = false;
            } else {
                toMoveThisTime = ProvinceBattleController::SIEGE_OVERALL_LOSSES;
            }

            // Place that number of armies in the adjacent province.
            pair.second->armyControl.placeArmyOrAddUnits(msg, box.common.turnCount, getData(), toMoveThisTime);

            idUpTo++;
        }

        // At the end, subtract the total from the target province.
        province.armyControl.addUnits(MiscUpdate::server(msg, &box), army.getArmyId(), -((int64_t)toMove), box.common.turnCount);
    }

    uint64_t ThreatManagedAI::doAttackArmyPlacement(simplenetwork::Message & msg, uint64_t maxPlaceable) {
        const ThreatManagedWeights & weights = getData()->getWeights();

        std::multimap<provvalue_t, Province *, std::greater<provvalue_t>> provincesByValue;

        // Put all positive values provinces in provincesByValue.
        for (Province * adjProv : info->getAdjacentProvinces().vec()) {
            provvalue_t attackValue = getProvinceAttackBorderPlacementDesirabilityBalance(*adjProv);
            if (attackValue > 0) {
                provincesByValue.emplace(attackValue, adjProv);
            }
        }

        // If there are no worthwhile provinces for placing attacking armies, return and do nothing.
        if (provincesByValue.empty()) return 0;

        // Work out how many provinces are the "attackPlacementPercentage" percent most
        // worthwhile attacking. Do this from total adjacent province number, make sure it is at least one,
        // and make sure it is not more than the length of provincesByValue.
        unsigned int mostValueCount = (info->getAdjacentProvinces().size() * weights.attackPlacementPercentage()) / 100;
        mostValueCount = Misc::minmax(mostValueCount, 1u, (unsigned int)provincesByValue.size());

        // Work out the total value in the most valued provinces.
        provstat_t totalValue = 0;
        auto iter = provincesByValue.begin();
        for (unsigned int i = 0; i < mostValueCount; i++) {
            totalValue += iter->first;
            ++iter;
        }

        // The amount we should place overall is maxPlaceable. Seperately work out the
        // number we *actually* placed. This will be slightly different because of rounding.
        uint64_t attackPlaced = 0;

        // Go through the valued provinces to place units near them.
        unsigned int idUpTo = 0;
        for (const std::pair<const provvalue_t, Province *> pair : provincesByValue) {
            // Loop at most mostValueCount times.
            if (idUpTo >= mostValueCount) break;

            // Work out how many we should place, and try to place them.
            uint64_t toPlace        = (pair.first * maxPlaceable) / totalValue,
                     actuallyPlaced = placeUnitsNextToEnemy(msg, *pair.second, toPlace);

            // If we actually placed them, add onto the total we placed.
            if (actuallyPlaced > 0) {
                attackPlaced += actuallyPlaced;
                // Only increment idUpTo if we actually placed some, (so ignore provinces we could not place units in).
                // Note that by doing this we will never end up placing too many, but will place slightly
                // too few units because the value decreases as we go along.
                idUpTo++;
            }
        }

        // Return the number of units we *actually* placed in this phase.
        return attackPlaced;
    }

}

/*
 * TMAIUtility.cpp
 *
 *  Created on: 15 Dec 2019
 *      Author: wilson
 *
 * This includes general purpose utility methods, for
 * use elswhere within the ThreatManagedAI class.
 * See ThreatManagedAI.h.
 */

#include "ThreatManagedAI.h"

#include <iostream>
#include <map>

#include "../../../common/gameMap/mapElement/types/Province.h"
#include "../../../common/stats/types/UnitsDeployedStat.h"
#include "../../../common/playerData/PlayerData.h"
#include "../../../common/gameMap/GameMap.h"
#include "../../ServerBox.h"

namespace rasc {

    void ThreatManagedAI::findMoveableArmiesInEnemyLand(std::vector<std::pair<Province *, const Army *>> & vec) {
        uint64_t ourId = getData()->getId();
        FOR_EACH_UNOWNED_DEPL_PROV(box.common, ourId, province)
            for (const Army & army : province.armyControl.getArmies()) {
                if (army.getTurnLastMoved() < box.common.turnCount) {
                    vec.emplace_back(&province, &army);
                }
            }
        FOR_EACH_UNOWNED_DEPL_PROV_END
    }
}

/*
 * ServerPlayer.cpp
 *
 *  Created on: 20 Dec 2018
 *      Author: wilson
 */

#include "ServerPlayer.h"

namespace rasc {

    ServerPlayer::ServerPlayer(ServerBox & box, PlayerData * data) :
        Player(data),
        box(box) {
    }

    ServerPlayer::~ServerPlayer(void) {
    }
}

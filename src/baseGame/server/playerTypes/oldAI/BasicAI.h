/*
 * BasicAI.h
 *
 *  Created on: 31 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_PLAYERTYPES_OLDAI_BASICAI_H_
#define BASEGAME_SERVER_PLAYERTYPES_OLDAI_BASICAI_H_

#include <cstdint>
#include "../AIPlayer.h"

namespace rasc {

    class BasicAI : public AIPlayer {
    public:
        BasicAI(ServerBox & box, PlayerData * data);
        virtual ~BasicAI(void);

    protected:
        virtual void aiTurn(simplenetwork::Message & msg) override;

    };
}

#endif

/*
 * BasicAI.h
 *
 *  Created on: 31 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_PLAYERTYPES_OLDAI_BASICAI_H_
#define BASEGAME_SERVER_PLAYERTYPES_OLDAI_BASICAI_H_

#include <GL/gl.h>
#include <cstdint>

#include "../AIPlayer.h"

namespace rasc {

    class ProvincesStatInfo;

    class PrioritisedAI : public AIPlayer {
    public:
        PrioritisedAI(ServerBox & box, PlayerData * data);
        virtual ~PrioritisedAI(void);


    private:
        /**
         * This works out how worthwhile it would be to attack the specified province.
         * The province must be adjacent to our territory.
         */
        GLfloat provinceAttackValue(const ProvincesStatInfo & info, Province * prov);

        /**
         * This works out how worthwhile it would be to station a defence force on this
         * edge province.
         */
        GLfloat provinceDefendValue(const ProvincesStatInfo & info, Province * prov);

    protected:
        virtual void aiTurn(simplenetwork::Message & msg) override;

    };
}

#endif

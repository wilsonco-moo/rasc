/*
 * BasicAI.cpp
 *
 *  Created on: 31 Aug 2018
 *      Author: wilson
 */

#include "PrioritisedAI.h"

#include <functional>
#include <iostream>
#include <GL/gl.h>
#include <cstdint>
#include <cmath>
#include <map>

#include "../../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../../common/gameMap/mapElement/types/Province.h"
#include "../../../common/gameMap/mapElement/types/Area.h"
#include "../../../common/stats/types/UnitsDeployedStat.h"
#include "../../../common/stats/types/ProvincesStat.h"
#include "../../../common/stats/types/AreasStat.h"
#include "../../../common/playerData/PlayerData.h"
#include "../../../common/util/numeric/Random.h"
#include "../../gameMap/ServerGameMap.h"
#include "../../ServerBox.h"

namespace rasc {

    PrioritisedAI::PrioritisedAI(ServerBox & box, PlayerData * data) : AIPlayer(box, data) {
    }

    PrioritisedAI::~PrioritisedAI() {
    }

    #define RAND01 (((GLfloat)(box.common.random->uintRange(0, 32768))) / 32768.0f)

    // ======================= PROVINCE ATTACK VALUE ===================================

    // A random value added on to the attack value. Higher values make the AI more irrational and random,
    // values too low cause the AI to stagnate, and blob too slowly.
    #define RANDOM_PLUS 0.2f
    // The multiplier for the area modifier.
    #define AREA_MODIFIER_MULTIPLIER 0.4f

    // The bonus of an unowned province, higher values make AI target unowned land.
    #define UNOWNED_PLUS 1.0f
    // The bonus of a province in an area we own no provinces in. Higher values cause the AI to screw over other AIs more.
    #define FOOTHOLD_BONUS 1.2f
    // The maximum value for the area modifier, higher values make the AI try to make a complete area more.
    #define MAX_AREA_MODIFIER 3.0f

    // Sizing factors
        // We will get a tiny nation bonus on this province if...
        // ... their size is this value or less
        #define SIZING_TINY_NATION_MAX_SIZE 3
        // ... our size is this value or greater
        #define SIZING_TINY_NATION_OPRESSOR_MIN_SIZE 6
        // this is the tiny nation bonus value
        #define SIZING_TINY_NATION_PLUS 4.0f

        // We will get a larger nation bonus on this province if...
        // ... they are at least this number of provinces
        #define SIZING_LARGER_NATION_MIN_SIZE 12
        // ... they are at least this many times larger than us
        #define SIZING_LARGER_NATION_FACTOR 2.5f
        // ... our size is this value or larger
        #define SIZING_LARGER_NATION_MIN_OUR_SIZE 4
        // this is base larger nation bonus
        #define SIZING_LARGER_NATION_PLUS 3.0f

    // The bonus value we get from the overall stat modifier in the province (the province value).
    // The province value is multiplied by this, e.g: a province value of +50% gives half this bonus.
    #define OVERALL_STAT_MODIFIER_BONUS 2.0f


    GLfloat PrioritisedAI::provinceAttackValue(const ProvincesStatInfo & info, Province * prov) {
        // 0: Return -1000 if the province is owned by us - we cannot attack our own provinces.
        uint64_t ourId = getData()->getId();
        if (prov->getProvinceOwner() == ourId) return -1000.0f;

        GLfloat value = 0.0f;

        // 1: Increase value if the province is unowned.
        if (prov->getProvinceOwner() == PlayerData::NO_PLAYER) {
            value += UNOWNED_PLUS;

        // 2: Apply various sizing factors.
        } else {
            int ourSize   = (GLfloat)info.getOwnedProvinces().size(),
                theirSize = (GLfloat)box.common.provincesStat->getProvinceCount(prov->getProvinceOwner());

            if (theirSize <= SIZING_TINY_NATION_MAX_SIZE && ourSize >= SIZING_TINY_NATION_OPRESSOR_MIN_SIZE) {
                value += SIZING_TINY_NATION_PLUS;
            }

            GLfloat sizeMult = (((GLfloat)theirSize)/((GLfloat)ourSize));
            if (ourSize >= SIZING_LARGER_NATION_MIN_OUR_SIZE && theirSize >= SIZING_LARGER_NATION_MIN_SIZE && sizeMult >= SIZING_LARGER_NATION_FACTOR) {
                value += (sizeMult / SIZING_LARGER_NATION_FACTOR) * SIZING_LARGER_NATION_PLUS;
            }
        }

        // 2: Add on a random value
        value += RAND01 * RANDOM_PLUS;

        // 3: Add the area modifier. The fewer provinces left to complete the area, the higher the value.
        {
            std::pair<unsigned int, unsigned int> pair = this->box.common.areasStat->getOwnedAndTotalWithinArea(ourId, prov->getAreaId());

            GLfloat unOwned = pair.second - pair.first,
                      total = pair.second;

            value += fmin(MAX_AREA_MODIFIER, ((total / unOwned) - 1.0f) * AREA_MODIFIER_MULTIPLIER);
            if (unOwned == total) value += FOOTHOLD_BONUS;
        }

        // 4. Add a bonus for the general province value.
        value += OVERALL_STAT_MODIFIER_BONUS * (prov->properties[ProvStatIds::provinceValue] / 100.0f);

        return value;
    }



    GLfloat PrioritisedAI::provinceDefendValue(const ProvincesStatInfo & info, Province * prov) {
        GLfloat value = -1000.0f;
        for (uint32_t provId : prov->getAdjacent()) {
            Province & adjProv = box.map->getProvince(provId);
            if (adjProv.isProvinceAccessible()) {
                value = fmax(value, provinceAttackValue(info, &adjProv));
            }
        }
        return value;
    }



    // =================================================================================


    void PrioritisedAI::aiTurn(simplenetwork::Message & msg) {

        uint64_t ourId = getData()->getId();
        const ProvincesStatInfo & info = box.common.provincesStat->getOwnershipInfo(ourId);

        // ----------------- STEP 1: FOR EACH UNIT IN EDGE PROVINCE, ATTACK MOST VALUABLE PROVINCE -------------------

        std::map<GLfloat, Province *, std::greater<GLfloat>> provValues;

        // Note: Don't move armies in provinces where there are battles.
        for (Province * prov : info.getEdgeProvinces().vec()) {
            if (!prov->armyControl.getArmies().empty() && !prov->battleControl->isBattleOngoing()) {
                const Army & army = prov->armyControl.getArmies().front();
                if (army.getArmyOwner() == ourId) {

                    // For each army we own, add all adjacent provinces to the attackProvinces map.
                    for (uint32_t provId : prov->getAdjacent()) {
                        Province & adjProv = box.map->getProvince(provId);
                        if (adjProv.isProvinceAccessible()) {
                            provValues[provinceAttackValue(info, &adjProv)] = &adjProv;
                        }
                    }

                    // If the map is not empty...
                    if (!provValues.empty()) {
                        // Then move the army to the most desirable adjacent province.
                        Province * moveProv = provValues.begin()->second;
                        moveProv->armyControl.moveArmy(MiscUpdate::server(msg, &box), prov->getId(), army.getArmyId());
                        // Then clear the attack provinces map.
                        provValues.clear();
                    }
                }
            }
        }

        // --------------- STEP 2: FOR EACH UNIT IN CENTRE PROVINCE, MOVE AROUND RANDOMLY ------------

        // Note: Don't move armies in provinces where there are battles.
        for (Province * prov : info.getCentreProvinces().vec()) {
            if (!prov->armyControl.getArmies().empty() && !prov->battleControl->isBattleOngoing()) {
                const Army & army = prov->armyControl.getArmies().front();
                if (army.getArmyOwner() == ourId) {
                    if (!prov->getAdjacent().empty()) {
                        std::vector<Province *> accessibleAdj;
                        for (uint32_t adjId : prov->getAdjacent()) {
                            Province & adjProv = box.map->getProvince(adjId);
                            if (adjProv.isProvinceAccessible()) accessibleAdj.push_back(&adjProv);
                        }
                        Province * moveProv = accessibleAdj[box.common.random->sizetRange(0, accessibleAdj.size())];
                        moveProv->armyControl.moveArmy(MiscUpdate::server(msg, &box), prov->getId(), army.getArmyId());
                    }
                }
            }
        }

        // ------------ STEP 3: REINFORCE THE MOST DESIRABLE PROVINCE ------------------------

        for (Province * prov : info.getEdgeProvinces().vec()) {
            provValues[provinceDefendValue(info, prov)] = prov;
        }
        if (!provValues.empty()) {
            Province * reinforceProv = provValues.begin()->second;


            provstat_t currencyPerTurn = getData()->getCurrencyBalancePerTurn(),
                       totalCurrency   = getData()->getTotalCurrency(),
                       totalManpower   = getData()->getTotalManpower();

            #define LOOKAHEAD_TURNS 4

            if (totalCurrency >= 0 && currencyPerTurn > 0) {

                // The maximum we can place, is the minimum between our total manpower,
                // and the armies we can support currency-per-turn wise.
                provstat_t maxPlaceable = std::min(
                    totalManpower / AreasStat::MANPOWER_PER_UNIT,
                    ((currencyPerTurn * LOOKAHEAD_TURNS + totalCurrency) * UnitsDeployedStat::CURRENCY_COST_UNIT_SET_SIZE) / (UnitsDeployedStat::CURRENCY_COST_PER_UNIT_SET * LOOKAHEAD_TURNS)
                );

                if (maxPlaceable >= 4000) {
                    getData()->addTotalManpower(MiscUpdate::server(msg, &box), -AreasStat::MANPOWER_PER_UNIT * maxPlaceable, PlayerStatChangeReasons::armyBuild);
                    reinforceProv->armyControl.placeArmyOrAddUnits(msg, box.common.turnCount, getData(), maxPlaceable);
                }
            }
        }
    }
}

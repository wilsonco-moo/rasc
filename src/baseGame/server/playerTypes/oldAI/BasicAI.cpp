/*
 * BasicAI.cpp
 *
 *  Created on: 31 Aug 2018
 *      Author: wilson
 */

#include "BasicAI.h"

#include "../../../common/gameMap/mapElement/types/Province.h"
#include "../../../common/stats/types/ProvincesStat.h"
#include "../../../common/stats/types/AreasStat.h"
#include "../../../common/playerData/PlayerData.h"
#include "../../../common/util/numeric/Random.h"
#include "../../../common/gameMap/units/Army.h"
#include "../../gameMap/ServerGameMap.h"
#include "../../ServerBox.h"

namespace rasc {

    BasicAI::BasicAI(ServerBox & box, PlayerData * data) : AIPlayer(box, data) {
    }

    BasicAI::~BasicAI(void) {
    }

    void BasicAI::aiTurn(simplenetwork::Message & msg) {

        uint64_t ourId = getData()->getId();
        const ProvincesStatInfo & info = box.common.provincesStat->getOwnershipInfo(ourId);

        std::vector<Province *> moveProvs;

        for (int i = 0; i < (int)Army::MAX_ARMY_MOVES; i++) {

            for (Province * prov : info.getEdgeProvinces().vec()) {
                if (!prov->armyControl.getArmies().empty()) {
                    const Army & army = prov->armyControl.getArmies().front();
                    if (army.getArmyOwner() == ourId) {

                        for (uint32_t adjProvId : prov->getAdjacent()) {
                            Province & adjProv = box.map->getProvince(adjProvId);
                            if (adjProv.getProvinceOwner() != ourId && adjProv.isProvinceAccessible()) {
                                moveProvs.push_back(&adjProv);
                            }
                        }

                        if (!moveProvs.empty()) {
                            Province * moveProv = moveProvs[box.common.random->sizetRange(0, moveProvs.size())];
                            moveProv->armyControl.moveArmy(MiscUpdate::server(msg, &box), prov->getId(), army.getArmyId());
                            moveProvs.clear();
                        }
                    }
                }
            }

            if (info.getAdjacentProvinces().empty()) return; // Do nothing else if we have the entire map.
        }

        auto iter = info.getEdgeProvinces().vec().begin();
        if (iter != info.getEdgeProvinces().vec().end()) {
            Province * placeProv = *iter;
            placeProv->armyControl.placeArmyOrAddUnits(msg, box.common.turnCount, getData(), box.common.areasStat->getPlayerManpowerIncome(ourId));
        }
    }

}

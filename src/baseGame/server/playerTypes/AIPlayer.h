/*
 * AIPlayer.h
 *
 *  Created on: 21 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_PLAYERTYPES_AIPLAYER_H_
#define BASEGAME_SERVER_PLAYERTYPES_AIPLAYER_H_

#include "ServerPlayer.h"

#include <cstdint>

namespace rasc {

    class Province;
    class UnifiedStatSystem;

    class AIPlayer : public ServerPlayer {
    public:

        AIPlayer(ServerBox & box, PlayerData * data);
        virtual ~AIPlayer(void);

        virtual bool haveFinishedTurn(void) override;
        virtual bool isHumanPlayer(void) const override;

        // ====================== AI CALCULATIONS ==================================

    private:

        /**
         * Returns true if we should abandon our turn because we have no provinces left.
         */
        bool updateHomeProvince(void);

    protected:

        /**
         * This is set to our home province, before aiTurn is called.
         * If we have no provinces, aiTurn is not called.
         * As such, this is always guaranteed to point at a province.
         * TODO: The stat system should track home provinces.
         */
        //Province * homeProvince;

    public:
        virtual bool startTurn(simplenetwork::Message & msg) override;

    protected:
        virtual void aiTurn(simplenetwork::Message & msg) = 0;

    };
}

#endif


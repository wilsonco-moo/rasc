/*
 * RascServerNetIntf.cpp
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#include "HumanPlayer.h"

#include <unordered_map>
#include <unordered_set>
#include <iostream>
#include <cstdlib>
#include <mutex>
#include <thread>

#include <sockets/plus/simpleNetworkServer/SimpleNetworkServer.h>
#include <sockets/plus/util/LockedIterable.h>

#include "../../common/playerData/PlayerDataSystem.h"
#include "../../common/stats/types/ProvincesStat.h"
#include "../../common/playerData/PlayerData.h"
#include "../../common/config/StaticConfig.h"
#include "../../common/config/ReleaseMode.h"
#include "../../common/gameMap/GameMap.h"
#include "../../common/GameMessages.h"
#include "../RascBaseGameServer.h"
#include "../ServerBox.h"

namespace rasc {

    HumanPlayer::HumanPlayer(ServerBox & box) :
        ServerPlayer(box, NULL),
        lobbyChosenProvince(false) {
    }

    HumanPlayer::~HumanPlayer(void) {
        killableDestroy();
    }

    void HumanPlayer::onConnected(void) {
        // Note: We don't need to do any synchronisation here, onConnected method calls on the server side are implictly
        // synchronised to the server's main killableMutex.

        // First, send over the map name and the game version.
        simplenetwork::Message msg(ServerToClient::mapNameAndGameVersion);
        msg.writeString(box.mapFilename);
        msg.writeString(StaticConfig::RASC_VERSION_WITHOUT_PLATFORM);
        // Client waits for this message using a condition variable
        // in LoadingRoom::initialiseGameClient.
        send(msg);

        // Next send over the initial world data.
        msg.clear();
        msg.setId(ServerToClient::initialWorldData);
        // Write the game map initial world data to it. Supply false to isSaving since this is not a save operation.
        box.generateInitialData(msg, false);
        // And send the message.
        // Client waits for this, since they wait for ServerToClient::initialTurnCount in
        // RascClientNetIntf::readyToContinueToLobby, which they receive afterwards.
        send(msg);

        // Now we need to generate our player data, and send it to all players.
        // Do this with a server auto update, and assign our data to the new one.
        uint64_t ourId = box.common.playerDataSystem->newHumanPlayerData(MiscUpdate::serverAuto(box.server, &box), box.playerIdAssigner);
        setData(box.common.playerDataSystem->idToData(ourId));
        // Next we must send our player id and initial turn count.
        msg.clear();
        msg.setId(ServerToClient::initialTurnCount);
        // Send our player id. They can figure out the colour for themself, using the id and the player data system.
        msg.write64(ourId);
        // Finally, send the current turn count
        msg.write64(box.common.turnCount);
        // Client waits for this in RascClientNetIntf::readyToContinueToLobby, by refusing to switch
        // to the lobby room until they have received this.
        send(msg);

        // Add our player to the server's players list. This cannot be done until after we have PlayerData.
        ((RascBaseGameServer *)getServer())->onAddPlayer(this);
    }

    void HumanPlayer::receive(simplenetwork::Message & msg) {

        // Client --> Server  messages received.

        switch(msg.getId()) {

        // -----------------------------------------------------------------------------------
        case ClientToServer::finishTurn: {
            std::cout << "--- TURN: Player " << getData()->getName() << ": Received turn finish message.\n";
            std::lock_guard<std::recursive_mutex> lock(((RascBaseGameServer *)getServer())->getKillableMutex());
            getData()->changeHaveFinishedTurn(MiscUpdate::serverAuto(getServer(), &box), true);
            std::cout << "--- TURN: Player " << getData()->getName() << ": Completed turn.\n";
            ((RascBaseGameServer *)getServer())->tryToMoveToNextTurn();
            break;
        }
        // -----------------------------------------------------------------------------------
        case ClientToServer::miscUpdate:
            // In development mode, allow misc updates from clients, but print a warning.
            // Otherwise (in release mode), print a warning and ignore the misc update.
            #ifdef RASC_DEVELOPMENT_MODE
                std::cerr << "WARNING: Server received misc update from client " << getData()->getName() << ".\nThis is ONLY allowed in DEVELOPMENT MODE.\n";
                ((RascBaseGameServer *)getServer())->onReceiveMiscUpdate(msg, this);
            #else
                std::cerr << "WARNING: Server received misc update from client " << getData()->getName() << ".\nThis is NOT allowed in RELEASE MODE. They are likely trying to cheat.\n";
            #endif
            break;
        // -----------------------------------------------------------------------------------
        case ClientToServer::checkedUpdate:
            std::cout << "Server received checked update.\n";
            ((RascBaseGameServer *)getServer())->onReceiveCheckedUpdate(msg, this);
            break;
        // -----------------------------------------------------------------------------------
        case ClientToServer::lobbyProvinceRequest:
            // Don't allow the player to do a lobby province request AFTER they have already done it.
            if (!lobbyChosenProvince) {
                lobbyChosenProvince = ((RascBaseGameServer *)getServer())->onLobbyProvinceRequest(msg.read32(), this);
            }
            break;
        // -----------------------------------------------------------------------------------
        case ClientToServer::requestColourChange: {
            std::lock_guard<std::recursive_mutex> lock(((RascBaseGameServer *)getServer())->getKillableMutex());
            uint64_t playerId = msg.read64();
            uint32_t newColour = msg.read32();
            ((RascBaseGameServer *)getServer())->changePlayerColour(MiscUpdate::serverAuto(getServer(), &box), playerId, newColour);
            break;
        }
        // -----------------------------------------------------------------------------------
        default:
            std::cout << "Warning: Unknown message id: " << msg.getId() << " received.\n";
        }
    }

    void HumanPlayer::onDisconnected(void) {
        ((RascBaseGameServer *)getServer())->onRemovePlayer(this);
    }



    // ------------------------ TURN STUFF --------------------------

    bool HumanPlayer::haveFinishedTurn(void) {
        // If we have no provinces, we have always finished our turn, since there is nothing for us to do.
        return getData()->getHaveFinishedTurn() || box.common.provincesStat->getProvinceCount(getData()->getId()) == 0;
    }
    bool HumanPlayer::startTurn(simplenetwork::Message & msg) {
        getData()->changeHaveFinishedTurn(MiscUpdate::server(msg, &box), false);
        std::cout << "--- TURN: Player " << getData()->getName() << ": Starting turn, sending turn start message.\n";
        send(ServerToClient::startTurn);
        // Return value does not matter here.
        return false;
    }

    bool HumanPlayer::isHumanPlayer(void) const {
        return true;
    }
}

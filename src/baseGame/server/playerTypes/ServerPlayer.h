/*
 * ServerPlayer.h
 *
 *  Created on: 20 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_PLAYERTYPES_SERVERPLAYER_H_
#define BASEGAME_SERVER_PLAYERTYPES_SERVERPLAYER_H_

#include "../Player.h"

namespace rasc {

    class ServerBox;

    /**
     * This is a player for the server side. This is a simple extension to the Player class,
     * which includes a pointer to a GameMap, since the server does not use the RascBox system.
     */
    class ServerPlayer : public Player {
    protected:
        ServerBox & box;

    public:
        ServerPlayer(ServerBox & box, PlayerData * data);
        virtual ~ServerPlayer(void);
    };

}

#endif

/*
 * HumanPlayer.h
 *
 * This represents a human player on the server. It is also, in fact, a network interface. This
 * class is essentially the server-side equivalent to RascClientNetIntf.
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_PLAYERTYPES_HUMANPLAYER_H_
#define BASEGAME_SERVER_PLAYERTYPES_HUMANPLAYER_H_

#include <sockets/plus/message/Message.h>
#include <sockets/plus/networkinterface/NetworkInterface.h>
#include "ServerPlayer.h"


namespace rasc {

    class HumanPlayer : public ServerPlayer, public simplenetwork::NetworkInterface {
    private:
        bool lobbyChosenProvince;

    public:
        HumanPlayer(ServerBox & box);
        virtual ~HumanPlayer(void);

        virtual void onConnected(void) override;
        virtual void receive(simplenetwork::Message & msg) override;
        virtual void onDisconnected(void) override;

    public:

        virtual bool haveFinishedTurn(void) override;
        virtual bool startTurn(simplenetwork::Message & msg) override;

        virtual bool isHumanPlayer(void) const override;

    };

}

#endif

/*
 * AIPlayer.cpp
 *
 *  Created on: 21 Aug 2018
 *      Author: wilson
 */

#include "AIPlayer.h"

#include "../../common/gameMap/mapElement/types/Province.h"
#include "../../common/stats/types/UnitsDeployedStat.h"
#include "../../common/stats/types/ProvincesStat.h"
#include "../../common/playerData/PlayerData.h"
#include "../gameMap/ServerGameMap.h"
#include "../ServerBox.h"

namespace rasc {

    AIPlayer::AIPlayer(ServerBox & box, PlayerData * data) :
        ServerPlayer(box, data) {
    }

    AIPlayer::~AIPlayer(void) {
    }

    // -------------------- TURN STUFF -------------------------

    bool AIPlayer::haveFinishedTurn(void) {
        // AI players have always finished their turn.
        return true;
    }
    bool AIPlayer::isHumanPlayer(void) const {
        return false;
    }

    // ========================== AI CALCULATIONS ===========================================


    bool AIPlayer::startTurn(simplenetwork::Message & msg) {

        const ProvincesStatInfo & info = box.common.provincesStat->getOwnershipInfo(getData()->getId());

        // If we have no provinces:
        // Return true if we have no deployed armies, (this will remove us from the server).
        // Return false if we still have deployed armies, since we cannot be removed from the server
        // while we still have deployed armies.
        if (info.getOwnedProvinces().size() == 0) {
            return (box.common.unitsDeployedStat->getUnitsDeployed(getData()->getId()) == 0);
        }

        // Take the AI turn, but only if we have some adjacent provinces.
        // If we have no adjacent provinces, we cannot do anything anyway.
        if (!info.getAdjacentProvinces().empty()) {
            aiTurn(msg);
        }

        // Otherwise, return false: We are not out of the game.
        return false;
    }

}

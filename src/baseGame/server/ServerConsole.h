/*
 * ServerConsole.h
 *
 *  Created on: 18 Mar 2019
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_SERVERCONSOLE_H_
#define BASEGAME_SERVER_SERVERCONSOLE_H_

#include <unordered_map>
#include <functional>
#include <string>
#include <vector>
#include <map>

#include "ServerBox.h"

namespace rasc {

    /**
     * This class provides the command-line stuff for running the server.
     */
    class ServerConsole {
    private:
        // Help text for each command.
        const static std::unordered_map<std::string, std::string> COMMAND_HELP;
        
        ServerBox box;
        bool continueRunning, * shouldRestartServer;
        std::map<std::string, std::function<void(const std::vector<std::string> &)>> commands;

    public:
        ServerConsole(bool * restartServer, uint16_t port, int aisToPlace, const std::string & mapFilename);
        virtual ~ServerConsole(void);

    private:
        void runServerCommand(void);
    };

}

#endif

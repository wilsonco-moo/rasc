/*
 * RascBaseGameServer.h
 *
 * This class is a subclass of SimpleNetworkServer, and coordinates the entire game.
 * One server instance can have one active game at once only.
 *
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_RASCBASEGAMESERVER_H_
#define BASEGAME_SERVER_RASCBASEGAMESERVER_H_

#include <unordered_map>
#include <cstdint>
#include <string>
#include <map>

#include <sockets/plus/simpleNetworkServer/SimpleNetworkServer.h>
#include <sockets/plus/message/Message.h>

#include "../common/gameMap/GameMap.h"
#include "Player.h"


namespace simplenetwork {
    class NetworkInterface;
}



namespace rasc {
    class Player;
    class HumanPlayer;
    class Army;
    class ServerBox;


    class RascBaseGameServer : public simplenetwork::SimpleNetworkServer {
    public:
        /**
         * The threshold below which colours of adjacent players
         * should be considered too close. This threshold must relate to the distance squared
         * (i.e: MultiCol::distanceSquared) of the player's LAB colour space colours.
         */
        static const unsigned int COLOUR_CLOSENESS_THRESHOLD;

    private:
        std::map<uint64_t, Player *> players; // Allows iterating through players in order.
        std::unordered_map<uint64_t, Player *> playersTable; // Allows fast Player lookup.

        /**
         * Our server box. We use this to access all global properties.
         */
        ServerBox & box;

    public:

        RascBaseGameServer(ServerBox & box);
        virtual ~RascBaseGameServer(void);

        virtual simplenetwork::NetworkInterface * createInterface(void) override;


    // ---------------------- TURN STUFF ---------------------------------

        void tryToMoveToNextTurn(void);
        void startNextTurn(void);

        void placeAIs(int count, simplenetwork::Message & msg);

        void onAddPlayer(Player * player);
        void onRemovePlayer(Player * player);

    // ----------------------- UPON RECEIVING A MISC UPDATE ---------------

        /**
         * This is run by a HumanPlayer when their client counterpart sends us a misc update.
         * When the server receives a misc update, it must do this as an atomic operation
         * to avoid any timing ambiguity:
         *     > Send the update back to the player that sent it, FIRST (avoid latency).
         *     > Send the update to all other players.
         *     > Update the server's internal GameMap,
         */
        void onReceiveMiscUpdate(simplenetwork::Message & msg, simplenetwork::NetworkInterface * source);

        /**
         * This is run by a HumanPlayer when their client counterpart sends us a checked update.
         * When the server receives a checked update, it must do this as an atomic operation
         * to avoid any timing ambiguity:
         *     > Process the checked update locally, to generate a misc update.
         *     > Process this misc update normally, using onReceiveMiscUpdate.
         */
        void onReceiveCheckedUpdate(simplenetwork::Message & msg, HumanPlayer * source);

    // ------------------------- LOBBY ------------------------------------

        /**
         * This is called when a player selects a starting province in the lobby screen.
         * True is returned if we successfully choose a starting province, or false otherwise.
         * When this happens, the server must do this in an atomic operation:
         *     > Obtain the Province object requested by the player.
         *     > Get the owner player id of the province.
         *     > If the province is un-owned:
         *         - Claim the province for that player
         *         - Send the client back a success message
         *         - Return true
         *     > If the province is owned:
         *         - If the province is owned by an active human player:
         *             > Send the client back a failure message
         *             > Return false
         *         - Otherwise
         *             > Change that player's id to the id of the ownership of that province
         *             > Return true
         */
        bool onLobbyProvinceRequest(uint32_t provinceId, HumanPlayer * player);


    // ------------------------------ MISC ----------------------------------

        /**
         * Returns our player count.
         */
        size_t getPlayerCount(void);

        /**
         * Allows easy iteration through players.
         */
        simplenetwork::LockedIterable<std::map<uint64_t, Player *>> playersRaw(void);

        /**
         * Allows access to the player with the specified ID.
         */
        inline const Player * getPlayerById(uint64_t id) const {
            return players.at(id);
        }

        /**
         * This allows players to lock to our main mutex, on their own.
         */
        inline std::recursive_mutex & getKillableMutex(void) {
            return killableMutex;
        }

        /**
         * The only truly safe method to change the colour of a player.
         * This does the following things:
         *  > Goes through all provinces owned by the specified player:
         *    - Writes and applies misc updates to change the colour of those provinces.
         *  > Goes through all armies owned by the specified player:
         *    - Writes and applies misc updates to change the colour of those armies.
         *  > Finds the PlayerData for this player:
         *    - Writes and applies a misc update which changes the colour
         *      of the player data.
         *    - When this misc update is applied, the stat system is notified
         *      of the change to a player's colour.
         *  > Goes through all provinces which this player has deployed units:
         *    - Tells the battle controller to re-send the colour of it's main attacker
         *      player, so siege displays get updated.
         */
        void changePlayerColour(MiscUpdate update, uint64_t playerId, uint32_t newColour);

        /**
         * Tries to remove all adjacent player colours which are too close according to
         * COLOUR_CLOSENESS_THRESHOLD. If more than the total number of players on the map's
         * colours are changed, we give up, and assume that the colours cannot be satisfied.
         *
         * This is done by repeatedly finding the closest pair of players, randomising one
         * of the colours (using changePlayerColour), until there is no pair below the threshold.
         */
        void removeClosePlayerColours(MiscUpdate update);

        /**
         * Returns true if the specified player exists, and false otherwise.
         */
        bool doesPlayerExist(uint64_t playerId) const;

        /**
         * Returns whether the specified player is human or not, by their ID.
         * Note that this can (and will) change depending on human players joining and leaving the game.
         * If the specified player id does not exist, this will always return false.
         */
        bool isHumanPlayer(uint64_t playerId) const;

        /**
         * Returns true if the first AI player takes their turn first, and false
         * if the second AI player takes their turn first.
         */
        static inline bool aiTurnOrder(uint64_t firstAiPlayer, uint64_t secondAiPlayer) {
            return firstAiPlayer < secondAiPlayer;
        }

        /**
         * This method is used by the battle controller to figure out whether two players
         * are actually allowed to attack each other. Currently, this simply returns true
         * if the two player IDs are different, (i.e: Players can attack every other player,
         * but not themself, as that wouldn't make any sense).
         * If a system of diplomacy is added in the future, this method should be modified
         * to stop allied players attacking each other, (or similar).
         */
        inline bool canAttackEachOther(uint64_t firstPlayerId, uint64_t secondPlayerId) const {
            return firstPlayerId != secondPlayerId;
        }

        /**
         * This returns exactly the same thing as canAttackEachOther, except that this
         * is able to be used with ProvinceArmyController utility methods.
         * The context is assumed to be a RascBaseGameServer instance.
         */
        static bool canAttackCxt(void * context, uint64_t firstPlayerId, uint64_t secondPlayerId);

    // -------------------------------- Loading and saving ----------------------

        bool loadGame(const std::string & filename);
        bool saveGame(const std::string & filename);

    };
}

#endif

/*
 * ServerBox.h
 *
 *  Created on: 21 Feb 2019
 *      Author: wilson
 */

#ifndef BASEGAME_SERVER_SERVERBOX_H_
#define BASEGAME_SERVER_SERVERBOX_H_

#include <string>

#include "../common/update/TopLevelRascUpdatable.h"
#include "../common/util/CommonBox.h"

namespace rasc {

    class RascBaseGameServer;
    class GameMap;
    class ServerGameMap;
    class PlayerDataSystem;
    class UniqueIdAssigner;
    class ServerBattleScheduler;
    class UnifiedStatSystem;
    class AreasStat;
    class ContinentsStat;
    class ProvincesStat;
    class UnitsDeployedStat;
    class ServerMetricsStat;

    /**
     * The purpose of this class is simply to store, for public access, all components of the rasc server.
     * This class is the server-side equivalent of RascBox, and should help to tidy up all components of the server.
     *
     * The server side box will be much simpler than the client:
     *  > There is no complex multi-threaded loading sequence on the server side, as we don't have to remain responsive
     *    while loading.
     *  > We also don't have multiple loading stages, as initialisation of the server is monolithic. On the client we
     *    have the title room, the main menu (with background texture loading), the loading room, the lobby room and the game room.
     */
    class ServerBox : public TopLevelRascUpdatable {
    private:
        // Don't allow copying: We hold raw resources.
        ServerBox(const ServerBox & other);
        ServerBox & operator = (const ServerBox & other);
    public:

        // ===================== Global server properties ========================

        const std::string mapFilename;
        const int aisToPlace;

        // Whether we are a dedicated server, or a background server running on the client-side.
        // This affects whether we can prompt the user on the command-line or not.
        const bool isDedicatedServer;

        // ===================== MAIN SERVER CLASSES =============================

        /**
         * The game's map.
         */
        ServerGameMap * map;

        /**
         * The most important class here: The server class itself. This is a SimpleNetworkServer.
         * This must be the last class to be destroyed.
         */
        RascBaseGameServer * server;

        /**
         * Unique id assigners for both players and armies. This allows persistent army and player
         * id generation, even after saving and loading.
         * These must added as rasc updatables last, as they are rasc updatable classes
         * only present on the server.
         */
        UniqueIdAssigner * playerIdAssigner, * armyIdAssigner, * propertiesIdAssigner;

        /**
         * The battle scheduler. Also this must be defined last, since this is only present on the server.
         */
        ServerBattleScheduler * battleScheduler;
        
        /**
         * Server metrics stat. This is created only on request by the server console,
         * so CAN BE NULL OTHERWISE.
         */
        ServerMetricsStat * metricsStat;

        /**
         * The CommonBox. This stores classes common to both the server
         * and the client, like stats, map and player data system.
         */
        CommonBox common;


        ServerBox(const std::string & mapFilename, int aisToPlace, bool isDedicatedServer);
        virtual ~ServerBox(void);

        // -------------------------- For loading and saving server-side data, such as turn count -------------------------

        virtual void onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) override;
        virtual bool onReceiveInitialData(simplenetwork::Message & msg) override;
    };
}

#endif

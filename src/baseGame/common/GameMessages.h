/*
 * GameMessages.h
 *
 *  Created on: 21 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMESSAGES_H_
#define BASEGAME_COMMON_GAMEMESSAGES_H_


namespace rasc {

    /**
     *
     *     -------------------------------
     *     |  Server to Client messages  |
     *     -------------------------------
     *
     */
    namespace ServerToClient {
        enum {
            none, // Message ID 0 is none/unknown. This causes all
                  // default constructed messages to be flagged as unknown.

            startTurn,

            initialWorldData, // Updating messages
            mapUpdate,

            miscUpdate,    // Misc update: a generic unchecked map update
            checkedUpdate, // Checked update: an update which is first processed by the server.

            initialTurnCount,  // Initialisation messages

            lobbyProvinceResponse,   // Lobby messages

            mapNameAndGameVersion
        };
    }


    /**
     *
     *     -------------------------------
     *     |  Client to Server messages  |
     *     -------------------------------
     *
     */
    namespace ClientToServer {
        enum {
            none, // Message ID 0 is none/unknown. This causes all
                  // default constructed messages to be flagged as unknown.

            finishTurn,

            miscUpdate,       // A standard misc update.
            checkedUpdate,    // An update executed by the server, that generates a misc update.

            lobbyProvinceRequest, // Lobby messages

            requestColourChange
        };
    }


}


#endif

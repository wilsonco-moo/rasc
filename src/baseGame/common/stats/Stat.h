/*
 * Stat.h
 *
 *  Created on: 25 Oct 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_STATS_STAT_H_
#define BASEGAME_COMMON_STATS_STAT_H_

#include <cstdint>
#include <vector>

#include "../gameMap/provinceUtil/ProvinceStatTypes.h"
#include "../playerData/PlayerStatDef.h"

namespace rasc {

    class UnifiedStatSystem;
    class PlayerStatArr;
    class CommonBox;
    class Province;
    class MultiCol;

    /**
     * This class represents a type of stat, which can be registered to the unified
     * stat system.
     * A stat can receive updates about any type specified in the StatUpdateTypes enum.
     * If registered to a type of update, the stat will be notified whenever the appropriate
     * thing changes, through the appropriate method.
     */
    class Stat {
    private:
        friend class UnifiedStatSystem;
    protected:

        /**
         * This enum represents all of the types of updates
         * that a stat can receive.
         */
        class StatUpdateTypes {
        public:
            enum {
                changeProvinceOwnership,
                changeUnitsInProvince,
                createPropertyInProvince,
                deletePropertyFromProvince,
                changeStatsInProvince,
                addPlayer,
                removePlayer,
                changePlayerColour,
                changeBattleStatusInProvince,
                changePlayerStats,
                COUNT
            };
        };
        using StatUpdateType = unsigned int;

    public:
        Stat(void);
        virtual ~Stat(void);

    protected:

        // ==================================== Misc stuff ==========================================

        /**
         * This must cause the stat to initialise itself from existing data.
         * This may be called multiple times, so any data structures should be cleared first here.
         * The GameMap and PlayerDataSystem might be empty, like when starting a new game,
         * or contain data, like after loading a save.
         */
        virtual void initialise(CommonBox & box) = 0;

        /**
         * This should return all of the stat update types required for this stat.
         * This is used by unified stat system when we are registered.
         */
        virtual const std::vector<StatUpdateType> & getUpdateTypes(void) const = 0;

        // ===================================== Stat change methods ================================

        /**
         * Called when a province changes ownership.
         * (Must be called by Province, when it receives a claim misc update).
         * Provided is the relevant province, the old player ID and the new player ID.
         * Note: This will only actually be called if something has definately changed,
         *       i.e: The old owner is different to the new owner.
         */
        virtual void onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner);

        /**
         * Called when units, owned by the specified player ID, are added or removed from the province.
         * (Must be called by ProvinceArmyController, when it receives a misc update which moves or
         * adds units to an army).
         * Provided is the relevant province, the army owner, and the number of units added or removed.
         * unitsAdded will be a positive value if armies are added, or a negative value
         * if units are removed.
         * Note: This will only actually be called if something has definitely changed,
         *       i.e: The number of units added can be assumed to be non-zero.
         *       The unitsAdded will also never bring the total number of units deployed, for any
         *       player or overall, to a value below zero. The army owner will also never be NO_PLAYER.
         */
        virtual void onChangeUnitsInProvince(Province & province, uint64_t armyOwner, int64_t unitsAdded);

        /**
         * Called when a property is added to a province, immediately AFTER adding the property
         * to the province's property set, but BEFORE updating the stats in the province, and
         * BEFORE any corresponding calls to onChangeStatsInProvince.
         */
        virtual void onCreatePropertyInProvince(Province & province, uint64_t propertyUniqueId);

        /**
         * Called when a property is deleted from a province, immediately BEFORE removing
         * the property from the province's property set, BEFORE updating the stats in the
         * province, and BEFORE any corresponding calls to onChangeStatsInProvince.
         */
        virtual void onDeletePropertyFromProvince(Province & province, uint64_t propertyUniqueId);

        /**
         * Called when the stats in a province change.
         * (Must be called by ProvinceProperties, when it receives a misc update adding
         * or removing a trait).
         * Provided is the relevant province, and a stat array of added stats.
         * addedStats represents the difference in value for each of the province stats.
         * They will be positive if the stat value increased, negative otherwise.
         * Note: When this is called, the province must now contain the NEW values for
         *       the stats. The old values can be worked out by subtracting addedStats
         *       from the province's current (NEW) stat values.
         * Note: This will only actually be called if something has definately changed,
         *       i.e: At least one of the stat difference values will be non-zero.
         */
        virtual void onChangeStatsInProvince(Province & province, const StatArr & addedStats);

        /**
         * Called when a new player is added.
         * (Must be called by PlayerDataSystem, when it receives a misc update adding a player,
         * or a missing child).
         * The player ID is provided.
         */
        virtual void onAddPlayer(uint64_t playerId);

        /**
         * Called when a player is removed.
         * (Must be called by PlayerDataSystem, when it receives a misc update removing a player).
         * This should be used by stats to remove data structures relating to that player. Warnings
         * should be printed if that player still holds stuff like provinces, or has units deployed.
         */
        virtual void onRemovePlayer(uint64_t playerId);

        /**
         * Called when the colour of a player is changed. Copies of the colours in both the RGB
         * and LAB colour spaces are required.
         * (Must be called by PlayerData, when it receives a misc update changing it's colour).
         * This represents the last part of changing a player's colour. By this point, all of
         * the colours in all of it's provinces must have already been changed, (the stat
         * system is not told about that happening).
         * Note: This MUST be called as the LAST step in changing a player colour. I.e: The colour
         *       of all owned provinces, and the colour within the PlayerData must have ALREADY changed.
         * Note: This will only be called if something has definitely changed,
         *       i.e: The new colour is definitely different to the old colour.
         */
        virtual void onChangePlayerColour(uint64_t playerId, uint32_t oldColour, const MultiCol & oldLabColour, uint32_t newColour, const MultiCol & newLabColour);

        /**
         * Called whenever a battle starts or finishes, AFTER updating the internal "isBattleOngoing"
         * flag within the province's battle controller.
         * This is called by ProvinceBattleController when it gets a message notifying
         * the start or end of a battle. This will only ever be called when a battle either starts
         * or ends, (so will never be called with the same status twice in a row).
         */
        virtual void onChangeBattleStatusInProvince(Province & province, bool isBattleOngoing);
        
        /**
         * Called whenever the cumulative stats owned by a player are changed.
         * This is called by PlayerData, when it receives a misc update making a change to its stats,
         * AFTER updating its stats internally.
         * Note: This will only ever be called if the stat change is definitely NON ZERO, i.e:
         * at least one player stat has changed.
         */
        virtual void onChangePlayerStats(uint64_t playerId, const PlayerStatArr & statChange, PlayerStatChangeReason reason);
    };
}

#endif

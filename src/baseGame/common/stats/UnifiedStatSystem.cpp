/*
 * UnifiedStatSystem.cpp
 *
 *  Created on: 25 Oct 2019
 *      Author: wilson
 */

#include "UnifiedStatSystem.h"

#include <array>

#include "../gameMap/properties/ProvinceProperties.h"

namespace rasc {

    UnifiedStatSystem::UnifiedStatSystem(void) :
        stats(),
        statsPerUpdateType({}),
        initialiseCalled(false),
        afterUpdateFunctions(),
        afterUpdateNeeded(false) {
    }

    UnifiedStatSystem::~UnifiedStatSystem(void) {
    }

    // ================================= Misc stuff =======================================

    void UnifiedStatSystem::registerStat(Stat * stat) {
        stats.push_back(stat);
        for (Stat::StatUpdateType updateType : stat->getUpdateTypes()) {
            statsPerUpdateType[updateType].push_back(stat);
        }
    }

    void UnifiedStatSystem::initialise(CommonBox & box) {
        for (Stat * stat : stats) {
            stat->initialise(box);
        }
        initialiseCalled = true;
        afterUpdateNeeded = true;
    }

    void UnifiedStatSystem::runAfterUpdateFunctions(void) {
        if (initialiseCalled && afterUpdateNeeded) {
            afterUpdateFunctions.updateFunctions();
            afterUpdateNeeded = false;
        }
    }

    void UnifiedStatSystem::forceUpdate(void) {
        afterUpdateNeeded = true;
    }

    void * UnifiedStatSystem::addAfterUpdateFunction(std::function<void(void)> func) {
        return afterUpdateFunctions.addFunction(func);
    }
    
    void UnifiedStatSystem::removeAfterUpdateFunction(void * token) {
        afterUpdateFunctions.removeFunction(token);
    }

    // ============================ Public stat update methods ============================

    void UnifiedStatSystem::onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) {
        if (initialiseCalled) {
            afterUpdateNeeded = true;
            for (Stat * stat : statsPerUpdateType[Stat::StatUpdateTypes::changeProvinceOwnership]) {
                stat->onChangeProvinceOwnership(province, oldOwner, newOwner);
            }
        }
    }

    void UnifiedStatSystem::onChangeUnitsInProvince(Province & province, uint64_t armyOwner, int64_t unitsAdded) {
        if (initialiseCalled) {
            afterUpdateNeeded = true;
            for (Stat * stat : statsPerUpdateType[Stat::StatUpdateTypes::changeUnitsInProvince]) {
                stat->onChangeUnitsInProvince(province, armyOwner, unitsAdded);
            }
        }
    }

    void UnifiedStatSystem::onCreatePropertyInProvince(Province & province, uint64_t propertyUniqueId) {
        if (initialiseCalled) {
            afterUpdateNeeded = true;
            for (Stat * stat : statsPerUpdateType[Stat::StatUpdateTypes::createPropertyInProvince]) {
                stat->onCreatePropertyInProvince(province, propertyUniqueId);
            }
        }
    }

    void UnifiedStatSystem::onDeletePropertyFromProvince(Province & province, uint64_t propertyUniqueId) {
        if (initialiseCalled) {
            afterUpdateNeeded = true;
            for (Stat * stat : statsPerUpdateType[Stat::StatUpdateTypes::deletePropertyFromProvince]) {
                stat->onDeletePropertyFromProvince(province, propertyUniqueId);
            }
        }
    }

    void UnifiedStatSystem::onChangeStatsInProvince(Province & province, const StatArr & addedStats) {
        if (initialiseCalled) {
            afterUpdateNeeded = true;
            for (Stat * stat : statsPerUpdateType[Stat::StatUpdateTypes::changeStatsInProvince]) {
                stat->onChangeStatsInProvince(province, addedStats);
            }
        }
    }

    void UnifiedStatSystem::onAddPlayer(uint64_t playerId) {
        if (initialiseCalled) {
            afterUpdateNeeded = true;
            for (Stat * stat : statsPerUpdateType[Stat::StatUpdateTypes::addPlayer]) {
                stat->onAddPlayer(playerId);
            }
        }
    }

    void UnifiedStatSystem::onRemovePlayer(uint64_t playerId) {
        if (initialiseCalled) {
            afterUpdateNeeded = true;
            for (Stat * stat : statsPerUpdateType[Stat::StatUpdateTypes::removePlayer]) {
                stat->onRemovePlayer(playerId);
            }
        }
    }

    void UnifiedStatSystem::onChangePlayerColour(uint64_t playerId, uint32_t oldColour, const MultiCol & oldLabColour, uint32_t newColour, const MultiCol & newLabColour) {
        if (initialiseCalled) {
            afterUpdateNeeded = true;
            for (Stat * stat : statsPerUpdateType[Stat::StatUpdateTypes::changePlayerColour]) {
                stat->onChangePlayerColour(playerId, oldColour, oldLabColour, newColour, newLabColour);
            }
        }
    }

    void UnifiedStatSystem::onChangeBattleStatusInProvince(Province & province, bool isBattleOngoing) {
        if (initialiseCalled) {
            afterUpdateNeeded = true;
            for (Stat * stat : statsPerUpdateType[Stat::StatUpdateTypes::changeBattleStatusInProvince]) {
                stat->onChangeBattleStatusInProvince(province, isBattleOngoing);
            }
        }
    }
    
    void UnifiedStatSystem::onChangePlayerStats(uint64_t playerId, const PlayerStatArr & statChange, PlayerStatChangeReason reason) {
        // Uncomment for player stats debug logging.
        // std::cout << "Change player stats: player: " << playerId << ", (manpower: " << statChange[PlayerStatIds::manpower] << ", currency: " << statChange[PlayerStatIds::currency] << "), reason: " << (unsigned int)reason << "\n";
        
        if (initialiseCalled) {
            afterUpdateNeeded = true;
            for (Stat * stat : statsPerUpdateType[Stat::StatUpdateTypes::changePlayerStats]) {
                stat->onChangePlayerStats(playerId, statChange, reason);
            }
        }
    }
}

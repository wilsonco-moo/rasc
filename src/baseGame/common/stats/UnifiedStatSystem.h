/*
 * UnifiedStatSystem.h
 *
 *  Created on: 25 Oct 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_STATS_UNIFIEDSTATSYSTEM_H_
#define BASEGAME_COMMON_STATS_UNIFIEDSTATSYSTEM_H_

#include <functional>
#include <cstdint>
#include <vector>
#include <array>

#include "../util/templateTypes/TokenFunctionList.h"
#include "Stat.h"

namespace rasc {
    class PlayerStatArr;
    class CommonBox;
    class MultiCol;

    /**
     * The unified stat system keeps track of various things, whenever they change.
     * The same unified stat system should be defined on both the client side and the
     * server side.
     * Stats are updated when relevant RascUpdatables RECEIVE misc updates. This means
     * the stat system needs no network-awareness.
     *
     * For example, if a province were to change ownership.
     *  1. The server (or something) wants to change ownership of a province to a new player id.
     *  2. A misc update defining the new province claim is generated.
     *  3. This misc update is received by the relevant province, on the server side, and all
     *     connected clients.
     *  4. The province updates it's ownership, and runs the onChangeProvinceOwnership method
     *     with appropriate parameters on the unified stat system.
     *
     * This way, all stats are always up-to-date, without having to manually run any stat updates,
     * which (using the old stat system), required iterating through every single province periodically.
     */
    class UnifiedStatSystem {
    private:
        std::vector<Stat *> stats;
        std::array<std::vector<Stat *>, Stat::StatUpdateTypes::COUNT> statsPerUpdateType;

        // This is set to true when initialise is called.
        // Before that, any public stat updates will be ignored.
        bool initialiseCalled;

        // This stores after update functions.
        TokenFunctionList<void(void)> afterUpdateFunctions;

        // This decides whether after update functions need to be called.
        // This is only relevant on the client-side.
        bool afterUpdateNeeded;

    public:
        UnifiedStatSystem(void);
        ~UnifiedStatSystem(void);

        // ==================================== Misc stuff ==========================================

        /**
         * Registers a stat with the stat system.
         * This must be called ONCE and ONLY ONCE for each stat.
         */
        void registerStat(Stat * stat);

        /**
         * This causes all stats to initialise themself from initial data.
         * This may be called multiple times, so all data structures are cleared first here.
         * The GameMap and PlayerDataSystem might be empty, like when starting a new game,
         * or contain data, like after loading a save.
         *
         * NOTE: Stats must be registered BEFORE calling this.
         * ALSO: All public stat update methods will do nothing until after this has been called.
         */
        void initialise(CommonBox & box);

        /**
         * This runs all after update functions, IF AND ONLY IF something
         * has changed since the last time this function was called.
         * This will only do anything if initialise has already been called.
         */
        void runAfterUpdateFunctions(void);

        /**
         * Ensures that next time runAfterUpdateFunctions is called, update functions will be
         * called. This should be called at the start of each turn on the client side, to ensure
         * that UI is updated at least once per turn, even if the stat system saw no changes.
         */
        void forceUpdate(void);

        /**
         * Adds an after update function, which will be called when
         * runAfterUpdateFunctions is run, if necessary.
         * Returns a token which should be used in removeAfterUpdateFunction.
         */
        void * addAfterUpdateFunction(std::function<void(void)> func);
        
        /**
         * Removed an after update function added by addAfterUpdateFunction. All
         * after update functions MUST be removed before we are destroyed.
         */
        void removeAfterUpdateFunction(void * token);

        // ============================= PUBLIC STAT UPDATE METHODS =====================

        /**
         * Called when a province changes ownership.
         * (Must be called by Province, when it receives a claim misc update).
         * Provided is the relevant province, the old player ID and the new player ID.
         * Note: This must only be called if something has definitely changed,
         *       i.e: The old owner is different to the new owner.
         * Note: This will do nothing until after initialise has been called.
         */
        void onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner);

        /**
         * Called when units, owned by the specified player ID, are added or removed from the province.
         * (Must be called by ProvinceArmyController, when it receives a misc update which moves or
         * adds units to an army).
         * Provided is the relevant province, the army owner, and the number of units added or removed.
         * unitsAdded will be a positive value if armies are added, or a negative value
         * if units are removed.
         * Note: This must only be called if something has definately changed,
         *       i.e: The number of units added must be non-zero.
         *       The unitsAdded must also never bring the total number of units deployed, for any
         *       player or overall, to a value below zero. The army owner must also never be NO_PLAYER.
         * Note: This will do nothing until after initialise has been called.
         */
        void onChangeUnitsInProvince(Province & province, uint64_t armyOwner, int64_t unitsAdded);

        /**
         * Called when a property is added to a province, immediately AFTER adding the property
         * to the province's property set, but BEFORE updating the stats in the province, and
         * BEFORE any corresponding calls to onChangeStatsInProvince.
         * Note: This will do nothing until after initialise has been called.
         */
        void onCreatePropertyInProvince(Province & province, uint64_t propertyUniqueId);

        /**
         * Called when a property is deleted from a province, immediately BEFORE removing
         * the property from the province's property set, BEFORE updating the stats in the
         * province, and BEFORE any corresponding calls to onChangeStatsInProvince.
         * Note: This will do nothing until after initialise has been called.
         */
        void onDeletePropertyFromProvince(Province & province, uint64_t propertyUniqueId);

        /**
         * Called when the stats in a province change.
         * (Must be called by ProvinceProperties, when it receives a misc update adding
         * or removing a trait).
         * Provided is the relevant province, and a stat array of added stats.
         * addedStats represents the difference in value for each of the province stats.
         * They will be positive if the stat value increased, negative otherwise.
         * Note: When this is called, the province must now contain the NEW values for
         *       the stats. The old values can be worked out by subtracting addedStats
         *       from the province's current (NEW) stat values.
         * Note: This must only be called if something has definately changed,
         *       i.e: At least one of the stat difference values must be non-zero.
         * Note: This will do nothing until after initialise has been called.
         */
        void onChangeStatsInProvince(Province & province, const StatArr & addedStats);

        /**
         * Called when a new player is added.
         * (Must be called by PlayerDataSystem, when it receives a misc update adding a player,
         * or a missing child).
         * The player ID is provided.
         * Note: This will do nothing until after initialise has been called.
         */
        void onAddPlayer(uint64_t playerId);

        /**
         * Called when a player is removed.
         * (Must be called by PlayerDataSystem, when it receives a misc update removing a player).
         * This should be used by stats to remove data structures relating to that player. Warnings
         * should be printed if that player still holds stuff like provinces, or has units deployed.
         * Note: This will do nothing until after initialise has been called.
         */
        void onRemovePlayer(uint64_t playerId);

        /**
         * Called when the colour of a player is changed. Copies of the colours in both the RGB
         * and LAB colour spaces are required.
         * (Must be called by PlayerData, when it receives a misc update changing it's colour).
         * This represents the last part of changing a player's colour. By this point, all of
         * the colours in all of it's provinces must have already been changed, (the stat
         * system is not told about that happening).
         * Note: This must only be called if something has definitely changed,
         *       i.e: The new colour is definitely different to the old colour.
         * Note: This MUST be called as the LAST step in changing a player colour. I.e: The colour
         *       of all owned provinces, and the colour within the PlayerData must have ALREADY changed.
         * Note: This will do nothing until after initialise has been called.
         */
        void onChangePlayerColour(uint64_t playerId, uint32_t oldColour, const MultiCol & oldLabColour, uint32_t newColour, const MultiCol & newLabColour);

        /**
         * Called whenever a battle starts or finishes, AFTER updating the internal "isBattleOngoing"
         * flag within the province's battle controller.
         * This is called by ProvinceBattleController when it gets a message notifying
         * the start or end of a battle. This will only ever be called when a battle either starts
         * or ends, (so will never be called with the same status twice in a row).
         * Note: This will do nothing until after initialise has been called.
         */
        void onChangeBattleStatusInProvince(Province & province, bool isBattleOngoing);
        
        /**
         * Called whenever the cumulative stats owned by a player are changed.
         * This is called by PlayerData, when it receives a misc update making a change to its stats,
         * AFTER updating its stats internally.
         * Note: This will only ever be called if the stat change is definitely NON ZERO, i.e:
         * at least one player stat has changed.
         */
        void onChangePlayerStats(uint64_t playerId, const PlayerStatArr & statChange, PlayerStatChangeReason reason);
    };
}

#endif

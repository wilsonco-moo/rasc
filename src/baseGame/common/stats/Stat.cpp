/*
 * Stat.cpp
 *
 *  Created on: 25 Oct 2019
 *      Author: wilson
 */

#include "Stat.h"

#include <iostream>

namespace rasc {

    Stat::Stat(void) {
    }
    Stat::~Stat(void) {
    }

    void Stat::onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) {
        std::cerr << "WARNING: Stat::" << "onChangeProvinceOwnership" << ": Received unimplemented update.\n";
    }

    void Stat::onChangeUnitsInProvince(Province & province, uint64_t armyOwner, int64_t unitsAdded) {
        std::cerr << "WARNING: Stat::" << "onChangeUnitsInProvince" << ": Received unimplemented update.\n";
    }

    void Stat::onCreatePropertyInProvince(Province & province, uint64_t propertyUniqueId) {
        std::cerr << "WARNING: Stat::" << "onCreatePropertyInProvince" << ": Received unimplemented update.\n";
    }

    void Stat::onDeletePropertyFromProvince(Province & province, uint64_t propertyUniqueId) {
        std::cerr << "WARNING: Stat::" << "onDeletePropertyFromProvince" << ": Received unimplemented update.\n";
    }

    void Stat::onChangeStatsInProvince(Province & province, const StatArr & addedStats) {
        std::cerr << "WARNING: Stat::" << "onChangeStatsInProvince" << ": Received unimplemented update.\n";
    }

    void Stat::onAddPlayer(uint64_t playerId) {
        std::cerr << "WARNING: Stat::" << "onAddPlayer" << ": Received unimplemented update.\n";
    }

    void Stat::onRemovePlayer(uint64_t playerId) {
        std::cerr << "WARNING: Stat::" << "onRemovePlayer" << ": Received unimplemented update.\n";
    }

    void Stat::onChangePlayerColour(uint64_t playerId, uint32_t oldColour, const MultiCol & oldLabColour, uint32_t newColour, const MultiCol & newLabColour) {
        std::cerr << "WARNING: Stat::" << "onChangePlayerColour" << ": Received unimplemented update.\n";
    }

    void Stat::onChangeBattleStatusInProvince(Province & province, bool isBattleOngoing) {
        std::cerr << "WARNING: Stat::" << "onChangeBattleStatusInProvince" << ": Received unimplemented update.\n";
    }
    
    void Stat::onChangePlayerStats(uint64_t playerId, const PlayerStatArr & statChange, PlayerStatChangeReason reason) {
        std::cerr << "WARNING: Stat::" << "onChangePlayerStats" << ": Received unimplemented update.\n";
    }
}

/*
 * AreasStat.h
 *
 *  Created on: 25 Oct 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_STATS_TYPES_AREASSTAT_H_
#define BASEGAME_COMMON_STATS_TYPES_AREASSTAT_H_

#include <unordered_map>
#include <utility>
#include <cstdint>
#include <cstddef>
#include <vector>
#include <array>

#include "../Stat.h"

namespace rasc {

    /**
     * AreasStat keeps track of:
     *  > For each player, how many entire areas they own.
     *  > For each area, which player (if any) owns every province within it.
     *  > The total per-turn province stats, (like manpower, currency, etc), for each player.
     */
    class AreasStat : public Stat {
    public:

        /**
         * The amount of manpower that the placement of each unit requires.
         */
        const static provstat_t MANPOWER_PER_UNIT;

        /**
         * This defines, using X macros, the stats which are counted by AreasStat, (by SummedStats).
         * This is ALL that needs updating to get AreasStat to count new stats.
         * Stat name:             The name of the stat. This must match the name in ProvStatIds.
         * Stat name (caps):      The name of the stat, with a capital letter at the beginning. This is
         *                        needed for generating accessor method names.
         * Base value:            This is the amount of this stat which a player gains, without owning
         *                        any provinces or areas.
         * Owned area multiplier: A function representing the bonus to this stat gained when we own an
         *                        entire area.
         * Unowned multiplier:    A function representing the bonus to this stat gained when we do not
         *                        own an entire area.
         */
        #define RASC_AREAS_STAT_COUNTED_STATS                                                  \
            X(currencyIncome,      CurrencyIncome,      16,  ((value) + (value)/2), (value))   \
            X(currencyExpenditure, CurrencyExpenditure, 4,   (value),               (value))   \
            X(manpowerIncome,      ManpowerIncome,      200, ((value) + (value)/2), (value))   \
            X(manpowerExpenditure, ManpowerExpenditure, 50,  (value),               (value))   \
            X(landAvailable,       LandAvailable,       0,   (value),               (value))   \
            X(landUsed,            LandUsed,            0,   (value),               (value))

    private:
        /**
         * AreasStat::CountedStats provides IDs for each stat counted by AreasStat.
         * The special value CountedStats::COUNT represents the total number of stats we count.
         */
        class CountedStats {
        public:
            #define X(name, nameCaps, baseValue, ownedMult, unownedMult) name,
            enum {
                RASC_AREAS_STAT_COUNTED_STATS
                COUNT
            };
            #undef X
        };

        // --------------------- Summed stats -----------------------------

        /**
         * This simple class should represent all of the per-province
         * stats which require to be added up.
         * Extra stats can be added to provinces, but only need adding to
         * this if they actually need adding up.
         */
        class SummedStats {
        private:
            std::array<provstat_t, CountedStats::COUNT> statValues;
        public:
            /**
             * Creates a SummedStats, with the initial value of zero for all stats.
             */
            SummedStats(void);
            /**
             * This will create a SummedStats instance from a snapshot
             * of the current stats in this province.
             */
            SummedStats(Province & province);
            /**
             * This will create a SummedStats instance from all relevant
             * stats in a stat array. The stat array should have been provided
             * by the province.
             * This is a static method so that it does not conflict with the
             * constructor for creating from stat values.
             */
            static SummedStats fromStatArray(const StatArr & statArr);
            /**
             * Creates a SummedStats instance from the specified values for
             * all stats.
             */
            SummedStats(const std::array<provstat_t, CountedStats::COUNT> & statValues);

            // Operators for performing arithmetic on SummedStats.
            SummedStats operator + (const SummedStats & other);
            SummedStats & operator += (const SummedStats & other);
            SummedStats operator - (const SummedStats & other);
            SummedStats & operator -= (const SummedStats & other);
            bool operator == (const SummedStats & other) const;
            inline bool operator != (const SummedStats & other) const { return !(*this == other); }

            // Print a representation of this class, to stderr, for logging purposes.
            void print(void) const;

            // Should return a SummedStats, modified with the bonus for owning (or not owning) a whole area.
            SummedStats applyAreaBonus(bool ownWholeArea) const;

            // Allows access to our stat values.
            inline const std::array<provstat_t, CountedStats::COUNT> & getStatValues(void) const {
                return statValues;
            }
        };

        // ---------------- Static stuff ----------------------------------
        const static std::vector<Stat::StatUpdateType> updateTypes;
        const static std::array<provstat_t, CountedStats::COUNT> baseStatsArr;
        const static SummedStats baseStats;

        // ---------------- Per player info -------------------------------

        /**
         * This class stores, for each player, the following information:
         *  > The overall stats for this player.
         *  > How many complete areas this player owns.
         *  > For each area this player holds provinces in:
         *    - The number of provinces within this area they own.
         *    - The overall stats generated by this area.
         */
        class PerPlayerInfo {
        public:

            /**
             * An instance of this class should be created for each area the player holds provinces in.
             * This stores, for that area:
             *  > How many provinces, within this area, the player owns.
             *  > The overall stats generated by this area.
             *  > The total number of provinces contained within the area.
             */
            class PerAreaInfo {
            private:
                unsigned int provincesOwned,
                             maxProvinces;
                SummedStats areaStats;
            public:
                /**
                 * Default constructor: Initialises provinces owned and stats to zero.
                 * The total number of provinces in this area must be specified.
                 */
                PerAreaInfo(unsigned int maxProvinces);
                /**
                 * Increments our number of owned provinces, and adds the specified province's
                 * stats to our cumulative stats.
                 */
                void onGainProvince(Province & province);
                /**
                 * Decrements our number of owned provinces, and subtracts the specified province's
                 * stats from our cumulative stats.
                 */
                void onLoseProvince(Province & province);
                /**
                 * Adds the provided stat array to our cumulative area stats.
                 * This should be called when a province owned by us within the area has a change of stats.
                 */
                void onChangeStats(const StatArr & addedStats);
                /**
                 * Adds our stats, and our contribution to the total areas owned,
                 * to the specified PerPlayerInfo instance.
                 * This MUST BE DONE AFTER making modifications.
                 */
                void addToPerPlayerInfo(PerPlayerInfo & info);
                /**
                 * Subtracts our stats, and our contribution to the total areas owned,
                 * from the specified PerPlayerInfo instance.
                 * This MUST BE DONE BEFORE making modifications.
                 */
                void subtractFromPerPlayerInfo(PerPlayerInfo & info);
                /**
                 * This should be called before removing information about this area.
                 * Prints warnings to the log if we still own provinces in this area.
                 */
                void checkRemovalSafety(void);
                /**
                 * Returns true if the player owns NO provinces in this area.
                 * If this is true, then this NewAreasStat no longer needs storing.
                 */
                inline bool ownsNoProvinces(void) const { return provincesOwned == 0; }
                /**
                 * Returns true if the player owns the entire area.
                 */
                inline bool ownsEntireArea(void) const { return provincesOwned == maxProvinces; }
                /**
                 * Returns true if we own all except one province in the area.
                 * If we just lost a province, and this is true, then we just lost total
                 * area ownership.
                 */
                inline bool ownsAllButOne(void) const { return provincesOwned == maxProvinces - 1; }
                /**
                 * Returns the number of provinces owned by the player within this area.
                 */
                inline unsigned int getProvincesOwned(void) const { return provincesOwned; }
                /**
                 * Returns the maximum possible number of provinces the player could own in this area.
                 */
                inline unsigned int getMaxProvinces(void) const { return maxProvinces; }
            };

            // The overall stats for this player.
            SummedStats overallStats;
            // The number of areas this player owns completely.
            unsigned int areasOwned;
            // The player ID.
            uint64_t playerId;
            // Stores relevant info for each area we hold provinces in.
            std::unordered_map<uint32_t, PerAreaInfo> perAreaStats;

        public:
            PerPlayerInfo(uint64_t playerId);

        private:
            /**
             * Access the PerAreaInfo instance for the specified area ID, or creates one if
             * it doesn't already exist.
             */
            PerAreaInfo & getOrCreatePerAreaInfo(AreasStat & areasStat, uint32_t areaId);

        public:
            /**
             * This should be called before removing information about this player.
             * Prints warnings to the log if we still own provinces in any area.
             */
            void checkRemovalSafety(void);
            /**
             * Accesses the PerAreaInfo instance for the specified area ID. NULL is returned
             * if we have none assigned for this area.
             */
            const PerAreaInfo * accessPerAreaInfo(uint32_t areaId) const;
            /**
             * This should be called when this player gains a province. This will update our
             * area ownership and stats appropriately. This is done by accessing our area info,
             * subtracting it from overall stats, updating it, then adding it to overall stats again.
             */
            void onGainProvince(AreasStat & areasStat, uint64_t playerId, Province & province);
            /**
             * This should be called when this player loses a province. This will update our
             * area ownership and stats appropriately. This is done by accessing our area info,
             * subtracting it from overall stats, updating it, then adding it to overall stats again,
             * (but only if we still own provinces in that area).
             */
            void onLoseProvince(AreasStat & areasStat, uint64_t playerId, Province & province);
            /**
             * This should be called when stats are changed in a province owned by this player.
             * This updates overall and per area stats accordingly. This is done by first subtracting
             * the area from overall stats, updating the area, then re-adding it to overall stats.
             */
            void onChangeStats(AreasStat & areasStat, Province & province, const StatArr & addedStats);

            /**
             * Allows access to the total number of areas we own.
             */
            inline unsigned int getAreasOwned(void) const { return areasOwned; }
            /**
             * Allows access to our overall stats, from all areas we hold provinces in.
             */
            inline const SummedStats & getOverallStats(void) const { return overallStats; }
        };

        // --------------------------------------------------------------

        // Stores the relevant area and stat information for each player.
        std::unordered_map<uint64_t, PerPlayerInfo> infoPerPlayer;

        // For each area, how many provinces are contained within it.
        std::vector<unsigned int> provincesCountPerArea;

        // Stores, for each area, the complete owner, if they exist.
        std::unordered_map<uint32_t, uint64_t> areaToOwner;

        // Stores the total number of areas completely owned.
        unsigned int areasCompletelyOwned;

    public:
        AreasStat(void);
        virtual ~AreasStat(void);

    protected:

        // Misc methods
        virtual void initialise(CommonBox & box) override;
        virtual const std::vector<Stat::StatUpdateType> & getUpdateTypes(void) const override;

        // Stat update methods
        virtual void onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) override;
        virtual void onChangeStatsInProvince(Province & province, const StatArr & addedStats) override;
        virtual void onAddPlayer(uint64_t playerId) override;
        virtual void onRemovePlayer(uint64_t playerId) override;

    public:

        // ================================ Public stat accessor methods ===================================

        /**
         * Returns the total number of completely owned areas, for the specified
         * player ID. A player completely owns an area if they own every province
         * contained within it.
         * If the specified player does not exist, then they are (sensibly) assumed
         * to own zero areas.
         */
        unsigned int getAreaCount(uint64_t playerId) const;

        /**
         * Returns the owner player of the specified area, or PlayerData::NO_PLAYER
         * if none exists.
         * An area has an owner player if there exists a player who owns every
         * single province contained within it.
         */
        uint64_t getAreaOwner(uint32_t areaId) const;

        /**
         * Returns the total number of completely owned areas, across all players.
         */
        inline size_t getTotalAreasCompletelyOwned(void) const { return areasCompletelyOwned; }

        /**
         * These methods return, for the specified player, stats counted per province.
         * For example: getPlayerCurrency, getPlayerManpower, etc. For cumulative stats like
         * manpower and currency, these represent per-turn player values, but only from provinces.
         * For non-cumulative stats like land, these represent total values.
         *
         * Provinces contained within areas the player owns completely gain appropriate bonuses,
         * (see the definition of RASC_AREAS_STAT_COUNTED_STATS at the top of this file).
         *
         * If the specified player does not exist, then they are (sensibly) assumed to have
         * stats of zero.
         *
         * NOTE: These are not balance values, they are raw income or expenditure stats.
         *       For stuff like manpower and currency balance, use the appropriate methods
         *       from PlayerData.
         */
        #define X(name, nameCaps, baseValue, ownedMult, unownedMult) \
            provstat_t getPlayer##nameCaps(uint64_t playerId) const;
        RASC_AREAS_STAT_COUNTED_STATS
        #undef X

        /**
         * Returns the number of provinces the specified player owns within the specified
         * area.
         * If the specified player does not exist, then they are (sensibly) assumed
         * to own zero provinces within this area.
         */
        unsigned int getOwnedWithinArea(uint64_t playerId, uint32_t areaId) const;

        /**
         * Returns a std::pair, where the first value is the number of provinces owned by the specified
         * player in the specified area, and the second value is the total number of provinces contained
         * within the area.
         * If the specified player does not exist, then they are (sensibly) assumed
         * to own zero provinces within this area.
         */
        std::pair<unsigned int, unsigned int> getOwnedAndTotalWithinArea(uint64_t playerId, uint32_t areaId) const;


        // ========================== Stat multiplier utility methods ===========================

        /**
         * These methods are helpful for estimating stat values outside of AreasStat.
         * Names are generated using concatenation of stat name, so these methods can be
         * easily called with X macros.
         */

        /**
         * Applies the multiplier to a stat value, where the player DOES own an entire area,
         * for example: currencyIncomeApplyOwnedMultiplier(value).
         */
        #define X(name, nameCaps, baseValue, ownedMult, unownedMult)                                   \
            static inline provstat_t name##ApplyOwnedMultiplier(provstat_t value) {                    \
                return ownedMult;                                                                      \
            }
        RASC_AREAS_STAT_COUNTED_STATS
        #undef X

        /**
         * Applies the multiplier to a stat value, where the player DOES NOT own an entire area,
         * for example: currencyIncomeApplyUnownedMultiplier(value).
         */
        #define X(name, nameCaps, baseValue, ownedMult, unownedMult)                                   \
            static inline provstat_t name##ApplyUnownedMultiplier(provstat_t value) {                  \
                return unownedMult;                                                                    \
            }
        RASC_AREAS_STAT_COUNTED_STATS
        #undef X

        /**
         * Applies the multiplier to a stat value, where the status of whether the player owns
         * the entire area is provided with the "doesOwnEntireArea" parameter,
         * for example: currencyIncomeApplyMultiplier(value, doesOwnEntireArea)
         */
        #define X(name, nameCaps, baseValue, ownedMult, unownedMult)                                   \
            static inline provstat_t name##ApplyMultiplier(provstat_t value, bool doesOwnEntireArea) { \
                return doesOwnEntireArea ? (ownedMult) : (unownedMult);                                \
            }
        RASC_AREAS_STAT_COUNTED_STATS
        #undef X
    };
}

#endif

/*
 * ContinentsStat.cpp
 *
 *  Created on: 25 Oct 2019
 *      Author: wilson
 */

#include "ContinentsStat.h"

#include <iostream>

#include "../../gameMap/mapElement/types/Province.h"
#include "../../gameMap/mapElement/types/Area.h"
#include "../../playerData/PlayerDataSystem.h"
#include "../../playerData/PlayerData.h"
#include "../../gameMap/GameMap.h"
#include "../../util/CommonBox.h"

namespace rasc {

    const std::vector<Stat::StatUpdateType> ContinentsStat::updateTypes = {
        Stat::StatUpdateTypes::changeProvinceOwnership,
        Stat::StatUpdateTypes::addPlayer,
        Stat::StatUpdateTypes::removePlayer
    };

    ContinentsStat::ContinentsStat(void) :
        Stat(),
        provinceCountPerContinent(),
        playersMap(),
        continentToOwner() {
    }

    ContinentsStat::~ContinentsStat(void) {
    }

    // ------------------------------- Misc methods ---------------------------------------

    void ContinentsStat::initialise(CommonBox & box) {

        // Clear existing data structures
        provinceCountPerContinent.clear();
        playersMap.clear();
        continentToOwner.clear();

        // Count the number of provinces contained within each continent
        provinceCountPerContinent.insert(provinceCountPerContinent.end(), box.map->getContinentCount(), 0);
        for (Province * province : box.map->getProvinces()) {
            provinceCountPerContinent[box.map->getAreaRaw(province->getAreaId()).getContinentId()]++;
        }

        // Add all existing players from the player data system.
        for (const std::pair<const uint64_t, RascUpdatable *> pair : box.playerDataSystem->mapChildren()) {
            onAddPlayer(pair.first);
        }

        // Add all existing province ownership.
        for (Province * province : box.map->getProvinces()) {
            onChangeProvinceOwnership(*province, PlayerData::NO_PLAYER, province->getProvinceOwner());
        }
    }

    const std::vector<Stat::StatUpdateType> & ContinentsStat::getUpdateTypes(void) const {
        return updateTypes;
    }

    // Stores for each player,
    //  > How many continents they own
    //  > For each continent they own provinces in, how many provinces.
    // std::unordered_map<uint64_t, std::pair<unsigned int, std::unordered_map<uint32_t, unsigned int>>> playersMap;


    // Get continent count, increment number of provinces owned.
    // If that reaches total provinces in continent, increment continent count,
    // and mark the continent as being owned by that player.
    void ContinentsStat::onGainProvince(Province & prov, uint64_t playerId, std::pair<unsigned int, std::unordered_map<uint32_t, unsigned int>> & pair) {
        uint32_t contId = prov.getGameMap().getAreaRaw(prov.getAreaId()).getContinentId();
        unsigned int provincesOwned = ++pair.second[contId];
        if (provincesOwned == provinceCountPerContinent[contId]) {
            pair.first++;
            continentToOwner[contId] = playerId;
        }
    }

    // Get continent count, decrement number of provinces owned.
    // If that equals total in provinces minus one, the player JUST lost a continent,
    // so decrement the continent count, and mark the continent as no longer being
    // owned by that player. If the number is zero, erase since the player lost their
    // foothold in this continent.
    void ContinentsStat::onLoseProvince(Province & prov, uint64_t playerId, std::pair<unsigned int, std::unordered_map<uint32_t, unsigned int>> & pair) {
        uint32_t contId = prov.getGameMap().getAreaRaw(prov.getAreaId()).getContinentId();
        unsigned int provincesOwned = --pair.second[contId];
        if (provincesOwned == provinceCountPerContinent[contId] - 1) {
            pair.first--;
            continentToOwner.erase(contId);
        }
        if (provincesOwned == 0) {
            pair.second.erase(contId);
        }
    }

    // ----------------------------------- Stat updates -----------------------------------

    void ContinentsStat::onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) {

        if (oldOwner != PlayerData::NO_PLAYER) {
            auto iter = playersMap.find(oldOwner);
            if (iter == playersMap.end()) {
                std::cerr << "WARNING: AreasStat: Seemingly unknown player " << oldOwner << " lost province " << province.getName() << ".\n";
            } else {
                onLoseProvince(province, oldOwner, iter->second);
            }
        }

        if (newOwner != PlayerData::NO_PLAYER) {
            auto iter = playersMap.find(newOwner);
            if (iter == playersMap.end()) {
                std::cerr << "WARNING: AreasStat: Seemingly unknown player " << newOwner << " gained province " << province.getName() << ".\n";
            } else {
                onGainProvince(province, newOwner, iter->second);
            }
        }
    }

    void ContinentsStat::onAddPlayer(uint64_t playerId) {
        if (!playersMap.emplace(playerId, std::make_pair(0, std::unordered_map<uint32_t, unsigned int>())).second) {
            std::cerr << "WARNING: ContinentsStat: Player ID added twice: " << playerId << ".\n";
        }
    }

    void ContinentsStat::onRemovePlayer(uint64_t playerId) {
        auto iter = playersMap.find(playerId);
        if (iter == playersMap.end()) {
            std::cerr << "WARNING: ContinentsStat: Seemingly unknown player id " << playerId << " removed.\n";
        } else {
            playersMap.erase(iter);
        }
    }

    // ================================ Public stat accessor methods ===================================

    unsigned int ContinentsStat::getContinentCount(uint64_t playerId) const {
        auto iter = playersMap.find(playerId);
        if (iter == playersMap.cend()) {
            return 0;
        } else {
            return iter->second.first;
        }
    }

    uint64_t ContinentsStat::getContinentOwner(uint32_t continentId) const {
        std::unordered_map<uint32_t, uint64_t>::const_iterator iter = continentToOwner.find(continentId);
        if (iter == continentToOwner.cend()) {
            return PlayerData::NO_PLAYER;
        } else {
            return iter->second;
        }
    }

    unsigned int ContinentsStat::getContinentProvinceCount(uint32_t continentId) const {
        return provinceCountPerContinent[continentId];
    }
}

/*
 * ProvincesStat.h
 *
 *  Created on: 25 Oct 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_STATS_TYPES_PROVINCESSTAT_H_
#define BASEGAME_COMMON_STATS_TYPES_PROVINCESSTAT_H_

#include <unordered_map>
#include <utility>
#include <vector>
#include <set>

#include "../../util/templateTypes/MapVec.h"
#include "../Stat.h"

namespace rasc {

    class ProvincesStat;

    /**
     * This class stores the relevant data structures of provinces for each player,
     * required for the AI to function.
     * For provinces, in documentation and naming:
     *      owned: Owned by us
     *    unowned: Owned by another player
     */
    class ProvincesStatInfo {
    public:
        /**
         * A custom comparison type, which allows comparison of pairs of province
         * values and provinces, sorted lexicographically first by province value,
         * then by province ID. Province values are sorted in ascending order, so
         * a sorted container using this has low province values first, high province
         * values last.
         */
        class ProvCmp {
        public:
            bool operator()(const std::pair<provstat_t, Province *> & pair1,
                            const std::pair<provstat_t, Province *> & pair2) const;
        };

    private:
        friend class ProvincesStat;
        // The ID of the player this province info class relates to.
        uint64_t playerId;
        // This contains all of the accessible provinces we own.
        MapVec<Province *> ownedProvinces;
        // This is a subset of the accessible provinces we own: The ones which are immediately
        // adjacent to another player.
        MapVec<Province *> edgeProvinces;
        // These are all of the accessible owned provinces, which are NOT immediately adjacent
        // to another player.
        MapVec<Province *> centreProvinces;
        // These are the accessible provinces, owned by other players, which are immediately adjacent
        // to our edge provinces.
        MapVec<Province *> adjacentProvinces;
        // Stores the provinces, owned by the player, sorted lexicographically first by province
        // value, then by province ID.
        std::set<std::pair<provstat_t, Province *>, ProvincesStatInfo::ProvCmp> provincesByValue;

    public:
        ProvincesStatInfo(uint64_t playerId);

    private:
        // Assuming the province is owned:
        // adds the province to centreProvinces/adjacentProvinces, (and erases from the other),
        // depending on whether the province is adjacent to an accessible unowned province.
        void chooseEdgeOrCentre(Province & province);
        // Assuming the province is unowned and accessible:
        // adds the province to adjacentProvinces, if adjacent to a province we own,
        // erased from adjacentProvinces otherwise.
        void chooseAdjacent(Province & province);
        /**
         * This should be called when we gain a province. Will ensure:
         *  > countOfPlayersWithLand will be incremented if we have no owned provinces when this is called.
         *  > The method chooseEdgeOrCentre is called on all adjacent owned provinces.
         *  > All adjacent unowned accessible provinces are added to adjacentProvinces.
         *  > Specified province gets added to ownedProvinces.
         *  > Specified province gets removed from adjacentProvinces.
         *  > Specified province gets added to edgeProvinces or centreProvinces depending on adjacent.
         */
        void onGainProvince(ProvincesStat & stat, Province & province);
        /**
         * This should be called when we lose a privince. Will ensure:
         *  > All adjacent owned provinces are added to edgeProvinces, and removed from centreProvinces.
         *  > The method chooseAcjacent is run on all adjacent unowned accessible provinces.
         *  > Specified province gets removed from ownedProvinces.
         *  > Specified provinces gets removed from edgeProvinces and centreProvinces.
         *  > Specified province gets added to adjacentProvinces depending on adjacent.
         *  > countOfPlayersWithLand will be decremented if we have no owned provinces after this is called.
         */
        void onLoseProvince(ProvincesStat & stat, Province & province);
        /**
         * This should be called when stats change in a province that we own.
         * This updates the provincesByValue set.
         */
        void onChangeStatsInProvince(Province & province, const StatArr & addedStats);

    public:
        // --------------------------- Accessing data structures --------------------------
        /**
         * All of the provinces that we own.
         */
        inline const MapVec<Province *> & getOwnedProvinces(void) const { return ownedProvinces; }
        /**
         * All of the provinces we own, which are adjacent to accessible provinces
         * which we do not own.
         */
        inline const MapVec<Province *> & getEdgeProvinces(void) const { return edgeProvinces; }
        /**
         * All of the provinces we own, which are NOT adjacent to any accessible provinces
         * we do not own.
         */
        inline const MapVec<Province *> & getCentreProvinces(void) const { return centreProvinces; }
        /**
         * Accessible provinces adjacent to provinces that we own.
         */
        inline const MapVec<Province *> & getAdjacentProvinces(void) const { return adjacentProvinces; }
        /**
         * Allows access to a sorted set of our owned provinces. The provinces are
         * sorted in order of province value, (or by province ID when values are the same).
         * Provinces with a lower province value appear first.
         */
        inline const std::set<std::pair<provstat_t, Province *>, ProvincesStatInfo::ProvCmp> & getOwnedProvincesByValue(void) const { return provincesByValue; }

        // ----------------------------- Querying data structures -------------------------
        /**
         * Returns true if the provided province is one of our owned provinces.
         * Note that this is not a very useful thing to do, as the province's owner can easily
         * be checked with it's getProvinceOwner method.
         */
        inline bool isOwnedProvince(Province * province) const { return ownedProvinces.contains(province); }
        /**
         * Returns true if the provided province is one of our edge provinces.
         * This is likely faster (and more convenient) than the equivalent loop through it's adjacent provinces.
         */
        inline bool isEdgeProvince(Province * province) const { return edgeProvinces.contains(province); }
        /**
         * Returns true if the provided province is one of our centre provinces.
         * This is likely faster (and more convenient) than the equivalent loop through it's adjacent provinces.
         */
        inline bool isCentreProvince(Province * province) const { return centreProvinces.contains(province); }
        /**
         * Returns true if the provided province is one of our adjacent provinces,
         * (i.e: If it is adjacent to a province we own, but not owned by us).
         * This is likely faster (and more convenient) than the equivalent loop through it's adjacent provinces.
         */
        inline bool isAdjacentProvince(Province * province) const { return adjacentProvinces.contains(province); }

        // ----------------------------- General accessor methods -------------------------
        /**
         * Returns the player which we are relating to.
         */
        inline uint64_t getPlayerId(void) const { return playerId; }
    };



    /**
     * ProvincesStat keeps track of:
     *  > For each player, how many provinces they own.
     */
    class ProvincesStat : public Stat {
    private:
        friend class ProvincesStatInfo;

        const static std::vector<Stat::StatUpdateType> updateTypes;

        // Stores a ProvinceInfo which never has any provinces. This is the return
        // value of province info requests for NO_PLAYER of non-existent players.
        const static ProvincesStatInfo emptyProvinceInfo;

        // Stores a province info for each player id.
        std::unordered_map<uint64_t, ProvincesStatInfo> playerProvinceInfo;

        // Stores the total number of players who actually own at least one province.
        unsigned int countOfPlayersWithLand;

    public:
        ProvincesStat(void);
        virtual ~ProvincesStat(void);

    protected:

        // Misc methods
        virtual void initialise(CommonBox & box) override;
        virtual const std::vector<Stat::StatUpdateType> & getUpdateTypes(void) const override;

        // Stat update methods
        virtual void onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) override;
        virtual void onChangeStatsInProvince(Province & province, const StatArr & addedStats) override;
        virtual void onAddPlayer(uint64_t playerId) override;
        virtual void onRemovePlayer(uint64_t playerId) override;

    public:

        // ================================ Public stat accessor methods ===================================

        /**
         * Returns the total number of provinces owned by the specified
         * player ID.
         * If the specified player does not exist, then they are (sensibly) assumed
         * to have zero owned provinces in total.
         */
        unsigned int getProvinceCount(uint64_t playerId) const;

        /**
         * Returns the total number of players who actually own at least one province.
         */
        inline unsigned int getNumberOfPlayersWithLand(void) const { return countOfPlayersWithLand; }

        /**
         * Accesses the province info for the specified player.
         * If the specified player does not exist, then they are (sensibly) assumed
         * to own zero provinces, so an empty ProvinceInfo is returned.
         */
        const ProvincesStatInfo & getOwnershipInfo(uint64_t playerId) const;
    };
}

#endif

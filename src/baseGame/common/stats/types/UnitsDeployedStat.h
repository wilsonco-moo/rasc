/*
 * UnitsDeployedStat.h
 *
 *  Created on: 25 Oct 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_STATS_TYPES_UNITSDEPLOYEDSTAT_H_
#define BASEGAME_COMMON_STATS_TYPES_UNITSDEPLOYEDSTAT_H_

#include <vector>

#include <unordered_map>

#include "../Stat.h"

namespace rasc {

    /**
     * UnitsDeployedStat keeps track of:
     *  > For each player, how many (in total), units they have deployed.
     *  > For each player, how many units they have deployed in each province that they own.
     *  > For each player, how many units they have deployed in each province that they don't own.
     *  > ...And implicitly, the ability, for each player, to be able to iterate through all
     *    provinces which contain armies which they own.
     *  > How many units are deployed across the entire map.
     */
    class UnitsDeployedStat : public Stat {
    public:

        /**
         * These should be used for calculating army expenditure.
         * Every multiple of CURRENCY_COST_UNIT_SET_SIZE men,
         * should cost CURRENCY_COST_PER_UNIT_SET currency per turn.
         */
        constexpr static provstat_t CURRENCY_COST_PER_UNIT_SET  = 20,
                                    CURRENCY_COST_UNIT_SET_SIZE = 1000;

    private:
        const static std::vector<Stat::StatUpdateType> updateTypes;

        // Stores, for each player, how many units they have deployed.
        std::unordered_map<uint64_t, uint64_t> unitsDeployedPerPlayer;

        // Stores the total number of deployed units, across the entire map.
        uint64_t totalUnitsDeployed;

        // Stores, for each player (ID), for each province in which they have deployed units,
        // how many units they have deployed.
        std::unordered_map<uint64_t, std::unordered_map<uint32_t, uint64_t>> unitsDeployedPerProvince;
    public:
        UnitsDeployedStat(void);
        virtual ~UnitsDeployedStat(void);

    protected:

        // Misc methods
        virtual void initialise(CommonBox & box) override;
        virtual const std::vector<Stat::StatUpdateType> & getUpdateTypes(void) const override;

        // Stat update methods
        virtual void onAddPlayer(uint64_t playerId) override;
        virtual void onChangeUnitsInProvince(Province & province, uint64_t armyOwner, int64_t unitsAdded) override;
        virtual void onRemovePlayer(uint64_t playerId) override;

    public:

        // ================================ Public stat accessor methods ===================================

        /**
         * Returns the total number of units deployed on the entire map.
         * I.e: This is the sum of the total deployed units across every player.
         */
        inline uint64_t getTotalUnitsDeployed(void) const { return totalUnitsDeployed; }

        /**
         * Returns the total number of deployed units, across all armies owned
         * by the specified player ID.
         * If the specified player does not exist, then they are (sensibly) assumed
         * to have zero deployed units in total.
         */
        uint64_t getUnitsDeployed(uint64_t playerId) const;

        /**
         * Returns the total number of units which are deployed by the specified player
         * in the specified province.
         */
        uint64_t getUnitsDeployedInProvince(uint64_t playerId, uint32_t provinceId) const;

        /**
         * Returns a map of province ID to deployed units, which represents the
         * provinces which the specified player has deployed units in.
         * Note that the provided player ID must exist.
         */
        inline const std::unordered_map<uint32_t, uint64_t> & getProvincesWithUnitsDeployed(uint64_t playerId) const {
            return unitsDeployedPerProvince.at(playerId);
        }

        /**
         * Returns, for the specified player id, the total expenditure, in terms of
         * currency, for that player's armies.
         *
         * NOTE: This is not the currency balance, since this does not add
         *       income. For currency balance, use the appropriate method
         *       from PlayerData.
         */
        provstat_t getArmyExpenditure(uint64_t playerId) const;

        // ---------------------- Macros for iterating through provinces ---------------------------

        /**
         * Allows conveniently iterating through the provinces in which the specified
         * player has deployed units.
         *
         * commonBox: This should be a reference to a CommonBox.
         * playerId:  The player ID to look for.
         * provName:  The name of the variable to use for the province during iteration.
         *
         * For example:
         *
         * FOR_EACH_DEPL_PROV(box->common, playerId, someProvince)
         *     someProvince->doSomething();
         * FOR_EACH_DEPL_PROV_END
         */
        #define FOR_EACH_DEPL_PROV(commonBox, playerId, provName)                                                                                                \
            for (const std::pair<const uint32_t, uint64_t> & FOR_EACH_DEPL_PROV_pair : (commonBox).unitsDeployedStat->getProvincesWithUnitsDeployed(playerId)) { \
                Province & provName = (commonBox).map->getProvince(FOR_EACH_DEPL_PROV_pair.first);
        #define FOR_EACH_DEPL_PROV_END }

        /**
         * Allows conveniently iterating through the provinces which the specified player
         * player owns AND has deployed units.
         *
         * commonBox: This should be a reference to a CommonBox.
         * playerId:  The player ID to look for.
         * provName:  The name of the variable to use for the province during iteration.
         *
         * For example:
         *
         * FOR_EACH_OWNED_DEPL_PROV(box->common, playerId, someProvince)
         *     someProvince->doSomething();
         * FOR_EACH_OWNED_DEPL_PROV_END
         */
        #define FOR_EACH_OWNED_DEPL_PROV(commonBox, playerId, provName)                                                                                                \
            for (const std::pair<const uint32_t, uint64_t> & FOR_EACH_OWNED_DEPL_PROV_pair : (commonBox).unitsDeployedStat->getProvincesWithUnitsDeployed(playerId)) { \
                Province & provName = (commonBox).map->getProvince(FOR_EACH_OWNED_DEPL_PROV_pair.first);                                                               \
                if (provName.getProvinceOwner() == (playerId)) {
        #define FOR_EACH_OWNED_DEPL_PROV_END }}

        /**
         * Allows conveniently iterating through the provinces which the specified player
         * player does not own AND has deployed units.
         *
         * commonBox: This should be a reference to a CommonBox.
         * playerId:  The player ID to look for.
         * provName:  The name of the variable to use for the province during iteration.
         *
         * For example:
         *
         * FOR_EACH_OWNED_DEPL_PROV(box->common, playerId, someProvince)
         *     someProvince->doSomething();
         * FOR_EACH_OWNED_DEPL_PROV_END
         */
        #define FOR_EACH_UNOWNED_DEPL_PROV(commonBox, playerId, provName)                                                                                                \
            for (const std::pair<const uint32_t, uint64_t> & FOR_EACH_UNOWNED_DEPL_PROV_pair : (commonBox).unitsDeployedStat->getProvincesWithUnitsDeployed(playerId)) { \
                Province & provName = (commonBox).map->getProvince(FOR_EACH_UNOWNED_DEPL_PROV_pair.first);                                                               \
                if (provName.getProvinceOwner() != (playerId)) {
        #define FOR_EACH_UNOWNED_DEPL_PROV_END }}

    };
}

#endif


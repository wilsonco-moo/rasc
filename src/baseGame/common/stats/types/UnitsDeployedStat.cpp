/*
 * UnitsDeployedStat.cpp
 *
 *  Created on: 25 Oct 2019
 *      Author: wilson
 */

#include "UnitsDeployedStat.h"

#include <iostream>

#include "../../gameMap/mapElement/types/Province.h"
#include "../../playerData/PlayerDataSystem.h"
#include "../../gameMap/GameMap.h"
#include "../../util/CommonBox.h"

namespace rasc {

    const std::vector<Stat::StatUpdateType> UnitsDeployedStat::updateTypes = {
        Stat::StatUpdateTypes::changeUnitsInProvince,
        Stat::StatUpdateTypes::addPlayer,
        Stat::StatUpdateTypes::removePlayer
    };

    UnitsDeployedStat::UnitsDeployedStat(void) :
        Stat(),
        unitsDeployedPerPlayer(),
        totalUnitsDeployed(0),
        unitsDeployedPerProvince() {
    }

    UnitsDeployedStat::~UnitsDeployedStat(void) {
    }

    // ------------------------------- Misc methods ---------------------------------------

    void UnitsDeployedStat::initialise(CommonBox & box) {

        // Clear existing data structures.
        unitsDeployedPerPlayer.clear();
        unitsDeployedPerProvince.clear();
        totalUnitsDeployed = 0;

        // Add all existing players from the player data system.
        for (const std::pair<const uint64_t, RascUpdatable *> pair : box.playerDataSystem->mapChildren()) {
            onAddPlayer(pair.first);
        }

        // Iterate through every army on the map.
        for (Province * province : box.map->getProvinces()) {
            for (const Army & army : province->armyControl.getArmies()) {
                onChangeUnitsInProvince(*province, army.getArmyOwner(), army.getUnitCount());
            }
        }
    }

    const std::vector<Stat::StatUpdateType> & UnitsDeployedStat::getUpdateTypes(void) const {
        return updateTypes;
    }

    // ----------------------------------- Stat updates -----------------------------------

    void UnitsDeployedStat::onAddPlayer(uint64_t playerId) {
        // Ensure that there is a value for this player in unitsDeployedPerPlayer. Complain
        // if there already is one.
        if (!unitsDeployedPerPlayer.emplace(playerId, 0).second) {
            std::cerr << "WARNING: UnitsDeployedStat: Player " << playerId << " added twice to units deployed per player maps.\n";
        }

        // Ensure that empty unordered maps exist for this player.
        if (!unitsDeployedPerProvince.emplace(playerId, std::unordered_map<uint32_t, uint64_t>()).second) {
            std::cerr << "WARNING: UnitsDeployedStat: Player " << playerId << " added twice to units deployed per province map.\n";
        }
    }

    void UnitsDeployedStat::onChangeUnitsInProvince(Province & province, uint64_t armyOwner, int64_t unitsAdded) {
        // Update the units deployed per player.
        {
            auto iter = unitsDeployedPerPlayer.find(armyOwner);
            if (iter == unitsDeployedPerPlayer.end()) {
                std::cerr << "WARNING: UnitsDeployedStat: Seemingly unknown player " << armyOwner << " gained or lost armies in a province.\n";
            } else {
                iter->second += unitsAdded;
            }
        }
        // Update the units deployed per province per player.
        {
            auto iter = unitsDeployedPerProvince.find(armyOwner);
            if (iter == unitsDeployedPerProvince.end()) {
                std::cerr << "WARNING: UnitsDeployedStat: Seemingly unknown player " << armyOwner << " gained or lost armies in a province.\n";
            } else {
                std::unordered_map<uint32_t, uint64_t>::iterator prov = iter->second.find(province.getId());
                if (prov == iter->second.end()) {
                    iter->second.emplace(province.getId(), unitsAdded);
                } else {
                    prov->second += unitsAdded;
                    if (prov->second == 0) {
                        iter->second.erase(prov);
                    }
                }
            }
        }
        // Update the units deployed overall.
        totalUnitsDeployed += unitsAdded;
    }

    void UnitsDeployedStat::onRemovePlayer(uint64_t playerId) {
        // Erase that player from the map. Complain if that player seems to still own armies.
        {
            std::unordered_map<uint64_t, uint64_t>::iterator iter = unitsDeployedPerPlayer.find(playerId);
            if (iter == unitsDeployedPerPlayer.end()) {
                std::cerr << "WARNING: UnitsDeployedStat: Seemingly non existent player " << playerId << " removed.\n";
            } else {
                if (iter->second > 0) {
                    std::cerr << "WARNING: UnitsDeployedStat: Player removed while seemingly still having " << iter->second << " units deployed.\n";
                }
                unitsDeployedPerPlayer.erase(iter);
            }
        }

        // Erase that player from unitsDeployedPerProvince, complaining if they are not in it,
        // or if they apparently still have deployed units.
        {
            auto iter = unitsDeployedPerProvince.find(playerId);
            if (iter == unitsDeployedPerProvince.end()) {
                std::cerr << "WARNING: UnitsDeployedStat: Seemingly non existent player " << playerId << " removed.\n";
            } else {
                if (!iter->second.empty()) {
                    std::cerr << "WARNING: UnitsDeployedStat: Player " << playerId << " removed while seemingly still haveing units deployed in a province.\n";
                }
                unitsDeployedPerProvince.erase(iter);
            }
        }
    }

    // ================================ Public stat accessor methods ===================================

    uint64_t UnitsDeployedStat::getUnitsDeployed(uint64_t playerId) const {
        // Assume that any player, which does not exist, has no deployed units.
        std::unordered_map<uint64_t, uint64_t>::const_iterator iter = unitsDeployedPerPlayer.find(playerId);
        if (iter == unitsDeployedPerPlayer.cend()) {
            return 0;
        } else {
            return iter->second;
        }
    }

    uint64_t UnitsDeployedStat::getUnitsDeployedInProvince(uint64_t playerId, uint32_t provinceId) const {
        auto iter = unitsDeployedPerProvince.find(playerId);
        if (iter == unitsDeployedPerProvince.end()) {
            // Assume that a player which does not exist has no deployed units in any province.
            return 0;
        } else {
            std::unordered_map<uint32_t, uint64_t>::const_iterator provIter = iter->second.find(provinceId);
            if (provIter == iter->second.end()) {
                // If a province is not listed in the map of provinces to deployed units for a specific
                // player, then they have no units deployed in this province.
                return 0;
            } else {
                return provIter->second;
            }
        }
    }

    provstat_t UnitsDeployedStat::getArmyExpenditure(uint64_t playerId) const {
        return (getUnitsDeployed(playerId) * CURRENCY_COST_PER_UNIT_SET) / CURRENCY_COST_UNIT_SET_SIZE;
    }
}

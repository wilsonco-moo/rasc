/*
 * AdjacencyStat.cpp
 *
 *  Created on: 24 Dec 2019
 *      Author: wilson
 */

#include "AdjacencyStat.h"

#include <iostream>
#include <cstddef>

#include "../../gameMap/mapElement/types/Province.h"
#include "../../gameMap/mapElement/types/Area.h"
#include "../../playerData/PlayerDataSystem.h"
#include "../../playerData/PlayerData.h"
#include "../../util/numeric/MultiCol.h"
#include "../../config/RascConfig.h"
#include "../../gameMap/GameMap.h"
#include "../../util/CommonBox.h"
#include "../../util/XMLUtil.h"
#include "../../util/Misc.h"

// Uncomment to enable verbose logging.
//#define ADJACENCY_STAT_VERBOSE_PRINT_MODE
#ifdef ADJACENCY_STAT_VERBOSE_PRINT_MODE
    #define VERBOSE_OUT(text) std::cout << text
#else
    #define VERBOSE_OUT(text)
#endif

namespace rasc {

    // ----------------------- Player colour pair --------------------------
    AdjacencyStat::PlayerColourPair::PlayerColourPair(unsigned int colourDiff, std::pair<uint64_t, uint64_t> playerIdPair) :
        colourDiff(colourDiff),
        playerIdPair(std::min(playerIdPair.first, playerIdPair.second), std::max(playerIdPair.first, playerIdPair.second)) {
    }
    bool AdjacencyStat::PlayerColourPair::operator == (const PlayerColourPair & other) const {
        return colourDiff == other.colourDiff && playerIdPair == other.playerIdPair;
    }
    bool AdjacencyStat::PlayerColourPair::operator < (const PlayerColourPair & other) const {
        // If colour diff is both the same, order lexicographically by player ID pair.
        if (colourDiff == other.colourDiff) {
            return playerIdPair < other.playerIdPair;
        } else {
            return colourDiff < other.colourDiff;
        }
    }

    // --------------------- Player adjacency info -------------------------

    AdjacencyStat::PlayerAdjacencyInfo::PlayerAdjacencyInfo(uint64_t playerId) :
        playerId(playerId),
        totalBorders(0),
        bordersPerPlayer() {
    }
    // ----------------
    void AdjacencyStat::PlayerAdjacencyInfo::onGainAdjacencyTo(AdjacencyStat & stat, uint64_t otherPlayerId) {
        VERBOSE_OUT("ADJ: Player " << stat.dataSystem->idToData(playerId)->getName() << " gain adjacency to " << stat.dataSystem->idToData(otherPlayerId)->getName() << "\n");
        // Find player data for us and them, complain if not found.
        PlayerData * ourData   = stat.dataSystem->idToDataChecked(playerId),
                   * theirData = stat.dataSystem->idToDataChecked(otherPlayerId);
        if (ourData == NULL) { std::cerr << "WARNING: AdjacencyStat: Missing player data for player " << playerId << ".\n"; return; }
        if (theirData == NULL) { std::cerr << "WARNING: AdjacencyStat: Gained border with player which is not in PlayerDataSystem: " << otherPlayerId << ".\n"; return; }

        // Add a borders value for this player. Make sure that the initial count of borders is 1,
        // since this call can only have been caused by gaining a border with the player.
        bordersPerPlayer.emplace(otherPlayerId, 1);

        // Get difference between colours. Do this with the LAB colours, so the difference is perceptually uniform.
        unsigned int colourDiff = ourData->getColourLab().distanceSquared(theirData->getColourLab(), stat.colourBlindMode);

        // Notify AdjacencyStat of the gained colour difference and player ids.
        stat.onGainPlayerColourPair(PlayerColourPair(colourDiff, playerId, otherPlayerId));
    }
    void AdjacencyStat::PlayerAdjacencyInfo::onLoseAdjacencyTo(AdjacencyStat & stat, uint64_t otherPlayerId) {
        VERBOSE_OUT("ADJ: Player " << stat.dataSystem->idToData(playerId)->getName() << " lose adjacency to " << stat.dataSystem->idToData(otherPlayerId)->getName() << "\n");
        // Find player data for us and them, complain if not found.
        PlayerData * ourData   = stat.dataSystem->idToDataChecked(playerId),
                   * theirData = stat.dataSystem->idToDataChecked(otherPlayerId);
        if (ourData == NULL) { std::cerr << "WARNING: AdjacencyStat: Missing player data for player " << playerId << ".\n"; return; }
        if (theirData == NULL) { std::cerr << "WARNING: AdjacencyStat: Gained border with player which is not in PlayerDataSystem: " << otherPlayerId << ".\n"; return; }

        // Erase this player from borders per player.
        auto iter = bordersPerPlayer.find(otherPlayerId);
        if (iter == bordersPerPlayer.end()) {
            std::cerr << "WARNING: AdjacencyStat: Lost adjacency with unknown player " << otherPlayerId << ".\n";
        } else {
            bordersPerPlayer.erase(iter);
        }

        // Get difference between colours. Do this with the LAB colours, so the difference is perceptually uniform.
        unsigned int colourDiff = ourData->getColourLab().distanceSquared(theirData->getColourLab(), stat.colourBlindMode);

        // Notify AdjacencyStat of the lost colour difference and player ids.
        stat.onLosePlayerColourPair(PlayerColourPair(colourDiff, playerId, otherPlayerId));
    }
    // ----------------
    void AdjacencyStat::PlayerAdjacencyInfo::onGainBorderWith(AdjacencyStat & stat, uint64_t otherPlayerId) {
        VERBOSE_OUT("ADJ: Player " << stat.dataSystem->idToData(playerId)->getName() << " gain border to " << stat.dataSystem->idToData(otherPlayerId)->getName() << "\n");
        auto iter = bordersPerPlayer.find(otherPlayerId);
        if (iter == bordersPerPlayer.end()) {
            // Run onGainAdjacencyTo if this is first border with that player.
            // Note that this initialises the number of borders with them to 1, so incrementing is not necessary.
            onGainAdjacencyTo(stat, otherPlayerId);
        } else {
            // If this is not first border with them, then increment our number of borders with them.
            iter->second++;
        }
        // Increment total borders since we gained an adjacency.
        totalBorders++;
    }
    void AdjacencyStat::PlayerAdjacencyInfo::onLoseBorderWith(AdjacencyStat & stat, uint64_t otherPlayerId) {
        VERBOSE_OUT("ADJ: Player " << stat.dataSystem->idToData(playerId)->getName() << " lose border to " << stat.dataSystem->idToData(otherPlayerId)->getName() << "\n");
        auto iter = bordersPerPlayer.find(otherPlayerId);
        if (iter == bordersPerPlayer.end()) {
            // This should never happen.
            std::cerr << "WARNING: AdjacencyStat: Lost border with unknown player " << otherPlayerId << "\n";
        } else {
            // Decrement the number of borders we have with this player, and the total number of borders.
            iter->second--;
            totalBorders--;
            // If we lost our last border with them, run onLoseAdjacencyTo.
            if (iter->second == 0) {
                onLoseAdjacencyTo(stat, otherPlayerId);
            }
        }
    }
    // ----------------
    void AdjacencyStat::PlayerAdjacencyInfo::onGainProvince(AdjacencyStat & stat, Province & province, bool considerProvincesAfter) {
        VERBOSE_OUT("ADJ: Player " << stat.dataSystem->idToData(playerId)->getName() << " gain province " << province.getName() << (considerProvincesAfter ? "" : ", without considering provinces after") << ".\n");
        for (uint32_t adjProvId : province.getAdjacent()) {
            // Only allow adjacent provinces with a greater province ID, if considerProvincesAfter is true.
            // considerProvincesAfter should be set to false during initialisation, to avoid double
            // adjacencies. These result when onGainProvince is independently called on both sides of
            // a border, so onGainBorderWith gets called twice for each side.
            if (considerProvincesAfter || adjProvId < province.getId()) {
                Province & adjProv = province.getGameMap().getProvince(adjProvId);
                uint64_t adjOwner = adjProv.getProvinceOwner();
                if (adjOwner != PlayerData::NO_PLAYER) { // (ignore borders to unowned provinces)

                    // We gain borders with the adjacent province's owner each time this new province
                    // is adjacent to a player other than us.
                    if (adjOwner != playerId) {
                        onGainBorderWith(stat, adjOwner);

                        // The other player must ALSO gain borders with US.
                        auto iter = stat.adjacencyInfo.find(adjOwner);
                        if (iter == stat.adjacencyInfo.end()) {
                            std::cerr << "WARNING: AdjacencyStat: Gained border with seemingly unknown player " << adjOwner << ".\n";
                        } else {
                            iter->second.onGainBorderWith(stat, playerId);
                        }
                    }
                }
            }
        }
    }
    void AdjacencyStat::PlayerAdjacencyInfo::onLoseProvince(AdjacencyStat & stat, Province & province) {
        VERBOSE_OUT("ADJ: Player " << stat.dataSystem->idToData(playerId)->getName() << " lose province " << province.getName() << "\n");
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = province.getGameMap().getProvince(adjProvId);
            uint64_t adjOwner = adjProv.getProvinceOwner();
            if (adjOwner != PlayerData::NO_PLAYER) { // (ignore borders to unowned provinces)

                // We lose borders with the adjacent province's owner each time this lost province
                // is adjacent to a player other than us.
                if (adjOwner != playerId) {
                    onLoseBorderWith(stat, adjOwner);

                    // The other player must ALSO lose borders with US.
                    auto iter = stat.adjacencyInfo.find(adjOwner);
                    if (iter == stat.adjacencyInfo.end()) {
                        std::cerr << "WARNING: AdjacencyStat: Lost border with seemingly unknown player " << adjOwner << ".\n";
                    } else {
                        iter->second.onLoseBorderWith(stat, playerId);
                    }
                }
            }
        }
    }
    void AdjacencyStat::PlayerAdjacencyInfo::onColourChange(AdjacencyStat & stat, uint32_t oldColour, const MultiCol & oldLabColour, uint32_t newColour, const MultiCol & newLabColour) {
        // Iterate through all our adjacent players.
        for (const std::pair<const uint64_t, size_t> & adjPlayerPair : bordersPerPlayer) {
            // Find player data for us and them, complain if not found.
            PlayerData * ourData   = stat.dataSystem->idToDataChecked(playerId),
                       * theirData = stat.dataSystem->idToDataChecked(adjPlayerPair.first);
            if (ourData == NULL) { std::cerr << "WARNING: AdjacencyStat: Noticed missing player data for player " << playerId << " (us), during colour change.\n"; return; }
            if (theirData == NULL) { std::cerr << "WARNING: AdjacencyStat: Noticed missing player data for player " << adjPlayerPair.first << " (adjacent player), during colour change.\n"; return; }

            // Remove adjacency entry for old colour difference. Do this with the LAB colours, so the difference is perceptually uniform.
            unsigned int colourDiff = oldLabColour.distanceSquared(theirData->getColourLab(), stat.colourBlindMode);
            stat.onLosePlayerColourPair(PlayerColourPair(colourDiff, playerId, adjPlayerPair.first));

            // Add adjacency entry for new colour difference. Do this with the LAB colours, so the difference is perceptually uniform.
            colourDiff = newLabColour.distanceSquared(theirData->getColourLab(), stat.colourBlindMode);
            stat.onGainPlayerColourPair(PlayerColourPair(colourDiff, playerId, adjPlayerPair.first));
        }
    }
    // ----------------
    void AdjacencyStat::PlayerAdjacencyInfo::checkRemovalSafety(void) {
        if (totalBorders > 0 || !bordersPerPlayer.empty()) {
            std::cerr << "WARNING: AdjacencyStat: Player ID " << playerId << " removed while still having borders with other players.\n";
        }
    }
    uint64_t AdjacencyStat::PlayerAdjacencyInfo::getPlayerId(void) const {
        return playerId;
    }
    size_t AdjacencyStat::PlayerAdjacencyInfo::getTotalBorders(void) const {
        return totalBorders;
    }
    size_t AdjacencyStat::PlayerAdjacencyInfo::getAdjacentPlayerCount(void) const {
        return bordersPerPlayer.size();
    }
    const std::unordered_map<uint64_t, size_t> & AdjacencyStat::PlayerAdjacencyInfo::getBordersPerPlayer(void) const {
        return bordersPerPlayer;
    }
    // ---------------------------------------------------------------------

    const std::vector<Stat::StatUpdateType> AdjacencyStat::updateTypes = {
        Stat::StatUpdateTypes::changeProvinceOwnership,
        Stat::StatUpdateTypes::addPlayer,
        Stat::StatUpdateTypes::removePlayer,
        Stat::StatUpdateTypes::changePlayerColour
    };
    
    const AdjacencyStat::PlayerAdjacencyInfo AdjacencyStat::emptyAdjacencyInfo(PlayerData::NO_PLAYER);

    AdjacencyStat::AdjacencyStat(void) :
        Stat(),
        dataSystem(NULL),
        adjacencyInfo(),
        playerColourPairs(),
        colourBlindMode(MultiCol::ColourBlindMode::none) {
    }

    AdjacencyStat::~AdjacencyStat(void) {
    }

    void AdjacencyStat::onGainPlayerColourPair(PlayerColourPair playerColourPair) {
        playerColourPairs.insert(playerColourPair);
    }

    void AdjacencyStat::onLosePlayerColourPair(PlayerColourPair playerColourPair) {
        playerColourPairs.erase(playerColourPair);
    }

    // ------------------------------- Misc methods ---------------------------------------

    void AdjacencyStat::initialise(CommonBox & box) {
        // Clear existing data structures.
        adjacencyInfo.clear();
        playerColourPairs.clear();

        // Make sure we keep a pointer to the player data system.
        dataSystem = box.playerDataSystem;

        // Set our colour-blind mode from the value set in RascConfig.
        {
            simplenetwork::MutexPtr<tinyxml2::XMLDocument> doc = box.rascConfig->getDocument();
            tinyxml2::XMLElement * sharedOptions = RascConfig::getOrInitBase(&*doc, "sharedOptions", &Misc::initSharedOptionsConfig);
            tinyxml2::XMLElement * colourBlindness = readElement(sharedOptions, "colourBlindness");
            colourBlindMode = MultiCol::friendlyNameToColourBlindMode(readAttribute(colourBlindness, "mode"));
        }
        
        // Add each player id listed in the player data system.
        for (const std::pair<const uint64_t, RascUpdatable *> pair : dataSystem->mapChildren()) {
            onAddPlayer(pair.first);
        }

        // Add ownership of each province. For the onGainProvince method, do NOT consider
        // provinces after this one. This is because for each border, onGainProvince gets
        // called twice (independently). Without setting considerProvincesAfter to false,
        // the adjacency would be added twice: Once for the province on either side.
        for (Province * prov : box.map->getProvinces()) {
            if (prov->getProvinceOwner() != PlayerData::NO_PLAYER) {
                auto iter = adjacencyInfo.find(prov->getProvinceOwner());
                if (iter == adjacencyInfo.end()) {
                    std::cerr << "WARNING: AdjacencyStat: Seemingly unknown player " << prov->getProvinceOwner() << " gained initial province " << prov->getName() << ".\n";
                } else {
                    iter->second.onGainProvince(*this, *prov, false);
                }
            }
        }
    }

    const std::vector<Stat::StatUpdateType> & AdjacencyStat::getUpdateTypes(void) const {
        return updateTypes;
    }

    void AdjacencyStat::onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) {
        if (oldOwner != PlayerData::NO_PLAYER) {
            auto iter = adjacencyInfo.find(oldOwner);
            if (iter == adjacencyInfo.end()) {
                std::cerr << "WARNING: AdjacencyStat: Seemingly unknown player " << oldOwner << " lost province " << province.getName() << ".\n";
            } else {
                iter->second.onLoseProvince(*this, province);
            }
        }

        if (newOwner != PlayerData::NO_PLAYER) {
            auto iter = adjacencyInfo.find(newOwner);
            if (iter == adjacencyInfo.end()) {
                std::cerr << "WARNING: AdjacencyStat: Seemingly unknown player " << newOwner << " gained province " << province.getName() << ".\n";
            } else {
                // Allow consideration of provinces after this one, in onGainProvince.
                // This is allowed here, because changes in province ownership ONLY happen
                // one province at a time, in isolation. This means that the owners of all
                // surrounding provinces are correctly known, so double adjacencies cannot
                // happen here.
                iter->second.onGainProvince(*this, province, true);
            }
        }
    }

    void AdjacencyStat::onAddPlayer(uint64_t playerId) {
        if (!adjacencyInfo.emplace(playerId, playerId).second) {
            std::cerr << "WARNING: AdjacencyStat: Player id " << playerId << " added when seemingly already existing.\n";
        }
    }

    void AdjacencyStat::onRemovePlayer(uint64_t playerId) {
        auto iter = adjacencyInfo.find(playerId);
        if (iter == adjacencyInfo.end()) {
            std::cerr << "WARNING: AdjacencyStat: Seemingly unknown player " << playerId << " removed.\n";
        } else {
            iter->second.checkRemovalSafety();
            adjacencyInfo.erase(iter);
        }
    }

    void AdjacencyStat::onChangePlayerColour(uint64_t playerId, uint32_t oldColour, const MultiCol & oldLabColour, uint32_t newColour, const MultiCol & newLabColour) {
        auto iter = adjacencyInfo.find(playerId);
        if (iter == adjacencyInfo.end()) {
            std::cerr << "WARNING: AdjacencyStat: Colour of seemingly unknown player " << playerId << " changed.\n";
        } else {
            iter->second.onColourChange(*this, oldColour, oldLabColour, newColour, newLabColour);
        }
    }
    
    const AdjacencyStat::PlayerAdjacencyInfo & AdjacencyStat::getPlayerAdjacencyInfo(uint64_t playerId) const {
        auto iter = adjacencyInfo.find(playerId);
        if (iter == adjacencyInfo.end()) {
            return emptyAdjacencyInfo;
        } else {
            return iter->second;
        }
    }
    
    unsigned int AdjacencyStat::getColourBlindMode(void) const {
        return colourBlindMode;
    }
}

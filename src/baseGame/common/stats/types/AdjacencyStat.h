/*
 * AdjacencyStat.h
 *
 *  Created on: 24 Dec 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_STATS_TYPES_ADJACENCYSTAT_H_
#define BASEGAME_COMMON_STATS_TYPES_ADJACENCYSTAT_H_

#include <unordered_map>
#include <utility>
#include <cstddef>
#include <cstdint>
#include <vector>
#include <set>

#include "../Stat.h"

namespace rasc {
    class PlayerDataSystem;
    class Province;

    /**
     *
     */
    class AdjacencyStat : public Stat {
    public:

        /**
         * A class representing a pair of player IDs, with an associated colour
         * difference, but able to be stored in a sorted container of some kind.
         * The smallest player ID is always stored first, but this ordering is done
         * automatically by the constructor.
         *
         * Colour differences must have been generated using MultiCol::distanceSquared
         * on the player's LAB colours, or using MultiCol::perceptUniformRgbColDistSquared
         * on the player's int colours.
         *
         * NOTE:
         *  Ordering is done lexicographically:
         *  First by colour diff, then lexicographically by player id pair.
         */
        class PlayerColourPair {
        private:
            unsigned int colourDiff;
            std::pair<uint64_t, uint64_t> playerIdPair;
        public:
            PlayerColourPair(unsigned int colourDiff, std::pair<uint64_t, uint64_t> playerIdPair);
            inline PlayerColourPair(unsigned int colourDiff, uint64_t firstPlayerId, uint64_t secondPlayerId) : PlayerColourPair(colourDiff, std::make_pair(firstPlayerId, secondPlayerId)) {}
            bool operator == (const PlayerColourPair & other) const;
            bool operator <  (const PlayerColourPair & other) const;
            inline unsigned int getColourDiff(void) const { return colourDiff; }
            inline const std::pair<uint64_t, uint64_t> & getPlayerIdPair(void) const { return playerIdPair; }
            inline uint64_t getFirstPlayerId(void) const { return playerIdPair.first; }
            inline uint64_t getSecondPlayerId(void) const { return playerIdPair.second; }
        };

        /**
         * Stores the border information for a specific player.
         */
        class PlayerAdjacencyInfo {
        private:

            // The player ID related to this info.
            uint64_t playerId;

            // The total number of borders we have around our nation, ignoring provinces without an owner.
            size_t totalBorders;

            // The number of borders that we have with every other player around us.
            std::unordered_map<uint64_t, size_t> bordersPerPlayer;

        public:
            PlayerAdjacencyInfo(uint64_t playerId);
        private:
            // ----------------
            // Should be called each time we gain overall adjacency to a player for the first time.
            // (called by onGainBorderWith), must never be called with NO_PLAYER.
            void onGainAdjacencyTo(AdjacencyStat & stat, uint64_t otherPlayerId);
            // Should be called each time we lose overall adjacency to a player.
            // (called by onLoseBorderWith), must never be called with NO_PLAYER.
            void onLoseAdjacencyTo(AdjacencyStat & stat, uint64_t otherPlayerId);
            // ----------------
            // Should be called each time we gain a single border with the specified player.
            // (called by onGainProvince and onLoseProvince), must never be called with NO_PLAYER.
            void onGainBorderWith(AdjacencyStat & stat, uint64_t otherPlayerId);
            // Should be called each time we lose a single border with the specified player.
            // (called by onGainProvince and onLoseProvince), must never be called with NO_PLAYER.
            void onLoseBorderWith(AdjacencyStat & stat, uint64_t otherPlayerId);
            // ----------------
        public:
            // Should be called each time we gain an entire province.
            // considerProvincesAfter should be set to false during initialisation, to avoid double
            // adjacencies. It should be set to true otherwise, as the rest of the time a province
            // only ever changes ownership in isolation, and the ownership of adjacent provinces is
            // correctly known.
            void onGainProvince(AdjacencyStat & stat, Province & province, bool considerProvincesAfter);
            // Should be called each time we lose an entire province.
            void onLoseProvince(AdjacencyStat & stat, Province & province);
            // Should be called each time our colour changes.
            void onColourChange(AdjacencyStat & stat, uint32_t oldColour, const MultiCol & oldLabColour, uint32_t newColour, const MultiCol & newLabColour);
            // ----------------
            void checkRemovalSafety(void);
            
            // Public access methods (i.e: const methods).
            
            /**
             * Gets our player id.
             */
            uint64_t getPlayerId(void) const;
            
            /**
             * Gets the total number of province borders we share with other players,
             * across all players we share borders with (i.e: the sum of borders per
             * player across all players we share borders with).
             */
            size_t getTotalBorders(void) const;
            
            /**
             * Gets the number of players which we share a border with, i.e: the size of
             * the map returned by getBordersPerPlayer.
             */
            size_t getAdjacentPlayerCount(void) const;
            
            /**
             * Gets a map containing a count of how many borders we share with each player
             * around us.
             */
            const std::unordered_map<uint64_t, size_t> & getBordersPerPlayer(void) const;
        };

    private:
        const static PlayerAdjacencyInfo emptyAdjacencyInfo;
        const static std::vector<Stat::StatUpdateType> updateTypes;

        // We keep a pointer to this so that we can find info about players later, (like player colours).
        PlayerDataSystem * dataSystem;

        // An PlayerAdjacencyInfo instance stored for each player.
        std::unordered_map<uint64_t, PlayerAdjacencyInfo> adjacencyInfo;

        // The currently known player colour pairs, across the entire map,
        // sorted in ascending order.
        std::set<PlayerColourPair> playerColourPairs;

        // Current colour-blindness mode (for colour distance calculations), set during
        // initialise (from RascConfig).
        unsigned int colourBlindMode;
        
    public:
        AdjacencyStat(void);
        virtual ~AdjacencyStat(void);
    private:
        // These must be called when overall adjacency is gained or lost, for the provided
        // pair of players.
        void onGainPlayerColourPair(PlayerColourPair playerColourPair);
        void onLosePlayerColourPair(PlayerColourPair playerColourPair);
    protected:

        // Misc methods
        virtual void initialise(CommonBox & box) override;
        virtual const std::vector<Stat::StatUpdateType> & getUpdateTypes(void) const override;

        // Stat update methods
        virtual void onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) override;
        virtual void onAddPlayer(uint64_t playerId) override;
        virtual void onRemovePlayer(uint64_t playerId) override;
        virtual void onChangePlayerColour(uint64_t playerId, uint32_t oldColour, const MultiCol & oldLabColour, uint32_t newColour, const MultiCol & newLabColour) override;

    public:
        // ----------------------------- STAT ACCESSOR METHODS ------------------------------

        /**
         * Accesses a set of pairs of players and colour differences,
         * sorted by smaller colour differences first.
         */
        inline const std::set<PlayerColourPair> & getPlayerColourPairs(void) const {
            return playerColourPairs;
        }

        /**
         * Returns true if there are no players adjacent to each other, so there are
         * no available player colour pairs.
         */
        inline bool adjacentEmpty(void) const {
            return playerColourPairs.empty();
        }

        /**
         * Finds the player colour pair, where the colour difference is smallest, across
         * the whole map. Note that this is only defined if adjacentEmpty() returns false.
         *
         * Colour differences in the PlayerColourPair are generated using MultiCol::distanceSquared
         * on the player's LAB colours.
         */
        inline const PlayerColourPair & getClosestPlayerColourPair(void) const {
            return *playerColourPairs.begin();
        }
        
        /**
         * Gets player adjacency info for the specified player id.
         */
        const PlayerAdjacencyInfo & getPlayerAdjacencyInfo(uint64_t playerId) const;
        
        /**
         * Gets the current colour-blindness mode being used for colour-pair sorting.
         */
        unsigned int getColourBlindMode(void) const;
    };
}

#endif

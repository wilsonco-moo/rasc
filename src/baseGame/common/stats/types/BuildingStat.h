/*
 * BuildingStat.h
 *
 *  Created on: 5 Oct 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_STATS_TYPES_BUILDINGSTAT_H_
#define BASEGAME_COMMON_STATS_TYPES_BUILDINGSTAT_H_

#include "../../gameMap/provinceUtil/ProvinceStatTypes.h"
#include "../../gameMap/properties/PropertyDef.h"
#include "../Stat.h"

#include <unordered_map>
#include <unordered_set>
#include <cstdint>
#include <vector>
#include <set>

namespace rasc {
    class AreasStat;
    class Province;

    /**
     * A simple class representing a single available building in a province.
     * This is provided to make building data available to users of BuildingStat.
     */
    class BuildingStatInfo {
    public:
        uint32_t provinceId;
        float valueForMoney;
        proptype_t building;

        // We are better value than (should come before another building) if
        // we have greater value for money.
        inline bool operator < (const BuildingStatInfo & other) const {
            return valueForMoney > other.valueForMoney;
        }
    };



    /**
     * This stat keeps track of "base-available" buildings in each province,
     * and maintains sets of available buildings, ordered by "value-for-money",
     * for each player.
     * NOTE: BuildingStat depends on AreasStat, so MUST be registered AFTER AreasStat.
     *       This is because BuildingStat uses AreasStat's data in its update operations,
     *       assuming that AreasStat has already been updated/initialised first.
     */
    class BuildingStat : public Stat {
    private:
        class ProvinceInfo;

        // Internally stores information about each player. This stores the player's
        // available buildings by value-for-money, for each build valued stat.
        class PerPlayerInfo {
        private:
            friend class BuildingStat::ProvinceInfo;
            uint64_t playerId;
            std::array<std::multiset<BuildingStatInfo>, BuildValuedStatIds::COUNT> buildingInfo;

        public:
            PerPlayerInfo(uint64_t playerId);
            void checkRemovalSafety(void);

            // Access of building info.
            const std::multiset<BuildingStatInfo> & getBuildingInfoByStat(unsigned int statId) const;
            inline const std::array<std::multiset<BuildingStatInfo>, BuildValuedStatIds::COUNT> & getBuildingInfo(void) const { return buildingInfo; }
        };



        // Internally stores information about each province.
        class ProvinceInfo {
        private:
            // Current battle status (for convenience).
            bool isBattleOngoing;
            // Current area ownership status (for convenience).
            bool areaCompletelyOwned;
            // Current base available buildings (i.e: ignoring land and battle requirements).
            std::unordered_set<proptype_t> baseAvailableBuildings;
            // Blocked traits resulting from currently existing traits, along
            // with a count of how many traits block each one.
            std::unordered_map<proptype_t, unsigned int> overallBlocking;
            // Our current player info, (or NULL for no player). Note that this is fine to store,
            // even though it is in an unordered_map, because references to keys/values are never
            // invalidated, even when inserting/erasing/rehashing (see notes in: https://en.cppreference.com/w/cpp/container/unordered_map).
            PerPlayerInfo * playerInfo;
            // Iterators for each building we have added in playerInfo, for each build-valued stat.
            std::vector<std::multiset<BuildingStatInfo>::iterator> iterators[BuildValuedStatIds::COUNT];

        public:
            ProvinceInfo(Province & province, PerPlayerInfo * playerInfo, AreasStat * areasStat);

        private:
            // Returns true if the province, given our current data and the province's current
            // situation, should make the trait available. Note that this assumes that
            // the provided trait is a valid REGULAR BUILDING trait.
            bool shouldMakeAvailable(Province & province, proptype_t trait) const;
            // Removes all of our buildings from our player info's set. This must only be
            // called if our player info is NOT NULL.
            void clearBuildingsInSet(void);
        public:
            // Should be called by BuildingStat when properties are added/removed.
            void onCreateTrait(Province & province, proptype_t trait);
            void onDeleteTrait(Province & province, proptype_t trait);
            // Should be called when battle status changes.
            void onChangeBattleStatus(Province & province, bool isBattleOngoing);
            // For BuildingStat to access our available buildings.
            inline const std::unordered_set<proptype_t> & getBaseAvailableBuildings(void) const {
                return baseAvailableBuildings;
            }
            // For when our province changes ownership. The old player ID is needed only for
            // checking and logging. NULL and PlayerData::NO_PLAYER should be used for "unowned".
            void changeOwnership(Province & province, uint64_t oldPlayerId, PerPlayerInfo * newPlayerInfo);
            // Should be called whenever stats change in the province, AND should be called
            // after each time onCreateTrait or onDeleteTrait is called, AFTER the new stats
            // in the province are known.
            void rebuildPlayerBuildings(Province & province);
            // Updates the area ownership status, then runs rebuildPlayerBuildings. This should be
            // called for all provinces in an area, if area ownership status changes. Note that
            // area ownership status can only change in onChangeProvinceOwnership.
            void updateAreaAndRebuild(bool areaCompletelyOwned, Province & province);
            // Accesses current area ownership status.
            inline bool isAreaCompletelyOwned(void) const {
                return areaCompletelyOwned;
            }
        };



        // -------------------------------------------------------------------------------

        const static std::vector<Stat::StatUpdateType> updateTypes;

        std::vector<ProvinceInfo> provinceInfo;
        std::unordered_map<uint64_t, PerPlayerInfo> playerInfo;

        // For convenience and easy access to area ownership status.
        AreasStat * areasStat;

    public:
        BuildingStat(void);
        virtual ~BuildingStat(void);

    protected:

        // Misc methods
        virtual void initialise(CommonBox & box) override;
        virtual const std::vector<Stat::StatUpdateType> & getUpdateTypes(void) const override;

        // Stat update methods
        virtual void onCreatePropertyInProvince(Province & province, uint64_t propertyUniqueId) override;
        virtual void onDeletePropertyFromProvince(Province & province, uint64_t propertyUniqueId) override;
        virtual void onChangeStatsInProvince(Province & province, const StatArr & addedStats) override;
        virtual void onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) override;
        virtual void onAddPlayer(uint64_t playerId) override;
        virtual void onRemovePlayer(uint64_t playerId) override;
        virtual void onChangeBattleStatusInProvince(Province & province, bool isBattleOngoing) override;
    public:

        // ----------------------- Accessor methods ---------------------------

        /**
         * This returns all BASE cost-available buildings, in the specified province. This
         * is raw cost-availability, ignoring land use, battle status and other factors.
         * Note that this only includes regular buildings, and does not include combat or
         * special ("none" category) buildings.
         */
        const std::unordered_set<proptype_t> & getBaseAvailableBuildings(uint32_t provinceId) const;

        /**
         * Allows access to a multiset of BuildingStatInfo instances, for the specified player and
         * building-valued stat ID (see BuildValuedStatIds in ProvinceStatTypes.h).
         * This is ordered such that the best "value-for-money" buildings, in terms of the specified
         * build-valued stat, are listed first in the multiset.
         * If the player ID does not exist, an empty multiset is returned: Non existing or unknown
         * players are sensibly assumed to have no available buildings.
         */
        const std::multiset<BuildingStatInfo> & getPlayerAvailableBuildingsByStat(uint64_t playerId, unsigned int statId) const;

        /**
         * Allows access to an array of multisets of BuildingStatInfo instances, for the specified
         * player, where each multiset represents a building-valued stat ID (see BuildValuedStatIds
         * in ProvinceStatTypes.h).
         * These multisets are ordered such that the best "value-for-money" buildings, in terms of
         * the relevant build-valued stat, are listed first.
         * If the player ID does not exist, an array of empty multisets is returned: Non existing or
         * unknown players are sensibly assumed to have no available buildings.
         */
        const std::array<std::multiset<BuildingStatInfo>, BuildValuedStatIds::COUNT> & getPlayerAvailableBuildings(uint64_t playerId) const;
    };
}

#endif

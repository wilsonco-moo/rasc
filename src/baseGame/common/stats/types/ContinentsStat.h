/*
 * ContinentsStat.h
 *
 *  Created on: 25 Oct 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_STATS_TYPES_CONTINENTSSTAT_H_
#define BASEGAME_COMMON_STATS_TYPES_CONTINENTSSTAT_H_

#include <unordered_map>
#include <vector>

#include "../Stat.h"

namespace rasc {

    /**
     * ContinentsStat keeps track of:
     *  > For each player, how many continents they own.
     *  > For each continent, which player (if any) owns every province within it.
     */
    class ContinentsStat : public Stat {
    private:
        const static std::vector<Stat::StatUpdateType> updateTypes;

        // For each continent, stores how many provinces are contained within it.
        std::vector<unsigned int> provinceCountPerContinent;

        // Stores for each player,
        //  > How many continents they own
        //  > For each continent they own provinces in, how many provinces.
        std::unordered_map<uint64_t, std::pair<unsigned int, std::unordered_map<uint32_t, unsigned int>>> playersMap;

        // Stores for each continent, the player ID which owns every province within it, (if any).
        std::unordered_map<uint32_t, uint64_t> continentToOwner;

    public:
        ContinentsStat(void);
        virtual ~ContinentsStat(void);

    private:
        void onGainProvince(Province & prov, uint64_t playerId, std::pair<unsigned int, std::unordered_map<uint32_t, unsigned int>> & pair);
        void onLoseProvince(Province & prov, uint64_t playerId, std::pair<unsigned int, std::unordered_map<uint32_t, unsigned int>> & pair);

    protected:

        // Misc methods
        virtual void initialise(CommonBox & box) override;
        virtual const std::vector<Stat::StatUpdateType> & getUpdateTypes(void) const override;

        // Stat update methods
        virtual void onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) override;
        virtual void onAddPlayer(uint64_t playerId) override;
        virtual void onRemovePlayer(uint64_t playerId) override;

    public:

        // ================================ Public stat accessor methods ===================================

        /**
         * Returns the total number of completely owned continents, for the specified
         * player ID. A player completely owns a continent if they own every province
         * contained within it.
         * If the specified player does not exist, then they are (sensibly) assumed
         * to own zero continents.
         */
        unsigned int getContinentCount(uint64_t playerId) const;

        /**
         * Returns the owner player of the specified continent, or PlayerData::NO_PLAYER
         * if none exists.
         * A continent has an owner player if there exists a player who owns every
         * single province contained within it.
         */
        uint64_t getContinentOwner(uint32_t continentId) const;

        /**
         * Returns the total number of provinces contained within the specified
         * continent.
         */
        unsigned int getContinentProvinceCount(uint32_t continentId) const;
    };
}

#endif

/*
 * BuildingStat.cpp
 *
 *  Created on: 5 Oct 2020
 *      Author: wilson
 */

#include "BuildingStat.h"

#include "../../gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../util/templateTypes/staticStorage/StaticVector.h"
#include "../../gameMap/properties/ProvinceProperties.h"
#include "../../gameMap/properties/trait/TraitRange.h"
#include "../../gameMap/mapElement/types/Province.h"
#include "../../gameMap/properties/trait/TraitDef.h"
#include "../../gameMap/properties/trait/Traits.h"
#include "../../gameMap/properties/PropertySet.h"
#include "../../gameMap/properties/Properties.h"
#include "../../gameMap/mapElement/types/Area.h"
#include "../../playerData/PlayerDataSystem.h"
#include "../../playerData/PlayerData.h"
#include "../../gameMap/GameMap.h"
#include "../../util/CommonBox.h"
#include "AreasStat.h"

#include <iostream>
#include <cstddef>
#include <array>

namespace rasc {

    // Empty building info, to be used as a return value for unknown player IDs.
    #define X(statName, calculation) std::multiset<BuildingStatInfo>(),
    const static std::array<std::multiset<BuildingStatInfo>, BuildValuedStatIds::COUNT> emptyBuildingInfo({ RASC_BUILDING_VALUED_PROVINCE_STATS });
    #undef X


    // =========================== BuildingStat::PerPlayerInfo ================================

    BuildingStat::PerPlayerInfo::PerPlayerInfo(uint64_t playerId) :
        playerId(playerId),
        #define X(statName, calculation) std::multiset<BuildingStatInfo>(),
        buildingInfo({ RASC_BUILDING_VALUED_PROVINCE_STATS }) {
        #undef X
    }

    void BuildingStat::PerPlayerInfo::checkRemovalSafety(void) {
        for (const std::multiset<BuildingStatInfo> & set : buildingInfo) {
            if (!set.empty()) {
                std::cerr << "WARNING: BuildingStat: " << "Player " << playerId << " removed while still having " << set.size() << " available buildings.\n";
                return;
            }
        }
    }

    const std::multiset<BuildingStatInfo> & BuildingStat::PerPlayerInfo::getBuildingInfoByStat(unsigned int statId) const {
        return buildingInfo[statId];
    }



    // ============================= BuildingStat::ProvinceInfo =================================

    BuildingStat::ProvinceInfo::ProvinceInfo(Province & province, PerPlayerInfo * playerInfo, AreasStat * areasStat) :
        isBattleOngoing(province.battleControl->isBattleOngoing()),
        areaCompletelyOwned(areasStat->getAreaOwner(province.getAreaId()) != PlayerData::NO_PLAYER),
        baseAvailableBuildings(),
        overallBlocking(),
        playerInfo(playerInfo),
        #define X(statName, calculation) std::vector<std::multiset<BuildingStatInfo>::iterator>(),
        iterators{ RASC_BUILDING_VALUED_PROVINCE_STATS } {
        #undef X

        // Build map of overall blocking traits, counting how many things block each of them.
        for (const std::pair<const proptype_t, std::unordered_set<uint64_t>> & byTypePair : province.properties.getPropertySet().getPropertiesByTypeMap()) {
            if (Properties::getBaseType(byTypePair.first) == Properties::BaseTypes::trait) {
                for (proptype_t blockingTrait : Traits::getBlockingTraits(byTypePair.first)) {
                    overallBlocking[blockingTrait]++;
                }
            }
        }

        // Look through all buildable regular building traits, to figure out (initially) which can be currently built.
        for (proptype_t trait : Traits::getRegularBuildingTraits()) {
            if (shouldMakeAvailable(province, trait)) {
                baseAvailableBuildings.insert(trait);
            }
        }
    }

    bool BuildingStat::ProvinceInfo::shouldMakeAvailable(Province & province, proptype_t trait) const {
        const PropertySet & propertySet = province.properties.getPropertySet();
        // Reject traits blocked by something already in the province.
        if (overallBlocking.find(trait) != overallBlocking.end()) {
            return false;
        }
        // Reject traits if the province is missing one of its cost-required traits.
        for (proptype_t requiredTrait : Traits::getCostRequiredTraits(trait)) {
            if (!propertySet.hasPropertiesOfType(requiredTrait)) {
                return false;
            }
        }
        // Reject non stackable traits which are already present.
        if (!Traits::isTraitStackable(trait) && propertySet.hasPropertiesOfType(trait)) {
            return false;
        }
        return true;
    }

    void BuildingStat::ProvinceInfo::clearBuildingsInSet(void) {
        // For each stat, erase all of our building valuations for that stat from the appropriate
        // building set.
        for (unsigned int statId = 0; statId < BuildValuedStatIds::COUNT; statId++) {
            for (const std::multiset<BuildingStatInfo>::iterator & iter : iterators[statId]) {
                playerInfo->buildingInfo[statId].erase(iter);
            }
            iterators[statId].clear();
        }
    }

    void BuildingStat::ProvinceInfo::onCreateTrait(Province & province, proptype_t trait) {
        // Traits which this one blocks should no longer be available. Update overallBlocking also.
        for (proptype_t blockingTrait : Traits::getBlockingTraits(trait)) {
            baseAvailableBuildings.erase(blockingTrait);
            overallBlocking[blockingTrait]++;
        }

        // If this trait is not stackable, *it* should no longer be available.
        if (!Traits::isTraitStackable(trait)) {
            baseAvailableBuildings.erase(trait);
        }

        // Add each of this trait's (regular building AND non-blocked) cost-allowing
        // traits to available.
        for (proptype_t allowingTrait : Traits::getCostAllowingTraits(trait)) {
            if (Traits::isTraitRegularBuilding(allowingTrait) && shouldMakeAvailable(province, allowingTrait)) {
                baseAvailableBuildings.insert(allowingTrait);
            }
        }
    }
    void BuildingStat::ProvinceInfo::onDeleteTrait(Province & province, proptype_t trait) {
        const PropertySet & propertySet = province.properties.getPropertySet();

        // The removed trait does not necessarily become an available building: It may exist
        // due to required traits, but without cost-required traits being satisfied. It may
        // even be a non-buildable or combat building being removed. Check to see if all of
        // its cost-required traits are satisfied, and if not skip making it an available trait.
        if (Traits::isTraitRegularBuilding(trait)) {
            for (proptype_t requiredTrait : Traits::getCostRequiredTraits(trait)) {
                if (!propertySet.hasPropertiesOfType(requiredTrait)) {
                    goto skipMakeRemovedAvailable;
                }
            }
            baseAvailableBuildings.insert(trait);
            skipMakeRemovedAvailable:;
        }

        // Update list of overall blocking buildings.
        for (proptype_t blockingTrait : Traits::getBlockingTraits(trait)) {
            auto iter = overallBlocking.find(blockingTrait);
            if (iter == overallBlocking.end()) {
                std::cerr << "WARNING: BuildingStat: " << "Missing blocking trait for removed trait " << Properties::typeToInternalName(trait) << ".\n";
            } else {
                // If nothing is blocking this trait now, remove from blocking.
                iter->second--;
                if (iter->second == 0) {
                    proptype_t potentialAvailable = iter->first;
                    overallBlocking.erase(iter);

                    if (Traits::isTraitRegularBuilding(potentialAvailable)) {
                        // Each removed blocking trait is now potentially an available building, (assuming it *is* actually a regular building trait).
                        // Don't check if it is blocked: It isn't - we just removed it from blocking.
                        // Don't check present/stackable status: It isn't present - it was being blocked a moment ago.
                        // It can't require the trait we removed, since it was *blocking* the trait we removed.
                        // Only skip making it available if one of the cost-required traits which *it* has are missing.
                        for (proptype_t requiredTrait : Traits::getCostRequiredTraits(potentialAvailable)) {
                            if (!propertySet.hasPropertiesOfType(requiredTrait)) {
                                goto skipMakeBlockingAvailable;
                            }
                        }
                        baseAvailableBuildings.insert(potentialAvailable);
                        skipMakeBlockingAvailable:;
                    }
                }
            }
        }

        // Each allowing building from the removed trait is no longer available.
        // Even if they are cost-allowed by another currently present trait, if the removed trait
        // cost-allows another trait, then that other trait cost-requires the removed (and no
        // longer present) trait - meaning it must not be available any more. Remember: required
        // is an AND relationship.
        for (proptype_t allowingTrait : Traits::getCostAllowingTraits(trait)) {
            baseAvailableBuildings.erase(allowingTrait);
        }
    }

    void BuildingStat::ProvinceInfo::onChangeBattleStatus(Province & province, bool newIsBattleOngoing) {
        if (isBattleOngoing == newIsBattleOngoing) {
            std::cerr << "WARNING: BuildingStat: " << "Province onChangeBattleStatus called with same battle status as last time (" << (isBattleOngoing ? "true" : "false") << ").\n";
        }
        isBattleOngoing = newIsBattleOngoing;
        rebuildPlayerBuildings(province);
    }

    void BuildingStat::ProvinceInfo::changeOwnership(Province & province, uint64_t oldPlayerId, PerPlayerInfo * newPlayerInfo) {
        // Changing from player to player
        if (playerInfo != NULL && newPlayerInfo != NULL) {
            if (oldPlayerId != playerInfo->playerId || oldPlayerId == newPlayerInfo->playerId) std::cerr << "WARNING: BuildingStat: " << "Wrong old player ID (" << oldPlayerId << ") or no change, when changing ownership of " << province.getName() << " from " << playerInfo->playerId << " to " << newPlayerInfo->playerId << ".\n";

            // Move buildings from old player's set to new player's set, and update iterators.
            for (unsigned int statId = 0; statId < BuildValuedStatIds::COUNT; statId++) {
                for (std::multiset<BuildingStatInfo>::iterator & iter : iterators[statId]) {
                    std::multiset<BuildingStatInfo>::iterator newIter = newPlayerInfo->buildingInfo[statId].insert(*iter);
                    playerInfo->buildingInfo[statId].erase(iter);
                    iter = newIter;
                }
            }
            playerInfo = newPlayerInfo;

        // Changing from no player to player: All that needs doing is assigning new player, THEN rebuilding buildings.
        } else if (playerInfo == NULL && newPlayerInfo != NULL) {
            if (oldPlayerId != PlayerData::NO_PLAYER) std::cerr << "WARNING: BuildingStat: " << "Wrong old player ID in province " << province.getName() << " when changing ownership from NO_PLAYER to ID " << newPlayerInfo->playerId << ".\n";
            playerInfo = newPlayerInfo;
            rebuildPlayerBuildings(province);

        // Changing from player to no player: All that needs doing is clearing the buildings and THEN assigning new player.
        } else if (playerInfo != NULL && newPlayerInfo == NULL) {
            if (oldPlayerId != playerInfo->playerId) std::cerr << "WARNING: BuildingStat: " << "Wrong old player ID (" << oldPlayerId << "), when changing ownership of " << province.getName() << " from " << playerInfo->playerId << " to NO_PLAYER.\n";
            clearBuildingsInSet();
            playerInfo = newPlayerInfo;

        // Changing from no player to no player is not allowed.
        } else {
            std::cerr << "WARNING: BuildingStat: " << "Province ownership of " << province.getName() << " changed from no player to no player.\n";
        }
    }

    void BuildingStat::ProvinceInfo::rebuildPlayerBuildings(Province & province) {
        const ProvinceProperties & properties = province.properties;

        // Do nothing if unowned (we have no player info).
        if (playerInfo == NULL) return;

        clearBuildingsInSet();

        // Iterate through all base available buildings.
        for (proptype_t trait : baseAvailableBuildings) {

            // Skip traits where construction isn't allowed during a battle, but a battle is going on.
            if (isBattleOngoing && !Traits::isTraitBattleConstructionAllowed(trait)) {
                continue;
            }

            // Get modifier from constructing a single building, skip it if building it exceeds land.
            ProvStatModSet modifier = Traits::getConstructAdditionalModifierSet(province, trait);
            if (properties.doesModifierExceedLand(modifier)) continue;

            // Calculate "value-for-money" of the building.
            std::array<float, BuildValuedStatIds::COUNT> valueArray;
            properties.calculateValueForMoney(
                valueArray, modifier, Traits::getTraitBuildingCost(NULL, trait, 1).getOverallCostEstimation(), areaCompletelyOwned);

            // For each building-valued stat with positive value-for-money, insert
            // into the appropriate player info building set, putting the iterators
            // into our iterator vectors.
            for (unsigned int statId = 0; statId < BuildValuedStatIds::COUNT; statId++) {
                if (valueArray[statId] > 0.0f) {
                    iterators[statId].push_back(
                        playerInfo->buildingInfo[statId].insert(
                            {province.getId(), valueArray[statId], trait}));
                }
            }
        }
    }

    void BuildingStat::ProvinceInfo::updateAreaAndRebuild(bool newAreaCompletelyOwned, Province & province) {
        areaCompletelyOwned = newAreaCompletelyOwned;
        rebuildPlayerBuildings(province);
    }





    // ========================================================================================

    const std::vector<Stat::StatUpdateType> BuildingStat::updateTypes = {
        Stat::StatUpdateTypes::createPropertyInProvince,
        Stat::StatUpdateTypes::deletePropertyFromProvince,
        Stat::StatUpdateTypes::changeStatsInProvince,
        Stat::StatUpdateTypes::changeProvinceOwnership,
        Stat::StatUpdateTypes::addPlayer,
        Stat::StatUpdateTypes::removePlayer,
        Stat::StatUpdateTypes::changeBattleStatusInProvince
    };

    BuildingStat::BuildingStat(void) :
        Stat(),
        provinceInfo(),
        playerInfo(),
        areasStat(NULL) {
    }

    BuildingStat::~BuildingStat(void) {
    }

    void BuildingStat::initialise(CommonBox & box) {
        provinceInfo.clear();
        playerInfo.clear();
        // We assume that areas stat has already been initialised.
        areasStat = box.areasStat;

        // Add each player id listed in the player data system.
        for (const std::pair<const uint64_t, RascUpdatable *> pair : box.playerDataSystem->mapChildren()) {
            onAddPlayer(pair.first);
        }

        // Add each province in the map. Find it's owner PerPlayerInfo, use
        // NULL for NO_PLAYER, complain if the player cannot be found.
        provinceInfo.reserve(box.map->getProvinceCount());
        for (Province * province : box.map->getProvinces()) {
            PerPlayerInfo * perPlayerInfo = NULL;
            uint64_t owner = province->getProvinceOwner();
            if (owner != PlayerData::NO_PLAYER) {
                auto iter = playerInfo.find(owner);
                if (iter == playerInfo.end()) {
                    std::cerr << "WARNING: BuildingStat: " << "Province " << province->getName() << " is owned by non-existing player ID: " << owner << "\n";
                } else {
                    perPlayerInfo = &(iter->second);
                }
            }
            provinceInfo.emplace_back(*province, perPlayerInfo, areasStat);
        }
    }

    const std::vector<Stat::StatUpdateType> & BuildingStat::getUpdateTypes(void) const {
        return updateTypes;
    }

    void BuildingStat::onCreatePropertyInProvince(Province & province, uint64_t propertyUniqueId) {
        // Get storage, complain if it doesn't exist.
        PropertySet::TypeAndStorage * storage = province.properties.getPropertySet().getPropertyStorage(propertyUniqueId);
        if (storage == NULL) {
            std::cerr << "WARNING: BuildingStat: " << "Property ID " << propertyUniqueId << " reported as added to province, but does not exist.\n";
            return;
        }
        proptype_t propertyType = storage->getType();
        if (Properties::getBaseType(propertyType) == Properties::BaseTypes::trait) {
            provinceInfo[province.getId()].onCreateTrait(province, propertyType);
            // If the property does not actually impart a stat effect, run rebuildPlayerBuildings
            // now since we will not get a related onChangeStatsInProvince call.
            if (Traits::propertyGetStatModifierSet(propertyType, storage->getStorage()).isZero()) {
                provinceInfo[province.getId()].rebuildPlayerBuildings(province);
            }
        }
    }

    void BuildingStat::onDeletePropertyFromProvince(Province & province, uint64_t propertyUniqueId) {
        // Get storage, complain if it doesn't exist.
        PropertySet::TypeAndStorage * storage = province.properties.getPropertySet().getPropertyStorage(propertyUniqueId);
        if (storage == NULL) {
            std::cerr << "WARNING: BuildingStat: " << "Property ID " << propertyUniqueId << " reported as deleted from province, but did not exist in the first place.\n";
            return;
        }
        proptype_t propertyType = storage->getType();
        if (Properties::getBaseType(propertyType) == Properties::BaseTypes::trait) {
            provinceInfo[province.getId()].onDeleteTrait(province, propertyType);
            // If the property does not actually impart a stat effect, run rebuildPlayerBuildings
            // now since we will not get a related onChangeStatsInProvince call.
            if (Traits::propertyGetStatModifierSet(propertyType, storage->getStorage()).isZero()) {
                provinceInfo[province.getId()].rebuildPlayerBuildings(province);
            }
        }
    }

    void BuildingStat::onChangeStatsInProvince(Province & province, const StatArr & addedStats) {
        // Rebuild the player buildings, since a change to province stats likely affects
        // which buildings are better value.
        provinceInfo[province.getId()].rebuildPlayerBuildings(province);
    }

    void BuildingStat::onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) {
        // Find new owner, complain if cannot find it.
        PerPlayerInfo * newOwnerInfo = NULL;
        if (newOwner != PlayerData::NO_PLAYER) {
            auto iter = playerInfo.find(newOwner);
            if (iter == playerInfo.end()) {
                std::cerr << "WARNING: BuildingStat: " << "Province " << province.getName() << " changed ownership to non-existing player ID: " << newOwner << "\n";
            } else {
                newOwnerInfo = &(iter->second);
            }
        }
        // Update the province info with the new owner.
        ProvinceInfo & targetInfo = provinceInfo[province.getId()];
        targetInfo.changeOwnership(province, oldOwner, newOwnerInfo);

        // If area ownership status has changed, update it for all provinces in that area.
        bool areaCompletelyOwned = (areasStat->getAreaOwner(province.getAreaId()) != PlayerData::NO_PLAYER);
        if (targetInfo.isAreaCompletelyOwned() != areaCompletelyOwned) {
            GameMap & map = province.getGameMap();
            for (uint32_t provinceId : map.getAreaRaw(province.getAreaId()).getProvinces()) {
                provinceInfo[provinceId].updateAreaAndRebuild(areaCompletelyOwned, map.getProvince(provinceId));
            }
        }
    }

    void BuildingStat::onAddPlayer(uint64_t playerId) {
        auto iter = playerInfo.find(playerId);
        if (iter == playerInfo.end()) {
            playerInfo.emplace(playerId, playerId);
        } else {
            std::cerr << "WARNING: BuildingStat: " << "Player ID " << playerId << " added twice.\n";
        }
    }

    void BuildingStat::onRemovePlayer(uint64_t playerId) {
        auto iter = playerInfo.find(playerId);
        if (iter == playerInfo.end()) {
            std::cerr << "WARNING: BuildingStat: " << "Cannot find removed player ID: " << playerId << ".\n";
        } else {
            iter->second.checkRemovalSafety();
            playerInfo.erase(iter);
        }
    }

    void BuildingStat::onChangeBattleStatusInProvince(Province & province, bool isBattleOngoing) {
        provinceInfo[province.getId()].onChangeBattleStatus(province, isBattleOngoing);
    }

    const std::unordered_set<proptype_t> & BuildingStat::getBaseAvailableBuildings(uint32_t provinceId) const {
        return provinceInfo[provinceId].getBaseAvailableBuildings();
    }

    const std::multiset<BuildingStatInfo> & BuildingStat::getPlayerAvailableBuildingsByStat(uint64_t playerId, unsigned int statId) const {
        auto iter = playerInfo.find(playerId);
        if (iter == playerInfo.end()) {
            return emptyBuildingInfo[statId];
        } else {
            return iter->second.getBuildingInfoByStat(statId);
        }
    }

    const std::array<std::multiset<BuildingStatInfo>, BuildValuedStatIds::COUNT> & BuildingStat::getPlayerAvailableBuildings(uint64_t playerId) const {
        auto iter = playerInfo.find(playerId);
        if (iter == playerInfo.end()) {
            return emptyBuildingInfo;
        } else {
            return iter->second.getBuildingInfo();
        }
    }
}

/*
 * ProvincesStat.cpp
 *
 *  Created on: 25 Oct 2019
 *      Author: wilson
 */

#include "ProvincesStat.h"

#include <iostream>

#include "../../gameMap/mapElement/types/Province.h"
#include "../../playerData/PlayerDataSystem.h"
#include "../../playerData/PlayerData.h"
#include "../../gameMap/GameMap.h"
#include "../../util/CommonBox.h"

namespace rasc {

    const std::vector<Stat::StatUpdateType> ProvincesStat::updateTypes = {
        Stat::StatUpdateTypes::changeProvinceOwnership,
        Stat::StatUpdateTypes::addPlayer,
        Stat::StatUpdateTypes::removePlayer,
        Stat::StatUpdateTypes::changeStatsInProvince
    };

    const ProvincesStatInfo ProvincesStat::emptyProvinceInfo(PlayerData::NO_PLAYER);

    // ------------------------------ Province info nested class -------------------------------------------

    bool ProvincesStatInfo::ProvCmp::operator()(const std::pair<provstat_t, Province *> & pair1,
                                                const std::pair<provstat_t, Province *> & pair2) const {
        if (pair1.first == pair2.first) {
            return pair1.second->getId() < pair2.second->getId();
        } else {
            return pair1.first < pair2.first;
        }
    }

    ProvincesStatInfo::ProvincesStatInfo(uint64_t playerId) :
        playerId(playerId),
        ownedProvinces(),
        edgeProvinces(),
        centreProvinces(),
        adjacentProvinces(),
        provincesByValue() {
    }

    void ProvincesStatInfo::chooseEdgeOrCentre(Province & province) {
        bool isEdge = false;
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = province.getGameMap().getProvince(adjProvId);
            if (adjProv.getProvinceOwner() != playerId && adjProv.isProvinceAccessible()) {
                isEdge = true;
                break;
            }
        }
        if (isEdge) {
            edgeProvinces.insert(&province);
            centreProvinces.erase(&province);
        } else {
            edgeProvinces.erase(&province);
            centreProvinces.insert(&province);
        }
    }

    void ProvincesStatInfo::chooseAdjacent(Province & province) {
        bool isAdjacent = false;
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = province.getGameMap().getProvince(adjProvId);
            if (adjProv.getProvinceOwner() == playerId) {
                isAdjacent = true;
                break;
            }
        }
        if (isAdjacent) {
            adjacentProvinces.insert(&province);
        } else {
            adjacentProvinces.erase(&province);
        }
    }

    void ProvincesStatInfo::onGainProvince(ProvincesStat & stat, Province & province) {
        if (ownedProvinces.empty()) {
            stat.countOfPlayersWithLand++;
        }

        bool isEdge = false;
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = province.getGameMap().getProvince(adjProvId);
            if (adjProv.getProvinceOwner() == playerId) {
                chooseEdgeOrCentre(adjProv);
            } else {
                if (adjProv.isProvinceAccessible()) {
                    adjacentProvinces.insert(&adjProv);
                    isEdge = true;
                }
            }
        }
        ownedProvinces.insert(&province);
        provincesByValue.emplace(province.properties[ProvStatIds::provinceValue], &province);
        adjacentProvinces.erase(&province);
        if (isEdge) {
            edgeProvinces.insert(&province);
        } else {
            centreProvinces.insert(&province);
        }
    }

    void ProvincesStatInfo::onLoseProvince(ProvincesStat & stat, Province & province) {
        bool isAdjacent = false;
        for (uint32_t adjProvId : province.getAdjacent()) {
            Province & adjProv = province.getGameMap().getProvince(adjProvId);
            if (adjProv.getProvinceOwner() == playerId) {
                isAdjacent = true;
                edgeProvinces.insert(&adjProv);
                centreProvinces.erase(&adjProv);
            } else {
                if (adjProv.isProvinceAccessible()) {
                    chooseAdjacent(adjProv);
                }
            }
        }
        ownedProvinces.erase(&province);
        provincesByValue.erase(std::make_pair(province.properties[ProvStatIds::provinceValue], &province));
        edgeProvinces.erase(&province);
        centreProvinces.erase(&province);
        if (isAdjacent) {
            adjacentProvinces.insert(&province);
        }

        if (ownedProvinces.empty()) {
            stat.countOfPlayersWithLand--;
        }
    }

    void ProvincesStatInfo::onChangeStatsInProvince(Province & province, const StatArr & addedStats) {
        // We don't care about changes, unless they affect province value.
        provstat_t addedValue = addedStats[ProvStatIds::provinceValue];
        if (addedValue == 0) return;

        // Figure out the old and new province value.
        provstat_t newProvValue = province.properties[ProvStatIds::provinceValue],
                   oldProvValue = newProvValue - addedValue;

        // Erase the province from provincesByValue, and re-add it. Complain if erasing failed.
        if (provincesByValue.erase(std::make_pair(oldProvValue, &province)) != 1) {
            std::cerr << "WARNING: ProvincesStat: Could not find province " << province.getName() << " within player ID " << playerId << ", to change stats.\n";
        }
        provincesByValue.emplace(newProvValue, &province);
    }

    // --------------------------------------------------------------------------

    ProvincesStat::ProvincesStat(void) {
    }

    ProvincesStat::~ProvincesStat(void) {
    }

     // ------------------------------- Misc methods ---------------------------------------

    void ProvincesStat::initialise(CommonBox & box) {

        // Clear data structures.
        playerProvinceInfo.clear();
        countOfPlayersWithLand = 0;

        // Add all existing players from the player data system.
        for (const std::pair<const uint64_t, RascUpdatable *> pair : box.playerDataSystem->mapChildren()) {
            onAddPlayer(pair.first);
        }

        // Add all existing province ownership.
        for (Province * province : box.map->getProvinces()) {
            onChangeProvinceOwnership(*province, PlayerData::NO_PLAYER, province->getProvinceOwner());
        }
    }

    const std::vector<Stat::StatUpdateType> & ProvincesStat::getUpdateTypes(void) const {
        return updateTypes;
    }

    // ----------------------------------- Stat updates -----------------------------------

    void ProvincesStat::onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) {
        if (oldOwner != PlayerData::NO_PLAYER) {
            std::unordered_map<uint64_t, ProvincesStatInfo>::iterator iter = playerProvinceInfo.find(oldOwner);
            if (iter == playerProvinceInfo.end()) {
                std::cerr << "WARNING: ProvinceStat: Seemingly unknown player " << oldOwner << " lost a province.\n";
            } else {
                iter->second.onLoseProvince(*this, province);
            }
        }

        if (newOwner != PlayerData::NO_PLAYER) {
            std::unordered_map<uint64_t, ProvincesStatInfo>::iterator iter = playerProvinceInfo.find(newOwner);
            if (iter == playerProvinceInfo.end()) {
                std::cerr << "WARNING: ProvinceStat: Seemingly unknown player " << newOwner << " gained a province.\n";
            } else {
                iter->second.onGainProvince(*this, province);
            }
        }
    }

    void ProvincesStat::onChangeStatsInProvince(Province & province, const StatArr & addedStats) {
        uint64_t ownerPlayer = province.getProvinceOwner();
        if (ownerPlayer != PlayerData::NO_PLAYER) {
            std::unordered_map<uint64_t, ProvincesStatInfo>::iterator iter = playerProvinceInfo.find(ownerPlayer);
            if (iter == playerProvinceInfo.end()) {
                std::cerr << "WARNING: ProvinceStat: Stats changed in province owned by unknown player " << ownerPlayer << ".\n";
            } else {
                iter->second.onChangeStatsInProvince(province, addedStats);
            }
        }
    }

    void ProvincesStat::onAddPlayer(uint64_t playerId) {
        if (!playerProvinceInfo.emplace(playerId, playerId).second) {
            std::cerr << "WARNING: ProvincesStat: Player " << playerId << " added twice.\n";
        }
    }

    void ProvincesStat::onRemovePlayer(uint64_t playerId) {
        std::unordered_map<uint64_t, ProvincesStatInfo>::iterator iter = playerProvinceInfo.find(playerId);
        if (iter == playerProvinceInfo.end()) {
            std::cerr << "WARNING: ProvincesStat: Seemingly non existent player " << playerId << " removed.\n";
        } else {
            playerProvinceInfo.erase(iter);
        }
    }

    // ================================ Public stat accessor methods ===================================

    unsigned int ProvincesStat::getProvinceCount(uint64_t playerId) const {
        std::unordered_map<uint64_t, ProvincesStatInfo>::const_iterator iter = playerProvinceInfo.find(playerId);
        if (iter == playerProvinceInfo.end()) {
            return 0;
        } else {
            return iter->second.getOwnedProvinces().size();
        }
    }

    const ProvincesStatInfo & ProvincesStat::getOwnershipInfo(uint64_t playerId) const {
        std::unordered_map<uint64_t, ProvincesStatInfo>::const_iterator iter = playerProvinceInfo.find(playerId);
        if (iter == playerProvinceInfo.end()) {
            return emptyProvinceInfo;
        } else {
            return iter->second;
        }
    }
}

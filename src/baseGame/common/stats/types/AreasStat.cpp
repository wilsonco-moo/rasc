/*
 * AreasStat.cpp
 *
 *  Created on: 25 Oct 2019
 *      Author: wilson
 */

#include "AreasStat.h"

#include <iostream>

#include "../../util/numeric/ElementWiseArrayMaths.h"
#include "../../gameMap/mapElement/types/Province.h"
#include "../../playerData/PlayerDataSystem.h"
#include "../../playerData/PlayerData.h"
#include "../../gameMap/GameMap.h"
#include "../../util/CommonBox.h"

namespace rasc {

    // ---------------------------- Static stuff ------------------------------------------------

    const provstat_t AreasStat::MANPOWER_PER_UNIT = 1;

    const std::vector<Stat::StatUpdateType> AreasStat::updateTypes = {
        Stat::StatUpdateTypes::changeProvinceOwnership,
        Stat::StatUpdateTypes::changeStatsInProvince,
        Stat::StatUpdateTypes::addPlayer,
        Stat::StatUpdateTypes::removePlayer
    };

    #define X(name, nameCaps, baseValue, ownedMult, unownedMult) baseValue,
    const std::array<provstat_t, AreasStat::CountedStats::COUNT> AreasStat::baseStatsArr = { RASC_AREAS_STAT_COUNTED_STATS };
    #undef X
    const AreasStat::SummedStats AreasStat::baseStats(baseStatsArr);

    // --------------------------------- Summed stats -------------------------------------------

    AreasStat::SummedStats::SummedStats(void) :
        // Use an initial value of zero for all stats.
        statValues{0} {
    }
    AreasStat::SummedStats::SummedStats(Province & province) :
        // Extract each stat from the appropriately named one in the province properties, using the X macros.
        #define X(name, nameCaps, baseValue, ownedMult, unownedMult) province.properties[ProvStatIds::name],
        statValues{ RASC_AREAS_STAT_COUNTED_STATS } {
        #undef X
    }
    AreasStat::SummedStats AreasStat::SummedStats::fromStatArray(const StatArr & statArr) {
        // Extract each stat from the appropriately named one in the stat array, using the X macros.
        #define X(name, nameCaps, baseValue, ownedMult, unownedMult) statArr[ProvStatIds::name],
        std::array<provstat_t, CountedStats::COUNT> stats = { RASC_AREAS_STAT_COUNTED_STATS };
        #undef X
        return SummedStats(stats);
    }
    AreasStat::SummedStats::SummedStats(const std::array<provstat_t, CountedStats::COUNT> & statValues) :
        statValues(statValues) {
    }
    AreasStat::SummedStats AreasStat::SummedStats::operator + (const SummedStats & other) {
        // Generate new stat value array, from element-wise sum of our stat values, and their stat values.
        return SummedStats(statValues + other.statValues);
    }
    AreasStat::SummedStats & AreasStat::SummedStats::operator += (const SummedStats & other) {
        // Simply element-wise add their stats onto ours.
        statValues += other.statValues;
        return *this;
    }
    AreasStat::SummedStats AreasStat::SummedStats::operator - (const SummedStats & other) {
        // Generate new stat value array, from element-wise subtraction of our stat values, and their stat values.
        return SummedStats(statValues - other.statValues);
    }
    AreasStat::SummedStats & AreasStat::SummedStats::operator -= (const SummedStats & other) {
        // Simply element-wise subtract their stats from ours.
        statValues -= other.statValues;
        return *this;
    }
    bool AreasStat::SummedStats::operator == (const SummedStats & other) const {
        return statValues == other.statValues;
    }
    void AreasStat::SummedStats::print(void) const {
        #define X(name, nameCaps, baseValue, ownedMult, unownedMult) << #name << statValues[CountedStats::name] << ", "
        std::cerr << '[' RASC_AREAS_STAT_COUNTED_STATS << ']';
        #undef X
    }
    AreasStat::SummedStats AreasStat::SummedStats::applyAreaBonus(bool ownWholeArea) const {
        // Generate a summed stats, with with each value either run through ownedMult or unownedMult
        // depending on whether we own the whole area or not.
        std::array<provstat_t, CountedStats::COUNT> returnStats;
        provstat_t value;
        if (ownWholeArea) {
            #define X(name, nameCaps, baseValue, ownedMult, unownedMult) \
                value = statValues[CountedStats::name];                  \
                value = ownedMult;                                       \
                returnStats[CountedStats::name] = value;
            RASC_AREAS_STAT_COUNTED_STATS
            #undef X
        } else {
            #define X(name, nameCaps, baseValue, ownedMult, unownedMult) \
                value = statValues[CountedStats::name];                  \
                value = unownedMult;                                     \
                returnStats[CountedStats::name] = value;
            RASC_AREAS_STAT_COUNTED_STATS
            #undef X
        }
        return SummedStats(returnStats);
    }

    // -------------------------------- Per player info -----------------------------------------

        // ------------------------ Per area info -----------------------------

        AreasStat::PerPlayerInfo::PerAreaInfo::PerAreaInfo(unsigned int maxProvinces) :
            provincesOwned(0),
            maxProvinces(maxProvinces),
            areaStats() { // Initialise SummedStats with zeroes: We don't want an additional base stats in ALL areas.
        }
        void AreasStat::PerPlayerInfo::PerAreaInfo::onGainProvince(Province & province) {
            provincesOwned++;
            areaStats += SummedStats(province);
        }
        void AreasStat::PerPlayerInfo::PerAreaInfo::onLoseProvince(Province & province) {
            provincesOwned--;
            areaStats -= SummedStats(province);
        }
        void AreasStat::PerPlayerInfo::PerAreaInfo::onChangeStats(const StatArr & addedStats) {
            areaStats += SummedStats::fromStatArray(addedStats);
        }
        void AreasStat::PerPlayerInfo::PerAreaInfo::addToPerPlayerInfo(PerPlayerInfo & info) {
            bool ownsEntire = ownsEntireArea();
            info.areasOwned += (ownsEntire ? 1 : 0);
            info.overallStats += areaStats.applyAreaBonus(ownsEntire);
        }
        void AreasStat::PerPlayerInfo::PerAreaInfo::subtractFromPerPlayerInfo(PerPlayerInfo & info) {
            bool ownsEntire = ownsEntireArea();
            info.areasOwned -= (ownsEntire ? 1 : 0);
            info.overallStats -= areaStats.applyAreaBonus(ownsEntire);
        }
        void AreasStat::PerPlayerInfo::PerAreaInfo::checkRemovalSafety(void) {
            if (provincesOwned > 0) {
                std::cerr << "WARNING: NewAreasStat: Area ownership removed while still containing owned provinces.\n";
            }
            if (areaStats != SummedStats()) {
                std::cerr << "WARNING: NewAreasStat: Area ownership removed while still gaining stats from the area.\n";
            }
        }

    // ------------------------------ Per player info main -------------------------------------------------------

    AreasStat::PerPlayerInfo::PerPlayerInfo(uint64_t playerId) :
        overallStats(baseStats), // Initialise OVERALL (i.e: not per area) stats with base stats. We want ONE set of additional base stats per PLAYER, not per area.
        areasOwned(0),
        playerId(playerId),
        perAreaStats() {
    }

    AreasStat::PerPlayerInfo::PerAreaInfo & AreasStat::PerPlayerInfo::getOrCreatePerAreaInfo(AreasStat & areasStat, uint32_t areaId) {
        std::unordered_map<uint32_t, PerAreaInfo>::iterator iter = perAreaStats.find(areaId);
        if (iter == perAreaStats.end()) {
            return perAreaStats.emplace(areaId, PerAreaInfo(areasStat.provincesCountPerArea[areaId])).first->second;
        } else {
            return iter->second;
        }
    }

    void AreasStat::PerPlayerInfo::checkRemovalSafety(void) {
        if (overallStats != baseStats) {
            std::cerr << "WARNING: NewAreasStat: Player removed while not having base stats: ";
            overallStats.print(); std::cerr << "\n";
        }
        if (areasOwned > 0 || !perAreaStats.empty()) std::cerr << "WARNING: NewAreasStat: Player removed while still owning areas.\n";
    }

    const AreasStat::PerPlayerInfo::PerAreaInfo * AreasStat::PerPlayerInfo::accessPerAreaInfo(uint32_t areaId) const {
        std::unordered_map<uint32_t, PerAreaInfo>::const_iterator iter = perAreaStats.find(areaId);
        if (iter == perAreaStats.cend()) {
            return NULL;
        } else {
            return &iter->second;
        }
    }

    void AreasStat::PerPlayerInfo::onGainProvince(AreasStat & areasStat, uint64_t playerId, Province & province) {
        PerAreaInfo & info = getOrCreatePerAreaInfo(areasStat, province.getAreaId());
        info.subtractFromPerPlayerInfo(*this);
        info.onGainProvince(province);
        info.addToPerPlayerInfo(*this);
        if (info.ownsEntireArea()) {
            areasStat.areaToOwner[province.getAreaId()] = playerId;
            areasStat.areasCompletelyOwned++;
        }
    }
    void AreasStat::PerPlayerInfo::onLoseProvince(AreasStat & areasStat, uint64_t playerId, Province & province) {
        PerAreaInfo & info = getOrCreatePerAreaInfo(areasStat, province.getAreaId());
        info.subtractFromPerPlayerInfo(*this);
        info.onLoseProvince(province);
        if (info.ownsAllButOne()) {
            areasStat.areaToOwner.erase(province.getAreaId());
            areasStat.areasCompletelyOwned--;
        }
        if (info.ownsNoProvinces()) {
            info.checkRemovalSafety();
            perAreaStats.erase(province.getAreaId());
        } else {
            info.addToPerPlayerInfo(*this);
        }
    }
    void AreasStat::PerPlayerInfo::onChangeStats(AreasStat & areasStat, Province & province, const StatArr & addedStats) {
        PerAreaInfo & info = getOrCreatePerAreaInfo(areasStat, province.getAreaId());
        info.subtractFromPerPlayerInfo(*this);
        info.onChangeStats(addedStats);
        info.addToPerPlayerInfo(*this);
    }

    // ======================================== Areas Stat main =======================================

    AreasStat::AreasStat(void) :
        Stat(),
        infoPerPlayer(),
        provincesCountPerArea(),
        areaToOwner(),
        areasCompletelyOwned(0) {
    }

    AreasStat::~AreasStat(void) {
    }

    // ------------------------------- Misc methods ---------------------------------------

    void AreasStat::initialise(CommonBox & box) {

        // Clear existing data structures.
        provincesCountPerArea.clear();
        infoPerPlayer.clear();
        areaToOwner.clear();
        areasCompletelyOwned = 0;

        // Count the number of provinces contained within each area.
        provincesCountPerArea.insert(provincesCountPerArea.end(), box.map->getAreaCount(), 0);
        for (Province * prov : box.map->getProvinces()) {
            provincesCountPerArea[prov->getAreaId()]++;
        }

        // Add each player id listed in the player data system.
        for (const std::pair<const uint64_t, RascUpdatable *> pair : box.playerDataSystem->mapChildren()) {
            onAddPlayer(pair.first);
        }

        // Add ownership of each province.
        for (Province * prov : box.map->getProvinces()) {
            onChangeProvinceOwnership(*prov, PlayerData::NO_PLAYER, prov->getProvinceOwner());
        }
    }

    const std::vector<Stat::StatUpdateType> & AreasStat::getUpdateTypes(void) const {
        return updateTypes;
    }

    // ----------------------------------- Stat updates -----------------------------------

    void AreasStat::onChangeProvinceOwnership(Province & province, uint64_t oldOwner, uint64_t newOwner) {

        if (oldOwner != PlayerData::NO_PLAYER) {
            std::unordered_map<uint64_t, PerPlayerInfo>::iterator iter = infoPerPlayer.find(oldOwner);
            if (iter == infoPerPlayer.end()) {
                std::cerr << "WARNING: AreasStat: Seemingly unknown player " << oldOwner << " lost province " << province.getName() << ".\n";
            } else {
                iter->second.onLoseProvince(*this, oldOwner, province);
            }
        }

        if (newOwner != PlayerData::NO_PLAYER) {
            std::unordered_map<uint64_t, PerPlayerInfo>::iterator iter = infoPerPlayer.find(newOwner);
            if (iter == infoPerPlayer.end()) {
                std::cerr << "WARNING: AreasStat: Seemingly unknown player " << newOwner << " gained province " << province.getName() << ".\n";
            } else {
                iter->second.onGainProvince(*this, newOwner, province);
            }
        }
    }

    void AreasStat::onChangeStatsInProvince(Province & province, const StatArr & addedStats) {
        uint64_t owner = province.getProvinceOwner();
        std::unordered_map<uint64_t, PerPlayerInfo>::iterator iter = infoPerPlayer.find(owner);
        if (iter == infoPerPlayer.end()) {
            if (owner != PlayerData::NO_PLAYER) {
                std::cerr << "WARNING: AreasStat: Seemingly unknown player " << owner << " gained province " << province.getName() << ".\n";
            }
        } else {
            iter->second.onChangeStats(*this, province, addedStats);
        }
    }

    void AreasStat::onAddPlayer(uint64_t playerId) {
        if (!infoPerPlayer.emplace(playerId, playerId).second) {
            std::cerr << "WARNING: AreasStat: Player id " << playerId << " added when seemingly already existing.\n";
        }
    }

    void AreasStat::onRemovePlayer(uint64_t playerId) {
        std::unordered_map<uint64_t, PerPlayerInfo>::iterator iter = infoPerPlayer.find(playerId);
        if (iter == infoPerPlayer.end()) {
            std::cerr << "WARNING: AreasStat: Seemingly unknown player " << playerId << " removed.\n";
        } else {
            iter->second.checkRemovalSafety();
            infoPerPlayer.erase(iter);
        }
    }

    // ================================ Public stat accessor methods ===================================

    unsigned int AreasStat::getAreaCount(uint64_t playerId) const {
        std::unordered_map<uint64_t, PerPlayerInfo>::const_iterator iter = infoPerPlayer.find(playerId);
        if (iter == infoPerPlayer.cend()) {
            return 0;
        } else {
            return iter->second.getAreasOwned();
        }
    }

    uint64_t AreasStat::getAreaOwner(uint32_t areaId) const {
        std::unordered_map<uint32_t, uint64_t>::const_iterator iter = areaToOwner.find(areaId);
        if (iter == areaToOwner.end()) {
            return PlayerData::NO_PLAYER;
        } else {
            return iter->second;
        }
    }

    // Define each stat accessor method, using X macro.
    #define X(name, nameCaps, baseValue, ownedMult, unownedMult)                                                \
        provstat_t AreasStat::getPlayer##nameCaps(uint64_t playerId) const {                                    \
            std::unordered_map<uint64_t, PerPlayerInfo>::const_iterator iter = infoPerPlayer.find(playerId);    \
            if (iter == infoPerPlayer.cend()) {                                                                 \
                return 0;                                                                                       \
            } else {                                                                                            \
                return iter->second.getOverallStats().getStatValues()[CountedStats::name];                      \
            }                                                                                                   \
        }
    RASC_AREAS_STAT_COUNTED_STATS
    #undef X

    unsigned int AreasStat::getOwnedWithinArea(uint64_t playerId, uint32_t areaId) const {
        std::unordered_map<uint64_t, PerPlayerInfo>::const_iterator iter = infoPerPlayer.find(playerId);
        if (iter == infoPerPlayer.cend()) {
            return 0;
        } else {
            const PerPlayerInfo::PerAreaInfo * info = iter->second.accessPerAreaInfo(areaId);
            if (info == NULL) {
                return 0;
            } else {
                return info->getProvincesOwned();
            }
        }
    }

    std::pair<unsigned int, unsigned int> AreasStat::getOwnedAndTotalWithinArea(uint64_t playerId, uint32_t areaId) const {
        std::unordered_map<uint64_t, PerPlayerInfo>::const_iterator iter = infoPerPlayer.find(playerId);
        if (iter == infoPerPlayer.cend()) {
            return std::make_pair(0, provincesCountPerArea[areaId]);
        } else {
            const PerPlayerInfo::PerAreaInfo * info = iter->second.accessPerAreaInfo(areaId);
            if (info == NULL) {
                return std::make_pair(0, provincesCountPerArea[areaId]);;
            } else {
                return std::make_pair(info->getProvincesOwned(), provincesCountPerArea[areaId]);
            }
        }
    }
}

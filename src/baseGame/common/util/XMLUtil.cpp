/*
 * XMLUtil.h
 *
 *  Created on: 14 Aug 2018
 *      Author: wilson
 */

#include "XMLUtil.h"

#include <tinyxml2/tinyxml2.h>
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <cstddef>
#include <string>

namespace rasc {

    void loadFile(tinyxml2::XMLDocument & document, const char * filename) {
        if (document.LoadFile(filename) != tinyxml2::XML_SUCCESS) {
            std::cerr << "CRITICAL ERROR: XMLUtil: " << "Failed to find/load/parse XML file: " << filename << ".\n";
            exit(EXIT_FAILURE);
        }
    }

    tinyxml2::XMLElement * readElement(tinyxml2::XMLDocument & document, const char * name) {
        tinyxml2::XMLElement * node = document.FirstChildElement(name);
        if (node == NULL) {
            std::cerr << "CRITICAL ERROR: XMLUtil: " << "Could not find XML tag <" << name << "> in XML file.\n";
            exit(EXIT_FAILURE);
        } else {
            return node;
        }
    }

    tinyxml2::XMLElement * readElement(tinyxml2::XMLElement * element, const char * name) {
        tinyxml2::XMLElement * node = element->FirstChildElement(name);
        if (node == NULL) {
            std::cerr << "CRITICAL ERROR: XMLUtil: " << "Could not find XML tag <" << name << "> in XML file.\n";
            exit(EXIT_FAILURE);
        } else {
            return node;
        }
    }
    
    tinyxml2::XMLElement * readElement(tinyxml2::XMLElement * element, size_t id) {
        size_t upto = 0;
        loopElementsAll(childElem, element) {
            if (upto == id) {
                return childElem;
            }
            upto++;
        }
        std::cerr << "CRITICAL ERROR: XMLUtil: " << "Could not find child element ID: " << id << ", since there were only " << upto << " child elements.\n";
        exit(EXIT_FAILURE);
    }

    const char * readAttribute(tinyxml2::XMLElement * element, const char * name) {
        const char * attr = element->Attribute(name);
        if (attr == NULL) {
            std::cerr << "CRITICAL ERROR: XMLUtil: " << "Could not find XML attribute " << name << " in XML file.\n";
            exit(EXIT_FAILURE);
        } else {
            return attr;
        }
    }

    uint32_t hexStringToInt(const char * string) {
        // NOTE: A stringstream is used here because Windoze strtol() implementation
        // gets very confused by hexadecimal values, with a 1 as their first bit, (due to conversion from
        // signed to unsigned).
        std::stringstream ss;
        ss << std::hex << string;
        uint32_t value;
        ss >> value;
        return value;
    }
}

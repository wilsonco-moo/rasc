/*
 * Misc.h
 *
 *  Created on: 29 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_MISC_H_
#define BASEGAME_COMMON_UTIL_MISC_H_

#include <GL/gl.h>
#include <climits>
#include <cstdint>
#include <utility>
#include <cstddef>
#include <string>
#include <vector>
#include <array>

#include "../gameMap/provinceUtil/ProvinceStatTypes.h"

namespace threading {
    class ThreadQueue;
}

namespace rascUI {
    class ScrollPane;
    class ToggleButton;
    class ToggleButtonSeries;
    class Theme;
    class TopLevelContainer;
}

namespace tinyxml2 {
    class XMLDocument;
    class XMLElement;
}

namespace rasc {

    class Misc {
    public:
        Misc(void);
        virtual ~Misc(void);

        const static GLfloat RIGHT_ANGLE;
        const static double RIGHT_ANGLE_PREC;

        static void drawThickLine(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat radius);

        // ===================================================================

        #define MISC_SCALE_TOPLEFT(x, y)                                                           \
            GLfloat miscScaleBaseX = (x) * (*cameraProperties.uiScale),                            \
                    miscScaleBaseY = (y) * (*cameraProperties.uiScale),                            \
                    miscScaleX1, miscScaleY1;

        #define MISC_SCALE_TOPRIGHT(x, y)                                                          \
        GLfloat miscScaleBaseX = cameraProperties.viewWidth - (x) * (*cameraProperties.uiScale),   \
                miscScaleBaseY = (y) * (*cameraProperties.uiScale),                                \
                miscScaleX1, miscScaleY1;

        #define MISC_SCALE_BOTTOMLEFT(x, y)                                                        \
        GLfloat miscScaleBaseX = (x) * (*cameraProperties.uiScale),                                \
                miscScaleBaseY = cameraProperties.viewHeight - (y) * (*cameraProperties.uiScale),  \
                miscScaleX1, miscScaleY1;

        #define MISC_SCALE_BOTTOMRIGHT(x, y)                                                       \
        GLfloat miscScaleBaseX = cameraProperties.viewWidth - (x) * (*cameraProperties.uiScale),   \
                miscScaleBaseY = cameraProperties.viewHeight - (y) * (*cameraProperties.uiScale),  \
                miscScaleX1, miscScaleY1;


        #define MISC_SCALE_RECT(x, y, width, height) {                                             \
                    miscScaleX1 = miscScaleBaseX + (x) * (*cameraProperties.uiScale);              \
                    miscScaleY1 = miscScaleBaseY + (y) * (*cameraProperties.uiScale);              \
            GLfloat miscScaleX2 = miscScaleX1 + (width) * (*cameraProperties.uiScale),             \
                    miscScaleY2 = miscScaleY1 + (height) * (*cameraProperties.uiScale);            \
            glVertex2f(miscScaleX1, miscScaleY1);                                                  \
            glVertex2f(miscScaleX2, miscScaleY1);                                                  \
            glVertex2f(miscScaleX2, miscScaleY2);                                                  \
            glVertex2f(miscScaleX1, miscScaleY2);                                                  \
        }


        #define MISC_SCALE_RECT_TWOPOINTS(x1, y1, x2, y2) {                                        \
                    miscScaleX1 = miscScaleBaseX + (x1) * (*cameraProperties.uiScale);             \
                    miscScaleY1 = miscScaleBaseY + (y1) * (*cameraProperties.uiScale);             \
            GLfloat miscScaleX2 = miscScaleBaseX + (x2) * (*cameraProperties.uiScale),             \
                    miscScaleY2 = miscScaleBaseY + (y2) * (*cameraProperties.uiScale);             \
            glVertex2f(miscScaleX1, miscScaleY1);                                                  \
            glVertex2f(miscScaleX2, miscScaleY1);                                                  \
            glVertex2f(miscScaleX2, miscScaleY2);                                                  \
            glVertex2f(miscScaleX1, miscScaleY2);                                                  \
        }



        #define MISC_SCALE_TEXT(text, x, y, scale) {                                               \
            GLfloat miscScaleScale = (scale) * (*cameraProperties.uiScale);                        \
            MapModeController::font.drawFloorStringScale(                                          \
                (text),                                                                            \
                miscScaleBaseX + (x) * (*cameraProperties.uiScale),                                \
                miscScaleBaseY + (y) * (*cameraProperties.uiScale),                                \
                miscScaleScale,                                                                    \
                miscScaleScale                                                                     \
            );                                                                                     \
        }

        #define MISC_SCALE_WIDE_TEXT(text, x, y, scale) {                                          \
            GLfloat miscScaleScale = (scale) * (*cameraProperties.uiScale);                        \
            MapModeController::wideSpacedFont.drawFloorStringScale(                                \
                (text),                                                                            \
                miscScaleBaseX + (x) * (*cameraProperties.uiScale),                                \
                miscScaleBaseY + (y) * (*cameraProperties.uiScale),                                \
                miscScaleScale,                                                                    \
                miscScaleScale                                                                     \
            );                                                                                     \
        }



        #define MISC_SCALE_IN_RECT(x, y, width, height, viewX, viewY)                              \
            (viewX >= (miscScaleX1 = miscScaleBaseX + (x) * (*cameraProperties.uiScale)) &&        \
             viewY >= (miscScaleY1 = miscScaleBaseY + (y) * (*cameraProperties.uiScale)) &&        \
             viewX < miscScaleX1 + (width) * (*cameraProperties.uiScale) &&                        \
             viewY < miscScaleY1 + (height) * (*cameraProperties.uiScale))



        // ============================ Miscellaneous console, string, xml methods ====================

        /**
         * Reads a number from standard input. This returns true on success, and false on failure.
         */
        static bool readNumber(long * number);

        /**
         * Reads a number in the specified range. This can never fail, it prompts the user repeatedly
         * until they comply.
         */
        static long readNumberRange(long minimum, long maximum, bool printMinimum = true, bool printMaximum = true);

        /**
         * Returns a true/false value based on whether the user entered y or n.
         */
        static bool getYN(const std::string & msg);

        /**
         * Allows the user to select one option from a list of options, by printing the options
         * each preceded by a number.
         * The string topMsg is printed above the options, and bottomMsg is printed below the options.
         */
        static size_t selectFromOptions(const std::vector<std::string> options, const std::string & topMsg, const std::string & bottomMsg);

        /**
         * Reads a line from user input, using the specified initial message as a prompt.
         * If the user gives us an empty line, this will print the
         * specified complaint message, and read another line of user input. This will repeat until the user
         * types at least something.
         */
        static std::string readNonEmpty(const std::string & initialMessage, const std::string & complaintMessage);

        /**
         * Converts bool value to string ("true" or "false"), and vice versa. These should be used when saving/loading
         * bool values to XML, or when getting bool values from user text input. When converting string to bool, case
         * sensitivity is ignored, and the following values are considered true: "true", "yes", "1". Everything else
         * is considered false.
         */
        static std::string boolToStr(bool value);
        static bool strToBool(const std::string & str);
        
        /**
         * A constexpr string length function (like strlen).
         */
        constexpr static size_t constStrlen(const char * str) {
            size_t length = 0;
            while(*str != '\0') {
                str++; length++;
            }
            return length;
        }
        
        /**
         * Splits the string by character, returning a vector of parts of the string.
         * Note: for empty strings, this returns a vector containing a single empty string.
         */
        static std::vector<std::string> splitString(const std::string & string, char character);
        
        /**
         * Method to initialise config for shared (server) options (for use by RascConfig::getOrInitBase).
         * Used by main menu (options menu) and other places which need access to shared (server) config.
         */
        static void initSharedOptionsConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem);
        
        // =========================================== Miscellaneous numeric methods ===================================

        /**
         * This converts the specified string to the closest number within the specified range.
         * If something which is not a number is given, then the value within the range which is closest
         * to zero is returned.
         * NOTE: Both min and max are INCLUSIVE.
         */
        static long strtolRange(const std::string & str, long min = LONG_MIN, long max = LONG_MAX);

        /**
         * Templated minmax method. Returns a value between min and max.
         * For minmax, it is assumed that min has a value less than or equal to max.
         */
        template<typename T> static T minmax(T value, T min, T max) {
            if (value < min) return min;
            else if (value > max) return max;
            else return value;
        }

        /**
         * Converts the specified GLfloat value to a string representation,
         * with the specified number of decimal places.
         */
        static std::string formatDecimal(GLfloat value, int decimalPlaces);

        /**
         * Counts the number of digits in an unsigned number.
         */
        static unsigned int digitCount(uint64_t value);
        
        /**
         * Returns random numbers in the specified ranges. This is seeded just
         * from current system time, using a simple random generator,
         * (consecutive calls will return the same value until system time
         * advances!).
         * This is a quick "single fire" random number, for use cases where
         * ONLY a single value is needed and seeding something like a Mersenne
         * twister engine would be overkill and unnecessarily slow.
         * (timeSeededRandom is inclusive range (includes max),
         * timeSeededRandomRange is exclusive range (does NOT include end)).
         */
        static uint64_t timeSeededRandom(uint64_t min, uint64_t max);
        static uint64_t timeSeededRandomRange(uint64_t begin, uint64_t end);
        
        /**
         * Calculates how large value is compared to total, in terms of the specified outOf value.
         * For example, when outOf is specified as 100, it works out percentages.
         * If total is zero, zero is returned for zero value, and positive/negative outOf is returned
         * for positive/negative value. For example:
         * valueOutOfTotalProvstat(2, 8)  = 25%
         * valueOutOfTotalProvstat(-2, 8) = -25%
         * valueOutOfTotalProvstat(1, 10) = 10%
         * valueOutOfTotalProvstat(0, 10) = 0%
         * valueOutOfTotalProvstat(0, 0)  = 0%
         * valueOutOfTotalProvstat(5, 0)  = 100%
         * valueOutOfTotalProvstat(-5, 0) = -100%
         */
        static provstat_t valueOutOfTotalProvstat(provstat_t value, provstat_t total, provstat_t outOf = 100);
        static uint64_t valueOutOfTotalUnsigned(uint64_t value, uint64_t total, uint64_t outOf = 100);


        // =========================================== Miscellaneous UI methods ========================================

        /**
         * This adds the values to the scroll pane, from a separate thread. The values are generated by the specified
         * function pointer, and added to the specified scroll pane.
         */
        static void backgroundScrollpaneAdd(rascUI::Theme * theme, rascUI::TopLevelContainer * topLevel, void * token, threading::ThreadQueue * queue, rascUI::ScrollPane * scrollPane, std::vector<rascUI::ToggleButton> * buttonVector, rascUI::ToggleButtonSeries * series, std::vector<std::string> (*namesFunc)(void), bool selectFirstButton);

        // ======================================= Other utility methods =================================

        /**
         * If running in linux, this attaches a signal hander, and uses
         * std::set_terminate, to print a stack trace upon segmentation
         * fault or terminate.
         */
        static void setupSignalAndTerminateHandlers(void);

        /**
         * Modifies the string so that the first letter is a capital letter.
         * This assumes that the first letter is an alphabetic letter.
         */
        static void makeFirstLetterCapital(std::string & string);

        /**
         * This simple templated method allows the initialisation of a std::array,
         * using C99-style designated initialisers.
         * For example, in C99 I could do this:
         * char arr[3] = {[2]='a', [1]='b', [0]='a'};
         * This method makes the following (admittedly quite clumsy) equivalent thing possible:
         * std::array<char, 3> arr = Misc::designatedInit<char, 3>({{2,'a'}, {1,'b'}, {0,'c'}});
         */
        template<class T, size_t N>
        static std::array<T, N> designatedInit(std::initializer_list<std::pair<size_t, T>> list) {
            std::array<T, N> arr;
            for (const std::pair<size_t, T> & pair : list) {
                arr[pair.first] = pair.second;
            }
            return arr;
        }

        // ====================================== Colour and image methods ===============================

        /**
         * Converts a straight alpha image to a premultiplied alpha image.
         * (For 32 bit RGBA images).
         * The size parameter is the size of the image, in bytes (i.e: width times height times four).
         */
        static void straightToPremultiplied(unsigned char * image, size_t sizeInBytes);

        /**
         * Converts a premultiplied alpha image to a straight alpha image.
         * (For 32 bit RGBA images).
         * The size parameter is the size of the image, in bytes (i.e: width times height times four).
         */
        static void premultipliedToStraight(unsigned char * image, size_t sizeInBytes);

        /**
         * Assuming the two images have premultiplied alphas, this puts
         * the specified image "on top of" the result image, using alpha
         * blending, writing to result.
         * (For 32 bit RGBA images).
         * The size parameter is the size of the image, in bytes (i.e: width times height times four).
         */
        static void alphaOver(unsigned char * result, const unsigned char * image, size_t sizeInBytes);

        /**
         * The same as the above version of straightToPremultiplied, except this version
         * takes the width and height in pixels.
         */
        static inline void straightToPremultiplied(unsigned char * image, size_t width, size_t height) {
            straightToPremultiplied(image, width * height * 4);
        }

        /**
         * The same as the above version of premultipliedToStraight, except this version
         * takes the width and height in pixels.
         */
        static inline void premultipliedToStraight(unsigned char * image, size_t width, size_t height) {
            premultipliedToStraight(image, width * height * 4);
        }

        /**
         * The same as the above version of alphaOver, except this version takes the width
         * and height in pixels.
         */
        static inline void alphaOver(unsigned char * result, const unsigned char * image, size_t width, size_t height) {
            alphaOver(result, image, width * height * 4);
        }
    };
    
    // -------------------------- Utility functions outside Misc --------------------------------
    
    /**
     * These can provide printing functionality, without having to include iostream
     * or cstdio. They are outside any class: so simply provide a forward declare for
     * and use it. This is convenient for basic logging in templated classes.
     */
    void rascBasicPrint(const char * format, ...);
    void rascBasicPrintErr(const char * format, ...);
}

#endif

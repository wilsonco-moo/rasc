/*
 * MacroList.inl
 *
 *  Created on: 19 Feb 2021
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_MACROS_MACROLIST_INL_
#define BASEGAME_COMMON_UTIL_MACROS_MACROLIST_INL_

#include "MacroList.h"

#include "MacroUtil.h"

// -------------------------- List head ----------------------------------------

#define LIST_HEAD_INTERNAL_0(...) \
    "Error: LIST_HEAD: Cannot extract head from empty list."

#define LIST_HEAD_INTERNAL_1(head, ...) \
    head

// -------------------------- List tail ----------------------------------------

#define LIST_TAIL_INTERNAL_0(...) \
    "Error: LIST_TAIL: Cannot extract tail from empty list."

#define LIST_TAIL_INTERNAL_1(head, ...) \
    (__VA_ARGS__)

// ------------------------- List back -----------------------------------------

#define LIST_BACK_INTERNAL_0(list) \
    "Error: LIST_BACK: Cannot extract back from empty list."

#define LIST_BACK_INTERNAL_1(list) \
    LIST_GET(list, LIST_LENGTH(LIST_TAIL(list)))

// ------------------------- List append ---------------------------------------

#define LIST_APPEND_INTERNAL_0(list, value) \
    (value)

#define LIST_APPEND_INTERNAL_1(list, value) \
    LIST_APPEND_INTERNAL_CONCAT(MUTIL_EXPAND_1 list, value)

#define LIST_APPEND_INTERNAL_CONCAT(...) \
    (__VA_ARGS__)

// ------------------------- List prepend --------------------------------------

#define LIST_PREPEND_INTERNAL_0(list, value) \
    (value)

#define LIST_PREPEND_INTERNAL_1(list, value) \
    LIST_APPEND_INTERNAL_CONCAT(value, MUTIL_EXPAND_1 list)

// ----------------------- List concatenate ------------------------------------

#define LIST_CONCAT_INTERNAL_00(list0, list1) ()
#define LIST_CONCAT_INTERNAL_01(list0, list1) list1
#define LIST_CONCAT_INTERNAL_10(list0, list1) list0

#define LIST_CONCAT_INTERNAL_11(list0, list1) \
    (MUTIL_EXPAND_1 list0, MUTIL_EXPAND_1 list1)

// -------------------------- List filled --------------------------------------

// > First, we put the varargs between LIST_FILLED_INTERNAL_FUNC0 and ()(). FUNC0 is a chain of macros where each expands
//   to the next only if there are brackets after it. Doing this results in:
//    - Empty varargs results in precisely "LIST_FILLED_INTERNAL_FUNC2".
//    - Varargs containing only brackets (e.g: "()"), expands past FUNC2 to the meaningless string.
//    - Varargs starting with "()()" expands the same as empty (this is illegal list syntax, so doesn't matter).
//    - Everything else expands to LIST_FILLED_INTERNAL_FUNC0 <some stuff> ()(), which is also a meaningless string.
// > Next, we make sure we do not have more than one argument with MUTIL_FIRST_ARG, and concatenate LIST_FILLED_INTERNAL_EMPTY_
//   to the front of it. An empty varargs here results in exactly LIST_FILLED_INTERNAL_EMPTY_LIST_FILLED_INTERNAL_FUNC2, which
//   expands to TWO arguments. Everything else is a ONE argument meaningless string.
// > This result is then passed through LIST_FILLED_INTERNAL_GET_VALUE, which returns 0 for two arguments, and 1 for one argument.
#define LIST_FILLED_INTERNAL(...)                  \
    LIST_FILLED_INTERNAL_GET_VALUE(                \
     MUTIL_CONCAT_2(LIST_FILLED_INTERNAL_EMPTY_,   \
      MUTIL_FIRST_ARG(                             \
       LIST_FILLED_INTERNAL_FUNC0 __VA_ARGS__ ()() \
    )))

// FUNC0 expands to FUNC1 if there are brackets behind it,
// FUNC1 expands to FUNC2 if there are brackets behind it,
// FUNC2 expands to a meaningless string if there are brackets behind it.
#define LIST_FILLED_INTERNAL_FUNC0(...) \
    LIST_FILLED_INTERNAL_FUNC1

#define LIST_FILLED_INTERNAL_FUNC1(...) \
    LIST_FILLED_INTERNAL_FUNC2

#define LIST_FILLED_INTERNAL_FUNC2(...) \
    meaningless string

// Used for empty lists, expands to two arguments.
#define LIST_FILLED_INTERNAL_EMPTY_LIST_FILLED_INTERNAL_FUNC2 \
    actually empty, two arguments

// Expands to 0 if varargs is two arguments, and 1 if varargs has one argument.
// Thanks to CONCAT and FUNC2, varargs will only contain two arguments if the
// original list was empty.
#define LIST_FILLED_INTERNAL_GET_VALUE(...) \
    LIST_FILLED_INTERNAL_GET_ARG(__VA_ARGS__, 0, 1)

#define LIST_FILLED_INTERNAL_GET_ARG(arg0, arg1, value, ...) value

// ------------------------ List length ----------------------------------------

// Special handling for empty lists.
#define LIST_LENGTH_INTERNAL_0(...) \
    0

// Pass args, then a reverse list of numbers, to the internal choose macro. Since
// the choose macro picks the argument after the first 64 arguments, when varargs
// has one arg it will pick 1 (at the end), two arguments will shift it to 2, etc etc.
#define LIST_LENGTH_INTERNAL_1(...)                                             \
    LIST_LENGTH_INTERNAL_CHOOSE(__VA_ARGS__,                                    \
        64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, \
        46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, \
        28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, \
        10, 9, 8, 7, 6, 5, 4, 3, 2, 1)

// Returns the argument immediately after the first 64 arguments.
#define LIST_LENGTH_INTERNAL_CHOOSE(                                                                                \
    arg0,  arg1,  arg2,  arg3,  arg4,  arg5,  arg6,  arg7,  arg8,  arg9,  arg10, arg11, arg12, arg13, arg14, arg15, \
    arg16, arg17, arg18, arg19, arg20, arg21, arg22, arg23, arg24, arg25, arg26, arg27, arg28, arg29, arg30, arg31, \
    arg32, arg33, arg34, arg35, arg36, arg37, arg38, arg39, arg40, arg41, arg42, arg43, arg44, arg45, arg46, arg47, \
    arg48, arg49, arg50, arg51, arg52, arg53, arg54, arg55, arg56, arg57, arg58, arg59, arg60, arg61, arg62, arg63, \
    value, ...) value

// ------------------------ List pow2 length -----------------------------------

#define LIST_POW2_LENGTH_INTERNAL(...)                                          \
    LIST_LENGTH_INTERNAL_CHOOSE(__VA_ARGS__,                                    \
        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, \
        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 32, 32, 32, 32, \
        32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 16, 16, 16, 16, 16, 16, \
        16, 16, 8, 8, 8, 8, 4, 4, 2, 1)

// ----------------------------- List get --------------------------------------

// Gets the internal list get macro relevant for the specified list and get index.
// This expands to either the error macro, or an internal get macro, depending on whether the list is empty.
#define LIST_GET_INTERNAL_SELECT(list, index) MUTIL_CONCAT_2(LIST_GET_INTERNAL_SELECT_, LIST_FILLED(list))(index)
#define LIST_GET_INTERNAL_SELECT_0(index)     LIST_GET_INTERNAL_ERR
#define LIST_GET_INTERNAL_SELECT_1(index)     LIST_GET_INTERNAL_##index

// Prints an error message for gets which are out of range. This has an internal variant to ensure arguments are
// expanded before generating error string.
#define LIST_GET_INTERNAL_ERR(list, origIndex, origLen) \
    LIST_GET_INTERNAL_ERR_INTERNAL(origIndex, origLen)
#define LIST_GET_INTERNAL_ERR_INTERNAL(origIndex, origLen) \
    MUTIL_STRINGIFY(Error: LIST_GET: Cannot access index origIndex from list of length origLen##.)

// List get internal macros. Each one passes the tail of the list to the previous. By the end, the head of the list
// is the index wanted by the user. The origIndex and origLen arguments are provided purely for error messages.
#define LIST_GET_INTERNAL_0(list, origIndex, origLen) LIST_HEAD(list)
#define LIST_GET_INTERNAL_1(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 0)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_2(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 1)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_3(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 2)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_4(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 3)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_5(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 4)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_6(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 5)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_7(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 6)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_8(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 7)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_9(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 8)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_10(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 9)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_11(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 10)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_12(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 11)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_13(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 12)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_14(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 13)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_15(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 14)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_16(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 15)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_17(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 16)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_18(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 17)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_19(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 18)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_20(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 19)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_21(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 20)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_22(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 21)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_23(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 22)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_24(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 23)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_25(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 24)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_26(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 25)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_27(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 26)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_28(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 27)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_29(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 28)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_30(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 29)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_31(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 30)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_32(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 31)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_33(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 32)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_34(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 33)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_35(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 34)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_36(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 35)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_37(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 36)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_38(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 37)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_39(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 38)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_40(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 39)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_41(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 40)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_42(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 41)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_43(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 42)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_44(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 43)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_45(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 44)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_46(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 45)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_47(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 46)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_48(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 47)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_49(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 48)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_50(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 49)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_51(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 50)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_52(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 51)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_53(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 52)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_54(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 53)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_55(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 54)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_56(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 55)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_57(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 56)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_58(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 57)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_59(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 58)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_60(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 59)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_61(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 60)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_62(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 61)(LIST_TAIL(list), origIndex, origLen)
#define LIST_GET_INTERNAL_63(list, origIndex, origLen) LIST_GET_INTERNAL_SELECT(LIST_TAIL(list), 62)(LIST_TAIL(list), origIndex, origLen)

// ----------------------------- List for each ---------------------------------

// Empty macro for end of list.
#define LIST_FOR_EACH_INTERNAL_0(wrap, func, prev, list)

// Defer concatenation for next macro - that way this requires expanding twice
// to recurse. That is important, as it means the second expansion can happen in
// the caller macro's context, thus getting around the restriction on recursion.
#define LIST_FOR_EACH_INTERNAL_1(wrap, func, prev, list)                    \
    wrap(func, prev, LIST_HEAD(list), LIST_TAIL(list))                      \
    MUTIL_DEFER(MUTIL_CONCAT_2)                                             \
     (LIST_FOR_EACH_INTERNAL_, LIST_FILLED(LIST_TAIL(list)))                \
      (wrap, func, LIST_APPEND(prev, LIST_HEAD(list)), LIST_TAIL(list))

// --------------------------- List for each data ------------------------------

// Empty macro for end of list.
#define LIST_FOR_EACH_DATA_INTERNAL_0(wrap, func, prev, list, ...)

// Defer concatenation for next macro - that way this requires expanding twice
// to recurse. That is important, as it means the second expansion can happen in
// the caller macro's context, thus getting around the restriction on recursion.
#define LIST_FOR_EACH_DATA_INTERNAL_1(wrap, func, prev, list, ...)                   \
    wrap(func, prev, LIST_HEAD(list), LIST_TAIL(list), __VA_ARGS__)                  \
    MUTIL_DEFER(MUTIL_CONCAT_2)                                                      \
     (LIST_FOR_EACH_DATA_INTERNAL_, LIST_FILLED(LIST_TAIL(list)))                    \
      (wrap, func, LIST_APPEND(prev, LIST_HEAD(list)), LIST_TAIL(list), __VA_ARGS__)

// ----------------------------- List for --------------------------------------

// Empty macro for end of list.
#define LIST_FOR_INTERNAL_0(func, list)

// Defer concatenation for next macro - that way this requires expanding twice
// to recurse. That is important, as it means the second expansion can happen in
// the caller macro's context, thus getting around the restriction on recursion.
// Also defer expansion of func, as to not block expansion of LIST_FOR_INTERNAL_1
// within func (allows nesting).
#define LIST_FOR_INTERNAL_1(func, list)                 \
    MUTIL_DEFER(func)(LIST_HEAD(list))                  \
    MUTIL_DEFER(MUTIL_CONCAT_2)                         \
     (LIST_FOR_INTERNAL_, LIST_FILLED(LIST_TAIL(list))) \
      (func, LIST_TAIL(list))

// ----------------------------- List for with data ----------------------------

// Empty macro for end of list.
#define LIST_FOR_DATA_INTERNAL_0(func, list, ...)

// Defer concatenation for next macro - that way this requires expanding twice
// to recurse. That is important, as it means the second expansion can happen in
// the caller macro's context, thus getting around the restriction on recursion.
// Also defer expansion of func, as to not block expansion of LIST_FOR_INTERNAL_1
// within func (allows nesting).
#define LIST_FOR_DATA_INTERNAL_1(func, list, ...)            \
    MUTIL_DEFER(func)(LIST_HEAD(list), __VA_ARGS__)          \
    MUTIL_DEFER(MUTIL_CONCAT_2)                              \
     (LIST_FOR_DATA_INTERNAL_, LIST_FILLED(LIST_TAIL(list))) \
      (func, LIST_TAIL(list), __VA_ARGS__)

// ---------------------------- List wrappers ----------------------------------

// List wrapper next has two internal variants, depending on whether a "next"
// list element exists.
#define LIST_WRAP_NEXT_INTERNAL_0(function, current, after) \
    MUTIL_CONCAT_2(function, _0)(current)

#define LIST_WRAP_NEXT_INTERNAL_1(function, current, after) \
    MUTIL_CONCAT_2(function, _1)(current, LIST_HEAD(after))

#define LIST_WRAP_DATA_NEXT_INTERNAL_0(function, current, after, ...) \
    MUTIL_CONCAT_2(function, _0)(current, __VA_ARGS__)

#define LIST_WRAP_DATA_NEXT_INTERNAL_1(function, current, after, ...) \
    MUTIL_CONCAT_2(function, _1)(current, LIST_HEAD(after), __VA_ARGS__)

#endif

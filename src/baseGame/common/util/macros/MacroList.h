/*
 * MacroList.h
 *
 *  Created on: 19 Feb 2021
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_MACROS_MACROLIST_H_
#define BASEGAME_COMMON_UTIL_MACROS_MACROLIST_H_

#include "MacroList.inl"

/**
 * This header file provides the ability to perform list operations, using macros.
 * 
 * For the purposes of these macros, what is a list?
 * A list starts with opening rounded brackets and ends with matching closing
 * rounded brackets. Within these, is a comma separated sequence of elements.
 * An element is either a list, or some data which does not start with rounded
 * brackets.
 * For example:
 * ()        Is a valid empty list
 * (1, 2)    Is a valid list with two elements
 * (1, ())   A valid list containing two elements, where the second element is an empty list.
 * ()()      Not a valid list or element - there is stuff after the closing rounded brackets.
 * (1, ()    Not a valid list or element - closing rounded brackets are missing.
 * (1, ()()) Not a valid list or element - it contains an invalid element.
 */


// ---------------------------- General list utility -------------------------------

/**
 * Expands to the head of a non empty list - i.e: the first element.
 * For example:
 *   LIST_HEAD((1, 2, 3))    expands to 1
 *   LIST_TAIL((4, 5))       expands to 4
 *   LIST_HEAD((6))          expands to 6
 *   LIST_HEAD(())           expands to "error message"
 *   LIST_HEAD((()))         expands to ()
 */
#define LIST_HEAD(list) \
    MUTIL_CONCAT_2(LIST_HEAD_INTERNAL_, LIST_FILLED(list)) list

/**
 * Expands to the tail of a non empty list - i.e: a list of all elements except
 * the first one.
 * For example:
 *   LIST_TAIL((1, 2, 3))    expands to (2, 3)
 *   LIST_TAIL((4, 5))       expands to (5)
 *   LIST_TAIL((6))          expands to ()
 *   LIST_TAIL(())           expands to "error message"
 *   LIST_TAIL((()))         expands to ()
 */
#define LIST_TAIL(list) \
    MUTIL_CONCAT_2(LIST_TAIL_INTERNAL_, LIST_FILLED(list)) list

/**
 * Expands to the back of a non empty list - i.e: the last element.
 * WARNING: Size is limited.
 * For example:
 *   LIST_BACK((1, 2, 3))    expands to 3
 *   LIST_BACK((4, 5))       expands to 5
 *   LIST_BACK((6))          expands to 6
 *   LIST_BACK(())           expands to "error message"
 *   LIST_BACK((()))         expands to ()
 */
#define LIST_BACK(list) \
    MUTIL_CONCAT_2(LIST_BACK_INTERNAL_, LIST_FILLED(list))(list)

/**
 * Appends a single value to the end of the list.
 * For example:
 *   LIST_APPEND((), 1)        expands to (1)
 *   LIST_APPEND((2, 3, 4), 5) expands to (2, 3, 4, 5)
 *   LIST_APPEND((), ())       expands to (())
 */
#define LIST_APPEND(list, value) \
    MUTIL_CONCAT_2(LIST_APPEND_INTERNAL_, LIST_FILLED(list))(list, value)

/**
 * Prepends a single value to the start of the list.
 * For example:
 *   LIST_APPEND((), 1)        expands to (1)
 *   LIST_APPEND((2, 3, 4), 5) expands to (5, 2, 3, 4)
 *   LIST_APPEND((), ())       expands to (())
 */
#define LIST_PREPEND(list, value) \
    MUTIL_CONCAT_2(LIST_PREPEND_INTERNAL_, LIST_FILLED(list))(list, value)

/**
 * Concatenates two lists together.
 * For example:
 *   LIST_CONCAT((), ())                  expands to ()
 *   LIST_CONCAT((), (1, 2, 3))           expands to (1, 2, 3)
 *   LIST_CONCAT((4, 5, 6), ())           expands to (4, 5, 6)
 *   LIST_CONCAT((7, 8, 9), (10, 11, 12)) expands to (7, 8, 9, 10, 11, 12)
 */
#define LIST_CONCAT(list0, list1) \
    MUTIL_CONCAT_3(               \
        LIST_CONCAT_INTERNAL_,    \
        LIST_FILLED(list0),       \
        LIST_FILLED(list1)        \
    )(list0, list1)

/**
 * Expands to 1 for lists with more than one element,
 * expands to 0 for lists which are empty.
 * A useful trick is concatenating the result of this macro to another, to
 * provide different handling for empty lists (which is often required). Most
 * list macros do this internally.
 * For example:
 *   LIST_FILLED(())          expands to 0
 *   LIST_FILLED((1))         expands to 1
 *   LIST_FILLED((1, 2, 3))   expands to 1
 *   LIST_FILLED((()))        expands to 1
 *   LIST_FILLED(((), 2))     expands to 1
 */
#define LIST_FILLED(list) \
    LIST_FILLED_INTERNAL list

/**
 * Expands to a number representing the length of the list.
 * WARNING: Size is limited.
 * For example:
 *   LIST_LENGTH(())          expands to 0
 *   LIST_LENGTH((1))         expands to 1
 *   LIST_LENGTH((2, 3))      expands to 2
 *   LIST_LENGTH((4, 5, 6))   expands to 3
 */
#define LIST_LENGTH(list) \
    MUTIL_CONCAT_2(LIST_LENGTH_INTERNAL_, LIST_FILLED(list)) list

/**
 * Expands to a the smallest power of two number, which is greater than or
 * equal to the length of the list. This is useful when doing recursion using
 * deferred expansion, using MUTIL_EXPAND__*. Note that empty lists expand to
 * 1 here, since 0 is not a power of two.
 * WARNING: Size is limited.
 * For example:
 *   LIST_LENGTH(())          expands to 1
 *   LIST_LENGTH((1))         expands to 1
 *   LIST_LENGTH((2, 3))      expands to 2
 *   LIST_LENGTH((4, 5, 6))   expands to 4
 */
#define LIST_POW2_LENGTH(list) \
    LIST_POW2_LENGTH_INTERNAL list

/**
 * Expands to the specified list index.
 * WARNING: Size is limited.
 * For example:
 *   LIST_GET((1, 2, 3), 0)  expands to 1
 *   LIST_GET((1, 2, 3), 1)  expands to 2
 *   LIST_GET((1, 2, 3), 2)  expands to 3
 *   LIST_GET((1, 2, 3), 3)  expands to "error message"
 */
#define LIST_GET(list, index) \
    LIST_GET_INTERNAL_SELECT(list, index)(list, index, LIST_LENGTH(list))


// ---------------------------- List for each macros -------------------------------

/**
 * Runs wrapper function and function for each list index. For each index, the
 * wrapper function is provided with a list of items before the current, the
 * current item, and a list of items after the current.
 * WARNING: Size is limited.
 * For example:
 *   LIST_FOR_EACH(wrap, func, (1, 2, 3, 4))
 * expands to:
 *   wrap(func, (), 1, (2, 3, 4))
 *   wrap(func, (1), 2, (3, 4))
 *   wrap(func, (1, 2), 3, (4))
 *   wrap(func, (1, 2, 3), 4, ())
 * 
 * A wrapper function extracts only the arguments wanted by the function. Some
 * pre-defined wrapper functions are given below, although creating custom
 * wrappers is easy.
 */
#define LIST_FOR_EACH(wrapperFunction, function, list)           \
    MUTIL_CONCAT_2(MUTIL_EXPAND_, LIST_POW2_LENGTH(list))(       \
     MUTIL_CONCAT_2(LIST_FOR_EACH_INTERNAL_, LIST_FILLED(list))( \
      wrapperFunction, function, (), list                        \
    ))

/**
 * Works the same as LIST_FOR_EACH, although also allows additional data to be
 * passed to the list function.
 * WARNING: Size is limited.
 * For example:
 *   LIST_FOR_EACH_DATA(wrap, func, (1, 2, 3, 4), data0, data1)
 * expands to:
 *   wrap(func, (), 1, (2, 3, 4), data0, data1)
 *   wrap(func, (1), 2, (3, 4), data0, data1)
 *   wrap(func, (1, 2), 3, (4), data0, data1)
 *   wrap(func, (1, 2, 3), 4, (), data0, data1)
 * 
 * A wrapper function extracts only the arguments wanted by the function. Some
 * pre-defined wrapper functions are given below, although creating custom
 * wrappers is easy.
 */
#define LIST_FOR_EACH_DATA(wrapperFunction, function, list, ...)      \
    MUTIL_CONCAT_2(MUTIL_EXPAND_, LIST_POW2_LENGTH(list))(            \
     MUTIL_CONCAT_2(LIST_FOR_EACH_DATA_INTERNAL_, LIST_FILLED(list))( \
      wrapperFunction, function, (), list, __VA_ARGS__                \
    ))

/**
 * List for each without wrapper - acts exactly the same as list for each with basic list wrapper.
 * WARNING: Size is limited.
 * For example:
 *   LIST_FOR(func, (1, 2, 3))
 * expands to:
 *   func(1) func(2) func(3)
 */
#define LIST_FOR(function, list)                            \
    MUTIL_CONCAT_2(MUTIL_EXPAND_, LIST_POW2_LENGTH(list))(  \
     MUTIL_CONCAT_2(LIST_FOR_INTERNAL_, LIST_FILLED(list))( \
      function, list                                        \
    ))

/**
 * List for each with data without wrapper.
 * WARNING: Size is limited.
 * For example:
 *   LIST_FOR_DATA(func, (1, 2, 3), data0, data1)
 * expands to:
 *   func(1, data0, data1) func(2, data0, data1) func(3, data0, data1)
 */
#define LIST_FOR_DATA(function, list, ...)                       \
    MUTIL_CONCAT_2(MUTIL_EXPAND_, LIST_POW2_LENGTH(list))(       \
     MUTIL_CONCAT_2(LIST_FOR_DATA_INTERNAL_, LIST_FILLED(list))( \
      function, list, __VA_ARGS__                                \
    ))


// --------------------------- Nested list for each macros -------------------------

/**
 * Nested iteration is tricky, but the LIST_FOR_NESTED_* make it possible.
 * 
 * Because a macro is blocked from expanding within itself, the same for-each macro cannot be used for
 * both outer and inner loops. To get around this, multiple variants of LIST_FOR_NESTED_* are provided.
 * 
 * This also rules out using wrapper functions, as the inner loop wouldn't be able to use the same wrapper
 * function as the outer loop. This could be solved by creating many variants of every wrapper function,
 * although that would be confusing, so these nested for loops simply don't allow wrapper functions.
 * 
 * Expansion is also a problem: MUTIL_EXPAND_* macros in the outer loop would block expansion of
 * the MUTIL_EXPAND_* macros in the inner loop. This can be partly solved by removing expansion from the
 * inner loop, although that leads to another issue: Nesting loops requires an additional expansion
 * for each time the loops are nested, i.e: if you nest a loop within an iteration of 4 items, you
 * need to expand 5 times. To get around this, use the nested for macros, which do not include any
 * expansion. Then, surround the entire thing in LIST_NESTED_EXPAND_*, specifying the number of nested
 * loops within the outer loop. LIST_NESTED_EXPAND_N expands the same as a regular for each loop, plus
 * N additional expansions. The longest list iterated in the chain should be passed to LIST_NESTED_EXPAND_*.
 * WARNING: Size is limited.
 * For example:
 *   #define LIST ((a, b), (c, d))
 *   #define INNER_LOOP(innerList) LIST_FOR_NESTED_1(func, innerList)
 *   LIST_NESTED_EXPAND_1(LIST, LIST_FOR_NESTED_0(INNER_LOOP, LIST))
 * expands to:
 *   func (a) func (b) func (c) func (d)
 */
#define LIST_FOR_NESTED_0(function, list) MUTIL_CONCAT_2(LIST_FOR_INTERNAL_, LIST_FILLED(list))(function, list)
#define LIST_FOR_NESTED_1(function, list) MUTIL_CONCAT_2(LIST_FOR_INTERNAL_, LIST_FILLED(list))(function, list)
#define LIST_FOR_NESTED_2(function, list) MUTIL_CONCAT_2(LIST_FOR_INTERNAL_, LIST_FILLED(list))(function, list)
#define LIST_FOR_NESTED_3(function, list) MUTIL_CONCAT_2(LIST_FOR_INTERNAL_, LIST_FILLED(list))(function, list)
#define LIST_FOR_NESTED_4(function, list) MUTIL_CONCAT_2(LIST_FOR_INTERNAL_, LIST_FILLED(list))(function, list)
#define LIST_FOR_NESTED_5(function, list) MUTIL_CONCAT_2(LIST_FOR_INTERNAL_, LIST_FILLED(list))(function, list)
#define LIST_FOR_NESTED_6(function, list) MUTIL_CONCAT_2(LIST_FOR_INTERNAL_, LIST_FILLED(list))(function, list)
#define LIST_FOR_NESTED_7(function, list) MUTIL_CONCAT_2(LIST_FOR_INTERNAL_, LIST_FILLED(list))(function, list)

/**
 * Nested iteration with data, see documentation above. Passing data is especially
 * useful for nested iteration, as it allows passing in a separate inner list to iterate.
 * This is shown in the following example. Note that the inner loop doesn't have to also
 * take data, that is done for illustrative purposes to show how the same data for macro
 * can be nested.
 * WARNING: Size is limited.
 * For example:
 *   #define OUTER_LIST (outer0, outer1, outer2)
 *   #define INNER_LIST (inner0, inner1)
 *   #define INNER_FUNC(innerElem, separator) innerElem separator
 *   #define OUTER_FUNC(outerElem, innerList) outerElem: {LIST_FOR_DATA_NESTED_1(INNER_FUNC, innerList, |)}
 *   LIST_NESTED_EXPAND_1(OUTER_LIST, LIST_FOR_DATA_NESTED_0(OUTER_FUNC, OUTER_LIST, INNER_LIST))
 * Expands to:
 *   outer0: {inner0| inner1| } outer1: {inner0| inner1| } outer2: {inner0| inner1| }
 */
#define LIST_FOR_DATA_NESTED_0(function, list, ...) MUTIL_CONCAT_2(LIST_FOR_DATA_INTERNAL_, LIST_FILLED(list))(function, list, __VA_ARGS__)
#define LIST_FOR_DATA_NESTED_1(function, list, ...) MUTIL_CONCAT_2(LIST_FOR_DATA_INTERNAL_, LIST_FILLED(list))(function, list, __VA_ARGS__)
#define LIST_FOR_DATA_NESTED_2(function, list, ...) MUTIL_CONCAT_2(LIST_FOR_DATA_INTERNAL_, LIST_FILLED(list))(function, list, __VA_ARGS__)
#define LIST_FOR_DATA_NESTED_3(function, list, ...) MUTIL_CONCAT_2(LIST_FOR_DATA_INTERNAL_, LIST_FILLED(list))(function, list, __VA_ARGS__)
#define LIST_FOR_DATA_NESTED_4(function, list, ...) MUTIL_CONCAT_2(LIST_FOR_DATA_INTERNAL_, LIST_FILLED(list))(function, list, __VA_ARGS__)
#define LIST_FOR_DATA_NESTED_5(function, list, ...) MUTIL_CONCAT_2(LIST_FOR_DATA_INTERNAL_, LIST_FILLED(list))(function, list, __VA_ARGS__)
#define LIST_FOR_DATA_NESTED_6(function, list, ...) MUTIL_CONCAT_2(LIST_FOR_DATA_INTERNAL_, LIST_FILLED(list))(function, list, __VA_ARGS__)
#define LIST_FOR_DATA_NESTED_7(function, list, ...) MUTIL_CONCAT_2(LIST_FOR_DATA_INTERNAL_, LIST_FILLED(list))(function, list, __VA_ARGS__)

/**
 * Expansion to go around the outside of nested iteration. LIST_NESTED_EXPAND_N expands the same
 * number of times as a regular for macro would, plus N additional times (allowing N additional
 * levels of nesting). See documentation above.
 * WARNING: Size is limited.
 * Examples are shown above.
 */
#define LIST_NESTED_EXPAND_0(list, ...) MUTIL_CONCAT_2(MUTIL_EXPAND_, LIST_POW2_LENGTH(LIST_CONCAT(list, ())))(__VA_ARGS__)
#define LIST_NESTED_EXPAND_1(list, ...) MUTIL_CONCAT_2(MUTIL_EXPAND_, LIST_POW2_LENGTH(LIST_CONCAT(list, (a))))(__VA_ARGS__)
#define LIST_NESTED_EXPAND_2(list, ...) MUTIL_CONCAT_2(MUTIL_EXPAND_, LIST_POW2_LENGTH(LIST_CONCAT(list, (a,b))))(__VA_ARGS__)
#define LIST_NESTED_EXPAND_3(list, ...) MUTIL_CONCAT_2(MUTIL_EXPAND_, LIST_POW2_LENGTH(LIST_CONCAT(list, (a,b,c))))(__VA_ARGS__)
#define LIST_NESTED_EXPAND_4(list, ...) MUTIL_CONCAT_2(MUTIL_EXPAND_, LIST_POW2_LENGTH(LIST_CONCAT(list, (a,b,c,d))))(__VA_ARGS__)
#define LIST_NESTED_EXPAND_5(list, ...) MUTIL_CONCAT_2(MUTIL_EXPAND_, LIST_POW2_LENGTH(LIST_CONCAT(list, (a,b,c,d,e))))(__VA_ARGS__)
#define LIST_NESTED_EXPAND_6(list, ...) MUTIL_CONCAT_2(MUTIL_EXPAND_, LIST_POW2_LENGTH(LIST_CONCAT(list, (a,b,c,d,e,f))))(__VA_ARGS__)
#define LIST_NESTED_EXPAND_7(list, ...) MUTIL_CONCAT_2(MUTIL_EXPAND_, LIST_POW2_LENGTH(LIST_CONCAT(list, (a,b,c,d,e,f,g))))(__VA_ARGS__)


// ------------------------------ List wrappers ------------------------------------

/**
 * Passes just the current list element through to the function.
 * For example:
 *   LIST_FOR_EACH(LIST_WRAP_BASIC, func, (1, 2, 3, 4))
 * expands to:
 *   func(1) func(2) func(3) func(4)
 */
#define LIST_WRAP_BASIC(function, before, current, after) \
    function(current)

/**
 * Passes numeric list indices along with each element.
 * For example:
 *   LIST_FOR_EACH(LIST_WRAP_INDEX, func, (zero, one, two, three))
 * expands to:
 *   func(0, zero) func(1, one) func(2, two) func(3, three)
 */
#define LIST_WRAP_INDEX(function, before, current, after) \
    function(LIST_LENGTH(before), current)

/**
 * When a next list element exists, this runs function_1 with current and next.
 * Otherwise, function_0 is run just with the current element.
 * For example:
 *   LIST_FOR_EACH(LIST_WRAP_NEXT, func, (1, 2, 3))
 * expands to:
 *   func_1(1, 2) func_1(2, 3) func_0(3)
 */
#define LIST_WRAP_NEXT(function, before, current, after) \
    MUTIL_CONCAT_2(LIST_WRAP_NEXT_INTERNAL_, LIST_FILLED(after))(function, current, after)

/**
 * Simply passes all arguments (before, current and after) through to the funcion.
 * For example:
 *   LIST_FOR_EACH(LIST_WRAP_ALL, func, (1, 2, 3))
 * expands to:
 *   func((), 1, (2, 3)) func((1), 2, (3)) func((1, 2), 3, ())
 */
#define LIST_WRAP_ALL(function, before, current, after) \
    function(before, current, after)


// -------------------------- List with data wrappers ------------------------------

/**
 * Passes just the current list element through to the function.
 * For example:
 *   LIST_FOR_EACH_DATA(LIST_WRAP_DATA_BASIC, func, (1, 2, 3, 4), data)
 * expands to:
 *   func(1, data) func(2, data) func(3, data) func(4, data)
 */
#define LIST_WRAP_DATA_BASIC(function, before, current, after, ...) \
    function(current, __VA_ARGS__)

/**
 * Passes numeric list indices along with each element.
 * For example:
 *   LIST_FOR_EACH_DATA(LIST_WRAP_DATA_INDEX, func, (zero, one, two, three), data)
 * expands to:
 *   func(0, zero, data) func(1, one, data) func(2, two, data) func(3, three, data)
 */
#define LIST_WRAP_DATA_INDEX(function, before, current, after, ...) \
    function(LIST_LENGTH(before), current, __VA_ARGS__)

/**
 * When a next list element exists, this runs function_1 with current and next.
 * Otherwise, function_0 is run just with the current element.
 * For example:
 *   LIST_FOR_EACH_DATA(LIST_WRAP_DATA_NEXT, func, (1, 2, 3), data)
 * expands to:
 *   func_1(1, 2, data) func_1(2, 3, data) func_0(3, data)
 */
#define LIST_WRAP_DATA_NEXT(function, before, current, after, ...) \
    MUTIL_CONCAT_2(LIST_WRAP_DATA_NEXT_INTERNAL_, LIST_FILLED(after))(function, current, after, __VA_ARGS__)

/**
 * Simply passes all arguments (before, current and after) through to the funcion.
 * For example:
 *   LIST_FOR_EACH_DATA(LIST_WRAP_DATA_ALL, func, (1, 2, 3), data)
 * expands to:
 *   func((), 1, (2, 3), data) func((1), 2, (3), data) func((1, 2), 3, (), data)
 */
#define LIST_WRAP_DATA_ALL(function, before, current, after, ...) \
    function(before, current, after, __VA_ARGS__)

#endif

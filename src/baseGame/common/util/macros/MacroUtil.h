/*
 * MacroUtil.h
 *
 *  Created on: 28 Jun 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_MACROS_MACROUTIL_H_
#define BASEGAME_COMMON_UTIL_MACROS_MACROUTIL_H_

/**
 * This header file contains (and provides somewhere to put
 * in the future) general purpose utility macros. These are
 * macros which make other macro-based stuff more convenient,
 * such as deferring and forcing macro expansion.
 */

/**
 * An empty macro. This is helpful for providing a placeholder.
 */
#define MUTIL_EMPTY

/**
 * An function macro which ignores its single parameter. This
 * is (again), helpful for providing a placeholder.
 */
#define MUTIL_IGNORE(x)

/**
 * This allows deferring the expansion of a function-based macro. This
 * is helpful in situations where the function based macro cannot be
 * expanded until the next "level up" without being invalid, such
 * as when concatenating "halves" of function based macro calls
 * like "X(1," and "2)".
 * For example:
 * X(1)                Will result in the macro X being expanded with the parameter 1.
 * MUTIL_DEFER(X)(1)   Will expand into "X (1)", which requires being expanded again
 *                     to result in the macro X being expanded.
 */
#define MUTIL_DEFER(x) x MUTIL_EMPTY

/**
 * A function macro which expands to its arguments. This
 * is helpful for forcing an additional level of expansion to
 * a macro (such as when using MUTIL_DEFER).
 */
#define MUTIL_EXPAND_1(...) __VA_ARGS__

/**
 * Expand macros which expand the argument twice, four times, etc.
 * This makes it possible to implement simple recursion (upto a limit) using
 * a chain of MUTIL_DEFERs, without defining a macro for every single index.
 */
#define MUTIL_EXPAND_2(...)  MUTIL_EXPAND_1(MUTIL_EXPAND_1(__VA_ARGS__))
#define MUTIL_EXPAND_4(...)  MUTIL_EXPAND_2(MUTIL_EXPAND_2(__VA_ARGS__))
#define MUTIL_EXPAND_8(...)  MUTIL_EXPAND_4(MUTIL_EXPAND_4(__VA_ARGS__))
#define MUTIL_EXPAND_16(...) MUTIL_EXPAND_8(MUTIL_EXPAND_8(__VA_ARGS__))
#define MUTIL_EXPAND_32(...) MUTIL_EXPAND_16(MUTIL_EXPAND_16(__VA_ARGS__))
#define MUTIL_EXPAND_64(...) MUTIL_EXPAND_32(MUTIL_EXPAND_32(__VA_ARGS__))

/**
 * Fully expands the argument, then converts it into a string literal.
 */
#define MUTIL_STRINGIFY(x) \
    MUTIL_STRINGIFY_INTERNAL(x)
#define MUTIL_STRINGIFY_INTERNAL(x) \
    #x

/**
 * Fully expands arguments, then concatenates together the results.
 * There is a variant for two, three and four arguments.
 */
#define MUTIL_CONCAT_2(arg0, arg1)                   MUTIL_CONCAT_2_INTERNAL(arg0, arg1)
#define MUTIL_CONCAT_3(arg0, arg1, arg2)             MUTIL_CONCAT_3_INTERNAL(arg0, arg1, arg2)
#define MUTIL_CONCAT_4(arg0, arg1, arg2, arg3)       MUTIL_CONCAT_4_INTERNAL(arg0, arg1, arg2, arg3)
#define MUTIL_CONCAT_5(arg0, arg1, arg2, arg3, arg4) MUTIL_CONCAT_5_INTERNAL(arg0, arg1, arg2, arg3, arg4)

#define MUTIL_CONCAT_2_INTERNAL(arg0, arg1)                   arg0##arg1
#define MUTIL_CONCAT_3_INTERNAL(arg0, arg1, arg2)             arg0##arg1##arg2
#define MUTIL_CONCAT_4_INTERNAL(arg0, arg1, arg2, arg3)       arg0##arg1##arg2##arg3
#define MUTIL_CONCAT_5_INTERNAL(arg0, arg1, arg2, arg3, arg4) arg0##arg1##arg2##arg3##arg4

/**
 * Returns the first argument in a set of varargs.
 */
#define MUTIL_FIRST_ARG(x, ...) x

#endif

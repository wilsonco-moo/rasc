/*
 * FixedVector.cpp
 *
 *  Created on: 6 Aug 2023
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_STATICSTORAGE_FIXEDVECTOR_INL_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_STATICSTORAGE_FIXEDVECTOR_INL_

#include "FixedVector.h"

namespace rasc {

    template<typename Type, size_t FixedSize>
    constexpr FixedVector<Type, FixedSize>::FixedVector(void) :
        data(),
        elementCount(0) {
    }

    template<typename Type, size_t FixedSize>
    constexpr FixedVector<Type, FixedSize>::FixedVector(std::initializer_list<Type> list) :
        data(),
        elementCount(list.size()) {

        size_t index = 0;
        for (const Type & element : list) {
            data[index] = element;
            index++;
        }
    }

    template<typename Type, size_t FixedSize>
    constexpr Type & FixedVector<Type, FixedSize>::operator[] (size_t index) {
       return data[index];
    }

    template<typename Type, size_t FixedSize>
    constexpr const Type & FixedVector<Type, FixedSize>::operator[] (size_t index) const {
       return data[index];
    }

    template<typename Type, size_t FixedSize>
    constexpr size_t FixedVector<Type, FixedSize>::size(void) const {
        return elementCount;
    }
    
    template<typename Type, size_t FixedSize>
    constexpr size_t FixedVector<Type, FixedSize>::max_size(void) const {
        return FixedSize;
    }
    
    template<typename Type, size_t FixedSize>
    constexpr bool FixedVector<Type, FixedSize>::empty(void) const {
        return elementCount == 0;
    }

    template<typename Type, size_t FixedSize>
    constexpr void FixedVector<Type, FixedSize>::push_back(const Type & element) {
        data[elementCount++] = element;
    }

    template<typename Type, size_t FixedSize>
    constexpr typename FixedVector<Type, FixedSize>::iterator FixedVector<Type, FixedSize>::begin(void) {
        return data.begin();
    }

    template<typename Type, size_t FixedSize>
    constexpr typename FixedVector<Type, FixedSize>::const_iterator FixedVector<Type, FixedSize>::begin(void) const {
        return data.begin();
    }

    template<typename Type, size_t FixedSize>
    constexpr typename FixedVector<Type, FixedSize>::iterator FixedVector<Type, FixedSize>::end(void) {
        return data.begin() + elementCount;
    }

    template<typename Type, size_t FixedSize>
    constexpr typename FixedVector<Type, FixedSize>::const_iterator FixedVector<Type, FixedSize>::end(void) const {
        return data.begin() + elementCount;
    }
}

#endif

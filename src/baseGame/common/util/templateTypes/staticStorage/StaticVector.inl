/*
 * StaticVector.cpp
 *
 *  Created on: 6 Aug 2023
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_STATICSTORAGE_STATICVECTOR_INL_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_STATICSTORAGE_STATICVECTOR_INL_

#include "StaticVector.h"

namespace rasc {

    template<typename Type>
    constexpr StaticVector<Type>::StaticVector(void) :
        data(NULL),
        elementCount(0) {
    }

    template<typename Type>
    constexpr StaticVector<Type>::StaticVector(const Type * data, size_t elementCount) :
        data(data),
        elementCount(elementCount) {
    }

    template<typename Type>
    constexpr const Type & StaticVector<Type>::operator[] (size_t index) const {
       return data[index];
    }

    template<typename Type>
    constexpr size_t StaticVector<Type>::size(void) const {
        return elementCount;
    }

    template<typename Type>
    constexpr bool StaticVector<Type>::empty(void) const {
        return elementCount == 0;
    }
    
    template<typename Type>
    constexpr typename StaticVector<Type>::const_iterator StaticVector<Type>::begin(void) const {
        return data;
    }

    template<typename Type>
    constexpr typename StaticVector<Type>::const_iterator StaticVector<Type>::end(void) const {
        return data + elementCount;
    }
}

#endif

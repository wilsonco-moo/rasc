/*
 * StaticStorageHelpers.h
 *
 *  Created on: 6 Aug 2023
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_STATICSTORAGE_STATICSTORAGEHELPERS_H_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_STATICSTORAGE_STATICSTORAGEHELPERS_H_

#include <cstddef>
#include <array>

#include "FixedVector.h"
#include "StaticVector.h"

namespace rasc {
    
    /**
     * StaticStorageHelpers provides a way to constexpr-initialise
     * an arbitrary number of arbitrarily sized vectors, as if they're
     * dynamically allocated at runtime.
     * 
     * Consider the case of some data-storing structure, which contains
     * vectors of data. Vectors aren't great for static storage, as
     * they create wasteful allocations at runtime (which could "easily"
     * be static).
     * 
     * struct Data {
     *     std::vector<int> thing0;
     *     std::vector<SomeStruct> thing1;
     * };
     * 
     * If you only had one of these, the situation is easy: Those vectors
     * can be swapped for std::arrays to make initialisation constexpr.
     * 
     * But if you have an array of lots of your Data struct, that can't be
     * done as these all need different sizes. Storing a fixed size vector
     * (like FixedVector) for all of them, just to provide constexpr
     * initialisation would be very wasteful.
     * 
     * StaticStorageHelpers solves this case:
     * It allows compile-time "packing" of the data in these vectors, into
     * static buffers and StaticVectors.
     * 
     * To set this up:
     *  - First that Data struct needs to be able to *either* use FixedVector or
     *    StaticVector. It is easiest to do this by templating it (so you can
     *    have either Data<FixedVector> or Data<SizedStaticVector>).
     *    SizedStaticVector (instead of StaticVector) can be used, as it accepts
     *    a dummy size parameter - so has identical template params to FixedVector.
     *    This makes it possible for the Data struct to use a template template.
     *  - Secondly, you need to provide a "query" function, for StaticStorageHelpers
     *    to be able to know about which arrays of what type your data struct
     *    contains. Note that a const version of this isn't needed: Internally
     *    StaticStorageHelpers does a whole load of copying to make this possible.
     *    But thats only a compile-time cost.
     * 
     * Overall, your Data struct will look like this:
     * 
     * // template-template type for the vector type (both FixedVector
     * // and SizedStaticVector accept identical template parameter lists).
     * template<template<typename, size_t> class VectorType>
     * struct Data {
     *     // Fixed size of 100 for FixedVector, can be as large as needed.
     *     VectorType<int, 100> thing0;
     *     VectorType<SomeStruct, 100> thing1;
     * 
     *     // Query function to expose both vectors to StaticStorageHelpers.
     *     template<typename Query>
     *     constexpr void query(Query & query) {
     *         query.run(thing0);
     *         query.run(thing1);
     *     }
     * };
     * 
     * Next, static data must all be set up within a constexpr method, which
     * must return an array of Data<FixedVector>.
     * 
     * // Fixed number of Data instances known at compile time.
     * constexpr static size_t COUNT = 2;
     * 
     * constexpr static std::array<Data<FixedVector>, COUNT> getData(void) {
     *     std::array<Data<FixedVector>, COUNT> array;
     * 
     *     // data initialisation code, whatever is needed.
     *     array[0].thing0 = { 1, 2, 3, 4 };
     *     array[1].thing0 = { 2, 3, 4 };
     *     ...
     * 
     *     return array;
     * }
     * 
     * Now, buffers to store this static data can be defined with these macros:
     * This must be done for each type where an array exists containing it.
     * 
     * STATIC_STORAGE_DEFINE(int, intData, getData())
     * STATIC_STORAGE_DEFINE(SomeStruct, someStructData, getData())
     * 
     * And finally, the StaticVector versions of the Data can be generated,
     * pointing to the data in the buffers defined above.
     * 
     * constexpr std::array<Data<SizedStaticVector>, COUNT> getStaticData(void) {
     *     std::array<Data<SizedStaticVector>, COUNT> staticData;
     *     
     *     // Again, populate must be done for each used array type.
     *     STATIC_STORAGE_POPULATE(int, staticData, intData, getData())
     *     STATIC_STORAGE_POPULATE(SomeStruct, staticData, someStructData, getData())
     *
     *     // Additional logic can be put here to copy any non-vector
     *     // data from getData() to staticData.
     *     return staticData;
     * }
     * 
     * constexpr static std::array<Data<SizedStaticVector>, COUNT> staticData = getStaticData();
     * 
     * That last staticData array contains all the "packed" data.
     */
    class StaticStorageHelpers {
    private:
        // Query structure to get total size across all arrays of specified search type.
        // Used internally within getTotalSize.
        template<typename SearchType>
        struct SizeQuery {
            size_t totalSize;
            
            constexpr SizeQuery(void);
            
            template<typename Type, size_t FixedSize>
            constexpr void run(const FixedVector<Type, FixedSize> & fixedVector);
        };
        
        // Query structure to get count of all arrays of specified search type.
        // Used internally within getTotalCount.
        template<typename SearchType>
        struct CountQuery {
            size_t totalCount;
            
            constexpr CountQuery(void);
            
            template<typename Type, size_t FixedSize>
            constexpr void run(const FixedVector<Type, FixedSize> & fixedVector);
        };
        
        // Query structure to copy data in to the specified static storage buffer,
        // for all arrays of specified search type.
        // Used internally within StaticStorage.
        template<typename SearchType>
        struct CopyToStaticQuery {
            SearchType * bufferUpto;
            
            constexpr CopyToStaticQuery(SearchType * buffer);
            
            template<typename Type, size_t FixedSize>
            constexpr void run(const FixedVector<Type, FixedSize> & fixedVector);
        };
        
        // Query structure to record the size of all arrays of specified search type.
        // Used internally within PopulateStaticQuery.
        template<typename SearchType, size_t ArrayCount>
        struct AllSizesQuery {
            std::array<size_t, ArrayCount> allSizes;
            size_t upto;

            constexpr AllSizesQuery(void);

            template<typename Type, size_t FixedSize>
            constexpr void run(const FixedVector<Type, FixedSize> & fixedVector);
        };
        
        // Query structure to populate all static vectors with appropriate addresses,
        // for all arrays of specified search type.
        // This first requires a pointer to the buffer to use, and the fixed data
        // (so we can work out the offsets to use for each array).
        // Used internally within populateStaticVectors.
        template<typename SearchType, size_t ArrayCount>
        struct PopulateStaticQuery {
            AllSizesQuery<SearchType, ArrayCount> allSizesQuery;
            const SearchType * bufferUpto;
            size_t arrayUpto;
            
            template<typename StorageType, size_t ArraySize>
            constexpr PopulateStaticQuery(const SearchType * buffer, std::array<StorageType, ArraySize> fixedData);
            
            template<typename Type>
            constexpr void run(StaticVector<Type> & staticVector);
        };
        
    public:
        /**
         * Gets the total cumulative size of all query-able arrays of
         * specified type, across all members of the provided data.
         */
        template<typename SearchType, typename StorageType, size_t ArraySize>
        static constexpr size_t getTotalSize(std::array<StorageType, ArraySize> data);
        
        /**
         * Gets the total count (number of arrays) of all query-able arrays of
         * specified type, across all members of the provided data.
         */
        template<typename SearchType, typename StorageType, size_t ArraySize>
        static constexpr size_t getTotalCount(std::array<StorageType, ArraySize> data);
        
        /**
         * A buffer of specified type and total size.
         * 
         * Our constructor copies and stores in our buffer, all the values of
         * all the query-able arrays of specified type, from the data provided
         * in the constructor, to this buffer.
         * 
         * It is expected that our TotalSize matches getTotalSize<SearchType>(data),
         * and if not an exception is thrown in our (constexpr) constructor (which
         * fails compilation).
         * The macro STATIC_STORAGE_DEFINE automates definition of a StaticStorage.
         */
        template<typename SearchType, size_t TotalSize>
        struct StaticStorage {
            SearchType buffer[TotalSize];
            
            template<typename StorageType, size_t ArraySize>
            constexpr StaticStorage(std::array<StorageType, ArraySize> data);
        };
        
        /**
         * Helper macro to define StaticStorage of specified searchType and name,
         * populated with specified data. StaticStorage size is automatically set.
         * 
         * The static storage copies and stores in its buffer, all the values of
         * all the query-able arrays of specified type, from fixedData.
         */
        #define STATIC_STORAGE_DEFINE(searchType, staticStorageName, fixedData) \
            constexpr static StaticStorageHelpers::StaticStorage<searchType, StaticStorageHelpers::getTotalSize<searchType>(fixedData)> staticStorageName(fixedData);
        
        /**
         * Populates the StaticVectors in outputStaticData (of matching search type),
         * with appropriate ranges in the specified StaticStorage.
         * Offsets within the static storage are worked out from the FixedVector sizes
         * within the provided fixedData.
         * 
         * The total count of the arrays within fixedData must be passed as the
         * "ArrayCount" template parameter, though that is automated by the
         * STATIC_STORAGE_POPULATE macro.
         */
        template<typename SearchType, size_t ArrayCount, typename StaticVectorStorageType, typename FixedVectorStorageType, size_t ArraySize, size_t StaticStorageTotalSize>
        static constexpr void populateStaticVectors(
            std::array<StaticVectorStorageType, ArraySize> & outputStaticData,
            std::array<FixedVectorStorageType, ArraySize> fixedData,
            const StaticStorage<SearchType, StaticStorageTotalSize> & staticStorage);
        
        /**
         * Macro to make it more convenient to use populateStaticVectors.
         * 
         * Populates the StaticVectors in outputStaticData (of matching search type),
         * with appropriate ranges in the specified StaticStorage.
         * Offsets within the static storage are worked out from the FixedVector sizes
         * within the provided fixedData.
         */
        #define STATIC_STORAGE_POPULATE(type, outputStaticData, staticStorage, fixedData) \
            StaticStorageHelpers::populateStaticVectors<type, StaticStorageHelpers::getTotalCount<type>(fixedData)>(outputStaticData, fixedData, staticStorage);
    };
}

#include "StaticStorageHelpers.inl"

#endif

/*
 * StaticStorageHelpers.cpp
 *
 *  Created on: 6 Aug 2023
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_STATICSTORAGE_STATICSTORAGEHELPERS_INL_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_STATICSTORAGE_STATICSTORAGEHELPERS_INL_

#include "StaticStorageHelpers.h"

#include <type_traits>

namespace rasc {
    
    // ---------------------------- Size query --------------------------------
    
    template<typename SearchType>
    constexpr StaticStorageHelpers::SizeQuery<SearchType>::SizeQuery(void) :
        totalSize(0) {
    }
    
    template<typename SearchType>
    template<typename Type, size_t FixedSize>
    constexpr void StaticStorageHelpers::SizeQuery<SearchType>::run(const FixedVector<Type, FixedSize> & fixedVector) {
        // Just add up sizes of all vectors with matching type.
        if (std::is_same<Type, SearchType>::value) {
            totalSize += fixedVector.size();
        }
    }
    
    // ---------------------------- Count query -------------------------------
    
    template<typename SearchType>
    constexpr StaticStorageHelpers::CountQuery<SearchType>::CountQuery(void) :
        totalCount(0) {
    }
    
    template<typename SearchType>
    template<typename Type, size_t FixedSize>
    constexpr void StaticStorageHelpers::CountQuery<SearchType>::run(const FixedVector<Type, FixedSize> & fixedVector) {
        // Just count the number of arrays with matching type.
        if (std::is_same<Type, SearchType>::value) {
            totalCount++;
        }
    }
    
    // ---------------------------- Copy to static query ----------------------
    
    template<typename SearchType>
    constexpr StaticStorageHelpers::CopyToStaticQuery<SearchType>::CopyToStaticQuery(SearchType * buffer) :
        bufferUpto(buffer) {
    }

    template<typename SearchType>
    template<typename Type, size_t FixedSize>
    constexpr void StaticStorageHelpers::CopyToStaticQuery<SearchType>::run(const FixedVector<Type, FixedSize> & fixedVector) {
        // For each array of matching type (use if constexpr to
        // avoid invalid conversions).
        if constexpr (std::is_same<Type, SearchType>::value) {
            // Copy all values to the buffer, move along our pointer
            // to where we're upto in the buffer.
            for (const Type & value : fixedVector) {
                *(bufferUpto++) = value;
            }
        }
    }
    
    // ---------------------------- All sizes query ---------------------------
    
    template<typename SearchType, size_t ArrayCount>
    constexpr StaticStorageHelpers::AllSizesQuery<SearchType, ArrayCount>::AllSizesQuery(void) :
        allSizes(),
        upto(0) {
    }

    template<typename SearchType, size_t ArrayCount>
    template<typename Type, size_t FixedSize>
    constexpr void StaticStorageHelpers::AllSizesQuery<SearchType, ArrayCount>::run(const FixedVector<Type, FixedSize> & fixedVector) {
        // For each array of matching size, take note of its size.
        if (std::is_same<Type, SearchType>::value) {
            allSizes[upto++] = fixedVector.size();
        }
    }
    
    // ---------------------------- Populate static query ---------------------
    
    template<typename SearchType, size_t ArrayCount>
    template<typename StorageType, size_t ArraySize>
    constexpr StaticStorageHelpers::PopulateStaticQuery<SearchType, ArrayCount>::PopulateStaticQuery(const SearchType * buffer, std::array<StorageType, ArraySize> fixedData) :
        allSizesQuery(),
        bufferUpto(buffer),
        arrayUpto(0) {
        
        // Complain if array count is wrong.
        if (getTotalCount<SearchType>(fixedData) != ArrayCount) {
            throw "Mismatching total count passed to ReadFromStaticQuery!";
        }
        
        // Run all-sizes query to get size of each array. This size
        // information is used to populate the offsets of each static vector.
        for (StorageType & element : fixedData) {
            element.query(allSizesQuery);
        }
    }
    
    template<typename SearchType, size_t ArrayCount>
    template<typename Type>
    constexpr void StaticStorageHelpers::PopulateStaticQuery<SearchType, ArrayCount>::run(StaticVector<Type> & staticVector) {
        // For each array of matching type (use if constexpr
        // to avoid bad type conversions).
        if constexpr (std::is_same<Type, SearchType>::value) {
            // Get size of the current array.
            const size_t arraySize = allSizesQuery.allSizes[arrayUpto];

            // Set the output static vector's address and array size.
            staticVector = StaticVector<Type>(bufferUpto, arraySize);

            // Move to next array along for next time, move buffer along by array size.
            arrayUpto++;
            bufferUpto += arraySize;
        }
    }

    // ---------------------------- Other helpers -----------------------------
    
    template<typename SearchType, typename StorageType, size_t ArraySize>
    constexpr size_t StaticStorageHelpers::getTotalSize(std::array<StorageType, ArraySize> data) {
        SizeQuery<SearchType> sizeQuery;
        for (StorageType & element : data) {
            element.query(sizeQuery);
        }
        return sizeQuery.totalSize;
    }
    
    template<typename SearchType, typename StorageType, size_t ArraySize>
    constexpr size_t StaticStorageHelpers::getTotalCount(std::array<StorageType, ArraySize> data) {
        CountQuery<SearchType> sizeQuery;
        for (StorageType & element : data) {
            element.query(sizeQuery);
        }
        return sizeQuery.totalCount;
    }
    
    template<typename SearchType, size_t TotalSize>
    template<typename StorageType, size_t ArraySize>
    constexpr StaticStorageHelpers::StaticStorage<SearchType, TotalSize>::StaticStorage(std::array<StorageType, ArraySize> data) :
        buffer() {
        
        // Complain if size doesn't match data's total size (will fail compilation
        // in a constexpr context).
        if (getTotalSize<SearchType>(data) != TotalSize) {
            throw "Mismatching total size passed to StaticStorage! Use STATIC_STORAGE_DEFINE to avoid this!";
        }
        
        // Use copy to static query to populate our buffer's data.
        CopyToStaticQuery<SearchType> copyQuery(buffer);
        for (StorageType & element : data) {
            element.query(copyQuery);
        }
    }
    
    template<typename SearchType, size_t ArrayCount, typename StaticVectorStorageType, typename FixedVectorStorageType, size_t ArraySize, size_t StaticStorageTotalSize>
    constexpr void StaticStorageHelpers::populateStaticVectors(std::array<StaticVectorStorageType, ArraySize> & outputStaticData, std::array<FixedVectorStorageType, ArraySize> fixedData, const StaticStorage<SearchType, StaticStorageTotalSize> & staticStorage) {
        // Complain if fixed data's total size or total count doesn't match
        // the template parameters (will fail compilation in a constexpr context).
        if (getTotalSize<SearchType>(fixedData) != StaticStorageTotalSize) {
            throw "Mismatching total size passed to populateStaticVectors! Use STATIC_STORAGE_DEFINE to avoid this!";
        }
        if (getTotalCount<SearchType>(fixedData) != ArrayCount) {
            throw "Mismatching total count passed to populateStaticVectors! Use STATIC_STORAGE_POPULATE to avoid this!";
        }
        
        // Use populate static query to populate the StaticVectors'
        // addresses and sizes.
        PopulateStaticQuery<SearchType, ArrayCount> populateQuery(staticStorage.buffer, fixedData);
        for (StaticVectorStorageType & element : outputStaticData) {
            element.query(populateQuery);
        }
        
        // Complain if the populate query didn't populate all arrays,
        // or didn't use up the entire static storage buffer (if so
        // something went wrong).
        if (populateQuery.arrayUpto != ArrayCount) {
            throw "Populate query didn't populate all arrays!";
        }
        if (populateQuery.bufferUpto != std::end(staticStorage.buffer)) {
            throw "Populate query didn't use all the static storage buffer!";
        }
    }
}

#endif

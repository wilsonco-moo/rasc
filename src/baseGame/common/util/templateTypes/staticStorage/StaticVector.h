/*
 * StaticVector.h
 *
 *  Created on: 6 Aug 2023
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_STATICSTORAGE_STATICVECTOR_H_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_STATICSTORAGE_STATICVECTOR_H_

#include <cstddef>

namespace rasc {

    /**
     * StaticVector allows interpreting a pointer to some arbitrary const
     * data of some type, as an array/vector-like object. It allows
     * accessing by index, querying size and iterating, but NOT the ability
     * to modify the data.
     * 
     * This is helpful when paired with FixedVector and StaticStorageHelpers,
     * to provide (mostly) arbitrary-sized vectors of data, allocated at
     * compile-time.
     * 
     * Note: The DummySize template parameter is added for convenience, to
     * make it easier to define templated data structures using either
     * FixedVector or StaticVector.
     */
    template<typename Type>
    class StaticVector {
    private:
        const Type * data;
        size_t elementCount;

    public:
        using const_iterator = const Type *;
        
        /**
         * Construct empty with null data and zero elements.
         */
        constexpr StaticVector(void);
        
        /**
         * Construct from the specified data, with the specified fixed
         * size.
         */
        constexpr StaticVector(const Type * data, size_t elementCount);

        /**
         * Accesses members by index.
         */
        constexpr const Type & operator[] (size_t index) const;

        /**
         * Returns the fixed size passed into our constructor.
         */
        constexpr size_t size(void) const;
        
        /**
         * Returns true if we're empty (i.e: size() == 0).
         */
        constexpr bool empty(void) const;

        /**
         * Allows iterating our fixed range of data.
         */
        constexpr const_iterator begin(void) const;
        constexpr const_iterator end(void) const;
    };
    
    /**
     * Template typedef which accepts an additional dummy size parameter.
     * This makes it possible to build a template parameter list able
     * to accept either FixedVector or StaticVector - as SizedStaticVector
     * has an identical template parameter list to FixedVector.
     * Like:
     * template<template<typename, size_t> class VectorType>
     * class Whatever { ... };
     */
    template<typename Type, size_t DummySize>
    using SizedStaticVector = StaticVector<Type>;
}

#include "StaticVector.inl"

#endif

/*
 * FixedVector.h
 *
 *  Created on: 6 Aug 2023
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_STATICSTORAGE_FIXEDVECTOR_H_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_STATICSTORAGE_FIXEDVECTOR_H_

#include <initializer_list>
#include <cstddef>
#include <array>

namespace rasc {

    /**
     * FixedVector provides a vector with a fixed maximum size, backed
     * by array storage. This makes it possible to assign or push_back in
     * a constexpr context.
     * 
     * Note: This is only intended to be used inside constexpr functions
     * for initialisation (paired with StaticVector and StaticStorageHelpers).
     * Internally we just use a std::array, so all elements are always
     * constructed by default, and push_back copy-assigns them (incrementing
     * our number of "used" elements, returned by size()).
     * 
     * If you want to use a fixed vector at runtime without always constructing
     * all members, use rascUI::EmplaceArray or rascUI::EmplaceVector instead.
     */
    template<typename Type, size_t FixedSize>
    class FixedVector {
    private:
        std::array<Type, FixedSize> data;
        size_t elementCount;

    public:
        // Value and iterator types matching std::array.
        using value_type = Type;
        using iterator = typename std::array<Type, FixedSize>::iterator;
        using const_iterator = typename std::array<Type, FixedSize>::const_iterator;
        
        /**
         * Construct empty with zero "used" elements (size() is zero).
         */
        constexpr FixedVector(void);
        
        /**
         * Construct from initialiser list. This must be shorter than
         * our FixedSize, (you'll get a compile error in constexpr context
         * if not).
         */
        constexpr FixedVector(std::initializer_list<Type> list);

        /**
         * Member access operators.
         */
        constexpr Type & operator[] (size_t index);
        constexpr const Type & operator[] (size_t index) const;

        /**
         * Note: size() returns our number of "used" elements,
         * max_size() returns our FixedSize.
         */
        constexpr size_t size(void) const;
        constexpr size_t max_size(void) const;
        
        /**
         * Returns true if we're empty (i.e: size() == 0).
         */
        constexpr bool empty(void) const;
        
        /**
         * Add an additional "used" element, incrementing our size().
         */
        constexpr void push_back(const Type & element);

        /**
         * Note: begin and end allow iterating over only the "used"
         * elements, not everything in the fixed array.
         */
        constexpr iterator begin(void);
        constexpr const_iterator begin(void) const;
        constexpr iterator end(void);
        constexpr const_iterator end(void) const;
    };
}

#include "FixedVector.inl"

#endif

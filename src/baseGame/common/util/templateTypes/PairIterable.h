/*
 * PairIterable.h
 *
 *  Created on: 27 Feb 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_PAIRITERABLE_H_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_PAIRITERABLE_H_


namespace rasc {

    /**
     * PairIterable allows iteration through a std::vector or std::list, using
     * a std::pair with the item's index, as you would with a std::unordered_map.
     */
    template<typename Index, typename BaseType, typename Container>
    class PairIterable {
    public:
        class iterator {
            friend class PairIterable;
        private:
            Index index;
            typename Container::const_iterator iter;
            iterator(Index index, typename Container::const_iterator iter) :
                index(index),
                iter(iter) {
            }
        public:
            ~iterator(void) {
            }
            inline bool operator == (const iterator & other) {
                return iter == other.iter;
            }
            inline bool operator != (const iterator & other) {
                return iter != other.iter;
            }
            inline iterator operator++ (int) {
                iterator m = *this;
                ++iter;
                ++index;
                return m;
            }
            inline iterator & operator++ (void) {
                ++iter;
                ++index;
                return *this;
            }
            inline std::pair<const Index, BaseType> operator * (void) const {
                return std::pair<const Index, BaseType>(index, *iter);
            }
        };
    private:
        iterator beginIter, endIter;
    public:
        PairIterable(const Container & container) :
            beginIter(0, container.cbegin()),
            endIter(container.size(), container.cend()) {
        }
        virtual ~PairIterable(void) {}
        inline const iterator & begin(void) const {
            return beginIter;
        }
        inline const iterator & end(void) const {
            return endIter;
        }
    };
}



#endif /* BASEGAME_COMMON_UTIL_PAIRITERABLE_H_ */

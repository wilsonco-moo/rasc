/*
 * TokenFunctionList.inl
 *
 *  Created on: 14 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_TOKENFUNCTIONLIST_INL_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_TOKENFUNCTIONLIST_INL_

#include "TokenFunctionList.h"

namespace rasc {
    
    // Use this to avoid #including iostream or cstdio in this header.
    void rascBasicPrintErr(const char * format, ...);
    
    template<typename Return, typename ... Args>
    TokenFunctionList<Return(Args...)>::TokenFunctionList(void) :
        functions() {
    }
    
    template<typename Return, typename ... Args>
    TokenFunctionList<Return(Args...)>::~TokenFunctionList(void) {
        if (!functions.empty()) {
            rascBasicPrintErr("WARNING: TokenFunctionList: Function list destroyed while %zu function(s) remain.\n", functions.size());
        }
    }
    
    template<typename Return, typename ... Args>
    void * TokenFunctionList<Return(Args...)>::addFunction(std::function<Return(Args...)> func) {
        using FuncIter = typename std::list<std::function<Return(Args...)>>::iterator;
        functions.push_back(func);
        return (void *)new FuncIter(--functions.end());
    }
    
    template<typename Return, typename ... Args>
    void TokenFunctionList<Return(Args...)>::removeFunction(void * token) {
        using FuncIter = typename std::list<std::function<Return(Args...)>>::iterator;
        FuncIter * iter = (FuncIter *)token;
        functions.erase(*iter);
        delete iter;
    }
    
    template<typename Return, typename ... Args>
    void TokenFunctionList<Return(Args...)>::updateFunctions(Args ... values) {
        for (std::function<Return(Args...)> & func : functions) {
            func(values...);
        }
    }
}

#endif

/*
 * TokenFunctionList.h
 *
 *  Created on: 14 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_TOKENFUNCTIONLIST_H_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_TOKENFUNCTIONLIST_H_

#include <functional>
#include <list>

namespace rasc {

    /**
     * TokenFunctionList provides a simple mechanism for "callback" functions.
     * 
     * Functions can be added to the list using addFunction. This returns a
     * token which can later be used to remove the function, using removeFunction.
     * Each time updateFunctions is called, all functions currently in the list are
     * run. Functions can then be removed later by token, using removeFunction.
     * 
     * Note: All functions must be removed before we are destroyed, otherwise a
     * warning is printed to the log.
     * Note: Where a return type other than void *can* be provided, there is
     * little point, as nothing is returned by updateFunctions. The return type
     * simply makes the template syntax similar to std::function, for convenience.
     * 
     * Example:
     *   TokenFunctionList<void(int)> funcList;
     *   void * token = funcList.addFunction([](int value) { printf("%d\n", value); });
     *   funcList.updateFunctions();
     *   funcList.removeFunction(token);
     */
    template<typename Type>
    class TokenFunctionList {};
    template<typename Return, typename ... Args>
    class TokenFunctionList<Return(Args...)> {
    private:
        std::list<std::function<Return(Args...)>> functions;
        
    public:
        /**
         * Constructor/destructor. If functions remain in the queue when we're
         * destroyed, a warning is printed.
         */
        TokenFunctionList(void);
        ~TokenFunctionList(void);
        
        /**
         * Adds a new function to the list, returning a token. This token should
         * then be used later on to remove the function.
         */
        void * addFunction(std::function<Return(Args...)> func);
        
        /**
         * Removes a function by token. This must be a token from the same
         * function list, returned by addFunction, and must not be removed more
         * than once. All added functions must be removed before we're destroyed.
         */
        void removeFunction(void * token);
        
        /**
         * Updates all functions in the list, using the provided parameters.
         */
        void updateFunctions(Args ... values);
    };
}

#include "TokenFunctionList.inl"

#endif

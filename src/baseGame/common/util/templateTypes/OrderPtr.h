/*
 * OrderPtr.h
 *
 *  Created on: 24 Sep 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_ORDERPTR_H_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_ORDERPTR_H_

namespace rasc {

    /**
     * OrderPtr is a very simple smart pointer class.
     * This allows pointers to any class which implements an operator< and operator==, to be
     * stored in an ordered standard template library container, such as a std::set or std::map.
     */
    template<typename T>
    class OrderPtr {
    private:
        T * ptr;
    public:
        OrderPtr(T * ptr) :
            ptr(ptr) {
        }
        inline T & operator * (void) const { return *ptr; }
        inline T * operator -> (void) const { return ptr; }
        inline bool operator <  (const OrderPtr<T> & other) const { return *ptr <  *other; }
        inline bool operator == (const OrderPtr<T> & other) const { return *ptr == *other; }
    };
}

#endif

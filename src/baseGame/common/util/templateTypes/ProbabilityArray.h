/*
 * ProbabilityArray.h
 *
 *  Created on: 1 Jan 2024
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_PROBABILITYARRAY_H_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_PROBABILITYARRAY_H_

#include <utility>
#include <cstddef>
#include <array>

namespace rasc {

    /**
     * Array-like container able to store a set of weighted values, able to be
     * used like a probability distribution. Internally the weighted values
     * are converted to be cumulative, so a binary search (upper_bound) can
     * find the result.
     * 
     * For example, if I constructed with:
     * { 1, A }, { 2, B }, { 3, C }
     * 
     * Then array.getTotal() == 1 + 2 + 3 == 6
     * So if I had a random number with 6 possible values, we get a probability
     * distribution weighted like this:
     * array.getValue(0) == A   (A weighted with 1)
     * array.getValue(1) == B   (B weighted with 2)
     * array.getValue(2) == B
     * array.getValue(3) == C   (C weighted with 3)
     * array.getValue(4) == C
     * array.getValue(5) == C
     * 
     * Note: Key should preferably be an integer type (could be float too),
     * or something else with sensible overloads in order to perform arithmetic.
     */
    template<typename Key, typename Value, size_t arraySize>
    class ProbabilityArray {
    public:
        // Typedefs, to roughly match with other container types.
        using key_type = Key;
        using mapped_type = Value;
        using value_type = std::pair<Key, Value>;
        using iterator = typename std::array<value_type, arraySize>::iterator;
        using const_iterator = typename std::array<value_type, arraySize>::const_iterator;
        
    private:
        // Our internal storage.
        std::array<value_type, arraySize> array;
        
    public:
        /**
         * Construct with a list of pairs of key, value.
         */
        template<size_t initArraySize>
        constexpr ProbabilityArray(const value_type(&initArray)[initArraySize]);
        
    private:
        // Comparison function for upper bound.
        static bool valueCompare(const Key & value, const value_type & element);
        
    public:
        // Normal array-like members.
        
        /**
         * Gets current size (== arraySize template parameter).
         */
        constexpr static size_t size(void);
        
        /**
         * Gets max size (== arraySize template parameter).
         */
        constexpr static size_t max_size(void);
        
        /**
         * Access by index normally, where index is at minimum 0, but less than size().
         */
        constexpr value_type & operator[](size_t index);
        constexpr const value_type & operator[](size_t index) const;
        
        /**
         * The various array iterator methods.
         */
        constexpr iterator begin(void);
        constexpr const_iterator begin(void) const;
        constexpr const_iterator cbegin(void) const;
        constexpr iterator end(void);
        constexpr const_iterator end(void) const;
        constexpr const_iterator cend(void) const;
        
        // Probability members.
        
        /**
         * Gets the "total" value for the probability array, i.e: the sum of all
         * probabilities passed into the constructor.
         */
        constexpr key_type getTotal(void) const;
        
        /**
         * For the specified key, which must be at minimum zero, but less than
         * getTotal(), returns the corresponding value. This returns the upper
         * bound across all cumulative values.
         * 
         * So, if the specified key is a random number (up to getTotal()), the
         * returned value is weighted according to the probabilities specified
         * in the constructor.
         * 
         * Note: For values who were given a zero weighting in the constructor,
         * they will never be returned here!
         */
        mapped_type & getValue(key_type key);
        const mapped_type & getValue(key_type key) const;
    };
}

#include "ProbabilityArray.inl"

#endif

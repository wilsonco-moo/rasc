/*
 * ProbabilityArray.inl
 *
 *  Created on: 1 Jan 2024
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_PROBABILITYARRAY_INL_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_PROBABILITYARRAY_INL_

#include "ProbabilityArray.h"

#include <algorithm>

namespace rasc {

    template<typename Key, typename Value, size_t arraySize>
    template<size_t initArraySize>
    constexpr ProbabilityArray<Key, Value, arraySize>::ProbabilityArray(const value_type(&initArray)[initArraySize]) {
        static_assert(arraySize == initArraySize, "Array passed into ProbabilityArray constructor must have size matching that of the ProbabilityArray!");
        
        // Copy everything from specified array to our local array.
        Key totalUpto = 0;
        for (size_t index = 0; index < arraySize; index++) {
            // First (key) values in our array are cumulative, (including their
            // own value, so increment first).
            totalUpto += initArray[index].first;
            array[index].first = totalUpto;
            
            // Second (value) values are just copied.
            array[index].second = initArray[index].second;
        }
    }
    
    template<typename Key, typename Value, size_t arraySize>
    bool ProbabilityArray<Key, Value, arraySize>::valueCompare(const Key & value, const value_type & element) {
        return value < element.first;
    }
    
    template<typename Key, typename Value, size_t arraySize>
    constexpr size_t ProbabilityArray<Key, Value, arraySize>::size(void) {
        return arraySize;
    }
    
    template<typename Key, typename Value, size_t arraySize>
    constexpr size_t ProbabilityArray<Key, Value, arraySize>::max_size(void) {
        return arraySize;
    }
    
    template<typename Key, typename Value, size_t arraySize>
    constexpr typename ProbabilityArray<Key, Value, arraySize>::value_type & ProbabilityArray<Key, Value, arraySize>::operator[](size_t index) {
        return array[index];
    }
    
    template<typename Key, typename Value, size_t arraySize>
    constexpr const typename ProbabilityArray<Key, Value, arraySize>::value_type & ProbabilityArray<Key, Value, arraySize>::operator[](size_t index) const {
        return array[index];
    }
    
    template<typename Key, typename Value, size_t arraySize>
    constexpr typename ProbabilityArray<Key, Value, arraySize>::iterator ProbabilityArray<Key, Value, arraySize>::begin(void) {
        return array.begin();
    }
    
    template<typename Key, typename Value, size_t arraySize>
    constexpr typename ProbabilityArray<Key, Value, arraySize>::const_iterator ProbabilityArray<Key, Value, arraySize>::begin(void) const {
        return array.begin();
    }
    
    template<typename Key, typename Value, size_t arraySize>
    constexpr typename ProbabilityArray<Key, Value, arraySize>::const_iterator ProbabilityArray<Key, Value, arraySize>::cbegin(void) const {
        return array.cbegin();
    }
    
    template<typename Key, typename Value, size_t arraySize>
    constexpr typename ProbabilityArray<Key, Value, arraySize>::iterator ProbabilityArray<Key, Value, arraySize>::end(void) {
        return array.end();
    }
    
    template<typename Key, typename Value, size_t arraySize>
    constexpr typename ProbabilityArray<Key, Value, arraySize>::const_iterator ProbabilityArray<Key, Value, arraySize>::end(void) const {
        return array.end();
    }
    
    template<typename Key, typename Value, size_t arraySize>
    constexpr typename ProbabilityArray<Key, Value, arraySize>::const_iterator ProbabilityArray<Key, Value, arraySize>::cend(void) const {
        return array.cend();
    }
    
    template<typename Key, typename Value, size_t arraySize>
    constexpr typename ProbabilityArray<Key, Value, arraySize>::key_type ProbabilityArray<Key, Value, arraySize>::getTotal(void) const {
        if constexpr (arraySize == 0) {
            return 0;
        } else {
            return array.back().first;
        }
    }
    
    template<typename Key, typename Value, size_t arraySize>
    typename ProbabilityArray<Key, Value, arraySize>::mapped_type & ProbabilityArray<Key, Value, arraySize>::getValue(key_type key) {
        return std::upper_bound(begin(), end(), key, &ProbabilityArray<Key, Value, arraySize>::valueCompare)->second;
    }
    
    template<typename Key, typename Value, size_t arraySize>
    const typename ProbabilityArray<Key, Value, arraySize>::mapped_type & ProbabilityArray<Key, Value, arraySize>::getValue(key_type key) const {
        return std::upper_bound(begin(), end(), key, &ProbabilityArray<Key, Value, arraySize>::valueCompare)->second;
    }
}

#endif

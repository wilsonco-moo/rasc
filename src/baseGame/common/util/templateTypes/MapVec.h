/*
 * MapVec.h
 *
 *  Created on: 28 Feb 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_TEMPLATETYPES_MAPVEC_H_
#define BASEGAME_COMMON_UTIL_TEMPLATETYPES_MAPVEC_H_

#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <cstddef>
#include <vector>

namespace rasc {

    /**
     * MapVec is a class which provides a std::vector, backed by a std::unordered_map.
     * Assuming that the type T is hashable in a sensible way.
     * This class combines the most useful properties of both a vector and a set.
     *  > Constant time insertion and erasure is provided, since the map
     *    keeps track of indices in the vector.
     *  > Constant time existence checking is provided, using the map.
     *  > Convenient (and faster compared to an unordered_set), iteration is provided
     *    by iterating through the vector.
     *  > Constant time access by index is provided, using the vector, which allows things
     *    like picking elements randomly in constant time: something which unordered_set
     *    does not support.
     */
    template<class T>
    class MapVec {
    private:
        std::unordered_map<T, size_t> internalMap;
        std::vector<T> internalVec;
    public:
        // ------------------ Constructors -------------------------

        /**
         * Creates an empty MapVec, with no elements.
         */
        MapVec(void) :
            internalMap(),
            internalVec() {
        }

        /**
         * Creates a MapVec with an initial set of elements.
         */
        MapVec(std::initializer_list<T> list) :
            internalMap(),
            internalVec() {
            for (const T & element : list) {
                insert(element);
            }
        }

        // ------------------------- Utility ----------------------------

        /**
         * Returns true if we contain the specified element.
         */
        bool contains(const T & element) const {
            return internalMap.find(element) != internalMap.end();
        }

        /**
         * Returns the number of elements that we have.
         */
        size_t size(void) const {
            return internalVec.size();
        }

        /**
         * Returns true if we are empty.
         */
        bool empty(void) const {
            return internalVec.empty();
        }
        
        /**
         * Swaps elements by index in the vector, this does not
         * affect the internal map.
         */
        void swapIndices(size_t indexA, size_t indexB) {
            T tmp = std::move(internalVec[indexA]);
            internalVec[indexA] = std::move(internalVec[indexB]);
            internalVec[indexB] = std::move(tmp);
        }

        // ----------------- Insert/erase ---------------------------
        /**
         * Inserts the specified element, into both the map
         * and the vector. Returns true if insertion actually took place.
         */
        bool insert(const T & element) {
            if (contains(element)) return false;
            internalMap[element] = internalVec.size();
            internalVec.push_back(element);
            return true;
        }

        /**
         * Erases the specified element from both the map and the vector.
         * Returns true (1) if something was actually erased, or false (0)
         * otherwise.
         */
        bool erase(const T & element) {
            auto iter = internalMap.find(element);
            if (iter == internalMap.end()) return false;
            size_t indexToRemove = iter->second;
            if (indexToRemove < internalVec.size() - 1) {
                T movedElem = internalVec.back();
                internalVec[indexToRemove] = movedElem;
                internalMap[movedElem] = indexToRemove;
            }
            internalVec.pop_back();
            internalMap.erase(iter);
            return true;
        }

        // ------------------------ Accessor ---------------------------

        /**
         * Allows (const) access to our internal map.
         */
        inline const std::unordered_map<T, size_t> & map(void) const {
            return internalMap;
        }
        /**
         * Allows (const) access to our internal vector.
         * This should be used for iterating, and selecting by index.
         */
        inline const std::vector<T> & vec(void) const {
            return internalVec;
        }
    };
}

#endif

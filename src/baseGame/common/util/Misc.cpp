/*
 * Misc.cpp
 *
 *  Created on: 29 Aug 2018
 *      Author: wilson
 */

#include "Misc.h"

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <threading/ThreadQueue.h>
#include <rascUI/util/Layout.h>
#include <tinyxml2/tinyxml2.h>
#include <rascUI/base/Theme.h>
#include <unordered_set>
#include <exception>
#include <algorithm>
#include <iostream>
#include <cstdarg>
#include <GL/gl.h>
#include <cstdlib>
#include <string>
#include <chrono>
#include <random>
#include <cctype>
#include <cerrno>
#include <cstdio>
#include <cmath>

// For attaching signal handlers, and obtaining backtraces.
#ifndef _WIN32
    #include <execinfo.h>
    #include <signal.h>
    #include <unistd.h>
#endif

#define LAYOUT_THEME theme

#include "../playerData/flag/Flag.h"
#include "numeric/MultiCol.h"
#include "numeric/Random.h"

namespace rasc {

    Misc::Misc(void) {
    }

    Misc::~Misc(void) {
    }

    const GLfloat Misc::RIGHT_ANGLE = (GLfloat)(acos(-1.0) * 0.5);
    const double Misc::RIGHT_ANGLE_PREC = acos(-1.0) * 0.5;

    void Misc::drawThickLine(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat radius) {
        double dir = atan2(x2 - x1, y2 - y1) - RIGHT_ANGLE;
        GLfloat xOff = (GLfloat) (radius * cos(dir + RIGHT_ANGLE_PREC));
        GLfloat yOff = -(GLfloat) (radius * sin(dir + RIGHT_ANGLE_PREC));
        glVertex2f(x1 + xOff, y1 + yOff);
        glVertex2f(x1 - xOff, y1 - yOff);
        glVertex2f(x2 - xOff, y2 - yOff);
        glVertex2f(x2 + xOff, y2 + yOff);
    }

    bool Misc::readNumber(long * number) {
        // Read the text, return failure if the user entered nothing.
        std::string text;
        std::getline(std::cin, text);
        if (text.empty()) return false;

        // Convert the text to a number.
        char * endPtr;
        errno = 0;
        long val = strtol(text.c_str(), &endPtr, 0);
        if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN)) ||  // If there was overflow/underflow ...
            (errno != 0 && val == 0) ||                                   // ... or some other error ...
            (endPtr == text.c_str())) {                                   // ... or strtol didn't actually find any numbers ...
            // Then return a failure.
            return false;
        }

        *number = val;
        return true;
    }

    long Misc::readNumberRange(long minimum, long maximum, bool printMinimum, bool printMaximum) {
        long number;
        if (!readNumber(&number) || number < minimum || number > maximum) {
            do {
                if (printMinimum && printMaximum) {
                    std::cout << "Please enter a number between " << minimum << " and " << maximum << " (inclusive).\n> ";
                } else if (printMinimum) {
                    std::cout << "Please enter a number that is " << minimum << " or bigger.\n> ";
                } else if (printMaximum) {
                    std::cout << "Please enter a number that is " << maximum << " or less.\n> ";
                } else {
                    std::cout << "Please enter a number.\n> ";
                }
            } while(!readNumber(&number) || number < minimum || number > maximum);
        }
        return number;
    }

    bool Misc::getYN(const std::string & msg) {
        while(true) {
            std::cout << msg;
            std::string str;
            std::getline(std::cin, str);
            bool cont = true;
            bool ret;
            if (str.size() == 1) {
                switch(str[0]) {
                case 'y': case 'Y':
                    cont = false; ret = true; break;
                case 'n': case 'N':
                    cont = false; ret = false; break;
                }
            }
            if (cont) continue;
            return ret;
        }
    }

    size_t Misc::selectFromOptions(const std::vector<std::string> options, const std::string & topMsg, const std::string & bottomMsg) {
        std::cout << topMsg;
        for (size_t i = 0; i < options.size(); i++) {
            std::cout << i << ": " << options[i] << "\n";
        }
        std::cout << bottomMsg;
        return readNumberRange(0, options.size() - 1);
    }

    std::string Misc::readNonEmpty(const std::string & initialMessage, const std::string & complaintMessage) {
        std::string str;
        std::cout << initialMessage;
        std::getline(std::cin, str);
        while(str.empty()) {
            std::cout << complaintMessage;
            std::getline(std::cin, str);
        }
        return str;
    }
    
    std::string Misc::boolToStr(bool value) {
        return value ? "true" : "false";
    }
    
    static const std::unordered_set<std::string> strToBoolTrueValues = { "true", "yes", "1" };
    
    bool Misc::strToBool(const std::string & str) {
        std::string lowerStr = str;
        for (char & character : lowerStr) {
            character = (char)std::tolower((unsigned char)character);
        }
        return strToBoolTrueValues.find(lowerStr) != strToBoolTrueValues.end();
    }
    
    std::vector<std::string> Misc::splitString(const std::string & string, char character) {
        std::vector<std::string> output;
        output.push_back(std::string());
        for (char c : string) {
            if (c == ' ') {
                output.push_back(std::string());
            } else {
                output.back().push_back(c);
            }
        }
        return output;
    }
    
    void Misc::initSharedOptionsConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
        tinyxml2::XMLElement * colourBlindness = doc->NewElement("colourBlindness");
            colourBlindness->SetAttribute("mode", MultiCol::colourBlindModeToFriendlyName(MultiCol::ColourBlindMode::none));
        elem->InsertEndChild(colourBlindness);
        
        tinyxml2::XMLElement * humanFlag = doc->NewElement("humanFlag");
            humanFlag->SetAttribute("createMode", Flag::createModeToFriendlyName(Flag::CreateMode::defaultFlag));
        elem->InsertEndChild(humanFlag);
        
        tinyxml2::XMLElement * aiFlag = doc->NewElement("aiFlag");
            aiFlag->SetAttribute("createMode", Flag::createModeToFriendlyName(Flag::CreateMode::defaultFlag));
        elem->InsertEndChild(aiFlag);
    }

    // ========================================== Miscellaneous numeric methods ====================================

    long Misc::strtolRange(const std::string & str, long min, long max) {
        return minmax(strtol(str.c_str(), NULL, 0), min, max);
    }

    #define FORMAT_DECIMAL_BUFFSIZE 32
    std::string Misc::formatDecimal(GLfloat value, int decimalPlaces) {
        char buffer[FORMAT_DECIMAL_BUFFSIZE];
        std::snprintf(&buffer[0], FORMAT_DECIMAL_BUFFSIZE, "%.*f", decimalPlaces, value);
        return std::string(&buffer[0]);
    }
    
    unsigned int Misc::digitCount(uint64_t value) {
        unsigned int digits = 0;
        do {
            digits++;
            value /= 10;
        } while (value > 0);
        return digits;
    }
    
    uint64_t Misc::timeSeededRandom(uint64_t min, uint64_t max) {
        // Grab system time, and XOR both halves of it to seed a basic random
        // number generator.
        const uint64_t time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        std::minstd_rand generator((time & 0xffffffff) ^ (time >> 32u));
        
        // Skip a few values (or results from consecutive timestamps are
        // similar/identical), then return the result.
        generator.discard(8);
        std::uniform_int_distribution<uint64_t> intDistribution(min, max);
        return intDistribution(generator);
    }
    
    uint64_t Misc::timeSeededRandomRange(uint64_t begin, uint64_t end) {
        return timeSeededRandom(begin, end - 1u);
    }
    
    provstat_t Misc::valueOutOfTotalProvstat(provstat_t value, provstat_t total, provstat_t outOf) {
        if (total == 0) {
            if (value > 0) {
                return outOf;
            } else if (value < 0) {
                return -outOf;
            } else {
                return 0;
            }
        } else {
            return (value * outOf) / total;
        }
    }
    
    uint64_t Misc::valueOutOfTotalUnsigned(uint64_t value, uint64_t total, uint64_t outOf) {
        if (total == 0) {
            if (value > 0) {
                return outOf;
            } else {
                return 0;
            }
        } else {
            return (value * outOf) / total;
        }
    }
    
    // =========================================== Miscellaneous UI methods ========================================

    void Misc::backgroundScrollpaneAdd(rascUI::Theme * theme, rascUI::TopLevelContainer * topLevel, void * token, threading::ThreadQueue * queue, rascUI::ScrollPane * scrollPane, std::vector<rascUI::ToggleButton> * buttonVector, rascUI::ToggleButtonSeries * series, std::vector<std::string> (*namesFunc)(void), bool selectFirstButton) {
        queue->add(token, [theme, topLevel, scrollPane, buttonVector, series, namesFunc, selectFirstButton](void) {

            std::vector<std::string> values = namesFunc();

            // Once we have all the values, populate the toggle button array. This is safe to do in the other
            // thread, since we assume that buttonVector is never used anywhere until the toggle buttons are actually added.
            buttonVector->reserve(values.size());
            for (const std::string & value : values) {
                buttonVector->emplace_back(
                    series,
                    rascUI::Location(SUB_BORDER_B(SUB_BORDER_R(GEN_FILL))),
                    value
                );
            }

            // Then safely add the toggle buttons to the scroll pane. We can safely access buttonVector since
            // we assume it will never be modified again.
            topLevel->beforeEvent->addFunction([scrollPane, buttonVector, series, selectFirstButton](void) {
                for (rascUI::ToggleButton & button : *buttonVector) {
                    scrollPane->contents.add(&button);
                }
                if (selectFirstButton && !buttonVector->empty()) {
                    series->setSelectedButton(&(*buttonVector)[0]);
                }
            });
        });
    }

    // ======================================= Other utility methods =================================

    #ifndef _WIN32
        // The maximum depth of stacktrace to display.
        #define TRACE_MAXLEN 50

        /**
         * A signal handler which prints a stacktrace. This prints a message,
         * then gets at most TRACE_MAXLEN stack frame return addresses using
         * backtrace, and prints them using backtrace_symbols_fd. It also
         * clears the signal handler then raises the signal again.
         * Adapted from: https://stackoverflow.com/a/77336
         *               http://web.archive.org/web/20200619194305/https://stackoverflow.com/questions/77005/how-to-automatically-generate-a-stacktrace-when-my-program-crashes
         */
        static void signalHandler(int sig) {
            // Find signal name.
            const char* signalName = "unknown";
            switch(sig) {
            case SIGSEGV:
                signalName = "SIGSEGV";
                break;
            case SIGFPE:
                signalName = "SIGFPE";
                break;
            }
            // Print message and stack trace.
            fprintf(stderr, "\nTERMINATING due to signal: %d (%s).\n", sig, signalName);
            void * returnAddresses[TRACE_MAXLEN];
            int count = backtrace(returnAddresses, TRACE_MAXLEN);
            backtrace_symbols_fd(returnAddresses, count, STDERR_FILENO);
            // Raise the signal again so we still get a core dump!
            signal(sig, SIG_DFL);
            raise(sig);
        }

        /**
         * A terminate handler for uncaught exceptions. This does the
         * same as segvHandler, but then runs the default action (std::abort).
         * See: https://stackoverflow.com/a/2445569
         *      http://web.archive.org/web/20161223111637/https://stackoverflow.com/questions/2443135/how-do-i-find-where-an-exception-was-thrown-in-c
         */
        static void terminateHandler(void) {
            fprintf(stderr, "\nTERMINATING due to uncaught exception.\n");
            void * returnAddresses[TRACE_MAXLEN];
            int count = backtrace(returnAddresses, TRACE_MAXLEN);
            backtrace_symbols_fd(returnAddresses, count, STDERR_FILENO);
            std::abort();
        }
    #endif

    void Misc::setupSignalAndTerminateHandlers(void) {
        // Set signal and terminate handlers (but only in linux).
        #ifndef _WIN32
            signal(SIGSEGV, &signalHandler);
            signal(SIGFPE, &signalHandler);
            std::set_terminate(&terminateHandler);
        #endif
    }

    void Misc::makeFirstLetterCapital(std::string & string) {
        if (!string.empty()) {
            string[0] = std::toupper(string[0]);
        }
    }

    // ====================================== Colour and image methods ===============================

    class Channel {
    public:
        enum { RED, GREEN, BLUE, ALPHA, COUNT };
    };

    void Misc::straightToPremultiplied(unsigned char * image, size_t sizeInBytes) {
        unsigned char * imageEnd = image + sizeInBytes;
        while(image < imageEnd) {

            // Multiply each channel by the alpha.
            unsigned int alpha = image[Channel::ALPHA];
            image[Channel::RED]   = (image[Channel::RED]   * alpha) / 255;
            image[Channel::GREEN] = (image[Channel::GREEN] * alpha) / 255;
            image[Channel::BLUE]  = (image[Channel::BLUE]  * alpha) / 255;

            image += Channel::COUNT;
        }
    }

    void Misc::premultipliedToStraight(unsigned char * image, size_t sizeInBytes) {
        unsigned char * imageEnd = image + sizeInBytes;
        while(image < imageEnd) {

            // Divide each channel by the alpha. Make sure the alpha is not zero.
            unsigned int alpha = image[Channel::ALPHA];
            alpha = std::max((unsigned int)1, alpha);
            image[Channel::RED]   = (image[Channel::RED]   * 255) / alpha;
            image[Channel::GREEN] = (image[Channel::GREEN] * 255) / alpha;
            image[Channel::BLUE]  = (image[Channel::BLUE]  * 255) / alpha;

            image += Channel::COUNT;
        }
    }

    void Misc::alphaOver(unsigned char * result, const unsigned char * image, size_t sizeInBytes) {
        unsigned char * resultEnd = result + sizeInBytes;
        while(result < resultEnd) {

            // Set all four channels of the result to  image + result * (1 - imageAlpha).
            unsigned int multAlpha = 255 - image[Channel::ALPHA];
            result[Channel::ALPHA] = image[Channel::ALPHA] + (result[Channel::ALPHA] * multAlpha) / 255;
            result[Channel::RED]   = image[Channel::RED]   + (result[Channel::RED]   * multAlpha) / 255;
            result[Channel::GREEN] = image[Channel::GREEN] + (result[Channel::GREEN] * multAlpha) / 255;
            result[Channel::BLUE]  = image[Channel::BLUE]  + (result[Channel::BLUE]  * multAlpha) / 255;

            result += Channel::COUNT;
            image += Channel::COUNT;
        }
    }
    
    void rascBasicPrint(const char * format, ...) {
        std::va_list args;
        va_start(args, format);
        std::vprintf(format, args);
        va_end(args);
    }
    
    void rascBasicPrintErr(const char * format, ...) {
        std::va_list args;
        va_start(args, format);
        std::vfprintf(stderr, format, args);
        va_end(args);
    }
}

/*
 * TerrainDisplayDef.h
 *
 *  Created on: 20 Jun 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_DEFINITIONS_TERRAINDISPLAYDEF_H_
#define BASEGAME_COMMON_UTIL_DEFINITIONS_TERRAINDISPLAYDEF_H_

namespace rasc {

    /**
     * This header file contains macros, enums and types required for
     * interacting with the terrain display system, (see TerrainDisplayManager.h).
     *
     * This is separate from TerrainDisplayManager.h, since this is accessed
     * by common classes, for trait interaction.
     */

    /**
     * This defines the list of terrain display texture
     * names.
     */
    #define RASC_TERRAIN_DISPLAY_TEXTURES \
        X(wasteland)                      \
        X(boggy)                          \
        X(cityBack)                       \
        X(forested)                       \
        X(cityFront)                      \
        X(tundra)                         \
        X(plains)                         \
        X(mountainousFront)               \
        X(hillyFront)                     \
        X(hillyBack)                      \
        X(sky)                            \
        X(sea)                            \
        X(mountainousBack)                \
        X(desert)                         \
        X(grassland)                      \
        X(iceSheet)                       \
        X(jungle)                         \
        X(lake)                           \
        X(moistWoodland)                  \
        X(monsoonRegionBack)              \
        X(monsoonRegionFore)              \
        X(mudflat)                        \
        X(outback)                        \
        X(plateau)                        \
        X(rainforest)                     \
        X(saltFlat)                       \
        X(saltMarsh)                      \
        X(savannah)                       \
        X(steppe)                         \
        X(taiga)                          \
        X(woodland)

    /**
     * This defines an enum of terrain display texture IDs,
     * along with a count.
     */
    class TerDispTex {
    public:
        #define X(terrainDisplayName) terrainDisplayName,
        enum { RASC_TERRAIN_DISPLAY_TEXTURES COUNT };
        #undef X
    };

    /**
     * This represents a pair of depth, and texture ID (from TerDispTex).
     * A set of these is intended to be used with TerrainDisplayManager,
     * to represent how to draw a terrain type/trait.
     */
    class TerDispLayer {
    public:
        float depth;
        unsigned int textureId;
    };
}

#endif

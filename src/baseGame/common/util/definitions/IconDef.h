/*
 * IconDef.h
 *
 *  Created on: 28 Jun 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_DEFINITIONS_ICONDEF_H_
#define BASEGAME_COMMON_UTIL_DEFINITIONS_ICONDEF_H_

#include "../macros/MacroUtil.h"

namespace rasc {

    // An X macro list (see helper macros below). This provides:
    // X(previousName, previousShapeCount, name, shapeCount)
    // Note: The first one's previous has a name and shapeCount of 0.
    // Also: The last one is given through RASC_ICON_LAST(name, shapeCount),
    //       and defining this is optional.
    #define RASC_ICON_LIST \
        MUTIL_EXPAND_1(RASC_ICON_LIST_INTERNAL)

    // Defines each icon, along with the count of how many shapes it comprises
    // of. Note that the first one must be suffixed _F, the last one must be
    // suffixed _L, and all others must be suffixed _M. Note that lengths
    // must NOT BE ZERO.
    #define RASC_ICON_LIST_INTERNAL       \
        RASC_ICON_F(alert, 5)             \
        RASC_ICON_M(build, 7)             \
        RASC_ICON_M(terrain, 7)           \
        RASC_ICON_M(terrainGround, 6)     \
        RASC_ICON_M(terrainWater, 9)      \
        RASC_ICON_M(terrainClimate, 9)    \
        RASC_ICON_M(terrainShape, 7)      \
        RASC_ICON_M(garrison, 4)          \
        RASC_ICON_M(menu, 7)              \
        RASC_ICON_M(army, 2)              \
        RASC_ICON_M(bill, 6)              \
        RASC_ICON_M(manpower, 10)         \
        RASC_ICON_M(currency, 11)         \
        RASC_ICON_M(unrest, 4)            \
        RASC_ICON_M(land, 13)             \
        RASC_ICON_M(battle, 6)            \
        RASC_ICON_M(province, 10)         \
        RASC_ICON_M(provinceValue, 12)    \
        RASC_ICON_M(manpowerBuilding, 5)  \
        RASC_ICON_M(currencyBuilding, 11) \
        RASC_ICON_M(provinceBuilding, 17) \
        RASC_ICON_M(combatBuilding, 5)    \
        RASC_ICON_M(currencyLack, 9)      \
        RASC_ICON_M(demolish, 9)          \
        RASC_ICON_L(modularTopbar, 5)

    // Each macro defines itself and the start of the next, to provide information
    // about the previous icon. Defer expansion of macro X, so that "halves" of X
    // macros are not expanded. The last icon is further put through "RASC_ICON_LAST",
    // allowing the total size to be calculated.
    #define RASC_ICON_F(name, shapeCount) MUTIL_DEFER(X)(0, 0, name, shapeCount) MUTIL_DEFER(X)(name, shapeCount,
    #define RASC_ICON_M(name, shapeCount) name, shapeCount) MUTIL_DEFER(X)(name, shapeCount,
    #define RASC_ICON_L(name, shapeCount) name, shapeCount) MUTIL_DEFER(RASC_ICON_LAST)(name, shapeCount)



    /**
     * This class contains an enum of icon IDs. These can be accessed
     * like "IconDef::garrison", for use with IconDrawer.
     *
     * Note that icon IDs are not sequential: they represents positions within
     * the data array managed by IconDrawer.
     *
     * The special value IconDef::SIZE represents the total amount of data
     * required to store the data of all icons.
     *
     * Icons are stored as an array of GLfloats. The first HEADER_SIZE
     * GLfloats represent the size of the icon (shape count). Each
     * shape comprises SHAPE_VERTICES vertices, and is stored using
     * SHAPE_SIZE GLfloat values. This means that each icon comprises
     * of HEADER_SIZE + SHAPE_SIZE * shapeCount GLfloat values.
     */
    class IconDef {
    public:
        /**
         * This represents the size of the header for each icon,
         * within the IconDrawer internal array.
         */
        constexpr static unsigned int HEADER_SIZE = 1;

        /**
         * This represents the number of vertices which each icon
         * shape comprises of.
         */
        constexpr static unsigned int SHAPE_VERTICES = 4;

        /**
         * This represents the size (in GLfloats) of each icon shape.
         * This assumes that each vertex is stored using two values.
         */
        constexpr static unsigned int SHAPE_SIZE = SHAPE_VERTICES * 2;

        /**
         * The maximum number of characters which an icon's name can
         * consist of. This must be set to the length of the longest
         * icon's name, (a fact which is asserted using static_assert
         * in IconDrawer.cpp). This exists to make it easier to search
         * for and store icon names.
         */
        constexpr static unsigned int MAX_ICON_NAME_LEN = 16;

        /**
         * This is generally used to represent invalid icons.
         * Do not try to draw this icon ID.
         */
        constexpr static unsigned int INVALID = (unsigned int)-1;

        /**
         * An icon ID which is always valid (the first icon).
         * This is always fine to draw, and should be used as a placeholder.
         */
        constexpr static unsigned int DEFAULT = 0;

        // Each icon ID (where the previous shape count is not zero,
        // i.e: isn't the first one), is offsetted from the previous by
        // "HEADER_SIZE + (previousShapeCount * SHAPE_SIZE)".
        #define X(previousName, previousShapeCount, name, shapeCount)                \
            name = previousName + (previousShapeCount == 0 ? 0 :                     \
                                   HEADER_SIZE + (previousShapeCount * SHAPE_SIZE)),
        // The size can be worked out as an offset of
        // "HEADER_SIZE + (shapeCount * SHAPE_SIZE)"
        // from the last icon ID.
        #define RASC_ICON_LAST(name, shapeCount) \
            SIZE = name + HEADER_SIZE + (shapeCount * SHAPE_SIZE)
        enum { RASC_ICON_LIST };
        #undef X
        #undef RASC_ICON_LAST
    };

    // Define this to be empty, so further uses of RASC_ICON_LIST
    // do not *have* to define a RASC_ICON_LAST.
    #define RASC_ICON_LAST(name, shapeCount)
}

#endif

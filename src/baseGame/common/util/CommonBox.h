/*
 * CommonBox.h
 *
 *  Created on: 14 Nov 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_COMMONBOX_H_
#define BASEGAME_COMMON_UTIL_COMMONBOX_H_

#include <cstdint>
#include <cstddef>

namespace rasc {

    class UnifiedStatSystem;
    class AreasStat;
    class ContinentsStat;
    class ProvincesStat;
    class UnitsDeployedStat;
    class AdjacencyStat;
    class BuildingStat;
    class GameMap;
    class PlayerDataSystem;
    class RascConfig;
    class Random;

    /**
     * This class defines a set of things which exist on both the client
     * and the server. An instance of this should be stored within both
     * RascBox and ServerBox.
     * The idea is that classes which are part of common, should use this
     * instead of RascBox or ServerBox.
     * This class includes no memory management or implementation. That
     * should be handled respectively by ServerBox and ClientBox.
     */
    class CommonBox {
    public:
        /**
         * This represents the current turn count, for the client and the server.
         * Any client or server classes which need the current turn count, should
         * use this value. This value should only be accessed from within the
         * main mutex of the client or server.
         */
        uint64_t turnCount;

        UnifiedStatSystem * statSystem;
        AreasStat * areasStat;
        ContinentsStat * continentsStat;
        ProvincesStat * provincesStat;
        UnitsDeployedStat * unitsDeployedStat;
        AdjacencyStat * adjacencyStat;
        BuildingStat * buildingStat;

        GameMap * map;
        PlayerDataSystem * playerDataSystem;
        RascConfig * rascConfig;
        Random * random;
    };
}

#endif

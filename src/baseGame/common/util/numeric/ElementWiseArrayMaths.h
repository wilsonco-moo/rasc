/*
 * ElementWiseArrayMaths.h
 *
 *  Created on: 9 Feb 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_NUMERIC_ELEMENTWISEARRAYMATHS_H_
#define BASEGAME_COMMON_UTIL_NUMERIC_ELEMENTWISEARRAYMATHS_H_

#include <cstddef>
#include <array>

namespace rasc {

    /**
     * This header file contains a series of convenient templated methods, which allow
     * element wise addition, subtraction, multiplication and division of std::arrays.
     * This is in addition to the element-wise equality and comparison operators provided
     * by the standard library.
     */

    // --------------------------- Addition -----------------------------

    /**
     * operator+ allows element-wise addition of two arrays, like this:
     * result = first + second;
     */
    template<class T, size_t N>
    std::array<T, N> operator + (const std::array<T, N> & first, const std::array<T, N> & second) {
        std::array<T, N> result;
        for (size_t i = 0; i < N; i++) {
            result[i] = first[i] + second[i];
        }
        return result;
    }

    /**
     * operator+= allows element-wise addition of array to an existing array, like this:
     * first += second;
     */
    template<class T, size_t N>
    std::array<T, N> & operator += (std::array<T, N> & first, const std::array<T, N> & second) {
        for (size_t i = 0; i < N; i++) {
            first[i] += second[i];
        }
        return first;
    }

    // --------------------------- Subtraction --------------------------

    /**
     * operator- allows element-wise subtraction of two arrays, like this:
     * result = first - second;
     */
    template<class T, size_t N>
    std::array<T, N> operator - (const std::array<T, N> & first, const std::array<T, N> & second) {
        std::array<T, N> result;
        for (size_t i = 0; i < N; i++) {
            result[i] = first[i] - second[i];
        }
        return result;
    }

    /**
     * operator-= allows element-wise subtraction of an array from an existing array, like this:
     * first -= second;
     */
    template<class T, size_t N>
    std::array<T, N> & operator -= (std::array<T, N> & first, const std::array<T, N> & second) {
        for (size_t i = 0; i < N; i++) {
            first[i] -= second[i];
        }
        return first;
    }

    // --------------------------- Multiplication -----------------------

    /**
     * operator* allows element-wise multiplication of two arrays, like this:
     * result = first * second;
     */
    template<class T, size_t N>
    std::array<T, N> operator * (const std::array<T, N> & first, const std::array<T, N> & second) {
        std::array<T, N> result;
        for (size_t i = 0; i < N; i++) {
            result[i] = first[i] * second[i];
        }
        return result;
    }

    /**
     * operator*= allows element-wise multiplication of an array to an existing array, like this:
     * first *= second;
     */
    template<class T, size_t N>
    std::array<T, N> & operator *= (std::array<T, N> & first, const std::array<T, N> & second) {
        for (size_t i = 0; i < N; i++) {
            first[i] *= second[i];
        }
        return first;
    }

    // --------------------------- Division -----------------------------

    /**
     * operator/ allows element-wise division of two arrays, like this:
     * result = first / second;
     */
    template<class T, size_t N>
    std::array<T, N> operator / (const std::array<T, N> & first, const std::array<T, N> & second) {
        std::array<T, N> result;
        for (size_t i = 0; i < N; i++) {
            result[i] = first[i] / second[i];
        }
        return result;
    }

    /**
     * operator/= allows element-wise division of an array by an existing array, like this:
     * first /= second;
     */
    template<class T, size_t N>
    std::array<T, N> & operator /= (std::array<T, N> & first, const std::array<T, N> & second) {
        for (size_t i = 0; i < N; i++) {
            first[i] /= second[i];
        }
        return first;
    }
}

#endif

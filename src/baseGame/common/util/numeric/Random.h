/*
 * Random.h
 *
 *  Created on: 26 Dec 2023
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_NUMERIC_RANDOM_H_
#define BASEGAME_COMMON_UTIL_NUMERIC_RANDOM_H_

#include <GL/gl.h>
#include <cstddef>
#include <random>

namespace rasc {

    /**
     * Storage for a random number generator, able to be stored in CommonBox
     * and shared between multiple systems. Also provided are utility methods
     * for generating random numbers.
     */
    class Random final {
    public:
        /**
         * Shared random number generator for all game systems to use.
         * Note: Don't allow concurrent access on multiple threads!
         */
        std::mt19937 generator;
        
        /**
         * Generator (above) is automatically seeded with getSeedSeq when
         * constructed.
         */
        Random(void);
        
        /**
         * Builds a seed_seq, populated with some entropy available to us:
         * System random device, system time, an address from the stack.
         */
        static std::seed_seq getSeedSeq(void);
        
        /**
         * Convenience methods to generate random integers in the specified
         * range, NOT INCLUDING the end value. Begin must be smaller than end.
         * i.e: begin <= n < end
         */
        unsigned int uintRange(const unsigned int begin, const unsigned int end);
        size_t sizetRange(const size_t begin, const size_t end);
        
        /**
         * Convenience method to generate a random float in the specified range,
         * NOT INCLUDING the end value. Begin must be smaller than end.
         * NOTE: Internally we loop to cope with rounding issues in
         * std::uniform_real_distribution, to guarantee that the returned value
         * ACTUALLY isn't equal to end!
         * i.e: begin <= n < end
         */
        GLfloat glfloatRange(const GLfloat begin, const GLfloat end);
        
        /**
         * Convenience method to return a random float in range 0 - 1 (not
         * including 1), i.e: slightly more convenient wrapper around
         * std::generate_canonical.
         */
        GLfloat glfloat01(void);
    };
}

#endif

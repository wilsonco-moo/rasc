/*
 * Random.cpp
 *
 *  Created on: 26 Dec 2023
 *      Author: wilson
 */

#include "Random.h"

#include <type_traits>
#include <cstdint>
#include <chrono>

namespace rasc {

    Random::Random(void) :
        generator() {
        
        // Automatically seed with system random device and time.
        std::seed_seq seedSeq = getSeedSeq();
        generator.seed(seedSeq);
        
        // Skip the first few: If there's only one bit of difference in how
        // the generator is seeded, the first few values generated are pretty similar!
        generator.discard(100);
    }
    
    std::seed_seq Random::getSeedSeq(void) {
        // Entropy source 1: System random device. Usually this will give us
        // good results, but on some systems this might not be the case.
        std::random_device device;
        
        // Entropy source 2: Current system time. Most of the time this works
        // well, but you may still end up with multiple instances being started
        // at the same time (in which case it won't work).
        // Assume this is a 64 bit int (should be).
        const uint64_t time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        
        // Entropy source 3: If all else fails, we can take advantage of address
        // space randomisation. Throw an address from the stack into the mix.
        const uint64_t address = (uint64_t)&device;
        
        // Seed seq uses 32 bit int.
        using IntType = std::seed_seq::result_type;
        return std::seed_seq({
            (IntType)device(), (IntType)device(), (IntType)device(), (IntType)device(), // might as well grab a few from random device
            (IntType)time, // time (lower 32 bits)
            (IntType)(time >> 32u), // time (upper 32 bits)
            (IntType)address, // address from stack (lower 32 bits)
            (IntType)(address >> 32u) // address from stack (upper 32 bits, if applicable)
        });
    }
    
    unsigned int Random::uintRange(const unsigned int begin, const unsigned int end) {
        std::uniform_int_distribution<unsigned int> distribution(begin, end - 1u);
        return distribution(generator);
    }
    
    size_t Random::sizetRange(const size_t begin, const size_t end) {
        std::uniform_int_distribution<size_t> distribution(begin, end - 1u);
        return distribution(generator);
    }
    
    GLfloat Random::glfloatRange(const GLfloat begin, const GLfloat end) {
        std::uniform_real_distribution<GLfloat> distribution(begin, end);
        
        // std::generate_canonical is now guaranteed NOT to include the end value in gcc as of:
        // https://gcc.gnu.org/bugzilla/show_bug.cgi?id=63176
        // http://web.archive.org/web/20231029034508/https://gcc.gnu.org/bugzilla/show_bug.cgi?id=63176
        // BUT, std::uniform_real_distribution still goes wrong and returns the
        // end value, due to rounding. See:
        // https://bugs.llvm.org/show_bug.cgi?id=18767
        // http://web.archive.org/web/20231028175443/https://bugs.llvm.org/show_bug.cgi?id=18767
        // So before we return the value to the user, double-check that it isn't
        // equal to the end value. If it is (and the user has given us a sensible
        // combination of begin/end), just try again. This can be reproduced easily
        // if you set end to the next floating point value up from begin.
        GLfloat output;
        do {
            output = distribution(generator);
        } while(output >= end && begin < end);
        
        return output;
    }
    
    GLfloat Random::glfloat01(void) {
        constexpr size_t digits = std::numeric_limits<GLfloat>::digits;
        return std::generate_canonical<GLfloat, digits>(generator);
    }
}

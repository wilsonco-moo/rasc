/*
 * UniqueIdAssigner.h
 *
 *  Created on: 7 Mar 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_NUMERIC_UNIQUEIDASSIGNER_H_
#define BASEGAME_COMMON_UTIL_NUMERIC_UNIQUEIDASSIGNER_H_

#include <cstdint>
#include <mutex>

#include "../../update/RascUpdatable.h"

namespace rasc {

    /**
     * This is a very simple rasc updatable class, which allows easy assignment of unique ids.
     * This class contains initial data generation and receiving, allowing ids to remain unique,
     * even after loading and saving the game.
     *
     * Note: This class will only generate initial data for save operations, so instances of this
     *       should only exist on the server.
     */
    class UniqueIdAssigner : public RascUpdatable {
    public:
        /**
         * This ID (which is the largest possible ID able to fit into a 64 bit int),
         * should be used as an invalid (dummy) ID value. This ID will never be returned
         * by the UniqueIdAssigner (at least in any reasonable circumstance).
         */
        const static uint64_t INVALID_ID;

    private:
        std::mutex mutex;
        uint64_t idUpTo;
    public:
        UniqueIdAssigner(void);
        virtual ~UniqueIdAssigner(void);

        /**
         * Generates a new unique id.
         */
        uint64_t newId(void);

    protected:
        virtual void onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) override;
        virtual bool onReceiveInitialData(simplenetwork::Message & msg) override;
    };
}

#endif

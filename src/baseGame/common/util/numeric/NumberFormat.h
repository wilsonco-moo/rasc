/*
 * NumberFormat.h
 *
 *  Created on: 30 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_NUMERIC_NUMBERFORMAT_H_
#define BASEGAME_COMMON_UTIL_NUMERIC_NUMBERFORMAT_H_

#include <cstdint>

#include "../../gameMap/provinceUtil/ProvinceStatTypes.h"

namespace rasc {
    class ProvStatModSet;
    class ProvStatMod;
    
    /**
     * This class provides static methods for formatting numbers of various types. The
     * results are written to a character buffer. To provide consistent and
     * maintainable number formatting, wherever numbers are displayed these formats
     * should be used. Likewise, a method should be provided for each type of displayed
     * number in order for re-use elsewhere.
     * 
     * Most formats internally fall back to appropriate K-shortened numbers, in order
     * to avoid displaying too many digits.
     * 
     * Number formatting methods here take two types:
     *  > provstat_t:     This is a regular number used by the stat system. Some formatting
     *                    methods take multiple of these.
     *  > ProvStatMod:    This represents a number and percentage modifier. Both number
     *                    and percentage are only typically shown when necessary.
     * 
     * Most methods take four parameters: (for safe templated versions, the size is a template parameter).
     *  > buffer:     A character buffer of size at least "size". The size INCLUDES any null
     *                terminator.
     *  > size:       Size of the provided buffer, INCLUDING the null terminator. The minimum
     *                buffer size is specified for each method.
     *  > value:      The provstat_t or ProvStatMod value to convert to a string.
     *  > prefixPlus: Pass true here to force a "+" symbol to be prefixed to positive numbers.
     * 
     * Modifier methods take an additional parameter which is by default false: alwaysShowBoth.
     * When this is set to true, both value and percentage are always shown even if zero.
     * 
     * The documentation for each method specifies a minimum buffer size - buffer sizes
     * below this will result in undefined behaviour. It is recommended to use the safer
     * templated variants of the conversion functions, as these use a static assert to ensure
     * buffer sizes are not too small.
     *
     * All methods return the number of characters actually written (NOT INCLUDING the null
     * terminator).
     */
    class NumberFormat {
    public:
        /**
         * This should be used for the display of generic integer values without any specific
         * type - for example the count of something (like turns, players, etc).
         * Note that this does not support plus prefix.
         * 
         * If you are considering using this for a number with some meaning, please consider adding
         * a new number format (even if it just internally uses this one), to allow future flexibility.
         */
        static unsigned int generic(char * buffer, unsigned int size, uint64_t value);
        static constexpr unsigned int GENERIC_MIN = 5;
        
        /**
         * This should be used for the display of generic signed integer values without
         * any specific type, for example the change in the count of something.
         * 
         * If you are considering using this for a number with some meaning, please consider adding
         * a new number format (even if it just internally uses this one), to allow future flexibility.
         */
        static unsigned int genericSigned(char * buffer, unsigned int size, provstat_t value, bool prefixPlus);
        static constexpr unsigned int GENERIC_SIGNED_MIN = 6;
        
        /**
         * Displays generic signed modifier, showing a generic signed integer and a percentage.
         * This should be used for stat modifiers, which are not of any specific type.
         */
        static unsigned int genericSignedModifier(char * buffer, unsigned int size, const ProvStatMod & value, bool prefixPlus, bool alwaysShowBoth = false);
        static constexpr unsigned int GENERIC_SIGNED_MODIFIER_MIN = 15;
        
        /**
         * Displays currency, showing hundredths where possible.
         */
        static unsigned int currency(char * buffer, unsigned int size, provstat_t value, bool prefixPlus);
        static constexpr unsigned int CURRENCY_MIN = 6;
        
        /**
         * Displays currency modifier, showing hundredths where possible.
         */
        static unsigned int currencyModifier(char * buffer, unsigned int size, const ProvStatMod & value, bool prefixPlus, bool alwaysShowBoth = false);
        static constexpr unsigned int CURRENCY_MODIFIER_MIN = 15;
        
        /**
         * Displays manpower.
         */
        static inline unsigned int manpower(char * buffer, unsigned int size, provstat_t value, bool prefixPlus) {
            return genericSigned(buffer, size, value, prefixPlus);
        }
        static constexpr unsigned int MANPOWER_MIN = GENERIC_SIGNED_MIN;
        
        /**
         * Displays manpower modifier.
         */
        static inline unsigned int manpowerModifier(char * buffer, unsigned int size, const ProvStatMod & value, bool prefixPlus, bool alwaysShowBoth = false) {
            return genericSignedModifier(buffer, size, value, prefixPlus, alwaysShowBoth);
        }
        static constexpr unsigned int MANPOWER_MODIFIER_MIN = GENERIC_SIGNED_MODIFIER_MIN;
        
        /**
         * Displays province value.
         */
        static inline unsigned int provinceValue(char * buffer, unsigned int size, provstat_t value, bool prefixPlus) {
            return genericSigned(buffer, size, value, prefixPlus);
        }
        static constexpr unsigned int PROVINCE_VALUE_MIN = GENERIC_SIGNED_MIN;
        
        /**
         * Displays province value modifier.
         */
        static inline unsigned int provinceValueModifier(char * buffer, unsigned int size, const ProvStatMod & value, bool prefixPlus, bool alwaysShowBoth = false) {
            return genericSignedModifier(buffer, size, value, prefixPlus, alwaysShowBoth);
        }
        static constexpr unsigned int PROVINCE_VALUE_MODIFIER_MIN = GENERIC_SIGNED_MODIFIER_MIN;
        
        /**
         * Displays land.
         */
        static inline unsigned int land(char * buffer, unsigned int size, provstat_t value, bool prefixPlus) {
            return genericSigned(buffer, size, value, prefixPlus);
        }
        static constexpr unsigned int LAND_MIN = GENERIC_SIGNED_MIN;
        
        /**
         * Displays land modifier.
         */
        static inline unsigned int landModifier(char * buffer, unsigned int size, const ProvStatMod & value, bool prefixPlus, bool alwaysShowBoth = false) {
            return genericSignedModifier(buffer, size, value, prefixPlus, alwaysShowBoth);
        }
        static constexpr unsigned int LAND_MODIFIER_MIN = GENERIC_SIGNED_MODIFIER_MIN;
        
        /**
         * Displays usage and availability, like "14/36". Note that prefix plus doesn't make sense here.
         */
        static unsigned int landUseAvailable(char * buffer, unsigned int size, provstat_t use, provstat_t available);
        static constexpr unsigned int LAND_USE_AVAILABLE_MIN = 12;
        
        /**
         * Displays a percentage value, the percentage symbol is always shown.
         */
        static unsigned int percentage(char * buffer, unsigned int size, provstat_t value, bool prefixPlus);
        static constexpr unsigned int PERCENTAGE_MIN = 7;
        
        /**
         * Displays a percentage value shown to one decimal place (where possible), specified as a permille value,
         * (i.e: 345 -> 34.5%). The percentage symbol is always shown.
         */
        static unsigned int permille(char * buffer, unsigned int size, provstat_t value, bool prefixPlus);
        static constexpr unsigned int PERMILLE_MIN = 7;
        
        /**
         * Displays army unit count.
         */
        static inline unsigned int armyUnits(char * buffer, unsigned int size, uint64_t value) {
            return generic(buffer, size, value);
        }
        static constexpr unsigned int ARMY_UNITS_MIN = 5;
        
        
        /**
         * Safer variants of functions. These should be preferred where possible, as they take the
         * size as a compile time constant, and include static asserts to make sure the buffer size is
         * not too small.
         */
        
        template<unsigned int size>
        static inline unsigned int generic(char * buffer, uint64_t value) {
            static_assert(size >= GENERIC_MIN, "Generic buffer size too small");
            return generic(buffer, size, value);
        }
        
        template<unsigned int size>
        static inline unsigned int genericSigned(char * buffer, provstat_t value, bool prefixPlus) {
            static_assert(size >= GENERIC_SIGNED_MIN, "Generic signed buffer size too small");
            return genericSigned(buffer, size, value, prefixPlus);
        }
        
        template<unsigned int size>
        static inline unsigned int genericSignedModifier(char * buffer, const ProvStatMod & value, bool prefixPlus, bool alwaysShowBoth = false) {
            static_assert(size >= GENERIC_SIGNED_MODIFIER_MIN, "Generic signed modifier buffer size too small");
            return genericSignedModifier(buffer, size, value, prefixPlus, alwaysShowBoth);
        }
        
        template<unsigned int size>
        static inline unsigned int currency(char * buffer, provstat_t value, bool prefixPlus) {
            static_assert(size >= CURRENCY_MIN, "Currency buffer size too small");
            return currency(buffer, size, value, prefixPlus);
        }
        
        template<unsigned int size>
        static inline unsigned int currencyModifier(char * buffer, const ProvStatMod & value, bool prefixPlus, bool alwaysShowBoth = false) {
            static_assert(size >= CURRENCY_MODIFIER_MIN, "Currency modifier buffer size too small");
            return currencyModifier(buffer, size, value, prefixPlus, alwaysShowBoth);
        }

        template<unsigned int size>
        static inline unsigned int manpower(char * buffer, provstat_t value, bool prefixPlus) {
            static_assert(size >= MANPOWER_MIN, "Manpower buffer size too small");
            return manpower(buffer, size, value, prefixPlus);
        }
        
        template<unsigned int size>
        static inline unsigned int manpowerModifier(char * buffer, const ProvStatMod & value, bool prefixPlus, bool alwaysShowBoth = false) {
            static_assert(size >= MANPOWER_MODIFIER_MIN, "Manpower modifier buffer size too small");
            return manpowerModifier(buffer, size, value, prefixPlus, alwaysShowBoth);
        }
        
        template<unsigned int size>
        static inline unsigned int provinceValue(char * buffer, provstat_t value, bool prefixPlus) {
            static_assert(size >= PROVINCE_VALUE_MIN, "Province value buffer size too small");
            return provinceValue(buffer, size, value, prefixPlus);
        }
        
        template<unsigned int size>
        static inline unsigned int provinceValueModifier(char * buffer, const ProvStatMod & value, bool prefixPlus, bool alwaysShowBoth = false) {
            static_assert(size >= PROVINCE_VALUE_MODIFIER_MIN, "Province value modifier buffer size too small");
            return provinceValueModifier(buffer, size, value, prefixPlus, alwaysShowBoth);
        }
        
        template<unsigned int size>
        static inline unsigned int land(char * buffer, provstat_t value, bool prefixPlus) {
            static_assert(size >= LAND_MIN, "Land buffer size too small");
            return land(buffer, size, value, prefixPlus);
        }
        
        template<unsigned int size>
        static inline unsigned int landModifier(char * buffer, const ProvStatMod & value, bool prefixPlus, bool alwaysShowBoth = false) {
            static_assert(size >= LAND_MODIFIER_MIN, "Land modifier buffer size too small");
            return landModifier(buffer, size, value, prefixPlus, alwaysShowBoth);
        }
        
        template<unsigned int size>
        static inline unsigned int landUseAvailable(char * buffer, provstat_t use, provstat_t available) {
            static_assert(size >= LAND_USE_AVAILABLE_MIN, "Land use available buffer size too small");
            return landUseAvailable(buffer, size, use, available);
        }
        
        template<unsigned int size>
        static inline unsigned int percentage(char * buffer, provstat_t value, bool prefixPlus) {
            static_assert(size >= PERCENTAGE_MIN, "Percentage buffer size too small");
            return percentage(buffer, size, value, prefixPlus);
        }
        
        template<unsigned int size>
        static inline unsigned int permille(char * buffer, provstat_t value, bool prefixPlus) {
            static_assert(size >= PERMILLE_MIN, "Permille buffer size too small");
            return permille(buffer, size, value, prefixPlus);
        }
        
        template<unsigned int size>
        static inline unsigned int armyUnits(char * buffer, uint64_t value) {
            static_assert(size >= ARMY_UNITS_MIN, "Army units buffer size too small");
            return armyUnits(buffer, size, value);
        }
        
        
        /**
         * Formatting helper macro which declares a fixed character array, then populates
         * it with a text prefix and a value formatted with one of the NumberFormat methods.
         * The size of the fixed character array is worked out using Misc's constexpr string
         * length function.
         * To use this, include Misc.h and cstring.
         */
        #define NUMBER_FORMAT_DECL(bufferName, formatFunc, bufferSize, text, ...)                 \
            constexpr unsigned int bufferName##_textLen = Misc::constStrlen(text);                \
            char bufferName[bufferName##_textLen + (bufferSize)];                                 \
            std::memcpy(bufferName, text, bufferName##_textLen);                                  \
            NumberFormat::formatFunc<bufferSize>(bufferName + bufferName##_textLen, __VA_ARGS__);
        
        /**
         * Formatting helper macro which populates a character array with a text prefix, then
         * a value formatted with one of the NumberFormat methods. This macro includes static
         * asserts to ensure that the character array is large enough. This is known by working
         * out the size of the prefix text, using Misc's constexpr string length function.
         * To use this, include Misc.h, cstring and type_traits.
         */
        #define NUMBER_FORMAT_FILL(bufferName, formatFunc, bufferSize, text, ...) {                                                         \
            /* Check array properties.                                                                                                   */ \
            static_assert(std::is_array<decltype(bufferName)>::value, "Buffer must be an array.");                                          \
            static_assert(std::is_same<std::remove_all_extents<decltype(bufferName)>::type, char>::value, "Buffer must be a char array.");  \
            static_assert(std::extent<decltype(bufferName)>::value >= Misc::constStrlen(text) + bufferSize, "Array must be large enough."); \
                                                                                                                                            \
            /* Copy string data, add formatted number.                                                                                   */ \
            constexpr unsigned int bufferName##_textLen = Misc::constStrlen(text);                                                          \
            std::memcpy(bufferName, text, bufferName##_textLen);                                                                            \
            NumberFormat::formatFunc<bufferSize>(bufferName + bufferName##_textLen, __VA_ARGS__);                                           \
        }
    };
}

#endif

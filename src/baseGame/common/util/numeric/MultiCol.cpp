/*
 * MultiCol.cpp
 *
 *  Created on: 3 Feb 2020
 *      Author: wilson
 */

#include "MultiCol.h"

#include <cstdlib>
#include <cstring>
#include <cmath>
#include <array>

#include "Random.h"

namespace rasc {

    // For converting between modes and their string representations.
    #define X(name, friendlyName, ...) friendlyName,
    const static std::array<const char *, MultiCol::ColourBlindMode::COUNT> colourBlindModeNames = { MULTI_COL_COLOUR_BLIND_MODE_LIST };
    #undef X
    
    /**
     * These values can be experimentally found with the following simple program:
     *
     *  float minL = std::numeric_limits<float>::max(), minA = std::numeric_limits<float>::max(), minB = std::numeric_limits<float>::max(),
     *        maxL = std::numeric_limits<float>::min(), maxA = std::numeric_limits<float>::min(), maxB = std::numeric_limits<float>::min();
     *
     *  for (unsigned int red = 0; red < 256; red++) {
     *      for (unsigned int green = 0; green < 256; green++) {
     *          for (unsigned int blue = 0; blue < 256; blue++) {
     *
     *              rasc::MultiCol mcol(red, green, blue);
     *              mcol.rgbToXyz();
     *              mcol.xyzToLab();
     *
     *              minL = std::min(minL, mcol.l()); minA = std::min(minA, mcol.a()); minB = std::min(minB, mcol.b());
     *              maxL = std::max(maxL, mcol.l()); maxA = std::max(maxA, mcol.a()); maxB = std::max(maxB, mcol.b());
     *          }
     *      }
     *      std::cout << "Progress: " << red << " of 255...\n";
     *  }
     *
     *  std::cout << std::setprecision(16) <<
     *                 "L: MIN: " << minL << " SIZE: " << (maxL - minL) <<
     *               "\nA: MIN: " << minA << " SIZE: " << (maxA - minA) <<
     *               "\nB: MIN: " << minB << " SIZE: " << (maxB - minB) << '\n';
     */
    const GLfloat MultiCol::LAB_MIN_L = 0.0f,
                  MultiCol::LAB_L_SIZE = 100.0f,
                  MultiCol::LAB_MIN_A = -86.18462371826172f,
                  MultiCol::LAB_A_SIZE = 184.4388580322266f,
                  MultiCol::LAB_MIN_B = -107.8636856079102f,
                  MultiCol::LAB_B_SIZE = 202.3461608886719f;

    MultiCol::MultiCol(void) :
        values {
            0.0f, 0.0f, 0.0f
        } {
    }

    MultiCol::MultiCol(uint32_t intColour) :
        values {
            (GLfloat)((intColour >> 24) & 255), // r
            (GLfloat)((intColour >> 16) & 255), // g
            (GLfloat)((intColour >>  8) & 255)  // b
        } {
    }
    MultiCol::MultiCol(GLfloat r, GLfloat g, GLfloat b) :
        values {
            r, g, b
        } {
    }
    MultiCol & MultiCol::operator = (uint32_t intColour) {
        values[0] = (intColour >> 24) & 255; // r
        values[1] = (intColour >> 16) & 255; // g
        values[2] = (intColour >>  8) & 255; // b
        return *this;
    }

    bool MultiCol::isWithinRangeRGB(void) const {
        return r() >= 0.0f && r() <= 255.0f &&
               g() >= 0.0f && g() <= 255.0f &&
               b() >= 0.0f && b() <= 255.0f;
    }

    uint32_t MultiCol::toIntRGBA(void) const {
        return (((uint32_t)std::roundf(r())) << 24) | // r
               (((uint32_t)std::roundf(g())) << 16) | // g
               (((uint32_t)std::roundf(b())) <<  8) | // b
               255; // Alpha 255 for opaque      // a
    }

    void MultiCol::rgbToXyz(void) {
        // Source: http://www.easyrgb.com/en/math.php
        //         http://web.archive.org/web/20191208180824/http://www.easyrgb.com/en/math.php

        GLfloat varR = r() / 255.0,
                varG = g() / 255.0,
                varB = b() / 255.0;

        if (varR > 0.04045) {
            varR = std::pow((varR + 0.055) / 1.055, 2.4);
        } else {
            varR = varR / 12.92;
        }

        if (varG > 0.04045) {
            varG = std::pow((varG + 0.055) / 1.055, 2.4);
        } else {
            varG = varG / 12.92;
        }

        if (varB > 0.04045) {
            varB = std::pow((varB + 0.055) / 1.055, 2.4);
        } else {
            varB = varB / 12.92;
        }

        varR *= 100.0;
        varG *= 100.0;
        varB *= 100.0;

        x() = (varR * 0.4124) + (varG * 0.3576) + (varB * 0.1805);
        y() = (varR * 0.2126) + (varG * 0.7152) + (varB * 0.0722);
        z() = (varR * 0.0193) + (varG * 0.1192) + (varB * 0.9505);
    }

    void MultiCol::xyzToLab(void) {
        // Source: http://www.easyrgb.com/en/math.php
        //         http://web.archive.org/web/20191208180824/http://www.easyrgb.com/en/math.php

        // Divide by reference X,Y,Z: (D65, Daylight, sRGB, Adobe-RGB)
        GLfloat varX = x() / 95.047,
                varY = y() / 100.0,
                varZ = z() / 108.883;

        if (varX > 0.008856) {
            varX = std::pow(varX, 1.0/3.0);
        } else {
            varX = (7.787 * varX) + (16.0/116.0);
        }

        if (varY > 0.008856) {
            varY = std::pow(varY, 1.0/3.0);
        } else {
            varY = (7.787 * varY) + (16.0/116.0);
        }

        if (varZ > 0.008856) {
            varZ = std::pow(varZ, 1.0/3.0);
        } else {
            varZ = (7.787 * varZ) + (16.0/116.0);
        }

        l() = (116.0 * varY) - 16.0;
        a() = 500.0 * (varX - varY);
        b() = 200.0 * (varY - varZ);
    }

    void MultiCol::labToXyz(void) {
        // Source: http://www.easyrgb.com/en/math.php
        //         http://web.archive.org/web/20191208180824/http://www.easyrgb.com/en/math.php

        GLfloat varY = (l() + 16.0) / 116.0,
                varX = (a() / 500.0) + varY,
                varZ = varY - (b() / 200);

        {
            GLfloat varYCubed = varY * varY * varY,
                    varXCubed = varX * varX * varX,
                    varZCubed = varZ * varZ * varZ;

            if (varYCubed > 0.008856) {
                varY = varYCubed;
            } else {
                varY = (varY - (16.0/116.0)) / 7.787;
            }

            if (varXCubed > 0.008856) {
                varX = varXCubed;
            } else {
                varX = (varX - (16.0/116.0)) / 7.787;
            }

            if (varZCubed > 0.008856) {
                varZ = varZCubed;
            } else {
                varZ = (varZ - (16.0/116.0)) / 7.787;
            }
        }

        // Multiply by reference X,Y,Z: (D65, Daylight, sRGB, Adobe-RGB)
        x() = varX * 95.047;
        y() = varY * 100.0;
        z() = varZ * 108.883;
    }

    void MultiCol::xyzToRgb(void) {
        // Source: http://www.easyrgb.com/en/math.php
        //         http://web.archive.org/web/20191208180824/http://www.easyrgb.com/en/math.php

        GLfloat var_X = x() / 100.0,
                var_Y = y() / 100.0,
                var_Z = z() / 100.0,

                var_R = (var_X *  3.2406) + (var_Y * -1.5372) + (var_Z * -0.4986),
                var_G = (var_X * -0.9689) + (var_Y *  1.8758) + (var_Z *  0.0415),
                var_B = (var_X *  0.0557) + (var_Y * -0.2040) + (var_Z *  1.0570);

        if (var_R > 0.0031308) {
            var_R = (1.055 * std::pow(var_R, 1.0/2.4)) - 0.055;
        } else {
            var_R = 12.92 * var_R;
        }

        if (var_G > 0.0031308) {
            var_G = (1.055 * std::pow(var_G, 1.0/2.4)) - 0.055;
        } else {
            var_G = 12.92 * var_G;
        }

        if (var_B > 0.0031308) {
            var_B = (1.055 * std::pow(var_B, 1.0/2.4)) - 0.055;
        } else {
            var_B = 12.92 * var_B;
        }

        r() = var_R * 255.0;
        g() = var_G * 255.0;
        b() = var_B * 255.0;
    }

    GLfloat MultiCol::distance(const MultiCol & other) const {
        GLfloat d0 = r() - other.r(),
                d1 = g() - other.g(),
                d2 = b() - other.b();
        return std::sqrt((d0 * d0) + (d1 * d1) + (d2 * d2));
    }

    unsigned int MultiCol::distanceSquared(const MultiCol & other, unsigned int colourBlindMode) const {
        // Note: We FIRST independently round each channel to the nearest
        // whole number, before doing comparisons and multiplications.
        // Doing the whole thing in integer arithmetic guarantees commutativity.
        // (Note: First convert to int as channels can be negative!)
        switch (colourBlindMode) {
            // For red-green mode, only care about 'l' and 'b' channels (ignore 'a'),
            // as thats difference between red and green in LAB.
            case ColourBlindMode::redGreen: {
                int d0 = ((int)std::roundf(l())) - ((int)std::roundf(other.l())),
                    d1 = ((int)std::roundf(b())) - ((int)std::roundf(other.b()));
                return (unsigned int)((d0 * d0) + (d1 * d1));
            }

            // Normally, compare and sum distance for all channels.
            default: {
                int d0 = ((int)std::roundf(l())) - ((int)std::roundf(other.l())),
                    d1 = ((int)std::roundf(a())) - ((int)std::roundf(other.a())),
                    d2 = ((int)std::roundf(b())) - ((int)std::roundf(other.b()));
                return (unsigned int)((d0 * d0) + (d1 * d1) + (d2 * d2));
            }
        }
    }

    MultiCol MultiCol::randomLabColour(Random & random) {
        return MultiCol(
            LAB_MIN_L + random.glfloat01() * LAB_L_SIZE, // l
            LAB_MIN_A + random.glfloat01() * LAB_A_SIZE, // a
            LAB_MIN_B + random.glfloat01() * LAB_B_SIZE  // b
        );
    }
    
    MultiCol MultiCol::randomLabColourWithValidRGB(Random & random) {
        MultiCol labCol, rgbCol;
        do {
            labCol = rgbCol = randomLabColour(random);
            rgbCol.labToXyz();
            rgbCol.xyzToRgb();
        } while(!rgbCol.isWithinRangeRGB());
        return labCol;
    }
    
    MultiCol MultiCol::perceptUniformRandomCol(Random & random) {
        MultiCol col;
        do {
            col = randomLabColour(random);
            col.labToXyz();
            col.xyzToRgb();
        } while(!col.isWithinRangeRGB());
        return col;
    }

    uint32_t MultiCol::perceptUniformRandomColInt(Random & random) {
        return perceptUniformRandomCol(random).toIntRGBA();
    }

    MultiCol MultiCol::randomColRGB(Random & random) {
        return MultiCol(
            random.glfloatRange(0.0f, 255.0f), // r
            random.glfloatRange(0.0f, 255.0f), // g
            random.glfloatRange(0.0f, 255.0f)  // b
        );
    }

    uint32_t MultiCol::randomColRGBInt(Random & random) {
        return (random.uintRange(0, 256) << 24u) | // r
               (random.uintRange(0, 256) << 16u) | // g
               (random.uintRange(0, 256) <<  8u) | // b
               255u; // Opaque alpha               // a
    }

    unsigned int MultiCol::linearRgbColDist(uint32_t col1, uint32_t col2) {
        int comp1, comp2;
        unsigned int diff, n;

        comp1 = (int)((col1 >> 24) & 255);
        comp2 = (int)((col2 >> 24) & 255);
        diff = std::abs(comp1 - comp2);

        comp1 = (int)((col1 >> 16) & 255);
        comp2 = (int)((col2 >> 16) & 255);
        n = std::abs(comp1 - comp2);
        if (n > diff) diff = n;

        comp1 = (int)((col1 >> 8) & 255);
        comp2 = (int)((col2 >> 8) & 255);
        n = std::abs(comp1 - comp2);
        if (n > diff) return n; else return diff;
    }

    GLfloat MultiCol::perceptUniformRgbColDist(uint32_t col1, uint32_t col2) {
        MultiCol mcol1(col1), mcol2(col2);
        mcol1.rgbToXyz(); mcol1.xyzToLab();
        mcol2.rgbToXyz(); mcol2.xyzToLab();
        return mcol1.distance(mcol2);
    }

    unsigned int MultiCol::perceptUniformRgbColDistSquared(uint32_t col1, uint32_t col2, unsigned int colourBlindMode) {
        MultiCol mcol1(col1), mcol2(col2);
        mcol1.rgbToXyz(); mcol1.xyzToLab();
        mcol2.rgbToXyz(); mcol2.xyzToLab();
        return mcol1.distanceSquared(mcol2, colourBlindMode);
    }
    
    const char * MultiCol::colourBlindModeToFriendlyName(unsigned int colourBlindMode) {
        return colourBlindModeNames[colourBlindMode];
    }
    
    unsigned int MultiCol::friendlyNameToColourBlindMode(const char * friendlyName) {
        // Just search the array of names and do a string compare of each,
        // there aren't that many of them!
        for (unsigned int modeIndex = 0; modeIndex < ColourBlindMode::COUNT; modeIndex++) {
            if (std::strcmp(friendlyName, colourBlindModeNames[modeIndex]) == 0) {
                return modeIndex;
            }
        }
        return ColourBlindMode::none;
    }
}

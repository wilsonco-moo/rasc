/*
 * UniqueIdAssigner.cpp
 *
 *  Created on: 7 Mar 2019
 *      Author: wilson
 */

#include "UniqueIdAssigner.h"

#include <sockets/plus/message/Message.h>

namespace rasc {

    const uint64_t UniqueIdAssigner::INVALID_ID = (uint64_t)-1;

    UniqueIdAssigner::UniqueIdAssigner(void) :
        RascUpdatable(),
        mutex(),
        idUpTo(0) {
    }

    UniqueIdAssigner::~UniqueIdAssigner(void) {
    }

    uint64_t UniqueIdAssigner::newId(void) {
        std::lock_guard<std::mutex> lock(mutex);
        return idUpTo++;
    }

    void UniqueIdAssigner::onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) {
        if (isSaving) {
            std::lock_guard<std::mutex> lock(mutex);
            writeUpdateHeader(msg);
            msg.write64(idUpTo);
        }
    }

    bool UniqueIdAssigner::onReceiveInitialData(simplenetwork::Message & msg) {
        std::lock_guard<std::mutex> lock(mutex);
        idUpTo = msg.read64();
        return true;
    }
}

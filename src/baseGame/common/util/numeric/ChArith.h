/*
 * CheckedArithmetic.h
 *
 *  Created on: 13 Mar 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_NUMERIC_CHARITH_H_
#define BASEGAME_COMMON_UTIL_NUMERIC_CHARITH_H_

#include <limits>

namespace rasc {

    /**
     * This header file: ChArith (checked arithmetic) contains a series of methods, designed for performing
     * signed integer arithmetic in a robust way. Each of these methods:
     *  > First checks to see if the operation is possible. If the operation is not possible
     *    due to integer overflow, or dividing by zero, then:
     *    - The error flag is set to true.
     *    - Zero is returned (a placeholder value).
     *  > Otherwise, the operation is performed, the result is returned. The error flag is left
     *    unchanged.
     *
     * Because these methods return a result, and set errors as a flag, they can be chained like
     * the following. This avoids the need to check errors for every single operation. Rather,
     * the errors can be checked ONCE at the end.
     *
     *     int a = ...,
     *         b = ...,
     *         c = ...;
     *     bool error = false;
     *     int result = ChArith::subtract(&error, ChArith::multiply(&error, a, b), c);
     *     if (error) { ... }
     *
     * All of these methods are adapted from the following:
     *            https://wiki.sei.cmu.edu/confluence/display/c/INT32-C.+Ensure+that+operations+on+signed+integers+do+not+result+in+overflow
     * (archive)  https://web.archive.org/web/20191210022641/https://wiki.sei.cmu.edu/confluence/display/c/INT32-C.+Ensure+that+operations+on+signed+integers+do+not+result+in+overflow
     */
    class ChArith {
    public:

        /**
         * Tries to add a to b.
         * If this would result in an overflow, then the error flag is set to true, and zero is returned.
         * Otherwise, the operation is performed and returned, and the error flag is left unchanged.
         */
        template<typename T>
        static T add(bool * error, T a, T b) {
            if (((b > 0) && (a > (std::numeric_limits<T>::max() - b))) ||
                ((b < 0) && (a < (std::numeric_limits<T>::min() - b)))) {
                *error = true;
                return 0;
            } else {
                return a + b;
            }
        }

        /**
         * Tries to subtract b from a.
         * If this would result in an overflow, then the error flag is set to true, and zero is returned.
         * Otherwise, the operation is performed and returned, and the error flag is left unchanged.
         */
        template<typename T>
        static T subtract(bool * error, T a, T b) {
            if ((b > 0 && a < std::numeric_limits<T>::min() + b) ||
                (b < 0 && a > std::numeric_limits<T>::max() + b)) {
                *error = true;
                return 0;
            } else {
                return a - b;
            }
        }

        /**
         * Tries to multiply a and b.
         * If this would result in an overflow, then the error flag is set to true, and zero is returned.
         * Otherwise, the operation is performed and returned, and the error flag is left unchanged.
         */
        template<typename T>
        static T multiply(bool * error, T a, T b) {

            // a is positive
            if (a > 0) {
                // a and b are positive
                if (b > 0) {
                    // Handle error
                    if (a > (std::numeric_limits<T>::max() / b)) {
                        *error = true;
                        return 0;
                    }

                // a positive, b nonpositive
                } else {
                    // Handle error
                    if (b < (std::numeric_limits<T>::min() / a)) {
                        *error = true;
                        return 0;
                    }
                }

            // a is nonpositive
            } else {
                // a is nonpositive, b is positive
                if (b > 0) {
                    // Handle error
                    if (a < (std::numeric_limits<T>::min() / b)) {
                        *error = true;
                        return 0;
                    }

                // a and b are nonpositive
                } else {
                    // Handle error
                    if ((a != 0) && (b < (std::numeric_limits<T>::max() / a))) {
                        *error = true;
                        return 0;
                    }
                }
            }

            // If no errors encountered, work out result.
            return a * b;
        }

        /**
         * Tries to divide a by b.
         * If this would result in an overflow or divide by zero, then the error flag is set to true, and zero is returned.
         * Otherwise, the operation is performed and returned, and the error flag is left unchanged.
         */
        template<typename T>
        static T divide(bool * error, T a, T b) {
            if ((b == 0) || ((a == std::numeric_limits<T>::min()) && (b == -1))) {
                *error = true;
                return 0;
            } else {
                return a / b;
            }
        }
    };
}

#endif

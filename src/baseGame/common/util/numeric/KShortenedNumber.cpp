/*
 * KShortenedNumber.cpp
 *
 *  Created on: 14 Feb 2020
 *      Author: wilson
 */

#include "KShortenedNumber.h"

#include <cstdio>

#include "../Misc.h"

namespace rasc {
    
    /**
     * This class defines a suffix and a divisor. To specify decimals,
     * it also stores a number of decimals, a divisor/mod for numbers after
     * the decimal point. This is used as a helper for formatting
     * k shortened and k minimised numbers.
     */
    class FormatDef {
    public:
        const char * suffix;
        unsigned int decimals;
        uint64_t divisor,
                 pointDivisor;
        unsigned int mod;
    };
    
    /**
     * Using the provided array of format definitions, this formats the provided value,
     * writes the results to the buffer, and returns the number of characters written.
     * This is used internally by k shortened and k minimised number methods.
     */
    static unsigned int formatDefFormat(const FormatDef * formatDefs, char * buffer, uint64_t value) {
        // First work out number of digits.
        unsigned int digits = Misc::digitCount(value);

        // Then print depending on the format.
        const FormatDef & formatDef = formatDefs[digits];
        if (formatDef.decimals == 0) {
            return std::sprintf(buffer, "%u%s", (unsigned int)(value / formatDef.divisor), formatDef.suffix);
        } else {
            return std::sprintf(buffer, "%u.%0*u%s", (unsigned int)(value / formatDef.divisor), formatDef.decimals, ((unsigned int)(value / formatDef.pointDivisor)) % formatDef.mod, formatDef.suffix);
        }
    }
    
    /**
     * Format defs for k shortened numbers.
     * 
     *           1 -> 1
     *          13 -> 13
     *         167 -> 167
     *        1535 -> 1.53k
     *       16291 -> 16.2k
     *      127093 -> 127k
     *     1606261 -> 1.60M
     *    18792023 -> 18.7M
     *   175922897 -> 175M
     *         .. etc ..
     */
    const static FormatDef kShortenedFormatDefs[21] = {
        {"",  0u, 1u,                   0u,                  0   },
        {"",  0u, 1u,                   0u,                  0   },
        {"",  0u, 1u,                   0u,                  0   },
        {"",  0u, 1u,                   0u,                  0   },
        {"k", 2u, 1000u,                10u,                 100u},
        {"k", 1u, 1000u,                100u,                10u },
        {"k", 0u, 1000u,                0u,                  0   },
        {"M", 2u, 1000000u,             10000u,              100u},
        {"M", 1u, 1000000u,             100000u,             10u },
        {"M", 0u, 1000000u,             0u,                  0   },
        {"G", 2u, 1000000000u,          10000000u,           100u},
        {"G", 1u, 1000000000u,          100000000u,          10u },
        {"G", 0u, 1000000000u,          0u,                  0   },
        {"T", 2u, 1000000000000u,       10000000000u,        100u},
        {"T", 1u, 1000000000000u,       100000000000u,       10u },
        {"T", 0u, 1000000000000u,       0u,                  0   },
        {"P", 2u, 1000000000000000u,    10000000000000u,     100u},
        {"P", 1u, 1000000000000000u,    100000000000000u,    10u },
        {"P", 0u, 1000000000000000u,    0u,                  0   },
        {"E", 2u, 1000000000000000000u, 10000000000000000u,  100u},
        {"E", 1u, 1000000000000000000u, 100000000000000000u, 10u }
    };

    unsigned int KShortenedNumber::format(char * buffer, uint64_t largeNumber) {
        return formatDefFormat(kShortenedFormatDefs, buffer, largeNumber);
    }

    unsigned int KShortenedNumber::formatSigned(char * buffer, int64_t largeSignedNumber) {
        // For positive numbers just use the regular method.
        if (largeSignedNumber >= 0) {
            return formatDefFormat(kShortenedFormatDefs, buffer, (uint64_t)largeSignedNumber);

        // For negative numbers, use a minus sign as first character, then use regular
        // method for all characters after it.
        } else {
            buffer[0] = '-';
            return formatDefFormat(kShortenedFormatDefs, buffer + 1, (uint64_t)(-largeSignedNumber)) + 1;
        }
    }
    
    /**
     * Format defs for k minimised numbers.
     * 
     *           1 -> 1
     *          13 -> 13
     *         167 -> 167
     *        1535 -> 1.5k
     *       16291 -> 16k
     *      127093 -> 127k
     *     1606261 -> 1.6M
     *    18792023 -> 18M
     *   175922897 -> 175M
     *         .. etc ..
     */
    const static FormatDef kMinimisedFormatDefs[21] = {
        {"",  0u, 1u,                   0u,                  0u },
        {"",  0u, 1u,                   0u,                  0u },
        {"",  0u, 1u,                   0u,                  0u },
        {"",  0u, 1u,                   0u,                  0u },
        {"k", 1u, 1000u,                100u,                10u},
        {"k", 0u, 1000u,                0u,                  0u },
        {"k", 0u, 1000u,                0u,                  0u },
        {"M", 1u, 1000000u,             100000u,             10u},
        {"M", 0u, 1000000u,             0u,                  0u },
        {"M", 0u, 1000000u,             0u,                  0u },
        {"G", 1u, 1000000000u,          100000000u,          10u},
        {"G", 0u, 1000000000u,          0u,                  0u },
        {"G", 0u, 1000000000u,          0u,                  0u },
        {"T", 1u, 1000000000000u,       100000000000u,       10u},
        {"T", 0u, 1000000000000u,       0u,                  0u },
        {"T", 0u, 1000000000000u,       0u,                  0u },
        {"P", 1u, 1000000000000000u,    100000000000000u,    10u},
        {"P", 0u, 1000000000000000u,    0u,                  0u },
        {"P", 0u, 1000000000000000u,    0u,                  0u },
        {"E", 1u, 1000000000000000000u, 100000000000000000u, 10u},
        {"E", 0u, 1000000000000000000u, 0u,                  0u }
    };

    unsigned int KMinimisedNumber::format(char * buffer, uint64_t largeNumber) {
        return formatDefFormat(kMinimisedFormatDefs, buffer, largeNumber);
    }

    unsigned int KMinimisedNumber::formatSigned(char * buffer, int64_t largeSignedNumber) {
        // For positive numbers just use the regular method.
        if (largeSignedNumber >= 0) {
            return formatDefFormat(kMinimisedFormatDefs, buffer, (uint64_t)largeSignedNumber);

        // For negative numbers, use a minus sign as first character, then use regular
        // method for all characters after it.
        } else {
            buffer[0] = '-';
            return formatDefFormat(kMinimisedFormatDefs, buffer + 1, (uint64_t)(-largeSignedNumber)) + 1;
        }
    }
}

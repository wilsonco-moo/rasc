/*
 * KShortenedNumber.h
 *
 *  Created on: 14 Feb 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_NUMERIC_KSHORTENEDNUMBER_H_
#define BASEGAME_COMMON_UTIL_NUMERIC_KSHORTENEDNUMBER_H_

#include <cstddef>
#include <cstdint>

namespace rasc {

    /**
     * KShortenedNumber provides static utility methods and constant values for buffer
     * sizes, for displaying 64 bit numbers in a fixed length representation.
     * KShortenedNumber always shows exactly three significant figures, and allows
     * display in either five or six characters for unsigned and signed numbers respectively,
     * (not including null terminator).
     * 
     * Unsigned numbers are displayed as follows:
     *           1 -> 1
     *          13 -> 13
     *         167 -> 167
     *        1535 -> 1.53k
     *       16291 -> 16.2k
     *      127093 -> 127k
     *     1606261 -> 1.60M
     *    18792023 -> 18.7M
     *   175922897 -> 175M
     * 
     * Signed numbers are displayed as follows:
     *   -16291 -> -16.2k
     *    -1535 -> -1.53k
     *     -167 -> -167
     *      -13 -> -13
     *       -1 -> -1
     *        1 -> 1
     *       13 -> 13
     *      167 -> 167
     *     1535 -> 1.53k
     *    16291 -> 16.2k
     */
    class KShortenedNumber {
    public:

        /**
         * The minimum buffer size for KShortenedNumber::format.
         * Note that this size INCLUDES SPACE TO ACCOMMODATE A NULL TERMINATOR.
         */
        static constexpr size_t SIZE = 6;

        /**
         * The minimum buffer size for KShortenedNumber::formatSigned.
         * Note that this size INCLUDES SPACE TO ACCOMMODATE THE A NULL TERMINATOR.
         * This is one character larger than "SIZE" in order to accommodate a possible
         * minus sign character.
         */
        static constexpr size_t SIZE_SIGNED = SIZE + 1;

        /**
         * Formats a 64 bit number to be displayed to 3 significant figures.
         * This requires a buffer size of at least KShortenedNumber::SIZE.
         * Note that this function automatically adds a null terminator.
         * 
         * Returns the number of characters actually written, (NOT INCLUDING the null
         * terminator, at least one, and at most KShortenedNumber::SIZE - 1).
         */
        static unsigned int format(char * buffer, uint64_t largeNumber);

        /**
         * Formats a 64 bit signed number to be displayed to 3 significant figures.
         * This requires a buffer size of at least KShortenedNumber::SIZE_SIGNED.
         * Note that this function automatically adds a null terminator.
         * 
         * Returns the number of characters actually written, (NOT INCLUDING the null
         * terminator, at least one, and at most KShortenedNumber::SIZE - 1).
         */
        static unsigned int formatSigned(char * buffer, int64_t largeSignedNumber);
    };
    
    
    
    /**
     * KMinimisedNumber provides static utility methods and constant values for buffer
     * sizes, for displaying 64 bit numbers in a fixed length representation.
     * Compared to KShortenedNumber, KMinimisedNumber displays either two or three
     * significant figures, in order to display numbers in a smaller representation.
     * This requires either four or five characters,for unsigned or signed numbers
     * respectively, (not including null terminator).
     * 
     * Unsigned numbers are displayed as follows:
     *           1 -> 1
     *          13 -> 13
     *         167 -> 167
     *        1535 -> 1.5k
     *       16291 -> 16k
     *      127093 -> 127k
     *     1606261 -> 1.6M
     *    18792023 -> 18M
     *   175922897 -> 175M
     * 
     * Signed numbers are displayed as follows:
     *   -16291 -> -16k
     *    -1535 -> -1.5k
     *     -167 -> -167
     *      -13 -> -13
     *       -1 -> -1
     *        1 -> 1
     *       13 -> 13
     *      167 -> 167
     *     1535 -> 1.5k
     *    16291 -> 16k
     */
    class KMinimisedNumber {
    public:

        /**
         * The minimum buffer size for KMinimisedNumber::format.
         * Note that this size INCLUDES SPACE TO ACCOMMODATE A NULL TERMINATOR.
         */
        static constexpr size_t SIZE = 5;

        /**
         * The minimum buffer size for KMinimisedNumber::formatSigned.
         * Note that this size INCLUDES SPACE TO ACCOMMODATE THE A NULL TERMINATOR.
         * This is one character larger than "SIZE" in order to accommodate a possible
         * minus sign character.
         */
        static constexpr size_t SIZE_SIGNED = SIZE + 1;

        /**
         * Formats a 64 bit number to be displayed to 1, 2 or 3 significant figures.
         * This requires a buffer size of at least KMinimisedNumber::SIZE.
         * Note that this function automatically adds a null terminator.
         * 
         * Returns the number of characters actually written, (NOT INCLUDING the null
         * terminator, at least one, and at most KMinimisedNumber::SIZE - 1).
         */
        static unsigned int format(char * buffer, uint64_t largeNumber);

        /**
         * Formats a 64 bit signed number to be displayed to 1, 2 or 3 significant figures.
         * This requires a buffer size of at least KMinimisedNumber::SIZE_SIGNED.
         * Note that this function automatically adds a null terminator.
         * 
         * Returns the number of characters actually written, (NOT INCLUDING the null
         * terminator, at least one, and at most KMinimisedNumber::SIZE - 1).
         */
        static unsigned int formatSigned(char * buffer, int64_t largeSignedNumber);
    };
}

#endif

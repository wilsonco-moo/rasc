/*
 * NumberFormat.cpp
 *
 *  Created on: 30 Jan 2021
 *      Author: wilson
 */

#include "NumberFormat.h"

#include "../../gameMap/provinceUtil/ProvStatMod.h"
#include "KShortenedNumber.h"
#include "../Misc.h"

#include <cinttypes>
#include <algorithm>
#include <cstdio>

namespace rasc {
    
    /**
     * Used for modifier values, where the buffer must be split by size ratio to display two values.
     * Returns the size in character count of the left and right halves of the
     * buffer area, assuming the provided split ratio.
     * One must be subtracted before splitting and added again afterwards,
     * as we are splitting the string characters not the null terminator.
     */
    template<unsigned int leftRatio, unsigned int rightRatio, unsigned int total = leftRatio + rightRatio>
    constexpr static inline unsigned int splitLeft(unsigned int bufferSize) {
        unsigned int charArea = bufferSize - 1;
        return ((leftRatio * charArea) / total) + 1;
    }
    template<unsigned int leftRatio, unsigned int rightRatio, unsigned int total = leftRatio + rightRatio>
    constexpr static inline unsigned int splitRight(unsigned int bufferSize) {
        unsigned int charArea = bufferSize - 1;
        return (charArea - (leftRatio * charArea) / total) + 1;
    }
    
    // Sizes cannot be smaller than k shortened number sizes.
    static_assert(NumberFormat::GENERIC_MIN >= KMinimisedNumber::SIZE, "Size too small.");
    static_assert(NumberFormat::GENERIC_SIGNED_MIN >= KMinimisedNumber::SIZE_SIGNED, "Size too small.");
    static_assert(NumberFormat::GENERIC_SIGNED_MODIFIER_MIN >= KMinimisedNumber::SIZE_SIGNED * 2u, "Size too small.");
    static_assert(NumberFormat::CURRENCY_MIN >= KMinimisedNumber::SIZE_SIGNED, "Size too small.");
    static_assert(NumberFormat::CURRENCY_MODIFIER_MIN >= KMinimisedNumber::SIZE_SIGNED * 2u, "Size too small.");
    static_assert(NumberFormat::LAND_USE_AVAILABLE_MIN >= KMinimisedNumber::SIZE_SIGNED * 2u, "Size too small.");
    static_assert(NumberFormat::PERCENTAGE_MIN >= KMinimisedNumber::SIZE_SIGNED + 1, "Size too small.");
    
    // Split ratios
    constexpr static inline unsigned int genericSignedModifierSplitLeft(unsigned int bufferSize) {
        return splitLeft<NumberFormat::GENERIC_SIGNED_MIN, NumberFormat::PERCENTAGE_MIN>(bufferSize - 3);
    }
    constexpr static inline unsigned int genericSignedModifierSplitRight(unsigned int bufferSize) {
        return splitRight<NumberFormat::GENERIC_SIGNED_MIN, NumberFormat::PERCENTAGE_MIN>(bufferSize - 3);
    }
    
    constexpr static inline unsigned int currencyModifierSplitLeft(unsigned int bufferSize) {
        return splitLeft<NumberFormat::CURRENCY_MIN, NumberFormat::PERCENTAGE_MIN>(bufferSize - 3);
    }
    constexpr static inline unsigned int currencyModifierSplitRight(unsigned int bufferSize) {
        return splitRight<NumberFormat::CURRENCY_MIN, NumberFormat::PERCENTAGE_MIN>(bufferSize - 3);
    }
    
    constexpr static inline unsigned int landUseAvailableSplitLeft(unsigned int bufferSize) {
        return splitLeft<1, 1>(bufferSize - 1);
    }
    constexpr static inline unsigned int landUseAvailableSplitRight(unsigned int bufferSize) {
        return splitRight<1, 1>(bufferSize - 1);
    }
    
    // When using minimum sizes for each modifier, constituent halves must
    // meet minimum requirements for buffer sizes.
    static_assert(genericSignedModifierSplitLeft(NumberFormat::GENERIC_SIGNED_MODIFIER_MIN) >= NumberFormat::GENERIC_SIGNED_MIN, "Ratio wrong");
    static_assert(genericSignedModifierSplitRight(NumberFormat::GENERIC_SIGNED_MODIFIER_MIN) >= NumberFormat::PERCENTAGE_MIN, "Ratio wrong");
    
    static_assert(currencyModifierSplitLeft(NumberFormat::CURRENCY_MODIFIER_MIN) >= NumberFormat::CURRENCY_MIN, "Ratio wrong");
    static_assert(currencyModifierSplitRight(NumberFormat::CURRENCY_MODIFIER_MIN) >= NumberFormat::PERCENTAGE_MIN, "Ratio wrong");
    
    static_assert(landUseAvailableSplitLeft(NumberFormat::LAND_USE_AVAILABLE_MIN) >= NumberFormat::LAND_MIN, "Ratio wrong");
    static_assert(landUseAvailableSplitRight(NumberFormat::LAND_USE_AVAILABLE_MIN) >= NumberFormat::LAND_MIN, "Ratio wrong");
    
    
    unsigned int NumberFormat::generic(char * buffer, unsigned int size, uint64_t value) {
        // Work out digits.
        unsigned int digits = Misc::digitCount(value);
        
        // Use regular decimal format if possible.
        if (digits < size) {
            return std::sprintf(buffer, "%" PRIu64, value);
        }
        
        // Otherwise use k shortened
        if (KShortenedNumber::SIZE <= size) {
            return KShortenedNumber::format(buffer, value);
        }
        
        // Otherwise fall back to k minimised.
        return KMinimisedNumber::format(buffer, value);
    }
    
    // Used in generic and currency formats: provides a fallback, taking care of modifiers correctly.
    static unsigned int kShortenedFallback(char * buffer, unsigned int size, provstat_t value, uint64_t posVal, bool prefixPlus) {
        // Try positive k shortened.
        if (value >= 0 && !prefixPlus && KShortenedNumber::SIZE <= size) {
            return KShortenedNumber::format(buffer, posVal);
        }
        
        // Try signed k shortened (taking care of modifiers).
        if (KShortenedNumber::SIZE_SIGNED <= size) {
            if (prefixPlus && value >= 0) {
                buffer[0] = '+';
                return KShortenedNumber::format(buffer + 1, posVal) + 1;
            } else {
                return KShortenedNumber::formatSigned(buffer, value);
            }
        }
        
        // Otherwise fall back to k minimised.
        if (prefixPlus && value >= 0) {
            buffer[0] = '+';
            return KMinimisedNumber::format(buffer + 1, posVal) + 1;
        } else {
            return KMinimisedNumber::formatSigned(buffer, value);
        }
    }
    
    unsigned int NumberFormat::genericSigned(char * buffer, unsigned int size, provstat_t value, bool prefixPlus) {
        // Work out digits.
        uint64_t posVal = std::abs(value);
        unsigned int digits = Misc::digitCount(posVal);
        if (value < 0 || prefixPlus) digits++;
        
        // Use regular decimal format if possible.
        if (digits < size) {
            const char * prefix = (value < 0 ? "-" : (prefixPlus ? "+" : ""));
            return std::sprintf(buffer, "%s%" PRIu64, prefix, posVal);
        }
        
        // Otherwise fall back to k shortened.
        return kShortenedFallback(buffer, size, value, posVal, prefixPlus);
    }
    
    unsigned int NumberFormat::genericSignedModifier(char * buffer, unsigned int size, const ProvStatMod & value, bool prefixPlus, bool alwaysShowBoth) {
        // Handle the case where only one number out of the province stat modifier is populated, when alwaysShowBoth isn't set.
        if (!alwaysShowBoth) {
            if (value.getValue() == 0) {
                if (value.getPercentage() == 0) {
                    return genericSigned(buffer, size, 0, prefixPlus);
                }
                return percentage(buffer, size, value.getPercentage(), true);
            }
            if (value.getPercentage() == 0) {
                return genericSigned(buffer, size, value.getValue(), prefixPlus);
            }
        }
        
        // Otherwise, add both manpower and percentage with brackets, using split.
        unsigned int upto = genericSigned(buffer, genericSignedModifierSplitLeft(size), value.getValue(), prefixPlus);
        buffer[upto++] = ' ';
        buffer[upto++] = '(';
        upto += percentage(buffer + upto, genericSignedModifierSplitRight(size), value.getPercentage(), true);
        buffer[upto++] = ')';
        buffer[upto] = '\0';
        return upto;
    }
    
    unsigned int NumberFormat::currency(char * buffer, unsigned int size, provstat_t value, bool prefixPlus) {
        // Work out digits (requires special treatment for decimal places).
        uint64_t posVal = std::abs(value);
        unsigned int digits = Misc::digitCount(posVal);
        digits = std::max(digits + 1u, 4u);
        if (value < 0 || prefixPlus) digits++;
        
        // Work out prefix string.
        const char * prefix = (value < 0 ? "-" : (prefixPlus ? "+" : ""));
        
        // Use regular currency format if possible.
        if (digits < size) {
            return std::sprintf(buffer, "%s%" PRIu64 ".%02u", prefix, posVal / 100, (unsigned int)(posVal % 100));
        }
        
        // Use format without hundredths if possible
        if (digits - 3 < size) {
            return std::sprintf(buffer, "%s%" PRIu64, prefix, posVal / 100);
        }
        
        // Otherwise fall back to k shortened.
        return kShortenedFallback(buffer, size, value / 100, posVal / 100, prefixPlus);
    }
    
    unsigned int NumberFormat::currencyModifier(char * buffer, unsigned int size, const ProvStatMod & value, bool prefixPlus, bool alwaysShowBoth) {
        // Handle the case where only one number out of the province stat modifier is populated, when alwaysShowBoth isn't set.
        if (!alwaysShowBoth) {
            if (value.getValue() == 0) {
                if (value.getPercentage() == 0) {
                    return currency(buffer, size, 0, prefixPlus);
                }
                return percentage(buffer, size, value.getPercentage(), true);
            }
            if (value.getPercentage() == 0) {
                return currency(buffer, size, value.getValue(), prefixPlus);
            }
        }
        
        // Otherwise, add both currency and percentage with brackets, using split.
        unsigned int upto = currency(buffer, currencyModifierSplitLeft(size), value.getValue(), prefixPlus);
        buffer[upto++] = ' ';
        buffer[upto++] = '(';
        upto += percentage(buffer + upto, currencyModifierSplitRight(size), value.getPercentage(), true);
        buffer[upto++] = ')';
        buffer[upto] = '\0';
        return upto;
    }
    
    unsigned int NumberFormat::landUseAvailable(char * buffer, unsigned int size, provstat_t use, provstat_t available) {
        unsigned int upto = 0;
        upto += land(buffer, landUseAvailableSplitLeft(size), use, false);
        buffer[upto++] = '/';
        return upto + land(buffer + upto, landUseAvailableSplitRight(size), available, false);
    }
    
    unsigned int NumberFormat::percentage(char * buffer, unsigned int size, provstat_t value, bool prefixPlus) {
        // Work out digits (requires special treatment for percentage symbol).
        uint64_t posVal = std::abs(value);
        unsigned int digits = Misc::digitCount(posVal) + 1;
        if (value < 0 || prefixPlus) digits++;
        
        // Use regular decimal format if possible.
        if (digits < size) {
            const char * prefix = (value < 0 ? "-" : (prefixPlus ? "+" : ""));
            return std::sprintf(buffer, "%s%" PRIu64 "%%", prefix, posVal);
        }
        
        // Otherwise fall back to k shortened, adding a percentage symbol to the end.
        unsigned int upto = kShortenedFallback(buffer, size - 1, value, posVal, prefixPlus);
        buffer[upto++] = '%';
        buffer[upto] = '\0';
        return upto;
    }
    
    unsigned int NumberFormat::permille(char * buffer, unsigned int size, provstat_t value, bool prefixPlus) {
        // Work out digits (requires special treatment for percentage symbol and decimal places).
        uint64_t posVal = std::abs(value);
        unsigned int digits = Misc::digitCount(posVal);
        digits = std::max(digits + 2u, 4u);
        if (value < 0 || prefixPlus) digits++;
        
        // Work out prefix string.
        const char * prefix = (value < 0 ? "-" : (prefixPlus ? "+" : ""));
        
        // Use permille format if possible.
        if (digits < size) {
            return std::sprintf(buffer, "%s%" PRIu64 ".%u%%", prefix, posVal / 10, (unsigned int)(posVal % 10));
        }
        
        // Otherwise fall back to regular percentage format.
        if (digits - 2 < size) {
            return std::sprintf(buffer, "%s%" PRIu64 "%%", prefix, posVal / 10);
        }
        
        // Otherwise fall back to k shortened, adding a percentage symbol to the end.
        unsigned int upto = kShortenedFallback(buffer, size - 1, value / 10, posVal / 10u, prefixPlus);
        buffer[upto++] = '%';
        buffer[upto] = '\0';
        return upto;
    }
}

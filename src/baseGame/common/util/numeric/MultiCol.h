/*
 * MultiCol.h
 *
 *  Created on: 3 Feb 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_NUMERIC_MULTICOL_H_
#define BASEGAME_COMMON_UTIL_NUMERIC_MULTICOL_H_

#include <GL/gl.h>
#include <cstdint>
#include <cstddef>

namespace rasc {
    class Random;
    
    /**
     * For convenience, a colour class which can be used for multiple colour spaces.
     * This can be set from an int colour, using operator=.
     * Channels can also be accessed by index, with operator[].
     * Each channel is represented as GLfloat values 0-255 inclusive.
     * For RGB:   colour.r(), colour.g(), colour.b()
     * For XYZ:   colour.x(), colour.y(), colour.z()
     * For LAB:   colour.l(), colour.a(), colour.b()
     */
    class MultiCol {
    public:
        /**
         * These represent the sizes of the ranges, within LAB colour space,
         * that the RGB colour space covers,
         * (using the XYZ reference: D65, Daylight, sRGB, Adobe-RGB - the reference that all
         * the conversion methods use).
         * If LAB colours are picked, with view to convert them to RGB, only these ranges
         * need considering.
         * These ranges were found experimentally by converting all possible RGB colours to
         * the LAB colour space, and recording minimum and maximum values.
         */
        const static GLfloat LAB_MIN_L, LAB_L_SIZE,
                             LAB_MIN_A, LAB_A_SIZE,
                             LAB_MIN_B, LAB_B_SIZE;
        
        /**
         * List of colour-blindness modes, used when finding colour-difference,
         * (see our distanceSquared method). These modify the calculation
         * to ignore certain axes according to the mode.
         * Format: name, friendly name.
         */
        #define MULTI_COL_COLOUR_BLIND_MODE_LIST \
            X(none, "None")                      \
            X(redGreen, "Red-Green")
        
        #define X(name, ...) name,
        class ColourBlindMode { public: enum { MULTI_COL_COLOUR_BLIND_MODE_LIST COUNT }; };
        #undef X
        
    private:
        // Stores our colours internally.
        GLfloat values[3];
    public:
        /**
         * Default constructor: Populates all 3 values with zeroes.
         */
        MultiCol(void);

        /**
         * Creates a MultiCol from an integer colour. The integer colour should be in the
         * format RGBA (or equivalent for LAB or XYZ). The alpha value is discarded, the
         * most significant byte becomes red (or L or X), and the other two are picked
         * appropriately.
         */
        explicit MultiCol(uint32_t intColour);

        /**
         * Creates a MultiCol from three floating point values. These can represent RGB, XYZ or LAB.
         */
        MultiCol(GLfloat r, GLfloat g, GLfloat b);

        /**
         * Allows convenient assignment to an integer colour. This acts the same as the
         * integer colour constructor.
         */
        MultiCol & operator = (uint32_t intColour);

        // Non-const accessor methods.
        inline GLfloat & operator [] (size_t index) { return values[index]; }
        inline GLfloat & r(void) { return values[0]; } inline GLfloat & g(void) { return values[1]; } inline GLfloat & b(void) { return values[2]; }
        inline GLfloat & x(void) { return values[0]; } inline GLfloat & y(void) { return values[1]; } inline GLfloat & z(void) { return values[2]; }
        inline GLfloat & l(void) { return values[0]; } inline GLfloat & a(void) { return values[1]; }

        // Const accessor methods.
        inline GLfloat operator [] (size_t index) const { return values[index]; }
        inline GLfloat r(void) const { return values[0]; } inline GLfloat g(void) const { return values[1]; } inline GLfloat b(void) const { return values[2]; }
        inline GLfloat x(void) const { return values[0]; } inline GLfloat y(void) const { return values[1]; } inline GLfloat z(void) const { return values[2]; }
        inline GLfloat l(void) const { return values[0]; } inline GLfloat a(void) const { return values[1]; }

        /**
         * Returns true if this MultiCol is within the range of a valid RGB colour,
         * i.e: Red, green and blue components are all within the inclusive range 0-255.
         */
        bool isWithinRangeRGB(void) const;

        /**
         * Converts the MultiCol instance to an RGBA int colour. The alpha component is
         * set to maximum (255). All other components are set to the closest integer
         * value.
         * Note that this will only return sensible results if isWithinRangeRGB returns true.
         */
        uint32_t toIntRGBA(void) const;

        /**
         * Methods to convert between colour spaces RGB, XYZ and LAB.
         * For all conversions, the following XYZ reference is used: D65, Daylight, sRGB, Adobe-RGB.
         * Source: http://www.easyrgb.com/en/math.php
         *         http://web.archive.org/web/20191208180824/http://www.easyrgb.com/en/math.php
         */
        void rgbToXyz(void);
        void xyzToLab(void);
        void labToXyz(void);
        void xyzToRgb(void);

        /**
         * Returns the distance between this MultiCol and another. For best results, the MultiCol
         * instances should be in the LAB colour space.
         *
         * This is most helpful for absolute measurements on the distance between colours, as
         * unlike distanceSquared, this returns the linear distance.
         */
        GLfloat distance(const MultiCol & other) const;

        /**
         * Returns the (squared) distance between this MultiCol and the other one, as an integer.
         * For best results, the MultiCol instances should be in the LAB colour space.
         *
         * This method (compared to the regular distance method), is most appropriate for comparing
         * multiple pairs of colours, for how close together each one is. This is because it returns
         * an integer, and is slightly faster than the regular distance method, as it does not
         * require an additional square root operation.
         *
         * Note that the colour channels are rounded to the nearest integer, and the distance calculation
         * is done using integer arithmetic. This guarantees that the calculation is commutative, but
         * does sacrifice some accuracy.
         * 
         * The colour-blindness mode can be specified, (note this only makes sense when comparing
         * colours in LAB colour space!!).
         * In "none" colour-blindness mode:
         *   All channels are compared.
         * In "redGreen" colour-blindness mode:
         *   The 'a' channel is ignored (this represents red/green difference in LAB).
         *   Experimentally I've found that this does seem to work for red-green colour-blindness,
         *   see src/test/tests/ColourBlindImageGenTest.cpp and the experiment test results in
         *   development/colourBlindImage/test-results.
         */
        unsigned int distanceSquared(const MultiCol & other, unsigned int colourBlindMode) const;

        // =================== Static utility methods ==========================

        /**
         * Returns a uniformly distributed random colour, in the LAB colour space, as
         * a MultiCol. This is uniformly distributed within the range of equivalent RGB
         * colours. It is highly likely (although not guaranteed) that this will have
         * a valid RGB conversion.
         */
        static MultiCol randomLabColour(Random & random);

        /**
         * Same as above, returns a uniformly distributed random colour in the LAB colour
         * space. But this one does additional steps to make sure the returned colour has
         * a valid RGB conversion. 
         */
        static MultiCol randomLabColourWithValidRGB(Random & random);
        
        /**
         * Returns a perceptually uniformly distributed random opaque RGB colour, as an integer.
         * This picks a uniformly distributed colour in the LAB colour space, then converts
         * it to an RGB colour.
         * This is a very visually correct way to pick random colours,
         * but is slow. On average 4.5 random LAB colours have to be picked before a valid
         * RGB colour is found. This can be found experimentally by counting the number of times
         * the loop in this methods runs on average.
         */
        static MultiCol perceptUniformRandomCol(Random & random);
        static uint32_t perceptUniformRandomColInt(Random & random);

        /**
         * Returns a random opaque colour, uniformly distributed across the RGB colour space.
         * Note that this is not perceptually uniformly distributed: For that use perceptUniformRandomCol.
         * This tends to generate a lot more green-ish and similar colours than perceptUniformRandomCol,
         * but is much faster.
         */
        static MultiCol randomColRGB(Random & random);
        static uint32_t randomColRGBInt(Random & random);

        /**
         * Returns a value representing how close the two provided colours are.
         * Larger values represent colours which are less similar.
         * This is done by finding the maximum distance across the three RGB channels.
         *
         * Note that this is a bad (but simple and fast) way to find colour distance.
         * For something which aims to be perceptably uniform, use perceptUniformRgbColDist
         * or perceptUniformRgbColDistSquared instead, (or create two MultiCol instances
         * and use their distance or distanceSquared methods).
         */
        static unsigned int linearRgbColDist(uint32_t col1, uint32_t col2);

        /**
         * Finds the perceptably uniform colour distance between the two provided RGB colours.
         * This is done by constructing two MultiCol instances, converting them to the LAB colour
         * space, and using the MultiCol::distance method.
         *
         * If this operation needs to be repeated, manually construct two MultiCol instances,
         * convert them to the LAB colour space, and use their distance methods. This is because
         * converting to LAB colour space is a much more expensive operation than finding the distance.
         *
         * This is most helpful for absolute measurements on the distance between colours, as
         * unlike perceptUniformRgbColDistSquared, this returns the linear distance.
         */
        static GLfloat perceptUniformRgbColDist(uint32_t col1, uint32_t col2);

        /**
         * Finds the perceptably uniform (squared) colour distance between the two provided RGB colours.
         * This is done by constructing two MultiCol instances, converting them to the LAB colour
         * space, and using the MultiCol::distanceSquared method.
         *
         * If this operation needs to be repeated, manually construct two MultiCol instances,
         * convert them to the LAB colour space, and use their distanceSquared methods. This is because
         * converting to LAB colour space is a much more expensive operation than finding the distanceSquared.
         *
         * This method (compared to the regular distance method), is most appropriate for comparing
         * multiple pairs of colours, for how close together each one is. This is because it returns
         * an integer, and is slightly faster than the regular perceptUniformRgbColDist method,
         * as it does not require an additional square root operation.
         *
         * Note that the colour channels are rounded to the nearest integer, and the distance calculation
         * is done using integer arithmetic. This guarantees that the calculation is commutative, but
         * does sacrifice some accuracy.
         * 
         * Note: See documentation for distanceSquared for information about colourBlindMode.
         */
        static unsigned int perceptUniformRgbColDistSquared(uint32_t col1, uint32_t col2, unsigned int colourBlindMode);
        
        /**
         * Methods to convert between colour-blindness modes and friendly names for them.
         * Used for displaying the modes to the user (in options menu), and saving them
         * to the config file.
         */
        static const char * colourBlindModeToFriendlyName(unsigned int colourBlindMode);
        static unsigned int friendlyNameToColourBlindMode(const char * friendlyName);
    };
}

#endif

/*
 * XMLUtil.h
 *
 *  Created on: 14 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UTIL_XMLUTIL_H_
#define BASEGAME_COMMON_UTIL_XMLUTIL_H_

#include <cstdint>
#include <cstddef>

namespace tinyxml2 {
    class XMLDocument;
    class XMLElement;
}

namespace rasc {

    /**
     * Loads an XMLDocument from the specified filename.
     * If this file is not found, a relevant message is
     * printed to the log and exit(EXIT_FAILURE) is called.
     */
    void loadFile(tinyxml2::XMLDocument & document, const char * filename);

    /**
     * Gets the first child element of the specified document, with the tag
     * name specified by name.
     * If this is not found, a relevant message is printed to the
     * log and exit(EXIT_FAILURE) is called.
     */
    tinyxml2::XMLElement * readElement(tinyxml2::XMLDocument & document, const char * name);

    /**
     * Gets the first child element of the specified element, with the tag
     * name specified by name.
     * If this is not found, a relevant message is printed to the
     * log and exit(EXIT_FAILURE) is called.
     */
    tinyxml2::XMLElement * readElement(tinyxml2::XMLElement * element, const char * name);
    
    /**
     * Gets the child element by ID.
     * If this is not found, a relevant message is printed to the
     * log and exit(EXIT_FAILURE) is called.
     */
    tinyxml2::XMLElement * readElement(tinyxml2::XMLElement * element, size_t id);

    /**
     * Gets the string value associated with the attribute called name.
     * If this is not found, a relevant message is printed to the
     * log and exit(EXIT_FAILURE) is called.
     */
    const char * readAttribute(tinyxml2::XMLElement * element, const char * name);

    /**
     * Converts a 32 bit hexadecimal number, represented as a string, to a 32 bit
     * integer value. For example, the string "6a6a6aff" is converted to the
     * 32 bit integer 1785359103.
     */
    uint32_t hexStringToInt(const char * string);

    /**
     * This macro allows conveniently iterating through all the child elements
     * in a tinyxml2 element, which have the specified tag name.
     *
     * For example, this will loop through all the elements with the tag
     * name "continent", which are children of the continentsElement element,
     * and call each one continent.
     * loopElements(continent, continentsElement, "continent") {
     *     // Use tinyxml2::XMLElement * continent
     * }
     *
     * See: https://stackoverflow.com/a/34622910
     *      http://web.archive.org/web/20200622135739/https://stackoverflow.com/questions/12150465/tinyxml-looping-over-elements/34622910
     */
    #define loopElements(forLoopElementName, elementToSearch, nameToFind)                                 \
        for(tinyxml2::XMLElement * forLoopElementName = (elementToSearch)->FirstChildElement(nameToFind); \
            (forLoopElementName) != NULL;                                                                 \
            forLoopElementName = (forLoopElementName)->NextSiblingElement(nameToFind))

    /**
     * Allows easy iteration through the child elements of an element, without
     * needing to supply a name for the element, (i.e: This spits out all
     * elements regardless of their name).
     */
    #define loopElementsAll(forLoopElementName, elementToSearch)                                \
        for(tinyxml2::XMLElement * forLoopElementName = (elementToSearch)->FirstChildElement(); \
            (forLoopElementName) != NULL;                                                       \
            forLoopElementName = (forLoopElementName)->NextSiblingElement())
}

#endif

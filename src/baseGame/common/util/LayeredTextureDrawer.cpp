
#include "LayeredTextureDrawer.h"

namespace rasc {

    LayeredTextureDrawer::DepthTextureLayer::DepthTextureLayer(const LayeredTextureDrawer & drawer, GLfloat depth, unsigned int gridX, unsigned int gridY) :
        depth(depth),
        uvX1(((GLfloat)gridX) / ((GLfloat)drawer.gridWidth)),
        uvY1(((GLfloat)gridY) / ((GLfloat)drawer.gridHeight)),
        uvX2(((GLfloat)(gridX + 1)) / ((GLfloat)drawer.gridWidth)),
        uvY2(((GLfloat)(gridY + 1)) / ((GLfloat)drawer.gridHeight)) {
    }

    LayeredTextureDrawer::TextureLayer::TextureLayer(const DepthTextureLayer & layer) :
        uvX1(layer.uvX1),
        uvY1(layer.uvY1),
        uvX2(layer.uvX2),
        uvY2(layer.uvY2) {
    }

    LayeredTextureDrawer::LayeredTextureDrawer(unsigned int gridWidth, unsigned int gridHeight) :
        gridWidth(gridWidth),
        gridHeight(gridHeight) {
    }
    LayeredTextureDrawer::~LayeredTextureDrawer(void) {
    }

    void LayeredTextureDrawer::addLayer(GLfloat depth, unsigned int gridX, unsigned int gridY) {
        layerSet.emplace(*this, depth, gridX, gridY);
    }

    void LayeredTextureDrawer::addLayer(Layer layer) {
        layerSet.emplace(*this, layer.depth, layer.gridX, layer.gridY);
    }

    void LayeredTextureDrawer::addLayers(const std::vector<Layer> layers) {
        for (const Layer & layer : layers) {
            layerSet.emplace(*this, layer.depth, layer.gridX, layer.gridY);
        }
    }

    void LayeredTextureDrawer::finalise(void) {
        layerVector.clear();
        layerVector.reserve(layerSet.size());
        for (const DepthTextureLayer & layer : layerSet) {
            layerVector.emplace_back(layer);
        }
        layerSet.clear();
    }

    void LayeredTextureDrawer::draw(GLfloat x, GLfloat y, GLfloat width, GLfloat height) const {
        GLfloat x2 = x + width,
                y2 = y + height;
        for (const TextureLayer & layer : layerVector) {
            glTexCoord2f(layer.uvX1, layer.uvY1);
            glVertex2f(x, y);
            glTexCoord2f(layer.uvX2, layer.uvY1);
            glVertex2f(x2, y);
            glTexCoord2f(layer.uvX2, layer.uvY2);
            glVertex2f(x2, y2);
            glTexCoord2f(layer.uvX1, layer.uvY2);
            glVertex2f(x, y2);
        }
    }
}


#ifndef BASEGAME_COMMON_UTIL_LAYEREDTEXTUREDRAWER_H_
#define BASEGAME_COMMON_UTIL_LAYEREDTEXTUREDRAWER_H_

#include <GL/gl.h>
#include <vector>
#include <set>

namespace rasc {

    /**
     * LayeredTextureDrawer allows a series of texture coordinates, (assuming a grid-based
     * texture sheet), to be drawn on top of each other.
     * Each texture coordinate set is supplied a depth, and after finalise is called,
     * draw operations will draw these in order of lowest to highest depth.
     * Note: This class does not have a virtual destructor, do not make subclasses.
     */
    class LayeredTextureDrawer {
    public:

        /**
         * This class can be used to conveniently represent a layer in the texture.
         * There are no defined constructors in this class, aggregate initialisation is recommended.
         * There is an overload of addLayer which takes instances of this class.
         */
        class Layer {
        public:
            GLfloat depth;
            unsigned int gridX, gridY;
        };

    private:
        class TextureLayer;

        /**
         * This class is used internally to store each texture layer, with an associated depth.
         */
        class DepthTextureLayer {
        private:
            friend class LayeredTextureDrawer::TextureLayer;
            GLfloat depth;
            GLfloat uvX1, uvY1, uvX2, uvY2;
        public:
            DepthTextureLayer(const LayeredTextureDrawer & drawer, GLfloat depth, unsigned int gridX, unsigned int gridY);
            inline bool operator <  (const DepthTextureLayer & other) const { return depth <  other.depth; }
            inline bool operator == (const DepthTextureLayer & other) const { return depth == other.depth; }
        };

        /**
         * This class is used to store each texture layer, once we have finalised the layers,
         * so no longer need any depth information.
         */
        class TextureLayer {
        public:
            GLfloat uvX1, uvY1, uvX2, uvY2;
            TextureLayer(const DepthTextureLayer & layer);
        };

        // The width and height of the grid.
        unsigned int gridWidth, gridHeight;

        // This is used for building layers.
        std::set<DepthTextureLayer> layerSet;
        // This the layer set is copied into this when finalise is called.
        std::vector<TextureLayer> layerVector;

    public:
        /**
         * The parameters gridWidth and gridHeight should represent the number of columns
         * and rows respectively in the texture sheet.
         */
        LayeredTextureDrawer(unsigned int gridWidth, unsigned int gridHeight);
        ~LayeredTextureDrawer(void);

        /**
         * Adds a layer with the specified depth, at the specified grid position in the texture sheet.
         */
        void addLayer(GLfloat depth, unsigned int gridX, unsigned int gridY);

        /**
         * Adds a layer using the specified layer instance.
         * This can be called with aggregate initialisation, e.g:
         * tex.addLayer({2.0f, 2, 4});
         */
        void addLayer(Layer layer);

        /**
         * Adds multiple layers, using a vector of layer instances.
         */
        void addLayers(const std::vector<Layer> layers);

        /**
         * Finalises the layers. This must be called after addLayer, and before draw.
         */
        void finalise(void);

        /**
         * Draws all of the layers at the specified coordinates. This assumes whichever texture
         * has already been bound.
         */
        void draw(GLfloat x, GLfloat y, GLfloat width, GLfloat height) const;
    };
}

#endif

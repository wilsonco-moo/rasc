/*
 * PlayerStatArr.cpp
 *
 *  Created on: 27 Feb 2020
 *      Author: wilson
 */

#include "PlayerStatArr.h"

#include <sockets/plus/message/Message.h>
#include <algorithm>

#include "../util/numeric/ElementWiseArrayMaths.h"
#include "../util/numeric/ChArith.h"

namespace rasc {
    

    PlayerStatArr::PlayerStatArr(simplenetwork::Message & msg) :
        data() {
        // Don't bother initialising data, just run readFrom instead.
        readFrom(msg);
    }
    
    void PlayerStatArr::writeTo(simplenetwork::Message & msg) const {
        for (provstat_t value : data) {
            PROVINCE_STAT_WRITE(msg, value);
        }
    }

    void PlayerStatArr::readFrom(simplenetwork::Message & msg) {
        for (provstat_t & value : data) {
            value = PROVINCE_STAT_READ(msg);
        }
    }
    
    PlayerStatArr PlayerStatArr::multiplyCheckOverflow(bool * error, provstat_t count) const {
        // Build output, with each stat overflow-check-multiplied by the count.
        PlayerStatArr output;
        for (PlayerStatId statId = 0; statId < PlayerStatIds::COUNT; statId++) {
            output[statId] = ChArith::multiply(error, data[statId], count);
        }
        return output;
    }

    static constexpr provstat_t MAX_BASE            = std::max(BaseProvinceStats::manpowerIncome, BaseProvinceStats::currencyIncome),
                                CURRENCY_MULTIPLIER = MAX_BASE / BaseProvinceStats::currencyIncome,
                                MANPOWER_MULTIPLIER = MAX_BASE / BaseProvinceStats::manpowerIncome;

    provstat_t PlayerStatArr::getOverallCostEstimation(void) const {
        return data[PlayerStatIds::manpower] * MANPOWER_MULTIPLIER +
               data[PlayerStatIds::currency] * CURRENCY_MULTIPLIER;
    }
    
    bool PlayerStatArr::isZero(void) const {
        for (provstat_t value : data) {
            if (value != 0) return false;
        }
        return true;
    }
    
    bool PlayerStatArr::operator == (const PlayerStatArr & other) {
        return data == other.data;
    }
    
    bool PlayerStatArr::operator != (const PlayerStatArr & other) {
        return data != other.data;
    }

    PlayerStatArr & PlayerStatArr::operator += (const PlayerStatArr & other) {
        data += other.data;
        return *this;
    }
    PlayerStatArr PlayerStatArr::operator + (const PlayerStatArr & other) const {
        PlayerStatArr output(*this);
        output.data += other.data;
        return output;
    }
    PlayerStatArr & PlayerStatArr::operator -= (const PlayerStatArr & other) {
        data -= other.data;
        return *this;
    }
    PlayerStatArr PlayerStatArr::operator - (const PlayerStatArr & other) const {
        PlayerStatArr output(*this);
        output.data -= other.data;
        return output;
    }
}

/*
 * PlayerStatArr.h
 *
 *  Created on: 27 Feb 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_PLAYERDATA_PLAYERSTATARR_H_
#define BASEGAME_COMMON_PLAYERDATA_PLAYERSTATARR_H_

#include "../gameMap/provinceUtil/ProvinceStatTypes.h"
#include "PlayerStatDef.h"

namespace simplenetwork {
    class Message;
}

namespace rasc {

    /**
     * Utility class used for general purpose storage and manipulation of player stats.
     * One of these is stored within PlayerData, to represent player stats. This class is
     * also used to represent building costs, and more generally any changes to player stats.
     * 
     * Note: Some methods and our constructor are constexpr (in our inl file), more can
     * be made constexpr as needed.
     */
    class PlayerStatArr {
    private:
        // Our internal data is an array indexed by player stat ID.
        std::array<provstat_t, PlayerStatIds::COUNT> data;
        
    public:
        /**
         * Default constructor: initialises all player stats to zero.
         */
        constexpr PlayerStatArr(void);
        
        /**
         * Constructor to read player stat array from a message, as written by
         * our writeTo method. This is more convenient (possibly faster too)
         * than creating a PlayerStatArr then running readFrom.
         */
        PlayerStatArr(simplenetwork::Message & msg);
        
        /**
         * Writes our contents to the specified message. A player stat array
         * can be read from a message on the other side using the constructor
         * above, or our readFrom method.
         */
        void writeTo(simplenetwork::Message & msg) const;
        
        /**
         * Reads player stat array from a message, as written by our
         * writeTo method.
         */
        void readFrom(simplenetwork::Message & msg);

        /**
         * Operators to access player stats by index. These indices should
         * come from the PlayerStatIds enum (see ProvinceStatTypes.h).
         */
        constexpr provstat_t & operator[](PlayerStatId statId);
        constexpr const provstat_t & operator[](PlayerStatId statId) const;
        
        /**
         * Returns a player stat array which is multiplied by the specified count.
         * Sets the error flag to true in the case of overflow.
         */
        PlayerStatArr multiplyCheckOverflow(bool * error, provstat_t count) const;

        /**
         * Gets an overall cost estimation, as a single number. This is helpful
         * for working out the "value for money" of buildings or other stat changes.
         * This is worked out by multiplying the costs in the ratios defined by
         * the base values in ProvinceStatTypes, then adding them together.
         * For example, currency is about 10 times move valuable than manpower,
         * since provinces (currently) have a base currency output of 100, compared
         * to a base manpower output of 1000. This would mean we would return
         * manpowerCost + 10 * currencyCost.
         */
        provstat_t getOverallCostEstimation(void) const;
        
        /**
         * Returns true if all our stats equal zero.
         */
        bool isZero(void) const;

        /**
         * Element-wise equality operators.
         */
        bool operator == (const PlayerStatArr & other);
        bool operator != (const PlayerStatArr & other);
        
        /**
         * Element-wise addition and subtraction operators for player stat array.
         */
        PlayerStatArr & operator += (const PlayerStatArr & other);
        PlayerStatArr operator + (const PlayerStatArr & other) const;
        PlayerStatArr & operator -= (const PlayerStatArr & other);
        PlayerStatArr operator - (const PlayerStatArr & other) const;
    };
}

#include "PlayerStatArr.inl"

#endif

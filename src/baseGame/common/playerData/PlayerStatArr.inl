/*
 * PlayerStatArr.h
 *
 *  Created on: 8 Aug 2023
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_PLAYERDATA_PLAYERSTATARR_INL_
#define BASEGAME_COMMON_PLAYERDATA_PLAYERSTATARR_INL_

#include "PlayerStatArr.h"

namespace rasc {

    // Explicitly initialise all player stats to zero by default.
    #define X(...) 0,
    constexpr PlayerStatArr::PlayerStatArr(void) :
        data({ RASC_PLAYER_STATS }) {
    }
    #undef X

    constexpr provstat_t & PlayerStatArr::operator[](PlayerStatId statId) {
        return data[statId];
    }
    
    constexpr const provstat_t & PlayerStatArr::operator[](PlayerStatId statId) const {
        return data[statId];
    }
}

#endif

/*
 * PlayerStatDef.h
 *
 *  Created on: 28 Jul 2021
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_PLAYERDATA_PLAYERSTATDEF_H_
#define BASEGAME_COMMON_PLAYERDATA_PLAYERSTATDEF_H_

#include <cstdint>

namespace rasc {

    /**
     * Header file defining player stats, and other related things, as X macros,
     * enums and typedefs.
     */
    
    /**
     * X macro, enum and typedef defining the cumulative stats owned by each player.
     * These are stored within a PlayerStatArr inside each players' PlayerData.
     * Player stats represent "resources" owned by players, which are gained
     * from their land (etc), and are spent when they build things.
     */
    #define RASC_PLAYER_STATS \
        X(manpower)           \
        X(currency)
    
    class PlayerStatIds {
    public:
        #define X(name, ...) name,
        enum { RASC_PLAYER_STATS COUNT };
        #undef X
    };
    typedef uint8_t PlayerStatId;
    #define PLAYER_STAT_ID_WRITE(msg, provinceStatId) (msg).write8(provinceStatId)
    #define PLAYER_STAT_ID_READ(msg) (msg).read8()

    
    /**
     * X macro, enum and typedef defining the reasons which player stats can change.
     * One of these is passed to the stat system each time it is notified of a change
     * to player stats.
     */
    #define RASC_PLAYER_STAT_CHANGE_REASONS \
        X(incomeFromProvinces)              \
        X(armyBuild)                        \
        X(armyMaintenance)                  \
        X(constructBuilding)                \
        X(developmentConsole)
    
    class PlayerStatChangeReasons {
    public:
        #define X(name, ...) name,
        enum { RASC_PLAYER_STAT_CHANGE_REASONS COUNT };
        #undef X
    };
    typedef uint8_t PlayerStatChangeReason;
    #define PLAYER_STAT_CHANGE_REASON_WRITE(msg, statChangeReason) (msg).write8(statChangeReason)
    #define PLAYER_STAT_CHANGE_REASON_READ(msg) (msg).read8()
}

#endif

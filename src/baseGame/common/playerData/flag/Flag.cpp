/*
 * FlagComponent.cpp
 *
 *  Created on: 10 Dec 2018
 *      Author: wilson
 */

#include "Flag.h"

#include <cstdint>
#include <cstring>
#include <array>

#include <sockets/plus/message/Message.h>
#include <wool/misc/RGBA.h>

#include "../../util/templateTypes/ProbabilityArray.h"
#include "../../util/numeric/MultiCol.h"
#include "../../util/numeric/Random.h"

namespace rasc {
    
    // Forward declare for later.
    static void flagPopulateDefault(Flag &, const wool::RGBA &, const wool::RGBA &, const wool::RGBA &);
    
    namespace randomFlag {
        
        // Pallete types -----------------------------------------------
        
        // Interface for a pallete: Just a function to get the next colour (given random),
        // plus a helper function to re-try generating colours to avoid one (avoid adjacent same colours).
        class Pallete {
        public:
            virtual uint32_t nextColour(Random & random) const = 0;
            uint32_t nextColourAvoid(Random & random, const uint32_t avoidColour, const unsigned int attempts) const;
        };
        
        // Random pallete: Always a uniformly distributed random colour.
        class RandomPallete : public Pallete {
        public:
            virtual uint32_t nextColour(Random & random) const override;
        };
        
        // Data pallete: Stores a probability array of predefined colours, picks
        // one at random.
        template<size_t arraySize>
        class DataPallete : public Pallete {
        private:
            using ArrayType = ProbabilityArray<unsigned int, uint32_t, arraySize>;
            ArrayType probabilityArray;
        public:
            // Construct using array of pairs of weight, colour.
            template<size_t initArraySize>
            constexpr DataPallete(const typename ArrayType::value_type(&initArray)[initArraySize]);
            virtual uint32_t nextColour(Random & random) const override;
        };
        
        // Pallete implementation --------------------------------------
        
        uint32_t Pallete::nextColourAvoid(Random & random, const uint32_t avoidColour, const unsigned int attempts) const {
            uint32_t foundColour;
            unsigned int attempt = 0;
            // Keep generating colours until we find one which isn't the same as "avoidColour", (try at most "attempts" times).
            do {
                foundColour = nextColour(random);
                attempt++;
            } while(foundColour == avoidColour && attempt < attempts);
            return foundColour;
        }
        
        uint32_t RandomPallete::nextColour(Random & random) const {
            return MultiCol::perceptUniformRandomColInt(random);
        }
        
        template<size_t arraySize>
        template<size_t initArraySize>
        constexpr DataPallete<arraySize>::DataPallete(const typename ArrayType::value_type(&initArray)[initArraySize]) :
            probabilityArray(initArray) {
        }
        
        template<size_t arraySize>
        uint32_t DataPallete<arraySize>::nextColour(Random & random) const {
            return probabilityArray.getValue(random.uintRange(0, probabilityArray.getTotal()));
        }
        
        // Pallete definitions -----------------------------------------
        // Good source of flag colours: http://web.archive.org/web/20160222145446/https://www.englishclan.co.uk/Art_images/World.Flags.png
        
        namespace paletteData {
            constexpr static RandomPallete random;
            
            // The random spread of colours typical of the majority of flags.
            constexpr static DataPallete<14> spreadColours({
                { 75, 0x000099ff }, // dark blue (100)
                { 10, 0x006699ff }, // middle blue        
                { 10, 0x0066ccff }, // light blue
                { 5, 0x0099ffff }, // lighter blue        
                
                { 75, 0xff0000ff }, // bright red (100)
                { 10, 0xcc0000ff }, // dark red
                { 10, 0x990000ff }, // darker red
                { 5, 0xd81e05ff }, // mixed red
                
                { 100, 0xffffffff }, // white (100)
                
                { 40, 0xffcc00ff }, // dark yellow (50)
                { 10, 0xffff00ff }, // light yellow
                        
                { 50, 0x000000ff }, // black (50)
                
                { 20, 0x009900ff }, // green (40)
                { 20, 0x008400ff } // darker green
            });
            
            // Predominantly green, yellow, red colour scheme
            constexpr static DataPallete<7> greenYellowRed({
                { 30, 0x009900ff }, // green
                { 30, 0xffff00ff }, // yellow
                { 30, 0xff0000ff }, // red
                    
                { 2, 0x000000ff }, // black
                { 2, 0xffffffff }, // white
                { 1, 0x0000ccff }, // blue (light)
                { 1, 0x000099ff } // blue (dark)
            });
            
            // Predominantly black and red colour scheme
            constexpr static DataPallete<5> redBlack({
                { 15, 0x000000ff }, // black
                { 15, 0xff0000ff }, // red
                        
                { 3, 0xffffffff }, // white
                { 1, 0x581007ff }, // brown
                { 1, 0xffcc00ff } // orange
            });
            
            // Blue and white colour scheme
            constexpr static DataPallete<4> blueWhite({
                { 1, 0x000099ff }, // dark blue
                { 1, 0x0000ccff }, // medium blue
                { 1, 0x3399ffff }, // light blue
                { 3, 0xffffffff } // white
            });
            
            // Greyscale colour scheme
            constexpr static DataPallete<5> greyscale({
                { 1, 0x404040ff }, // dark grey
                { 1, 0xbfbfbfff }, // light grey
                { 1, 0x808080ff }, // grey
                { 1, 0x000000ff }, // black
                { 1, 0xffffffff } // white
            });
            
            // Plain just red white blue colour scheme (typical of lots of flags).
            constexpr static DataPallete<3> redWhiteBlue({
                { 1, 0xcc0000ff }, // red
                { 1, 0xffffffff }, // white
                { 1, 0x000066ff } // blue
            });
            
            // Probability array for pallete selection (list of all available palettes).
            constexpr static ProbabilityArray<unsigned int, const Pallete *, 7> probabilityArray({
                { 8, &random },
                { 85, &spreadColours },
                { 40, &greenYellowRed },
                { 50, &redBlack },
                { 35, &blueWhite },
                { 20, &greyscale },
                { 40, &redWhiteBlue }
            });
        }
        
        // Patterns ----------------------------------------------------
        
        // All pattern functions match this typedef.
        using PatternFunc = void (*)(Flag &, Random &, const Pallete &);
        
        namespace patterns {
            static void blank(Flag & flag, Random & random, const Pallete & pallete) {
                flag.rectangles.reserve(1);
                flag.rectangles.emplace_back(wool::RGBA(pallete.nextColour(random)));
            }

            // Three bar flags
            
            static void threeHorizontal(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t centreColour = pallete.nextColour(random);
                flag.rectangles.reserve(3);
                flag.rectangles.emplace_back(wool::RGBA(pallete.nextColourAvoid(random, centreColour, 5)), 0.0f, 0.0f / 3.0f, 1.0f, 1.0f / 3.0f);
                flag.rectangles.emplace_back(wool::RGBA(centreColour), 0.0f, 1.0f / 3.0f, 1.0f, 1.0f / 3.0f);
                flag.rectangles.emplace_back(wool::RGBA(pallete.nextColourAvoid(random, centreColour, 5)), 0.0f, 2.0f / 3.0f, 1.0f, 1.0f / 3.0f);
            }
            
            static void fiveHorizontal(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t backgroundColour(pallete.nextColour(random));
                const wool::RGBA barColour(pallete.nextColourAvoid(random, backgroundColour, 5));
                flag.rectangles.reserve(3);
                flag.rectangles.emplace_back(wool::RGBA(backgroundColour));
                flag.rectangles.emplace_back(barColour, 0.0f, 0.2f, 1.0f, 0.2f);
                flag.rectangles.emplace_back(barColour, 0.0f, 0.6f, 1.0f, 0.2f);
            }
            
            static void threeVertical(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t centreColour = pallete.nextColour(random);
                flag.rectangles.reserve(3);
                flag.rectangles.emplace_back(wool::RGBA(pallete.nextColourAvoid(random, centreColour, 5)), 0.0f / 3.0f, 0.0f, 1.0f / 3.0f, 1.0f);
                flag.rectangles.emplace_back(wool::RGBA(centreColour), 1.0f / 3.0f, 0.0f, 1.0f / 3.0f, 1.0f);
                flag.rectangles.emplace_back(wool::RGBA(pallete.nextColourAvoid(random, centreColour, 5)), 2.0f / 3.0f, 0.0f, 1.0f / 3.0f, 1.0f);
            }
            
            // Cross flags
            
            static void centredSingleCross(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t backgroundColour = pallete.nextColour(random);
                const wool::RGBA crossColour(pallete.nextColourAvoid(random, backgroundColour, 10));
                flag.rectangles.reserve(3);
                flag.rectangles.emplace_back(wool::RGBA(backgroundColour));
                flag.rectangles.emplace_back(crossColour, 0.5f - (0.1f * 0.5f), 0.0f, 0.1f, 1.0f);
                flag.rectangles.emplace_back(crossColour, 0.0f, 0.5f - 0.1f, 1.0f, 0.1f * 2.0f);
            }
            
            static void centredDoubleCross(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t backgroundColour = pallete.nextColour(random);
                const uint32_t crossColour = pallete.nextColourAvoid(random, backgroundColour, 5);
                const uint32_t centreCrossColour = pallete.nextColourAvoid(random, crossColour, 5);
                // Same as default flag!
                flagPopulateDefault(flag, wool::RGBA(backgroundColour), wool::RGBA(crossColour), wool::RGBA(centreCrossColour));
            }
            
            static void offCentreLeftSingleCross(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t backgroundColour = pallete.nextColour(random);
                const wool::RGBA crossColour(pallete.nextColourAvoid(random, backgroundColour, 10));
                flag.rectangles.reserve(3);
                flag.rectangles.emplace_back(wool::RGBA(backgroundColour));
                flag.rectangles.emplace_back(crossColour, 0.375f - (0.125f * 0.5f), 0.0f, 0.125f, 1.0f);
                flag.rectangles.emplace_back(crossColour, 0.0f, 0.5f - 0.125f, 1.0f, 0.125f * 2.0f);
            }
            
            static void offCentreLeftOutlinedCross(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t backgroundColourInt = pallete.nextColour(random);
                const uint32_t outlineColourInt = pallete.nextColourAvoid(random, backgroundColourInt, 5);
                const wool::RGBA outlineColour(outlineColourInt);
                const wool::RGBA crossColour(pallete.nextColourAvoid(random, outlineColourInt, 10));
                flag.rectangles.reserve(5);
                flag.rectangles.emplace_back(wool::RGBA(backgroundColourInt));
                flag.rectangles.emplace_back(outlineColour, 0.375f - (0.1875f * 0.5f), 0.0f, 0.1875f, 1.0f);
                flag.rectangles.emplace_back(outlineColour, 0.0f, 0.5f - 0.1875f, 1.0f, 0.1875f * 2.0f);
                flag.rectangles.emplace_back(crossColour, 0.375f - (0.1f * 0.5f), 0.0f, 0.1f, 1.0f);
                flag.rectangles.emplace_back(crossColour, 0.0f, 0.5f - 0.1f, 1.0f, 0.1f * 2.0f);
            }
            
            static void offCentreRightSingleCross(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t backgroundColour = pallete.nextColour(random);
                const wool::RGBA crossColour(pallete.nextColourAvoid(random, backgroundColour, 10));
                flag.rectangles.reserve(3);
                flag.rectangles.emplace_back(wool::RGBA(backgroundColour));
                flag.rectangles.emplace_back(crossColour, 0.625f - (0.125f * 0.5f), 0.0f, 0.125f, 1.0f);
                flag.rectangles.emplace_back(crossColour, 0.0f, 0.5f - 0.125f, 1.0f, 0.125f * 2.0f);
            }
            
            static void offCentreRightOutlinedCross(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t backgroundColourInt = pallete.nextColour(random);
                const uint32_t outlineColourInt = pallete.nextColourAvoid(random, backgroundColourInt, 5);
                const wool::RGBA outlineColour(outlineColourInt);
                const wool::RGBA crossColour(pallete.nextColourAvoid(random, outlineColourInt, 10));
                flag.rectangles.reserve(5);
                flag.rectangles.emplace_back(wool::RGBA(backgroundColourInt));
                flag.rectangles.emplace_back(outlineColour, 0.625f - (0.1875f * 0.5f), 0.0f, 0.1875f, 1.0f);
                flag.rectangles.emplace_back(outlineColour, 0.0f, 0.5f - 0.1875f, 1.0f, 0.1875f * 2.0f);
                flag.rectangles.emplace_back(crossColour, 0.625f - (0.1f * 0.5f), 0.0f, 0.1f, 1.0f);
                flag.rectangles.emplace_back(crossColour, 0.0f, 0.5f - 0.1f, 1.0f, 0.1f * 2.0f);
            }
            
            // Asymmetrical + triangle based flags
            
            static void threeBlocks(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t backgroundColour = pallete.nextColour(random);
                const uint32_t topColour = pallete.nextColourAvoid(random, backgroundColour, 5);
                flag.rectangles.reserve(3);
                flag.rectangles.emplace_back(wool::RGBA(backgroundColour), 0.0f, 0.0f, 0.4375f, 1.0f);
                flag.rectangles.emplace_back(wool::RGBA(topColour), 0.4375f, 0.0f, 0.5625f, 0.5f);
                flag.rectangles.emplace_back(wool::RGBA(pallete.nextColourAvoid(random, topColour, 5)), 0.4375f, 0.5f, 0.5625f, 0.5f);
            }
            
            static void leftTriangleTwoBars(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t triangleColour = pallete.nextColour(random);
                flag.rectangles.reserve(3);
                flag.rectangles.emplace_back(wool::RGBA(triangleColour), FlagPoint(0.0f, 0.0f), FlagPoint(0.25f, 0.5f), FlagPoint(0.0f, 1.0f), FlagPoint(0.0f, 0.0f));
                flag.rectangles.emplace_back(wool::RGBA(pallete.nextColourAvoid(random, triangleColour, 5)), FlagPoint(0.0f, 0.0f), FlagPoint(1.0f, 0.0f), FlagPoint(1.0f, 0.5f), FlagPoint(0.25f, 0.5f));
                flag.rectangles.emplace_back(wool::RGBA(pallete.nextColourAvoid(random, triangleColour, 5)), FlagPoint(0.25f, 0.5f), FlagPoint(1.0f, 0.5f), FlagPoint(1.0f, 1.0f), FlagPoint(0.0f, 1.0f));
            }
            
            static void leftTriangleThreeBars(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t triangleColour = pallete.nextColour(random);
                flag.rectangles.reserve(4);
                flag.rectangles.emplace_back(wool::RGBA(pallete.nextColourAvoid(random, triangleColour, 5)), 0.0f, 0.0f / 3.0f, 1.0f, 1.0f / 3.0f);
                flag.rectangles.emplace_back(wool::RGBA(pallete.nextColourAvoid(random, triangleColour, 5)), 0.15625f, 1.0f / 3.0f, 0.84375f, 1.0f / 3.0f);
                flag.rectangles.emplace_back(wool::RGBA(pallete.nextColourAvoid(random, triangleColour, 5)), 0.0f, 2.0f / 3.0f, 1.0f, 1.0f / 3.0f);
                flag.rectangles.emplace_back(wool::RGBA(triangleColour), FlagPoint(0.0f, 0.0f), FlagPoint(0.25f, 0.5f), FlagPoint(0.0f, 1.0f), FlagPoint(0.0f, 0.0f));
            }
            
            static void leftTriangleFiveBars(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t triangleColourInt = pallete.nextColour(random);
                const uint32_t outerColourInt(pallete.nextColourAvoid(random, triangleColourInt, 5));
                const uint32_t centreColourInt = pallete.nextColourAvoid(random, outerColourInt, 5);
                const wool::RGBA centreColour(centreColourInt);
                flag.rectangles.reserve(4);
                flag.rectangles.emplace_back(wool::RGBA(outerColourInt));
                flag.rectangles.emplace_back(centreColour, 0.0f, 0.2f, 1.0f, 0.2f);
                flag.rectangles.emplace_back(centreColour, 0.0f, 0.6f, 1.0f, 0.2f);
                flag.rectangles.emplace_back(wool::RGBA(triangleColourInt), FlagPoint(0.0f, 0.0f), FlagPoint(0.25f, 0.5f), FlagPoint(0.0f, 1.0f), FlagPoint(0.0f, 0.0f));
            }
            
            // Other shapes
            
            static void xShape(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t xColour = pallete.nextColour(random);
                const wool::RGBA backgroundColour(pallete.nextColourAvoid(random, xColour, 10));
                // From: https://en.wikipedia.org/wiki/File:Flag_of_Jamaica.svg
                flag.rectangles.reserve(5);
                flag.rectangles.emplace_back(wool::RGBA(backgroundColour));
                flag.rectangles.emplace_back(xColour, FlagPoint(1.0f, 55.859 / 600.0), FlagPoint(1.0f, 544.141 / 600.0), FlagPoint(711.719 / 1200.0, 0.5f), FlagPoint(1.0f, 55.859 / 600.0));
                flag.rectangles.emplace_back(xColour, FlagPoint(111.719 / 1200.0, 0.0f), FlagPoint(1088.281 / 1200.0, 0.0f), FlagPoint(0.5f, 244.141 / 600.0), FlagPoint(111.719 / 1200.0, 0.0f));
                flag.rectangles.emplace_back(xColour, FlagPoint(0.0f, 55.859 / 600.0), FlagPoint(488.281 / 1200.0, 0.5f), FlagPoint(0.0f, 544.141 / 600.0), FlagPoint(0.0f, 55.859 / 600.0));
                flag.rectangles.emplace_back(xColour, FlagPoint(0.5f, 355.859 / 600.0), FlagPoint(1088.281 / 1200.0, 1.0f), FlagPoint(111.719 / 1200.0, 1.0f), FlagPoint(0.5f, 355.859 / 600.0));
            }
            
            static void divergingBars(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t colour0 = pallete.nextColour(random);
                const uint32_t colour1 = pallete.nextColourAvoid(random, colour0, 5);
                const uint32_t colour2 = pallete.nextColourAvoid(random, colour1, 5);
                const uint32_t colour3 = pallete.nextColourAvoid(random, colour2, 5);
                // From: https://en.wikipedia.org/wiki/File:Flag_of_Seychelles_(construction_sheet).svg
                flag.rectangles.reserve(4);
                flag.rectangles.emplace_back(wool::RGBA(colour0), FlagPoint(0.0f, 0.0f), FlagPoint(1.0f / 3.0f, 0.0f), FlagPoint(0.0f, 1.0f), FlagPoint(0.0f, 0.0f));
                flag.rectangles.emplace_back(wool::RGBA(colour1), FlagPoint(1.0f / 3.0f, 0.0f), FlagPoint(2.0f / 3.0f, 0.0f), FlagPoint(0.0f, 1.0f), FlagPoint(1.0f / 3.0f, 0.0f));
                flag.rectangles.emplace_back(wool::RGBA(colour2), FlagPoint(2.0f / 3.0f, 0.0f), FlagPoint(1.0f, 0.0f), FlagPoint(1.0f, 1.0f / 3.0f), FlagPoint(0.0f, 1.0f));
                flag.rectangles.emplace_back(wool::RGBA(colour3), FlagPoint(1.0f, 1.0f / 3.0f), FlagPoint(1.0f, 1.0f), FlagPoint(0.0f, 1.0f), FlagPoint(1.0f, 1.0f / 3.0f));
            }
            
            static void star(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t backgroundColour = pallete.nextColour(random);
                const wool::RGBA starColour(pallete.nextColourAvoid(random, backgroundColour, 10));
                // outer points from: https://en.wikipedia.org/wiki/File:Flag_of_Vietnam.svg (0,1,2,3,4 going anticlockwise from right-most point)
                constexpr double x0 = (621.3 + 150.0) / 1200.0, y0 = 244.5 / 600.0,
                        x1 = (450.0 + 150.0) / 1200.0, y1 = 120.0 / 600.0,
                        x2 = (278.7 + 150.0) / 1200.0, y2 = 244.5 / 600.0,
                        x3 = (344.1 + 150.0) / 1200.0, y3 = 445.5 / 600.0,
                        x4 = (555.9 + 150.0) / 1200.0, y4 = 445.5 / 600.0;
                // so inner (intersection) points (0,1),2,3,4, anticlockwise from top-right point (gradient maths):
                constexpr double g30 = (y0 - y3) / (x0 - x3), g24 = (y4 - y2) / (x4 - x2), g31 = (y1 - y3) / (x1 - x3), g14 = (y4 - y1) / (x4 - x1), // bottom three points, gradients g = (y2 - y1) / (x2 - x1)
                        o30 = y3 - g30 * x3, o24 = y2 - g24 * x2, o31 = y3 - g31 * x3, o14 = y1 - g14 * x1, // offsets from x axis, (y = gx + c so c = y - gx)
                        ix2 = (o24 - o31) / (g31 - g24), ix3 = (o24 - o30) / (g30 - g24), ix4 = (o30 - o14) / (g14 - g30), // intersect point x positions (gx + c == hx + d so gx - hx = d - c so x(g - h) = d - c so x = (d - c) / (g - h))
                        iy2 = g24 * ix2 + o24, iy3 = g24 * ix3 + o24, iy4 = g30 * ix4 + o30; // y position of intersect point (y = gc + c)
                // Draw three triangles (not a triangle and concave quad), I don't want to have to rely on quads being triangulated in a consistent order.
                flag.rectangles.reserve(4);
                flag.rectangles.emplace_back(wool::RGBA(backgroundColour));
                flag.rectangles.emplace_back(starColour, FlagPoint(x2, y2), FlagPoint(x0, y0), FlagPoint(ix3, iy3), FlagPoint(x2, y2));
                flag.rectangles.emplace_back(starColour, FlagPoint(ix2, iy2), FlagPoint(x1, y1), FlagPoint(x4, y4), FlagPoint(ix2, iy2));
                flag.rectangles.emplace_back(starColour, FlagPoint(x3, y3), FlagPoint(ix4, iy4), FlagPoint(x1, y1), FlagPoint(x3, y3));
            }
            
            static void doubleTriangle(Flag & flag, Random & random, const Pallete & pallete) {
                const uint32_t backgroundColour = pallete.nextColour(random);
                const wool::RGBA triangleColour(pallete.nextColourAvoid(random, backgroundColour, 10));
                // https://en.wikipedia.org/wiki/File:Flag_of_Nepal.svg
                constexpr double wm = (0.5 / 726.000) * (726.000 / 885.000), hm = 1.0 / 885.000, x0 = 726.000 * wm, y0 = 454.651 * hm, y1 = 186.086 * hm, x1 = 698.005 * wm;
                flag.rectangles.reserve(3);
                flag.rectangles.emplace_back(wool::RGBA(backgroundColour));
                flag.rectangles.emplace_back(triangleColour, FlagPoint(0.0f, 0.0f), FlagPoint(x0, y0), FlagPoint(0.0f, y0), FlagPoint(0.0f, 0.0f));
                flag.rectangles.emplace_back(triangleColour, FlagPoint(0.0f, y1), FlagPoint(x1, 1.0f), FlagPoint(0.0f, 1.0f), FlagPoint(0.0f, y1));
            }
            
            static void fourTriangles(Flag & flag, Random & random, const Pallete & pallete) {
                std::array<uint32_t, 4> colours;
                colours[0] = pallete.nextColour(random);
                colours[1] = pallete.nextColourAvoid(random, colours[0], 5);
                colours[2] = pallete.nextColourAvoid(random, colours[1], 5);
                colours[3] = pallete.nextColourAvoid(random, colours[2], 5);
                const unsigned int rotate = random.uintRange(0, 4);
                flag.rectangles.reserve(4);
                flag.rectangles.emplace_back(wool::RGBA(colours[(rotate + 0u) % 4u]), FlagPoint(1.0f, 0.0f), FlagPoint(1.0f, 1.0f), FlagPoint(0.5f, 0.5f), FlagPoint(1.0f, 0.0f));
                flag.rectangles.emplace_back(wool::RGBA(colours[(rotate + 1u) % 4u]), FlagPoint(0.0f, 0.0f), FlagPoint(1.0f, 0.0f), FlagPoint(0.5f, 0.5f), FlagPoint(0.0f, 0.0f));
                flag.rectangles.emplace_back(wool::RGBA(colours[(rotate + 2u) % 4u]), FlagPoint(0.0f, 0.0f), FlagPoint(0.5f, 0.5f), FlagPoint(0.0f, 1.0f), FlagPoint(0.0f, 0.0f));
                flag.rectangles.emplace_back(wool::RGBA(colours[(rotate + 3u) % 4u]), FlagPoint(0.0f, 1.0f), FlagPoint(0.5f, 0.5f), FlagPoint(1.0f, 1.0f), FlagPoint(0.0f, 1.0f));
            }
        }
        
        // Pattern list ------------------------------------------------
        
        namespace patterns {
            // Probability array for pattern selection (list of all available patterns).
            constexpr static ProbabilityArray<unsigned int, PatternFunc, 19> probabilityArray({
                { 5, &blank },
                { 40, &threeHorizontal }, // bars (add up to 120)
                { 40, &threeVertical },
                { 40, &fiveHorizontal },
                { 30, &centredSingleCross }, // crosses ( add up to 110)
                { 20, &centredDoubleCross },
                { 15, &offCentreLeftSingleCross },
                { 15, &offCentreLeftOutlinedCross },
                { 15, &offCentreRightSingleCross },
                { 15, &offCentreRightOutlinedCross },
                { 15, &threeBlocks },  // asymmetrical ( add up to 100)
                { 20, &leftTriangleTwoBars },
                { 25, &leftTriangleThreeBars },
                { 40, &leftTriangleFiveBars },
                { 30, &xShape }, // other shapes ( add up to 130)
                { 20, &divergingBars },
                { 30, &star },
                { 20, &doubleTriangle },
                { 30, &fourTriangles }
            });
        }
    }
    
    constexpr GLfloat Flag::ASPECT_RATIO;

    #define X(name, friendlyName) friendlyName,
    constexpr static std::array<const char *, Flag::CreateMode::COUNT> flagCreateModeNames = { FLAG_CREATE_MODE_LIST };
    #undef X
    
    // Internal helper so the same logic can be used for default flag and centred double-cross pattern.
    static void flagPopulateDefault(Flag & flag, const wool::RGBA & backgroundColour, const wool::RGBA & crossColour, const wool::RGBA & centreCrossColour) {
        flag.rectangles.reserve(5);
        
        flag.rectangles.emplace_back(backgroundColour);
        flag.rectangles.emplace_back(crossColour, 0.42f, 0.0f, 0.16f, 1.0f);
        flag.rectangles.emplace_back(crossColour, 0.0f, 0.34f, 1.0f, 0.32f);

        flag.rectangles.emplace_back(centreCrossColour, 0.46f, 0.25f, 0.08f, 0.5f);
        flag.rectangles.emplace_back(centreCrossColour, 0.375f, 0.42f, 0.25f, 0.16f);
    }
    
    Flag::Flag(void) :
        rectangles() {
        populateDefault();
    }
    
    Flag::Flag(Random & random, const unsigned int createMode) :
        rectangles() {
        switch(createMode) {
        case CreateMode::random:
            populateRandom(random);
            break;
        default:
            populateDefault();
            break;
        }
    }

    Flag::Flag(simplenetwork::Message & msg) :
        rectangles() {

        // Read the correct number of flag rectangles.
        uint64_t count = msg.read64();
        rectangles.reserve(count);
        for (uint64_t i = 0; i < count; i++) {
            rectangles.emplace_back(msg);
        }
    }
    
    void Flag::populateDefault(void) {
        flagPopulateDefault(*this, wool::RGBA::WHITE, wool::RGBA::RED, wool::RGBA::YELLOW);
    }
    
    void Flag::populateRandom(Random & random) {
        // Pick random pallete.
        const randomFlag::Pallete & pallete = *randomFlag::paletteData::probabilityArray.getValue(
            random.uintRange(0, randomFlag::paletteData::probabilityArray.getTotal()));
        
        // Pick random pattern function.
        const randomFlag::PatternFunc patternFunc = randomFlag::patterns::probabilityArray.getValue(
            random.uintRange(0, randomFlag::patterns::probabilityArray.getTotal()));
        
        // Populate the flag.
        patternFunc(*this, random, pallete);
    }

    void Flag::draw(const rascUI::Location & location) const {
        for (const FlagRect & rect : rectangles) {
            rect.draw(location);
        }
    }
    
    void Flag::writeTo(simplenetwork::Message & msg) const {
        // Write the number of rectangles we have.
        msg.write64((uint64_t)rectangles.size());
        // Write each rectangle to the message.
        for (FlagRect const & rect : rectangles) {
            rect.writeTo(msg);
        }
    }
    
    unsigned int Flag::friendlyNameToCreateMode(const char * friendlyName) {
        for (unsigned int createMode = 0; createMode < CreateMode::COUNT; createMode++) {
            if (std::strcmp(flagCreateModeNames[createMode], friendlyName) == 0) {
                return createMode;
            }
        }
        return CreateMode::defaultFlag;
    }
    
    const char * Flag::createModeToFriendlyName(unsigned int createMode) {
        return flagCreateModeNames[createMode];
    }
}

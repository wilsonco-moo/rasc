/*
 * FlagRect.cpp
 *
 *  Created on: 10 Dec 2018
 *      Author: wilson
 */

#include "FlagRect.h"

#include <rascUI/util/Location.h>
#include <sockets/plus/message/Message.h>

namespace rasc {

    FlagRect::FlagRect(wool::RGBA colour, GLfloat x, GLfloat y, GLfloat width, GLfloat height) :
        colourInt(colour.intValue()),
        colourRGBA(colour) {
        
        const float x2 = x + width,
                    y2 = y + height;
        
        points[0].x = x;
        points[0].y = y;
        
        points[1].x = x2;
        points[1].y = y;
        
        points[2].x = x2;
        points[2].y = y2;
        
        points[3].x = x;
        points[3].y = y2;
    }
    
    FlagRect::FlagRect(wool::RGBA colour, FlagPoint point0, FlagPoint point1, FlagPoint point2, FlagPoint point3) :
        colourInt(colour.intValue()),
        colourRGBA(colour),
        points({ point0, point1, point2, point3 }) {
    }

    FlagRect::FlagRect(simplenetwork::Message & msg) :
        colourInt(msg.read32()),
        colourRGBA(colourInt) {
        
        for (FlagPoint & point : points) {
            point = FlagPoint(msg);
        }
    }

    void FlagRect::draw(const rascUI::Location & drawFrom) const {

        // Set the appropriate colour.
        wool_setColourRGBA(colourRGBA);

        // Draw each point.
        for (const FlagPoint & point : points) {
            point.draw(drawFrom);
        }
    }

    void FlagRect::writeTo(simplenetwork::Message & msg) const {
        msg.write32(colourInt);
        for (const FlagPoint & point : points) {
            point.writeTo(msg);
        }
    }
    
    FlagPoint & FlagRect::getPoint(size_t index) {
        return points[index];
    }
    
    const FlagPoint & FlagRect::getPoint(size_t index) const {
        return points[index];
    }
    
    const wool::RGBA & FlagRect::getColour(void) const {
        return colourRGBA;
    }
    
    void FlagRect::setColour(const wool::RGBA & colour) {
        colourRGBA = colour;
        colourInt = colour.intValue();
    }
}

/*
 * FlagRect.h
 *
 *  Created on: 10 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_PLAYERDATA_FLAG_FLAGRECT_H_
#define BASEGAME_COMMON_PLAYERDATA_FLAG_FLAGRECT_H_

#include <GL/gl.h>
#include <cstddef>
#include <cstdint>
#include <array>

#include <wool/misc/RGBA.h>

#include "FlagPoint.h"

namespace simplenetwork {
    class Message;
}
namespace rascUI {
    class Location;
}

namespace rasc {

    /**
     * This represents a rectangle which can be drawn as part of a flag.
     */
    class FlagRect final {
    public:
        /**
         * Number of points in a rectangle.
         */
        constexpr static size_t POINTS = 4;
        
    private:
        // Keep colour as RGBA *and* int for convenience.
        uint32_t colourInt;
        wool::RGBA colourRGBA;
        
        // Array of points.
        std::array<FlagPoint, POINTS> points;
        
    public:
        /**
         * Construct from x, y, width, height.
         */
        FlagRect(wool::RGBA colour = wool::RGBA::BLUE, GLfloat x = 0.0f, GLfloat y = 0.0f, GLfloat width = 1.0f, GLfloat height = 1.0f);

        /**
         * Construct from a list of points.
         */
        FlagRect(wool::RGBA colour, FlagPoint point0, FlagPoint point1, FlagPoint point2, FlagPoint point3);
        
        /**
         * A flag rectangle can alternately be constructed from a message.
         */
        FlagRect(simplenetwork::Message & msg);

        /**
         * Draws the flag rectangle, using specified location.
         */
        void draw(const rascUI::Location & drawFrom) const;

        /**
         * A flag rectangle can be written to a message.
         */
        void writeTo(simplenetwork::Message & msg) const;
        
        /**
         * Get flag point (can modify it).
         */
        FlagPoint & getPoint(size_t index);
        const FlagPoint & getPoint(size_t index) const;
        
        /**
         * Get or set colour.
         */
        const wool::RGBA & getColour(void) const;
        void setColour(const wool::RGBA & colour);
    };

}

#endif

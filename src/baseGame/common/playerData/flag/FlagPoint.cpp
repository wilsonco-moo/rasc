/*
 * FlagPoint.cpp
 *
 *  Created on: 17 Sep 2023
 *      Author: wilson
 */

#include "FlagPoint.h"

#include <rascUI/util/Location.h>
#include <sockets/plus/message/Message.h>

namespace rasc {

    FlagPoint::FlagPoint(void) {
    }
    
    FlagPoint::FlagPoint(GLfloat x, GLfloat y) :
        x(x),
        y(y) {
    }

    FlagPoint::FlagPoint(simplenetwork::Message & msg) :
        x(msg.readFloat()),
        y(msg.readFloat()) {
    }

    void FlagPoint::writeTo(simplenetwork::Message & msg) const {
        msg.writeFloat(x);
        msg.writeFloat(y);
    }

    void FlagPoint::draw(const rascUI::Location & drawFrom) const {
        glVertex2f(
            drawFrom.cache.x + x * drawFrom.cache.width,
            drawFrom.cache.y + y * drawFrom.cache.height);
    }
}

/*
 * FlagComponent.h
 *
 *  Created on: 10 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_PLAYERDATA_FLAG_FLAG_H_
#define BASEGAME_COMMON_PLAYERDATA_FLAG_FLAG_H_

#include <GL/gl.h>
#include <vector>

#include "FlagRect.h"

namespace simplenetwork {
    class Message;
}

namespace rascUI {
    class Location;
}


namespace rasc {
    class Random;
    
    /**
     * This class represents a flag, as a series of rectangles.
     */
    class Flag final {
    public:
        /**
         * Flag create mode, when passed into the relevant constructor,
         * specifies how the flag should be generated.
         * Default flag is the one used in all Rasc versions before 2.056.
         * Random flag is different each time.
         * Format: Name, friendly name.
         */
        #define FLAG_CREATE_MODE_LIST \
            X(defaultFlag, "Default") \
            X(random, "Random")

        #define X(name, ...) name,
        class CreateMode { public: enum { FLAG_CREATE_MODE_LIST COUNT }; };
        #undef X
        
        /**
         * Flags can be drawn at any size, but should be drawn to this approximate
         * aspect ratio, (width / height).
         */
        constexpr static GLfloat ASPECT_RATIO = 2.0f;

        std::vector<FlagRect> rectangles;

        /**
         * Default constructor: Generates the "default" flag used in all Rasc
         * versions before 2.056.
         */
        Flag(void);
        
        /**
         * Creates the flag according to the specified create mode, (see
         * documentation for that).
         */
        Flag(Random & random, const unsigned int createMode);
        
        /**
         * This constructor allows a flag to be created from a message.
         */
        Flag(simplenetwork::Message & msg);

    private:
        // Used internally within constructors, populates flag with either
        // default or random data.
        void populateDefault(void);
        void populateRandom(Random & random);
        
    public:
        /**
         * Here we simply draw all of our flag rectangles.
         */
        void draw(const rascUI::Location & location) const;

        /**
         * A flag component can be written to a message also.
         */
        void writeTo(simplenetwork::Message & msg) const;
        
        /**
         * Convert back-and-to between friendly name for create mode
         * and create mode.
         */
        static unsigned int friendlyNameToCreateMode(const char * friendlyName);
        static const char * createModeToFriendlyName(unsigned int createMode);
    };

}

#endif

/*
 * FlagPoint.h
 *
 *  Created on: 17 Sep 2023
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_PLAYERDATA_FLAG_FLAGPOINT_H_
#define BASEGAME_COMMON_PLAYERDATA_FLAG_FLAGPOINT_H_

#include <GL/gl.h>

namespace simplenetwork {
    class Message;
}
namespace rascUI {
    class Location;
}

namespace rasc {

    /**
     * FlagPoint is an x, y point within a flag rectangle. Flag rectangles contain four of these.
     * This class contains utility methods to read, write and draw points.
     */
    class FlagPoint final {
    public:
        /**
         * x and y coordinate.
         */
        GLfloat x, y;
        
        /**
         * Default construct (leaves coordinates uninitialised!).
         */
        FlagPoint(void);
        
        /**
         * Construct from x, y coordinates.
         */
        FlagPoint(GLfloat x, GLfloat y);
        
        /**
         * Read from message.
         */
        FlagPoint(simplenetwork::Message & msg);
        
        /**
         * Write to message.
         */
        void writeTo(simplenetwork::Message & msg) const;
        
        /**
         * Draw the point (within specified rascUI location).
         */
        void draw(const rascUI::Location & drawFrom) const;
    };
}

#endif

/*
 * PlayerDataSystem.h
 *
 *  Created on: 8 Mar 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_PLAYERDATA_PLAYERDATASYSTEM_H_
#define BASEGAME_COMMON_PLAYERDATA_PLAYERDATASYSTEM_H_

#include <functional>
#include <cstdint>
#include <string>

#include "../update/updateTypes/MiscUpdate.h"
#include "../update/RascUpdatable.h"

namespace rasc {

    class CommonBox;
    class UniqueIdAssigner;
    class PlayerData;

    /**
     * This class stores a PlayerData instance for each active player.
     *
     * When we receive an initial update, or misc update, about a child which does not exist, then:
     *  > We assume the message contains initial data about that child, so we create it.
     * This means that when creating a new PlayerData, we can just use it's onGenerateInitialData method to send
     * data about it. Since this is done in a misc update, it means we can bundle "new PlayerData" updates
     * along with other misc updates like deleting a PlayerData.
     */
    class PlayerDataSystem : public RascUpdatable {
    private:
        friend class PlayerData;

        static const char * const aiNames[];

        class MiscUpdateTypes {
        public:
            enum {
                deletePlayerData,
                createPlayerData
            };
        };
        typedef uint8_t MiscUpdateType;

        // This std::function is run each time we receive initial data or a misc update.
        std::function<void(void)> onUpdate;

        // This is required so that we can notify the stat system when players
        // are created and when players are removed.
        // Also, PlayerData instances need access to this so they can access the
        // stat system.
        CommonBox & box;
        
        // Flag create mode for human and AI players.
        unsigned int humanFlagCreateMode, aiFlagCreateMode;

    public:

        static const char * const DEFAULT_HUMAN_PLAYER_NAME;

        PlayerDataSystem(CommonBox & box);
        virtual ~PlayerDataSystem(void);

    protected:

        virtual bool onReceiveMiscUpdate(simplenetwork::Message & msg) override;

        // When we receive an initial update about a missing child, then we must create it. This happens
        // when a client initially joins, or a server loads the map.
        virtual bool onInitialDataMissingChild(simplenetwork::Message & msg, uint64_t childId) override;

    public:
        const char * getRandomName(void);

        /**
         * These methods create a new player data object, register it internally, and return the new
         * PlayerData's id. If called with a server update, the new PlayerData instance can be immediately used.
         */
        uint64_t newAIPlayerData(MiscUpdate update, UniqueIdAssigner * playerIdAssigner);
        uint64_t newHumanPlayerData(MiscUpdate update, UniqueIdAssigner * playerIdAssigner);

        /**
         * This should be used by the server to delete player data.
         */
        void deletePlayerData(MiscUpdate update, uint64_t playerId);

        // ---------------------------- Lookup methods ----------------------

        /**
         * These functions allow convenient access to PlayerData instances. This avoids
         * casting being necessary in code using the player data system.
         */
        inline PlayerData * idToData(uint64_t playerId) {
            return (PlayerData *)childForwardLookup(playerId);
        }
        inline PlayerData * idToDataChecked(uint64_t playerId) {
            return (PlayerData *)childForwardLookupChecked(playerId);
        }

        // --------------------------- Misc stuff --------------------------

        /**
         * This should be used by the client side player list, so it can update itself when we receive updates.
         */
        inline void setOnUpdateFunc(std::function<void(void)> onUpdate) {
            this->onUpdate = onUpdate;
        }
    };
}

#endif

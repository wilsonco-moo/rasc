/*
 * PlayerData.h
 *
 *  Created on: 8 Mar 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_PLAYERDATA_PLAYERDATA_H_
#define BASEGAME_COMMON_PLAYERDATA_PLAYERDATA_H_

#include <cstdint>
#include <string>

#include <wool/misc/RGBA.h>

#include "../gameMap/provinceUtil/ProvinceStatTypes.h"
#include "../update/updateTypes/CheckedUpdate.h"
#include "../update/updateTypes/MiscUpdate.h"
#include "info/ThreatManagedWeights.h"
#include "../util/numeric/MultiCol.h"
#include "../update/RascUpdatable.h"
#include "PlayerStatArr.h"
#include "PlayerStatDef.h"
#include "flag/Flag.h"

namespace rasc {

    class UniqueIdAssigner;
    class PlayerDataSystem;
    class ProvStatModSet;
    class PlayerStatArr;
    class ProvStatMod;
    class CommonBox;

    /**
     * A PlayerData instance stores all of the useful and globally visible information about a single player.
     * Note that PlayerData does not store (yet?) whether a player is AI or human, that is up to the server to control.
     *
     * A PlayerData instance remains existing for as long as the player.
     */
    class PlayerData : public RascUpdatable {
        friend class PlayerDataSystem;
    private:

        class MiscUpdateTypes {
        public:
            enum {
                changeName,
                changeColour,
                changeTurnStatus,
                changeTotalManpower,
                changeTotalCurrency,
                changeTotalStats,
                turnMiscUpdate
            };
        };
        typedef uint8_t MiscUpdateType;

        class CheckedUpdateTypes {
        public:
            enum {
                requestChangeName
            };
        };
        typedef uint8_t CheckedUpdateType;

        // This is required so that we can access stats easily, for working
        // out per-turn stats when requested.
        CommonBox & box;

        // Our player id.
        uint64_t id;

        // Our player colour. The main copy is an integer. For convenience, we keep
        // two more copies generated from this: A wool::RGBA and a version converted
        // to the LAB colour space.
        uint32_t colourInt;
        wool::RGBA colour;
        MultiCol labColour;
        
        // Our current flag, either generated according to flag create mode, or
        // saved/read from the message.
        Flag flag;

        // Our name and whether we have finished our turn.
        std::string name;
        bool finishedTurn;
        
        // Our total cumulative player stats, (manpower and currency). Note that the
        // per-turn values are never stored: They can be worked out from the stat
        // system trivially easily, (hence why we store a pointer to the CommonBox).
        PlayerStatArr stats;

        // Our weights, for decision making, for the AI.
        ThreatManagedWeights weights;

    public:

        /**
         * This represents a player id, pointing to no particular player.
         * In the case of province ownership, this represents unowned.
         */
        constexpr static uint64_t NO_PLAYER = 0xffffffffffffffff;

        /**
         * The number of turns worth of manpower gain, which total manpower
         * is capped at.
         */
        constexpr static unsigned int MANPOWER_CAP_TURNS = 4;

        
        // ====================================================================================

        /**
         * This creates a PlayerData instance with an automatically picked name and colour.
         * This constructor should be used by the server when creating AI players.
         * Flag is generated according to specified create mode.
         */
        PlayerData(UniqueIdAssigner * playerIdAssigner, CommonBox & box, unsigned int flagCreateMode);

        /**
         * This creates a PlayerData instance with a specified name, but an automatically picked colour.
         * This constructor should be used by the server when creating human players.
         * Flag is generated according to specified create mode.
         */
        PlayerData(UniqueIdAssigner * playerIdAssigner, CommonBox & box, unsigned int flagCreateMode, const char * name);

        /**
         * The constructor toe create a PlayerData instance from initial data. This should only be used by
         * PlayerDataSystem, when receiving initial data.
         */
        PlayerData(uint64_t id, simplenetwork::Message & msg, CommonBox & box);
        virtual ~PlayerData(void);

    private:
        /**
         * This should write the PlayerData's initial data, without writing an update header.
         * This should only be used by PlayerDataSystem, when writing the initial data of a new PlayerData, which is not assigned
         * into the hierarchy yet.
         *
         * This will write only the data portion of this PlayerData, i.e: Without any id information.
         */
        void writeInitialDataWithoutHeader(simplenetwork::Message & msg);

        // Called internally when we receive stat change misc updates.
        void onReceiveChangeTotalManpower(simplenetwork::Message & msg);
        void onReceiveChangeTotalCurrency(simplenetwork::Message & msg);
        void onReceiveChangeTotalStats(simplenetwork::Message & msg);
        
        // This is called internally when we receive a message written by our onGenerateTurnMiscUpdate.
        void onReceiveTurnMiscUpdate(simplenetwork::Message & msg);

    protected:
        virtual void onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) override;
        virtual bool onReceiveMiscUpdate(simplenetwork::Message & msg) override;
        virtual bool onReceiveCheckedUpdate(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) override;

    public:

        // -------------------------- Public update methods -----------------------

        /**
         * Writes a misc update, which updates all per-turn stats, like manpower and currency.
         * When this is received, the balance should be added to the total value.
         * Note that this does not actually need to send the balance, since the balance
         * must always be worked out from stats.
         */
        virtual void onGenerateTurnMiscUpdate(MiscUpdate update) override;

        // -------------------- Stat based accessor methods ----------------------

        /**
         * Returns, for currency, the income, expenditure and balance.
         * Currently, the income is currency from provinces, (from AreasStat),
         * and expenditure is spent on armies only, (from UnitsDeployedStat).
         */
        provstat_t getCurrencyIncomePerTurn(void) const;
        provstat_t getCurrencyExpenditurePerTurn(void) const;
        provstat_t getCurrencyBalancePerTurn(void) const;

        /**
         * Returns, for manpower, the income, expenditure and balance.
         * Currently, the income is from provinces, and there is never any
         * manpower expenditure.
         */
        provstat_t getManpowerIncomePerTurn(void) const;
        provstat_t getManpowerExpenditurePerTurn(void) const;
        provstat_t getManpowerBalancePerTurn(void) const;

        /**
         * Returns, for land, the total available and used land which we have.
         * Both of these are calculated directly from provinces (AreasStat).
         * Surplus (unused) land is available land minus used land.
         */
        provstat_t getTotalLandAvailable(void) const;
        provstat_t getTotalLandUsed(void) const;
        provstat_t getTotalLandSurplus(void) const;
        
        /**
         * Returns the amount of manpower currently in use as a manpower cap.
         */
        provstat_t getManpowerCap(void) const;
        
        // --------------------- General accessor methods ------------------------

        /**
         * Returns the ID of this player data. This can never change.
         */
        uint64_t getId(void) const;

        /**
         * Returns the colour of this player, as an integer.
         */
        uint32_t getColourInt(void) const;

        /**
         * Returns the colour of this player, as a wool::RGBA.
         * This always changes alongside with, and remains consistent with, our integer colour.
         */
        const wool::RGBA & getColour(void) const;

        /**
         * Returns the colour of this player, as a MultiCol, in the LAB colour space.
         * This is helpful for comparing the visual difference between player colours.
         * See MultiCol.h for more information.
         * This always changes alongside with, and remains consistent with, our integer colour.
         */
        const MultiCol & getColourLab(void) const;
        
        /**
         * Returns our current flag.
         */
        const Flag & getFlag(void) const;

        /**
         * Returns our current player name.
         */
        const std::string & getName(void) const;

        /**
         * Returns true if this player has finished their turn, or false otherwise.
         */
        const bool getHaveFinishedTurn(void) const;

        /**
         * Returns our threat managed weights. This is for the AI, for decision-making.
         */
        const ThreatManagedWeights & getWeights(void) const;
        
        /**
         * Returns the total amount of stored manpower, owned by this player.
         * For income/expenditure/balance values, see the getManpower*PerTurn methods.
         */
        provstat_t getTotalManpower(void) const;
        
        /**
         * Returns the total amount of stored currency, owned by this player.
         * For income/expenditure/balance values, see the getCurrency*PerTurn methods.
         */
        provstat_t getTotalCurrency(void) const;

        /**
         * Returns true if we have enough manpower and currency to afford the
         * provided building cost.
         */
        bool canAffordBuilding(const PlayerStatArr & buildingCost) const;

        // --------- Methods to extract string representations of behaviour values ---------

        /**
         * Each method, (like getAggressivenessStr), returns a string representation
         * of the relevant behaviour value, such as:
         * "very aggressive (93)"
         * "mildly reckless (24)"
         */
        #define X(id, name, defaultValue, antonym, verb) \
            std::string get##name##Str(void) const;
        TM_STORED_BEHAVIOUR_VALUES
        #undef X

        // ============================ MISC UPDATE METHODS ===============================

        /**
         * This is how the client (and server) should modify PlayerData properties.
         */
        void changeName(MiscUpdate update, const std::string & newName);
        void changeColour(MiscUpdate update, uint32_t newColour);
        void changeHaveFinishedTurn(MiscUpdate update, bool finishedTurn);

        /**
         * For stats like manpower and currency (cumulative player stats), only the total
         * value can be changed manually. The per-turn values are calculated from the stat system.
         * To change them, a change reason is needed (this is provided to the unified stat system).
         */
        void changeTotalManpower(MiscUpdate update, provstat_t newValue, PlayerStatChangeReason changeReason);
        void changeTotalCurrency(MiscUpdate update, provstat_t newValue, PlayerStatChangeReason changeReason);
        void changeTotalStats(MiscUpdate update, const PlayerStatArr & newStats, PlayerStatChangeReason changeReason);

        // ------------------ Alternate (convenience) methods -----------------

        /**
         * Methods to add manpower and currency, and a method to subtract
         * building costs. These are more convenient than the equivalent
         * changeTotal* methods. A change reason is also needed for these.
         */
        void addTotalManpower(MiscUpdate update, provstat_t valueToAdd, PlayerStatChangeReason changeReason);
        void addTotalCurrency(MiscUpdate update, provstat_t valueToAdd, PlayerStatChangeReason changeReason);
        /**
         * Subtracts the building cost from our total manpower and currency. This automatically uses
         * the constructBuilding player stat change reason.
         */
        void spendBuildingCost(MiscUpdate update, const PlayerStatArr & buildingCost);

        // ============================ CHECKED UPDATE METHODS ============================

        /**
         * This method should be used by the client to request a change to their name.
         */
        void requestChangeName(CheckedUpdate update, const std::string & newName);
    };
}

#endif

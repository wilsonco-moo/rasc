/*
 * PlayerData.cpp
 *
 *  Created on: 8 Mar 2019
 *      Author: wilson
 */

#include "PlayerData.h"

#include <sockets/plus/message/Message.h>
#include <algorithm>
#include <iostream>
#include <sstream>

#include "../gameMap/provinceUtil/ProvStatMod.h"
#include "../stats/types/UnitsDeployedStat.h"
#include "../util/numeric/UniqueIdAssigner.h"
#include "../stats/UnifiedStatSystem.h"
#include "../stats/types/AreasStat.h"
#include "../util/numeric/MultiCol.h"
#include "../../server/ServerBox.h"
#include "PlayerDataSystem.h"
#include "PlayerStatArr.h"

namespace rasc {

    PlayerData::PlayerData(UniqueIdAssigner * playerIdAssigner, CommonBox & box, unsigned int flagCreateMode) :
        PlayerData(playerIdAssigner, box, flagCreateMode, box.playerDataSystem->getRandomName()) {
    }

    PlayerData::PlayerData(UniqueIdAssigner * playerIdAssigner, CommonBox & box, unsigned int flagCreateMode, const char * name) :
        RascUpdatable(),
        box(box),
        id(playerIdAssigner->newId()),

        // Generate int colour, initialise other two copies from this.
        // Use the method to pick perceptually uniform random colours.
        colourInt(MultiCol::perceptUniformRandomColInt(*box.random)),
        colour(colourInt),
        labColour(colourInt),
        
        // Generate flag according to create mode (using random generator from box).
        flag(*box.random, flagCreateMode),

        name(name),
        finishedTurn(false),
        stats(),
        weights(box) {

        // Convert from RGB to LAB for the LAB colour space copy.
        labColour.rgbToXyz(); labColour.xyzToLab();
    }

    PlayerData::PlayerData(uint64_t id, simplenetwork::Message & msg, CommonBox & box) :
        RascUpdatable(),
        box(box),
        id(id),

        // Read int colour, initialise other two copies from this.
        colourInt(msg.read32()),
        colour(colourInt),
        labColour(colourInt),
        
        flag(msg),
        name(msg.readString()),
        finishedTurn(msg.readBool()),
        stats(msg),
        weights(msg) {

        // Convert from RGB to LAB for the LAB colour space copy.
        labColour.rgbToXyz(); labColour.xyzToLab();
    }

    PlayerData::~PlayerData(void) {
    }

    void PlayerData::writeInitialDataWithoutHeader(simplenetwork::Message & msg) {
        msg.write32(colourInt);
        flag.writeTo(msg);
        msg.writeString(name);
        msg.writeBool(finishedTurn);
        stats.writeTo(msg);
        weights.writeTo(msg);
    }

    void PlayerData::onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) {
        writeUpdateHeader(msg);
        writeInitialDataWithoutHeader(msg);
    }

    bool PlayerData::onReceiveMiscUpdate(simplenetwork::Message & msg) {
        MiscUpdateType update = msg.read8();
        switch(update) {
        case MiscUpdateTypes::changeName:
            name = msg.readString();
            break;
        case MiscUpdateTypes::changeColour: {
            // Keep a copy of our old colour, for calling the onChangePlayerColour method.
            uint32_t oldColourInt = colourInt;
            MultiCol oldLabColour = labColour;

            // Read int colour, initialise other two copies from this.
            colourInt = msg.read32();
            colour = wool::RGBA(colourInt);
            labColour = MultiCol(colourInt);
            labColour.rgbToXyz(); labColour.xyzToLab(); // Convert from RGB to LAB for the LAB colour space copy.

            // Notify the stat system, since our colour has changed: The last stage in changing
            // the colour of a player. By this point we assume that the colour of all our provinces
            // has already changed.
            if (oldColourInt != colourInt) {
                box.statSystem->onChangePlayerColour(id, oldColourInt, oldLabColour, colourInt, labColour);
            }
            break;
        }
        case MiscUpdateTypes::changeTurnStatus:
            finishedTurn = msg.readBool();
            break;
        case MiscUpdateTypes::changeTotalManpower:
            onReceiveChangeTotalManpower(msg);
            break;
        case MiscUpdateTypes::changeTotalCurrency:
            onReceiveChangeTotalCurrency(msg);
            break;
        case MiscUpdateTypes::changeTotalStats:
            onReceiveChangeTotalStats(msg);
            break;
        case MiscUpdateTypes::turnMiscUpdate:
            onReceiveTurnMiscUpdate(msg);
            break;
        default:
            std::cerr << "ERROR: PlayerData: Unknown misc update type " << (unsigned int)update << ".\n";
            return false;
        }
        // Since we have received an update, run the PlayerDataSystem's update lambda.
        ((PlayerDataSystem *)getRascUpdatableParent())->onUpdate();
        return true;
    }

    bool PlayerData::onReceiveCheckedUpdate(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) {
        CheckedUpdateType update = checkedUpdate.read8();
        switch(update) {
            case CheckedUpdateTypes::requestChangeName: {
                std::string newName = checkedUpdate.readString();
                // If the player is trying to change the name of another player, ignore the request.
                if (getId() != contextPlayer->getId()) break;
                // If the name is longer than 12 characters, ignore characters after that.
                if (newName.length() > 12) newName = newName.substr(0, 12);
                // Finally write the misc update to change the name.
                changeName(MiscUpdate::client(miscUpdate), newName);
                break;
            }
            default:
                std::cerr << "ERROR: PlayerData: Unknown checked update type " << (unsigned int)update << ".\n";
                return false;
        }
        return true;
    }

    #define X(id, name, defaultValue, antonym, verb)                                        \
        std::string PlayerData::get##name##Str(void) const {                                \
            std::stringstream output;                                                       \
            unsigned int value = getWeights().get##name(),                                  \
                         centreVal;                                                         \
            /* Make centreVal how far it is from middle.                                 */ \
            if (value < 50) {                                                               \
                centreVal = 50 - value;                                                     \
            } else {                                                                        \
                centreVal = value - 50;                                                     \
            }                                                                               \
            /* Print describe word based on distance from centre.                        */ \
            switch(centreVal / 17) {                                                        \
            case 0:                                                                         \
                output << "slightly ";                                                      \
                break;                                                                      \
            case 1:                                                                         \
                output << "mildly ";                                                        \
                break;                                                                      \
            case 2:                                                                         \
                output << "very ";                                                          \
                break;                                                                      \
            }                                                                               \
            /* Print either antonym or verb depending on whether above or below 50.      */ \
            if (value < 50) {                                                               \
                output << #antonym " (";                                                    \
            } else {                                                                        \
                output << #verb " (";                                                       \
            }                                                                               \
            /* Add numeric value in brackets.                                            */ \
            output << value << ')';                                                         \
            /* Convert output to a string and return it.                                 */ \
            return output.str();                                                            \
        }
    TM_STORED_BEHAVIOUR_VALUES
    #undef X

    void PlayerData::changeName(MiscUpdate update, const std::string & newName) {
        writeUpdateHeader(update.msg());
        update.msg().write8(MiscUpdateTypes::changeName);
        update.msg().writeString(newName);
    }
    void PlayerData::changeColour(MiscUpdate update, uint32_t newColour) {
        writeUpdateHeader(update.msg());
        update.msg().write8(MiscUpdateTypes::changeColour);
        update.msg().write32(newColour);
    }
    void PlayerData::changeHaveFinishedTurn(MiscUpdate update, bool finishedTurn) {
        writeUpdateHeader(update.msg());
        update.msg().write8(MiscUpdateTypes::changeTurnStatus);
        update.msg().writeBool(finishedTurn);
    }
    void PlayerData::changeTotalManpower(MiscUpdate update, provstat_t newValue, PlayerStatChangeReason changeReason) {
        writeUpdateHeader(update.msg());
        update.msg().write8(MiscUpdateTypes::changeTotalManpower);
        PROVINCE_STAT_WRITE(update.msg(), newValue);
        PLAYER_STAT_CHANGE_REASON_WRITE(update.msg(), changeReason);
    }
    void PlayerData::changeTotalCurrency(MiscUpdate update, provstat_t newValue, PlayerStatChangeReason changeReason) {
        writeUpdateHeader(update.msg());
        update.msg().write8(MiscUpdateTypes::changeTotalCurrency);
        PROVINCE_STAT_WRITE(update.msg(), newValue);
        PLAYER_STAT_CHANGE_REASON_WRITE(update.msg(), changeReason);
    }
    void PlayerData::changeTotalStats(MiscUpdate update, const PlayerStatArr & newStats, PlayerStatChangeReason changeReason) {
        writeUpdateHeader(update.msg());
        update.msg().write8(MiscUpdateTypes::changeTotalStats);
        newStats.writeTo(update.msg());
        PLAYER_STAT_CHANGE_REASON_WRITE(update.msg(), changeReason);
    }
    void PlayerData::addTotalManpower(MiscUpdate update, provstat_t valueToAdd, PlayerStatChangeReason changeReason) {
        writeUpdateHeader(update.msg());
        update.msg().write8(MiscUpdateTypes::changeTotalManpower);
        PROVINCE_STAT_WRITE(update.msg(), getTotalManpower() + valueToAdd);
        PLAYER_STAT_CHANGE_REASON_WRITE(update.msg(), changeReason);
    }
    void PlayerData::addTotalCurrency(MiscUpdate update, provstat_t valueToAdd, PlayerStatChangeReason changeReason) {
        writeUpdateHeader(update.msg());
        update.msg().write8(MiscUpdateTypes::changeTotalCurrency);
        PROVINCE_STAT_WRITE(update.msg(), getTotalCurrency() + valueToAdd);
        PLAYER_STAT_CHANGE_REASON_WRITE(update.msg(), changeReason);
    }
    void PlayerData::spendBuildingCost(MiscUpdate update, const PlayerStatArr & buildingCost) {
        writeUpdateHeader(update.msg());
        update.msg().write8(MiscUpdateTypes::changeTotalStats);
        (stats - buildingCost).writeTo(update.msg());
        PLAYER_STAT_CHANGE_REASON_WRITE(update.msg(), PlayerStatChangeReasons::constructBuilding);
    }

    void PlayerData::onGenerateTurnMiscUpdate(MiscUpdate update) {
        writeUpdateHeader(update.msg());
        update.msg().write8(MiscUpdateTypes::turnMiscUpdate);
    }

    void PlayerData::requestChangeName(CheckedUpdate update, const std::string & newName) {
        writeUpdateHeader(update.msg());
        update.msg().write8(CheckedUpdateTypes::requestChangeName);
        update.msg().writeString(newName);
    }

    provstat_t PlayerData::getCurrencyIncomePerTurn(void) const {
        // Count only currency income from provinces.
        return box.areasStat->getPlayerCurrencyIncome(id);
    }
    provstat_t PlayerData::getCurrencyExpenditurePerTurn(void) const {
        // Count currency expenditure for armies AND for provinces.
        return box.unitsDeployedStat->getArmyExpenditure(id) + box.areasStat->getPlayerCurrencyExpenditure(id);
    }
    provstat_t PlayerData::getCurrencyBalancePerTurn(void) const {
        // Currency balance is income minus expenditure.
        return getCurrencyIncomePerTurn() - getCurrencyExpenditurePerTurn();
    }
    provstat_t PlayerData::getManpowerIncomePerTurn(void) const {
        // Count only manpower income from provinces.
        return box.areasStat->getPlayerManpowerIncome(id);
    }
    provstat_t PlayerData::getManpowerExpenditurePerTurn(void) const {
        // Count only manpower expenditure in provinces.
        return box.areasStat->getPlayerManpowerExpenditure(id);
    }
    provstat_t PlayerData::getManpowerBalancePerTurn(void) const {
        // Manpower balance is income minus expenditure.
        return getManpowerIncomePerTurn() - getManpowerExpenditurePerTurn();
    }
    provstat_t PlayerData::getTotalLandAvailable(void) const {
        return box.areasStat->getPlayerLandAvailable(id);
    }
    provstat_t PlayerData::getTotalLandUsed(void) const {
        return box.areasStat->getPlayerLandUsed(id);
    }
    provstat_t PlayerData::getTotalLandSurplus(void) const {
        return getTotalLandAvailable() - getTotalLandUsed();
    }
    provstat_t PlayerData::getManpowerCap(void) const {
        return std::max((provstat_t)0, MANPOWER_CAP_TURNS * getManpowerBalancePerTurn());
    }
    
    uint64_t PlayerData::getId(void) const {
        return id;
    }
    uint32_t PlayerData::getColourInt(void) const {
        return colourInt;
    }
    const wool::RGBA & PlayerData::getColour(void) const {
        return colour;
    }
    const MultiCol & PlayerData::getColourLab(void) const {
        return labColour;
    }
    const Flag & PlayerData::getFlag(void) const {
        return flag;
    }
    const std::string & PlayerData::getName(void) const {
        return name;
    }
    const bool PlayerData::getHaveFinishedTurn(void) const {
        return finishedTurn;
    }
    const ThreatManagedWeights & PlayerData::getWeights(void) const {
        return weights;
    }
    provstat_t PlayerData::getTotalManpower(void) const {
        return stats[PlayerStatIds::manpower];
    }
    provstat_t PlayerData::getTotalCurrency(void) const {
        return stats[PlayerStatIds::currency];
    }

    bool PlayerData::canAffordBuilding(const PlayerStatArr & buildingCost) const {
        return getTotalManpower() >= buildingCost[PlayerStatIds::manpower] && getTotalCurrency() >= buildingCost[PlayerStatIds::currency];
    }

    void PlayerData::onReceiveChangeTotalManpower(simplenetwork::Message & msg) {
        // Get old manpower, then new manpower and change reason from message.
        provstat_t oldManpower = stats[PlayerStatIds::manpower],
                   newManpower = PROVINCE_STAT_READ(msg);
        PlayerStatChangeReason changeReason = PLAYER_STAT_CHANGE_REASON_READ(msg);
        
        // If manpower has changed, work out stat change, update our local value, notify stat system.
        if (oldManpower != newManpower) {
            PlayerStatArr statChange;
            statChange[PlayerStatIds::manpower] = (newManpower - oldManpower);
            stats[PlayerStatIds::manpower] = newManpower;
            box.statSystem->onChangePlayerStats(id, statChange, changeReason);
        }
    }
    
    void PlayerData::onReceiveChangeTotalCurrency(simplenetwork::Message & msg) {
        // Get old currency, then new currency and change reason from message.
        provstat_t oldCurrency = stats[PlayerStatIds::currency],
                   newCurrency = PROVINCE_STAT_READ(msg);
        PlayerStatChangeReason changeReason = PLAYER_STAT_CHANGE_REASON_READ(msg);
        
        // If currency has changed, work out stat change, update our local value, notify stat system.
        if (oldCurrency != newCurrency) {
            PlayerStatArr statChange;
            statChange[PlayerStatIds::currency] = (newCurrency - oldCurrency);
            stats[PlayerStatIds::currency] = newCurrency;
            box.statSystem->onChangePlayerStats(id, statChange, changeReason);
        }
    }
    
    void PlayerData::onReceiveChangeTotalStats(simplenetwork::Message & msg) {
        // Get new stats and change reason from message.
        PlayerStatArr newStats(msg);
        PlayerStatChangeReason changeReason = PLAYER_STAT_CHANGE_REASON_READ(msg);
        
        // If stats have changed, work out stat change, update our local value, notify stat system.
        if (stats != newStats) {
            PlayerStatArr statChange = newStats - stats;
            stats = newStats;
            box.statSystem->onChangePlayerStats(id, statChange, changeReason);
        }
    }
    
    void PlayerData::onReceiveTurnMiscUpdate(simplenetwork::Message & msg) {
        // For notifying stats later, record old stat values and get army expenditure.
        const PlayerStatArr oldStats = stats;
        const provstat_t armyExpenditure = box.unitsDeployedStat->getArmyExpenditure(id);
        
        // Update stats (manually add army expenditure to avoid looking it up in stats twice).
        stats[PlayerStatIds::manpower] += getManpowerBalancePerTurn();
        stats[PlayerStatIds::currency] += (getCurrencyIncomePerTurn() - armyExpenditure - box.areasStat->getPlayerCurrencyExpenditure(id));

        // Make sure total manpower does not exceed manpower cap.
        stats[PlayerStatIds::manpower] = std::min(stats[PlayerStatIds::manpower], getManpowerCap());
        
        // Now notify stat system.
        
        // First notify stat system about army maintenence separately (this is negative!). If non zero
        // tell stat system about it, then add it to overall stat change to negate it.
        PlayerStatArr statChange = stats - oldStats;
        if (armyExpenditure != 0) {
            PlayerStatArr armyExpenditureArr;
            armyExpenditureArr[PlayerStatIds::currency] = -armyExpenditure;
            box.statSystem->onChangePlayerStats(id, armyExpenditureArr, PlayerStatChangeReasons::armyMaintenance);
            statChange[PlayerStatIds::currency] += armyExpenditure;
        }
        
        // If, without the army expenditure, stats have actually changed, notify stat system about the rest
        // our our currency balance, (label it income from provinces).
        if (!statChange.isZero()) {
            box.statSystem->onChangePlayerStats(id, statChange, PlayerStatChangeReasons::incomeFromProvinces);
        }
    }
}

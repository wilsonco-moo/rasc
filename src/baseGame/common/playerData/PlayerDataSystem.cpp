/*
 * PlayerDataSystem.cpp
 *
 *  Created on: 8 Mar 2019
 *      Author: wilson
 */

#include "PlayerDataSystem.h"

#include <unordered_map>
#include <cstdlib>
#include <cstdio>

#include <sockets/plus/message/Message.h>

#include "../update/updateTypes/ProvinceUpdate.h"
#include "../stats/UnifiedStatSystem.h"
#include "../util/numeric/Random.h"
#include "../config/RascConfig.h"
#include "../util/CommonBox.h"
#include "../util/XMLUtil.h"
#include "../util/Misc.h"
#include "PlayerData.h"
#include "flag/Flag.h"

namespace rasc {

    // Don't make these names any longer than 12 characters. Also only put 10 per line for easier counting.
    const char * const PlayerDataSystem::aiNames[] = {
        "Almaz", "Dave", "Stan", "Toto", "Tim Rubert", "Zeebo", "Calpurnia", "Mr. C", "Tituba", "Elart Mask",
        "Paul", "Skeet", "Bob", "Barry", "Cooper", "Dandy", "Ewart McGreg", "Flum-dubbo", "Gazzer", "Humbibble",
        "Invertebrate", "Jurgen", "Krum", "Liberty", "Munch", "Nibble", "OpenGL", "Plib-Plab", "Qertunsh", "Reginald",
        "Stench", "Thule", "Um Bongo", "Vulmet", "Weirding Way", "X-illyphon", "Yotta", "Zoom", "Coningsby", "Reee",
        "Blin", "Lemon", "Laos", "Deirdry", "Geoff", "Jeoffry", "Jeffery", "Phil J.", "Cornelius", "Bert McMann",
        "Frank Side", "Pinkles", "Krem", "Larg", "Lard", "Soy Milk", "Uganda", "Gambia", "Zimbabwe", "Congo",
        "Splenge", "Hungary", "Azerty", "Qwerty", "Dvorak", "ABCD", "Atticus", "G.Cole", "Cremme", "Skree",
        "Pain", "Seebo", "Windum", "Africa", "Slunge", "Dishcloth", "Geneva", "Washington", "Diffuse BSDF", "Meldge",
        "Phasso", "Yeet", "Meme Man", "Chrome Lad", "Nelmengus" ,"Slump" , "Stefan", "Karlssonn", "Margerine", "Eggy Bill",
        "The Shwartz", "Olifant", "Centurion", "Squint", "Flump", "Hubert", "Pedersen","Madsen", "Shwartzloze", "Maxim",
        "Boris", "Hofmeyr", "Kahn", "Kwembe", "Augustas", "Adgdgdgwengo", "Gestank", "Der Duft", "Endlos" ,"Farbton",
        "Kubrick", "Clarke", "HAL", "Ape", "Monolith", "Jupiter", "Moon", "Danube", "Clavius" ,"Discovery",
        "Crisp", "Krosps", "Rooshan", "Chimp", "Bonobo", "Baboon", "Monke", "Stonks", "Bach" ,"Wensleydale",
        "Yelt", "Mehardu", "Morn", "Kukundi", "Kaloon", "Gelim", "Belbu", "McKellan", "Smuigel" ,"Gelli",
        "Curry", "Chahke", "Sphahce", "Gindolf", "Nnyme", "Whippet", "Tontris", "Lime", "Orange" ,"Grapefruit",
        "Ullenous", "Ki-Row", "Erenklud", "Cloudman", "Dripping", "Ledgu", "Persei", "Arou", "Bettur" ,"Deaight"
    };

    const char * const PlayerDataSystem::DEFAULT_HUMAN_PLAYER_NAME = "???";

    PlayerDataSystem::PlayerDataSystem(CommonBox & box) :
        RascUpdatable(RascUpdatable::Mode::map, 8),
        onUpdate([](void){}),
        box(box) {
        
        // Set flag create modes from Rasc config.
        {
            simplenetwork::MutexPtr<tinyxml2::XMLDocument> doc = box.rascConfig->getDocument();
            tinyxml2::XMLElement * sharedOptions = RascConfig::getOrInitBase(&*doc, "sharedOptions", &Misc::initSharedOptionsConfig);
            
            tinyxml2::XMLElement * humanFlag = readElement(sharedOptions, "humanFlag");
            humanFlagCreateMode = Flag::friendlyNameToCreateMode(readAttribute(humanFlag, "createMode"));
            
            tinyxml2::XMLElement * aiFlag = readElement(sharedOptions, "aiFlag");
            aiFlagCreateMode = Flag::friendlyNameToCreateMode(readAttribute(aiFlag, "createMode"));
        }
    }

    PlayerDataSystem::~PlayerDataSystem(void) {
        // All of our children are dynamically allocated, so delete them when we are deleted.
        for (std::pair<uint64_t, RascUpdatable *> child : mapChildren()) {
            delete child.second;
        }
    }

    #define CREATE_CHILD_FROM_MESSAGE                                   \
        PlayerData * playerData = new PlayerData(childId, msg, box);    \
        registerChild(playerData, childId);

    bool PlayerDataSystem::onReceiveMiscUpdate(simplenetwork::Message & msg) {
        MiscUpdateType update = msg.read8();
        switch(update) {
        // -------------------------- Delete player data ----------------------------
        case MiscUpdateTypes::deletePlayerData: {
            uint64_t playerId = msg.read64();
            RascUpdatable * child = childForwardLookupChecked(playerId);
            if (child == NULL) {
                fprintf(stderr, "WARNING: PlayerDataSystem: Cannot delete player with id %llu, it doesn't exist.\n", (unsigned long long)playerId);
            } else {
                // Notify the stat system, since this will happen during the game.
                box.statSystem->onRemovePlayer(playerId);
                // ... And unregister and delete the player data.
                unregisterChild(playerId);
                delete child;
            }
            break;
        }
        // -------------------------- Create player data ----------------------------
        case MiscUpdateTypes::createPlayerData: {
            uint64_t childId = msg.read64();
            CREATE_CHILD_FROM_MESSAGE
            // Notify the stat system, since this will happen during the game.
            box.statSystem->onAddPlayer(childId);
            break;
        }
        // --------------------------------------------------------------------------
        default:
            fprintf(stderr, "ERROR: PlayerDataSystem unknown misc update type: %d\n", (int)update);
            return false;
        }
        // Since we have received an update, run the onUpdate lambda.
        onUpdate();
        return true;
    }

    bool PlayerDataSystem::onInitialDataMissingChild(simplenetwork::Message & msg, uint64_t childId) {
        CREATE_CHILD_FROM_MESSAGE
        // Since we have received an update, run the onUpdate lambda.
        // Don't notify the stat system here, since we don't need to when receiving initial data.
        onUpdate();
        return true;
    }

    const char * PlayerDataSystem::getRandomName(void) {
        return aiNames[box.random->sizetRange(0, std::size(aiNames))];
    }

    /**
     * This, given a temporary PlayerData instance, assigned on the stack, which is not part of the RascUpdatable
     * hierarchy, will write a createPlayerData update message for it.
     */
    #define WRITE_CREATE_PLAYER_DATA_MESSAGE                                        \
        writeUpdateHeader(update.msg());                                            \
        update.msg().write8(MiscUpdateTypes::createPlayerData);                     \
        update.msg().writeVariableSize(data.getId(), getIdentifierBytes());         \
        data.writeInitialDataWithoutHeader(update.msg());                           \
        return data.id;

    uint64_t PlayerDataSystem::newAIPlayerData(MiscUpdate update, UniqueIdAssigner * playerIdAssigner) {
        PlayerData data(playerIdAssigner, box, aiFlagCreateMode);
        WRITE_CREATE_PLAYER_DATA_MESSAGE
    }
    uint64_t PlayerDataSystem::newHumanPlayerData(MiscUpdate update, UniqueIdAssigner * playerIdAssigner) {
        PlayerData data(playerIdAssigner, box, humanFlagCreateMode, DEFAULT_HUMAN_PLAYER_NAME);
        WRITE_CREATE_PLAYER_DATA_MESSAGE
    }

    void PlayerDataSystem::deletePlayerData(MiscUpdate update, uint64_t playerId) {
        writeUpdateHeader(update.msg());
        update.msg().write8(MiscUpdateTypes::deletePlayerData);
        update.msg().write64(playerId);
    }
}

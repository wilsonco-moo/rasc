/*
 * ThreatManagedWeights.h
 *
 *  Created on: 2 Dec 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_PLAYERDATA_INFO_THREATMANAGEDWEIGHTS_H_
#define BASEGAME_COMMON_PLAYERDATA_INFO_THREATMANAGEDWEIGHTS_H_

#include <cstdint>

namespace simplenetwork {
    class Message;
}

namespace rasc {
    class CommonBox;

    // ---------------------------- Province value type ----------------------------

    /**
     * This typedef should be used to represent the relative threat
     * and value of a province or adjacent provinces.
     * If this is added up across all owned provinces, a provstat_t should
     * be used for the total value.
     */
    typedef int32_t provvalue_t;

    /**
     * These macros should be used for reading and writing
     * provvalue_t values from a message.
     * NOTE: TM = Threat Managed.
     */
    #define TM_WRITE_PROVVALUE(message, value) \
        message.writeS32(value)
    #define TM_READ_PROVVALUE(message) \
        message.readS32()

    /**
     * This class represents a series of values, which are used to make decisions
     * in the threat managed AI.
     * There are two types of values represented:
     *  > Core behaviour values. These are stored as values, and can change from one AI
     *    player to another.
     *    - Aggressiveness
     *      A larger values will cause the AI to prioritise more on attacking provinces owned
     *      by other players. A lower value will cause the AI to prioritise more on defence.
     *    - Extravagance
     *      A larger value will cause the AI to spend more on units than they can necessarily
     *      afford, possibly putting them into debt. A lower value will cause them to try to
     *      avoid spending, and possibly accumulate wealth that they may never use.
     *    - Focus
     *      A larger value will cause the AI to focus more an attacking and defending only a
     *      small number of provinces at once. A smaller value will cause the AI to spread
     *      their armies over a much wider area at once.
     *    - Cautiousness
     *      A larger value will cause the AI to try to prioritise more on reinforcing battles
     *      and always try to outnumber the enemy. A smaller value will cause the AI to prioritise
     *      elsewhere, and mostly ignore losing battles.
     *
     *  > Other values, which are either constant expressions, or calculated from the
     *    core behaviour values.
     */
    class ThreatManagedWeights {
    public:

        // X macros defining our stored behaviour values.
        // Format: ID, name, default value.
        #define TM_STORED_BEHAVIOUR_VALUES                                                \
            X(0, Aggressiveness, box.random->uintRange(0, 101), defensive,    aggressive)  \
            X(1, Extravagance,   box.random->uintRange(0, 101), conservative, extravagant) \
            X(2, Focus,          box.random->uintRange(0, 101), unfocused,    focused)     \
            X(3, Cautiousness,   box.random->uintRange(0, 101), reckless,     cautious)

        // The number of stored behaviour values. This must be updated
        // if new stored behaviour values are added.
        constexpr static unsigned int BEHAVIOUR_VALUES_COUNT = 4;

    private:
        // The internal array of stored behaviour values.
        unsigned int behaviourValues[BEHAVIOUR_VALUES_COUNT];

    public:

        /**
         * Default constructor for ThreatManagedWeights.
         * This initialises all stored behaviour values to default random values.
         */
        ThreatManagedWeights(CommonBox & box);

        /**
         * Create a ThreatManagedWeights from a message. This reads
         * the stored behaviour values from the message.
         */
        ThreatManagedWeights(simplenetwork::Message & msg);

        /**
         * Reads stored behaviour values from the message, replacing
         * the values we currently have stored.
         */
        void readFrom(simplenetwork::Message & msg);

        /**
         * Writes our behaviour to the provided message, such that a new
         * ThreatManagedWeights instance could be created from this.
         */
        void writeTo(simplenetwork::Message & msg) const;

        // ================================ ACCESSIBLE VALUES ======================================

        /**
         * Each of the stored behaviour values can be accessed using an accessor method,
         * for example Aggressiveness can be accessed with the getAggressiveness method.
         */
        #define X(id, name, defaultValue, antonym, verb) \
            inline unsigned int get##name(void) const { return behaviourValues[id]; }
        TM_STORED_BEHAVIOUR_VALUES
        #undef X

        // ----------------- Fort and building related values ----------------------

        /**
         * The amount of garrison desirability gained per 4096 friendly standing army men.
         * Note that this should be roughly proportional to the amount of threat counteracted
         * by friendly standing armies.
         *
         * Should be affected by cautiousness, (20-60).
         */
        inline unsigned int fortDesirabilityPer4096Standing(void) const {
            return 60 - ((getCautiousness()*2)/5);
        }

        /**
         * The bonus to fort desirability, per 100 province value. For example, if a province
         * has a province value of +50, it will receive half of this bonus.
         * Note that bad provinces, like bogs, will cause negative values here.
         * This is set so that a medium-good province, (+50 province value), is equivalent
         * to the fort desirability from 6000-8000 enemy men (threat).
         *
         * This should be affected by focus, (120-160).
         */
        inline provvalue_t fortDesirabilityFromProvinceValue(void) const {
            return 120 + ((2*getFocus())/5);
        }

        /**
         * The bonus to fort desirability, if we own the entire area which the province belongs
         * to. The idea here, is that once we gain a whole area, we should build forts to
         * try to keep it.
         * This is set to a constant value, equivalent to the fort desirability from
         * 5000 enemy men.
         */
        constexpr provvalue_t fortDesirabilityFromAreaOwnership(void) const { return 50; }

        /**
         * The bonus to fort desirability, if an adjacent enemy province contains a fort.
         * This should be smaller than the desirability gained from area ownership.
         */
        constexpr provvalue_t fortDesirabilityFromNearbyFort(void) const { return 20; }

        /**
         * The bonus to fort desirability, per 4096 attack value of nearby enemy provinces.
         * This is actually negative, because if nearby enemy provinces are very desirable,
         * then we should avoid building a fort.
         *
         * This is set so that the common maximum attack value (~280), is -20 for very
         * aggressive players, and -5 for very defensive players.
         *
         * This should be affected by defensiveness, (-73 - -292).
         */
        inline provvalue_t fortDesirabilityPer4096AttackValue(void) const {
            return -73 - ((getAggressiveness() * 219) / 100);
        }

        /**
         * The bonus fort desirability, for provinces which do not contain enough available
         * land to build a fort. This should be a very large negative value, to try to deterr
         * having to demolish buildings to make way for forts.
         * Extravagant players are more likely to demolish buildings to make way for forts.
         *
         * This should be affected by extravagance, (-150 - -110).
         */
        inline provvalue_t fortDesirabilityIfNotEnoughLand(void) const {
            return -150 + ((getExtravagance() * 2) / 5);
        }

        /**
         * The maximum fort desirability threshold before the AI should consider
         * building forts. Note that this must ALWAYS be above zero, or the AI
         * can fail to pick provinces to build forts in.
         *
         * This should be affected by aggressiveness, (500 - 600).
         */
        inline provvalue_t fortDesirabilityBuildThreshold(void) const {
            return 600 - getAggressiveness();
        }

        /**
         * The minimum standing army size that should be left over, after building a fort
         * and absorbing available units into the garrison.
         */
        inline constexpr provvalue_t fortMinStandingArmyAfterBuild(void) const { return 4000; }

        /**
         * The probability, per turn, of removing each fort in a centre province,
         * where the province's profit is less than fortRemovalCurrencyProfit.
         * For example, if this has a value of 5, it means a probability of 1 in 5.
         */
        inline constexpr provvalue_t fortRemovalProbability(void) const { return 5; }

        /**
         * If a province, even with a fort and garrison stationed in it, is making
         * a profit of at least this much, it will not ever be removed.
         */
        inline constexpr provvalue_t fortRemovalCurrencyProfit(void) const { return 8000; }

        /**
         * 100 plus the percentage larger that the enemy appears, when considering
         * whether to retreat a standing army into a fort. For example, if this has the
         * value 140, armies will only retreat into garrisons if they do not outnumber
         * the enemy by 40%. This should be larger than defenseBattleSafety.
         *
         * Should be affected by cautiousness, (100-220).
         */
        inline unsigned int garrisonRetreatBattleSafety(void) const {
            return 100 + ((getCautiousness()*6)/5);
        }

        /**
         * The number of 1024'ths of total income that are reserved for constructing
         * buildings. For example, a value of 102 here is 102/1024 of the total income,
         * (roughly 10%). More conservative players should reserve a larger proportion
         * of income for constructing buildings.
         * Note that building reserved income the minimum
         * between this buildingReservedProvinceValuePercent.
         *
         * Should be affected by extravagance, (50-150).
         */
        inline unsigned int buildingReservedIncome1024(void) const {
            return 150 - getExtravagance();
        }

        /**
         * The percent of the currency income of our highest province value province,
         * which is reserved for constructing buildings. For example, if this has a value
         * of 80%, then 80% of the appropriate province's income is reserved. More
         * conservative players should reserve a larger proportion of income for
         * constructing buildings.
         * Note that building reserved income the minimum
         * between this buildingReservedIncome1024.
         *
         * Should be affected by extravagance, (75-225).
         */
        inline unsigned int buildingReservedProvinceValuePercent(void) const {
            return 225 - (getExtravagance()*3)/2;
        }

        /**
         * The maximum percentage overall land use which should be present in a
         * player's land, before land improvement buildings are constructed by default.
         *
         * Should be affected by cautiousness, (70-95).
         */
        inline int maximumUsedLandPercentageThreshold(void) const {
            return 95 - getCautiousness()/4;
        }

        // --------------------- Threat from enemy provinces -----------------------

        /**
         * The amount of threat contributed by enemy armies, per 4096 men.
         * For example, if this is 128, then 32 men contribute 1 threat.
         * (This value approximates 1 threat per 100 men).
         */
        constexpr unsigned int enemyThreatPer4096(void) const { return 41; }

        /**
         * The threat generated, per 4096 men in a garrison. This should be
         * significantly less than enemyThreatPer4096, since a garrison cannot
         * be used to attack the player. A garrison *still* should contribute
         * to threat however, because a nation with lots of large garrisons
         * surely must have lots of resources to build them.
         */
        constexpr unsigned int garrisonThreatPer4096(void) const { return 12; }

        /**
         * These specify the ratio at which bigness starts to influence threat.
         * For example:
         *    If bignessEnemyMultiplier is 3 and bignessOurMultiplier is 4,
         *    then 3 times enemy provinces would have to be larger than 4 times our
         *    provinces, (they're 4/3 times larger), before bigness starts to affect
         *    threat.
         */
        constexpr unsigned int bignessEnemyMultiplier(void) const { return 3; }
        constexpr unsigned int bignessOurMultiplier(void) const { return 4; }

        // -------------------- Threat when placing friendly units -------------------

        /**
         * The amount of threat counteracted per 4096 friendly standing army men.
         * Note that garrisoned friendly men should be added, and scaled by garrisonBattleSizeMultiplier,
         * before working out now much threat they counteract.
         *
         * Should be affected by cautiousness, (30-90).
         * (Originally 82 (1 threat per 50 men), average 60).
         */
        inline unsigned int friendlyThreatCounteractPer4096(void) const {
            return 90 - ((getCautiousness()*3)/5);
        }

        // ----------------------------- Threat in sieges ----------------------------

        /**
         * The amount of threat generated, per 4096 enemy men, who are sieging our land.
         * (This value approximates 1 threat per 100 men).
         */
        constexpr unsigned int siegeThreatPerEnemy4096(void) const { return 205; }

        // --------------------------- Threat in battles -----------------------------

        /**
         * This is the amount of threat that is generated if the enemy army is double
         * our size.
         * So (1/battleDivisor) is the proportion larger that the enemy army has to be to
         * generate a threat of 1.
         * For example, if the enemy outnumber us by 1/5 of our army size, then a
         * divisor of 50 will result in 10 threat.
         */
        constexpr provvalue_t battleDivisor(void) const { return 200; }

        /**
         * 100 plus the percentage larger that the enemy army appears to the threat calculation.
         * For example, if this has the value 120, the enemy army will appear 20% larger to the
         * threat calculation than it actually is. This will result in the
         * AI trying to outnumber them by 20%.
         *
         * NOTE: This is also used for evaluating the attack value of provinces
         *       in enemy territory, where there is already a battle going on.
         *
         * Should be affected by cautiousness, (80-200).
         * (Originally 120, average 140).
         */
        inline unsigned int defenseBattleSafety(void) const {
            return 80 + ((getCautiousness()*6)/5);
        }

        // ------------------------------- Unit placement ----------------------------

        /**
         * The AI will not place down units, unless it has the reserve economy to pay for them
         * for at least this many turns, before going into debt.
         * Making this bigger will result in AI better avoiding debt, but not placing all the
         * armies they can support.
         *
         * Should be affected by extravagence, (1-6).
         * (Originally 4, average 4).
         */
        inline unsigned int economyLookaheadTurns(void) const {
            return 6 - ((getExtravagance()*5)/100);
        }

        /**
         * The percentage most threatened provinces in which we will place units.
         * For example, if this is 40, only 40% of our most threatened edge provinces will have
         * units placed in them in a turn.
         *
         * Should be affected by focus, (15-65)
         * (Originally 30, average 40)
         */
        inline unsigned int threatPercentageUnitPlacement(void) const {
            return 65 - (getFocus()/2);
        }

        /**
         * The amount of threat, (counted across all most threatened
         * provinces), required to make us place 4096 men.
         *
         * Should be affected by cautiousness, (20-70).
         * (Originally 51 (1 threat per 80 men), average 60).
         */
        inline unsigned int threatPer4096DefensivePlacement(void) const {
            return 70 - (getCautiousness()/2);
        }

        /**
         * The maximum percentage of overall placeable men which can be used for
         * placing defensive units.
         *
         * Should be affected by aggressiveness, (40-90).
         * (Originally 75, average 65)
         */
        inline unsigned int maximumDefensivePlacementPercentage(void) const {
            return 90 - (getAggressiveness()/2);
        }

        // -------------------- Base province value evaluation -----------------------

        /**
         * A random number between 0 and this value is added to the attack value of every province.
         * A random value added on to the attack value. Higher values make the AI more irrational and random,
         * values too low cause the AI to be too predictable.
         */
        constexpr provvalue_t provValueRandomPlus(void) const { return 20; }

        /**
         * The bonus of an unowned province, higher values make AI target unowned land more.
         */
        constexpr provvalue_t provValueUnownedPlus(void) const { return 100; }

        /**
         * This is added to the province value if the province is in an area we hold
         * no provinces in. Higher values cause the AI to screw over other AIs more.
         *
         * This should be affected by aggressiveness, (10-200).
         * (Originally 100, average 105)
         */
        inline provvalue_t provValueFootholdPlus(void) const {
            return 10 + ((getAggressiveness()*19)/10);
        }

        /**
         * The maximum value for the area modifier.
         * Higher values make the AI try to make a complete area more aggressively.
         */
        constexpr provvalue_t provValueMaxAreaModifier(void) const { return 300; }

        /**
         * The multiplier for the area modifier.
         * The larger this value is, the less complete an area has to be before it gets
         * large numbers for the area modifier.
         */
        constexpr provvalue_t provValueAreaModifierMultiplier(void) const { return 40; }

        /**
         * If our nation is provValueTinyNationOppressorMinSize or larger,
         * and their nation is provValueTinyNationMaxSize or smaller,
         * then provValueTinyNationPlus should be added to the province value.
         */
        constexpr provvalue_t provValueTinyNationMaxSize(void) const { return 3; }
        constexpr provvalue_t provValueTinyNationOppressorMinSize(void) const { return 6; }
        constexpr provvalue_t provValueTinyNationPlus(void) const { return 400; }

        /**
         * If our nation is at least provValueLargerNationMinOurSize provinces,
         * and their nation is more than provValueLargerNationMinSize provinces,
         * and their nation is at least provValueLargerPercentageLargerFactor percent larger than us,
         * then provValueLargerNationPlus should be added to the province value.
         */
        constexpr provvalue_t provValueLargerNationMinSize(void) const { return 12; }
        constexpr provvalue_t provValueLargerPercentageLargerFactor(void) const { return 250; }
        constexpr provvalue_t provValueLargerNationMinOurSize(void) const { return 4; }
        constexpr provvalue_t provValueLargerNationPlus(void) const { return 300; }

        /**
         * The bonus value we get from the overall stat modifier in the province (the province value).
         * The province value is multiplied by this, e.g: a province value of +50% gives half this bonus.
         *
         * This should be affected by focus, (150-250).
         * (Originally 200, average 200)
         */
        inline provvalue_t provValueStatModifierPlus(void) const {
            return 150 + getFocus();
        }

        // ------------------ Attack province value evaluation -----------------------

        /**
         * This is the amount of attack province value that is generated if the enemy army is double
         * our size.
         * So (1/battleDivisor) is the proportion larger that the enemy army has to be to
         * generate an attack province value of 1.
         * For example, if the enemy outnumber us by 1/5 of our army size, then a
         * divisor of 50 will result in 10 value.
         */
        constexpr provvalue_t attackValueBattleDivisor(void) const { return 200; }

        /**
         * If in a battle, there are fewer than this number of men, then there is no point
         * reinforcing the battle, so zero should be returned as the province value.
         */
        constexpr unsigned int attackValueMinReinforce(void) const { return 1000; }

        /**
         * The amount of desirability created per 4096 enemy units, in enemy territory.
         * This can be a positive or a negative value, depending on whether this AI
         * wants to attack or avoid enemy border forces.
         *
         * Less cautious, and more aggressive players try to attack border forces.
         * More cautious, and less aggressive players try to sneak around border forces.
         *
         * This should be affected by aggressiveness and cautiousness, (-50 - +50).
         *
         * (Originally 41 (1 desirability per 100 men), average 0).
         */
        inline provvalue_t attackDesirabilityPer4096(void) const {
            return (((provvalue_t)getAggressiveness()) - ((provvalue_t)getCautiousness()))/2;
        }

        /**
         * If, when looking around at all the friendly armies that we could attack an
         * enemy with, we do not outnumber them by battleSafety, then this modifier is added
         * to the province value.
         */
        constexpr provvalue_t attackValueOutnumbermentModifier(void) const { return -100000; }

        /**
         * If, when looking around at all the friendly armies that we could attack an
         * enemy with, outnumbering the enemy by battleSafety requires breaking units away
         * from sieges, then this value is added to the province value.
         */
        constexpr provvalue_t attackValueSiegeBreakModifier(void) const { return -500; }

        /**
         * This is used when moving attacking armies within enemy territory.
         * A province gains this modifier if there is a siege already happening, where we have
         * enough men to complete it. This should be negative to avoid the AI reinforcing
         * existing sieges, where they ought to distribute their armies to other provinces.
         */
        constexpr provvalue_t attackValueEnoughSiegeModifier(void) const { return -750; }

        /**
         * This is used when moving attacking armies around within enemy territory.
         * A province gains this modifier if there is a siege already happening, where
         * we DO NOT HAVE ENOUGH men to complete it. This should be positive to make the
         * AI reinforce this, to avoid losing siege progress.
         */
        constexpr provvalue_t attackValueNotEnoughSiegeModifier(void) const { return 800; }

        /**
         * 100 plus the percentage larger that the enemy army appears to the value calculation.
         * For example, if this has the value 120, the enemy army will appear 20% larger to the
         * attack value calculation than it actually is. This will result in the
         * AI trying to outnumber them by 20%.
         *
         * NOTE: This is used for evaluating the attack value of provinces
         *       in enemy territory, where there is already a battle going on.
         *
         * This should be affected by cautiousness, (70-190).
         * (Originally 105, average 130)
         */
        inline unsigned int attackBattleSafety(void) const {
            return 70 + ((getCautiousness()*6)/5);
        }

        /**
         * 100 plus the percentage stronger that garrisons appear to us when we are attacking.
         * This exists so that we can handle garrisoned armies the same as regular standing
         * armies, when working out stuff like battle safety for attacking.
         * For example, if this had the value 150, then we would treat a garrison of size 5000
         * as if we were in battle with a standing army of size 7500.
         *
         * NOTE: This should be adjusted as the garrison mechanics are changed/balanced.
         */
        constexpr unsigned int garrisonBattleSizeMultiplier(void) const { return 130; }

        /**
         * The percentage of enemy adjacent provinces that we should place armies next to
         * with view to attack, (during the attack placement phase).
         *
         * This should be affected by focus, (10-70).
         * (Originally 25, average 40)
         */
        inline unsigned int attackPlacementPercentage(void) const {
            return 70 - ((getFocus()*3)/5);
        }

        /**
         * The percentage of enemy adjacent provinces that we should push existing border
         * forces into, to attack.
         *
         * This should be affected by focus, (5-55).
         */
        inline unsigned int attackBorderPushPercentage(void) const {
            return 55 - (getFocus()/2);
        }

        /**
         * We should never bother to move fewer than this many men at once.
         */
        constexpr unsigned int attackMinMoveSize(void) const { return 600; }
    };
}

#endif

/*
 * ThreatManagedWeights.cpp
 *
 *  Created on: 2 Dec 2019
 *      Author: wilson
 */

#include "ThreatManagedWeights.h"

#include <cstdlib>

#include <sockets/plus/message/Message.h>

#include "../../util/numeric/Random.h"
#include "../../util/CommonBox.h"

namespace rasc {

    ThreatManagedWeights::ThreatManagedWeights(CommonBox & box) :
        // Initialise all stored behaviour values to default.
        #define X(id, name, defaultValue, antonym, verb) defaultValue,
        behaviourValues {
            TM_STORED_BEHAVIOUR_VALUES
        }
        #undef X
        {
    }

    ThreatManagedWeights::ThreatManagedWeights(simplenetwork::Message & msg) {
        readFrom(msg);
    }

    void ThreatManagedWeights::readFrom(simplenetwork::Message & msg) {
        for (unsigned int & value : behaviourValues) {
            value = msg.read8();
        }
    }

    void ThreatManagedWeights::writeTo(simplenetwork::Message & msg) const {
        for (unsigned int value : behaviourValues) {
            msg.write8(value);
        }
    }
}

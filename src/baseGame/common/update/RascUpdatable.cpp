/*
 * RascUpdatable.cpp
 *
 *  Created on: 22 Feb 2019
 *      Author: wilson
 */
#include <sockets/simplenetwork/simplenetwork.h>   // *** INCLUDE THIS FIRST ***

#include "RascUpdatable.h"

#include <unordered_map>
#include <cstring>
#include <string>
#include <iostream>
#include <sockets/plus/message/Message.h>


namespace rasc {

    #define INITIAL_ARRAY_SIZE 16

    std::recursive_mutex RascUpdatable::updateHeaderMutex;


    RascUpdatable::RascUpdatable(Mode mode, int identifierBytes) :
        mode(mode),
        identifierBytes(identifierBytes),
        rascUpdatableParent(NULL),
        rascUpdatableRoot(NULL),
        rtable(),
        cachedUpdateHeaderLength(0),
        cachedUpdateHeader(NULL),
        haveUpdateHeader(false) {


        if (mode == Mode::array) {
            new (&forwardArray) std::vector<RascUpdatable *>(0);
        } else {
            forwardMap = {
                0,
                new std::recursive_mutex(),
                new std::unordered_map<uint64_t, RascUpdatable *>()
            };
        }
    }

    RascUpdatable::~RascUpdatable(void) {
        if (mode == Mode::array) {
            // Explicitly call the destructor of the forward lookup table, since it was placement initialised.
            forwardArray.~vector();
        } else {
            // In map mode, delete both the mutex and the unordered map.
            delete forwardMap.mutex;
            delete forwardMap.map;
        }
        free(cachedUpdateHeader);
    }

    // ------------------- Internal methods, (not declared in header) -------------------------------------

    std::string getIdInformationInternal(RascUpdatable * object) {
        if (object->getRascUpdatableParent() == NULL) {
            return "root, ";
        } else {
            std::string parent = getIdInformationInternal(object->getRascUpdatableParent());
            return parent + std::to_string(object->getRascUpdatableId()) + ", ";
        }
    }
    std::string getIdInformation(RascUpdatable * object) {
        std::string str = getIdInformationInternal(object);
        str.resize(str.size() - 2);
        return "RascUpdatable [" + str + "]";
    }

    // ------------------- 'Getter' methods ---------------------------

    uint64_t RascUpdatable::getRascUpdatableId(void) {
        return rascUpdatableParent->childReverseLookup(this);
    }

    void RascUpdatable::printIdInformation(void) {
        std::cout << getIdInformation(this);
    }

    // ================================= GENERAL USE METHODS ======================================

    void RascUpdatable::writeUpdateHeader(simplenetwork::Message & message) {
        if (!haveUpdateHeader) {
            std::lock_guard<std::recursive_mutex> lock(updateHeaderMutex);
            if (cachedUpdateHeader == NULL) {
                generateCachedUpdateHeader();
                haveUpdateHeader = true;
            }
        }
        message.writeData(cachedUpdateHeader, cachedUpdateHeaderLength);
    }

    // ================================== REGISTRATION METHODS ====================================

    uint64_t RascUpdatable::registerChild(RascUpdatable * child) {
        uint64_t id;
        if (mode == Mode::array) {
            id = forwardArray.size();
            forwardArray.push_back(child);
            rtable[child] = id;
        } else {
            std::lock_guard<std::recursive_mutex> lock(*forwardMap.mutex); // Lock to the mutex only in map mode.
            id = forwardMap.childIdUpTo++;
            (*forwardMap.map)[id] = child;
            rtable[child] = id;
        }
        child->rascUpdatableParent = this;
        if (rascUpdatableRoot != NULL) child->setRascUpdatableRoot(rascUpdatableRoot);
        return id;
    }

    void RascUpdatable::registerChild(RascUpdatable * child, uint64_t id) {
        if (mode == Mode::array) {
            fprintf(stderr, "WARNING: Cannot register child rasc updatable with id, in array mode.\n");
        } else {
            std::lock_guard<std::recursive_mutex> lock(*forwardMap.mutex); // Lock to the mutex only in map mode.
            if (id >= forwardMap.childIdUpTo) {
                forwardMap.childIdUpTo = id + 1;
            }
            (*forwardMap.map)[id] = child;
            rtable[child] = id;
        }
        child->rascUpdatableParent = this;
        if (rascUpdatableRoot != NULL) child->setRascUpdatableRoot(rascUpdatableRoot);
    }



    void RascUpdatable::unregisterChild(RascUpdatable * child) {
        if (mode == Mode::array) {
            fprintf(stderr, "WARNING: Cannot unregister child rasc updatable, in array mode.\n");
        } else {
            std::lock_guard<std::recursive_mutex> lock(*forwardMap.mutex); // Lock to the mutex only in map mode.
            std::unordered_map<RascUpdatable *, uint64_t>::iterator iter = rtable.find(child);
            if (iter != rtable.end()) {
                child->onUnregister(true); // Unregister, AND reset their parent, since they will be removed.
                forwardMap.map->erase(iter->second);
                rtable.erase(iter);
            }
        }
    }

    void RascUpdatable::unregisterChild(uint64_t id) {
        if (mode == Mode::array) {
            fprintf(stderr, "WARNING: Cannot unregister child rasc updatable, in array mode.\n");
        } else {
            std::lock_guard<std::recursive_mutex> lock(*forwardMap.mutex); // Lock to the mutex only in map mode.
            std::unordered_map<uint64_t, RascUpdatable *>::iterator iter = forwardMap.map->find(id);
            if (iter != forwardMap.map->end()) {
                iter->second->onUnregister(true); // Unregister, AND reset their parent, since they will be removed.
                rtable.erase(iter->second);
                forwardMap.map->erase(iter);
            }
        }
    }

    bool RascUpdatable::isChildRegistered(RascUpdatable * child) {
        if (mode == Mode::array) {
            return rtable.find(child) != rtable.cend();
        } else {
            std::lock_guard<std::recursive_mutex> lock(*forwardMap.mutex); // Lock to the mutex only in map mode.
            return rtable.find(child) != rtable.cend();
        }
    }

    bool RascUpdatable::isChildRegistered(uint64_t id) {
        if (mode == Mode::array) {
            return id < forwardArray.size();
        } else {
            std::lock_guard<std::recursive_mutex> lock(*forwardMap.mutex); // Lock to the mutex only in map mode.
            return forwardMap.map->find(id) != forwardMap.map->cend();
        }
    }

    // ----------------------- Lookup methods -----------------------------

    RascUpdatable * RascUpdatable::childForwardLookup(uint64_t id) {
        if (mode == Mode::array) {
            return forwardArray[id];
        } else {
            std::lock_guard<std::recursive_mutex> lock(*forwardMap.mutex); // Lock to the mutex only in map mode.
            return forwardMap.map->at(id);
        }
    }

    RascUpdatable * RascUpdatable::childForwardLookupChecked(uint64_t id) {
        if (mode == Mode::array) {
            if (id < forwardArray.size()) {
                return forwardArray[id];
            } else {
                return NULL;
            }
        } else {
            std::lock_guard<std::recursive_mutex> lock(*forwardMap.mutex); // Lock to the mutex only in map mode.
            std::unordered_map<uint64_t, RascUpdatable *>::iterator iter = forwardMap.map->find(id);
            if (iter == forwardMap.map->end()) {
                return NULL;
            } else {
                return iter->second;
            }
        }
    }

    uint64_t RascUpdatable::childReverseLookup(RascUpdatable * child) {
        if (mode == Mode::array) {
            return rtable.at(child);
        } else {
            std::lock_guard<std::recursive_mutex> lock(*forwardMap.mutex); // Lock to the mutex only in map mode.
            return rtable.at(child);
        }
    }

    size_t RascUpdatable::getRascUpdatableChildCount(void) {
        if (mode == Mode::array) {
            return forwardArray.size();
        } else {
            std::lock_guard<std::recursive_mutex> lock(*forwardMap.mutex); // Lock to the mutex only in map mode.
            return forwardMap.map->size();
        }
    }

    // ------------------------------- Internal methods -------------------------------------

    void RascUpdatable::onUnregister(bool resetParent) {

        // Clear our update header and pointer to our parent, when we are removed.
        // Also set haveUpdateHeader to false, so that later on if we are added again, the update header is regenerated.
        free(cachedUpdateHeader);
        cachedUpdateHeader = NULL;
        haveUpdateHeader = false;

        rascUpdatableRoot = NULL;

        if (resetParent) {
            rascUpdatableParent = NULL;
        }

        // Then call this method for our children. Do not reset their parent's, since they have not been removed from us.
        if (mode == Mode::array) {
            for (RascUpdatable * child : forwardArray) {
                child->onUnregister(false);
            }
        } else {
            for (std::pair<const uint64_t, RascUpdatable *> child : *forwardMap.map) {
                child.second->onUnregister(false);
            }
        }
    }

    void RascUpdatable::setRascUpdatableRoot(TopLevelRascUpdatable * root) {
        rascUpdatableRoot = root;
        // Then call this method for our children.
        if (mode == Mode::array) {
            for (RascUpdatable * child : forwardArray) {
                child->setRascUpdatableRoot(root);
            }
        } else {
            for (std::pair<const uint64_t, RascUpdatable *> child : *forwardMap.map) {
                child.second->setRascUpdatableRoot(root);
            }
        }
    }

    void RascUpdatable::generateCachedUpdateHeader(void) {
        struct simplenetwork_Message message = simplenetwork_newEmptyMessageDefault();
        uint8_t depth = writeIdSection(&message);
        cachedUpdateHeaderLength = message.length + sizeof(uint8_t);
        cachedUpdateHeader = malloc(cachedUpdateHeaderLength);
        if (cachedUpdateHeader == NULL) {
            fprintf(stderr, "Failed to allocate memory to generated cached update header in rasc updatable.\n");
        }
        *((uint8_t *)cachedUpdateHeader) = depth;
        memcpy(((char *)cachedUpdateHeader) + sizeof(uint8_t), message.data, message.length);
        free(message.data);
    }

    uint8_t RascUpdatable::writeIdSection(struct simplenetwork_Message * message) {
        uint8_t depth;
        if (rascUpdatableParent == NULL) {
            depth = 0;
        } else {
            depth = rascUpdatableParent->writeIdSection(message) + 1;
            simplenetwork_writeVariableSize(message, rascUpdatableParent->childReverseLookup(this), rascUpdatableParent->identifierBytes);
        }
        return depth;
    }

    // ---------------------------- Array and map iteration ---------------------------------


    PairIterable<uint64_t, RascUpdatable *, std::vector<RascUpdatable *>> RascUpdatable::arrayChildrenPair(void) const {
        if (mode != Mode::array) {
            fprintf(stderr, "ERROR: Cannot use RascUpdatable::arrayChildrenPair() on a RascUpdatable which is not in array mode.\n");
            exit(EXIT_FAILURE);
        }
        return PairIterable<uint64_t, RascUpdatable *, std::vector<RascUpdatable *>>(forwardArray);
    }
    const std::vector<RascUpdatable *> & RascUpdatable::arrayChildren(void) const {
        if (mode != Mode::array) {
            fprintf(stderr, "ERROR: Cannot use RascUpdatable::arrayChildren() on a RascUpdatable which is not in array mode.\n");
            exit(EXIT_FAILURE);
        }
        return forwardArray;
    }
    simplenetwork::LockedIterable<std::unordered_map<uint64_t, RascUpdatable *>> RascUpdatable::mapChildren(void) {
        if (mode != Mode::map) {
            fprintf(stderr, "ERROR: Cannot use RascUpdatable::mapChildren() on a RascUpdatable which is not in map mode.\n");
            exit(EXIT_FAILURE);
        }
        forwardMap.mutex->lock();
        return simplenetwork::LockedIterable<std::unordered_map<uint64_t, RascUpdatable *>>(forwardMap.mutex, forwardMap.map->begin(), forwardMap.map->end());
    }

    // ================================ GENERAL METHODS, DEFAULT IMPLEMENTATIONS ==================================

    void RascUpdatable::onGenerateTurnMiscUpdate(MiscUpdate update) {
        // Note: By default we do nothing.
    }
    void RascUpdatable::onGenerateServerUpdate(simplenetwork::Message & msg) {
        // Note: By default we do nothing.
    }
    bool RascUpdatable::onReceiveServerUpdate(simplenetwork::Message & msg) {
        fprintf(stderr, "WARNING: %s UNIMPLEMENTED RECEIVE server update\n", getIdInformation(this).c_str());
        return false;
    }
    void RascUpdatable::onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) {
        // Note: By default we do nothing.
    }
    bool RascUpdatable::onReceiveInitialData(simplenetwork::Message & msg) {
        fprintf(stderr, "WARNING: %s UNIMPLEMENTED RECEIVE initial data\n", getIdInformation(this).c_str());
        return false;
    }
    bool RascUpdatable::onReceiveMiscUpdate(simplenetwork::Message & msg) {
        fprintf(stderr, "WARNING: %s UNIMPLEMENTED RECEIVE misc update\n", getIdInformation(this).c_str());
        return false;
    }
    bool RascUpdatable::onReceiveCheckedUpdate(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) {
        fprintf(stderr, "WARNING: %s UNIMPLEMENTED RECEIVE checked update\n", getIdInformation(this).c_str());
        return false;
    }

    bool RascUpdatable::onServerUpdateMissingChild(simplenetwork::Message & msg, uint64_t childId) {
        fprintf(stderr, "WARNING: %s MISSING CHILD on server update, id: %llu\n", getIdInformation(this).c_str(), (unsigned long long)childId);
        return false;
    }
    bool RascUpdatable::onInitialDataMissingChild(simplenetwork::Message & msg, uint64_t childId) {
        fprintf(stderr, "WARNING: %s MISSING CHILD on initial data, id: %llu\n", getIdInformation(this).c_str(), (unsigned long long)childId);
        return false;
    }
    bool RascUpdatable::onMiscUpdateMissingChild(simplenetwork::Message & msg, uint64_t childId) {
        fprintf(stderr, "WARNING: %s MISSING CHILD on misc update, id: %llu\n", getIdInformation(this).c_str(), (unsigned long long)childId);
        return false;
    }
    bool RascUpdatable::onCheckedUpdateMissingChild(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, uint64_t childId, PlayerData * contextPlayer) {
        fprintf(stderr, "WARNING: %s MISSING CHILD on checked update, id: %llu\n", getIdInformation(this).c_str(), (unsigned long long)childId);
        return false;
    }
}

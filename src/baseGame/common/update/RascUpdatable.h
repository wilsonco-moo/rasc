/*
 * RascUpdatable.h
 *
 *  Created on: 22 Feb 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UPDATE_RASCUPDATABLE_H_
#define BASEGAME_COMMON_UPDATE_RASCUPDATABLE_H_

#include <sockets/plus/util/MutexPtrIterator.h>
#include <sockets/plus/util/LockedIterable.h>
#include <sockets/socketsTypedefs.h>
#include <unordered_map>
#include <cstddef>
#include <vector>
#include <atomic>
#include <mutex>

#include "../util/templateTypes/PairIterable.h"
#include "updateTypes/MiscUpdate.h"

namespace simplenetwork {
    class Message;
}

namespace rasc {

    class TopLevelRascUpdatable;
    class PlayerData;
    class ServerBox;

    class RascUpdatable {
        friend class TopLevelRascUpdatable;
    public:

        /**
         * This selects data structure mode for this RascUpdatable. Array mode stores the child instances
         * in an automatically extended array, whereas map mode stores the child instances in an unordered_map.
         *
         * If objects are being regularly created and deleted, and require unique ids, then map mode must be used.
         *
         * If the number of objects remains constant, such as provinces in a map, array mode should be used as
         * it is much faster.
         *
         * NOTE: NO SYNCHRONISATION IS DONE IN array MODE, as we assume all objects are added once on creation.
         *       IN map MODE, ALL ACCESS IS SYNCHRONISED TO A MUTEX.
         */
        enum class Mode {
            array,
            map
        };

    private:
        // Don't allow copying: We hold raw resources.
        RascUpdatable(const RascUpdatable & other);
        RascUpdatable & operator = (const RascUpdatable & other);

        // ------------------ Config stuff -----------------

        // Our mode: Array or map mode.
        Mode mode;
        // The number of bytes required to identify each of our children.
        int identifierBytes;
        // Our parent rasc updatable object. This is NULL for the root object.
        RascUpdatable * rascUpdatableParent;
        // Our root rasc updatable. This is set once we are added to a TopLevelRascUpdatable.
        TopLevelRascUpdatable * rascUpdatableRoot;

        // ----------------- Data structures ---------------

        // The forward lookup table:
        // In array mode: This stores a std::vector for forward lookups.
        // In map mode:   This includes a mutex, an unordered map, and an index for storing the next child id.
        union {
            std::vector<RascUpdatable *> forwardArray;
            struct {
                // The id we are up to when adding children: this stores the value of the largest id ever added
                uint64_t childIdUpTo;
                std::recursive_mutex * mutex;
                std::unordered_map<uint64_t, RascUpdatable *> * map;
            } forwardMap;
        };

        // The reverse lookup table, used in both mapmodes.
        std::unordered_map<RascUpdatable *, uint64_t> rtable;

        // --------------- Update header -------------------

        // The length and pointer for the update header.
        size_t cachedUpdateHeaderLength;
        void * cachedUpdateHeader;

        // Allows quick and thread safe access to the knowledge of whether we have already generated the update header.
        std::atomic_bool haveUpdateHeader;
        // It would be too much overhead to define a separate mutex for each object, just to synchronise generateCachedUpdateHeader
        // calls. A compromise here is to use a static mutex.
        static std::recursive_mutex updateHeaderMutex;

    public:
        RascUpdatable(Mode mode = Mode::array, int identifierBytes = 1);
        virtual ~RascUpdatable(void);

        // ------------------- 'Getter' methods ---------------------------

        /**
         * Gets the data structure mode we are using. This is either array mode, or map mode.
         */
        inline Mode getRascUpdatableDataStructureMode(void) const {
            return mode;
        }
        /**
         * Gets the number of bytes required to uniquely identify one of our children. Values of this length
         * are read and written to the network to identify our children.
         */
        inline int getIdentifierBytes(void) const {
            return identifierBytes;
        }
        /**
         * Gets the rasc updatable object which is our parent. This will be NULL for the root node (which
         * should be a TopLevelRascUpdatable).
         */
        inline RascUpdatable * getRascUpdatableParent(void) const {
            return rascUpdatableParent;
        }
        /**
         * Gets the root rasc updatable object. This will be NULL until we are added to an object.
         */
        inline TopLevelRascUpdatable * getRascUpdatableRoot(void) const {
            return rascUpdatableRoot;
        }

        /**
         * This works out our rasc updatable id, (our id within our parent object), by doing a reverse
         * lookup in our parent object.
         * This will cause undefined behaviour if we are not registered, or we are the root object.
         */
        uint64_t getRascUpdatableId(void);

        /**
         * Prints to standard output information about the id of this rasc updatable, in the format:
         * RascUpdatable [root, 23, 12, 4]
         * Supposing we have an id of 4, within a parent with and id of 12, within a parent with an
         * if of 23, within the root node.
         *
         * A method to access this information as a std::string could be added, but then we would have
         * to include the <string> header here.
         */
        void printIdInformation(void);

        // ------------------- General use methods ------------------------

        /**
         * Writes an update header that will direct a message of the specified type to this instance in the hierarchy.
         * All of this data is generated the first time this is called, and cached from there on. This means this call
         * is very fast.
         *
         * The format of the update header is as follows:
         *  > An one byte unsigned value representing the depth in the RascUpdatable hierarchy,
         *    where 0 represents the root node, 1 is the child of the root, etc etc.
         *  > For each RascUpdatable down the hierarchy, below the root node, their id within their parent. This value
         *    stored to a number of bytes specified by their parent RascUpdatable's identifierBytes value.
         *
         * NOTE: THIS WILL CAUSE UNDEFINED BEHAVIOUR, if we (or any parent of ours up to the root), are not registered.
         *       As such, it is UNDEFINED to register or unregister us while this is being called by another thread.
         *       Since we assume that we are registered during this call, little synchronisation is needed.
         */
        void writeUpdateHeader(simplenetwork::Message & message);

        // ------------------ Registration/container methods --------------

    protected:
        /**
         * Registers a child update object, using the next id along.
         * The id is automatically chosen as an id that does not already exist:
         *  > The largest id ever added to this RascUpdatable so far, plus one,
         * This is an increasing sequence starting from zero.
         * The chosen id is returned.
         */
        uint64_t registerChild(RascUpdatable * child);

        /**
         * Registers a child with the specified id. This can only be done in map mode.
         *
         * NOTE: IF THE SPECIFIED ID IS ALREADY REGISTERED, THIS CAUSES UNDEFINED BEHAVIOUR.
         */
        void registerChild(RascUpdatable * child, uint64_t id);

        /**
         * Unregisters the specified child, by their pointer.
         * If the specified child is not registered, this will do nothing.
         * This is only possible to do in map mode.
         */
        void unregisterChild(RascUpdatable * child);

        /**
         * Unregisters the specified child, by their id.
         * If the specified child is not registered, this will do nothing.
         * This is only possible to do in map mode.
         */
        void unregisterChild(uint64_t id);

    public:
        /**
         * Returns true if the specified child instance is registered with us.
         */
        bool isChildRegistered(RascUpdatable * child);

        /**
         * Returns true if the specified child id is registered with us.
         */
        bool isChildRegistered(uint64_t id);

        // ----------------------- Lookup methods -----------------------------

        /**
         * Returns the child object related to the specified id.
         *
         * NOTE: IF THE SPECIFIED ID IS NOT REGISTERED, THIS CAUSES UNDEFINED BEHAVIOUR.
         */
        RascUpdatable * childForwardLookup(uint64_t id);

        /**
         * Returns the child object related to the specified id. Unlike childForwardLookup, this
         * first does a check to see if a child with that id exists or not. If it does not exist, NULL is returned.
         *
         * This is slower than childForwardLookup, but faster than separate calls to isChildRegistered and childForwardLookup.
         */
        RascUpdatable * childForwardLookupChecked(uint64_t id);

        /**
         * Returns the id of the specified child. If the specified child is not
         * registered, this causes undefined behaviour.
         *
         * NOTE: IF THE SPECIFIED CHILD IS NOT REGISTERED, THIS CAUSES UNDEFINED BEHAVIOUR.
         */
        uint64_t childReverseLookup(RascUpdatable * child);

        /**
         * Gets the number of rasc updatable children that we have.
         */
        size_t getRascUpdatableChildCount(void);

    private:
        // ----------------------------- Internal methods -------------------------------

        /**
         * This is called just before we are unregistered from our parent container. This must
         * erase any cached update header data, for us and our child objects.
         * In map mode, this is called from within the mutex.
         *
         * Note: If a child object, which has child objects of it's own, is removed, then only
         *       the top object should have it's parent reset.
         */
        void onUnregister(bool resetParent);

        /**
         * This is called when we are registered by a rasc updatable which has an initialised
         * root rasc updatable. This will recursively call itself for all of our child objects.
         */
        void setRascUpdatableRoot(TopLevelRascUpdatable * root);

        /**
         * This is called whenever we try to write our update header, but don't have one.
         * This uses writeIdSection to generate an update header.
         */
        void generateCachedUpdateHeader(void);

        /**
         * This recursively writes the ids of our parent objects to the message, and returns the depth
         * we are in the hierarchy. This is used by generateCachedUpdateHeader to generate the update
         * header data.
         */
        uint8_t writeIdSection(struct simplenetwork_Message * message);

    public:
        // ---------------------------- Array iteration ---------------------------------
        /**
         * This allows iteration through each child object and associated id, in array mode.
         * for (std::pair<const uint64_t, RascUpdatable *> child : rascUpdatable.arrayChildren()) { ...
         *
         * Note: This is guaranteed to iterate through child objects in ascending order of their id, starting from zero.
         */
        PairIterable<uint64_t, RascUpdatable *, std::vector<RascUpdatable *>> arrayChildrenPair(void) const;
        /**
         * This allows access to our internal array of children, for iteration purposes.
         * This returns a const reference to our internal children vector, so it cannot be modified from outside.
         * for (RascUpdatable * child : rascUpdatable.arrayChildren()) { ...
         *
         * Note: This is guaranteed to iterate through child objects in ascending order of their id, starting from zero.
         */
        const std::vector<RascUpdatable *> & arrayChildren(void) const;

        // ---------------------------- Map iteration -----------------------------------
        /**
         * This allows synchronised raw iteration through each child object, in map mode.
         * for (std::pair<const uint64_t, RascUpdatable *> child : rascUpdatable.mapChildrenRaw()) { ...
         *
         * Note: The order of iteration through child objects is undefined, since this relies on an unordered map.
         */
        simplenetwork::LockedIterable<std::unordered_map<uint64_t, RascUpdatable *>> mapChildren(void);

        // ================================ GENERAL METHODS, TO BE OVERIDDEN ==========================================
        // If any of these methods are NOT overridden, the default implementation is to print a warning,
        // with an identification of which rasc updatable instance is being used.
        //
        // Note: The receive methods, and the missing child methods, return a bool. This is so we can stop
        //       iterating through the message if an error of some kind occurs. Here:
        //       true  MEANS SUCCESSFUL
        //       false MEANS FAILURE
        //       If TopLevelRascUpdatable finds one of the receive or missing child methods to return a failure,
        //       it WILL ABORT READING THE REST OF THE MESSAGE, to avoid reading nonsense, caused by being in the
        //       wrong place in the message.
        //
        //       User implementations of these methods should return true, unless something major and unexpected
        //       happens.

        /**
         * This is called by the server, on selected objects, at the appropriate time each
         * turn. This should either do nothing (not be overridden), or write one or more misc updates
         * (using writeUpdateHeader()) to the message.
         *
         * This misc update should contain any required updates to stats etc. For example, for player
         * data this could include stat updates like manpower, and for provinces this could include
         * dynamic updates to traits.
         *
         * Note that no stats should be directly modified in this method. Updates should be written
         * using MiscUpdate::server, to indirectly apply any updates using a misc update.
         * Indeed this is called using MiscUpdate::server.
         *
         * If not overridden this method will do nothing - i.e: Not generate any misc update.
         */
        virtual void onGenerateTurnMiscUpdate(MiscUpdate update);

    protected:
        /**
         * This is called to generate data for an entire hierarchy server update, called with
         * TopLevelRascUpdatable's generateServerUpdate method.
         *
         * Note that this is not (currently) used. Per-turn updates are instead applied to objects
         * selected by the server, at the most appropriate time, using onGenerateTurnMiscUpdate.
         * Per-turn updates are not applied to the entire rasc updatable hierarchy at once.
         */
        virtual void onGenerateServerUpdate(simplenetwork::Message & msg);
        /**
         * This is called to receive data from an entire hierarchy server update, called with
         * TopLevelRascUpdatable's generateServerUpdate method.
         *
         * Note that this is not (currently) used. Per-turn updates are instead applied to objects
         * selected by the server, at the most appropriate time, using onGenerateTurnMiscUpdate.
         * Per-turn updates are not applied to the entire rasc updatable hierarchy at once.
         */
        virtual bool onReceiveServerUpdate(simplenetwork::Message & msg);


        /**
         * This must generate all initial data about the object, for example when a player joins, or when saving.
         *
         * The parameter isSaving allows the server to contain data which needs to be persistent, but does not
         * need sending to the client.
         *
         * NOTE: If a RascUpdatable instance wants to write any data to a server update here, it should first
         *       call writeUpdateHeader().
         *
         * By default this does nothing.
         */
        virtual void onGenerateInitialData(simplenetwork::Message & msg, bool isSaving);
        /**
         * This must set all initial data about the object, for example when a player joins.
         *
         * This should read in all the data written to the message in a corresponding onGenerateInitialData
         * call.
         *
         * By default this prints an "unimplemented" warning, and returns a failure.
         */
        virtual bool onReceiveInitialData(simplenetwork::Message & msg);


        /**
         * This allows a misc update to happen, such as the user doing an action mid-turn.
         *
         * Note: There are no corresponding write methods for misc updates defined here, since their use is more random.
         *
         * By default this prints an "unimplemented" warning, and returns a failure.
         */
        virtual bool onReceiveMiscUpdate(simplenetwork::Message & msg);
        /**
         * NOTE: This is only relevant on the server side.
         * > This allows a checked update to happen. A checked update is an update sent by the client, and received by the server.
         *   It allows an update to be processed by the server, before being distributed to all clients.
         * > A checked update is sent to the server, the server processes it, generates a misc update, which is then sent to all
         *   clients. All of this MUST happen atomically, within the server's mutex, to ensure correct behaviour.
         *
         * When this is received, the target RascUpdatable must process the checked update, and write data to the other
         * message provided. This other message is then distributed as a misc update.
         *
         * NOTE: The server should NOT apply the update locally, as the server will immediately receive the update that it writes.
         *
         * Note: There are no corresponding write methods for misc updates defined here, since their use is more random.
         *
         * By default this prints an "unimplemented" warning, and returns a failure.
         */
        virtual bool onReceiveCheckedUpdate(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer);

        /**
         * These methods are run when their respective receive methods encounter a child which does
         * not exist. By default these print a warning.
         *
         * NOTE: To allow non-existent children, i.e: In something rapidly changing, where it is not possible
         *       to keep everything *perfectly* synchronised:
         *        > Override one of these methods
         *        > These methods MUST READ FROM THE MESSAGE, the EXACT amount of data which the respective
         *          receive method SHOULD read, if there was an existing child here.
         *          - If this is not done, the following update will be read wrong.
         *        > onInitialDataMissingChild can be used as a neat way to add children when initially loading.
         *
         * By default, all of these methods print a warning, and return a failure.
         */
        virtual bool onServerUpdateMissingChild(simplenetwork::Message & msg, uint64_t childId);
        virtual bool onInitialDataMissingChild(simplenetwork::Message & msg, uint64_t childId);
        virtual bool onMiscUpdateMissingChild(simplenetwork::Message & msg, uint64_t childId);
        virtual bool onCheckedUpdateMissingChild(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, uint64_t childId, PlayerData * contextPlayer);
    };
}

#endif


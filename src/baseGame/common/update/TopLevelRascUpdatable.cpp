/*
 * TopLevelRascUpdatable.cpp
 *
 *  Created on: 1 Mar 2019
 *      Author: wilson
 */

#include "TopLevelRascUpdatable.h"
#include <sockets/plus/message/Message.h>

namespace rasc {

    TopLevelRascUpdatable::TopLevelRascUpdatable(RascUpdatable::Mode mode, int identifierBytes) :
        RascUpdatable(mode, identifierBytes) {
        // Set the root object to ourself.
        rascUpdatableRoot = this;
    }

    TopLevelRascUpdatable::~TopLevelRascUpdatable(void) {
    }



    /**
     * Calls the specified generateFunc for each object, then recursively
     * calls recurseFunc for each child of that object.
     */
    #define RASC_UPDATABLE_GENERATE(recurseFunc, generateFunc)                              \
        object->generateFunc;                                                               \
        if (object->getRascUpdatableDataStructureMode() == RascUpdatable::Mode::array) {    \
            for (RascUpdatable * child : object->arrayChildren()) {                         \
                recurseFunc;                                                                \
            }                                                                               \
        } else {                                                                            \
            for (std::pair<uint64_t, RascUpdatable *> childPair : object->mapChildren()) {  \
                RascUpdatable * child = childPair.second;                                   \
                recurseFunc;                                                                \
            }                                                                               \
        }

    /**
     * Iterates down the hierarchy by reading ids from msg, until we either find a child
     * which does not exist, or we find the object.
     * If we find a child which does not exist, we jump to the ignoreObject label, after
     * running the specified "missing child" method. If that missing child method returns a failure,
     * then we immediately abort by returning.
     */
    #define RASC_UPDATABLE_RESOLVE(msg, nonExistFunc)                                                  \
        RascUpdatable * object = this;                                                                 \
        int depth = (msg).read8();                                                                     \
        for (int i = 0; i < depth; i++) {                                                              \
            uint64_t childId = (msg).readVariableSize(object->getIdentifierBytes());                   \
            RascUpdatable * child = object->childForwardLookupChecked(childId);                        \
            if (child == NULL) {                                                                       \
                if (!object->nonExistFunc) return false; /* Abort if the missing child method fails. */\
                goto ignoreObject;                                                                     \
            }                                                                                          \
            object = child;                                                                            \
        }


    // ------------------------ Server updates --------------------------------------------------------------------

    // This calls onGenerateServerUpdate for us all other child rasc updatable
    // instances in the hierarchy, by recursively calling itself (internalGenerateServerUpdate).
    void TopLevelRascUpdatable::internalGenerateServerUpdate(RascUpdatable * object, simplenetwork::Message & msg) {
        RASC_UPDATABLE_GENERATE(internalGenerateServerUpdate(child, msg), onGenerateServerUpdate(msg))
    }
    void TopLevelRascUpdatable::generateServerUpdate(simplenetwork::Message & msg) {
        internalGenerateServerUpdate(this, msg);
    }
    bool TopLevelRascUpdatable::receiveServerUpdate(simplenetwork::Message & msg) {
        // Repeat until we have read the entire message: Get the correct object in the hierarchy, call onReceiveServerUpdate.
        // If that object does not exist, call its parent's onServerUpdateMissingChild method.
        while(!msg.finishedReading()) {
            RASC_UPDATABLE_RESOLVE(msg, onServerUpdateMissingChild(msg, childId))
            if (!object->onReceiveServerUpdate(msg)) return false; // Abort if the receive method fails.
            ignoreObject:;
        }
        return true;
    }

    // -------------------------- Initial data --------------------------------------------------------------------

    // This calls onGenerateInitialData for us all other child rasc updatable
    // instances in the hierarchy, by recursively calling itself (internalGenerateInitialData).
    void TopLevelRascUpdatable::internalGenerateInitialData(RascUpdatable * object, simplenetwork::Message & msg, bool isSaving) {
        RASC_UPDATABLE_GENERATE(internalGenerateInitialData(child, msg, isSaving), onGenerateInitialData(msg, isSaving))
    }
    void TopLevelRascUpdatable::generateInitialData(simplenetwork::Message & msg, bool isSaving) {
        internalGenerateInitialData(this, msg, isSaving);
    }
    bool TopLevelRascUpdatable::receiveInitialData(simplenetwork::Message & msg) {
        // Repeat until we have read the entire message: Get the correct object in the hierarchy, call onReceiveInitialData.
        // If that object does not exist, call its parent's onInitialDataMissingChild method.
        while(!msg.finishedReading()) {
            RASC_UPDATABLE_RESOLVE(msg, onInitialDataMissingChild(msg, childId))
            if (!object->onReceiveInitialData(msg)) return false; // Abort if the receive method fails.
            ignoreObject:;
        }
        return true;
    }

    // -------------------------- Misc and checked updates --------------------------------------------------------

    bool TopLevelRascUpdatable::receiveMiscUpdate(simplenetwork::Message & msg) {
        // Repeat until we have read the entire message: Get the correct object in the hierarchy, call onReceiveMiscUpdate.
        // If that object does not exist, call its parent's onMiscUpdateMissingChild method.
        while(!msg.finishedReading()) {
            RASC_UPDATABLE_RESOLVE(msg, onMiscUpdateMissingChild(msg, childId))
            if (!object->onReceiveMiscUpdate(msg)) return false; // Abort if the receive method fails.
            ignoreObject:;
        }
        return true;
    }

    bool TopLevelRascUpdatable::receiveCheckedUpdate(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) {
        // Repeat until we have read the entire message: Get the correct object in the hierarchy, call onReceiveCheckedUpdate.
        // If that object does not exist, call its parent's onCheckedUpdateMissingChild method.
        while(!checkedUpdate.finishedReading()) {
            RASC_UPDATABLE_RESOLVE(checkedUpdate, onCheckedUpdateMissingChild(checkedUpdate, miscUpdate, childId, contextPlayer))
            {
                bool ret = object->onReceiveCheckedUpdate(checkedUpdate, miscUpdate, contextPlayer);
                // Always IMMEDIETELY process the misc update after processing each checked update.
                receiveMiscUpdate(miscUpdate);
                if (!ret) return false; // Abort if receiving the checked update reported a failure.
            }
            ignoreObject:;
        }
        return true;
    }
}

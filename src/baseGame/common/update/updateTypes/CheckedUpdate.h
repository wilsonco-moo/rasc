/*
 * CheckedUpdate.h
 *
 *  Created on: 18 Sep 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UPDATE_UPDATETYPES_CHECKEDUPDATE_H_
#define BASEGAME_COMMON_UPDATE_UPDATETYPES_CHECKEDUPDATE_H_

#include "ProvinceUpdate.h"

namespace simplenetwork {
    class NetworkInterface;
}



namespace rasc {


    /**
     * The CheckedUpdate class provides a type of province update which must only be called from the client side.
     * When a misc update is done, the update is automatically applied by the server and all clients.
     *
     * When a client calls a CheckedUpdate, the checked update is read only by the server. The server then generates
     * a misc update from this, which it applies then distributes to all clients. This allows actions which require server
     * control to be implemented easily, such as merging, splitting and moving armies.
     *
     * When the server is generating a misc update, they can either use a MiscUpdate::server or MiscUpdate::client.
     * Using MiscUpdate::server is preferable (and will result in immediate update), but not required, since the
     * misc update is read in automatically anyway.
     */
    class CheckedUpdate : public ProvinceUpdate {

    private:

        /**
         * Note: The main constructor is private, but still implemented. CheckedUpdate instances should be created exclusively
         * using the static methods CheckedUpdate::client(), CheckedUpdate::clientAuto().
         */
        CheckedUpdate(UpdateType updateType, simplenetwork::Message * msgPtr, void * clientOrServer);

        // Make copy constructor, copy assignment, move assignment private.
        // This makes it impossible for this class to be used for anything other than it's intended purpose:
        // a function parameter.
        CheckedUpdate(CheckedUpdate & other);
        CheckedUpdate & operator = (CheckedUpdate & other);
        CheckedUpdate & operator = (CheckedUpdate && other);

    public:
        // Allow the move constructor, so we can be passed as a function parameter.
        CheckedUpdate(CheckedUpdate && other);

        virtual ~CheckedUpdate();


        /**
         * Generates a standard checked update, which acts
         */
        static CheckedUpdate client(simplenetwork::Message & msg);

        /**
         * Generates a province update type for client auto update mode.
         * This automatically creates a message for the province to write to, then automatically
         * sends it when updating is done.
         */
        static CheckedUpdate clientAuto(simplenetwork::NetworkInterface * netIntf);

    };

}

#endif

/*
 * MiscUpdate.h
 *
 *  Created on: 18 Sep 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UPDATE_UPDATETYPES_MISCUPDATE_H_
#define BASEGAME_COMMON_UPDATE_UPDATETYPES_MISCUPDATE_H_

#include "ProvinceUpdate.h"

namespace simplenetwork {
    class NetworkInterface;
    class SimpleNetworkServer;
}

namespace rasc {
    class TopLevelRascUpdatable;

    /**
     * The MiscUpdate class allows the type of a misc update to be defined.
     * The class provides a message for the updates to be written to, while automatically doing certain
     * actions when the instance is destroyed, such as automatically sending the message (in some modes).
     *
     * These actions are implemented through this classes' destructor.
     */
    class MiscUpdate : public ProvinceUpdate {

    private:
        /**
         * A pointer to the top level rasc updatable. We must never free this, regardless of the update type.
         *
         * This is set in server update modes so that the server can apply updates to it's own rasc updatable hierarchy automatically.
         *
         * This is only defined in MiscUpdate, (not CheckedUpdate), as CheckedUpdate only applies to the client side,
         * and will never automatically update the GameMap.
         */
        TopLevelRascUpdatable * topLevel;

        /**
         * Note: The main constructor is private, but still implemented. MiscUpdate instances should be created exclusively
         * using the static methods MiscUpdate::client(), MiscUpdate::clientAuto(), MiscUpdate::server(), MiscUpdate::serverAuto().
         */
        MiscUpdate(UpdateType updateType, simplenetwork::Message * msgPtr, TopLevelRascUpdatable * topLevel, void * clientOrServer);

        // Make copy constructor, copy assignment, move assignment private.
        // This makes it impossible for this class to be used for anything other than it's intended purpose:
        // a function parameter.
        MiscUpdate(MiscUpdate & other);
        MiscUpdate & operator = (MiscUpdate & other);
        MiscUpdate & operator = (MiscUpdate && other);

    public:
        // Allow the move constructor, so we can be passed as a function parameter.
        MiscUpdate(MiscUpdate && other);

        virtual ~MiscUpdate(void);


        /**
         * Generates a misc update type for client update mode.
         * This will do nothing other than allow the rasc updatable to write to the message.
         */
        static MiscUpdate client(simplenetwork::Message & msg);

        /**
         * Generates a misc update type for client auto update mode.
         * This automatically creates a message for the rasc updatable to write to, then automatically
         * sends it when updating is done.
         */
        static MiscUpdate clientAuto(simplenetwork::NetworkInterface * netIntf);

        /**
         * Generates a misc update type for server update mode.
         * This provides a message for the rasc updatable to write to, then automatically applies the update
         * at the end. The ServerBox must be provided, to allow automatically applying the update locally.
         *
         * Note: Even if this is called multiple times using the same message, ONLY the update most recently
         *       written to the message is applied locally, when this MiscUpdate is processed. The entire message
         *       is not read, only between the current read position and the end. Unless the read position is
         *       reset, we won't repeat reading in old messages.
         */
        static MiscUpdate server(simplenetwork::Message & msg, TopLevelRascUpdatable * topLevel);

        /**
         * Generates a misc update type for server auto update mode.
         * This creates a message for the rasc updatable to write to. When updating is done, the
         * message is automatically sent to all connected clients, then the update is applied locally.
         * The ServerBox must be provided, to allow automatically applying the update locally.
         *
         *  NOTE: THIS METHOD SYNCHRONISES ON THE SERVER'S MUTEX, BY ITERATING THROUGH IT'S CLIENTS.
         */
        static MiscUpdate serverAuto(simplenetwork::SimpleNetworkServer * server, TopLevelRascUpdatable * topLevel);
    };
}

#endif

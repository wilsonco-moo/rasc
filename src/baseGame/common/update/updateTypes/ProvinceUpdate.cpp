/*
 * ProvinceUpdate.cpp
 *
 *  Created on: 18 Sep 2018
 *      Author: wilson
 */

#include "ProvinceUpdate.h"

#include <cstdlib>




namespace rasc {


    ProvinceUpdate::ProvinceUpdate(UpdateType updateType, simplenetwork::Message * msgPtr, void * clientOrServer) :
        updateType(updateType),
        msgPtr(msgPtr),
        clientOrServer(clientOrServer) {
    }
    ProvinceUpdate::ProvinceUpdate(ProvinceUpdate && other) {
        //std::cout << "Move constructing province update.\n";
        updateType = other.updateType;
        msgPtr = other.msgPtr;
        clientOrServer = other.clientOrServer;
        other.msgPtr = NULL;
    }
    ProvinceUpdate::~ProvinceUpdate(void) {
    }
}

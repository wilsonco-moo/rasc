/*
 * MiscUpdate.cpp
 *
 *  Created on: 18 Sep 2018
 *      Author: wilson
 */

#include "MiscUpdate.h"

#include <cstdlib>

#include <sockets/plus/message/Message.h>
#include <sockets/plus/networkinterface/NetworkInterface.h>
#include <sockets/plus/simpleNetworkServer/SimpleNetworkServer.h>
#include "../../GameMessages.h"
#include "../TopLevelRascUpdatable.h"

namespace rasc {

    MiscUpdate::MiscUpdate(UpdateType updateType, simplenetwork::Message * msgPtr, TopLevelRascUpdatable * topLevel, void * clientOrServer) :
        ProvinceUpdate(updateType, msgPtr, clientOrServer),
        topLevel(topLevel) {
    }

    MiscUpdate::MiscUpdate(MiscUpdate && other) : ProvinceUpdate((ProvinceUpdate &&)other) {
        topLevel = other.topLevel;
    }

    MiscUpdate::~MiscUpdate(void) {
        if (msgPtr == NULL) {
            // If the message pointer is NULL, we are destroying a ProvinceUpdate who's message has been transferred
            // away to a new instance, which was move-constructed. As such, we must do nothing.
            return;
        }
        switch(updateType) {
        case UpdateType::miscClient:
            // Don't do anything in basic client update mode, as there is nothing that needs to be done.
            // Since this is not an auto update mode, we did not dynamically allocated the message, so nothing needs deleting.
            break;

        case UpdateType::miscClientAuto:
            // In client auto mode, we must first send the message.
            ((simplenetwork::NetworkInterface *)clientOrServer)->send(*msgPtr);
            // Next, we must delete the message, since we dynamically allocated it when we were constructed.
            delete msgPtr;
            break;

        case UpdateType::miscServer:
            // In server update mode, we must do nothing other than apply the update locally to our rasc updatable hierarchy.
            topLevel->receiveMiscUpdate(*msgPtr);
            // Note, since this is not an auto update mode, we did not dynamically allocated the message, so nothing needs deleting.
            break;

        case UpdateType::miscServerAuto:
            // In server auto update mode, we must first send the message to all clients, then apply it to ourself.
            // So, first loop through all clients
            for (simplenetwork::NetworkInterface * netIntf : ((simplenetwork::SimpleNetworkServer *)clientOrServer)->clientsRaw()) {
                // Send all clients the message.
                netIntf->send(*msgPtr);
            }
            // Then apply the update locally to our rasc updatable hierarchy.
            topLevel->receiveMiscUpdate(*msgPtr);
            // Finally, we must delete our message pointer, since in auto mode we dynamically allocated it.
            delete msgPtr;
            break;

        default:
            // This should never actually happen, as the checked update types do not happen here,
            // but is included to suppress a compiler warning.
            break;
        }
    }




    MiscUpdate MiscUpdate::client(simplenetwork::Message & msg) {
        // In client mode, we create a province update, and supply it a pointer to the supplied message.
        // The province pointer is set to NULL, since this is not a server update mode, so we do not have to apply the update to ourself.
        // The client/server pointer is set to NULL, since this is not an auto update mode.
        return MiscUpdate(UpdateType::miscClient, &msg, NULL, NULL);
    }
    MiscUpdate MiscUpdate::clientAuto(simplenetwork::NetworkInterface * netIntf) {
        // In client auto mode, we first create a new dynamically allocated message.
        simplenetwork::Message * msg = new simplenetwork::Message(ClientToServer::miscUpdate);
        // Next we create a province update, and supply it a pointer to this dynamically allocated message.
        // The province pointer is set to NULL, since this is not a server update mode, so we do not have to apply the update to ourself.
        // The client/server pointer is set to the supplied NetworkInterface pointer.
        return MiscUpdate(UpdateType::miscClientAuto, msg, NULL, (void *)netIntf);
    }
    MiscUpdate MiscUpdate::server(simplenetwork::Message & msg, TopLevelRascUpdatable * topLevel) {
        // In server mode, we create a province update, and supply it a pointer to the supplied message.
        // We also supply a pointer to a province, since this is a server update mode. In server update mode, the province is automatically updated locally.
        // The client/server pointer is set to NULL, since this is not an auto update mode.
        return MiscUpdate(UpdateType::miscServer, &msg, topLevel, NULL);
    }
    MiscUpdate MiscUpdate::serverAuto(simplenetwork::SimpleNetworkServer * server, TopLevelRascUpdatable * topLevel) {
        // In server auto mode, we first create a new dynamically allocated message.
        simplenetwork::Message * msg = new simplenetwork::Message(ServerToClient::miscUpdate);
        // Next we create a province update, and supply it a pointer to this dynamically allocated message.
        // We also supply a pointer to a province, since this is a server update mode. In server update mode, the province is automatically updated locally.
        // The client/server pointer is set to the supplied SimpleNetworkServer pointer.
        return MiscUpdate(UpdateType::miscServerAuto, msg, topLevel, (void *)server);
    }

}

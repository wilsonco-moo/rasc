/*
 * CheckedUpdate.cpp
 *
 *  Created on: 18 Sep 2018
 *      Author: wilson
 */

#include "CheckedUpdate.h"

#include <cstdlib>

#include <sockets/plus/message/Message.h>
#include <sockets/plus/networkinterface/NetworkInterface.h>
#include "../../GameMessages.h"

namespace rasc {

    CheckedUpdate::CheckedUpdate(UpdateType updateType, simplenetwork::Message * msgPtr, void * clientOrServer) :
        ProvinceUpdate(updateType, msgPtr, clientOrServer) {
    }

    CheckedUpdate::CheckedUpdate(CheckedUpdate && other) :
        ProvinceUpdate((ProvinceUpdate &&)other) {
    }

    CheckedUpdate::~CheckedUpdate() {
        if (msgPtr == NULL) {
            // If the message pointer is NULL, we are destroying a ProvinceUpdate who's message has been transferred
            // away to a new instance, which was move-constructed. As such, we must do nothing.
            return;
        }
        switch(updateType) {
        case UpdateType::checkedClient:
            // Don't do anything in basic client update mode, as there is nothing that needs to be done.
            // Since this is not an auto update mode, we did not dynamically allocated the message, so nothing needs deleting.
            break;

        case UpdateType::checkedClientAuto:
            // In client auto mode, we must first send the message.
            ((simplenetwork::NetworkInterface *)clientOrServer)->send(*msgPtr);
            // Next, we must delete the message, since we dynamically allocated it when we were constructed.
            delete msgPtr;
            break;

        default:
            // This should never actually happen, as the misc update types do not happen here,
            // but is included to suppress a compiler warning.
            break;
        }
    }




    CheckedUpdate CheckedUpdate::client(simplenetwork::Message & msg) {
        // In client mode, we create a checked update, and supply it a pointer to the supplied message.
        // The client/server pointer is set to NULL, since this is not an auto update mode.
        return CheckedUpdate(UpdateType::checkedClient, &msg, NULL);
    }
    CheckedUpdate CheckedUpdate::clientAuto(simplenetwork::NetworkInterface * netIntf) {
        // In client auto mode, we first create a new dynamically allocated message, setting it's type to checkedUpdate.
        simplenetwork::Message * msg = new simplenetwork::Message(ClientToServer::checkedUpdate);
        // Next we create a province update, and supply it a pointer to this dynamically allocated message.
        // The client/server pointer is set to the supplied NetworkInterface pointer, which is used to send the message, in CheckedUpdate's destructor.
        return CheckedUpdate(UpdateType::checkedClientAuto, msg, (void *)netIntf);
    }


}

/*
 * ProvinceUpdate.h
 *
 *  Created on: 18 Sep 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_UPDATE_UPDATETYPES_PROVINCEUPDATE_H_
#define BASEGAME_COMMON_UPDATE_UPDATETYPES_PROVINCEUPDATE_H_

namespace simplenetwork {
    class Message;
}




namespace rasc {

    /**
     * The ProvinceUpdate class is the superclass for both MiscUpdate and CheckedUpdate.
     */
    class ProvinceUpdate {
    protected:

        /**
         * The update types, for misc update and checked update.
         */
        enum class UpdateType {
            // Misc update types
            miscClient, miscClientAuto, miscServer, miscServerAuto,
            // Checked update types
            checkedClient, checkedClientAuto
        };
        /**
         * The update type we are using.
         */
        UpdateType updateType;
        /**
         * A pointer to our related message. This is a pointer to a stack location, or a dynamically allocated
         * message, depending on the update mode.
         */
        simplenetwork::Message * msgPtr;
        /**
         * This is used in auto update modes. In clientAuto mode, this points to a network interface.
         * In serverAuto mode, this points to a SimpleNetworkServer. This exists so that we can automatically send messages.
         * This also must never be freed.
         */
        void * clientOrServer;

        // Make constructor private, so we can only create instances with the specified 4 methods.
        ProvinceUpdate(UpdateType updateType, simplenetwork::Message * msgPtr, void * clientOrServer);
        // Make copy constructor, copy assignment, move assignment private.
        // This makes it impossible for this class to be used for anything other than it's intended purpose:
        // a function parameter.
        ProvinceUpdate(ProvinceUpdate & other);
        ProvinceUpdate & operator = (ProvinceUpdate & other);
        ProvinceUpdate & operator = (ProvinceUpdate && other);
    public:
        // Allow the move constructor, so we can be passed as a function parameter.
        ProvinceUpdate(ProvinceUpdate && other);

        virtual ~ProvinceUpdate(void);

        /**
         * A public accessor method, to allow update methods using this instance to access the message
         * that we contain.
         */
        inline simplenetwork::Message & msg() {
            return *msgPtr;
        }
    };

}

#endif

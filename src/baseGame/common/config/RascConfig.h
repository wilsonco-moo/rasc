/*
 * RascConfig.h
 *
 *  Created on: 11 Feb 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_CONFIG_RASCCONFIG_H_
#define BASEGAME_COMMON_CONFIG_RASCCONFIG_H_

#include <cstdint>
#include <mutex>
#include <string>

#include <sockets/plus/util/MutexPtr.h>
#include <threading/JobStatus.h>
#include <tinyxml2/tinyxml2.h>

namespace threading {
    class ThreadQueue;
}

namespace rasc {

    /**
     * This is a simple class for controlling persistent settings.
     * RascConfig provides thread-safe access to an XMLDocument, which was initially loaded from a config file,
     * and can be saved at any point in time.
     *
     * This class can be used by both the client and the server.
     */
    class RascConfig {
    public:
        /**
         * The filename of the config file we load in.
         */
        static const std::string CONFIG_FILENAME;

    private:
        // This mutex controls access to the data in the XMLDocument, so someone doesn't modify it while we are saving it.
        std::recursive_mutex dataMutex;
        // This mutex controls adding job to the thread queue, so we don't add a job if we already have a job waiting
        // to start.
        std::mutex jobAddMutex;
        void * token;
        threading::ThreadQueue * threadQueue;
        tinyxml2::XMLDocument document;
        tinyxml2::XMLElement * root;

        // The job id of our most recent job.
        uint64_t mostRecentSaveJob;

    public:

        /**
         * RascConfig requires a thread queue if save operations are going to be done,
         * (as save operations are carried out on a different thread).
         * If you're never gonna call "saveLater", the thread queue can be NULL.
         */
        RascConfig(threading::ThreadQueue * threadQueue);
        virtual ~RascConfig(void);

        /**
         * We will add a job to the thread queue, where we will save our stuff, (as long as we don't already
         * have a save job waiting to start).
         * Note: Can only be called if we were passed a thread queue in the constructor.
         */
        void saveLater(void);

        /**
         * This allows controlled access to our XMLDocument.
         */
        simplenetwork::MutexPtr<tinyxml2::XMLDocument> getDocument(void);

        /**
         * Accesses the base element for a particular configuration name, or creates it and adds it to
         * the document if it does not exist.
         *
         * NOTE: This returned element MUST NOT BE USED outside the scope of the MutexPtr.
         */
        static tinyxml2::XMLElement * getOrCreateBase(tinyxml2::XMLDocument * document, const char * name);

        /**
         * Accesses the base element for a particular configuration name. If the base element does not exist,
         * it is created, then it is initialised with the provided function.
         */
        static tinyxml2::XMLElement * getOrInitBase(
                tinyxml2::XMLDocument * document, const char * name,
                void (*initElement)(tinyxml2::XMLDocument * document, tinyxml2::XMLElement * element));
    };
}

#endif

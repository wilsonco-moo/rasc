/*
 * StaticConfig.cpp
 *
 *  Created on: 11 Feb 2019
 *      Author: wilson
 */

#include "StaticConfig.h"

#include <rascUItheme/utils/config/PathUtils.h>
#include <algorithm>
#include <iostream>
#include <cctype>
#include <mutex>

#include "ReleaseMode.h"

namespace rasc {

    // ===================================== GLOBAL STUFF =====================================

    // Select the appropriate rasc version number depending on whether we are in release mode
    // or development mode.
    #ifdef RASC_RELEASE_MODE
        const std::string StaticConfig::RASC_VERSION_AND_PLATFORM = PACKAGE_PROJECT_NAME;
    #endif
    #ifdef RASC_DEVELOPMENT_MODE
        const std::string StaticConfig::RASC_VERSION_AND_PLATFORM = "rasc-development/unknown";
    #endif

    const std::string StaticConfig::RASC_VERSION_WITHOUT_PLATFORM = std::string(RASC_VERSION_AND_PLATFORM, 0, RASC_VERSION_AND_PLATFORM.find('/'));

    const std::string StaticConfig::MAP_VERSION = "0.5";
    const std::string StaticConfig::RASC_NAME = "rasc";

    // ======================================== LOCAL PATHS ====================================

    const std::string StaticConfig::ASSETS_DIRECTORY = "assets/";

    const std::string StaticConfig::THEMES_DIRECTORY = "themes/";

    const std::string StaticConfig::DEFAULT_THEME_FRIENDLY_NAME = "Default theme";

    #ifdef _WIN32
        const std::string StaticConfig::SHARED_LIBRARY_PREFIX = "";
        const std::string StaticConfig::SHARED_LIBRARY_SUFFIX = ".dll";
    #else
        const std::string StaticConfig::SHARED_LIBRARY_PREFIX = "lib";
        const std::string StaticConfig::SHARED_LIBRARY_SUFFIX = ".so";
    #endif

    const std::string StaticConfig::MAPS_LOCATION = ASSETS_DIRECTORY + "maps/";
    const std::string StaticConfig::TERRAIN_DISPLAY_LOCATION = ASSETS_DIRECTORY + "terrainDisplay/";

    const std::string StaticConfig::GAMEMAP_CONFIG_FILE = "gameMap.xml";
    const std::string StaticConfig::MAP_TEXTURES[] = {
        "geographicalMapMode.png",
        "provincialMapMode.png",
        "areaMapMode.png",
        "continentalMapMode.png"
    };
    const std::string StaticConfig::SAVEFILE_EXTENSION = ".rscdat";



    // ------------------- Internal stuff --------------------

    std::recursive_mutex StaticConfig::mutex;

    bool StaticConfig::haveFoundRascConfigDirectory = false;
    std::string StaticConfig::rascConfigDirectory = "";

    bool StaticConfig::haveFoundSavefileDirectory = false;
    std::string StaticConfig::savefileDirectory = "";

    // -------------------------------------------------------

    const std::string & StaticConfig::getRascConfigDirectory(void) {
        std::lock_guard<std::recursive_mutex> lock(mutex);
        if (!haveFoundRascConfigDirectory) {
            findRascConfigDirectory();
        }
        return rascConfigDirectory;
    }

    const std::string & StaticConfig::getSavefileDirectory(void) {
        std::lock_guard<std::recursive_mutex> lock(mutex);
        if (!haveFoundSavefileDirectory) {
            findSavefileDirectory();
        }
        return savefileDirectory;
    }

    std::vector<std::string> StaticConfig::filterFilesByExtension(const std::vector<std::string> & files, std::string extension) {
        std::vector<std::string> output;
        for (const std::string & str : files) {
            if (str.size() >= extension.size() &&
                std::equal(extension.begin(), extension.end(), str.begin() + (str.size() - extension.size()), str.end())) {
                output.push_back(str);
            }
        }
        return output;
    }
    
    std::vector<std::string> StaticConfig::listFiles(const std::string & directory, const std::string & extension, bool sort) {
        return filterFilesByExtension(rascUItheme::PathUtils::listFiles(directory.c_str(), sort), extension);
    }

    std::string StaticConfig::removeExtension(const std::string & filename) {
        size_t pos = filename.find_last_of('.');
        if (pos == filename.npos) {
            return filename;
        } else {
            return filename.substr(0, pos);
        }
    }

    std::vector<std::string> StaticConfig::removeExtensions(const std::vector<std::string> & files) {
        std::vector<std::string> output;
        output.reserve(files.size());
        for (const std::string & file : files) {
            output.push_back(removeExtension(file));
        }
        return output;
    }

    std::string StaticConfig::removeNonAscii(const std::string & str) {
        std::string output;
        output.reserve(str.length());
        for (unsigned char chr : str) {
            if (chr < 32 || chr > 127) {
                output.push_back('?');
            } else {
                output.push_back(chr);
            }
        }
        return output;
    }

    std::string StaticConfig::camelCaseToSpaceSeparated(const std::string & str) {
        std::string output;
        bool first = true;
        for (char chr : str) {
            if (first) {
                output.push_back(toupper(chr));
                first = false;
            } else if (isupper(chr)) {
                output.push_back(' ');
                output.push_back(tolower(chr));
            } else {
                output.push_back(chr);
            }
        }
        return output;
    }
    
    std::string StaticConfig::themeFileNameToFriendlyName(const std::string & str) {
        size_t totalLen = SHARED_LIBRARY_PREFIX.length() + SHARED_LIBRARY_SUFFIX.length();
        if (str.length() < totalLen) {
            return str;
        }
        return camelCaseToSpaceSeparated(str.substr(SHARED_LIBRARY_PREFIX.length(), str.length() - totalLen));
    }

    std::string StaticConfig::themeFriendlyNameToFilename(const std::string & str) {
        return SHARED_LIBRARY_PREFIX + spaceSeparatedToCamelCase(str) + SHARED_LIBRARY_SUFFIX;
    }

    std::string StaticConfig::spaceSeparatedToCamelCase(const std::string & str) {
        bool justHadSpace = false;
        std::string output;
        for (char chr : str) {
            if (chr == ' ') {
                justHadSpace = true;
            } else if (justHadSpace) {
                output.push_back(toupper(chr));
                justHadSpace = false;
            } else {
                output.push_back(tolower(chr));
            }
        }
        return output;
    }

    bool StaticConfig::isValidMapDirectory(const std::string & directoryIn, bool onClientSide) {
        std::string directory = rascUItheme::PathUtils::addTrailingForwardSlash(directoryIn.c_str());
        if (!rascUItheme::PathUtils::isDirectory(directory.c_str())) return false;
        if (!rascUItheme::PathUtils::isRegularFile((directory + GAMEMAP_CONFIG_FILE).c_str())) return false;
        if (onClientSide) {
            for (int i = 0; i < MapModes::MODES_COUNT; i++) {
                if (!rascUItheme::PathUtils::isRegularFile((directory + MAP_TEXTURES[i]).c_str())) return false;
            }
        }
        return true;
    }

    std::vector<std::string> StaticConfig::getSaves(void) {
       return StaticConfig::removeExtensions(listFiles(getSavefileDirectory(), SAVEFILE_EXTENSION));
    }

    std::string StaticConfig::getFullSaveFilepath(const std::string & saveName) {
        return getSavefileDirectory() + saveName + SAVEFILE_EXTENSION;
    }

    std::vector<std::string> StaticConfig::getMaps(bool onClientSide) {
        std::vector<std::string> mapsProspective = rascUItheme::PathUtils::listFiles(MAPS_LOCATION.c_str()), maps;
        for (std::string & map : mapsProspective) {
            if (isValidMapDirectory(MAPS_LOCATION + map, onClientSide)) {
                maps.emplace_back(std::move(map));
            }
        }
        return maps;
    }

    std::vector<std::string> StaticConfig::getThemeFilenames(void) {
        std::vector<std::string> filenames = rascUItheme::PathUtils::listFiles(THEMES_DIRECTORY.c_str()); // Get the filenames
        std::transform(filenames.begin(), filenames.end(), filenames.begin(), &removeNonAscii);          // Remove non ascii characters
        return filenames;                                                                                // return it.
    }

    std::vector<std::string> StaticConfig::getThemeFriendlyNames(void) {
        std::vector<std::string> filenames = getThemeFilenames();                                            // Get the theme filenames
        std::transform(filenames.begin(), filenames.end(), filenames.begin(), &themeFileNameToFriendlyName); // Convert to friendly names
        return filenames;                                                                                    // Return it.
    }

    std::string StaticConfig::getMapConfigFile(const std::string & mapName) {
        return MAPS_LOCATION + mapName + std::string("/") + GAMEMAP_CONFIG_FILE;
    }

    void StaticConfig::findRascConfigDirectory(void) {
        // Get config directory using path utils.
        std::string confDir = rascUItheme::PathUtils::getConfigDirectory();
        
        // Don't bother trying to create subdirectories if the config directory returned empty.
        if (confDir.empty()) {
            std::cerr << "Warning: StaticConfig::findRascConfigDirectory: Falling back to working directory for Rasc config directory, as we failed to find config directory.\n";
            goto foundRascConfigDirectory;
        }
        
        confDir += RASC_NAME;
        confDir += '/';
        if (!rascUItheme::PathUtils::ensureDirectoryExists(confDir.c_str())) {
            goto foundRascConfigDirectory;
        }
        
        confDir += RASC_VERSION_WITHOUT_PLATFORM;
        confDir += '/';
        if (!rascUItheme::PathUtils::ensureDirectoryExists(confDir.c_str())) {
            goto foundRascConfigDirectory;
        }
        
        // Everything seems successful, write output.
        rascConfigDirectory = confDir;

        foundRascConfigDirectory:;
        haveFoundRascConfigDirectory = true;
        std::cout << "Using Rasc config directory: [" << rascConfigDirectory << "]\n";
    }

    void StaticConfig::findSavefileDirectory(void) {
        savefileDirectory = getRascConfigDirectory();
        std::string subdir = savefileDirectory+"saves/";
        if (rascUItheme::PathUtils::ensureDirectoryExists(subdir.c_str())) {
            savefileDirectory = subdir;
        }
        haveFoundSavefileDirectory = true;
        std::cout << "Using savefile directory: [" << savefileDirectory << "]\n";
    }
}

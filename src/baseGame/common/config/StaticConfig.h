/*
 * StaticConfig.h
 *
 *  Created on: 11 Feb 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_CONFIG_STATICCONFIG_H_
#define BASEGAME_COMMON_CONFIG_STATICCONFIG_H_

#include <string>
#include <vector>

#include "../../client/config/MapModes.h"

namespace std {
    class recursive_mutex;
}

namespace rasc {

    /**
     * This class provides some static functions for dealing with version numbers and filepaths and such.
     */
    class StaticConfig {
    public:

        // ====================================== GLOBAL STUFF ============================================

        /**
         * The name of rasc, i.e: "rasc".
         * Configuration is stored in     .config/<RASC_NAME>/<RASC_VERSION>/
         *                         or     %appdata%/<RASC_NAME>/<RASC_VERSION>/
         */
        static const std::string RASC_NAME;

        /**
         * The game version, including the platform,
         * for example:  rasc-someversion/i386
         *          or:  rasc-development/unknown
         */
        static const std::string RASC_VERSION_AND_PLATFORM;

        /**
         * The game version, without the name of the platform,
         * for example:  rasc-someversion
         *          or:  rasc-development
         */
        static const std::string RASC_VERSION_WITHOUT_PLATFORM;

        /**
         * This stores the version of the map we can read. This should be changed to reflect which version
         * of the map converter which this version of the game supports.
         */
        const static std::string MAP_VERSION;

        // ==================================== LOCAL PATHS AND STUFF ==========================================

        /**
         * The directory which the game's assets are stored in, (with a trailing forward slash).
         * We assume that this already exists.
         */
        const static std::string ASSETS_DIRECTORY;

        /**
         * The directory in which all theme shared libraries are contained, (with a trailing forward slash).
         * We assume that this already exists.
         */
        const static std::string THEMES_DIRECTORY;

        /**
         * The friendly name, (NOT ACTUAL FILENAME), of the default theme to load. We assume that this always
         * exists within the themes directory. To convert this to the actual filename of the default theme,
         * use StaticConfig::themeFriendlyNameToFilename.
         */
        const static std::string DEFAULT_THEME_FRIENDLY_NAME;

        /**
         * These are the platform specific start and end of the filenames for shared libraries.
         * For example, a library called "potato", would in unix like systems be called:
         *  libpotato.so
         * But on windows systems it would be called:
         *  potato.dll
         */
        const static std::string SHARED_LIBRARY_PREFIX;
        const static std::string SHARED_LIBRARY_SUFFIX;

        /**
         * This stores the location, relative to the working directory, of the maps directory,
         * (with a trailing forward slash).
         * We assume that this already exists.
         */
        const static std::string MAPS_LOCATION;

        /**
         * This stores the location, relative to the working directory, of the directory
         * containing images for terrain display, (with a trailing forward slash).
         * We assume that this already exists.
         */
        const static std::string TERRAIN_DISPLAY_LOCATION;

        /**
         * The filename for the xml file within the map which defines provinces etc.
         */
        const static std::string GAMEMAP_CONFIG_FILE;

        /**
         * Stores the texture names for each map mode.
         */
        const static std::string MAP_TEXTURES[MapModes::MODES_COUNT];

        /**
         * The file extension to use for save files.
         */
        const static std::string SAVEFILE_EXTENSION;

        // ======================================== USER DIRECTORIES ======================================

        /**
         * This ensures that the rasc config file directory exists, and returns it, (with a trailing forward slash).
         * The directory that is returned is either empty (the working directory), or has a trailing forward slash.
         */
        static const std::string & getRascConfigDirectory(void);

        /**
         * This ensures that the savefile directory exists, and returns it, (with a trailing forward slash).
         * This will be:
         *  > [CONFIG DIRECTORY]/saves/
         *  > saves/                        if [CONFIG DIRECTORY] falls back to working directory
         *  > the working directory         if we cannot create a saves/ subdirectory.
         */
        static const std::string & getSavefileDirectory(void);

        // =================================== GENERIC FILE MANAGEMENT ====================================

        /**
         * Filters the given std::vector of files by the specified file extension.
         */
        static std::vector<std::string> filterFilesByExtension(const std::vector<std::string> & files, std::string extension);

        /**
         * A wrapper around rascUItheme::PathUtil::listFiles, which filters the files by extension
         * using the function above.
         * NOTE: THIS WILL NOT RETURN THE FULL PATH, ONLY THE FILENAME.
         * By default this is sorted in alphabetical order. This can be disabled by setting sort to false.
         */
        static std::vector<std::string> listFiles(const std::string & directory, const std::string & extension, bool sort = true);
        
        /**
         * Removes the file extension from a filename.
         * Note: This will only remove the LAST part of a multi-part file extension. So bob.tar.xz will become bob.tar.
         * If the filename has no extension, it won't be modified.
         */
        static std::string removeExtension(const std::string & filename);

        /**
         * Removes the file extension from every file in the std::vector of filenames.
         * A new std::vector is returned, with the modified filenames.
         */
        static std::vector<std::string> removeExtensions(const std::vector<std::string> & files);

        // ===================== General purpose string and path functions ================================

        /**
         * Iterates through every character in the string. All non-printable or non-ascii characters are
         * replaced with a "?" character.
         */
        static std::string removeNonAscii(const std::string & str);

        /**
         * These two methods convert back and fourth between camel case and space seperated.
         * For example:
         *       camel case: "thisIsAPotato"
         *  space separated: "This is a potato"
         */
        static std::string camelCaseToSpaceSeparated(const std::string & str);
        static std::string spaceSeparatedToCamelCase(const std::string & str);

        /**
         * These methods convert back and fourth between the filename of themes, (which is platform specific, and does
         * not! include the file's parent directory), and the friendly name of the theme, (which is NOT platform specific,
         * and should be displayed to the user).
         * For example:
         *    Friendly name:                 "The best theme ever"
         *    Filename (unix like systems):  "libtheBestThemeEver.so"
         *    Filename (windows systems):    "theBestThemeEver.dll"
         */
        static std::string themeFileNameToFriendlyName(const std::string & str);
        static std::string themeFriendlyNameToFilename(const std::string & str);

        // ====================== Convenient functions for maps, save files and themes ============================

        /**
         * Gets all of the maps which we have available to us.
         * The map names are sorted in alphabetical order.
         * This returns only the map names, not the actual directories. To get the directory for each, do MAPS_LOCATION + mapName + "/".
         * The parameter onClientSide is required since the server has a different definition
         * of what constitutes a map to the client. The server does not require a map to contain
         * images.
         */
        static std::vector<std::string> getMaps(bool onClientSide);

        /**
         * Gets a list of all the theme filenames. This contains only the filenames, not
         * their contained directories.
         * The theme filenames are sorted in alphabetical order.
         * It is assumed that theme filenames ONLY CONTAIN ASCII CHARACTERS. The method removeNonAscii is automatically
         * used to remove non-ascii characters from the theme filenames.
         */
        static std::vector<std::string> getThemeFilenames(void);

        /**
         * Gets a list of the friendly names for all themes. Any of these can be converted back to the
         * theme filename using themeFriendlyNameToFilename.
         * This internally uses getThemeFilenamesAscii
         */
        static std::vector<std::string> getThemeFriendlyNames(void);

        /**
         * This returns the filename of the config file for the specified map name.
         * For example, getMapConfigFile("someMap") = MAPS_LOCATION + "someMap" + "/" + GAMEMAP_CONFIG_FILE
         */
        static std::string getMapConfigFile(const std::string & mapName);

        static inline std::vector<std::string> getMapsClient(void) { return getMaps(true); }
        static inline std::vector<std::string> getMapsServer(void) { return getMaps(false); }

        /**
         * Returns true if the specified directory contains all of the correct files to be a valid map.
         * If we are on the server side, this will not require map textures to exist.
         */
        static bool isValidMapDirectory(const std::string & directory, bool onClientSide);

        /**
         * This gets the name of all save files available to us. This is simply done by listing files in the save file
         * directory, and filtering by SAVEFILE_EXTENSION, then removing the file extensions.
         */
        static std::vector<std::string> getSaves(void);

        /**
         * Given the name of a save, this returns the full filepath for the save file.
         * This essentially just returns getSavefileDirectory() + saveName + SAVEFILE_EXTENSION.
         */
        static std::string getFullSaveFilepath(const std::string & saveName);

        // ======================================= INTERNAL STUFF =========================================

    private:
        // This controls access, to allow getConfigLocation() and getHomeDirectory() to be cross-platform capable.
        static std::recursive_mutex mutex;

        // This stores true when we have found our configuration directory, to avoid working it out every time we call getConfigLocation().
        // We make the assumption that the config directory will not change, need to be changed, or be modified, while rasc is running.
        static bool haveFoundRascConfigDirectory;
        // This stores the config directory, after we have worked it out.
        static std::string rascConfigDirectory;

        // This stores true when we have found our savefile directory.
        static bool haveFoundSavefileDirectory;
        // This stores our savefile directory, after we have worked it out.
        static std::string savefileDirectory;


        // Instances of this class should never need to be created.
        StaticConfig(void);
        virtual ~StaticConfig(void);

        // Finds the rasc config directory, setting rascConfigDirectory and haveFoundRascConfigDirectory appropriately.
        static void findRascConfigDirectory(void);

        // Finds the home directory, setting homeDirectory and haveFoundHomeDirectory appropriately.
        static void findHomeDirectory(void);

        // Finds the savefile directory, setting savefileDirectory and haveFoundSavefileDirectory appropriately.
        static void findSavefileDirectory(void);
    };
}

#endif

/*
 * RascConfig.cpp
 *
 *  Created on: 12 Feb 2019
 *      Author: wilson
 */

#include "RascConfig.h"

#include <cstddef>
#include <cstdio>

#include <threading/ThreadQueue.h>

#include "../../client/GlobalProperties.h"
#include "../util/XMLUtil.h"
#include "StaticConfig.h"

#define GET_CONFIG_FILE_LOCATION std::string configFileLocation = StaticConfig::getRascConfigDirectory() + CONFIG_FILENAME;
#define ROOT_NAME "config"


namespace rasc {

    const std::string RascConfig::CONFIG_FILENAME = "config.xml";

    RascConfig::RascConfig(threading::ThreadQueue * threadQueue) :
        dataMutex(),
        jobAddMutex(),
        threadQueue(threadQueue),
        document(),
        mostRecentSaveJob(threading::ThreadQueue::INVALID_JOB) {

        // Note: Only create token if user actually passed a thread queue.
        if (threadQueue != NULL) {
            token = threadQueue->createToken();
        }
        
        GET_CONFIG_FILE_LOCATION
        tinyxml2::XMLError err = document.LoadFile(configFileLocation.c_str());

        switch(err) {
        case tinyxml2::XML_SUCCESS:
            printf("Successfully loaded config file: [%s].\n", configFileLocation.c_str());
            break;
        case tinyxml2::XML_ERROR_FILE_NOT_FOUND:
            printf("Config file [%s] has not been created yet.\n", configFileLocation.c_str());
            break;
        default:
            fprintf(stderr, "Failed to load config file [%s], error: %s.\n", configFileLocation.c_str(), tinyxml2::XMLDocument::ErrorIDToName(err));
            break;
        }

        if (err == tinyxml2::XML_SUCCESS) {
            // If the document loaded successfully, get the root element.
            root = readElement(document, ROOT_NAME);
        } else {
            // If the document failed to load, make sure the document is reset to it's initial state, and create
            // the root element.
            document.Clear();
            root = document.NewElement(ROOT_NAME);
            document.InsertEndChild(root);
        }

    }

    RascConfig::~RascConfig(void) {
        // Ensure no further save jobs are running before we are deleted,
        // (if user actually passed us a thread queue).
        if (threadQueue != NULL) {
            threadQueue->deleteToken(token);
        }
    }

    void RascConfig::saveLater(void) {
        // If we don't already have a save job waiting to start
        std::lock_guard<std::mutex> lock(jobAddMutex);
        if (threadQueue->getJobStatus(mostRecentSaveJob) != threading::JobStatus::waiting) {

            // Then add a save job.
            mostRecentSaveJob = threadQueue->add(token, [this](void) {
                std::lock_guard<std::recursive_mutex> lock(dataMutex);
                GET_CONFIG_FILE_LOCATION
                if (document.SaveFile(configFileLocation.c_str()) == tinyxml2::XML_SUCCESS) {
                    printf("Saved config file %s.\n", configFileLocation.c_str());
                } else {
                    fprintf(stderr, "Failed to save config file to %s.\n", configFileLocation.c_str());
                }
            });
        }
    }

    simplenetwork::MutexPtr<tinyxml2::XMLDocument> RascConfig::getDocument(void) {
        dataMutex.lock();
        return simplenetwork::MutexPtr<tinyxml2::XMLDocument>(&dataMutex, &document);
    }

    tinyxml2::XMLElement * RascConfig::getOrCreateBase(tinyxml2::XMLDocument * document, const char * name) {
        // Find the root element, find our base element within it.
        tinyxml2::XMLElement * root = document->FirstChildElement(ROOT_NAME),
                             * elem = root->FirstChildElement(name);
        // If our base element does not exist, create it and add it to the root element.
        if (elem == NULL) {
            elem = document->NewElement(name);
            root->InsertEndChild(elem);
        }
        // Return our base element
        return elem;
    }

    tinyxml2::XMLElement * RascConfig::getOrInitBase(tinyxml2::XMLDocument * document, const char * name, void (*initElement)(tinyxml2::XMLDocument * document, tinyxml2::XMLElement * element)) {
        // Find the root element, find our base element within it.
        tinyxml2::XMLElement * root = document->FirstChildElement(ROOT_NAME),
                             * elem = root->FirstChildElement(name);
        // If our base element does not exist, create it, initialise it, then add it to the root element.
        if (elem == NULL) {
            elem = document->NewElement(name);
            root->InsertEndChild(elem);
            initElement(document, elem);
        }
        // Return our base element
        return elem;
    }
}

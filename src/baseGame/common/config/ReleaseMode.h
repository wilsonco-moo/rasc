/*
 * ReleaseMode.h
 *
 * This very simple header file provides macros for identifying whether
 * we are being compiled in development mode or release mode.
 *
 * We are in release mode whenever PACKAGE_PROJECT_NAME is defined by
 * the build script.
 *
 * If RASC_RELEASE_MODE is defined, we are being compiled using release mode,
 *                      so development features should be disabled.
 * If RASC_DEVELOPMENT_MODE is defined, we are being compiled in development
 *                          mode, so development features should be available.
 *
 * Note that RASC_RELEASE_MODE and RASC_DEVELOPMENT_MODE will never be defined
 * at the same time.
 *
 *  Created on: 8 Dec 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_CONFIG_RELEASEMODE_H_
#define BASEGAME_COMMON_CONFIG_RELEASEMODE_H_

#ifdef PACKAGE_PROJECT_NAME
    #define RASC_RELEASE_MODE
#else
    #define RASC_DEVELOPMENT_MODE
#endif

#endif

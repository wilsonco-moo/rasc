/*
 * ArmySelectController.cpp
 *
 *  Created on: 16 Sep 2018
 *      Author: wilson
 */

#include "ArmySelectController.h"

#include "mapElement/types/Province.h"
#include "properties/trait/Traits.h"
#include "properties/Properties.h"
#include "GameMap.h"

// If in testing mode, with the army select control test enabled, with debug mode
// not already enabled, enable debug mode.
#ifndef RASC_ARMY_SELECT_CONTROLLER_DEBUG
    #include <test/TestControl.h>
    #ifdef TESTING_MODE_ENABLED
        #if TEST_ENABLED_armySelectControl == 1
            #define RASC_ARMY_SELECT_CONTROLLER_DEBUG
        #endif
    #endif
#endif

#ifdef RASC_ARMY_SELECT_CONTROLLER_DEBUG
    #include <iostream>
#endif

/**
 *
 * THE DATA STRUCTURES IN USE:
 *
 * > std::unordered_set<uint64_t> selectedArmies;                        A set of all the armies that are selected.
 * > std::vector<std::unordered_set<uint64_t>> selectedArmiesByProvince; Stores the selected armies of each province.
 * > std::unordered_map<uint64_t, uint32_t> armyIdToProvinceId;          Stores the province id for each selected army.
 *
 */

// ---------------------------------------- Utility macros ------------------------------------------------

/**
 * This macro unselects an army, by removing it from all appropriate data structures.
 * An iterator from within selectedArmies is required, to define which army to unselect.
 * This macro assumes it is already selected. If the army is NOT already selected, undefined behaviour will result.
 */
#define UNSELECT_ITER(selectedArmiesIterator) {                                                                 \
    uint64_t unselect_armyId;                                                                                   \
                                                                                                                \
    {   /* Get the army id from the iterator, remove the iterator from selectedArmies. */                       \
        std::unordered_set<uint64_t>::iterator unselect_a = (selectedArmiesIterator);                           \
        unselect_armyId = *unselect_a; selectedArmies.erase(unselect_a);                                        \
    }                                                                                                           \
    uint32_t unselect_provinceId;                                                                               \
    {   /* Get province id from selectedArmiesByProvince, relating to the id, remove it from map. */            \
        std::unordered_map<uint64_t, uint32_t>::iterator unselect_i = armyIdToProvinceId.find(unselect_armyId); \
        unselect_provinceId = unselect_i->second; armyIdToProvinceId.erase(unselect_i);                         \
    }                                                                                                           \
    /* Then remove it from the set relating to that province id. */                                             \
    selectedArmiesByProvince[unselect_provinceId].erase(unselect_armyId);                                       \
}

/**
 * This macro selects an army, by adding it to all appropriate data structures.
 * This macro assumes the army is NOT already selected. If the army is already selected, then nothing will happen,
 * except useless operations.
 */
#define SELECT_PROVINCE_ARMY(provinceId, armyId) {                                                              \
    /* Add the army id to the selectedArmies set. */                                                            \
    selectedArmies.insert(armyId);                                                                              \
    /* Put the province id into the army id to province id map. */                                              \
    armyIdToProvinceId[armyId] = (provinceId);                                                                  \
    /* Insert it into the appropriate province's selected armies set. */                                        \
    selectedArmiesByProvince[provinceId].insert(armyId);                                                        \
}

// ----------------------- Constructors, destructors, internal methods -----------------------------------

namespace rasc {
    
    ArmySelectController::ArmySelectController(size_t provinceCount, GameMap * gameMap) :
        splittable(false),
        mergeable(false),
        onSplittableStatusChange(),
        onMergeableStatusChange(),
        gameMap(gameMap),
        selectedArmies(),
        selectedArmiesByProvince(provinceCount),
        armyIdToProvinceId() {
        updateStatus();
    }

    ArmySelectController::ArmySelectController(void) :
        splittable(),
        mergeable(),
        onSplittableStatusChange(),
        onMergeableStatusChange(),
        gameMap(NULL),
        selectedArmies(),
        selectedArmiesByProvince(),
        armyIdToProvinceId() {
    }

    ArmySelectController::~ArmySelectController(void) {
    }
    
    void ArmySelectController::unselect(bool shouldUpdateStatus) {
        // Loop while there exist armies that are selected.
        while(!selectedArmies.empty()) {
            // For each one, unselect it by passing an iterator to the first selected army to the UNSELECT_ITER macro.
            UNSELECT_ITER(selectedArmies.begin())
        }
        if (shouldUpdateStatus) updateStatus(); // Update the status at the end, since we have modified the selection.
    }
    
    void ArmySelectController::setSplittable(bool splittable) {
        if (this->splittable != splittable) {
            this->splittable = splittable;
            onSplittableStatusChange.updateFunctions(splittable);
        }
    }
    
    void ArmySelectController::setMergeable(bool mergeable) {
        if (this->mergeable != mergeable) {
            this->mergeable = mergeable;
            onMergeableStatusChange.updateFunctions(mergeable);
        }
    }
    
    // ------------------------------------------- Utility methods ----------------------------------------------------

    void ArmySelectController::updateStatus(void) {

        // -------------- DO MERGE CALCULATION ---------------------
        std::unordered_set<uint32_t> provincesEncountered;
        // Set mergeable initially to false.
        bool newMergeable = false;
        // Loop through all selected armies
        for (uint64_t armyId : selectedArmies) {
            // Insert their province id into the encountered set
            if (!provincesEncountered.insert(armyIdToProvinceId[armyId]).second) {
                // If we have encountered this province before, i.e: we have two armies selected in the same province...
                // ... then set mergeable to true.
                newMergeable = true;
                // Note, we cannot exit here, as the encountered provinces set is required for the split calculation.
            }
        }
        setMergeable(newMergeable);

        // -------------- DO SPLIT CALCULATION ---------------------
        // Set splittable initially to false
        bool newSplittable = false;
        // Loop through all provinces which contain selected armies.
        for (uint32_t provinceId : provincesEncountered) {
            // Get the province instance. This is safe to do since we are within the synchronised context of gameMap.
            Province & province = gameMap->getProvince(provinceId);
            // Set splittable to true and exit if at least one province is splittable.
            if (province.armyControl.isSplittable()) {
                newSplittable = true;
                goto UPDATE_STATUS_SPLIT_END;
            }
        }
        UPDATE_STATUS_SPLIT_END:;
        setSplittable(newSplittable);

        // ------ do other status calculations -------
        // ...
    }

    void ArmySelectController::switchSelect(uint32_t provinceId, uint64_t armyId) {
        // Get whether we should select the army at the end, i.e: We do this if it it NOT already selected.
        bool selectAtEnd = (selectedArmies.find(armyId) == selectedArmies.end());
        // Unselect all other armies, since this is non-multiple selection. Don't update status here, hence false is supplied.
        unselect(false);
        // If we should update status at the end, then use the selection macro to do so.
        if (selectAtEnd) {
            SELECT_PROVINCE_ARMY(provinceId, armyId)
        }
        // Finally, update status.
        updateStatus();
    }

    void ArmySelectController::multipleSelect(uint32_t provinceId, uint64_t armyId) {
        // Get an iterator to the army to multiple select.
        std::unordered_set<uint64_t>::iterator oldArmyIter = selectedArmies.find(armyId);

        if (oldArmyIter == selectedArmies.end()) {
            // If the army is NOT already selected, run the select macro.
            SELECT_PROVINCE_ARMY(provinceId, armyId)
        } else {
            // If the army IS already selected, run the unselect macro.
            UNSELECT_ITER(oldArmyIter)
        }
        updateStatus(); // Update the status at the end, since we have modified the selection.
    }

    void ArmySelectController::onArmyMove(uint32_t oldProvince, uint32_t newProvince, uint64_t armyId) {
        // Get an iterator to the army, so we can move it to a new province.
        std::unordered_set<uint64_t>::iterator armyIter = selectedArmies.find(armyId);
        // If the army is selected
        if (armyIter != selectedArmies.end()) {
            // Erase it from the old province, add it to the new province.
            selectedArmiesByProvince[oldProvince].erase(armyId);
            selectedArmiesByProvince[newProvince].insert(armyId);
            // Change the army id to province id map accordingly
            armyIdToProvinceId[armyId] = newProvince;
            updateStatus(); // Update the status at the end, since we have modified the selection.
        }
        // If the army is not selected, nothing needs doing, as we are not storing it anyway.
    }

    void ArmySelectController::onArmyDestroy(uint64_t armyId) {
        // Get an iterator to the army, so we can unselect it if it is selected.
        std::unordered_set<uint64_t>::iterator armyIter = selectedArmies.find(armyId);
        // If the army is selected
        if (armyIter != selectedArmies.end()) {
            // Then unselect it
            UNSELECT_ITER(armyIter)
            updateStatus(); // Update the status at the end, since we have modified the selection.
        }
    }

    void ArmySelectController::splitArmies(CheckedUpdate update) {
        if (isSplittable()) {
            // Build a set of provinces which contain armies that are selected.
            std::unordered_set<uint32_t> provincesEncountered;
            for (uint64_t armyId : selectedArmies) {
                provincesEncountered.insert(armyIdToProvinceId[armyId]);
            }

            // Loop through each of the provinces with selected armies
            for (uint32_t provinceId : provincesEncountered) {
                // Get the province instance. This is safe to do since we are within the synchronised context of gameMap.
                Province & province = gameMap->getProvince(provinceId);
                // Call their split armies checked update method. Use client update mode here, since we are bundling multiple
                // checked updates into one message.
                province.armyControl.splitArmies(CheckedUpdate::client(update.msg()), this);
            }
        }
    }

    void ArmySelectController::mergeArmies(CheckedUpdate update) {
        if (isMergeable()) {
            // Build a set or provinces which contain armies that are selected.
            std::unordered_set<uint32_t> provincesEncountered;
            for (uint64_t armyId : selectedArmies) {
                provincesEncountered.insert(armyIdToProvinceId[armyId]);
            }
            // Loop through each of these provinces
            for (uint32_t provinceId : provincesEncountered) {
                // Get the province instance. This is safe to do since we are within the synchronised context of gameMap.
                Province & province = gameMap->getProvince(provinceId);
                // Call their merge armies checked update method. Use client update mode here, since we are bundling multiple
                // checked updates into one message.
                province.armyControl.mergeArmies(CheckedUpdate::client(update.msg()), this);
            }
        }
    }

    void ArmySelectController::absorbArmiesIntoGarrisons(CheckedUpdate update, uint64_t playerId) {
         // Build a set or provinces which contain armies that are selected.
        std::unordered_set<uint32_t> provincesEncountered;
        for (uint64_t armyId : selectedArmies) {
            provincesEncountered.insert(armyIdToProvinceId[armyId]);
        }
        // Loop through each of these provinces, ignoring ones which do not contain forts, or are not owned by us.
        for (uint32_t provinceId : provincesEncountered) {
            Province & province = gameMap->getProvince(provinceId);
            if (province.properties.getPropertySet().hasPropertiesOfType(Properties::Types::fort) && province.getProvinceOwner() == playerId) {
                // For each provinve, absorb selected into the garrison.
                province.armyControl.absorbSelectedArmiesIntoGarrison(CheckedUpdate::client(update.msg()), this);
            }
        }
    }

    void * ArmySelectController::addOnSplittableStatusChange(std::function<void(bool)> func) {
        return onSplittableStatusChange.addFunction(func);
    }
    
    void ArmySelectController::removeOnSplittableStatusChange(void * token) {
        onSplittableStatusChange.removeFunction(token);
    }

    void * ArmySelectController::addOnMergeableStatusChange(std::function<void(bool)> func) {
        return onMergeableStatusChange.addFunction(func);
    }
    
    void ArmySelectController::removeOnMergeableStatusChange(void * token) {
        onMergeableStatusChange.removeFunction(token);
    }
    
    #ifdef RASC_ARMY_SELECT_CONTROLLER_DEBUG
        void ArmySelectController::debugPrintStatus(void) {
            std::cout << "======================\nSelected armies:\n    ";
            for (uint64_t armyId : selectedArmies) {
                std::cout << armyId << ", ";
            }
            std::cout << "\n\nSelected armies by province:\n";
            int i = 0; for (std::unordered_set<uint64_t> & armies : selectedArmiesByProvince) {
                std::cout << "    Province " << (i++) << ": ";
                for (uint64_t armyId : armies) {
                    std::cout << armyId << ", ";
                }
                std::cout << "\n";
            }
            std::cout << "\nArmy ids to province ids:\n    ";
            for (std::pair<uint64_t, uint32_t> pair : armyIdToProvinceId) {
                std::cout << pair.first << "->" << pair.second << ", ";
            }
            std::cout << "\n\nSplitting enabled: " << isSplittable() << ", merging enabled: " << isMergeable() << "\n\n======================\n";
        }
    #endif
}

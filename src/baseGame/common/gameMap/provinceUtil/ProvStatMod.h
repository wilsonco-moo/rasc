/*
 * ProvStatMod.h
 *
 *  Created on: 10 Feb 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROVINCEUTIL_PROVSTATMOD_H_
#define BASEGAME_COMMON_GAMEMAP_PROVINCEUTIL_PROVSTATMOD_H_

#include <unordered_map>

#include "ProvinceStatTypes.h"

namespace rasc {

    /**
     * This very simple class allows a modifier on a stat to be very
     * easily defined.
     * ProvStatMod (province stat modifier) instances should be added up, then multiplied
     * to the base value. This will give the final stat.
     * NOTE: HERE, PERCENTAGES ARE ADDED UP, RATHER THAN MULTIPLIED.
     *       Reimplementing this class to multiply percentages is an easy possible future
     *       change, which would radically change the balance of the stat system.
     * This class has no destructor, so don't make subclasses.
     * 
     * Constructors and some accessors are constexpr, to allow this to be used in a
     * constexpr context. More methods can be (trivially) made constexpr in the future
     * if needed.
     *
     * This is used by traits, and for calculating modified stats within ProvinceProperties.
     */
    class ProvStatMod {
    private:
        provstat_t value,
                   percentage;
    public:
        /**
         * ProvStatMods require a value to add to the base stat value,
         * and a percentage multiplier.
         */
        constexpr ProvStatMod(void);
        constexpr ProvStatMod(provstat_t value, provstat_t percentage);

        /**
         * Adding and subtracting stat modifiers together accumulates the value and percentage.
         */
        ProvStatMod & operator += (const ProvStatMod & other);
        ProvStatMod operator + (const ProvStatMod & other) const;
        ProvStatMod & operator -= (const ProvStatMod & other);
        ProvStatMod operator - (const ProvStatMod & other) const;

        /**
         * Two ProvStatMod instances are equal if both the value and percentage matches.
         */
        bool operator == (const ProvStatMod & other) const;
        bool operator != (const ProvStatMod & other) const;

        /**
         * As the final step, the base value should be multiplied by the base stat value
         * to give the final value.
         */
        provstat_t operator * (provstat_t baseValue);

        // Accessor methods.
        constexpr provstat_t getValue(void) const;
        constexpr provstat_t getPercentage(void) const;

        /**
         * Returns true if both value and percentage are zero (for convenience).
         */
        constexpr bool isZero(void) const;

        /**
         * Evaluates the final value of the ProvStatMod, but without a base stat value.
         * This is equivalent to our operator* with a base value of zero, and is calculated
         * like this: value + (value * percentage) / 100.
         */
        provstat_t evaluateWithoutBaseStat(void) const;

        /**
         * Calculates the CHANGE in evaluation of this stat modifier, if the
         * provided ProvStatMod were to be added to it, assuming we have the
         * specified base stat value. Note that the base stat value is required,
         * because if the provided stat modifier specifies a percentage change,
         * the base stat value must be included in this percentage change.
         */
        provstat_t calculateProspectiveChange(const ProvStatMod & other, provstat_t baseValue) const;
    };

    /**
     * Simple class representing a pair of province stat and province stat modifier.
     * The typical use case for this, is to put in an array, vector etc, for static
     * initialisation of a set of province stat modifiers.
     * 
     * Everything here is constexpr to allow using for constexpr static initialisation.
     * 
     * A ProvStatModSet (see below) can be constructed from any container of
     * ProvStatModPairs - allowing sets to be added, subtracted etc.
     */
    class ProvStatModPair {
    private:
        ProvStatId provinceStat;
        ProvStatMod statModifier;
    public:
        /**
         * Default constructor: Sets stat id to invalid (COUNT) and stat modifier to zero.
         */
        constexpr ProvStatModPair(void);
        
        /**
         * Construct from stat id and modifier.
         */
        constexpr ProvStatModPair(ProvStatId provinceStat, ProvStatMod statModifier);

        /**
         * Gets our province stat id.
         */
        constexpr ProvStatId getProvinceStat(void) const;
        
        /**
         * Gets our province stat modifier.
         */
        constexpr const ProvStatMod & getStatModifier(void) const;
    };
    
    /**
     * This very simple convenience class represents a set of stat modifiers, (at most one defined for each province stat).
     *
     * Each province trait and type must define one of these.
     * This class has no virtual destructor, so don't make subclasses.
     *
     * This is used by traits, and for calculating modified stats within ProvinceProperties.
     *
     * Note that no province stat ID should have a default constructed ProvStatMod
     * associated with it, since it does not need to be there, and will cause
     * equality operator not to work properly. This rule must be adhered to for all
     * ProvStatModSet constructors.
     */
    class ProvStatModSet {
    private:
        std::unordered_map<ProvStatId, ProvStatMod> perStatModifiers;
    public:
        /**
         * Default constructor: Constructs empty set modifying no province stats.
         */
        ProvStatModSet(void);
        
        /**
         * Construct from a map of stat modifiers, helpful so you can initialise using
         * unordered map's initialiser list constructors (etc).
         */
        explicit ProvStatModSet(std::unordered_map<ProvStatId, ProvStatMod> perStatModifiers);
        
        /**
         * Construct a ProvStatModSet from some container of ProvStatModPairs.
         * This can be an array, vector, list, etc, as long as it provides a begin() and
         * end() for iteration.
         * 
         * Helpful as containers of pairs are convenient for static initialisation, this
         * allows building a proper ProvStatModSet.
         * 
         * NOTE: If there are duplicate province stats in the container (i.e: two modifiers
         * for the same province stat), only the last one is used. They're not added together.
         */
        template<typename Container>
        explicit ProvStatModSet(const Container & pairs);
        
        /**
         * Gets our internal map of per stat modifiers.
         */
        inline const std::unordered_map<ProvStatId, ProvStatMod> & getPerStatModifiers(void) const;

        /**
         * Gets the stat modifier (overall, value and percentage), for the
         * specified province stat. If none is defined for that province stat, then
         * zeroes are returned.
         */
        ProvStatMod getProvStatMod(ProvStatId provinceStat) const;
        provstat_t getProvStatModValue(ProvStatId provinceStat) const;
        provstat_t getProvStatModPercentage(ProvStatId provinceStat) const;

        /**
         * Returns true if this province stat modifier set imparts no stat effects.
         */
        inline bool isZero(void) const;

        /**
         * Returns true if we have a stat modifier assigned to the specified province stat.
         * This is simpler than and equivalent to: !modifier.getProvStatMod(provinceStat).isZero().
         */
        bool doesModify(ProvStatId provinceStat) const;

        /**
         * Two ProvStatModSet instances are equal if they have the same perStatModifiers.
         */
        bool operator == (const ProvStatModSet & other) const;
        bool operator != (const ProvStatModSet & other) const;

        /**
         * This allows ProvStatModSet instances to be accumulated and subtracted,
         * helpful for working out the combined effect of multiple properties.
         */
        ProvStatModSet & operator += (const ProvStatModSet & other);
        ProvStatModSet operator + (const ProvStatModSet & other) const;
        ProvStatModSet & operator -= (const ProvStatModSet & other);
        ProvStatModSet operator - (const ProvStatModSet & other) const;
    };
}

#include "ProvStatMod.inl"

#endif

/*
 * ProvStatMod.inl
 *
 *  Created on: 5 Aug 2023
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROVINCEUTIL_PROVSTATMOD_INL_
#define BASEGAME_COMMON_GAMEMAP_PROVINCEUTIL_PROVSTATMOD_INL_

#include "ProvStatMod.h"

namespace rasc {
    
    // ---------------- Stat modifier class -----------------------
    
    constexpr ProvStatMod::ProvStatMod(void) :
        value(0),
        percentage(0) {
    }
    
    constexpr ProvStatMod::ProvStatMod(provstat_t value, provstat_t percentage) :
        value(value),
        percentage(percentage) {
    }
    
    constexpr provstat_t ProvStatMod::getValue(void) const {
        return value;
    }
    
    constexpr provstat_t ProvStatMod::getPercentage(void) const {
        return percentage;
    }
    
    constexpr bool ProvStatMod::isZero(void) const {
         return value == 0 && percentage == 0;
    }
    
    // ---------------- Stat modifier pair class ------------------
    
    constexpr ProvStatModPair::ProvStatModPair(void) :
        provinceStat(ProvStatIds::COUNT),
        statModifier() {
    }

    constexpr ProvStatModPair::ProvStatModPair(ProvStatId provinceStat, ProvStatMod statModifier) :
        provinceStat(provinceStat),
        statModifier(statModifier) {
    }

    constexpr ProvStatId ProvStatModPair::getProvinceStat(void) const {
        return provinceStat;
    }

    constexpr const ProvStatMod & ProvStatModPair::getStatModifier(void) const {
        return statModifier;
    }
    
    // ------------- Province stat modifier set class -------------
    
    template<typename Container>
    ProvStatModSet::ProvStatModSet(const Container & pairs) :
        perStatModifiers() {
        for (const ProvStatModPair & pair : pairs) {
            perStatModifiers.emplace(pair.getProvinceStat(), pair.getStatModifier());
        }
    }
    
    const std::unordered_map<ProvStatId, ProvStatMod> & ProvStatModSet::getPerStatModifiers(void) const {
        return perStatModifiers;
    }
    
    bool ProvStatModSet::isZero(void) const {
        return perStatModifiers.empty();
    }
}

#endif

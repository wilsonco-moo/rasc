/*
 * ProvinceArmyController.cpp
 *
 *  Created on: 12 Oct 2019
 *      Author: wilson
 */

 #include "ProvinceArmyController.h"

#include <algorithm>
#include <iterator>
#include <iostream>

#include "battle/ProvinceBattleController.h"
#include "../../stats/UnifiedStatSystem.h"
#include "../mapElement/types/Province.h"
#include "../../playerData/PlayerData.h"
#include "../../stats/types/AreasStat.h"
#include "../properties/trait/Traits.h"
#include "../../../server/ServerBox.h"
#include "../properties/Properties.h"
#include "../ArmySelectController.h"
#include "../../../server/Player.h"
#include "../GameMap.h"

namespace rasc {
    
    // This defines how many army moves are advanced, each time we move an army.
    // See Army::MAX_ARMY_MOVES.
    #define MOVES_TO_MOVE 1

    ProvinceArmyController::ProvinceArmyController(Province & province, ArmySelectController * armySelectController) :
        RascUpdatable(),
        province(province),
        armySelectController(armySelectController),
        armiesList(),
        armiesById(),
        armiesByPlayerId() {
    }

    ProvinceArmyController::~ProvinceArmyController(void) {
    }

    // ---------------------------------------- Utility ------------------------------------

    std::list<Army>::const_iterator ProvinceArmyController::findArmyExternal(uint64_t id) const {
        std::unordered_map<uint64_t, std::list<Army>::iterator>::const_iterator iter = armiesById.find(id);
        if (iter == armiesById.cend()) {
            return armiesList.cend();
        } else {
            return iter->second;
        }
    }

    bool ProvinceArmyController::isSplittable(void) const {
        // Get our selected armies
        const std::unordered_set<uint64_t> * selectedArmies = &armySelectController->getProvinceSelectedArmies(province.getId());
        // Loop through each of our armies
        for (const Army & army : getArmies()) {
            // If this army is selected...
            if (selectedArmies->find(army.getArmyId()) != selectedArmies->end()) {
                // ... and it has two or more units ...
                if (army.getUnitCount() >= 2) {
                    // ... then return true.
                    return true;
                }
            }
        }
        // If we have not found any armies that we have with two or more units, then return false.
        return false;
    }

    void ProvinceArmyController::placeArmyOrAddUnits(simplenetwork::Message & msg, uint64_t turnCount, PlayerData * player, uint64_t units) {
        uint64_t playerId = player->getId();

        std::list<Army>::const_iterator armyIter = getArmies().begin();
        while(armyIter != getArmies().end()) {
            // Search through all armies
            if (armyIter->getArmyOwner() == playerId) {
                // The first army owned by this player:
                // Add units to it, then return.
                addUnits(MiscUpdate::server(msg, getRascUpdatableRoot()), armyIter->getArmyId(), (int64_t)units, turnCount);
                goto placeArmyOrAddUnits_onSuccess;
            }
            ++armyIter;
        }

        {
            // If we did not find an appropriate army, then create a new one.
            Army army(player, units, turnCount, ((ServerBox *)getRascUpdatableRoot())->armyIdAssigner);
            placeArmy(MiscUpdate::server(msg, getRascUpdatableRoot()), army);

            // Update the status of the army select controller, as the number of units in armies may have changed.
            armySelectController->updateStatus();
        }

        placeArmyOrAddUnits_onSuccess:
        // Since we have added units, notify the battle system.
        if (units > 0) {
            province.battleControl->onMoveArmyIntoProvince(msg, playerId, units);
        }
    }


    std::list<Army>::const_iterator ProvinceArmyController::getArmyOwnedByPlayer(uint64_t playerId) const {
        std::unordered_multimap<uint64_t, std::list<Army>::iterator>::const_iterator iter = armiesByPlayerId.find(playerId);
        if (iter == armiesByPlayerId.cend()) {
            return armiesList.cend();
        } else {
            return iter->second;
        }
    }

    uint64_t ProvinceArmyController::countUnits(void) const {
        uint64_t total = 0;
        for (const Army & army : armiesList) {
            total += army.getUnitCount();
        }
        return total;
    }
    uint64_t ProvinceArmyController::countUnitsOwnedBy(uint64_t playerId) const {
        uint64_t total = 0;
        for (const Army & army : armiesList) {
            if (army.getArmyOwner() == playerId) {
                total += army.getUnitCount();
            }
        }
        return total;
    }
    uint64_t ProvinceArmyController::countMoveableUnitsOwnedBy(uint64_t playerId, uint64_t turnCount) const {
        uint64_t total = 0;
        for (const Army & army : armiesList) {
            if (army.getArmyOwner() == playerId && army.getTurnLastMoved() < turnCount) {
                total += army.getUnitCount();
            }
        }
        return total;
    }
    uint64_t ProvinceArmyController::countUnitsNotOwnedBy(uint64_t playerId) const {
        uint64_t total = 0;
        for (const Army & army : armiesList) {
            if (army.getArmyOwner() != playerId) {
                total += army.getUnitCount();
            }
        }
        return total;
    }
    uint64_t ProvinceArmyController::countUnitsFriendly(uint64_t playerId, void * context, bool (*canAttack)(void *, uint64_t, uint64_t)) const {
        uint64_t total = 0;
        for (const Army & army : armiesList) {
            if (!canAttack(context, playerId, army.getArmyOwner())) {
                total += army.getUnitCount();
            }
        }
        return total;
    }
    uint64_t ProvinceArmyController::countUnitsEnemy(uint64_t playerId, void * context, bool (*canAttack)(void *, uint64_t, uint64_t)) const {
        uint64_t total = 0;
        for (const Army & army : armiesList) {
            if (canAttack(context, playerId, army.getArmyOwner())) {
                total += army.getUnitCount();
            }
        }
        return total;
    }
    void ProvinceArmyController::countUnitsFriendlyEnemy(uint64_t playerId, uint64_t * friendlyCountOutput, uint64_t * enemyCountOutput, void * context, bool (*canAttack)(void *, uint64_t, uint64_t)) const {
        uint64_t friendlyCount = 0,
                 enemyCount = 0;
        for (const Army & army : armiesList) {
            if (canAttack(context, playerId, army.getArmyOwner())) {
                enemyCount += army.getUnitCount();
            } else {
                friendlyCount += army.getUnitCount();
            }
        }
        *friendlyCountOutput = friendlyCount;
        *enemyCountOutput = enemyCount;
    }

    // ------------------------------------- Internal --------------------------------------

    std::list<Army>::iterator ProvinceArmyController::findArmy(uint64_t id) {
        std::unordered_map<uint64_t, std::list<Army>::iterator>::iterator iter = armiesById.find(id);
        if (iter == armiesById.end()) {
            return armiesList.end();
        } else {
            return iter->second;
        }
    }

    void ProvinceArmyController::internalAddArmy(bool notifyStatSystem, Army army) {
        if (armiesById.find(army.getArmyId()) != armiesById.end()) {
            std::cerr << "WARNING: ProvinceArmyController: Two armies with the same ID added to same province.\n";
        }
        armiesList.push_back(army);
        std::list<Army>::iterator iter = --armiesList.end();
        armiesById[army.getArmyId()] = iter;
        armiesByPlayerId.emplace(army.getArmyOwner(), iter);

        // Notify the stat system since the number of units in this province changed.
        if (notifyStatSystem) {
            province.getGameMap().getCommonBox().statSystem->onChangeUnitsInProvince(province, army.getArmyOwner(), army.getUnitCount());
        }
    }

    void ProvinceArmyController::internalRemoveArmy(bool notifyStatSystem, uint64_t armyId) {
        std::unordered_map<uint64_t, std::list<Army>::iterator>::iterator iter = armiesById.find(armyId);
        if (iter != armiesById.end()) {
            std::list<Army>::iterator listIter = iter->second;

            // Notify the army select controller that an army has disappeared.
            armySelectController->onArmyDestroy(armyId);

            // Notify the stat system since the number of units in this province changed.
            if (notifyStatSystem) {
                province.getGameMap().getCommonBox().statSystem->onChangeUnitsInProvince(province, listIter->getArmyOwner(), -listIter->getUnitCount());
            }

            // Iterate through armies owned by that player, when an army with a matching id
            // is found erase it.
            auto range = armiesByPlayerId.equal_range(listIter->getArmyOwner());
            while(range.first != range.second) {
                if (range.first->second->getArmyId() == armyId) {
                    armiesByPlayerId.erase(range.first);
                    break;
                }
                ++range.first;
            }

            armiesList.erase(listIter);
            armiesById.erase(iter);
        }
    }

    void ProvinceArmyController::internalAddUnits(bool notifyStatSystem, uint64_t armyId, int64_t unitsToAdd, bool updateTurn, uint64_t turnCount) {
        std::list<Army>::iterator army = findArmy(armyId);
        if (army != getArmies().end() && unitsToAdd != 0) {

            // Complain if subtracting that amount of units made the army size zero or smaller.
            if (((int64_t)army->getUnitCount()) + unitsToAdd <= 0) {
                std::cerr << "WARNING: ProvinceArmyController: Subtracting units made an army have a size of zero or less.\n";
            }

            if (updateTurn) {
                army->addUnits(unitsToAdd, turnCount); // If we are supposed to update the turn count
            } else {
                army->addUnitsWithoutTurn(unitsToAdd); // If we are NOT supposed to update the turn count
            }

            // Notify the stat system since the number of units in this province changed.
            if (notifyStatSystem) {
                province.getGameMap().getCommonBox().statSystem->onChangeUnitsInProvince(province, army->getArmyOwner(), unitsToAdd);
            }

            // Update the status of the army select controller, as the number of units in an army has changed.
            armySelectController->updateStatus();
        }
    }

    // ---------------------------------- INITIAL DATA -------------------------------------

    void ProvinceArmyController::onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) {
        writeUpdateHeader(msg);
        // Write the number of armies in this province.
        msg.write32((uint32_t)getArmies().size());
        // Loop through all armies
        for (const Army & army : getArmies()) {
            // Write them to the message
            army.writeTo(msg);
        }
    }

    bool ProvinceArmyController::onReceiveInitialData(simplenetwork::Message & msg) {
        // Read the number of armies in this province.
        uint32_t armyCount = msg.read32();
        // Add all armies to our list
        for (uint32_t i = 0; i < armyCount; i++) {
            // Complain and give up if the message is too short.
            if (msg.finishedReading()) {
                std::cerr << "WARNING: ProvinceArmyController::onReceiveInitialData: Savefile too short.\n";
                return false;
            }

            // Create an army from the message, and add it, ensuring order is kept.
            // Don't notify the stat system, since we are loading initial data.
            internalAddArmy(false, Army(msg));
        }
        return true;
    }

    // ------------------------------------ Misc update -----------------------------------

    bool ProvinceArmyController::onReceiveMiscUpdate(simplenetwork::Message & msg) {
        MiscUpdateType type = readMiscUpdateType(msg);
        switch(type) {
        case MiscUpdateTypes::placeArmy:
            onPlaceArmy(msg);
            break;
        case MiscUpdateTypes::removeArmy:
            onRemoveArmy(msg);
            break;
        case MiscUpdateTypes::addUnits:
            onAddUnits(msg);
            break;
        case MiscUpdateTypes::moveArmy:
            onMoveArmy(msg);
            break;
        case MiscUpdateTypes::changeArmyColour:
            onChangeArmyColour(msg);
            break;
        default:
            std::cerr << "WARNING: Unknown misc update type for ProvinceArmyController: ID: " << (unsigned int)type << ".\n";
            return false;
        }
        return true;
    }


    void ProvinceArmyController::placeArmy(MiscUpdate update, Army & army) {
        writeUpdateHeader(update.msg());
        writeMiscUpdateType(update.msg(), MiscUpdateTypes::placeArmy);
        army.writeTo(update.msg());
    }
    void ProvinceArmyController::onPlaceArmy(simplenetwork::Message & msg) {
        // Notify the stat system, since this is a change which happens during the game.
        internalAddArmy(true, Army(msg));
    }


    void ProvinceArmyController::removeArmy(MiscUpdate update, uint64_t armyId) {
        writeUpdateHeader(update.msg());
        writeMiscUpdateType(update.msg(), MiscUpdateTypes::removeArmy);
        update.msg().write64(armyId);
    }
    void ProvinceArmyController::onRemoveArmy(simplenetwork::Message & msg) {
        // Notify the stat system, since this is a change which happens during the game.
        internalRemoveArmy(true, msg.read64());
    }


    void ProvinceArmyController::addUnits(MiscUpdate update, uint64_t armyId, int64_t units, uint64_t turnCount) {
        writeUpdateHeader(update.msg());
        writeMiscUpdateType(update.msg(), MiscUpdateTypes::addUnits);
        update.msg().write64(armyId);
        update.msg().writeS64(units);
        update.msg().writeBool(true); // Write true, meaning YES, WE SHOULD UPDATE THE TURN COUNT.
        update.msg().write64(turnCount);
    }
    void ProvinceArmyController::onAddUnits(simplenetwork::Message & msg) {
        uint64_t armyId = msg.read64(),
                 unitsToAdd = msg.readS64();
        bool updateTurn = msg.readBool();
        // Notify the stat system, since this is a change which happens during the game.
        internalAddUnits(true, armyId, unitsToAdd, updateTurn, updateTurn ? msg.read64() : 0);
    }


    void ProvinceArmyController::addUnitsNoTurn(MiscUpdate update, uint64_t armyId, int64_t units) {
        writeUpdateHeader(update.msg());
        writeMiscUpdateType(update.msg(), MiscUpdateTypes::addUnits);
        update.msg().write64(armyId);
        update.msg().writeS64(units);
        update.msg().writeBool(false); // Write a false, meaning NO, WE SHOULD NOT UPDATE THE TURN COUNT.
    }


    void ProvinceArmyController::moveArmy(MiscUpdate update, uint32_t otherProvinceId, uint64_t armyId) {
        // First write and apply a move army misc update.
        writeUpdateHeader(update.msg());
        writeMiscUpdateType(update.msg(), MiscUpdateTypes::moveArmy);
        update.msg().write32(otherProvinceId);
        update.msg().write64(armyId);
        getRascUpdatableRoot()->receiveMiscUpdate(update.msg());

        // Next notify the battle controller, since a new unit has moved into this province,
        // if it actually exists, (which may not be the case since the army may have been killed
        // since a human player requested it to move).
        std::list<Army>::iterator armyIter = findArmy(armyId);
        if (armyIter != getArmies().end()) {
            province.getGameMap().getProvince(otherProvinceId).battleControl->onMoveArmyOutOfProvince(update.msg());
            province.battleControl->onMoveArmyIntoProvince(update.msg(), armyIter->getArmyOwner(), armyIter->getUnitCount());
        }
    }
    void ProvinceArmyController::onMoveArmy(simplenetwork::Message & msg) {
        // Read all message data BEFORE doing any checks.
        GameMap & map = province.getGameMap();
        uint32_t provinceId = msg.read32();
        uint64_t armyId     = msg.read64();

        // First make sure the province exists, then get it's army control and army.
        if (map.provinceExists(provinceId)) {
            Province & otherProvince = map.getProvince(provinceId);
            ProvinceArmyController & otherArmyControl = otherProvince.armyControl;
            std::list<Army>::iterator armyIter = otherArmyControl.findArmy(armyId);

            // Now make sure the army actually exists.
            if (armyIter != otherArmyControl.getArmies().end()) {
                // Run the onMove method of the army, to update it's turn last moved.
                armyIter->onMove(map.getCommonBox().turnCount, MOVES_TO_MOVE);
                // Add it to our province.
                // Notify the stat system, since this is a change which happens during the game.
                internalAddArmy(true, *armyIter);
                // Erase it from it's old province
                otherArmyControl.internalRemoveArmy(true, armyId);
                // Notify the army select controller that the army has moved, from the other province to this province.
                armySelectController->onArmyMove(otherProvince.getId(), province.getId(), armyId);
            }
        }
    }


    void ProvinceArmyController::mergeAllPlayerArmies(MiscUpdate update) {
        // First check if a merge is needed. If a merge isn't needed, just do nothing.
        {
            bool requireMerge = false;
            uint64_t lastPlayerId = PlayerData::NO_PLAYER;
            for (const std::pair<const uint64_t, std::list<Army>::iterator> & pair : armiesByPlayerId) {
                if (pair.first == lastPlayerId) {
                    requireMerge = true;
                    break;
                }
                lastPlayerId = pair.first;
            }
            if (!requireMerge) return;
        }

        // Next, we assume that a merge *is* needed.
        // Build a vector of army ids per player ID. The vector is IN THE SAME ORDER
        // AS ARMIES ARE IN THE PROVINCE.
        std::unordered_map<uint64_t, std::vector<uint64_t>> armyIdsPerPlayer;
        for (const Army & army : armiesList) {
            armyIdsPerPlayer[army.getArmyOwner()].push_back(army.getArmyId());
        }

        // Go through each list of armies per player.
        for (const std::pair<const uint64_t, std::vector<uint64_t>> & pair : armyIdsPerPlayer) {
            // Ignore players with only one army: They don't need merging.
            if (pair.second.size() < 2) continue;

            // Find the number of units in all but the left most army. Remove them and note
            // the maximum turn last moved, (to avoid the unit merging move exploit).
            int64_t unitsToMergeInto = 0;
            uint64_t turnLastMoved = 0;
            for (size_t pos = 1; pos < pair.second.size(); pos++) {
                Army & army = *armiesById[pair.second[pos]];
                unitsToMergeInto += army.getUnitCount();
                turnLastMoved = std::max(turnLastMoved, army.getTurnLastMoved());
                removeArmy(MiscUpdate::server(update.msg(), getRascUpdatableRoot()), army.getArmyId());
            }
            // Add units to the left most army, using the correct turn last moved.
            addUnits(MiscUpdate::server(update.msg(), getRascUpdatableRoot()), pair.second[0], unitsToMergeInto, turnLastMoved);
        }
    }

    void ProvinceArmyController::changeArmyColour(MiscUpdate update, uint64_t armyId, uint32_t newColour) {
        writeUpdateHeader(update.msg());
        writeMiscUpdateType(update.msg(), MiscUpdateTypes::changeArmyColour);
        update.msg().write64(armyId);
        update.msg().write32(newColour);
    }
    void ProvinceArmyController::onChangeArmyColour(simplenetwork::Message & msg) {
        uint64_t armyId = msg.read64();
        uint32_t newColour = msg.read32();
        std::list<Army>::iterator army = findArmy(armyId);
        if (army != getArmies().end()) {
            army->changeColour(newColour);
        }
    }

    // ----------------------------------- Checked update ---------------------------------

    bool ProvinceArmyController::onReceiveCheckedUpdate(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) {
        CheckedUpdateType type = readCheckedUpdateType(checkedUpdate);
        switch(type) {
        case CheckedUpdateTypes::splitArmies:
            onSplitArmies(checkedUpdate, miscUpdate, contextPlayer);
            break;
        case CheckedUpdateTypes::mergeArmies:
            onMergeArmies(checkedUpdate, miscUpdate, contextPlayer);
            break;
        case CheckedUpdateTypes::requestMoveArmy:
            onRequestMoveArmy(checkedUpdate, miscUpdate, contextPlayer);
            break;
        case CheckedUpdateTypes::absorbIntoGarrison:
            onAbsorbArmiesIntoGarrison(checkedUpdate, miscUpdate, contextPlayer);
            break;
        case CheckedUpdateTypes::requestPlaceUnits:
            onRequestPlaceUnits(checkedUpdate, miscUpdate, contextPlayer);
            break;
        default:
            std::cerr << "WARNING: Unknown checked update type for ProvinceArmyController: ID: " << (unsigned int)type << ".\n";
            return false;
        }
        return true;
    }


    void ProvinceArmyController::splitArmies(CheckedUpdate update, ArmySelectController * armySelectController) {
        writeUpdateHeader(update.msg());
        writeCheckedUpdateType(update.msg(), CheckedUpdateTypes::splitArmies);
        uint32_t splitCount = 0;
        const std::unordered_set<uint64_t> * selectedArmies = &armySelectController->getProvinceSelectedArmies(province.getId());
        // ------ find the number of armies we can split -----
        // Loop through the armies, and count how many are selected, and can be split.
        for (const Army & army : getArmies()) {
            if (army.getUnitCount() >= 2 && selectedArmies->find(army.getArmyId()) != selectedArmies->end()) {
                splitCount++;
            }
        }
        // ----- write general info ------
        // Write the number of armies that need splitting, and the turn count.
        update.msg().write32(splitCount);
        // ----- write the army ids -----
        // Then write the actual ids that need splitting.
        for (const Army & army : getArmies()) {
            if (army.getUnitCount() >= 2 && selectedArmies->find(army.getArmyId()) != selectedArmies->end()) {
                update.msg().write64(army.getArmyId());
            }
        }
    }
    void ProvinceArmyController::onSplitArmies(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) {
        // Read the number of armies to split.
        uint32_t splitCount = checkedUpdate.read32();
        // Loop <splitCount> times:
        for (uint32_t i = 0; i < splitCount; i++) {
            // Complain and give up if the message is too short.
            if (checkedUpdate.finishedReading()) {
                std::cerr << "WARNING: ProvinceArmyController::onSplitArmies: Message too short.\n";
                return;
            }

            uint64_t armyId = checkedUpdate.read64();
            std::list<Army>::iterator armyIter = findArmy(armyId);
            uint64_t unitCount;

            // If army doesnt exist, is too small, not owned by context player or player finished turn, then ignore this army.
            // Note that we still need to read army Ids from the message, even if we don't use any of them.
            if (armyIter == getArmies().end() ||
                (unitCount = armyIter->getUnitCount()) < 2 ||
                armyIter->getArmyOwner() != contextPlayer->getId() ||
                contextPlayer->getHaveFinishedTurn()) continue;

            // The new army we create should be smaller, we should keep selection of the larger army.
            uint64_t newArmySize = unitCount/2;
            // Subtract the size of the new army from the selected army.
            addUnitsNoTurn(MiscUpdate::client(miscUpdate), armyId, -((int64_t)newArmySize));
            // Next create and place a new army, with the correct size. This constructor duplicates all other properties (except id)
            // from the original army, such as the owner and the turn status. This means that splitting does not affect the turn.
            Army newArmy(*armyIter, newArmySize, ((ServerBox *)getRascUpdatableRoot())->armyIdAssigner);
            placeArmy(MiscUpdate::client(miscUpdate), newArmy);
        }
    }


    void ProvinceArmyController::mergeArmies(CheckedUpdate update, ArmySelectController * armySelectController) {
        writeUpdateHeader(update.msg());
        writeCheckedUpdateType(update.msg(), CheckedUpdateTypes::mergeArmies);
        const std::unordered_set<uint64_t> * selectedArmies = &armySelectController->getProvinceSelectedArmies(province.getId());
        // ----- write general info ------
        // The number of armies we can merge is the number of armies selected within our province.
        update.msg().write32((uint32_t)selectedArmies->size());
        // ----- write the army ids -----
        // Then write the actual ids that need merging. These are just the ids that we have selected.
        for (uint64_t armyId : *selectedArmies) {
            update.msg().write64(armyId);
        }
    }
    void ProvinceArmyController::onMergeArmies(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) {
        // Read the number of armies to merge.
        uint32_t mergeCount = checkedUpdate.read32();

        // First we must generate a vector of armies to merge.
        std::vector<Army *> armiesToMerge;
        for (uint32_t i = 0; i < mergeCount; i++) {
            // Complain and give up if the message is too short.
            if (checkedUpdate.finishedReading()) {
                std::cerr << "WARNING: ProvinceArmyController::onMergeArmies: Message too short.\n";
                return;
            }
            std::list<Army>::iterator armyIter = findArmy(checkedUpdate.read64());
            // Only allow armies when they exist and are owned by the context player.
            // Note that we still need to read army Ids from the message, even if we don't use any of them.
            if (armyIter != getArmies().end() && armyIter->getArmyOwner() == contextPlayer->getId()) {
                armiesToMerge.push_back(&*armyIter);
            }
        }

        // If at this point, less than two armies are available to merge, or the context player has finished
        // their turn, then there is nothing for us to do, so do nothing.
        if (armiesToMerge.size() < 2 || contextPlayer->getHaveFinishedTurn()) return;

        // We want to merge armies into the army that most recently moved, to avoid the unit merging
        // move exploit. So find the army that has most recently moved.
        Army * toMergeInto = armiesToMerge[0];
        {
            std::vector<Army *>::iterator armyIter = (armiesToMerge.begin() + 1);
            while(armyIter != armiesToMerge.end()) {
                Army * army = *(armyIter++);
                if (army->getTurnLastMoved() > toMergeInto->getTurnLastMoved()) {
                    toMergeInto = army;
                }
            }
        }

        // Next we work out the total number of units between all armies except our chosen army
        // to merge into.
        uint32_t unitCount = 0;
        for (Army * army : armiesToMerge) {
            if (army != toMergeInto) unitCount += army->getUnitCount();
        }

        // After that, we add this number of units to our chosen army.
        addUnitsNoTurn(MiscUpdate::client(miscUpdate), toMergeInto->getArmyId(), unitCount);

        // Finally, we delete all the armies except the first one.
        for (Army * army : armiesToMerge) {
            if (army != toMergeInto) {
                removeArmy(MiscUpdate::client(miscUpdate), army->getArmyId());
            }
        }
    }




    void ProvinceArmyController::requestMoveArmy(CheckedUpdate update, uint32_t otherProvinceId, uint64_t armyId) {
        writeUpdateHeader(update.msg());
        writeCheckedUpdateType(update.msg(), CheckedUpdateTypes::requestMoveArmy);
        update.msg().write32(otherProvinceId);
        update.msg().write64(armyId);
    }
    void ProvinceArmyController::onRequestMoveArmy(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) {
        GameMap & map = province.getGameMap();
        const std::vector<uint32_t> & adjToUs = province.getAdjacent();

        // Read all message data before doing any checks.
        uint32_t otherProvinceId = checkedUpdate.read32();
        uint64_t armyId = checkedUpdate.read64();

        // If this is request to move to same province, or the source province doesn't exist, ignore the request.
        if (province.getId() == otherProvinceId || !map.provinceExists(otherProvinceId)) return;
        Province & otherProvince = map.getProvince(otherProvinceId);

        // If the army doesnt exist, is not owned by context player, cannot be moved, context player has
        // finished their turn, or it is being moved from a province which is not adjacent to us,
        // then we also should ignore the request.
        std::list<Army>::const_iterator army = otherProvince.armyControl.findArmyExternal(armyId);
        if (army == otherProvince.armyControl.getArmies().cend() ||                             // If army doesnt exist
            army->getArmyOwner() != contextPlayer->getId() ||                                    // If not owned by context player
            !army->canMoveThisTurn(map.getCommonBox().turnCount, MOVES_TO_MOVE) ||               // If army cannot be moved
            contextPlayer->getHaveFinishedTurn() ||                                              // If player has finished their turn
            std::find(adjToUs.begin(), adjToUs.end(), otherProvinceId) == adjToUs.end()) return; // If other (source) province cannot be found in our province's adjacency vector, so is not adjacent to us.

        // All this checked update receive should do is to call the regular moveArmy misc update method.
        // Since this is now being received by the server, this allows the regular moveArmy misc update method
        // only to be called by the server.
        moveArmy(MiscUpdate::client(miscUpdate), otherProvinceId, armyId);
    }





    void ProvinceArmyController::absorbSelectedArmiesIntoGarrison(CheckedUpdate update, ArmySelectController * armySelectController) {
        writeUpdateHeader(update.msg());
        writeCheckedUpdateType(update.msg(), CheckedUpdateTypes::absorbIntoGarrison);
        // Write IDs of all selected armies.
        const std::unordered_set<uint64_t> & selectedArmies = armySelectController->getProvinceSelectedArmies(province.getId());
        update.msg().write32(selectedArmies.size());
        for (uint64_t armyId : selectedArmies) {
            update.msg().write64(armyId);
        }
    }
    void ProvinceArmyController::absorbAllArmiesIntoGarrison(CheckedUpdate update) {
        writeUpdateHeader(update.msg());
        writeCheckedUpdateType(update.msg(), CheckedUpdateTypes::absorbIntoGarrison);
        // Find the range of armies owned by the province owner, and write their IDs.
        auto range = armiesByPlayerId.equal_range(province.getProvinceOwner());
        update.msg().write32(std::distance(range.first, range.second));
        while(range.first != range.second) {
            update.msg().write64(range.first->second->getArmyId());
            ++range.first;
        }
    }
    void ProvinceArmyController::onAbsorbArmiesIntoGarrison(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) {

        // Read the count of the number of armies to absorb, then build a vector of armies which we must absorb.
        // This must be done before doing any checks, to ensure we read the entire message.
        std::vector<std::list<Army>::const_iterator> armiesToAbsorb;
        uint32_t armyCount = checkedUpdate.read32();
        for (uint32_t i = 0; i < armyCount; i++) {
            // Complain and give up if the message is too short.
            if (checkedUpdate.finishedReading()) {
                std::cerr << "WARNING: ProvinceArmyController::onAbsorbArmiesIntoGarrison: Message too short.\n";
                return;
            }
            std::list<Army>::const_iterator army = findArmyExternal(checkedUpdate.read64());

            // Only include armies which exist, and are owned by the context player.
            if (army != getArmies().cend() && army->getArmyOwner() == contextPlayer->getId()) {
                armiesToAbsorb.push_back(army);
            }
        }

        // If no armies to absorb, province not owned by context player, no fort exists,
        // or context player finished their turn, then give up and ignore the request.
        // Note that we must not use the regular Traits::canTraitBeBuilt method here to
        // check whether a garrison can be placed - that will reject this if a battle is
        // ongoing. Absorbing armies is a special case where construction is allowed during
        // a battle.
        if (armiesToAbsorb.empty() ||
            province.getProvinceOwner() != contextPlayer->getId() ||
            !province.properties.getPropertySet().hasPropertiesOfType(Properties::Types::fort) ||
            contextPlayer->getHaveFinishedTurn()) return;

        // Remove all armies, count the units as we go along.
        uint64_t unitCount = 0;
        for (std::list<Army>::const_iterator army : armiesToAbsorb) {
            unitCount += army->getUnitCount();
            removeArmy(MiscUpdate::client(miscUpdate), army->getArmyId());
        }

        // Add the units onto the garrison.
        province.properties.changeTraitCount(
            MiscUpdate::client(miscUpdate), Properties::Types::garrison,
            province.properties.getTraitCount(Properties::Types::garrison) + unitCount
        );
    }

    void ProvinceArmyController::requestPlaceUnits(CheckedUpdate update, int64_t unitsToPlace) {
        writeUpdateHeader(update.msg());
        writeCheckedUpdateType(update.msg(), CheckedUpdateTypes::requestPlaceUnits);
        update.msg().writeS64(unitsToPlace);
    }
    void ProvinceArmyController::onRequestPlaceUnits(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) {
        int64_t requestedUnits = checkedUpdate.readS64();

        // If there is a battle ongoing, or the context player doesn't own the province,
        // or the context player is in debt, or they have finished their turn, then block the request.
        if (province.battleControl->isBattleOngoing() ||
            province.getProvinceOwner() != contextPlayer->getId() ||
            contextPlayer->getTotalCurrency() < 0 ||
            contextPlayer->getHaveFinishedTurn()) return;

        // Work out number of units to actually place. Negative requested units means all possible
        // manpower. Otherwise, clip the number of units we place to the total manpower of the player.
        uint64_t unitsToPlace,
                 maxPlaceable = contextPlayer->getTotalManpower() / AreasStat::MANPOWER_PER_UNIT;
        if (requestedUnits < 0) {
            unitsToPlace = maxPlaceable;
        } else {
            unitsToPlace = std::min(maxPlaceable, (uint64_t)requestedUnits);
        }

        // Don't bother doing anything if at this point we are required to place no units.
        if (unitsToPlace == 0) return;

        // Subtract the manpower cost from the player, using armyBuild as the change reason.
        contextPlayer->addTotalManpower(MiscUpdate::client(miscUpdate), -((int64_t)unitsToPlace) * AreasStat::MANPOWER_PER_UNIT, PlayerStatChangeReasons::armyBuild);
        placeArmyOrAddUnits(miscUpdate, province.getGameMap().getCommonBox().turnCount, contextPlayer, unitsToPlace);
    }
}

/*
 * ProvinceArmyController.h
 *
 *  Created on: 12 Oct 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROVINCEUTIL_PROVINCEARMYCONTROLLER_H_
#define BASEGAME_COMMON_GAMEMAP_PROVINCEUTIL_PROVINCEARMYCONTROLLER_H_

#include <list>

#include <sockets/plus/message/Message.h>

#include "../../update/updateTypes/CheckedUpdate.h"
#include "../../update/updateTypes/MiscUpdate.h"
#include "../../update/RascUpdatable.h"
#include "../units/Army.h"

namespace rasc {

    class Province;
    class ArmySelectController;

    /**
     * ProvinceArmyController is a child RascUpdatable of Province.
     * This is in charge of storing all the armies in each province,
     * and providing methods for moving, splitting, mergin and placing them.
     */
    class ProvinceArmyController : public RascUpdatable {
    private:
        /**
         * Our owner province.
         */
        Province & province;

        /**
         * Our army select controller. We must have a pointer to this, so that we can access this during province
         * updates.
         */
        ArmySelectController * armySelectController;

        /**
         * The armies contained within this province, in order that they were added.
         */
        std::list<Army> armiesList;

        /**
         * A map storing armies by their army ID. This speeds up searching for armies
         * by ID if there are a lot of armies in the province.
         */
        std::unordered_map<uint64_t, std::list<Army>::iterator> armiesById;

        /**
         * This stores for each player ID, the armies owned by that player.
         */
        std::unordered_multimap<uint64_t, std::list<Army>::iterator> armiesByPlayerId;

    public:
        /**
         * The main constructor for ProvinceArmyController.
         * All we require is an owner province.
         */
        ProvinceArmyController(Province & province, ArmySelectController * armySelectController);
        virtual ~ProvinceArmyController(void);

        // --------------------------- Utility ----------------------------------------

        /**
         * This is so that the stat system can access the armies contained within this province.
         */
        inline const std::list<Army> & getArmies(void) const {
            return armiesList;
        }

        /**
         * Allows outside classes access to armies, by their id.
         */
        std::list<Army>::const_iterator findArmyExternal(uint64_t id) const;

        /**
         * Returns true if we have selected at least one army with multiple units.
         */
        bool isSplittable(void) const;

        /**
         * This method:
         *     > Searches to find armies owned by the specified player. If an army is found, the specified
         *       number of units are added to it.
         *     > If such an army is not found, a new army is created with the specified number of units.
         * This is done using a MiscUpdate::server update.
         * This will also notify the battle controller that new units have been added.
         */
        void placeArmyOrAddUnits(simplenetwork::Message & msg, uint64_t turnCount, PlayerData * player, uint64_t units);

        /**
         * Finds an army owned by the specified player, in the province.
         * If there are multiple armies owned by this player, the choice is arbitrary.
         * Otherwise, getArmies().cend() is returned.
         */
        std::list<Army>::const_iterator getArmyOwnedByPlayer(uint64_t playerId) const;

        /**
         * Counts the total number of units, owned by any player, within this province.
         */
        uint64_t countUnits(void) const;

        /**
         * Returns a total of the number of units owned by the specified player,
         * across all armies owned by them within this province, (which may be split up).
         */
        uint64_t countUnitsOwnedBy(uint64_t playerId) const;

        /**
         * Returns a total of the number of units owned by the specified player,
         * across all armies owned by them within this province, (which may be split up),
         * only including armies which are able to move this turn.
         */
        uint64_t countMoveableUnitsOwnedBy(uint64_t playerId, uint64_t turnCount) const;

        /**
         * Returns a total of the number of units NOT owned by the specified player,
         * across all armies within this province.
         */
        uint64_t countUnitsNotOwnedBy(uint64_t playerId) const;

        /**
         * Counts the number of friendly units in the province, (including armies
         * owned by the player themselves), where friendlyness is determined with
         * the provided context and function pointer.
         *
         * The function pointer must return true only if the two
         * provided player IDs are allowed to attack each other,
         * and must always return false for the same two IDs.
         */
        uint64_t countUnitsFriendly(uint64_t playerId, void * context, bool (*canAttack)(void *, uint64_t, uint64_t)) const;

        /**
         * Counts the number of enemy units in the province,
         * where friendlyness is determined with the provided context and function pointer.
         *
         * The function pointer must return true only if the two
         * provided player IDs are allowed to attack each other,
         * and must always return false for the same two IDs.
         */
        uint64_t countUnitsEnemy(uint64_t playerId, void * context, bool (*canAttack)(void *, uint64_t, uint64_t)) const;

        /**
         * Counts both the number of friendly and the number of enemy units in the province.
         * The results are stored in the friendlyCount and enemyCount parameters.
         *
         * The function pointer must return true only if the two
         * provided player IDs are allowed to attack each other,
         * and must always return false for the same two IDs.
         */
        void countUnitsFriendlyEnemy(uint64_t playerId, uint64_t * friendlyCount, uint64_t * enemyCount, void * context, bool (*canAttack)(void *, uint64_t, uint64_t)) const;

        // ------------------------------- Internal ------------------------------------
    private:
        // Finds an iterator to the specified army, within the armies list, by it's id.
        std::list<Army>::iterator findArmy(uint64_t id);

        /**
         * Note, for all of the following messages, there is a parameter which decides
         * whether to notify the unified stat system.
         *  > The unified stat system should be notified if:
         *    - There is a CHANGE which has happened during the game, such as an army
         *      being moved.
         *  > The unified stat system should NOT BE notified if:
         *    - One of these methods is being called during initialisation.
         *    - If these methods are used during loading initial data.
         */

        /**
         * Adds an army to our internal data structures.
         * Also notifies the stat system of the change.
         */
        void internalAddArmy(bool notifyStatSystem, Army army);

        /**
         * Removes an army from our internal data structures by it's army ID.
         * Also notifies the stat system of the change.
         */
        void internalRemoveArmy(bool notifyStatSystem, uint64_t armyId);

        /**
         * Adds units to the specified army.
         * Also notifies the stat system of the change.
         */
        void internalAddUnits(bool notifyStatSystem, uint64_t armyId, int64_t unitsToAdd, bool updateTurn, uint64_t turnCount);

        // -------------------------- Rasc updatable inherited -------------------------

    protected:
        virtual void onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) override;
        virtual bool onReceiveInitialData(simplenetwork::Message & msg) override;
        virtual bool onReceiveMiscUpdate(simplenetwork::Message & msg) override;
        virtual bool onReceiveCheckedUpdate(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) override;

        // ------------------------- Misc update definitions ---------------------------

    private:
        class MiscUpdateTypes {
        public:
            enum {
                placeArmy,
                removeArmy,
                addUnits,
                moveArmy,
                changeArmyColour,

                COUNT_OF_MISC_UPDATE_TYPES
            };
        };
        using MiscUpdateType = uint8_t;
        inline void writeMiscUpdateType(simplenetwork::Message & msg, MiscUpdateType update) const { msg.write8(update); }
        inline MiscUpdateType readMiscUpdateType(simplenetwork::Message & msg) const { return msg.read8(); }

        // ------------------------------ Misc updates --------------------------------

    public:
        /**
         * Places an army in this province.
         */
        void placeArmy(MiscUpdate update, Army & army);

        /**
         * Removes the specified army from this province.
         */
        void removeArmy(MiscUpdate update, uint64_t armyId);

        /**
         * Adds some units to and army in this province.
         * NOTE: units IS SIGNED, this means a negative value can be used to delete units also.
         * This version WILL cause the internal turn count of the unit to be changed, so should be used
         * for regular unit placement.
         * This misc update will NOT notify the battle system, since this is called *by* the
         * battle system. Use placeArmyOrAddUnits for general unit placement instead.
         */
        void addUnits(MiscUpdate update, uint64_t armyId, int64_t units, uint64_t turnCount);

        /**
         * An alternate version of addUnits. This version will not cause the internal turn count
         * of the unit to be changed, so should be used for things like unit splitting and merging.3
         * This misc update will NOT notify the battle system, since this is called *by* the
         * battle system. Use placeArmyOrAddUnits for general unit placement instead.
         */
        void addUnitsNoTurn(MiscUpdate update, uint64_t armyId, int64_t units);

        /**
         * Moves an army from the specified province to this province.
         * This misc update version MUST ONLY BE USED FROM THE SERVER, since this
         * method directly notifies the battle system.
         * As such, MiscUpdate::server or MiscUpdate::serverAuto must be used here.
         * The requestMoveArmy checked update MUST be used by the client.
         */
        void moveArmy(MiscUpdate update, uint32_t otherProvinceId, uint64_t armyId);

        /**
         * Ensures that each player who owns armies in this province has at most one army.
         * This should be used by the battle system.
         * This misc update version MUST ONLY BE USED FROM THE SERVER.
         * As such, MiscUpdate::server or MiscUpdate::serverAuto must be used here.
         */
        void mergeAllPlayerArmies(MiscUpdate update);

        /**
         * Changes the colour of the specified army, without changing the owner.
         * WARNING:
         *  This must only ever be used as part of changing a player colour,
         *  in RascBaseGameServer::changePlayerColour. Applying this misc update
         *  does not actually notify the stat system. That is done after all the
         *  player's provinces and armies have their colours changed, and the misc update
         *  to change the colour in their player data is applied.
         */
        void changeArmyColour(MiscUpdate update, uint64_t armyId, uint32_t newColour);


    private:
        void onPlaceArmy(simplenetwork::Message & msg);
        void onRemoveArmy(simplenetwork::Message & msg);
        void onAddUnits(simplenetwork::Message & msg);
        void onMoveArmy(simplenetwork::Message & msg);
        void onChangeArmyColour(simplenetwork::Message & msg);


        // -------------------------- Checked update definitions ----------------------

        class CheckedUpdateTypes {
        public:
            enum {
                splitArmies,
                mergeArmies,
                requestMoveArmy,
                absorbIntoGarrison,
                requestPlaceUnits,

                COUNT_OF_CHECKED_UPDATE_TYPES
            };
        };
        using CheckedUpdateType = uint8_t;
        inline void writeCheckedUpdateType(simplenetwork::Message & msg, CheckedUpdateType update) const { msg.write8(update); }
        inline CheckedUpdateType readCheckedUpdateType(simplenetwork::Message & msg) const { return msg.read8(); }

        // ------------------------------ Checked updates -----------------------------

    public:
        /**
         * If possible, splits the selected armies in this province.
         */
        void splitArmies(CheckedUpdate update, ArmySelectController * armySelectController);

        /**
         * If possible, merges the selected armies in this province.
         */
        void mergeArmies(CheckedUpdate update, ArmySelectController * armySelectController);

        /**
         * Requests the specified army to be moved, if possible.
         * This should be how the client moves armies.
         */
        void requestMoveArmy(CheckedUpdate update, uint32_t otherProvinceId, uint64_t armyId);

        /**
         * These methods request absorbtion of armies, which are owned by the owner of the province, into
         * the garrison of the province. It is assumed that the province contains a fort.
         *  > The first method filters only those armies which are selected.
         *  > The second method absorbs all armies, owned by the province owner.
         */
        void absorbSelectedArmiesIntoGarrison(CheckedUpdate update, ArmySelectController * armySelectController);
        void absorbAllArmiesIntoGarrison(CheckedUpdate update);

        /**
         * Requests that the specified number of units are placed.
         * If we request placement of more units than possible, our maximum possible units will be placed.
         * The special value -1 can be supplied as unitsToPlace. This represents that we
         * should place all possible units, (our entire manpower reserves).
         */
        void requestPlaceUnits(CheckedUpdate update, int64_t unitsToPlace);

    private:
        void onSplitArmies(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer);
        void onMergeArmies(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer);
        void onRequestMoveArmy(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer);
        void onAbsorbArmiesIntoGarrison(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer);
        void onRequestPlaceUnits(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer);
    };

}

#endif

/*
 * ProvStatMod.cpp
 *
 *  Created on: 10 Feb 2020
 *      Author: wilson
 */

#include "ProvStatMod.h"

namespace rasc {

    // ---------------- Stat modifier class -----------------------

    ProvStatMod & ProvStatMod::operator += (const ProvStatMod & other) {
        value += other.value;
        percentage += other.percentage;
        return *this;
    }
    ProvStatMod ProvStatMod::operator + (const ProvStatMod & other) const {
        return ProvStatMod(value + other.value, percentage + other.percentage);
    }
    ProvStatMod & ProvStatMod::operator -= (const ProvStatMod & other) {
        value -= other.value;
        percentage -= other.percentage;
        return *this;
    }
    ProvStatMod ProvStatMod::operator - (const ProvStatMod & other) const {
        return ProvStatMod(value - other.value, percentage - other.percentage);
    }
    provstat_t ProvStatMod::operator * (provstat_t baseValue) {
        baseValue += value;                                 // First add values...
        return baseValue + (baseValue * percentage) / 100;  // ... then add percentages.
    }
    bool ProvStatMod::operator == (const ProvStatMod & other) const {
        return value == other.value && percentage == other.percentage;
    }
    bool ProvStatMod::operator != (const ProvStatMod & other) const {
        return value != other.value || percentage != other.percentage;
    }
    provstat_t ProvStatMod::evaluateWithoutBaseStat(void) const {
        return value + (value * percentage) / 100;
    }
    provstat_t ProvStatMod::calculateProspectiveChange(const ProvStatMod & other, provstat_t baseValue) const {
        baseValue += value;
        provstat_t sumVal  = baseValue  + other.value,
                   sumPerc = percentage + other.percentage;
        return (sumVal+(sumVal*sumPerc)/100) - (baseValue+(baseValue*percentage)/100);
    }

    // ------------- Province stat modifier set class -------------

    ProvStatModSet::ProvStatModSet(void) :
        perStatModifiers() {
    }
    ProvStatModSet::ProvStatModSet(std::unordered_map<ProvStatId, ProvStatMod> perStatModifiers) :
        perStatModifiers(perStatModifiers) {
    }
    ProvStatMod ProvStatModSet::getProvStatMod(ProvStatId provinceStat) const {
        auto iter = perStatModifiers.find(provinceStat);
        if (iter == perStatModifiers.end()) {
            return ProvStatMod();
        } else {
            return iter->second;
        }
    }
    provstat_t ProvStatModSet::getProvStatModValue(ProvStatId provinceStat) const {
        auto iter = perStatModifiers.find(provinceStat);
        if (iter == perStatModifiers.end()) {
            return 0;
        } else {
            return iter->second.getValue();
        }
    }
    provstat_t ProvStatModSet::getProvStatModPercentage(ProvStatId provinceStat) const {
        auto iter = perStatModifiers.find(provinceStat);
        if (iter == perStatModifiers.end()) {
            return 0;
        } else {
            return iter->second.getPercentage();
        }
    }
    bool ProvStatModSet::doesModify(ProvStatId provinceStat) const {
        return perStatModifiers.find(provinceStat) != perStatModifiers.end();
    }
    bool ProvStatModSet::operator == (const ProvStatModSet & other) const {
        return perStatModifiers == other.perStatModifiers;
    }
    bool ProvStatModSet::operator != (const ProvStatModSet & other) const {
        return perStatModifiers != other.perStatModifiers;
    }
    ProvStatModSet & ProvStatModSet::operator += (const ProvStatModSet & other) {
        // Iterate through all of *their* stat modifiers.
        for (const std::pair<const ProvStatId, ProvStatMod> & pair : other.perStatModifiers) {
            auto iter = perStatModifiers.find(pair.first);

            // If we don't have a modifier for that stat ID, copy over the modifier.
            // Assume the modifier is non-zero since it exists.
            if (iter == perStatModifiers.end()) {
                perStatModifiers.insert(pair);

            // Otherwise, accumulate the modifier. If that resulted in us having a zero
            // modifier, erase it as it is unnecessary.
            } else {
                iter->second += pair.second;
                if (iter->second.isZero()) {
                    perStatModifiers.erase(iter);
                }
            }
        }
        return *this;
    }
    ProvStatModSet ProvStatModSet::operator + (const ProvStatModSet & other) const {
        ProvStatModSet newSet = *this;
        newSet += other;
        return newSet;
    }
    ProvStatModSet & ProvStatModSet::operator -= (const ProvStatModSet & other) {
        // Iterate through all of *their* stat modifiers.
        for (const std::pair<const ProvStatId, ProvStatMod> & pair : other.perStatModifiers) {
            auto iter = perStatModifiers.find(pair.first);

            // If we don't have a modifier for that stat ID, copy over the modifier (and negate it).
            // Assume the modifier is non-zero since it exists.
            if (iter == perStatModifiers.end()) {
                perStatModifiers.emplace(pair.first, ProvStatMod(-pair.second.getValue(), -pair.second.getPercentage()));

            // Otherwise, subtract the modifier. If that resulted in us having a zero
            // modifier, erase it as it is unnecessary.
            } else {
                iter->second -= pair.second;
                if (iter->second.isZero()) {
                    perStatModifiers.erase(iter);
                }
            }
        }
        return *this;
    }
    ProvStatModSet ProvStatModSet::operator - (const ProvStatModSet & other) const {
        ProvStatModSet newSet = *this;
        newSet -= other;
        return newSet;
    }
}

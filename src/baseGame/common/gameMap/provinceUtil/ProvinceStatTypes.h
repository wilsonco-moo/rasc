/*
 * ProvinceStatTypes.h
 *
 *  Created on: 26 Sep 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROVINCEUTIL_PROVINCESTATTYPES_H_
#define BASEGAME_COMMON_GAMEMAP_PROVINCEUTIL_PROVINCESTATTYPES_H_

#include <cstdint>
#include <array>

namespace rasc {

    /**
     * Header file defining the main typedef used for all province related stats.
     * This also contains X macros and enums defining the stats in each province,
     * as well as a couple of other related X macros and enums.
     */

    
    /**
     * This typedef must be used for all province stats, stat multipliers, and stat percentages.
     * When reading and writing province stat values, the associated macros must be used.
     * Having this as a typedef allows changing the stat system to use a different type in the future.
     */
    typedef int64_t provstat_t;
    #define PROVINCE_STAT_WRITE(msg, provinceStat) (msg).writeS64(provinceStat)
    #define PROVINCE_STAT_READ(msg) (msg).readS64()

    /**
     * The province stats available, defined as X macros.
     * Format: Stat name (must start with lowercase, and be one word).
     *         Whether this stat is a good or bad stat: (true=good, false=bad).
     *         Base value of stat.
     */
    #define RASC_PROVINCE_STATS             \
        X(manpowerIncome,      true,  1000) \
        X(currencyIncome,      true,  100)  \
        X(landAvailable,       true,  100)  \
        X(manpowerExpenditure, false, 0)    \
        X(currencyExpenditure, false, 0)    \
        X(landUsed,            false, 0)    \
        X(provinceValue,       true,  0)

    /**
     * This enum provides the stat ids for each province stat.
     * Province stat IDs can be accessed like this: ProvStatIds::manpowerIncome etc.
     *
     * The special value COUNT value defines the total number of stats.
     * If the total number of province stats ever exceeds 256, the typedef must be changed.
     *
     * The associated typedef should be used with this enum.
     * The associated macros should be used for reading and writing stat ids
     * to the message.
     *
     */
    class ProvStatIds {
    public:
        #define X(name, isGoodStat, baseProvinceValue) name,
        enum { RASC_PROVINCE_STATS COUNT };
        #undef X
    };
    typedef uint8_t ProvStatId;
    #define PROVINCE_STAT_ID_WRITE(msg, provinceStatId) (msg).write8(provinceStatId)
    #define PROVINCE_STAT_ID_READ(msg) (msg).read8()

    /**
     * For convenience, this provides constexpr values for base values, for each stat.
     * For example: BaseProvinceStats::manpowerIncome.
     */
    class BaseProvinceStats {
    public:
        #define X(name, isGoodStat, baseProvinceValue) \
            static constexpr provstat_t name = baseProvinceValue;
        RASC_PROVINCE_STATS
        #undef X
    };

    /**
     * This type defines an array, large enough to
     * hold all province stats.
     * To access a stat from a StatArr, do statArr[ProvStatIds::currency] or similar.
     *
     * StatArr can be conveniently added, subtracted, multiplied or
     * divided, by including baseGame/common/util/numeric/ElementWiseArrayMaths.h.
     */
    using StatArr = std::array<provstat_t, ProvStatIds::COUNT>;
    
    /**
     * X macros defining the stats which considered when working out the
     * "value-for-money" of a building. This is done by BuildingStat, and calculated
     * using ProvinceProperties::calculateValueForMoney.
     * The "Y-macro" defines how each stat is calculated: Some are composed of
     * income minus expenditure, and others are not.
     */
    #define RASC_BUILDING_VALUED_PROVINCE_STATS                     \
        X(currency, (Y(currencyIncome) - Y(currencyExpenditure)))   \
        X(manpower, (Y(manpowerIncome) - Y(manpowerExpenditure)))   \
        X(land,     (Y(landAvailable)))

    /**
     * Defines IDs for each building valued stat. Note that these are different
     * to the IDs defined ProvStatIds.
     * std::array<float, BuildValuedStatIds::COUNT> should be used to store
     * "value-for-money" of each building valued stat.
     */
    class BuildValuedStatIds {
    public:
        #define X(statName, calculation) statName,
        enum { RASC_BUILDING_VALUED_PROVINCE_STATS COUNT };
        #undef X
    };
}

#endif

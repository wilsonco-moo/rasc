/*
 * ProvinceBattleController.cpp
 *
 *  Created on: 13 Oct 2019
 *      Author: wilson
 */

#include "ProvinceBattleController.h"

#include "../../../stats/UnifiedStatSystem.h"
#include "../../mapElement/types/Province.h"
#include "../../../util/CommonBox.h"
#include "../../GameMap.h"

#include <algorithm>
#include <iostream>

namespace rasc {

    ProvinceBattleController::ProvinceBattleController(Province & province) :
        RascUpdatable(),
        province(province),
        battleOngoing(false),
        attackersVector(),
        defendersVector(),
        phasesExecuted(0),
        lastBattlePhase(BattlePhases::none),
        siegePhaseCount(0),
        mainAttackerColour(0),
        mainAttackerColourRGBA() {
    }

    ProvinceBattleController::~ProvinceBattleController(void) {
    }


    void ProvinceBattleController::onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) {
        writeUpdateHeader(msg);
        msg.writeBool(battleOngoing);
        // Only actually bother to save the attacker and defender lists if the battle is actually happening.
        if (battleOngoing) {
            msg.write64(phasesExecuted);
            msg.write8(lastBattlePhase);
            msg.write32(getAttackerCount());
            msg.write32(getDefenderCount());
            for (uint64_t attackerId : getAttackers()) {
                msg.write64(attackerId);
            }
            for (uint64_t defenderId : getDefenders()) {
                msg.write64(defenderId);
            }
            msg.write8(siegePhaseCount);
            msg.write32(mainAttackerColour);
        }
    }
    bool ProvinceBattleController::onReceiveInitialData(simplenetwork::Message & msg) {
        battleOngoing = msg.readBool();
        clearDefenders(); clearAttackers();
        if (battleOngoing) {
            phasesExecuted = msg.read64();
            lastBattlePhase = msg.read8();
            uint32_t attackerSize = msg.read32(),
                     defenderSize = msg.read32();
            for (uint32_t i = 0; i < attackerSize; i++) {
                // Complain and give up if the message is too short.
                if (msg.finishedReading()) {
                    std::cerr << "WARNING: ProvinceBattleController::onReceiveInitialData: Message too short: attackers.\n";
                    return false;
                }
                addAttacker(msg.read64());
            }
            for (uint32_t i = 0; i < defenderSize; i++) {
                // Complain and give up if the message is too short.
                if (msg.finishedReading()) {
                    std::cerr << "WARNING: ProvinceBattleController::onReceiveInitialData: Message too short: defenders.\n";
                    return false;
                }
                addDefender(msg.read64());
            }
            siegePhaseCount = msg.read8();
            mainAttackerColour = msg.read32();
            mainAttackerColourRGBA = wool::RGBA(mainAttackerColour);
        }
        return true;
    }


    bool ProvinceBattleController::onReceiveMiscUpdate(simplenetwork::Message & msg) {
        MiscUpdateType type = readMiscUpdateType(msg);
        switch(type) {
        case MiscUpdateTypes::startBattle:
            onStartBattle(msg);
            break;
        case MiscUpdateTypes::onBattlePhase:
            onBattlePhase(msg);
            break;
        case MiscUpdateTypes::onEndBattle:
            onEndBattle(msg);
            break;
        case MiscUpdateTypes::addAttacker:
            onAddAttacker(msg);
            break;
        case MiscUpdateTypes::removeAttacker:
            onRemoveAttacker(msg);
            break;
        case MiscUpdateTypes::addDefender:
            onAddDefender(msg);
            break;
        case MiscUpdateTypes::removeDefender:
            onRemoveDefender(msg);
            break;
        case MiscUpdateTypes::updateInvolvedPlayers:
            onUpdateInvolvedPlayers(msg);
            break;
        case MiscUpdateTypes::incrementSiegePhaseCount:
            onIncrementSiegePhaseCount(msg);
            break;
        case MiscUpdateTypes::updateAttackerColour:
            onUpdateAttackerColour(msg);
            break;
        default:
            std::cerr << "WARNING: Unknown misc update type for ProvinceBattleController: ID: " << (unsigned int)type << ".\n";
            return false;
        }
        return true;
    }

    // ----------------------------- Internal -------------------------------


    void ProvinceBattleController::addAttacker(uint64_t playerId) {
        if (isInvolved(playerId)) {
            std::cerr << "WARNING: ProvinceBattleController: Tried to add attacker player twice.\n";
            return;
        }
        attackersVector.push_back(playerId);
    }
    void ProvinceBattleController::addDefender(uint64_t playerId) {
        if (isInvolved(playerId)) {
            std::cerr << "WARNING: ProvinceBattleController: Tried to add defender player twice.\n";
            return;
        }
        defendersVector.push_back(playerId);
    }
    void ProvinceBattleController::removeAttacker(uint64_t playerId) {
        std::vector<uint64_t>::iterator iter = std::find(attackersVector.begin(), attackersVector.end(), playerId);
        if (iter == attackersVector.end()) {
            std::cerr << "WARNING: ProvinceBattleController: Tried to remove a non existent attacker player.\n";
            return;
        }
        // Replace with end and pop back.
        if (iter != attackersVector.end() - 1) {
            *iter = *(attackersVector.end() - 1);
        }
        attackersVector.pop_back();
    }
    void ProvinceBattleController::removeDefender(uint64_t playerId) {
        std::vector<uint64_t>::iterator iter = std::find(defendersVector.begin(), defendersVector.end(), playerId);
        if (iter == defendersVector.end()) {
            std::cerr << "WARNING: ProvinceBattleController: Tried to remove a non existent defender player.\n";
            return;
        }
        // Replace with end and pop back.
        if (iter != defendersVector.end() - 1) {
            *iter = *(defendersVector.end() - 1);
        }
        defendersVector.pop_back();
    }
    void ProvinceBattleController::clearAttackers(void) {
        attackersVector.clear();
    }
    void ProvinceBattleController::clearDefenders(void) {
        defendersVector.clear();
    }
    void ProvinceBattleController::reserveAttackers(size_t count) {
        attackersVector.reserve(count);
    }
    void ProvinceBattleController::reserveDefenders(size_t count) {
        defendersVector.reserve(count);
    }
    bool ProvinceBattleController::isAttacker(uint64_t playerId) const {
        return std::find(attackersVector.begin(), attackersVector.end(), playerId) != attackersVector.end();
    }
    bool ProvinceBattleController::isDefender(uint64_t playerId) const {
        return std::find(defendersVector.begin(), defendersVector.end(), playerId) != defendersVector.end();
    }

    // ---------------------------- Misc updates ----------------------------

    void ProvinceBattleController::onStartBattle(simplenetwork::Message & msg) {
        // Print a warning if a battle is already ongoing.
        if (battleOngoing) {
            std::cerr << "WARNING: ProvinceBattleController::onStartBattle while battle is already ongoing.\n";
        }

        // When a battle starts, we don't need to clear attackers or defenders, we assume
        // this has already happened if a previous battle ended.
        battleOngoing = true;

        // Notify the stat system that a battle just started.
        province.getGameMap().getCommonBox().statSystem->onChangeBattleStatusInProvince(province, true);
    }

    void ProvinceBattleController::onBattlePhase(simplenetwork::Message & msg) {
        phasesExecuted++;
        lastBattlePhase = msg.read8();
    }

    void ProvinceBattleController::onEndBattle(simplenetwork::Message & msg) {
        if (!battleOngoing) {
            std::cerr << "WARNING: ProvinceBattleController::onEndBattle for a battle which is not ongoing.\n";
        }
        battleOngoing = false;
        // When a battle ends, clear all attackers and defenders, and reset other stats.
        phasesExecuted = 0;
        lastBattlePhase = BattlePhases::none;
        siegePhaseCount = 0;
        clearAttackers();
        clearDefenders();

        // Notify the stat system that a battle just ended.
        province.getGameMap().getCommonBox().statSystem->onChangeBattleStatusInProvince(province, false);
    }

    void ProvinceBattleController::onAddAttacker(simplenetwork::Message & msg) {
        addAttacker(msg.read64());
    }
    void ProvinceBattleController::onRemoveAttacker(simplenetwork::Message & msg) {
        removeAttacker(msg.read64());
    }
    void ProvinceBattleController::onAddDefender(simplenetwork::Message & msg) {
        addDefender(msg.read64());
    }
    void ProvinceBattleController::onRemoveDefender(simplenetwork::Message & msg) {
        removeDefender(msg.read64());
    }

    void ProvinceBattleController::onUpdateInvolvedPlayers(simplenetwork::Message & msg) {
        clearAttackers();
        clearDefenders();
        uint32_t attackerSize = msg.read32(),
                 defenderSize = msg.read32();
        for (uint32_t i = 0; i < attackerSize; i++) {
            // Complain and give up if the message is too short.
            if (msg.finishedReading()) {
                std::cerr << "WARNING: ProvinceBattleController::onUpdateInvolvedPlayers: Message too short: attackers.\n";
                return;
            }
            addAttacker(msg.read64());
        }
        for (uint32_t i = 0; i < defenderSize; i++) {
            // Complain and give up if the message is too short.
            if (msg.finishedReading()) {
                std::cerr << "WARNING: ProvinceBattleController::onUpdateInvolvedPlayers: Message too short: defenders.\n";
                return;
            }
            addDefender(msg.read64());
        }
        mainAttackerColour = msg.read32();
        mainAttackerColourRGBA = wool::RGBA(mainAttackerColour);
    }

    void ProvinceBattleController::onIncrementSiegePhaseCount(simplenetwork::Message & msg) {
        siegePhaseCount++;
    }

    void ProvinceBattleController::onUpdateAttackerColour(simplenetwork::Message & msg) {
        mainAttackerColour = msg.read32();
        mainAttackerColourRGBA = wool::RGBA(mainAttackerColour);
    }

    void ProvinceBattleController::onMoveArmyIntoProvince(simplenetwork::Message & msg, uint64_t attackerId, uint64_t unitCount) {
        std::cerr << "WARNING: ProvinceBattleController: Direct army move " << "INTO" << " province not implemented on client side.\n";
    }

    void ProvinceBattleController::onMoveArmyOutOfProvince(simplenetwork::Message & msg) {
        std::cerr << "WARNING: ProvinceBattleController: Direct army move " << "OUT OF" << " province not implemented on client side.\n";
    }
}

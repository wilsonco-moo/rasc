/*
 * ProvinceBattleController.h
 *
 *  Created on: 13 Oct 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROVINCEUTIL_BATTLE_PROVINCEBATTLECONTROLLER_H_
#define BASEGAME_COMMON_GAMEMAP_PROVINCEUTIL_BATTLE_PROVINCEBATTLECONTROLLER_H_

#include <unordered_map>
#include <utility>
#include <vector>

#include <sockets/plus/message/Message.h>
#include <wool/misc/RGBA.h>

#include "../../../update/RascUpdatable.h"

namespace rasc {

    class Province;
    class Army;

    /**
     * This class controls multi-turn battles within this province.
     *
     * There are two versions of this class: ProvinceBattleController and ServerProvinceBattleController.
     * ServerProvinceBattleController is a subclass, which must only be used server-side.
     *
     * The battle system must always comply with the following requirements:
     *  > There is a list of attackers and defenders in the battle.
     *  > Each of the listed players involved in the battle have an army in the province.
     *  > If a player's army is killed during the battle, they stop being part of the battle.
     *  > A player cannot have more than one army in a battle. If they reinforce, units must
     *    automatically be merged.
     */
    class ProvinceBattleController : public RascUpdatable {
    public:
        /**
         * The number of losses an army should take each siege phase.
         * ServerProvinceBattleController must use this for it's siege phases.
         * This is currently constant, and repeated the appropriate number of times,
         * but that may change in the future.
         */
        constexpr static unsigned int SIEGE_PHASE_LOSSES = 1000;

        /**
         * The number of turns that it should take, (i.e: The number of siege
         * phases which must be run), to take a province.
         */
        constexpr static unsigned int TOTAL_SIEGE_PHASES = 4;

        /**
         * The total number of men, across each of the siege phases, who are
         * lost to take a province.
         */
        constexpr static unsigned int SIEGE_OVERALL_LOSSES = (SIEGE_PHASE_LOSSES * TOTAL_SIEGE_PHASES);

        /**
         * Multiply the current siege phase value by this number to get
         * a value between 0 and 1 representing the current siege progress.
         */
        constexpr static GLfloat SIEGE_PHASE_PROGRESS_MULTIPLIER = 1.0f / ((GLfloat)TOTAL_SIEGE_PHASES);

    protected:

        /**
         * The possible battle phases which can take place.
         * The order must always be:
         * strike phase (immediately),  defender's turn,  defense phase,  attacker's turn,  attack phase
         * ... then back to defender's turn, and around in a loop.
         */
        class BattlePhases {
        public:
            enum {
                none, // This is just the default value of lastBattlePhase, and should not be actually used as a phase.
                strike,
                defense,
                attack,
                siege
            };
        };
        using BattlePhase = uint8_t;


        Province & province;

    private:

        // The following variables represent the current battle state.
        // These are private. ServerProvinceBattleController can only
        // modify these with misc updates.

        /**
         * Stores true if a battle is ongoing, false otherwise.
         */
        bool battleOngoing;

        /**
         * The player ids of the attacker and defender players.
         */
        std::vector<uint64_t> attackersVector, defendersVector;

        /**
         * Contains the total number of battle phases executed so far.
         * This is accessible via getPhasesExecuted, is reset by
         * onStartBattle, and is incremented each time onBattlePhase
         * is called.
         */
        uint64_t phasesExecuted;

        /**
         * This should store the most recent battle phase executed.
         * This should be used to work out which battle phase needs doing
         * next.
         * This should be used to work out whether the battle type is currently
         * a siege or not.
         */
        BattlePhase lastBattlePhase;

        // --------------------- Siege stats (add more later) ----------------------

        /**
         * This counts siege phases. Once this reaches a certain value, the province
         * changes ownership. (Simple siege system).
         * This is saved as initial data, and reset on battle start.
         */
        uint8_t siegePhaseCount;

        /**
         * This should store the colour of the main attacker in the province,
         * for the purpose of drawing the siege overlay in the province.
         * This is updated each time we receive updateInvolvedPlayers.
         * The int version of the colour is sent across network and saved, the
         * RGBA version is worked out from it.
         */
        uint32_t mainAttackerColour;
        wool::RGBA mainAttackerColourRGBA;

        // -------------------------------------------------------------------------

    public:
        ProvinceBattleController(Province & province);
        virtual ~ProvinceBattleController(void);

    // -------------------------- Rasc updatable inherited -------------------------

    protected:
        virtual void onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) override;
        virtual bool onReceiveInitialData(simplenetwork::Message & msg) override;
        virtual bool onReceiveMiscUpdate(simplenetwork::Message & msg) override;

    // ------------------------------ Misc updates --------------------------------

        // Note that this base class can only RECEIVE misc updates.
        // Misc updates are GENERATED by ServerProvinceBattleController.

        class MiscUpdateTypes {
        public:
            enum {
                startBattle,
                onBattlePhase,
                onEndBattle,

                addAttacker,
                removeAttacker,
                addDefender,
                removeDefender,

                updateInvolvedPlayers,

                incrementSiegePhaseCount,

                updateAttackerColour,

                COUNT_OF_MISC_UPDATE_TYPES
            };
        };
        using MiscUpdateType = uint8_t;
        inline void writeMiscUpdateType(simplenetwork::Message & msg, MiscUpdateType update) const { msg.write8(update); }
        inline MiscUpdateType readMiscUpdateType(simplenetwork::Message & msg) const { return msg.read8(); }

    private:

        // ----------------- Internal methods --------------------------------

        // Adds and removes attacker and defender player IDs, by adding and removing them from appropriate data
        // structures.
        inline void addAttacker(uint64_t playerId);
        inline void addDefender(uint64_t playerId);
        void removeAttacker(uint64_t playerId);
        void removeDefender(uint64_t playerId);
        void clearAttackers(void);
        void clearDefenders(void);
        void reserveAttackers(size_t count);
        void reserveDefenders(size_t count);

        // ------------------------ Misc update receive methods -------------------------

        /**
         * This is called when the server tells us a battle has started.
         * This clears the lists of attackers and defenders, sets the
         * battle to ongoing, and resets the last phase executed and the count
         * of executed phases.
         */
        void onStartBattle(simplenetwork::Message & msg);

        /**
         * This is called each time the server does a battle phase.
         * This increments our phasesExecuted counter.
         */
        void onBattlePhase(simplenetwork::Message & msg);

        /**
         * This is called when we receive a message saying the battle has
         * ended. This will set battleOngoing to false. This will not
         * clear or reset anything else, since that will be done next
         * time a battle starts.
         */
        void onEndBattle(simplenetwork::Message & msg);

        /**
         * These are all called to respectively add and remove
         * attackers and defenders.
         */
        void onAddAttacker(simplenetwork::Message & msg);
        void onRemoveAttacker(simplenetwork::Message & msg);
        void onAddDefender(simplenetwork::Message & msg);
        void onRemoveDefender(simplenetwork::Message & msg);

        /**
         * This message sets the list of involved players in the battle.
         */
        void onUpdateInvolvedPlayers(simplenetwork::Message & msg);

        /**
         * This should be run each siege phase, it updates the siege phase count.
         */
        void onIncrementSiegePhaseCount(simplenetwork::Message & msg);

        /**
         * This can be called to change the colour of the main attacker, (for siege display).
         * This should be called if there is a change to player colour, of a player who has
         * deployed units in this province.
         */
        void onUpdateAttackerColour(simplenetwork::Message & msg);

    // -------------------------- General methods ---------------------------------

    public:
        /**
         * This must be run each time an army is moved into the province.
         * This should never be run on the client side.
         * On the server side this will cause an automatic battle rebalance,
         * Which may start or end a battle or siege.
         *
         * This will write generated misc updates to the provided message, and automatically apply them.
         *
         * The player id of the player who moved their army into the province is required.
         * The unit count is required so the player does not receive a strike phase on all of their army.
         */
        virtual void onMoveArmyIntoProvince(simplenetwork::Message & msg, uint64_t attackerId, uint64_t unitCount);

        /**
         * This must be called each time an army is moved out of the province.
         * This should never be run on the client side.
         * On the server side this will cause an automatic battle rebalance,
         * Which may start or end a battle or siege.
         *
         * This will write generated misc updates to the provided message, and automatically apply them.
         *
         * A player id is not requires since there is no "Attacker".
         */
        virtual void onMoveArmyOutOfProvince(simplenetwork::Message & msg);


    // ----------------- Accessor methods ---------------------------

        /**
         * Returns true only if there is a currently ongoing battle.
         */
        inline bool isBattleOngoing(void) const {
            return battleOngoing;
        }

        /**
         * Returns the total number of battle phases executed so far.
         * Only returns a useful result if there is a battle ongoing.
         */
        inline uint64_t getPhasesExecuted(void) const {
            return phasesExecuted;
        }

        /**
         * Allows access to the vector of attacker player Ids.
         * Only returns a useful result if there is a battle ongoing.
         */
        inline const std::vector<uint64_t> getAttackers(void) const { return attackersVector; }

        /**
         * Allows access to a count of how many attacking players are in this province.
         * Only returns a useful result if there is a battle ongoing.
         */
        inline size_t getAttackerCount(void) const { return attackersVector.size(); }

        /**
         * Allows access to the vector of defender player Ids.
         * Only returns a useful result if there is a battle ongoing.
         */
        inline const std::vector<uint64_t> getDefenders(void) const { return defendersVector; }

        /**
         * Allows access to how many defending players are in this province.
         * Only returns a useful result if there is a battle ongoing.
         */
        inline size_t getDefenderCount(void) const { return defendersVector.size(); }

        /**
         * Returns true if the specified player is an attacker involved in the battle in this province.
         * Only returns a useful result if there is a battle ongoing.
         */
        bool isAttacker(uint64_t playerId) const;

        /**
         * Returns true if the specified player is an defender involved in the battle in this province.
         * Only returns a useful result if there is a battle ongoing.
         */
        bool isDefender(uint64_t playerId) const;

        /**
         * Returns true if the specified player is involved in the battle, i.e: Is either an attacking
         * player or a defending player.
         * Only returns a useful result if there is a battle ongoing.
         */
        inline bool isInvolved(uint64_t playerId) const {
            return isAttacker(playerId) || isDefender(playerId);
        }

        /**
         * Gets the siege phase count.
         */
        inline uint8_t getSiegePhaseCount(void) const {
            return siegePhaseCount;
        }

        /**
         * This should return a value between zero and 1, which represents the progress
         * of the current siege. This is applicable every time a battle is ongoing.
         */
        inline GLfloat getSiegeProgress(void) const {
            return ((GLfloat)siegePhaseCount) * SIEGE_PHASE_PROGRESS_MULTIPLIER;
        }

        /**
         * Returns the number of units, (minimum), which a player must have to be able
         * to complete the siege, considering its current progress.
         * This is applicable only if there is a battle ongoing, and the garrison has
         * a size of zero.
         */
        inline unsigned int getUnitsLeftToCompleteSiege(void) const {
            return (TOTAL_SIEGE_PHASES - siegePhaseCount) * SIEGE_PHASE_LOSSES;
        }

        /**
         * Allows access to the colour of the main attacker, as a wool::RGBA.
         */
        inline const wool::RGBA & getMainAttackerColourRGBA(void) const {
            return mainAttackerColourRGBA;
        }

        /**
         * Allows access to the colour of the main attacker, as an int.
         */
        inline uint32_t getMainAttackerColour(void) const {
            return mainAttackerColour;
        }
    };
}

#endif

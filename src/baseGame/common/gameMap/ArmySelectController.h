/*
 * ArmySelectController.h
 *
 *  Created on: 16 Sep 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_ARMYSELECTCONTROLLER_H_
#define BASEGAME_COMMON_GAMEMAP_ARMYSELECTCONTROLLER_H_

#include <unordered_set>
#include <unordered_map>
#include <functional>
#include <cstdint>
#include <vector>
#include <list>

#include "../../common/util/templateTypes/TokenFunctionList.h"
#include "../update/updateTypes/CheckedUpdate.h"

namespace simplenetwork {
    class Message;
}

namespace rasc {
    class GameMap;

    /**
     * This class provides a convenient system to store all of the army selection for provinces.
     */
    class ArmySelectController {
    private:
        /**
         * Uncommenting this line forces the debug printing function to be compiled in.
         */
        //#define RASC_ARMY_SELECT_CONTROLLER_DEBUG

        // Stores internally the value of isSplittable(), to avoid recalculating it each time isSplittable() is called.
        bool splittable;
        // Stores internally the value of isMergeable(), to avoid recalculating it each time isMergeable() is called.
        bool mergeable;

        // These allow the UI system to be updated when the splittable or mergeable status changes.        
        TokenFunctionList<void(bool)> onSplittableStatusChange, onMergeableStatusChange;
        
        // The GameMap that we are contained within.
        GameMap * gameMap;
        // A set of all the armies that are selected.
        std::unordered_set<uint64_t> selectedArmies;
        // Stores the selected armies of each province.
        std::vector<std::unordered_set<uint64_t>> selectedArmiesByProvince;
        // Stores the province id for each selected army.
        std::unordered_map<uint64_t, uint32_t> armyIdToProvinceId;

    public:
        /**
         * This is the main constructor. This requires the number of provinces to be defined.
         */
        ArmySelectController(size_t provinceCount, GameMap * gameMap);
        /**
         * The default constructor. This is not useful for anything other than assigning an army select
         * controller outside the initialisation list.
         */
        ArmySelectController(void);
        virtual ~ArmySelectController(void);

    private:
        // An internal version of unselect, which takes a parameter allowing the user to decide
        // whether to automatically call updateStatus().
        void unselect(bool shouldUpdateStatus);
        
        // These methods are used internally to update the values of splittable and mergeable.
        // These methods allow the UI to be updated when these values change.
        void setSplittable(bool splittable);
        void setMergeable(bool mergeable);
        
    public:
        // ------------------------- Accessor methods -------------------------------
        
        /**
         * Returns whether the specified army id is selected.
         */
        inline bool isArmySelected(uint64_t armyId) {
            return selectedArmies.find(armyId) != selectedArmies.end();
        }

        /**
         * Returns true if at least one army is selected.
         */
        inline bool isAnyArmySelected(void) const {
            return !selectedArmies.empty();
        }

        /**
         * Returns the armies selected by the specified province.
         */
        inline const std::unordered_set<uint64_t> & getProvinceSelectedArmies(uint32_t provinceId) {
            return selectedArmiesByProvince[provinceId];
        }
        
        /**
         * Returns whether it is possible to split the current armies. This returns true if we have at least one
         * army with more than one unit contained within it, selected.
         */
        inline bool isSplittable(void) {
            return splittable;
        }

        /**
         * This returns true if we have multiple armies selected in at least one province.
         */
        inline bool isMergeable(void) {
            return mergeable;
        }
        
        // ------------------------------ Utility methods -----------------------------------
        
        /**
         * This should be internally called at the end of each call that modifies
         * the selection. This updates any status variables, such as the mergeable variable.
         *
         * This should also be manually be called if the number of units in an army changes.
         * """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
         */
        void updateStatus(void);

        /**
         * Switches selection to the specified army, for example if the user clicks on it.
         * The selection is removed if it is already selected.
         * All other armies are unselected when this is called.
         */
        void switchSelect(uint32_t provinceId, uint64_t armyId);
        
        /**
         * Multiple select adds the specified army, for example if the user ctrl+clicks on it.
         * The selection is removed if it is already selected.
         * The selection of other armies is unaffected by this call.
         */
        void multipleSelect(uint32_t provinceId, uint64_t armyId);

        /**
         * This unselects all armies. This should be called if the user clicks on something other than an army.
         * (Note: This is an inline method that automatically calls unselect(true)).
         */
        inline void unselect(void) {
            unselect(true);
        }

        /**
         * This should be called when an army is moved from one province to another.
         * Selection is managed accordingly.
         */
        void onArmyMove(uint32_t oldProvince, uint32_t newProvince, uint64_t armyId);
        
        /**
         * This should be called when an army is destroyed. Selection is managed accordingly.
         */
        void onArmyDestroy(uint64_t armyId);

        /**
         * Splits all appropriate armies, by calling the appropriate checked update method, only in the provinces
         * which contain armies that are selected.
         * This must be called from within the GameMap's mutex.
         * The checked update message will be written to the specified message.
         */
        void splitArmies(CheckedUpdate update);

        /**
         * Merges all appropriate armies, by calling the appropriate checked update method, only in the provinces
         * which contain armies that are selected.
         * This must be called from within the GameMap's mutex.
         * The checked update message will be written to the specified message.
         */
        void mergeArmies(CheckedUpdate update);

        /**
         * Absorbs all selected armies into forts, (to become garrisoned units), wherever possible.
         * Units which are not in provinces containing forts are unaffected.
         * The player Id is needed so we know which provinces to absorb into.
         */
        void absorbArmiesIntoGarrisons(CheckedUpdate update, uint64_t playerId);

        /**
         * These should be used by the UI system to update the status of the UI when the status
         * of splittable changes. For example, this could be used to update the active state of the split button.
         * Note: All splittable status change functions must be removed before we're destroyed.
         */
        void * addOnSplittableStatusChange(std::function<void(bool)> func);
        void removeOnSplittableStatusChange(void * token);
        
        /**
         * These should be used by the UI system to update the status of the UI when the status
         * of mergeable changes. For example, this could be used to update the active state of the merge button.
         * Note: All mergeable status change functions must be removed before we're destroyed.
         */
        void * addOnMergeableStatusChange(std::function<void(bool)> func);
        void removeOnMergeableStatusChange(void * token);
        
        /**
         * Prints the status of all data structures, if in debug mode.
         * Note that this is only compiled in if in testing mode, with the army
         * select controller test enabled.
         */
        void debugPrintStatus(void);
    };
}

#endif

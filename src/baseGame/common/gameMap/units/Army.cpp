/*
 * Army.cpp
 *
 *  Created on: 27 Aug 2018
 *      Author: wilson
 */

#include "Army.h"

#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <GL/gl.h>
#include <cstdio>
#include <cmath>
#include <mutex>

#include <wool/font/Font.h>

#include "../../../client/config/MapModeController.h"
#include "../../../client/config/CameraProperties.h"
#include "../../util/numeric/UniqueIdAssigner.h"
#include "../mapElement/types/Province.h"
#include "../../playerData/PlayerData.h"
#include "../mapElement/MapElement.h"

namespace rasc {

    const uint64_t Army::INVALID_ARMY = 0xffffffffffffffff;
    const uint64_t Army::MAX_ARMY_MOVES = 1;

    Army::Army(PlayerData * owner, uint64_t units, uint64_t turnCount, UniqueIdAssigner * uniqueIdAssigner) :
        id(uniqueIdAssigner->newId()),
        owner(owner->getId()),
        ownerColourInt(owner->getColourInt()),
        ownerColour(ownerColourInt),
        units(units),
        turnLastMoved(turnCount),
        movesThisTurn(MAX_ARMY_MOVES),
        unitsString() {
        KShortenedNumber::format(&unitsString[0], units); // We just assigned units initially, so update units string.
    }

    #ifdef RASC_DEVELOPMENT_MODE
        Army::Army(PlayerData * owner, uint64_t units, uint64_t turnCount, uint64_t id) :
            id(id),
            owner(owner->getId()),
            ownerColourInt(owner->getColourInt()),
            ownerColour(ownerColourInt),
            units(units),
            turnLastMoved(turnCount),
            movesThisTurn(MAX_ARMY_MOVES),
            unitsString() {
            KShortenedNumber::format(&unitsString[0], units); // We just assigned units initially, so update units string.
            std::cout << "WARNING: Army: Created army (id: " << id << ") without unique ID assigner. This is ONLY allowed in DEVELOPMENT MODE.\n";
        }
    #endif

    Army::Army(uint64_t playerId, uint32_t playerColour, uint64_t units, uint64_t turnCount, UniqueIdAssigner * uniqueIdAssigner) :
        id(uniqueIdAssigner->newId()),
        owner(playerId),
        ownerColourInt(playerColour),
        ownerColour(wool::RGBA(ownerColourInt)),
        units(units),
        turnLastMoved(turnCount),
        movesThisTurn(MAX_ARMY_MOVES),
        unitsString() {
        KShortenedNumber::format(&unitsString[0], units); // We just assigned units initially, so update units string.
    }

    Army::Army(const Army & otherArmy, uint64_t units, UniqueIdAssigner * uniqueIdAssigner) :
        id(uniqueIdAssigner->newId()),
        owner(otherArmy.owner),
        ownerColourInt(otherArmy.ownerColourInt),
        ownerColour(otherArmy.ownerColour),
        units(units),
        turnLastMoved(otherArmy.turnLastMoved),
        movesThisTurn(otherArmy.movesThisTurn),
        unitsString() {
        KShortenedNumber::format(&unitsString[0], units); // We just assigned units initially, so update units string.
    }

    Army::Army(simplenetwork::Message & msg) :
        id(msg.read64()),
        owner(msg.read64()),
        ownerColourInt(msg.read32()),
        ownerColour(wool::RGBA(ownerColourInt)),
        units(msg.read64()),
        turnLastMoved(msg.read64()),
        movesThisTurn(msg.read32()),
        unitsString() {
        KShortenedNumber::format(&unitsString[0], units); // We just assigned units initially (from the message), so update units string.
    }

    void Army::writeTo(simplenetwork::Message & msg) const {
        msg.write64(id);
        msg.write64(owner);
        msg.write32(ownerColourInt);
        msg.write64(units),
        msg.write64(turnLastMoved),
        msg.write32(movesThisTurn);
    }

    #define TRIANGLE_WIDTH_RADIUS 10.0f
    #define TRIANGLE_HEIGHT 20.0f

    #define BOX_WIDTH 58.0f
    #define BOX_HALF_WIDTH 29.0f
    #define BOX_HEIGHT 20.0f

    const GLfloat Army::ARMY_DISPLAY_WIDTH = BOX_WIDTH;

    #define TEXT_X 2
    #define TEXT_Y 2
    #define TEXT_SCALE 1.333333333333333333333333333f

    /**
     * This macro draws this army. Having this as a macro allows us to pick different
     * drawing modes, such as normal drawing and placeholder drawing.
     */
    #define ARMY_DRAW(cameraProperties, x, y, text,  triOut, triFill, boxOut, boxFill, textCol) \
                                                                                                \
        GLfloat uiScale = *cameraProperties.uiScale;                                            \
                                                                                                \
        GLfloat triW = TRIANGLE_WIDTH_RADIUS * uiScale,                                         \
                triH = TRIANGLE_HEIGHT * uiScale,                                               \
                                                                                                \
                boxW = BOX_WIDTH * uiScale,                                                     \
                boxH = BOX_HEIGHT * uiScale,                                                    \
                                                                                                \
                bord = floor(uiScale),                                                          \
                bord2 = bord * 2.0f,                                                            \
                                                                                                \
                textX = TEXT_X * uiScale,                                                       \
                textY = TEXT_Y * uiScale,                                                       \
                textScale = TEXT_SCALE * uiScale;                                               \
                                                                                                \
                                                                                                \
        /* ----------- DRAW THE TRIANGLE -------------- */                                      \
                                                                                                \
        triOut                                                                                  \
            glVertex2f(x, y);                                                                   \
            glVertex2f(x - triW, y - triH);                                                     \
            glVertex2f(x + triW, y - triH);                                                     \
            glVertex2f(x, y);                                                                   \
                                                                                                \
        y -= bord2; triH -= bord2; triW -= bord;                                                \
                                                                                                \
        triFill                                                                                 \
            glVertex2f(x, y);                                                                   \
            glVertex2f(x - triW, y - triH);                                                     \
            glVertex2f(x + triW, y - triH);                                                     \
            glVertex2f(x, y);                                                                   \
                                                                                                \
        /* --------- DRAW THE BOX ------------------- */                                        \
                                                                                                \
        y -= (boxH + triH);                                                                     \
        x -= BOX_HALF_WIDTH * uiScale;                                                          \
                                                                                                \
        boxOut                                                                                  \
            glVertex2f(x, y);                                                                   \
            glVertex2f(x + boxW, y);                                                            \
            glVertex2f(x + boxW, y + boxH);                                                     \
            glVertex2f(x, y + boxH);                                                            \
                                                                                                \
        x += bord; y += bord; boxW -= bord2; boxH -= bord2;                                     \
                                                                                                \
        boxFill                                                                                 \
            glVertex2f(x, y);                                                                   \
            glVertex2f(x + boxW, y);                                                            \
            glVertex2f(x + boxW, y + boxH);                                                     \
            glVertex2f(x, y + boxH);                                                            \
                                                                                                \
        /* --------- DRAW THE TEXT ----------------- */                                         \
                                                                                                \
        textCol                                                                                 \
            wool::Font::font.drawFloorStringScale(                                              \
                text, x + textX, y + textY, textScale, textScale                                \
            );

    // ======================================== default drawing ================================================

    #define DEFAULT_TRIANGLE_OUTLINE glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
    #define DEFAULT_TRIANGLE_FILL    glColor4f(ownerColour.red, ownerColour.green, ownerColour.blue, ownerColour.alpha);
    #define DEFAULT_BOX_OUTLINE      if (mouseOvered) { glColor4f(1.0f, 1.0f, 1.0f, 0.4f); } else { glColor4f(0.0f, 0.0f, 0.0f, 1.0f); }
    #define DEFAULT_TEXT_COLOUR      if (canMove) { glColor4f(0.0f, 0.0f, 0.0f, 1.0f); } else { glColor4f(0.8f, 1.0f, 1.0f, 1.0f); }

    #define DEFAULT_BOX_FILL         GLfloat alpha = mouseOvered ? 0.7f : 1.0f;     \
                                     if (canMove) {                                 \
                                         if (selected) {                            \
                                             glColor4f(1.0f, 1.0f, 0.0f, alpha);    \
                                         } else {                                   \
                                             glColor4f(1.0f, 1.0f, 1.0f, alpha);    \
                                         }                                          \
                                     } else {                                       \
                                         if (selected) {                            \
                                             glColor4f(0.5f, 0.5f, 0.0f, alpha);    \
                                         } else {                                   \
                                             glColor4f(0.4f, 0.4f, 0.4f, alpha);    \
                                         }                                          \
                                     }

    void Army::draw(const CameraProperties & cameraProperties, GLfloat x, GLfloat y, const std::unordered_set<uint64_t> & selectedArmies, uint64_t turnCount) const {
        bool mouseOvered = (cameraProperties.armyId == getArmyId()),
             selected = selectedArmies.find(getArmyId()) != selectedArmies.end(),
             canMove = canMoveThisTurn(turnCount, 1);

        ARMY_DRAW(cameraProperties, x, y, &unitsString[0], DEFAULT_TRIANGLE_OUTLINE, DEFAULT_TRIANGLE_FILL, DEFAULT_BOX_OUTLINE, DEFAULT_BOX_FILL, DEFAULT_TEXT_COLOUR)
    }

    // ======================================== placeholder drawing ============================================

    #define PLACEHOLDER_TRIANGLE_OUTLINE glColor4f(0.0f, 0.0f, 0.0f, 0.3f);
    #define PLACEHOLDER_TRIANGLE_FILL    glColor4f(1.0f, 1.0f, 1.0f, 0.3f);
    #define PLACEHOLDER_BOX_OUTLINE      glColor4f(0.0f, 0.0f, 0.0f, 0.3f);
    #define PLACEHOLDER_BOX_FILL         glColor4f(1.0f, 1.0f, 1.0f, 0.6f);
    #define PLACEHOLDER_TEXT_COLOUR      glColor4f(0.0f, 0.0f, 0.0f, 0.7f);

    void Army::drawPlaceholder(const CameraProperties & cameraProperties, int armyCount, GLfloat x, GLfloat y) const {
        ARMY_DRAW(cameraProperties, x, y, std::to_string(armyCount)+":", PLACEHOLDER_TRIANGLE_OUTLINE, PLACEHOLDER_TRIANGLE_FILL, PLACEHOLDER_BOX_OUTLINE, PLACEHOLDER_BOX_FILL, PLACEHOLDER_TEXT_COLOUR)
    }

    // ========================================================================================================

    bool Army::isMouseOver(const CameraProperties & cameraProperties, GLfloat x, GLfloat y, GLfloat viewX, GLfloat viewY) const {
        GLfloat uiScale = *cameraProperties.uiScale;
        return std::abs(x - viewX) <= BOX_HALF_WIDTH * uiScale &&
               viewY <= y && viewY >= y - (BOX_HEIGHT + TRIANGLE_HEIGHT) * uiScale;
    }

    // ----------------------------------- MOVING --------------------------------------------------------

    bool Army::canMoveThisTurn(uint64_t turn, uint32_t moves) const {
        if (turn > turnLastMoved) {
            return moves <= MAX_ARMY_MOVES;
        } else {
            return movesThisTurn + moves <= MAX_ARMY_MOVES;
        }
    }

    void Army::onMove(uint64_t turn, uint32_t moves) {
        if (turn > turnLastMoved) {
            movesThisTurn = moves;
        } else {
            movesThisTurn += moves;
        }
        turnLastMoved = turn;
    }

    void Army::addUnits(int64_t units, uint64_t turnCount) {
        this->units += units;
        turnLastMoved = turnCount;
        movesThisTurn = MAX_ARMY_MOVES;
        KShortenedNumber::format(&unitsString[0], this->units); // We just updated the value of units, so update the units string.
    }

    void Army::addUnitsWithoutTurn(int64_t units) {
        this->units += units;
        KShortenedNumber::format(&unitsString[0], this->units); // We just updated the value of units, so update the units string.
    }

    void Army::changeColour(uint32_t newColour) {
        ownerColourInt = newColour;
        ownerColour = wool::RGBA(ownerColourInt);
    }
}

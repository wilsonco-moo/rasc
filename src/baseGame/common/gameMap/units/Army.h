/*
 * Army.h
 *
 *  Created on: 27 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_UNITS_ARMY_H_
#define BASEGAME_COMMON_GAMEMAP_UNITS_ARMY_H_

#include <unordered_set>
#include <cstdint>
#include <cstddef>
#include <array>

#include <sockets/plus/message/Message.h>
#include <wool/misc/RGBA.h>

#include "../../util/numeric/KShortenedNumber.h"
#include "../../config/ReleaseMode.h"

namespace rasc {

    class CameraProperties;
    class UniqueIdAssigner;
    class PlayerData;
    class Province;

    /**
     * Note that Army is a simple class without
     * a (virtual) destructor, so do not make subclasses.
     */
    class Army {

    public:
        const static uint64_t INVALID_ARMY;
        
        /**
         * Maximum number of moves this army is able to make, each turn.
         */
        const static uint64_t MAX_ARMY_MOVES;

    private:
        /**
         * Our id.
         */
        const uint64_t id;

        /**
         * The player id that owns this army.
         */
        uint64_t owner;

        /**
         * Our owner colour (as an int).
         */
        uint32_t ownerColourInt;

        /**
         * The colour of our owner.
         */
        wool::RGBA ownerColour;

        /**
         * The number of units in our army.
         */
        uint64_t units;

        /**
         * The turn we last moved.
         */
        uint64_t turnLastMoved;

        /**
         * The number of moves we have made this turn.
         */
        uint32_t movesThisTurn;

        /**
         * This stores a string representation of the number of units
         * we have.
         */
        std::array<char, KShortenedNumber::SIZE> unitsString;

    public:
        /**
         * This is the normal constructor for Army. This should be used when creating a new Army, where we know
         * the player that must own it in the future.
         * This constructor allocates us a new unique army id, from the specified UniqueIdAssigner.
         */
        Army(PlayerData * owner, uint64_t units, uint64_t turnCount, UniqueIdAssigner * uniqueIdAssigner);

        #ifdef RASC_DEVELOPMENT_MODE
            /**
             * Creates an Army with the specified owner and units, and the specified ID.
             * Since this does not rely on a unique ID assigner, this should only be used for testing
             * purposes (like in the development console), and hence is only included in development mode.
             */
            Army(PlayerData * owner, uint64_t units, uint64_t turnCount, uint64_t id);
        #endif

        /**
         * This constructor allows a new army to be created from raw information.
         * This constructor allocates us a new unique army id, from the specified UniqueIdAssigner.
         */
        Army(uint64_t playerId, uint32_t playerColour, uint64_t units, uint64_t turnCount, UniqueIdAssigner * uniqueIdAssigner);

        /**
         * This constructor creates a new army, with all the owner and turn properties of the specified one,
         * but allows the number of units to be specified.
         * This constructor is useful for splitting armies.
         * This constructor allocates us a new unique army id, from the specified UniqueIdAssigner.
         */
        Army(const Army & otherArmy, uint64_t units, UniqueIdAssigner * uniqueIdAssigner);

        /**
         * This constructor should be used when we receive a message about a new army. This can create an army from
         * a respective army.writeTo() call.
         * This constructor, (which is mostly useful on the client-side), allocates us an army id defined by the message.
         */
        Army(simplenetwork::Message & msg);

        /**
         * This writes all the data necessary to create an army from the message, using the Army(simplenetwork::Message & msg)
         * constructor.
         */
        void writeTo(simplenetwork::Message & msg) const;

        /**
         * Province should call this.
         * This draws the default army label at the specified position.
         * The position should be the view position of the centre point of the province.
         */
        void draw(const CameraProperties & cameraProperties, GLfloat x, GLfloat y, const std::unordered_set<uint64_t> & selectedArmies, uint64_t turnCount) const;

        /**
         * Province should call this.
         * This draws the placeholder army label at the specified position.
         * The position should be the view position of the centre point of the province.
         */
        void drawPlaceholder(const CameraProperties & cameraProperties, int armyCount, GLfloat x, GLfloat y) const;

        /**
         * Province should call this.
         * x and y should be the view position of the centre point of the province.
         * viewX and viewY should be the view position of the mouse.
         * This method returns true if the mouse is hovering over this army.
         */
        bool isMouseOver(const CameraProperties & cameraProperties, GLfloat x, GLfloat y, GLfloat viewX, GLfloat viewY) const;

        inline bool operator == (const Army & other) { return id == other.id; }
        inline bool operator != (const Army & other) { return id != other.id; }

        inline bool operator <  (const Army & other) { return id <  other.id; }
        inline bool operator >  (const Army & other) { return id >  other.id; }
        inline bool operator <= (const Army & other) { return id <= other.id; }
        inline bool operator >= (const Army & other) { return id >= other.id; }

        /**
         * The the amount of horizontal space required between armies, when drawn by Province.
         */
        static const GLfloat ARMY_DISPLAY_WIDTH;

        inline uint64_t getArmyId(void) const {
            return id;
        }

        inline uint64_t getArmyOwner(void) const {
            return owner;
        }

        inline uint32_t getArmyOwnerColour(void) const {
            return ownerColourInt;
        }

        inline uint64_t getUnitCount(void) const {
            return units;
        }

        inline const char * getUnitCountString(void) const {
            return &unitsString[0];
        }

        inline uint64_t getTurnLastMoved(void) const {
            return turnLastMoved;
        }

        // ----------------------- MOVING AND STUFF ---------------------------

        bool canMoveThisTurn(uint64_t turn, uint32_t moves) const;

        void onMove(uint64_t turn, uint32_t moves);

        void addUnits(int64_t units, uint64_t turnCount);

        void addUnitsWithoutTurn(int64_t units);

        void changeColour(uint32_t newColour);
    };
}

#endif

/*
 * Properties.h
 *
 *  Created on: 8 Jun 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROPERTIES_PROPERTIES_H_
#define BASEGAME_COMMON_GAMEMAP_PROPERTIES_PROPERTIES_H_

#include <string>
#include <vector>

#include "../provinceUtil/ProvStatMod.h"
#include "PropertyDef.h"

namespace simplenetwork {
    class Message;
}

namespace rasc {
    class TerDispLayer;
    class CommonBox;
    class Province;

    /**
     * This class contains the static methods required for dealing with
     * properties.
     */
    class Properties {
    public:

        /**
         * The internal name for invalid property types, or invalid property base
         * types.
         */
        const static std::string INVALID_INTERNAL_NAME;

        /**
         * This enum stores the IDs of all property base types, along with two special
         * values: COUNT and INVALID.
         */
        class BaseTypes {
        public:
            enum {
                #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                          getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                          writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                          receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                        baseType,
                RASC_PROPERTY_BASE_TYPES_LIST
                #undef X
                COUNT, INVALID
            };
        };

        /**
         * This enum defines the IDs of all property types, along with two special
         * values: COUNT and INVALID.
         */
        class Types {
        public:
            enum {
                #define X(propertyType, baseType) propertyType,
                RASC_PROPERTIES_LIST
                #undef X
                COUNT, INVALID
            };
        };

    private:
        // This lookup table stores the base type of each property.
        static const propbasetype_t baseTypes[Types::COUNT];

    public:
        // ============================ Convenience/accessor methods ==================================

        /**
         * Gets the base type of a property.
         * Note that this is undefined for unknown or out of range property type IDs.
         */
        static inline propbasetype_t getBaseType(proptype_t propertyType) {
            return baseTypes[propertyType];
        }

        /**
         * Returns true if the specified property is valid (i.e: it is
         * not out of range).
         */
        static bool isValidProperty(proptype_t propertyType);

        /**
         * Returns true if the specified property base type is
         * valid (i.e: it is not out of range).
         */
        static bool isValidPropertyBaseType(propbasetype_t baseType);

        /**
         * Gets the internal name for a property type.
         * This is helpful, as it can be converted back to a property type
         * using internalNameToType.
         * If the property type is invalid, this returns INVALID_INTERNAL_NAME.
         */
        static const std::string & typeToInternalName(proptype_t propertyType);

        /**
         * Gets the internal name for a property base type.
         * This is helpful, as it can be converted back to a property base type
         * using internalNameToBaseType.
         * If the property type is invalid, this returns INVALID_INTERNAL_NAME.
         */
        static const std::string & baseTypeToInternalName(propbasetype_t baseType);

        /**
         * Gets the associated property type from the internal name (note that this
         * is case sensitive). This can be used in combination with typeToInternalName.
         * If the name does not represent a valid property, Properties::Types::INVALID will be
         * returned.
         */
        static proptype_t internalNameToType(const std::string & internalName);

        /**
         * Gets the associated property base type from the internal name (note that this
         * is case sensitive). This can be used in combination with baseTypeToInternalName.
         * If the name does not represent a valid property, Properties::BaseTypes::INVALID will be
         * returned.
         */
        static proptype_t internalNameToBaseType(const std::string & internalName);


        // ========================== General methods to deal with properties =========================
        // Note that all of these methods are undefined for unknown or out of range property IDs.

        /**
         * Creates a property, from a message.
         *
         * See createExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static PropertyStorage createProperty(CommonBox & commonBox, Province & province,
                                              simplenetwork::Message & msg, proptype_t propertyType);

        /**
         * Gets the stat modifier set associated with a property.
         *
         * See getStatModifierSetExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static ProvStatModSet getStatModifierSet(CommonBox & commonBox, Province & province,
                                                 proptype_t propertyType, PropertyStorage & property);

        /**
         * Gets the friendly name associated with a property.
         *
         * See getFriendlyNameExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static std::string getFriendlyName(CommonBox & commonBox, Province & province,
                                           proptype_t propertyType, PropertyStorage & property);

        /**
         * Gets the description associated with a property.
         *
         * See getDescriptionExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static std::string getDescription(CommonBox & commonBox, Province & province,
                                          proptype_t propertyType, PropertyStorage & property);

        /**
         * Returns whether the specified property needs to be updated each turn.
         *
         * See needsUpdatingExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static bool needsUpdating(CommonBox & commonBox, Province & province,
                                  proptype_t propertyType, PropertyStorage & property);

        /**
         * Returns the icon ID which is most appropriate for the specified property.
         *
         * See getIconIdExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static unsigned int getIconId(CommonBox & commonBox, Province & province,
                                      proptype_t propertyType, PropertyStorage & property);

        /**
         * Writes all appropriate terrain display layers, in order to provide
         * a pictorial display of this property.
         *
         * See addTerrDispLayersExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static void addTerrDispLayers(CommonBox & commonBox, Province & province,
                                      std::vector<TerDispLayer> & layersVec,
                                      proptype_t propertyType, PropertyStorage & property);

        /**
         * Runs an update on the provided property, writing it to the message.
         * This should be called on the server side. It generates a message, which should
         * be read by receiveUpdate. This should be called each turn on properties which
         * return true for needsUpdating.
         *
         * See writeUpdateExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static void writeUpdate(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                proptype_t propertyType, PropertyStorage & property);

        /**
         * Runs the second part of an update on the provided property. This provides
         * properties with a way to write misc updates.
         * This should be called each turn on properties which return true for needsUpdating,
         * after calling writeUpdate.
         *
         * See writeMiscUpdateExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static void writeMiscUpdate(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                    proptype_t propertyType, PropertyStorage & property);

        /**
         * This should be called on the client (and server) side, in response to a message
         * written by the writeUpdate method. This applies changes written by the per-turn update,
         * to the property.
         *
         * See receiveUpdateExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static void receiveUpdate(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                  proptype_t propertyType, PropertyStorage & property);

        /**
         * This should be called on the client (and server) side, in response to a
         * direct update. This allows property-specific updates to be created by the server.
         *
         * See receiveDirectUpdateExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static void receiveDirectUpdate(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                        proptype_t propertyType, PropertyStorage & property);

        /**
         * Writes initial data of the specified property. This is readable by the create method.
         * The isSaving parameter should specify whether this is saving to a savefile on the
         * server, or sending initial data to a client (when they join, or when the property
         * is added). This allows a property to hold additional data on the server side, which
         * the client does not need to know.
         *
         * See writeInitialDataExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static void writeInitialData(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                     bool isSaving, proptype_t propertyType, PropertyStorage & property);

        /**
         * This deletes all allocation or state associated with the property. This
         * must be called during cleanup (deallocating properties at the end), and whenever
         * a property is removed.
         *
         * See deleteExpr in PropertyDef.h (in documentation for RASC_PROPERTY_BASE_TYPES_LIST).
         */
        static void deleteProperty(CommonBox & commonBox, Province & province,
                                   proptype_t propertyType, PropertyStorage & property);


        // ======================== Reading/writing properties alongside type/ID =======================

        /**
         * A convenience method which writes a property type and a unique ID to a message.
         * This defines the standard format for doing this operation, and as a result MUST be
         * used whenever a property type and unique ID is written. Doing this assures
         * compatibility.
         */
        static void writeTypeAndId(simplenetwork::Message & msg, proptype_t propertyType, uint64_t uniqueId);

        /**
         * A convenience method which reads a property type and a unique ID from a message.
         * This defines the standard format for doing this operation, and as a result MUST be
         * used whenever a property type and unique ID is read. Doing this assures
         * compatibility.
         */
        static void readTypeAndId(simplenetwork::Message & msg, proptype_t & propertyType, uint64_t & uniqueId);

        /**
         * First writes the property type and ID of the property using writeTypeAndId,
         * then writes the initial data from the property using writeInitialData.
         *
         * The data written here is appropriate to use with createPropertyTypeAndId.
         */
        static void writeInitialDataTypeAndId(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                              bool isSaving, uint64_t uniqueId, proptype_t propertyType, PropertyStorage & property);

        /**
         * First reads the property type and ID of the property using readTypeAndId.
         * Next, if the property type is valid, creates a property from the message using createProperty.
         * If the property type is not valid, the rest of the message is ignored and false is returned.
         *
         * Returns true on success, and false on failure.
         * Note that if false is returned (on failure), the property parameter will not have been assigned.
         *
         * This is appropriate to use with writeInitialDataTypeAndId.
         */
        static bool createPropertyTypeAndId(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                            uint64_t & uniqueId, proptype_t & propertyType, PropertyStorage & property);
    };
}

#endif

/*
 * ProvinceProperties.h
 *
 *  Created on: 23 Sep 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROPERTIES_PROVINCEPROPERTIES_H_
#define BASEGAME_COMMON_GAMEMAP_PROPERTIES_PROVINCEPROPERTIES_H_

#include <vector>
#include <array>

#include "../../update/updateTypes/CheckedUpdate.h"
#include "../../update/updateTypes/MiscUpdate.h"
#include "../provinceUtil/ProvinceStatTypes.h"
#include "../provinceUtil/ProvStatMod.h"
#include "../../update/RascUpdatable.h"
#include "../properties/PropertySet.h"
#include "../properties/PropertyDef.h"
#include "trait/TraitDef.h"

namespace tinyxml2 {
    class XMLElement;
}

namespace simplenetwork {
    class Message;
}

namespace rasc {
    class Province;
    class UniqueIdAssigner;

    /**
     * This convenient macro avoids typing, when trying to access the size of the
     * garrison in a province.
     */
    #define GET_GARRISON_SIZE(provinceProperties) \
        ((provinceProperties).getTraitCount(Properties::Types::garrison))

    /**
     * This class represents each property-dependent stat within ProvinceProperties.
     * There is one of these created for each ProvinceStatId.
     */
    class ProvinceStat {
    private:
        friend class ProvinceProperties;

        provstat_t baseValue, finalValue;
        ProvStatMod accStatModifiers;  // Accumulated stat modifiers from properties.

        ProvinceStat(provstat_t baseValue);

        // These allow adding and subtracting stat modifiers.
        inline void operator += (const ProvStatMod & modifier) { accStatModifiers += modifier; }
        inline void operator -= (const ProvStatMod & modifier) { accStatModifiers -= modifier; }

        // This should be called by ProvinceProperties internally after adding or subtracting any stat modifiers.
        // This updates our final value appropriately, using out accumulated stat modifiers.
        void updateFinalValue(void);

        // This should be called by ProvinceProperties internally to reset our accumulated stat modifiers,
        // such as when resetting due to loading initial data.
        void resetAccStatModifiers(void);

    public:
        /**
         * Allow convenient implicit conversion to an provstat_t type.
         */
        inline operator provstat_t() const { return finalValue; }

        /**
         * Allows access to our accumulated stat modifiers. This can be used, for example,
         * to figure out the percentage stat bonuses in provinces.
         */
        inline const ProvStatMod & getAccumulatedStatModifiers(void) const { return accStatModifiers; }

        /**
         * Allows access to our base stat value. This is the default value of this stat
         * in a province, before any stat modifiers are applied.
         */
        inline provstat_t getBaseValue(void) const { return baseValue; }

        /**
         * Calculates the prospective value of this province stat, if the provided ProvStatMod
         * were to be added to it. This is the sum of our accumulated stat modifiers and the
         * provided modifier, applied to our base stat value.
         */
        provstat_t calculateProspective(const ProvStatMod & modifier) const;

        /**
         * Calculates the prospective CHANGE IN value of this province stat, if the
         * provided ProvStatMod were to be added to it.
         * This is equivalent to and simpler than calculateProspective of the
         * modifier, minus our final stat value.
         */
        provstat_t calculateProspectiveChange(const ProvStatMod & modifier) const;
    };

    /**
     * A single instance of this class is contained within each instance of Province.
     * ProvinceProperties has the simple job of keeping track of the properties contained
     * within the province, and counting the stats which are affected by the properties.
     * ProvinceProperties ensures that all necessary data is loaded from the map xml file,
     * and is saved to savefiles.
     * It also ensures that every time a property is added, removed or updated, the necessary stuff is
     * updated.
     *
     * Stats can be accessed very conveniently, for the following reasons:
     *  > ProvinceProperties is easily accessible in each province.
     *  > Our operator[] can be used to access stats.
     *  > ProvinceStat can be implicitly converted to an provstat_t.
     * For example:
     *   Province * someProvince = ...;
     *   provstat_t manpowerIncome = someProvince->properties[ProvinceStatIds::manpowerIncome];
     */
    class ProvinceProperties : public RascUpdatable {
    private:
        // A unique ID assigner. This is required for assigning unique IDs to new properties.
        // Note that this only exists on the server - on the client side this will be NULL.
        UniqueIdAssigner * uniqueIdAssigner;

        // Our stats. This should be accessed from the outside with
        // our operator[].
        std::array<ProvinceStat, ProvStatIds::COUNT> stats;

        // Our province's properties. The traits defined in the XML file are initially stored here.
        PropertySet propertySet;

        // The province we are related to. This is provided so that we can provide misc updates.
        Province & province;

        // The property ID representing our terrain type.
        // Note that this will be UniqueIdAssigner::INVALID_ID until either XML data is
        // read (on the server side), or initial data is read (on the client side).
        // This property ID MUST NEVER BE REMOVED.
        uint64_t typeProperty;

        // Stores whether our type defined us as accessible. This is provided for convenient
        // lookup of this information, and is set by our updateAccessibleStatus method.
        // By default this is false.
        bool isAccessible;

    public:
        /**
         * The xml element provided here must be the base XML tag for the province,
         * from the gameMap xml file.
         */
        ProvinceProperties(Province & province, UniqueIdAssigner * uniqueIdAssigner);
        virtual ~ProvinceProperties(void);

    private:
        // Zeroes our overall stat modifiers, and resets the accumulated stat.
        // modifiers for all of our stats.
        void resetAll(void);

        // This should be called after each time typeProperty is changed, (e.g:
        // after loading initially, and after . This
        // queries whether the type defines itself as accessible.
        void updateAccessibleStatus(void);

        // Adds and subtracts a set of province stat modifiers. Used internally.
        void addStatModifierSet(const ProvStatModSet & modifierSet);
        void subtractStatModifierSet(const ProvStatModSet & modifierSet);

        // Calculates the final value of all stats, using their base value, and their
        // accumulated stat modifiers. Used internally.
        // The parameter notifyStatSystem specifies whether to notify the unified
        // stat system about any changes to stat values.
        //  > The unified stat system should be notified if:
        //    - updateAllStats is being called because of a CHANGE which has happened
        //      during the game, such as a trait being added or removed.
        //  > The unified stat system should NOT BE notified if:
        //    - updateAllStats is being called during initialisation of ProvinceProperties,
        //      to set the initial values of stats.
        //    - updateAllStats is being called after loading initial data about
        //      province traits.
        void updateAllStats(bool notifyStatSystem);

    // ------------------ RascUpdatable stuff and methods --------------------------

        // Define all the misc update types for ProvinceProperties.
        class PropertiesMiscUpdateTypes {
        public:
            enum {
                propertyCreate,
                propertyDirectUpdate,
                propertyPerTurnUpdate,
                propertyDelete,

                COUNT_OF_MISC_UPDATE_TYPES
            };
        };
        using PropertiesMiscUpdateType = uint8_t;
        void writeMiscUpdateType(simplenetwork::Message & msg, PropertiesMiscUpdateType update) const;
        PropertiesMiscUpdateType readMiscUpdateType(simplenetwork::Message & msg) const;

        // Define all the checked update types for ProvinceProperties.
        class PropertiesCheckedUpdateTypes {
        public:
            enum {
                requestConstructBuildings,

                COUNT_OF_CHECKED_UPDATE_TYPES
            };
        };
        using PropertiesCheckedUpdateType = uint8_t;
        void writeCheckedUpdateType(simplenetwork::Message & msg, PropertiesCheckedUpdateType update) const;
        PropertiesCheckedUpdateType readCheckedUpdateType(simplenetwork::Message & msg) const;

        // ----------------- Misc and checked update receive methods ------------

        bool onPropertyCreate(simplenetwork::Message & msg);
        bool onPropertyDirectUpdate(simplenetwork::Message & msg);
        bool onPropertyPerTurnUpdate(simplenetwork::Message & msg);
        bool onPropertyDelete(simplenetwork::Message & msg);
        void onRequestConstructBuildings(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer);

        // ----------------------------------------------------------------------

    protected:
        // At the end of this we always run our province's runOnProvinceChange() method,
        // since an update to province properties counts as an update to the province itself.
        virtual bool onReceiveMiscUpdate(simplenetwork::Message & msg) override;

        // For receiving checked updates.
        virtual bool onReceiveCheckedUpdate(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) override;

        // This allows trait information to be saved to savefiles, and sent do players when they join.
        virtual void onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) override;
        virtual bool onReceiveInitialData(simplenetwork::Message & msg) override;

    // ------------------------- Accessor and other utility methods ----------------

    public:

        /**
         * This method MUST be called immediately after constructing ProvinceProperties,
         * when province is initialised from XML.
         * This loads all province traits and type from the map XML.
         */
        void loadFromXML(tinyxml2::XMLElement * element);
        
        /**
         * Allows const and non-const access to our property set.
         */
        inline const PropertySet & getPropertySet(void) const {
            return propertySet;
        }
        inline PropertySet & getPropertySet(void) {
            return propertySet;
        }

        /**
         * Allows convenient access to our province.
         */
        inline Province & getProvince(void) const {
            return province;
        }

        /**
         * Allows easy access to each of our stats.
         * Note that this is our operator[], and each stat can be implicitly converted
         * to a provstat_t, which allows things like this:
         *   Province * someProvince = ...;
         *   provstat_t manpowerIncome = someProvince->properties[ProvinceStatIds::manpowerIncome];
         */
        inline const ProvinceStat & operator [] (ProvStatId statId) const {
            return stats[statId];
        }

        /**
         * Returns whether our associated terrain type specifies that this province should
         * be accessible. Note that this will only return a correct value after loading XML
         * data on the server side, or after loading initial data on the client side.
         */
        inline bool isProvinceAccessible(void) const {
            return isAccessible;
        }

        /**
         * Returns the unique ID of our associated terrain type property. Note that
         * until we have either loaded XML data on the server side, or loaded initial
         * data on the client side, this will return UniqueIdAssigner::INVALID_ID.
         */
        inline uint64_t getTerrainTypeUniqueId(void) const {
            return typeProperty;
        }

        /**
         * A convenience method to access the name of our province type,
         * such as plains, tundra etc. This is done by getting the friendly
         * name of our terrain type property.
         * An empty string is returned in the case that the terrain type is
         * unknown, (i.e: not loaded from XML on the server side, or not
         * loaded initial data yet on the client side).
         */
        std::string getProvinceTypeName(void) const;

        /**
         * Makes a copy of our CURRENT overall stats, as a stat array.
         */
        StatArr getStatArray(void) const;

        /**
         * Generates a ProvStatModSet from the accumulated stat modifier of
         * each of our stats. This performs ProvinceStat::getAccumulatedStatModifiers,
         * on all of our stats.
         */
        ProvStatModSet getAccumulatedStatModifierSet(void) const;

        /**
         * Populates the provided array of float values with the calculated "value-for-money"
         * of each building valued stat, considering the provided stat modifier set and overall cost,
         * if it were to be added to this province.
         * Additionally, whether the area in which the province is located is totally owned by
         * a single player must be specified. This controls whether the stat bonus from owning
         * an entire area is taken into account, when working out "value-for-money".
         */
        void calculateValueForMoney(std::array<float, BuildValuedStatIds::COUNT> & valueArr,
                                    const ProvStatModSet & modifier, provstat_t overallCost,
                                    bool doesOwnEntireArea) const;

        /**
         * Returns true if the addition of the provided stat modifier set would exceed available land.
         * This is a more general purpose method than "doesLandUseModifierExceedLand".
         */
        bool doesModifierExceedLand(const ProvStatModSet & modifier) const;

        /**
         * Returns true if the addition of the provided stat modifier, to land use, would exceed
         * available land. Compared to doesModifierExceedLand, this is a shortcut which can be taken
         * only when it is known that a buildings does not apply any modifier to land availability.
         */
        bool doesLandUseModifierExceedLand(const ProvStatMod & landUseModifier) const;

        // ------------------------ Public RascUpdatable overrides ------------------------

        /**
         * On turn misc updates, ProvinceProperties runs a per-turn update
         * for all properties which need updating.
         */
        virtual void onGenerateTurnMiscUpdate(MiscUpdate update) override;


        // ------------------------ Basic property misc updates ---------------------------

        /**
         * Writes a property create misc update "header" to the message.
         *
         * First this writes a rasc updatable header and misc update type, to create a property.
         * Next, this generates a new unique ID and writes it to the message, alongside the provided
         * property type, using Properties::writeTypeAndId. The unique ID is then returned.
         *
         * After a create "header", the create "content" appropriate for the property type
         * must be written to the message. That will form a misc update message, able to
         * be read by ProvinceProperties, which will create the appropriate property.
         * The unique ID of the property is returned, and can be used later on to remove the property.
         *
         * Note that this must only be called from the server side, as this requires the
         * creation of a unique ID.
         */
        uint64_t writePropertyCreateHeader(simplenetwork::Message & msg, proptype_t propertyType);

        /**
         * Writes a property direct update misc update "header" to the message.
         *
         * This simply writes the appropriate rasc updatable header and misc update ID,
         * to interpret a property direct update. After this, the appropriate direct update
         * "content" must be written, to form a misc update able to be read by ProvinceProperties.
         */
        void writePropertyDirectUpdateHeader(simplenetwork::Message & msg, uint64_t uniqueId);

        /**
         * Writes a misc update to delete the property with the specified ID.
         */
        void deleteProperty(MiscUpdate update, uint64_t uniqueId);


        // ------------------------- Wrapper methods for traits ---------------------------

        /**
         * Assuming the specified property type *is* in fact a trait (its base type must
         * be trait), this returns the count associated with this property, or zero
         * if it is not present within this province.
         */
        provstat_t getTraitCount(proptype_t propertyType) const;

        /**
         * A convenience method which allows changing the count value associated with
         * the specified trait (property). This assumes that the specified property
         * *is* in fact a trait. If not, this will cause undefined behaviour. Note
         * that this must ONLY be called from the server-side.
         * If this results in no change, no message is written. Changing to/from zero
         * count values removes/adds properties, other changes write direct update messages.
         *
         * This method returns the unique ID of the property related to the change.
         *  > If this call causes a property to be created, the unique ID of the new
         *    property is returned. Note that this only becomes valid once the misc
         *    update is read.
         *  > If this call causes a property to be directly updated, the unique ID
         *    of the property which was updated is returned.
         *  > In the case that the provided count is zero:
         *    - If this call causes a property to be deleted, the old unique ID of the
         *      deleted property is returned. This only becomes an invalid property ID
         *      once the misc update is read.
         *    - Otherwise, (if a relevant property already didn't exist, so was not
         *      deleted, UniqueIdAssigner::INVALID_ID is returned.
         */
        uint64_t changeTraitCount(MiscUpdate update, proptype_t propertyType, provstat_t count);

        /**
         * Adds a trait, by setting the trait count value to one.
         * This is exactly equivalent to running changeTraitCount(miscUpdate, trait, 1).
         * Returns the same as changeTraitCount: the ID of the created property, or
         * the ID of an existing property if one was already present.
         */
        uint64_t addTrait(MiscUpdate update, proptype_t traitProperty);

        /**
         * Removes a trait, by setting the trait count value to zero.
         * This is exactly equivalent to running changeTraitCount(miscUpdate, trait, 0).
         * Returns the same as changeTraitCount: the ID of the property removed, or
         * UniqueIdAssigner::INVALID_ID if the property was already not present.
         */
        uint64_t removeTrait(MiscUpdate update, proptype_t traitProperty);

        /**
         * Clears, populates and sorts the std::vector, with the properties contained
         * within our property set, which happen to be traits.
         *
         * The properties are sorted in ascending order of ID. This is more helpful
         * than accessing traits directly from the property set, especially for UI,
         * as this will always show a consistent order.
         */
        void getSortedTraits(std::vector<std::pair<proptype_t, provstat_t>> & traitsVector) const;

        /**
         * Does the same as getSortedTraits, except that this additionally filters
         * as to show only traits which have the specified display mode.
         */
        void getSortedTraitsByDisplayMode(std::vector<std::pair<proptype_t, provstat_t>> & traitsVector, TraitDef::DisplayMode displayMode) const;


        // --------------------------- Checked update methods -----------------------------

        /**
         * Constructs the specified number of buildings in this province. If this is positive, it
         * builds additional buildings. If this is negative, it demolishes buildings.
         * This is only allowed if we own the specified province, there is no ongoing battle,
         * we have the required resources, and the specified property type *is indeed* a trait
         * property, which is additionally buildable.
         *
         * The server will translate this into misc updates which create/update/delete a property,
         * and subtract required resources.
         */
        void requestConstructBuildings(CheckedUpdate update, proptype_t propertyType, provstat_t count);
    };
}

#endif

/*
 * PropertySet.h
 *
 *  Created on: 9 Jun 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROPERTIES_PROPERTYSET_H_
#define BASEGAME_COMMON_GAMEMAP_PROPERTIES_PROPERTYSET_H_

#include "PropertyDef.h"

#include <unordered_map>
#include <unordered_set>
#include <cstdint>
#include <vector>
#include <string>

namespace simplenetwork {
    class Message;
}

namespace rasc {
    class ProvStatModSet;
    class TerDispLayer;
    class CommonBox;
    class Province;

    /**
     * PropertySet is a container designed to store and manage properties.
     *
     * Note: Do not make subclasses - this class does not have a virtual destructor.
     */
    class PropertySet {
    public:
        /**
         * A simple class representing a property type with an associated property storage.
         * This is to avoid confusion from lots of nested std::pairs.
         * Notice that the storage can be freely modified, but the type cannot.
         */
        class TypeAndStorage {
        private:
            proptype_t type;
            PropertyStorage storage;

        public:
            TypeAndStorage(void);
            TypeAndStorage(proptype_t type, PropertyStorage storage);

            /**
             * Alllows access to the property type, but not modification of it.
             */
            inline const proptype_t getType(void) const {
                return type;
            }

            /**
             * Allows const access to the property storage.
             */
            inline const PropertyStorage & getStorage(void) const {
                return storage;
            }

            /**
             * Allows non-const access to the property storage.
             * This is important as it allows methods from Properties to modify the storage freely.
             */
            inline PropertyStorage & getStorage(void) {
                return storage;
            }
        };

    private:
        // -----------------------------------------------------------

        // This allows properties to have access to "the outside".
        CommonBox & box;
        Province & province;

        // This stores properties by their unique IDs, alongside their property types.
        std::unordered_map<uint64_t, TypeAndStorage> propertiesByUniqueId;

        // This stores, for each property type, the unique property IDs
        // which match that type.
        std::unordered_map<proptype_t, std::unordered_set<uint64_t>> propertiesByType;

        // The unique IDs of properties which need updating each turn.
        std::unordered_set<uint64_t> needUpdatingIds;

        // Do not allow copying: we hold raw resources.
        PropertySet(const PropertySet & other);
        PropertySet & operator = (const PropertySet & other);

    public:
        /**
         * Creates a property set which, by default, contains no properties.
         */
        PropertySet(CommonBox & box, Province & province);

        /**
         * Creates a property set, and automatically reads the properties from
         * the provided message.
         */
        PropertySet(CommonBox & box, Province & province, simplenetwork::Message & msg);

        ~PropertySet(void);

    private:
        // Internally adds the property to our data structures.
        // This assumes that the property type is known to be valid, and the
        // unique ID is known to not already exist.
        // This is used internally within addProperty and addPropertyFromMessage.
        // This cannot fail as no error checking is done.
        void addPropertyInternal(uint64_t uniqueId, proptype_t propertyType, PropertyStorage property);

        // Calls the Properties delete method and internally removes the property from our data structures.
        // This is used internally within deleteProperty and deletePropertyFromMessage.
        // This cannot fail as no error checking is done.
        void deletePropertyInternal(std::unordered_map<uint64_t, TypeAndStorage>::iterator byUniqueIdIter);
    public:

        // ==================== Property modification (from messages) ============================

        /**
         * Adds a property from the message, from a corresponding call to
         * writeInitialDataTypeAndId or similar.
         * If the unique ID doesn't already exist, and the property type is valid,
         * this creates the property using Properties::createProperty and
         * adds the property to our internal data structures.
         * Otherwise, nothing is done and false is returned.
         * This returns true on success, and false on failure.
         */
        bool addPropertyFromMessage(simplenetwork::Message & msg);

        /**
         * Performs the same as the above method, although if successful,
         * this also gets the new property's stat modifier set, and its
         * unique ID.
         * If successful, the provided stat modifier set will represent the
         * ADDED stat modifier CHANGE caused by the addition of this new property.
         */
        bool addPropertyFromMessage(simplenetwork::Message & msg, ProvStatModSet & addedStatModifier, uint64_t & uniqueId);

        /**
         * Reads a message generated by a corresponding writePerTurnUpdate.
         * Note that this only reads the first part, the other additional misc updates
         * must be read by the rasc updatable system.
         * This returns true on success, and false on failure.
         * If the list of unique property IDs, which require updating, does not *exactly* match the list
         * that *we* have, a warning is printed to the log, the rest of the message is ignored,
         * and false is returned.
         * If successful, the cumulative (ADDED) stat modifier change of all properties modified
         * by the turn update, is added to the provided stat modifier.
         */
        bool readPerTurnUpdate(simplenetwork::Message & msg, ProvStatModSet & addedStatModifier);

        /**
         * Reads a direct update message. The unique ID of the property is read first from the message.
         * before and after the update respectively.
         * This returns true on success, and false on failure.
         * If the unique ID does not exist, this method will return false, and not read the message.
         * If successful, the ADDED stat modifier change, resulting from the direct update,
         * is added to the provided stat modifier.
         */
        bool readDirectUpdate(simplenetwork::Message & msg, ProvStatModSet & addedStatModifier);

        /**
         * Deletes the property, specified by the ID read first from the message.
         * This returns true on success, and false on failure.
         * If the property did not exist, a complaint is printed and false is returned.
         * If successful, the provided stat modifier set will represent the
         * SUBTRACTED stat modifier CHANGE caused by the deletion of this new property,
         * i.e: the stat modifier which used to be imparted by this property.
         */
        bool deletePropertyFromMessage(simplenetwork::Message & msg, ProvStatModSet & subtractedStatModifier);


        // ========================== Property modification (direct) =============================

        /**
         * Adds the specified property to our internal data structures.
         * This returns true on success, and false on failure.
         * If the unique ID already exists, or the property type is invalid, this complains
         * and does nothing (returning false);
         */
        bool addProperty(uint64_t uniqueId, proptype_t propertyType, PropertyStorage property);

        /**
         * Deletes the property with the specified unique ID. Returns true on success, and false
         * on failure. If the unique ID did not exist, a complaint is printed to the log and
         * nothing happens (returning false).
         */
        bool deleteProperty(uint64_t uniqueId);

        /**
         * Deletes the property, specified by ID.
         * This returns true on success, and false on failure.
         * If the property did not exist, a complaint is printed and false is returned.
         * If successful, the provided stat modifier set will represent the
         * SUBTRACTED stat modifier CHANGE caused by the deletion of this new property,
         * i.e: the stat modifier which used to be imparted by this property.
         */
        bool deleteProperty(uint64_t uniqueId, ProvStatModSet & subtractedStatModifier);

        /**
         * Removes all of our properties.
         */
        void clear(void);


        // ============================== Property lookup methods ================================

        /**
         * Returns true if a property with the specified unique ID exists
         * within this property set.
         */
        bool propertyUniqueIdExists(uint64_t uniqueId) const;

        /**
         * Allows access to the storage of the property with specified ID.
         * If the unique ID does not exist within this property set, NULL is returned.
         * Note that this pointer could become invalid after any operation (such as
         * adding or updating properties), so must only be used temporarily.
         * A const and a non-const variant of this method is provided.
         */
        TypeAndStorage * getPropertyStorage(uint64_t uniqueId);
        const TypeAndStorage * getPropertyStorage(uint64_t uniqueId) const;

        /**
         * Allows access to the storage of the property with specified ID.
         * Note that this reference could become invalid after any operation (such as
         * adding or updating properties), so must only be used temporarily.
         *
         * Note that this is only defined behaviour if the property definitely exists,
         * (for example: propertyUniqueIdExists returned true, addProperty/addPropertyFromMessage
         * succeeded, or a unique ID returned by an accessor method such as getPropertiesByType).
         * A const and a non-const variant of this method is provided.
         */
        TypeAndStorage & getPropertyStorageUnchecked(uint64_t uniqueId);
        const TypeAndStorage & getPropertyStorageUnchecked(uint64_t uniqueId) const;

        /**
         * Returns a pointer to a set of ALL unique IDs, of the properties contained within
         * us which match the specified property type. All unique IDs returned by this
         * method are guaranteed to exist within us.
         * In the case that we have at least one property matching the specified
         * property type, this returns a pointer to an unordered set containing
         * AT LEAST ONE element.
         * In the case that there are no properties within us matching the specified
         * property type, this returns NULL.
         * Note that this pointer could become invalid after any operation (such as
         * adding or updating properties), so must only be used temporarily.
         */
        const std::unordered_set<uint64_t> * getPropertiesByType(proptype_t propertyType) const;

        /**
         * Returns true if we have at least one property matching the specified
         * property type.
         * Note that this is equivalent to (and probably faster than) the following:
         * propertySet.getPropertiesByType(propertyType) != NULL
         */
        bool hasPropertiesOfType(proptype_t propertyType) const;

        /**
         * Returns the property type of the property represented by the specified
         * unique ID. If the property is not present within the province, this returns
         * Properties::Types::INVALID.
         */
        proptype_t getPropertyType(uint64_t uniqueId) const;


        // ============================== Network/other methods ==================================

        /**
         * Writes all property data to the provided message. This is helpful for saving,
         * and for sending initial data to clients.
         */
        void writeTo(simplenetwork::Message & msg, bool isSaving);

        /**
         * Clears then reads all property data from the provided message. Any properties
         * that we had previously will be reset.
         * Any invalid or duplicate properties read from the message will be warned about,
         * and the rest of the message will be ignored (returning false).
         * This returns true on success, and false on failure.
         */
        bool readFrom(simplenetwork::Message & msg);

        /**
         * Does the same as the normal readFrom method, except that this one accumulates
         * the effects of the stat modifiers from all read properties, to the provided
         * one (if successful).
         */
        bool readFrom(simplenetwork::Message & msg, ProvStatModSet & statModifier);

        /**
         * Writes a per turn update, along with additional misc updates afterwards.
         * This should be called each turn, if needsPerTurnUpdate returns true.
         * The parts of this which are not misc updates, should be read by readPerTurnUpdate.
         *
         * Note that unless needsPerTurnUpdate returns true, there is no point calling this.
         */
        void writePerTurnUpdate(simplenetwork::Message & msg);


        // ============================== Accessor methods =======================================

        /**
         * Returns the total number of properties we have.
         */
        inline size_t getPropertyCount(void) const {
            return propertiesByUniqueId.size();
        }

        /**
         * Returns the total number of distinct property types which we have.
         * Note that this is returned as a proptype_t, since we cannot have
         * more property types than properties which exist.
         */
        inline proptype_t getDistinctPropertyTypeCount(void) const {
            return (proptype_t)propertiesByType.size();
        }

        /**
         * Returns whether we actually need to run a per turn update on any of our
         * properties.
         */
        inline bool needsPerTurnUpdate(void) const {
            return !needUpdatingIds.empty();
        }

        /**
         * Allows access to the map of properties - helpful for iterating through the
         * properties in a province.
         * To access information about each one, use the unique ID (first in map pairs)
         * and one of our Properties wrapper methods, like getFriendlyName.
         * Note: To use Properties methods directly, access the non-const
         *       TypeAndStorage related to the unique ID, using the getPropertyStorage
         *       or getPropertyStorageUnchecked methods.
         */
        inline const std::unordered_map<uint64_t, TypeAndStorage> & getProperties(void) const {
            return propertiesByUniqueId;
        }

        /**
         * Allows const access to our map of properties by type - helpful for iterating
         * through all of the property TYPES in a province.
         */
        inline const std::unordered_map<proptype_t, std::unordered_set<uint64_t>> & getPropertiesByTypeMap(void) const {
            return propertiesByType;
        }

        // =================== Wrappers for data access methods from Properties ==================
        // Note that all of these methods are undefined for unknown property unique IDs.

        /**
         * Gets the stat modifier associated with the property of specified unique ID.
         * See Properties::getStatModifierSet.
         *
         * Note that this is only defined behaviour if the property definitely exists,
         * (for example: propertyUniqueIdExists returned true, addProperty/addPropertyFromMessage
         * succeeded, or a unique ID returned by an accessor method such as getPropertiesByType).
         */
        void getStatModifierSet(uint64_t uniqueId, ProvStatModSet & statModifier);

        /**
         * Gets the friendly name associated with the property of specified unique ID.
         * See Properties::getFriendlyName.
         *
         * Note that this is only defined behaviour if the property definitely exists,
         * (for example: propertyUniqueIdExists returned true, addProperty/addPropertyFromMessage
         * succeeded, or a unique ID returned by an accessor method such as getPropertiesByType).
         */
        std::string getFriendlyName(uint64_t uniqueId);

        /**
         * Gets the description associated with the property of specified unique ID.
         * See Properties::getDescription.
         *
         * Note that this is only defined behaviour if the property definitely exists,
         * (for example: propertyUniqueIdExists returned true, addProperty/addPropertyFromMessage
         * succeeded, or a unique ID returned by an accessor method such as getPropertiesByType).
         */
        std::string getDescription(uint64_t uniqueId);

        /**
         * Gets whether the property of specified unique ID needs updating each turn.
         * See Properties::needsUpdating.
         *
         * Note that this is only defined behaviour if the property definitely exists,
         * (for example: propertyUniqueIdExists returned true, addProperty/addPropertyFromMessage
         * succeeded, or a unique ID returned by an accessor method such as getPropertiesByType).
         */
        bool needsUpdating(uint64_t uniqueId);

        /**
         * Gets the icon ID associated with the property of specified unique ID.
         * See Properties::getIconId.
         *
         * Note that this is only defined behaviour if the property definitely exists,
         * (for example: propertyUniqueIdExists returned true, addProperty/addPropertyFromMessage
         * succeeded, or a unique ID returned by an accessor method such as getPropertiesByType).
         */
        unsigned int getIconId(uint64_t uniqueId);

        /**
         * Writes all appropriate terrain display layers, in order to provide a pictorial
         * display of the property of specified unique ID.
         * See Properties::addTerrDispLayers.
         *
         * Note that this is only defined behaviour if the property definitely exists,
         * (for example: propertyUniqueIdExists returned true, addProperty/addPropertyFromMessage
         * succeeded, or a unique ID returned by an accessor method such as getPropertiesByType).
         */
        void addTerrDispLayers(uint64_t uniqueId, std::vector<TerDispLayer> & layersVec);
    };
}

#endif

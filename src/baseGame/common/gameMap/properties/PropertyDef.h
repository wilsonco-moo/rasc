/*
 * PropertyDef.h
 *
 *  Created on: 9 Jun 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROPERTIES_PROPERTYDEF_H_
#define BASEGAME_COMMON_GAMEMAP_PROPERTIES_PROPERTYDEF_H_

#include <cstdint>

#include "../provinceUtil/ProvinceStatTypes.h"
#include "../properties/trait/TraitDef.h"

namespace rasc {

    /**
     * This is a minimal definition header file for the properties system. This
     * should be #included in the header files of classes which use these types.
     * This contains only the type definitions and macros for the property system.
     *
     * For the enums and other methods required for the property system, see
     * Properties.h.
     */


    /**
     * This typedef and associated macros should be used to represent
     * and read/write property type IDs.
     * Note that this type should be expanded if the number of property types
     * defined in RASC_PROPERTIES_LIST ever exceeds 254, (two additional
     * IDs are used for special values COUNT and INVALID).
     */
    using proptype_t = uint8_t;
    #define PROPERTY_TYPE_WRITE(msg, propertyTypeId) (msg).write8(propertyTypeId)
    #define PROPERTY_TYPE_READ(msg) (msg).read8()


    /**
     * This typedef and associated macros should be used to represent
     * and read/write property base type IDs.
     * Note that this is primarily intended to be only used internally within
     * Properties, for identifying base types.
     * Note that this type should be expanded if the number of base property
     * types defined in RASC_PROPERTIES_LIST ever exceeds 254, (two
     * additional IDs are used for special values COUNT and INVALID).
     */
    using propbasetype_t = uint8_t;
    #define PROPERTY_BASE_TYPE_WRITE(msg, propertyBaseTypeId) (msg).write8(propertyBaseTypeId)
    #define PROPERTY_BASE_TYPE_READ(msg) (msg).read8()


    /**
     * This union should be used to store the contents of each property. This
     * allows properties to either be their own unique instances, pointers to static
     * instances, or single values - all depending on the property type.
     */
    union PropertyStorage {
        void * ptr;
        provstat_t value;
    };


    /**
     * This list defines all properties, alongside their base property types.
     * This is amalgamated from lists defined in other header files, for
     * specific types of property (such as Traits.h).
     */
    #define RASC_PROPERTIES_LIST                                       \
                                                                       \
        /* Traits are a kind or property. This is defined           */ \
        /* in TraitDef.h.                                           */ \
        RASC_TRAITS_LIST


    /**
     * This defines each property base type, alongside the expressions required
     * to use it. The following expressions must be defined, each able to (optionally)
     * use the specified variables, and each returning the specified type.
     *
     * Note that createExpr, getStatModifierSetExpr, getFriendlyNameExpr,
     * getDescriptionExpr, needsUpdatingExpr and getIconExpr are mandatory.
     * Other expressions optionally may not be needed for simple properties.
     *
     * Variables available to each expression (types and names):
     *
     * propbasetype_t baseType          This is always available, but shouldn't need to be used (so isn't included as any parameter).
     * CommonBox & commonBox            The common box, allowing the property access to outside context.
     * Province & province              The province into which the property is being added.
     * simplenetwork::Message & msg     A message to either write to or read from (depending on function).
     * bool isSaving                    A parameter used only in writeInitialDataExpr.
     * proptype_t propertyType          The type ID of the relevant property.
     * PropertyStorage & property       The property itself (its storage). This can be modified (helpful for update methods).
     *
     * Expressions are described in detail below:
     *
     *  > PropertyStorage createExpr(commonBox, province, msg, propertyType)
     *    - This should create a property (as a PropertyStorage), from the
     *      message and property type.
     *    - The message contents should be in the same format as is written by
     *      writeInitialDataExpr.
     *    - For simple properties which do not need anything to be stored (like
     *      non-stackable traits), return a default constructed storage union
     *      here, i.e: "PropertyStorage()".
     *    - The message may not be needed for simple properties.
     *    - The type may not be needed where all properties of a type are stored
     *      identically.
     *  > ProvStatModSet getStatModifierSetExpr(commonBox, province, propertyType, property)
     *    - This should return a stat modifier set imparted by the specified
     *      property (as type and storage).
     *  > std::string getFriendlyNameExpr(commonBox, province, propertyType, property)
     *    - This should generate a (user-facing) friendly name, intended to be used in
     *      the user interface to represent the property. Note that this is different
     *      to the internal name, which is generated from the name of the property
     *      type using macros, and can be converted back to a type ID.
     *  > std::string getDescriptionExpr(commonBox, province, propertyType, property)
     *    - This should return longer description, intended to give more information
     *      to users about the property. Where this is not relevant, this could just
     *      return the same as getFriendlyNameExpr.
     *  > bool needsUpdatingExpr(commonBox, province, propertyType, property)
     *    - This should return true or false depending whether the property
     *      (as type and storage) needs to be updated each turn.
     *  > unsigned int getIconIdExpr(commonBox, province, propertyType, property)
     *    - This should return the icon ID (defined by IconDef) which best represents
     *      this property.
     *    - This (along with getFriendlyNameExpr) is used when displaying properties
     *      concisely to users.
     *  > void addTerrDispLayersExpr(commonBox, province, layersVec, propertyType, property)
     *    - This should write all appropriate terrain display layers, in order to provide
     *      a pictorial display of this property, (see TerrainDisplayDef.h).
     *    - This is used to generate terrain display images for provinces.
     *  > void writeUpdateExpr(commonBox, province, msg, propertyType, property)
     *    - This should run an update on the property, writing any changes to
     *      the message. Note that changes should not be applied locally, ONLY
     *      written to the message. Changes should be applied by receiveUpdate.
     *    - This is called each turn for properties which report that they need
     *      to be updated.
     *  > void writeMiscUpdateExpr(commonBox, province, msg, propertyType, property)
     *    - This should write any appropriate misc updates to the message
     *      (or none). This is helpful if the property wants to affect things
     *      outside of itself.
     *    - This is called each turn for properties which report that they need
     *      to be updated, (after all calls to writeUpdate).
     *  > void receiveUpdateExpr(commonBox, province, msg, propertyType, property)
     *    - This should read and apply updates, written by writeUpdate.
     *    - Note that this MUST read ALL of the data written by writeUpdate.
     *  > void receiveDirectUpdateExpr(commonBox, province, msg, propertyType, property)
     *    - This should receive direct update messages, written to modify the
     *      of the property.
     *    - This provides a property specific way for the server to make modifications
     *      to properties, without removing and re-creating them - for example, changing
     *      the count of a stackable trait.
     *  > void writeInitialDataExpr(commonBox, province, msg, isSaving, propertyType, property)
     *    - This should write all the data contained within the property, in a
     *      format readable by createExpr.
     *    - The parameter isSaving specifies whether initial data is being written
     *      for a save operation, or to send to clients. This allows server-side
     *      properties to contain additional state data, not needed by clients.
     *  > void deleteExpr(commonBox, province, propertyType, property)
     *    - This should erase any allocations or other state associated with the
     *      specified property.
     *    - This is used during cleanup, and when properties are removed.
     *
     * Expression list (for reference):
     *
     * createExpr, getStatModifierSetExpr, getFriendlyNameExpr, getDescriptionExpr,
     * needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr, writeUpdateExpr,
     * writeMiscUpdateExpr, receiveUpdateExpr, receiveDirectUpdateExpr,
     * writeInitialDataExpr, deleteExpr
     */
    #define RASC_PROPERTY_BASE_TYPES_LIST                                                       \
                                                                                                \
        /* Stackable or non stackable traits use the method from Traits to be created,       */ \
        /* stat modifier set from Traits, and do not require updating or any other           */ \
        /* behaviour.                                                                        */ \
        X(trait,                                                                                \
          Traits::propertyCreate(msg),                                                          \
          Traits::propertyGetStatModifierSet(propertyType, property),                           \
          Traits::propertyGetFriendlyName(propertyType, property),                              \
          Traits::propertyGetDescription(propertyType, property), false,                        \
          Traits::propertyGetIconId(propertyType),                                              \
          Traits::propertyAddTerrDispLayers(layersVec, propertyType), , , ,                     \
          Traits::propertyReceiveDirectUpdate(msg, property),                                   \
          Traits::propertyWriteInitialData(msg, property), )
}

#endif

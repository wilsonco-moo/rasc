/*
 * ProvinceProperties.cpp
 *
 *  Created on: 23 Sep 2019
 *      Author: wilson
 */

 #include "ProvinceProperties.h"

#include <tinyxml2/tinyxml2.h>
#include <functional>
#include <algorithm>
#include <iostream>

#include "../../util/numeric/UniqueIdAssigner.h"
#include "../../stats/UnifiedStatSystem.h"
#include "../mapElement/types/Province.h"
#include "../../playerData/PlayerData.h"
#include "../../stats/types/AreasStat.h"
#include "../properties/Properties.h"
#include "../../util/CommonBox.h"
#include "../../util/XMLUtil.h"
#include "trait/Traits.h"
#include "../GameMap.h"

namespace rasc {

    // ------------------ Province stat class ---------------------

    ProvinceStat::ProvinceStat(provstat_t baseValue) :
        baseValue(baseValue),
        finalValue(0),
        accStatModifiers(0, 0) {
    }
    void ProvinceStat::updateFinalValue(void) {
        finalValue = accStatModifiers * baseValue;
    }
    void ProvinceStat::resetAccStatModifiers(void) {
        accStatModifiers = ProvStatMod(0, 0);
    }
    provstat_t ProvinceStat::calculateProspective(const ProvStatMod & modifier) const {
        return (accStatModifiers + modifier) * baseValue;
    }
    provstat_t ProvinceStat::calculateProspectiveChange(const ProvStatMod & modifier) const {
        return accStatModifiers.calculateProspectiveChange(modifier, baseValue);
    }

    // --------------- Main province properties class -------------

    ProvinceProperties::ProvinceProperties(Province & province, UniqueIdAssigner * uniqueIdAssigner) :
        RascUpdatable(),
        uniqueIdAssigner(uniqueIdAssigner),

        // Define the ProvinceStat instances using the X macro definition of the province stats.
        #define X(name, isGoodStat, baseProvinceValue) ProvinceStat(baseProvinceValue),
        stats{ RASC_PROVINCE_STATS },
        #undef X

        propertySet(province.getGameMap().getCommonBox(), province),
        province(province),
        typeProperty(UniqueIdAssigner::INVALID_ID),
        isAccessible(false) {
    }

    ProvinceProperties::~ProvinceProperties(void) {
    }

    void ProvinceProperties::resetAll(void) {
        // Reset the accumulated stat modifiers for all of our stats.
        for (ProvinceStat & stat : stats) {
            stat.resetAccStatModifiers();
        }
    }

    void ProvinceProperties::updateAccessibleStatus(void) {
        // Get the type property, complain if it does not exist (somehow).
        PropertySet::TypeAndStorage * property = propertySet.getPropertyStorage(typeProperty);
        if (property == NULL) {
            std::cerr << "WARNING: ProvinceProperties: " << "updateAccessibleStatus: "
                      << "province type property (" << typeProperty << ") requested but does not exist. It either has not been loaded yet, or has been removed.\n";
            isAccessible = false;

        // Set the accessible status from the method in Traits.
        } else {
            isAccessible = Traits::isTraitAccessible(property->getType());
        }
    }

    void ProvinceProperties::addStatModifierSet(const ProvStatModSet & modifierSet) {
        for (const std::pair<const ProvStatId, ProvStatMod> & modifier : modifierSet.getPerStatModifiers()) {
            stats[modifier.first] += modifier.second;
        }
    }

    void ProvinceProperties::subtractStatModifierSet(const ProvStatModSet & modifierSet) {
        for (const std::pair<const ProvStatId, ProvStatMod> & modifier : modifierSet.getPerStatModifiers()) {
            stats[modifier.first] -= modifier.second;
        }
    }

    void ProvinceProperties::updateAllStats(bool notifyStatSystem) {
        if (notifyStatSystem) {
            // If we do have to notify the stat system,
            // FIRST take a copy of the current stat FINAL VALUES.
            StatArr statChange = getStatArray();

            // THEN, update the final values.
            for (ProvinceStat & stat : stats) {
                stat.updateFinalValue();
            }
            // FINALLY, work out the difference, noting if anything changed.
            bool difference = false;
            for (int i = 0; i < ProvStatIds::COUNT; i++) {
                provstat_t newValue = stats[i],
                           oldValue = statChange[i];
                statChange[i] = newValue - oldValue;
                if (newValue != oldValue) {
                    difference = true;
                }
            }
            // If at least something *has* changed, notify the unified stat system.
            if (difference) {
                province.getGameMap().getCommonBox().statSystem->onChangeStatsInProvince(province, statChange);
            }
        } else {
            // If we don't have to notify the stat system, all that needs doing is updating of stat final values.
            for (ProvinceStat & stat : stats) {
                stat.updateFinalValue();
            }
        }
    }

    void ProvinceProperties::writeMiscUpdateType(simplenetwork::Message & msg, PropertiesMiscUpdateType update) const { msg.write8(update); }
    ProvinceProperties::PropertiesMiscUpdateType ProvinceProperties::readMiscUpdateType(simplenetwork::Message & msg) const { return msg.read8(); }
    void ProvinceProperties::writeCheckedUpdateType(simplenetwork::Message & msg, PropertiesCheckedUpdateType update) const { msg.write8(update); }
    ProvinceProperties::PropertiesCheckedUpdateType ProvinceProperties::readCheckedUpdateType(simplenetwork::Message & msg) const { return msg.read8(); }

    bool ProvinceProperties::onReceiveMiscUpdate(simplenetwork::Message & msg) {
        bool ret = true;
        PropertiesMiscUpdateType type = readMiscUpdateType(msg);

        switch(type) {
        case PropertiesMiscUpdateTypes::propertyCreate:
            ret = onPropertyCreate(msg);
            break;
        case PropertiesMiscUpdateTypes::propertyDirectUpdate:
            ret = onPropertyDirectUpdate(msg);
            break;
        case PropertiesMiscUpdateTypes::propertyPerTurnUpdate:
            ret = onPropertyPerTurnUpdate(msg);
            break;
        case PropertiesMiscUpdateTypes::propertyDelete:
            ret = onPropertyDelete(msg);
            break;
        default:
            std::cerr << "WARNING: ProvinceProperties: " << "unknown misc update type: " << (unsigned int)type << ".\n";
            return false;
        }
        province.runOnProvinceChange();  // Run this since a change to the province's properties
        return ret;                      // counts as a change to the province itself.
    }

    bool ProvinceProperties::onReceiveCheckedUpdate(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) {
        PropertiesCheckedUpdateType type = readCheckedUpdateType(checkedUpdate);
        switch(type) {
        case PropertiesCheckedUpdateTypes::requestConstructBuildings:
            onRequestConstructBuildings(checkedUpdate, miscUpdate, contextPlayer);
            break;
        default:
            std::cerr << "WARNING: ProvinceProperties: " << "unknown checked update type: " << (unsigned int)type << ".\n";
            return false;
        }
        return true;
    }

    void ProvinceProperties::onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) {
        writeUpdateHeader(msg);
        // Simply save the property set, and the unique ID of the terrain type property.
        propertySet.writeTo(msg, isSaving);
        msg.write64(typeProperty);
    }

    bool ProvinceProperties::onReceiveInitialData(simplenetwork::Message & msg) {
        // Before receiving initial data, reset any data that we already had.
        resetAll();

        // Read the property set from the message, giving up if doing so failed.
        ProvStatModSet overallModifier;
        if (!propertySet.readFrom(msg, overallModifier)) {
            return false;
        }

        // Read the unique ID of the terrain type property. This should now exist,
        // since we have read in the property set.
        typeProperty = msg.read64();

        // Run this since we just assigned our province type.
        updateAccessibleStatus();

        // Since we have reset all of our stat modifiers, add the overall stat modifier
        // provided to us by PropertySet::readFrom.
        addStatModifierSet(overallModifier);

        // Update all stats since we have changed some traits.
        updateAllStats(false); // Don't notify the stat system, since this change happened while reading initial data.

        province.runOnProvinceChange(); // Run this since we got an update.
        return true;
    }
    
    void ProvinceProperties::loadFromXML(tinyxml2::XMLElement * element) {
        // Manually load traits (and terrain type) from the XML file. This must only
        // be done on the server-side, i.e: if we have a unique ID assigner. Note that
        // unless the map XML format is updated, the map XML file only supports very
        // basic traits. Assume that anything defined by the map XML file is a trait with
        // a value of one.
        ProvStatModSet newStatModifier;
        if (uniqueIdAssigner != NULL) {

            // Loop through each trait name specified by the XML file.
            tinyxml2::XMLElement * traitsElement = readElement(element, "traits");
            loopElements(traitElement, traitsElement, "trait") {
                std::string traitName = readAttribute(traitElement, "name");

                // Get the property type, complain and skip if it is invalid, not a trait, or a terrain type.
                proptype_t propertyType = Properties::internalNameToType(traitName);
                if (!Properties::isValidProperty(propertyType) ||
                    Properties::getBaseType(propertyType) != Properties::BaseTypes::trait ||
                    Traits::getTraitDisplayMode(propertyType) == TraitDef::DisplayMode::terrainType) {
                    std::cerr << "WARNING: ProvinceProperties: " << "terrain type, non-trait property or unknown property found when loading traits from XML file: [" << traitName << "].\n";
                    continue;
                }

                // Complain and skip if we already have this trait.
                if (propertySet.hasPropertiesOfType(propertyType)) {
                    std::cerr << "WARNING: ProvinceProperties: " << "trait [" << traitName << "] listed multiple times for one province, in XML file.\n";
                    continue;
                }

                // Manually generate a unique ID and add the trait to our property set.
                // If successful, add the stat modifier set from this new property.
                uint64_t uniqueId = uniqueIdAssigner->newId();
                if (propertySet.addProperty(uniqueId, propertyType, Traits::propertyCreate(1))) {
                    propertySet.getStatModifierSet(uniqueId, newStatModifier);
                    addStatModifierSet(newStatModifier);
                }
            }



            // Load the terrain type. Substitute it for "plains" if it is invalid.
            tinyxml2::XMLElement * propertiesElement = readElement(element, "properties");
            std::string terrainTypeName = readAttribute(propertiesElement, "type");

            // Get the property type, complain and use plains if it is invalid,
            // not a trait, or a not a terrain type trait.
            proptype_t propertyType = Properties::internalNameToType(terrainTypeName);
            if (!Properties::isValidProperty(propertyType) ||
                Properties::getBaseType(propertyType) != Properties::BaseTypes::trait ||
                Traits::getTraitDisplayMode(propertyType) != TraitDef::DisplayMode::terrainType) {
                std::cerr << "WARNING: ProvinceProperties: " << "unknown terrain type found when reading terrain type: [" << terrainTypeName << "]. Using \"plains\" terrain type instead.\n";
                propertyType = Properties::Types::plains;
            }

            // Manually generate a unique ID and add the terrain type to our property set.
            // If successful, add the stat modifier set from this new property.
            uint64_t uniqueId = uniqueIdAssigner->newId();
            if (propertySet.addProperty(uniqueId, propertyType, Traits::propertyCreate(1))) {
                propertySet.getStatModifierSet(uniqueId, newStatModifier);
                addStatModifierSet(newStatModifier);
                // Save the unique ID to typeProperty only if successful.
                typeProperty = uniqueId;
            }
            // Run this since we just assigned our province type.
            updateAccessibleStatus();
        }

        // ... And finally, update all the stats.
        updateAllStats(false); // Don't notify stat system, since this happens during initialisation.
    }

    // --------------------------------- Accessor -------------------------------------

    std::string ProvinceProperties::getProvinceTypeName(void) const {
        // Get the type property, complain if it does not exist (somehow).
        const PropertySet::TypeAndStorage * property = propertySet.getPropertyStorage(typeProperty);
        if (property == NULL) {
            std::cerr << "WARNING: ProvinceProperties: " << "getProvinceTypeName: "
                      << "province type property (" << typeProperty << ") requested but does not exist. It either has not been loaded yet, or has been removed.\n";
            return "";

        // Query the friendly name using the method from traits.
        } else {
            return Traits::getFriendlyName(property->getType());
        }
    }

    StatArr ProvinceProperties::getStatArray(void) const {
        StatArr output;
        for (int i = 0; i < ProvStatIds::COUNT; i++) {
            output[i] = stats[i];
        }
        return output;
    }

    ProvStatModSet ProvinceProperties::getAccumulatedStatModifierSet(void) const {
        std::unordered_map<ProvStatId, ProvStatMod> output;
        for (int i = 0; i < ProvStatIds::COUNT; i++) {
            const ProvStatMod & mod = stats[i].getAccumulatedStatModifiers();
            // Ignore stat modifiers which are zero - they don't need to be in the stat modifier set.
            if (!mod.isZero()) {
                output.emplace(i, mod);
            }
        }
        return ProvStatModSet(output);
    }

    void ProvinceProperties::calculateValueForMoney(std::array<float, BuildValuedStatIds::COUNT> & valueArr, const ProvStatModSet & modifier, provstat_t overallCost, bool doesOwnEntireArea) const {
        // If building costs nothing, we cannot work out its value. So set values to zero and give up.
        if (overallCost == 0) {
            for (float & val : valueArr) { val = 0.0f; }
            return;
        }

        // Change in value of each stat is prospective change from appropriate stat modifier from set.
        // Make sure we apply the areas stat multiplier.
        #define Y(statName)                                                                 \
            (AreasStat::statName##ApplyMultiplier(                                          \
               stats[ProvStatIds::statName].calculateProspectiveChange(                     \
                 modifier.getProvStatMod(ProvStatIds::statName)),                           \
               doesOwnEntireArea))

        // Set value to calculation per overall cost, for each building valued stat.
        #define X(statName, calculation)                                                    \
            valueArr[BuildValuedStatIds::statName] = (float)(calculation) / overallCost;
        RASC_BUILDING_VALUED_PROVINCE_STATS
        #undef X
        #undef Y
    }

    bool ProvinceProperties::doesModifierExceedLand(const ProvStatModSet & modifier) const {
        // If it has a positive VALUE effect on the land use:
        if (modifier.getProvStatModValue(ProvStatIds::landUsed) > 0) {
            // Find prospective land use and availability, after adding this building. If construction
            // causes land use to be more than is available, reject construction.
            provstat_t prospUse   = stats[ProvStatIds::landUsed].calculateProspective(modifier.getProvStatMod(ProvStatIds::landUsed)),
                       prospAvail = stats[ProvStatIds::landAvailable].calculateProspective(modifier.getProvStatMod(ProvStatIds::landAvailable));
            if (prospUse > prospAvail) return true;
        }
        return false;
    }

    bool ProvinceProperties::doesLandUseModifierExceedLand(const ProvStatMod & landUseModifier) const {
        // If it has a positive VALUE effect on the land use:
        if (landUseModifier.getValue() > 0) {
            provstat_t prospUse = stats[ProvStatIds::landUsed].calculateProspective(landUseModifier);
            if (prospUse > stats[ProvStatIds::landAvailable]) return true;
        }
        return false;
    }

    // ------------------------ Basic property misc updates ---------------------------

    uint64_t ProvinceProperties::writePropertyCreateHeader(simplenetwork::Message & msg, proptype_t propertyType) {
        // Write header/update type, allocate unique ID, write it (and type) to message, return ID.
        writeUpdateHeader(msg);
        writeMiscUpdateType(msg, PropertiesMiscUpdateTypes::propertyCreate);
        uint64_t uniqueId = uniqueIdAssigner->newId();
        Properties::writeTypeAndId(msg, propertyType, uniqueId);
        return uniqueId;
    }
    bool ProvinceProperties::onPropertyCreate(simplenetwork::Message & msg) {
        // Add the property to the set, give up if that failed.
        ProvStatModSet statModifier;
        uint64_t uniqueId;
        if (!propertySet.addPropertyFromMessage(msg, statModifier, uniqueId)) {
            return false;
        }

        // Notify the stat system that a property has been added, AFTER adding it to the property set.
        province.getGameMap().getCommonBox().statSystem->onCreatePropertyInProvince(province, uniqueId);

        // Add the stat modifier imparted by this property.
        addStatModifierSet(statModifier);

        // Update all stats since a property has changed.
        updateAllStats(true); // Notify the stat system since this change happened during the game.
        return true;
    }


    void ProvinceProperties::writePropertyDirectUpdateHeader(simplenetwork::Message & msg, uint64_t uniqueId) {
        writeUpdateHeader(msg);
        writeMiscUpdateType(msg, PropertiesMiscUpdateTypes::propertyDirectUpdate);
        msg.write64(uniqueId);
    }
    bool ProvinceProperties::onPropertyDirectUpdate(simplenetwork::Message & msg) {
        // Read the direct update, give up if that failed.
        ProvStatModSet addedStatModifier;
        if (!propertySet.readDirectUpdate(msg, addedStatModifier)) {
            return false;
        }

        // Add the stat modifier representing the ADDED stat modifier CHANGE resulting from this direct update.
        addStatModifierSet(addedStatModifier);

        // Update all stats since a property has changed.
        updateAllStats(true); // Notify the stat system since this change happened during the game.
        return true;
    }


    void ProvinceProperties::onGenerateTurnMiscUpdate(MiscUpdate update) {
        // There is no point running a turn misc update if no properties
        // actually need to be updated.
        if (propertySet.needsPerTurnUpdate()) {
            writeUpdateHeader(update.msg());
            writeMiscUpdateType(update.msg(), PropertiesMiscUpdateTypes::propertyPerTurnUpdate);
            propertySet.writePerTurnUpdate(update.msg());
        }
    }
    bool ProvinceProperties::onPropertyPerTurnUpdate(simplenetwork::Message & msg) {
        // Read the per-turn update, give up if that failed.
        ProvStatModSet addedStatModifier;
        if (!propertySet.readPerTurnUpdate(msg, addedStatModifier)) {
            return false;
        }

        // Add the stat modifier representing the ADDED stat modifier CHANGE resulting from
        // the cumulative effect of this per-turn update.
        addStatModifierSet(addedStatModifier);

        // Update all stats since properties have likely changed.
        updateAllStats(true); // Notify the stat system since this change happened during the game.
        return true;
    }


    void ProvinceProperties::deleteProperty(MiscUpdate update, uint64_t uniqueId) {
        writeUpdateHeader(update.msg());
        writeMiscUpdateType(update.msg(), PropertiesMiscUpdateTypes::propertyDelete);
        update.msg().write64(uniqueId);
    }
    bool ProvinceProperties::onPropertyDelete(simplenetwork::Message & msg) {
        // Read unique ID and notify the stat system, BEFORE removing the property from the PropertySet.
        uint64_t uniqueId = msg.read64();
        province.getGameMap().getCommonBox().statSystem->onDeletePropertyFromProvince(province, uniqueId);

        // Delete the property from the set, give up if that failed.
        ProvStatModSet subtractedStatModifier;
        if (!propertySet.deleteProperty(uniqueId, subtractedStatModifier)) {
            return false;
        }

        // Subtract the stat modifier representing the SUBTRACTED stat modifier CHANGE resulting
        // from deleting the property: i.e: the stat modifier which used to be imparted by this property.
        subtractStatModifierSet(subtractedStatModifier);

        // Update all stats since a property has changed.
        updateAllStats(true); // Notify the stat system since this change happened during the game.
        return true;
    }


    // ------------------------- Wrapper methods for traits ---------------------------

    provstat_t ProvinceProperties::getTraitCount(proptype_t propertyType) const {
        // If the property does not exist within our property set, return 0.
        const std::unordered_set<uint64_t> * matchingProperties = propertySet.getPropertiesByType(propertyType);
        if (matchingProperties == NULL) {
            return 0;

        // Otherwise pick the first matching property (assume that trait properties are not duplicated).
        // Use the method from Traits to extract the trait cound from the storage of this property.
        } else {
            uint64_t uniqueId = *matchingProperties->begin();
            return Traits::propertyGetTraitCount(propertySet.getPropertyStorageUnchecked(uniqueId).getStorage());
        }
    }

    uint64_t ProvinceProperties::changeTraitCount(MiscUpdate update, proptype_t propertyType, provstat_t count) {
        // If the trait (property) does not exist already, and the specified count
        // is non-zero (i.e: there is something for us to change):
        const std::unordered_set<uint64_t> * matchingProperties = propertySet.getPropertiesByType(propertyType);
        if (matchingProperties == NULL) {
            // Return invalid ID if there already wasn't a property, and we didn't do anything.
            if (count == 0) {
                return UniqueIdAssigner::INVALID_ID;

            // Otherwise, write a "create header", then the "create content" for a trait.
            // Return the unique ID of the property we just created.
            } else {
                uint64_t uniqueId = writePropertyCreateHeader(update.msg(), propertyType);
                Traits::propertyWriteCreateContent(update.msg(), count);
                return uniqueId;
            }

        // If the trait *does* already exist, get its ID from the set (trait properties ought not be duplicated).
        } else {
            uint64_t uniqueId = *matchingProperties->begin();
            // If the count is zero, send a message to delete the property (use client misc update so we don't apply it immediately).
            if (count == 0) {
                deleteProperty(MiscUpdate::client(update.msg()), uniqueId);

            // Otherwise, if the count is different to the one which is there already, write a direct
            // update to change the trait count.
            } else if (count != Traits::propertyGetTraitCount(propertySet.getPropertyStorageUnchecked(uniqueId).getStorage())) {
                writePropertyDirectUpdateHeader(update.msg(), uniqueId);
                Traits::propertyWriteDirectUpdate(update.msg(), count);
            }

            // Return the ID of the property that we either deleted, updated or didn't update,
            // since it already existed when the call was run.
            return uniqueId;
        }
    }

    uint64_t ProvinceProperties::addTrait(MiscUpdate update, proptype_t trait) {
        return changeTraitCount(MiscUpdate::client(update.msg()), trait, 1);
    }

    uint64_t ProvinceProperties::removeTrait(MiscUpdate update, proptype_t trait) {
        return changeTraitCount(MiscUpdate::client(update.msg()), trait, 0);
    }

    // Used internally within getSortedTraits and getSortedTraitsByDisplayMode.
    // Sorts pairs of property and count by property ID.
    static bool comparePropertyPairs(const std::pair<proptype_t, provstat_t> & pair1, const std::pair<proptype_t, provstat_t> & pair2) {
        return pair1.first < pair2.first;
    }

    void ProvinceProperties::getSortedTraits(std::vector<std::pair<proptype_t, provstat_t>> & traitsVector) const {
        traitsVector.clear();
        // Add all the properties, which happen to be traits, to the vector.
        for (const std::pair<const uint64_t, PropertySet::TypeAndStorage> & pair : propertySet.getProperties()) {
            if (Properties::getBaseType(pair.second.getType()) == Properties::BaseTypes::trait) {
                traitsVector.emplace_back(pair.second.getType(), Traits::propertyGetTraitCount(pair.second.getStorage()));
            }
        }
        std::sort(traitsVector.begin(), traitsVector.end(), &comparePropertyPairs);
    }

    void ProvinceProperties::getSortedTraitsByDisplayMode(std::vector<std::pair<proptype_t, provstat_t>> & traitsVector, TraitDef::DisplayMode displayMode) const {
        traitsVector.clear();
        // Add all the properties, which happen to be traits, and happen to have the
        // correct display mode, to the vector.
        for (const std::pair<const uint64_t, PropertySet::TypeAndStorage> & pair : propertySet.getProperties()) {
            if (Properties::getBaseType(pair.second.getType()) == Properties::BaseTypes::trait &&
                Traits::getTraitDisplayMode(pair.second.getType()) == displayMode) {
                traitsVector.emplace_back(pair.second.getType(), Traits::propertyGetTraitCount(pair.second.getStorage()));
            }
        }
        std::sort(traitsVector.begin(), traitsVector.end(), &comparePropertyPairs);
    }


    // --------------------------- Checked update methods -----------------------------

    void ProvinceProperties::requestConstructBuildings(CheckedUpdate update, proptype_t propertyType, provstat_t count) {
        writeUpdateHeader(update.msg());
        writeCheckedUpdateType(update.msg(), PropertiesCheckedUpdateTypes::requestConstructBuildings);
        PROPERTY_TYPE_WRITE(update.msg(), propertyType);
        PROVINCE_STAT_WRITE(update.msg(), count);
    }

    void ProvinceProperties::onRequestConstructBuildings(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) {
        // Read the property type and count (whole message) before doing any checks.
        proptype_t propertyType = PROPERTY_TYPE_READ(checkedUpdate);
        provstat_t count = PROVINCE_STAT_READ(checkedUpdate);

        // If the property is invalid, the property isn't a trait, the player does not own the province,
        // they have finished their turn, or the count is zero (do nothing), then block the request.
        if (!Properties::isValidProperty(propertyType) ||
            Properties::getBaseType(propertyType) != Properties::BaseTypes::trait ||
            province.getProvinceOwner() != contextPlayer->getId() ||
            contextPlayer->getHaveFinishedTurn() ||
            count == 0) return;

        // Work out the build cost for the specified count, record any errors (from overflow).
        bool error = false;
        PlayerStatArr buildCost = Traits::getTraitBuildingCost(&error, propertyType, count);

        // If the player requested a building count which caused overflow, the player
        // can't afford the building or the requested number of these traits cannot be built,
        // then block the request.
        if (error ||
            !contextPlayer->canAffordBuilding(buildCost) ||
            !Traits::canTraitBeBuilt(province, propertyType, count)) return;

        // If everything has been allowed, then write the misc update to change the trait count.
        changeTraitCount(MiscUpdate::client(miscUpdate), propertyType, getTraitCount(propertyType) + count);
        contextPlayer->spendBuildingCost(MiscUpdate::client(miscUpdate), buildCost);
    }
}

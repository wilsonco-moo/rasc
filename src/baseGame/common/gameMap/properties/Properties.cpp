/*
 * Properties.cpp
 *
 *  Created on: 8 Jun 2020
 *      Author: wilson
 */

#include "Properties.h"

#include <sockets/plus/message/Message.h>
#include <unordered_set>
#include <iostream>

#include "../../util/definitions/IconDef.h"
#include "trait/Traits.h"

namespace rasc {

    // As is stated in the documentation, if the count of properties
    // (or property base types) ever exceeds 254, then the type used
    // to store property IDs will have to be changed.
    static_assert(Properties::Types::COUNT <= 254);
    static_assert(Properties::BaseTypes::COUNT <= 254);

    // The internal name for invalid properties.
    const std::string Properties::INVALID_INTERNAL_NAME = "INVALID";

    // Define the base type, for each property.
    #define X(propertyType, baseType) Properties::BaseTypes::baseType,
    const propbasetype_t Properties::baseTypes[Properties::Types::COUNT] = { RASC_PROPERTIES_LIST };
    #undef X

    // Stores the name of all property types.
    #define X(propertyType, baseType) #propertyType,
    static const std::string propertyTypeNames[Properties::Types::COUNT] = { RASC_PROPERTIES_LIST };
    #undef X

    // Stores the name of all property base types.
    #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
              getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
              writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
              receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
            #baseType,
    static const std::string propertyBaseTypeNames[Properties::BaseTypes::COUNT] = { RASC_PROPERTY_BASE_TYPES_LIST };
    #undef X

    // A map from internal names to property type IDs.
    #define X(propertyType, baseType) { #propertyType, Properties::Types::propertyType },
    static const std::unordered_map<std::string, proptype_t> internalNameToTypeMap = { RASC_PROPERTIES_LIST };
    #undef X

    // A map from internal names to property base type IDs.
    #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
              getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
              writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
              receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
            { #baseType, Properties::BaseTypes::baseType },
    static const std::unordered_map<std::string, proptype_t> internalNameToBaseTypeMap = { RASC_PROPERTY_BASE_TYPES_LIST };
    #undef X

    /**
     * This macro assumes that X has been defined as a case statement, running or
     * returning the appropriate expression.
     * This should be used as a framework for property methods:
     * It first gets the base type, then does a switch-case based on the base type.
     * Into that, the X macro base types list is substituted.
     * In the case that the base type was not found, a warning is printed and the
     * provided return statement is used.
     */
    #define RUN_PROPERTY_METHOD(propertyMethodLogName, defaultReturnStatement)                                                              \
                                                                                                                                            \
        /* Get the base type.                                                                                                            */ \
        propbasetype_t baseType = getBaseType(propertyType);                                                                                \
                                                                                                                                            \
        /* Do a switch-case based on the base type.                                                                                      */ \
        switch(baseType) {                                                                                                                  \
            RASC_PROPERTY_BASE_TYPES_LIST                                                                                                   \
                                                                                                                                            \
        /* Complain about missing base types.                                                                                            */ \
        default:                                                                                                                            \
            std::cerr << "WARNING: Properties: " << "missing base type: " << (unsigned int)baseType << ", (" propertyMethodLogName ").\n";  \
            defaultReturnStatement;                                                                                                         \
        }


    // ============================ Convenience /accessor methods =================================

    bool Properties::isValidProperty(proptype_t propertyType) {
        return propertyType < Types::COUNT;
    }

    bool Properties::isValidPropertyBaseType(propbasetype_t baseType) {
        return baseType < BaseTypes::COUNT;
    }

    const std::string & Properties::typeToInternalName(proptype_t propertyType) {
        if (isValidProperty(propertyType)) {
            return propertyTypeNames[propertyType];
        } else {
            return INVALID_INTERNAL_NAME;
        }
    }

    const std::string & Properties::baseTypeToInternalName(propbasetype_t baseType) {
        if (isValidPropertyBaseType(baseType)) {
            return propertyBaseTypeNames[baseType];
        } else {
            return INVALID_INTERNAL_NAME;
        }
    }

    proptype_t Properties::internalNameToType(const std::string & internalName) {
        auto iter = internalNameToTypeMap.find(internalName);
        if (iter == internalNameToTypeMap.end()) {
            return Types::INVALID;
        } else {
            return iter->second;
        }
    }

    proptype_t Properties::internalNameToBaseType(const std::string & internalName) {
        auto iter = internalNameToBaseTypeMap.find(internalName);
        if (iter == internalNameToBaseTypeMap.end()) {
            return BaseTypes::INVALID;
        } else {
            return iter->second;
        }
    }


    // ========================== General methods to deal with properties =========================

    PropertyStorage Properties::createProperty(CommonBox & commonBox, Province & province,
                                               simplenetwork::Message & msg, proptype_t propertyType) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    return createExpr;

        RUN_PROPERTY_METHOD("create property", return {.value = 0})
        #undef X
    }

    ProvStatModSet Properties::getStatModifierSet(CommonBox & commonBox, Province & province,
                                                  proptype_t propertyType, PropertyStorage & property) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    return getStatModifierSetExpr;

        RUN_PROPERTY_METHOD("get stat modifier set", return ProvStatModSet())
        #undef X
    }

    std::string Properties::getFriendlyName(CommonBox & commonBox, Province & province,
                                            proptype_t propertyType, PropertyStorage & property) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    return getFriendlyNameExpr;

        RUN_PROPERTY_METHOD("get friendly name", return "Unknown friendly name")
        #undef X
    }

    std::string Properties::getDescription(CommonBox & commonBox, Province & province,
                                           proptype_t propertyType, PropertyStorage & property) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    return getDescriptionExpr;

        RUN_PROPERTY_METHOD("get description", return "Unknown description")
        #undef X
    }

    bool Properties::needsUpdating(CommonBox & commonBox, Province & province,
                                   proptype_t propertyType, PropertyStorage & property) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    return needsUpdatingExpr;

        RUN_PROPERTY_METHOD("needs updating", return false)
        #undef X
    }

    unsigned int Properties::getIconId(CommonBox & commonBox, Province & province,
                                       proptype_t propertyType, PropertyStorage & property) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    return getIconIdExpr;

        RUN_PROPERTY_METHOD("get icon ID", return IconDef::DEFAULT)
        #undef X
    }

    void Properties::addTerrDispLayers(CommonBox & commonBox, Province & province,
                                       std::vector<TerDispLayer> & layersVec,
                                       proptype_t propertyType, PropertyStorage & property) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    addTerrDispLayersExpr;                                                      \
                    break;

        RUN_PROPERTY_METHOD("add terrain display layers", break)
        #undef X
    }

    void Properties::writeUpdate(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                 proptype_t propertyType, PropertyStorage & property) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    writeUpdateExpr;                                                            \
                    break;

        RUN_PROPERTY_METHOD("write update", break)
        #undef X
    }

    void Properties::writeMiscUpdate(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                     proptype_t propertyType, PropertyStorage & property) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    writeMiscUpdateExpr;                                                        \
                    break;

        RUN_PROPERTY_METHOD("write misc update", break)
        #undef X
    }

    void Properties::receiveUpdate(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                   proptype_t propertyType, PropertyStorage & property) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    receiveUpdateExpr;                                                          \
                    break;

        RUN_PROPERTY_METHOD("receive update", break)
        #undef X
    }

    void Properties::receiveDirectUpdate(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                         proptype_t propertyType, PropertyStorage & property) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    receiveDirectUpdateExpr;                                                    \
                    break;

        RUN_PROPERTY_METHOD("receive direct update", break)
        #undef X
    }

    void Properties::writeInitialData(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                      bool isSaving, proptype_t propertyType, PropertyStorage & property) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    writeInitialDataExpr;                                                       \
                    break;

        RUN_PROPERTY_METHOD("write initial data", break)
        #undef X
    }

    void Properties::deleteProperty(CommonBox & commonBox, Province & province,
                                    proptype_t propertyType, PropertyStorage & property) {

        #define X(baseType, createExpr, getStatModifierSetExpr, getFriendlyNameExpr,            \
                  getDescriptionExpr, needsUpdatingExpr, getIconIdExpr, addTerrDispLayersExpr,  \
                  writeUpdateExpr, writeMiscUpdateExpr, receiveUpdateExpr,                      \
                  receiveDirectUpdateExpr, writeInitialDataExpr, deleteExpr)                    \
                                                                                                \
                case BaseTypes::baseType:                                                       \
                    deleteExpr;                                                                 \
                    break;

        RUN_PROPERTY_METHOD("delete property", break)
        #undef X
    }


    // ======================== Reading/writing properties alongside type/ID =======================

    void Properties::writeTypeAndId(simplenetwork::Message & msg, proptype_t propertyType, uint64_t uniqueId) {
        PROPERTY_TYPE_WRITE(msg, propertyType);
        msg.write64(uniqueId);
    }

    void Properties::readTypeAndId(simplenetwork::Message & msg, proptype_t & propertyType, uint64_t & uniqueId) {
        propertyType = PROPERTY_TYPE_READ(msg);
        uniqueId = msg.read64();
    }

    void Properties::writeInitialDataTypeAndId(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                               bool isSaving, uint64_t uniqueId, proptype_t propertyType, PropertyStorage & property) {
        writeTypeAndId(msg, propertyType, uniqueId);
        writeInitialData(commonBox, province, msg, isSaving, propertyType, property);
    }

    bool Properties::createPropertyTypeAndId(CommonBox & commonBox, Province & province, simplenetwork::Message & msg,
                                             uint64_t & uniqueId, proptype_t & propertyType, PropertyStorage & property) {
        readTypeAndId(msg, propertyType, uniqueId);
        // Only run the createProperty method if the property type is *actually* valid.
        if (isValidProperty(propertyType)) {
            property = createProperty(commonBox, province, msg, propertyType);
            return true;
        } else {
            std::cerr << "WARNING: Properties: " << "tried to create invalid property type: " << (unsigned int)propertyType << ".\n";
            return false;
        }
    }
}

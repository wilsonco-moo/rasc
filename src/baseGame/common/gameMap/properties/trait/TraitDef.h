/*
 * TraitDef.h
 *
 *  Created on: 6 Feb 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROPERTIES_TRAIT_TRAITDEF_H_
#define BASEGAME_COMMON_GAMEMAP_PROPERTIES_TRAIT_TRAITDEF_H_

#include <cstdint>

namespace rasc {

    /**
     * This header file defines the enums and macros required to
     * store and use traits.
     *
     * Note that traits no longer have their own type: traits
     * are now properties, so must use the property type defined
     * in PropertyDef.h (proptype_t).
     */
    class TraitDef {
    public:

        /**
         * Display modes for traits, within the province menu.
         */
        enum class DisplayMode {
            /**
             * This means that this trait should be shown as, and classified as, a
             * building. Building traits should typically be marked as buildable,
             * and placeable by users.
             */
            building,

            /**
             * This means that this trait should be shown as a natural terrain trait.
             * Natural terrain traits typically are not changed during the game, and
             * are specified as part of the map.
             * Note that this is the DEFAULT display mode for traits (BaseInternalTrait),
             * unless explicitly changed otherwise.
             */
            terrainTrait,

            /**
             * This means that the building should be shown as the province's terrain
             * type. Typically, only one of these should exist in each province,
             * in a special slot in ProvinceProperties. The properties defined here
             * decide such things as whether the province is accessible.
             */
            terrainType,

            /**
             * This means that the province menu should not list this trait, in any scroll pane.
             * In essence, this causes a trait to be "hidden".
             */
            none
        };

        /**
         * Each building has a category, representing, in general, what the building is for.
         */
        class BuildingCategories {
        public:
            enum {
                manpower,
                currency,
                province,
                combat,
                none,

                COUNT
            };
        };
        using BuildingCategory = unsigned int;
    };

    /**
     * This should be used to define each trait, in RASC_TRAITS_LIST.
     * Each trait is defined using the "trait" base type.
     */
    #define RASC_DEFINE_TRAIT(traitName) \
        X(traitName, trait)

    /**
     * Macros (RASC_DEFINE_TRAIT) defining a list of all the traits which exist.
     * All of these must have associated definitions in one of the trait declaration
     * header files, (which are #included by Traits.cpp).
     */
    #define RASC_TRAITS_LIST                            \
                                                        \
        /* Terrain traits (misc)                     */ \
        RASC_DEFINE_TRAIT(geothermal)                   \
        RASC_DEFINE_TRAIT(forested)                     \
        RASC_DEFINE_TRAIT(volcanic)                     \
        RASC_DEFINE_TRAIT(seismic)                      \
        RASC_DEFINE_TRAIT(stinking)                     \
        /* Terrain traits (climate)                  */ \
        RASC_DEFINE_TRAIT(arable)                       \
        RASC_DEFINE_TRAIT(boreal)                       \
        RASC_DEFINE_TRAIT(arid)                         \
        RASC_DEFINE_TRAIT(glacial)                      \
        RASC_DEFINE_TRAIT(desolate)                     \
        /* Terrain traits (weather)                  */ \
        /* Terrain traits (shape)                    */ \
        RASC_DEFINE_TRAIT(mountainous)                  \
        RASC_DEFINE_TRAIT(hilly)                        \
        RASC_DEFINE_TRAIT(steep)                        \
        RASC_DEFINE_TRAIT(valleyComprising)             \
        RASC_DEFINE_TRAIT(flat)                         \
        /* Terrain traits (water)                    */ \
        RASC_DEFINE_TRAIT(riverside)                    \
        RASC_DEFINE_TRAIT(coastal)                      \
        RASC_DEFINE_TRAIT(lakeContaining)               \
        RASC_DEFINE_TRAIT(boggy)                        \
        RASC_DEFINE_TRAIT(swampy)                       \
        RASC_DEFINE_TRAIT(marshy)                       \
        /* Terrain traits (ground)                   */ \
        RASC_DEFINE_TRAIT(gravelly)                     \
        RASC_DEFINE_TRAIT(bouldery)                     \
        RASC_DEFINE_TRAIT(rocky)                        \
        RASC_DEFINE_TRAIT(tarSeeping)                   \
        RASC_DEFINE_TRAIT(peaty)                        \
        RASC_DEFINE_TRAIT(sandy)                        \
        RASC_DEFINE_TRAIT(dusty)                        \
                                                        \
        /* Terrain types (temperate)                 */ \
        RASC_DEFINE_TRAIT(woodland)                     \
        RASC_DEFINE_TRAIT(grassland)                    \
        RASC_DEFINE_TRAIT(plains)                       \
        RASC_DEFINE_TRAIT(city)                         \
        /* Terrain types (dry)                       */ \
        RASC_DEFINE_TRAIT(plateau)                      \
        RASC_DEFINE_TRAIT(steppe)                       \
        RASC_DEFINE_TRAIT(saltFlat)                     \
        /* Terrain types (cold)                      */ \
        RASC_DEFINE_TRAIT(taiga)                        \
        RASC_DEFINE_TRAIT(tundra)                       \
        RASC_DEFINE_TRAIT(iceSheet)                     \
        /* Terrain types (humid)                     */ \
        RASC_DEFINE_TRAIT(moistWoodland)                \
        RASC_DEFINE_TRAIT(rainforest)                   \
        RASC_DEFINE_TRAIT(jungle)                       \
        /* Terrain types (wet)                       */ \
        RASC_DEFINE_TRAIT(monsoonRegion)                \
        RASC_DEFINE_TRAIT(saltMarsh)                    \
        RASC_DEFINE_TRAIT(mudflat)                      \
        /* Terrain types (hot)                       */ \
        RASC_DEFINE_TRAIT(savannah)                     \
        RASC_DEFINE_TRAIT(outback)                      \
        RASC_DEFINE_TRAIT(desert)                       \
        /* Terrain types (inaccessible)              */ \
        RASC_DEFINE_TRAIT(sea)                          \
        RASC_DEFINE_TRAIT(wasteland)                    \
        RASC_DEFINE_TRAIT(lake)                         \
                                                        \
        /* Manpower buildings.                       */ \
        RASC_DEFINE_TRAIT(barracks)                     \
        RASC_DEFINE_TRAIT(recruitmentOffice)            \
        RASC_DEFINE_TRAIT(armyBase)                     \
        RASC_DEFINE_TRAIT(town)                         \
        RASC_DEFINE_TRAIT(sewagePlant)                  \
                                                        \
        /* Currency buildings.                       */ \
        RASC_DEFINE_TRAIT(bank)                         \
        RASC_DEFINE_TRAIT(stockMarket)                  \
        RASC_DEFINE_TRAIT(taxOffice)                    \
        RASC_DEFINE_TRAIT(townHall)                     \
        RASC_DEFINE_TRAIT(fuelDepot)                    \
        RASC_DEFINE_TRAIT(spicePlantation)              \
        RASC_DEFINE_TRAIT(mine)                         \
                                                        \
        /* Stackable buildings.                      */ \
        RASC_DEFINE_TRAIT(housingProject)               \
        RASC_DEFINE_TRAIT(highDensityHousing)           \
        RASC_DEFINE_TRAIT(roadNetwork)                  \
        RASC_DEFINE_TRAIT(publicToilets)                \
        RASC_DEFINE_TRAIT(farm)                         \
        RASC_DEFINE_TRAIT(inn)                          \
        RASC_DEFINE_TRAIT(footPaths)                    \
        RASC_DEFINE_TRAIT(postbox)                      \
                                                        \
        /* Province buildings (trees)                */ \
        RASC_DEFINE_TRAIT(chopTrees)                    \
        RASC_DEFINE_TRAIT(burnRainforest)               \
        RASC_DEFINE_TRAIT(burnJungle)                   \
        RASC_DEFINE_TRAIT(cutDownForest)                \
                                                        \
        /* Province buildings (land shape)           */ \
        RASC_DEFINE_TRAIT(flattenMountain)              \
        RASC_DEFINE_TRAIT(bridgeValleys)                \
        RASC_DEFINE_TRAIT(solidifySand)                 \
        RASC_DEFINE_TRAIT(digUpSaltFlat)                \
                                                        \
        /* Province buildings (land reclamation)     */ \
        RASC_DEFINE_TRAIT(reclaimMudflat)               \
        RASC_DEFINE_TRAIT(reclaimSaltMarsh)             \
        RASC_DEFINE_TRAIT(reclaimLand)                  \
        RASC_DEFINE_TRAIT(drainMarsh)                   \
        RASC_DEFINE_TRAIT(drainBog)                     \
        RASC_DEFINE_TRAIT(drainSwamp)                   \
                                                        \
        /* Province buildings (misc)                 */ \
        RASC_DEFINE_TRAIT(thawTaiga)                    \
        RASC_DEFINE_TRAIT(thawIceSheet)                 \
        RASC_DEFINE_TRAIT(plugVolcanoHoles)             \
        RASC_DEFINE_TRAIT(industrialiseFarmland)        \
        RASC_DEFINE_TRAIT(drainTar)                     \
        RASC_DEFINE_TRAIT(fillInLake)                   \
                                                        \
        /* Combat related buildings and traits.      */ \
        RASC_DEFINE_TRAIT(fort)                         \
        RASC_DEFINE_TRAIT(garrison)
}

#endif

/*
 * Traits.h
 *
 *  Created on: 20 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROPERTIES_TRAIT_TRAITS_H_
#define BASEGAME_COMMON_GAMEMAP_PROPERTIES_TRAIT_TRAITS_H_

#include <sstream>
#include <cstdint>
#include <vector>
#include <string>

#include "../../../util/definitions/TerrainDisplayDef.h"
#include "../../provinceUtil/ProvinceStatTypes.h"
#include "../../../playerData/PlayerStatArr.h"
#include "../../provinceUtil/ProvStatMod.h"
#include "../PropertyDef.h"
#include "TraitDef.h"

namespace simplenetwork {
    class Message;
}

namespace rasc {
    class Province;
    template<typename Type>
    class StaticVector;

    /**
     * This class contains utility methods for use with traits. Since
     * traits are a type of property, the typedef to use for them is
     * proptype_t, defined in PropertyDef.h.
     *
     * Traits can be accessed the same as all other properties, for example,
     * like this: Properties::Types::fort, Properties::Types::mountainous, etc.
     *
     * The count of how many traits exist can be accessed with TraitRange::COUNT.
     * Note that this is different to the total count of property types
     * (Properties::Types::COUNT), since not all properties are traits.
     *
     * The special value Properties::Types::INVALID is returned by methods
     * to represent no trait. It is guaranteed to return false in
     * Properties::isValidProperty.
     */
    class Traits {
    public:
        // ------------------------- Constants (for convenience) ------------------------

        /**
         * The land use of forts. The existence of this as a static constant makes it
         * more convenient for the AI to check land use of forts quickly.
         */
        static const ProvStatMod FORT_LAND_USE;


        // ------------------------------ Trait properties ------------------------------

        /**
         * Generates a friendly name of the trait property. This should be used when
         * displaying the trait in the UI. This is generated from the property's internal
         * name (Properties::typeToInternalName),  passed through
         * StaticConfig::camelCaseToSpaceSeparated.
         */
        static std::string getFriendlyName(proptype_t trait);

        /**
         * Gets the general description of the trait.
         */
        static const char * getTraitDescription(proptype_t trait);

        /**
         * Gets a very long description of the trait, as used in the build menu (etc).
         * This includes: (but is not limited to)
         *  - Whether the trait is stackable
         *  - Stats (with icons surrounded in : for fancy text display)
         *  - Required/blocking traits (getRequiredTraitFriendlyNameList/getBlockingTraitFriendlyNameList)
         *  - Description (see getTraitDescription)
         */
        static void getTraitLongDescription(std::stringstream & outStr, proptype_t trait);
        
        /**
         * Gets the province stat modifier set for the specified trait,
         * and the specified trait count.
         * If the provided count is zero, then this is always guaranteed to be a
         * stat modifier set which has no effect, (a "zero" (default constructed) stat modifier set).
         *
         * Note unfortunately that this cannot return a ProvStatModSet by reference, as there can be
         * many, many possible variations of ProvStatModSet, depending on the count value.
         */
        static ProvStatModSet getProvinceStatModiferSet(proptype_t trait, provstat_t count);

        /**
         * Gets the stat modifier set which would be gained by constructing a SINGLE additional
         * instance of the specified trait in the province. Note that this is only relevant for
         * building traits.
         * Also note that this may not be the same as getProvinceStatModiferSet(trait, 1), since
         * stackable traits may not have a completely linear stat modifier effect in terms of count.
         * This method works out the real stat modifier effect of gaining an additional instance of
         * the trait.
         * Note: If the province already has the trait, and it is a non-stackable trait, results are
         *       meaningless, as this method would return the stat gain from constructing an additional
         *       instance of the non-stacking trait, in most cases returning a zero stat modifier set.
         * Note: The specified property MUST be a trait.
         */
        static ProvStatModSet getConstructAdditionalModifierSet(Province & province, proptype_t trait);

        /**
         * Returns the display mode for the specified trait.
         */
        static TraitDef::DisplayMode getTraitDisplayMode(proptype_t trait);

        /**
         * Returns (if possible) for a given trait, how much it should cost to build
         * the specified number of them. Note that this is only relevant for either
         * buildings, (like forts), or buildable stats (like garrisons).
         * Other traits will return useless values here, like zero.
         *
         * Note that a negative value can be supplied. This returns the cost of demolishing
         * that many traits.
         *
         * It is entirely possible that the provided count is so large that working out the
         * building cost would cause overflow. In this case, the error flag IS SET TO TRUE,
         * and an invalid and nonsense (zero) building cost is returned.
         * In all other cases, the value of the error flag IS NOT CHANGED.
         *
         * In cases where the cost of a fixed number of buildings is being queried (like 1 or -1),
         * overflow is not possible. In these cases, NULL can be supplied for the error flag,
         * and the overflow check will be ignored.
         */
        static PlayerStatArr getTraitBuildingCost(bool * error, proptype_t trait, provstat_t count);

        /**
         * Returns true if the FIRST trait is better "value for money" than the SECOND trait.
         * Note that this only makes sense for traits which are buildings.
         *
         * A trait is considered to have better "value for money" if it's benefit per amount
         * of CURRENCY build cost is larger. The benefit is chosen as manpower or currency
         * gain, depending on building category. Percentage bonuses to stats are always treated
         * as more important than value bonuses.
         *
         * This can be used as a comparator to sort a container of traits. This results in the
         * traits being sorted with best "value for money" first.
         *
         * The traits must have the same BuildingCategory, which must be either currency or manpower.
         * Any pair of traits which break either condition will cause the return of useless and
         * strange results.
         */
        static bool compareTraitValueForMoney(proptype_t trait1, proptype_t trait2);

        /**
         * Allows access to all of the traits which fit into the specified building category.
         *
         * Manpower and currency building categories are sorted, best value for money first.
         * Percentage improvements to stats are always considered to be better.
         *
         * Manpower buildings are sorted in terms of manpower gain per currency
         * building cost. Currency buildings are sorted in terms of currency
         * gain per currency building cost. Other building categories are not sorted.
         */
        static const std::vector<proptype_t> & getTraitsByCategory(TraitDef::BuildingCategory buildingCategory);

        /**
         * Allows access to all of the traits which are buildable
         * (see isTraitBuildable), in no particular order.
         */
        static const StaticVector<proptype_t> & getBuildableTraits(void);

        /**
         * Allows access to all of the traits which are regular buildings
         * (see isTraitRegularBuilding), in no particular order.
         */
        static const StaticVector<proptype_t> & getRegularBuildingTraits(void);

        /**
         * Returns true if the specified trait should be considered "buildable".
         * Users should be able to build and demolish "buildable" traits, but should be blocked
         * from building/demolishing non-buildable traits.
         */
        static bool isTraitBuildable(proptype_t trait);

        /**
         * Returns true if the specified trait should be considered a "regular building".
         * These are traits which are buildable (see isTraitBuildable), AND are neither
         * "combat" or "none" categorised buildings.
         * These are buildings which produce and/or consume currency, manpower or land,
         * and can be sensibly compared by "value-for-money". Combat buildings (and
         * "none" categorised traits, since they are for special purposes), cannot be
         * compared in this way, since they perform other effects than purely stat benefits.
         */
        static bool isTraitRegularBuilding(proptype_t trait);

        /**
         * Returns true if construction of the specified trait should be allowed during battles.
         */
        static bool isTraitBattleConstructionAllowed(proptype_t trait);

        /**
         * Returns true if the specified number of the specified trait can be added to the province.
         * Note that the provided count should be a non zero value, otherwise this operation doesn't make any sense.
         * This will return false (be blocked) if:
         *  > The trait is not buildable (see isTraitBuildable).
         *  > The trait is not stackable, and this would result in a value other than zero or one.
         *  > This would result in a negative value for the trait, for traits where negative values are disallowed.
         *  > There is a battle ongoing, and this trait does not allow construction/demolition during
         *    battles (the default behaviour for virtually all building traits).
         *  > If this results in a non-zero trait value:
         *    - If the province is missing any required traits or the required type.
         *    - If the province has any of the blocking traits or types.
         *    - If any existing traits in the province blocks this trait.
         *  > If this results in a zero trait value:
         *    - If any other traits in the province require this trait.
         */
        static bool canTraitBeBuilt(const Province & province, proptype_t trait, provstat_t count);

        /**
         * Populates the provided vector, with all the (building) traits of the specified
         * category, where one additional can be built in the specified province. The traits are ordered
         * with best value-for-money traits being first.
         * Note that the provided vector is always cleared first, before doing anything.
         *
         * For information about how trait buildability is decided, see documentation for canTraitBeBuilt.
         * For information about value-for-money trait ordering, see documentation for compareTraitValueForMoney.
         */
        static void findBuildableTraits(const Province & province, std::vector<proptype_t> & traitsVector, TraitDef::BuildingCategory buildingCategory);

        /**
         * The same as findBuildableTraits, except this function only includes buildings with a
         * maximum currency building cost (for one instance of the building), less than or equal
         * to the provided maximum cost.
         */
        static void findBuildableTraitsMaxCost(const Province & province, std::vector<proptype_t> & traitsVector, TraitDef::BuildingCategory buildingCategory, provstat_t maxCost);

        /**
         * Gets the category associated with the provided trait.
         */
        static TraitDef::BuildingCategory getTraitCategory(proptype_t trait);

        /**
         * Returns true if the trait is stackable, false otherwise. Note that this is only useful
         * for traits which are buildings.
         */
        static bool isTraitStackable(proptype_t trait);

        /**
         * Clears and populates the specified vector, with the internal names
         * (from Properties::typeToInternalName) of blocking/required traits.
         */
        static void getBlockingTraitNames(proptype_t trait, std::vector<std::string> & names);
        static void getRequiredTraitNames(proptype_t trait, std::vector<std::string> & names);

        /**
         * Generates a string which represents the blocking/required traits
         * for the specified trait. These are represented as a comma separated list of
         * friendly names. If there are none, the string "None" is returned.
         */
        static std::string getBlockingTraitFriendlyNameList(proptype_t trait);
        static std::string getRequiredTraitFriendlyNameList(proptype_t trait);

        /**
         * Allows access to a vector of required, blocking, allowing, costRequired
         * and costAllowing traits, for the specified trait.
         */
        static const StaticVector<proptype_t> & getBlockingTraits(proptype_t trait);
        static const StaticVector<proptype_t> & getRequiredTraits(proptype_t trait);
        static const StaticVector<proptype_t> & getAllowingTraits(proptype_t trait);
        static const StaticVector<proptype_t> & getCostRequiredTraits(proptype_t trait);
        static const StaticVector<proptype_t> & getCostAllowingTraits(proptype_t trait);

        /**
         * Returns true if trait "trait" lists either "blockingTrait", "requiredTrait",
         * "allowingTrait", "costRequiredTrait" or "costAllowingTrait" as either a
         * blocking, required, allowing, costRequired or costAllowing trait respectively.
         */
        static bool hasBlockingTrait(proptype_t trait, proptype_t blockingTrait);
        static bool hasRequiredTrait(proptype_t trait, proptype_t requiredTrait);
        static bool hasAllowingTrait(proptype_t trait, proptype_t allowingTrait);
        static bool hasCostRequiredTrait(proptype_t trait, proptype_t costRequiredTrait);
        static bool hasCostAllowingTrait(proptype_t trait, proptype_t costAllowingTrait);

        /**
         * Returns true if the specified trait (property) defines itself as accessible. Note that
         * this is only relevant for terrain types. A province should be accessible only if it's
         * associated terrain type (trait) defines itself as accessible.
         */
        static bool isTraitAccessible(proptype_t trait);


        // ------------------------------- Other methods --------------------------------

        /**
         * Adds the sky layer for the province display panel.
         */
        static void addSkyLayer(std::vector<TerDispLayer> & layers);


        // ---------------------------- Property methods -------------------------------

        /**
         * Creates a (stackable or non stackable) trait property, given the message.
         * Here, we read a provstat_t from the message, and use it for the Storage's value.
         */
        static PropertyStorage propertyCreate(simplenetwork::Message & msg);

        /**
         * Creates a property storage for any trait with the specified trait count.
         */
        static PropertyStorage propertyCreate(provstat_t traitCount);

        /**
         * Writes the "create content" in order to create a Trait with the specified count.
         * This should be paired with ProvinceProperties::writePropertyCreateHeader in order
         * to create traits.
         */
        static void propertyWriteCreateContent(simplenetwork::Message & msg, provstat_t count);

        /**
         * Assuming the provided PropertyStorage is the storage for a trait property,
         * this returns the associated trait count.
         */
        static provstat_t propertyGetTraitCount(PropertyStorage storage);

        /**
         * Gets the stat modifier relating to the specified (stackable or non stackable)
         * trait.
         * Note: If the property storage contains a zero trait count, this will return an
         *       incorrect stat modifier set. This is because when setting trait count to
         *       zero, the trait's property should be removed rather than remaining at zero.
         */
        static ProvStatModSet propertyGetStatModifierSet(proptype_t propertyType, PropertyStorage & property);

        /**
         * Gets the (user facing) friendly name for a (stackable or non stackable) trait.
         */
        static std::string propertyGetFriendlyName(proptype_t propertyType, PropertyStorage & property);

        /**
         * Gets the description for a (stackable or non stackable) trait.
         */
        static std::string propertyGetDescription(proptype_t propertyType, PropertyStorage & property);

        /**
         * Gets the icon ID relating to a (stackable or non stackable) trait.
         */
        static unsigned int propertyGetIconId(proptype_t propertyType);

        /**
         * Adds the terrain display layers, defined by the trait, to the specified
         * TerDispLayer vector.
         * This should be used in drawing the terrain display panel for the
         * provinces menu.
         */
        static void propertyAddTerrDispLayers(std::vector<TerDispLayer> & layers, proptype_t propertyType);

        /**
         * Writes a direct update message to change the trait count. This should be used in
         * conjunction with propertyReceiveDirectUpdate.
         */
        static void propertyWriteDirectUpdate(simplenetwork::Message & msg, provstat_t count);

        /**
         * Receives a direct update. This is used for changing trait count. This should be used in
         * conjunction with propertyWriteDirectUpdate.
         */
        static void propertyReceiveDirectUpdate(simplenetwork::Message & msg, PropertyStorage & property);

        /**
         * Writes the initial data required to create a (stackable or non stackable) trait
         * property. This data can be read by our propertyCreate method.
         */
        static void propertyWriteInitialData(simplenetwork::Message & msg, PropertyStorage & property);
    };
}

#endif

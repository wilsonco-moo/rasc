/*
 * Traits.cpp
 *
 *  Created on: 20 Aug 2018
 *      Author: wilson
 */

#include "Traits.h"

#include <sockets/plus/message/Message.h>
#include <unordered_map>
#include <functional>
#include <algorithm>
#include <utility>
#include <GL/gl.h>
#include <sstream>
#include <vector>
#include <array>

#include "../../../util/templateTypes/staticStorage/StaticVector.h"
#include "../../provinceUtil/battle/ProvinceBattleController.h"
#include "../../../../common/util/numeric/NumberFormat.h"
#include "../../../util/definitions/IconDef.h"
#include "../../mapElement/types/Province.h"
#include "../../../config/StaticConfig.h"
#include "../ProvinceProperties.h"
#include "../Properties.h"
#include "TraitRange.h"

// Include InternalTrait definitions here so we can directly
// use the stuff defined in there.
#define TRAIT_INTERNAL_TRAIT_DEF_INL
#include "InternalTraitDef.inl"
#undef TRAIT_INTERNAL_TRAIT_DEF_INL

namespace rasc {

    /**
     * This should be used for accessing traits internally. This correctly
     * applies the offset to the array from InternalTraitHelpers.
     */
    constexpr static const StaticTrait & getInternalTrait(proptype_t trait) {
        return internalTraitData::staticTraits[trait - TraitRange::MIN];
    }

    // Initialise land use of forts.
    const ProvStatMod Traits::FORT_LAND_USE = getInternalTrait(Properties::Types::fort).getStatModifierSet(1).getProvStatMod(ProvStatIds::landUsed);


    // ---------------------------- Traits by category ------------------------------
    // This stores a vector of traits, for each of the building categories.

    static std::array<std::vector<proptype_t>, TraitDef::BuildingCategories::COUNT> getTraitsByCategory(void) {
        // Put each trait in appropriate place by it's category.
        std::array<std::vector<proptype_t>, TraitDef::BuildingCategories::COUNT> trByCat;
        for (proptype_t trait = TraitRange::MIN; trait <= TraitRange::MAX; trait++) {
            const StaticTrait & internalTrait = getInternalTrait(trait);
            trByCat[internalTrait.buildingCategory].push_back(trait);
        }
        // Shrink each vector to fit, sort the vectors for manpower and currency by "value for money", and return.
        for (std::vector<proptype_t> & traitVec : trByCat) traitVec.shrink_to_fit();
        std::stable_sort(trByCat[TraitDef::BuildingCategories::currency].begin(), trByCat[TraitDef::BuildingCategories::currency].end(), &Traits::compareTraitValueForMoney);
        std::stable_sort(trByCat[TraitDef::BuildingCategories::manpower].begin(), trByCat[TraitDef::BuildingCategories::manpower].end(), &Traits::compareTraitValueForMoney);
        return trByCat;
    }
    const static std::array<std::vector<proptype_t>, TraitDef::BuildingCategories::COUNT> traitsByCategory = getTraitsByCategory();

    // -------------------- Buildable traits ----------------------------------------

    constexpr static size_t countBuildableTraits(void) {
        size_t count = 0;
        for (proptype_t trait = TraitRange::MIN; trait <= TraitRange::MAX; trait++) {
            if (getInternalTrait(trait).isBuildable) {
                count++;
            }
        }
        return count;
    }
    constexpr static size_t BUILDABLE_TRAITS_COUNT = countBuildableTraits();
    
    constexpr static std::array<proptype_t, BUILDABLE_TRAITS_COUNT> initialiseBuildableTraits(void) {
        std::array<proptype_t, BUILDABLE_TRAITS_COUNT> traits = {};
        size_t upto = 0;
        for (proptype_t trait = TraitRange::MIN; trait <= TraitRange::MAX; trait++) {
            if (getInternalTrait(trait).isBuildable) {
                traits[upto++] = trait;
            }
        }
        return traits;
    }
    constexpr static std::array<proptype_t, BUILDABLE_TRAITS_COUNT> buildableTraits = initialiseBuildableTraits();

    // -------------------- Regular building traits ---------------------------------
    
    constexpr static bool isRegularBuildingTrait(const StaticTrait & internalTrait) {
        return internalTrait.buildingCategory != TraitDef::BuildingCategories::combat &&
            internalTrait.buildingCategory != TraitDef::BuildingCategories::none;
    }
    constexpr static size_t countRegularBuildingTraits() {
        size_t count = 0;
        for (proptype_t building : buildableTraits) {
            if (isRegularBuildingTrait(getInternalTrait(building))) {
                count++;
            }
        }
        return count;
    }
    constexpr static size_t REGULAR_BUILDING_TRAITS_COUNT = countRegularBuildingTraits();
    
    constexpr static std::array<proptype_t, REGULAR_BUILDING_TRAITS_COUNT> initialiseRegularBuildingTraits(void) {
        std::array<proptype_t, REGULAR_BUILDING_TRAITS_COUNT> regularBuildings = {};
        size_t upto = 0;
        for (proptype_t building : buildableTraits) {
            if (isRegularBuildingTrait(getInternalTrait(building))) {
                regularBuildings[upto++] = building;
            }
        }
        return regularBuildings;
    }
    const static std::array<proptype_t, REGULAR_BUILDING_TRAITS_COUNT> regularBuildingTraits = initialiseRegularBuildingTraits();
    
    constexpr static StaticVector<proptype_t> buildableTraitsVector(&buildableTraits[0], buildableTraits.size());
    constexpr static StaticVector<proptype_t> regularBuildingTraitsVector(&regularBuildingTraits[0], regularBuildingTraits.size());

    // ---------------------- Trait property accessor methods -----------------------

    std::string Traits::getFriendlyName(proptype_t trait) {
        return StaticConfig::camelCaseToSpaceSeparated(Properties::typeToInternalName(trait));
    }

    const char * Traits::getTraitDescription(proptype_t trait) {
        return getInternalTrait(trait).description;
    }
    
    void Traits::getTraitLongDescription(std::stringstream & outStr, proptype_t trait) {
        // Show if trait is stackable.
        outStr << (isTraitStackable(trait) ? "Stackable\n" : "Not stackable\n");
        
        // Show trait stats using number formatting.
        {
            ProvStatModSet stats = getProvinceStatModiferSet(trait, 1);
            
            constexpr unsigned int size = 17;
            char numBuff[size];
            ProvStatMod mod;
            
            mod = stats.getProvStatMod(ProvStatIds::provinceValue);
            if (!mod.isZero()) {
                NumberFormat::provinceValueModifier<size>(numBuff, mod, true);
                outStr << ":provinceValue: Province value: " << numBuff << '\n';
            }
            
            mod = stats.getProvStatMod(ProvStatIds::currencyIncome) - stats.getProvStatMod(ProvStatIds::currencyExpenditure);
            if (!mod.isZero()) {
                NumberFormat::currencyModifier<size>(numBuff, mod, true);
                outStr << ":currency: Currency: " << numBuff << '\n';
            }
            
            mod = stats.getProvStatMod(ProvStatIds::manpowerIncome) - stats.getProvStatMod(ProvStatIds::manpowerExpenditure);
            if (!mod.isZero()) {
                NumberFormat::manpowerModifier<size>(numBuff, mod, true);
                outStr << ":manpower: Manpower: " << numBuff << '\n';
            }
            
            mod = stats.getProvStatMod(ProvStatIds::landAvailable);
            if (!mod.isZero()) {
                NumberFormat::landModifier<size>(numBuff, mod, true);
                outStr << ":land: Land available: " << numBuff << '\n';
            }
        }
        
        // Show required and blocking traits.
        if (!getRequiredTraits(trait).empty()) {
            outStr << "Requires: " << getRequiredTraitFriendlyNameList(trait) << '\n';
        }
        if (!getBlockingTraits(trait).empty()) {
            outStr << "Blocked by: " << getBlockingTraitFriendlyNameList(trait) << '\n';
        }

        // Add building description.
        outStr << '\n' << getTraitDescription(trait);
    }

    ProvStatModSet Traits::getProvinceStatModiferSet(proptype_t trait, provstat_t count) {
        // Just in case the trait does not define a "zero" stat modifier set for zero count,
        // return a default constructed ProvStatModSet for zero values.
        if (count == 0) {
            return ProvStatModSet();
        } else {
            return getInternalTrait(trait).getStatModifierSet(count);
        }
    }

    ProvStatModSet Traits::getConstructAdditionalModifierSet(Province & province, proptype_t trait) {
        const StaticTrait & internalTrait = getInternalTrait(trait);
        const PropertySet & propertySet = province.properties.getPropertySet();
        const std::unordered_map<proptype_t, std::unordered_set<uint64_t>> & byType = propertySet.getPropertiesByTypeMap();
        auto iter = byType.find(trait);

        // If trait currently doesn't exist in the province, just return "1" stat modifier.
        if (iter == byType.end()) {
            return internalTrait.getStatModifierSet(1);

        // Otherwise work out "change" stat modifier from current count, to count+1.
        } else {
            provstat_t oldCount = propertySet.getPropertyStorageUnchecked(*(iter->second.begin())).getStorage().value,
                       newCount = oldCount + 1;
            // New count could be zero if trait count is negative, old count cannot be
            // zero since we know the trait currently exists in the province.
            return (newCount == 0 ? ProvStatModSet() : internalTrait.getStatModifierSet(newCount)) - internalTrait.getStatModifierSet(oldCount);
        }
    }

    TraitDef::DisplayMode Traits::getTraitDisplayMode(proptype_t trait) {
        return getInternalTrait(trait).displayMode;
    }

    PlayerStatArr Traits::getTraitBuildingCost(bool * returnError, proptype_t trait, provstat_t count) {
        bool error = false;
        PlayerStatArr returnCost;

        // Multiply appropriate build cost by count, checking for overflow.
        // Use negative count for demolition.
        if (count > 0) {
            returnCost = getInternalTrait(trait).buildCost.multiplyCheckOverflow(&error, count);
        } else if (count < 0) {
            returnCost = getInternalTrait(trait).demolishCost.multiplyCheckOverflow(&error, -count);
        }

        // If the user didn't provide us with NULL, set the error flag if required.
        if (error && returnError != NULL) {
            *returnError = error;
        }

        return returnCost;
    }

    bool Traits::compareTraitValueForMoney(proptype_t trait1, proptype_t trait2) {
        return getInternalTrait(trait1).isBetterValueThan(getInternalTrait(trait2));
    }

    const std::vector<proptype_t> & Traits::getTraitsByCategory(TraitDef::BuildingCategory buildingCategory) {
        return traitsByCategory[buildingCategory];
    }

    const StaticVector<proptype_t> & Traits::getBuildableTraits(void) {
        return buildableTraitsVector;
    }

    const StaticVector<proptype_t> & Traits::getRegularBuildingTraits(void) {
        return regularBuildingTraitsVector;
    }

    bool Traits::isTraitBuildable(proptype_t trait) {
        return getInternalTrait(trait).isBuildable;
    }

    bool Traits::isTraitRegularBuilding(proptype_t trait) {
        const StaticTrait & internalTrait = getInternalTrait(trait);
        return internalTrait.isBuildable &&
               internalTrait.buildingCategory != TraitDef::BuildingCategories::combat &&
               internalTrait.buildingCategory != TraitDef::BuildingCategories::none;
    }

    bool Traits::isTraitBattleConstructionAllowed(proptype_t trait) {
        return getInternalTrait(trait).constructionAllowedDuringBattle;
    }

    bool Traits::canTraitBeBuilt(const Province & province, proptype_t trait, provstat_t count) {
        const StaticTrait & internalTrait = getInternalTrait(trait);
        const ProvinceProperties & properties = province.properties;
        provstat_t oldCount = properties.getTraitCount(trait),
                   newCount = oldCount + count;

        // We cannot build a trait which is not buildable.
        if (!internalTrait.isBuildable) return false;

        // If building this would result in a value other than zero or one, and the trait is not stackable, then we can't do this.
        if (newCount != 0 && newCount != 1 && !internalTrait.isStackable) return false;

        // If building this would result in a less than zero value, and that is not allowed, then we can't do that.
        if (newCount < 0 && !internalTrait.lessThanZeroAllowed) return false;

        // If there is a battle ongoing, and this trait does not allow construction during a battle, return false.
        if (province.battleControl->isBattleOngoing() && !internalTrait.constructionAllowedDuringBattle) return false;

        // If we are building (not demolishing):
        if (count > 0) {
            // Find modifier change of constructing this trait. Remember to ignore stat modifiers where count is zero.
            ProvStatModSet modifier;
            if (newCount != 0) modifier += internalTrait.getStatModifierSet(newCount);
            if (oldCount != 0) modifier -= internalTrait.getStatModifierSet(oldCount);

            if (properties.doesModifierExceedLand(modifier)) return false;
        }

        // If the new count results in zero, (i.e: demolishing)
        if (newCount == 0) {
            // If any traits which the one to be removed allows (they require this trait), exist in the province, block demolition.
            for (proptype_t allowingTrait : internalTrait.allowingTraits) {
                if (properties.getPropertySet().hasPropertiesOfType(allowingTrait)) return false;
            }
        // If the new count results in non-zero, (i.e: building)
        } else {
            // If the province is missing any of the required traits, block construction.
            for (proptype_t requiredTrait : internalTrait.requiredTraits) {
                if (!properties.getPropertySet().hasPropertiesOfType(requiredTrait)) return false;
            }
            // If the province has any of the blocking traits, block construction.
            for (proptype_t blockingTrait : internalTrait.blockingTraits) {
                if (properties.getPropertySet().hasPropertiesOfType(blockingTrait)) return false;
            }
        }

        return true; // .... If we have gone through all of that without finding a reason not to
    }                //      build the trait, then return true since we *can* build the trait.

    void Traits::findBuildableTraits(const Province & province, std::vector<proptype_t> & traitsVector, TraitDef::BuildingCategory buildingCategory) {
        // Always clear the vector before doing anything.
        traitsVector.clear();
        // Add all traits which are able to be built here, in order, filtering by category.
        for (proptype_t trait : getTraitsByCategory(buildingCategory)) {
            if (canTraitBeBuilt(province, trait, 1)) {
                traitsVector.push_back(trait);
            }
        }
    }

    void Traits::findBuildableTraitsMaxCost(const Province & province, std::vector<proptype_t> & traitsVector, TraitDef::BuildingCategory buildingCategory, provstat_t maxCost) {
        // Always clear the vector before doing anything.
        traitsVector.clear();
        // Add all traits which are able to be built here, in order, filtering by category.
        for (proptype_t trait : getTraitsByCategory(buildingCategory)) {
            if (canTraitBeBuilt(province, trait, 1) && getInternalTrait(trait).buildCost[PlayerStatIds::currency] <= maxCost) {
                traitsVector.push_back(trait);
            }
        }
    }

    TraitDef::BuildingCategory Traits::getTraitCategory(proptype_t trait) {
        return getInternalTrait(trait).buildingCategory;
    }
    bool Traits::isTraitStackable(proptype_t trait) {
        return getInternalTrait(trait).isStackable;
    }
    void Traits::getBlockingTraitNames(proptype_t trait, std::vector<std::string> & names) {
        names.clear();
        const StaticTrait & internalTrait = getInternalTrait(trait);
        for (proptype_t blockingProperty : internalTrait.blockingTraits) {
            names.push_back(Properties::typeToInternalName(blockingProperty));
        }
    }
    void Traits::getRequiredTraitNames(proptype_t trait, std::vector<std::string> & names) {
        names.clear();
        const StaticTrait & internalTrait = getInternalTrait(trait);
        for (proptype_t requiredProperty : internalTrait.requiredTraits) {
            names.push_back(Properties::typeToInternalName(requiredProperty));
        }
    }
    #define STR_VEC_TO_COMMA_SEP(vector) {                                      \
        if ((vector).empty()) return "None";                                    \
        std::stringstream str;                                                  \
        for (size_t i = 0; i < (vector).size(); i++) {                          \
            str << StaticConfig::camelCaseToSpaceSeparated((vector)[i]);        \
            if (i != (vector).size() - 1) str << ", ";                          \
        }                                                                       \
        return str.str();                                                       \
    }
    std::string Traits::getBlockingTraitFriendlyNameList(proptype_t trait) {
        std::vector<std::string> names;
        getBlockingTraitNames(trait, names);
        STR_VEC_TO_COMMA_SEP(names)
    }
    std::string Traits::getRequiredTraitFriendlyNameList(proptype_t trait) {
        std::vector<std::string> names;
        getRequiredTraitNames(trait, names);
        STR_VEC_TO_COMMA_SEP(names)
    }
    const StaticVector<proptype_t> & Traits::getBlockingTraits(proptype_t trait) {
        return getInternalTrait(trait).blockingTraits;
    }
    const StaticVector<proptype_t> & Traits::getRequiredTraits(proptype_t trait) {
        return getInternalTrait(trait).requiredTraits;
    }
    const StaticVector<proptype_t> & Traits::getAllowingTraits(proptype_t trait) {
        return getInternalTrait(trait).allowingTraits;
    }
    const StaticVector<proptype_t> & Traits::getCostRequiredTraits(proptype_t trait) {
        return getInternalTrait(trait).costRequiredTraits;
    }
    const StaticVector<proptype_t> & Traits::getCostAllowingTraits(proptype_t trait) {
        return getInternalTrait(trait).costAllowingTraits;
    }
    bool Traits::hasBlockingTrait(proptype_t trait, proptype_t blockingTrait) {
        return getInternalTrait(trait).hasBlocking(blockingTrait);
    }
    bool Traits::hasRequiredTrait(proptype_t trait, proptype_t requiredTrait) {
        return getInternalTrait(trait).hasRequired(requiredTrait);
    }
    bool Traits::hasAllowingTrait(proptype_t trait, proptype_t allowingTrait) {
        return getInternalTrait(trait).hasAllowing(allowingTrait);
    }
    bool Traits::hasCostRequiredTrait(proptype_t trait, proptype_t costRequiredTrait) {
        return getInternalTrait(trait).hasCostRequired(costRequiredTrait);
    }
    bool Traits::hasCostAllowingTrait(proptype_t trait, proptype_t costAllowingTrait) {
        return getInternalTrait(trait).hasCostAllowing(costAllowingTrait);
    }

    bool Traits::isTraitAccessible(proptype_t trait) {
        return getInternalTrait(trait).isAccessible;
    }

    void Traits::addSkyLayer(std::vector<TerDispLayer> & layers) {
        layers.push_back({0.0f, TerDispTex::sky});
    }

    // ---------------------------- Property methods -------------------------------

    PropertyStorage Traits::propertyCreate(simplenetwork::Message & msg) {
        PropertyStorage storage;
        storage.value = PROVINCE_STAT_READ(msg);
        return storage;
    }

    PropertyStorage Traits::propertyCreate(provstat_t traitCount) {
        PropertyStorage storage;
        storage.value = traitCount;
        return storage;
    }

    void Traits::propertyWriteCreateContent(simplenetwork::Message & msg, provstat_t count) {
        PROVINCE_STAT_WRITE(msg, count);
    }

    provstat_t Traits::propertyGetTraitCount(PropertyStorage storage) {
        return storage.value;
    }

    ProvStatModSet Traits::propertyGetStatModifierSet(proptype_t propertyType, PropertyStorage & property) {
        return getInternalTrait(propertyType).getStatModifierSet(property.value);
    }

    std::string Traits::propertyGetFriendlyName(proptype_t propertyType, PropertyStorage & property) {
        std::string name = getFriendlyName(propertyType);
        if (isTraitStackable(propertyType)) {
            name += (std::string(" [") + std::to_string(property.value) + ']');
        }
        return name;
    }

    std::string Traits::propertyGetDescription(proptype_t propertyType, PropertyStorage & property) {
        return getInternalTrait(propertyType).description;
    }

    unsigned int Traits::propertyGetIconId(proptype_t propertyType) {
        return getInternalTrait(propertyType).iconId;
    }

    void Traits::propertyAddTerrDispLayers(std::vector<TerDispLayer> & layers, proptype_t propertyType) {
        const StaticTrait & internalTrait = getInternalTrait(propertyType);
        layers.insert(layers.end(), internalTrait.layers.begin(), internalTrait.layers.end());
    }

    void Traits::propertyWriteDirectUpdate(simplenetwork::Message & msg, provstat_t count) {
        PROVINCE_STAT_WRITE(msg, count);
    }

    void Traits::propertyReceiveDirectUpdate(simplenetwork::Message & msg, PropertyStorage & property) {
        property.value = PROVINCE_STAT_READ(msg);
    }

    void Traits::propertyWriteInitialData(simplenetwork::Message & msg, PropertyStorage & property) {
        PROVINCE_STAT_WRITE(msg, property.value);
    }
}

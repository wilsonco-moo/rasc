/*
 * TraitRange.h
 *
 *  Created on: 15 Jun 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROPERTIES_TRAIT_TRAITRANGE_H_
#define BASEGAME_COMMON_GAMEMAP_PROPERTIES_TRAIT_TRAITRANGE_H_

#include <limits>

#include "../PropertyDef.h"
#include "../Properties.h"
#include "TraitDef.h"

namespace rasc {

    /**
     * This header file generates constants for managing the IDs
     * of trait properties. It works out the minimum and maximum
     * property type IDs, and the count, of properties which have
     * trait as their base type.
     *
     * This is done using constexpr functions and X macros.
     * Note that this *does* assume that ALL traits are defined
     * in RASC_TRAITS_LIST, and represent a contiguous block of
     * property types.
     *
     * This is included as a separate file, as to avoid
     * TraitDef.h and PropertyDef.h requiring the compilation of
     * constexpr functions.
     */

    class TraitRange;

    /**
     * Provides the constexpr functions for TraitRange.
     * This has to be in a separate class, since the entire
     * class needs to be defined before a constexpr function can
     * be used.
     */
    class TraitRangeFunctions {
    private:
        friend class TraitRange;

        // Make sure there are not zero traits (otherwise counting traits won't work).
        #define X(name, baseType) name,
        class InternalTraits {
        public:
            enum { RASC_TRAITS_LIST COUNT };
        };
        #undef X
        static_assert(InternalTraits::COUNT > 0);

        // Finds minimum trait value using X macros across all traits
        // (don't include non-trait properties).
        constexpr static inline proptype_t getMinimumTrait(void) {
            proptype_t minTrait = std::numeric_limits<proptype_t>::max();
            #define X(name, baseType) \
                if (Properties::Types::name < minTrait) { minTrait = Properties::Types::name; }
            RASC_TRAITS_LIST
            #undef X
            return minTrait;
        }

        // Finds maximum trait value using X macros across all traits
        // (don't include non-trait properties).
        constexpr static inline proptype_t getMaximumTrait(void) {
            proptype_t maxTrait = std::numeric_limits<proptype_t>::min();
            #define X(name, baseType) \
                if (Properties::Types::name > maxTrait) { maxTrait = Properties::Types::name; }
            RASC_TRAITS_LIST
            #undef X
            return maxTrait;
        }
    };

    /**
     * Provides minimum, maximum and count values from the
     * functions defined in TraitRangeFunctions.
     */
    class TraitRange {
    public:
        /**
         * This represents the minimum property type ID,
         * which has a trait base type.
         */
        constexpr static proptype_t MIN = TraitRangeFunctions::getMinimumTrait();

        /**
         * This represents the maximum property type ID,
         * which has a trait base type.
         */
        constexpr static proptype_t MAX = TraitRangeFunctions::getMaximumTrait();

        /**
         * This represents the total number of properties, which
         * have trait as their base type.
         */
        constexpr static proptype_t COUNT = (proptype_t)(TraitRangeFunctions::getMaximumTrait()
                                            - TraitRangeFunctions::getMinimumTrait() + 1);
    };
}

#endif

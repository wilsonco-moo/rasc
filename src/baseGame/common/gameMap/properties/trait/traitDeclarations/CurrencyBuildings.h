/*
 * CurrencyBuildings.h
 *
 *  Created on: 16 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for buildings in the currency building
 * category.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait bank(void) {
    FixedTrait trait;
    trait.description = "A bank is the bottom-tier currency building, being "
                        "very inexpensive, and providing only a small amount of "
                        "currency and province value.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::currency;

    trait.baseStats = {
        {ProvStatIds::currencyIncome, ProvStatMod(60, 0)}, // value, percentage
        {ProvStatIds::landUsed,       ProvStatMod(12, 0)},
        {ProvStatIds::provinceValue,  ProvStatMod(9, 0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 700;
    trait.demolishCost[PlayerStatIds::currency] = 700;
    trait.iconId = IconDef::currencyBuilding;
    return trait;
}

constexpr static FixedTrait stockMarket(void) {
    FixedTrait trait;
    trait.description = "A stock market is the middle-tier currency building, being "
                        "fairly inexpensive, and providing a medium amount of "
                        "currency and province value.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::currency;

    trait.baseStats = {
        {ProvStatIds::currencyIncome, ProvStatMod(100, 0)},  // value, percentage
        {ProvStatIds::landUsed,       ProvStatMod(15, 0)},
        {ProvStatIds::provinceValue,  ProvStatMod(15, 0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 1419;
    trait.demolishCost[PlayerStatIds::currency] = 1419;
    trait.costRequiredTraits = {Properties::Types::bank};
    trait.iconId = IconDef::currencyBuilding;
    return trait;
}

constexpr static FixedTrait taxOffice(void) {
    FixedTrait trait;
    trait.description = "A tax office is the top-tier currency building, being "
                        "somewhat expensive, and providing a large amount of "
                        "currency and province value.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::currency;

    trait.baseStats = {
        {ProvStatIds::currencyIncome, ProvStatMod(160, 0)}, // value, percentage
        {ProvStatIds::landUsed,       ProvStatMod(18, 0)},
        {ProvStatIds::provinceValue,  ProvStatMod(23, 0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 2600;
    trait.demolishCost[PlayerStatIds::currency] = 2600;
    trait.costRequiredTraits = {Properties::Types::stockMarket};
    trait.iconId = IconDef::currencyBuilding;
    return trait;
}

constexpr static FixedTrait townHall(void) {
    FixedTrait trait;
    trait.description = "This hall contains many things, such as horrible "
                        "methods to invent unavoidable taxes to make this "
                        "a high-density source of currency.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::currency;

    trait.baseStats = {
        {ProvStatIds::currencyIncome, ProvStatMod(165, 0)}, // value, percentage
        {ProvStatIds::landUsed,       ProvStatMod(16, 0)},
        {ProvStatIds::provinceValue,  ProvStatMod(24, 0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 2200;
    trait.demolishCost[PlayerStatIds::currency] = 2200;
    trait.requiredTraits     = {Properties::Types::town};
    trait.costRequiredTraits = {Properties::Types::town, Properties::Types::bank};
    trait.iconId = IconDef::currencyBuilding;
    return trait;
}

constexpr static FixedTrait fuelDepot(void) {
    FixedTrait trait;
    trait.description = "Store a large amount of volatile stuff to sell to "
                        "the very cold populus of your nation which is  "
                        "marked up by many times.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::currency;

    trait.baseStats = {
        {ProvStatIds::currencyIncome, ProvStatMod(105, 0)}, // value, percentage
        {ProvStatIds::landUsed,       ProvStatMod(13, 0)},
        {ProvStatIds::provinceValue,  ProvStatMod(15, 0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 1610;
    trait.demolishCost[PlayerStatIds::currency] = 1610;
    trait.blockingTraits = {Properties::Types::farm,Properties::Types::spicePlantation};
    trait.costRequiredTraits = {Properties::Types::stockMarket};
    trait.iconId = IconDef::currencyBuilding;
    return trait;
}

constexpr static FixedTrait spicePlantation(void) {
    FixedTrait trait;
    trait.description = "Grow undefined plants that are very valuable, "
                        "despite nobody knowing what it actually is. "
                        "Legally, Spice is not real, and never has been.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::currency;

    trait.baseStats = {
        {ProvStatIds::currencyIncome, ProvStatMod(220, 0)}, // value, percentage
        {ProvStatIds::landUsed,       ProvStatMod(17, 0)},
        {ProvStatIds::provinceValue,  ProvStatMod(32, 0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 3800;
    trait.demolishCost[PlayerStatIds::currency] = 3800;
    trait.blockingTraits = {Properties::Types::mountainous,Properties::Types::tarSeeping,Properties::Types::peaty,Properties::Types::marshy,Properties::Types::swampy,};
    trait.costRequiredTraits = {Properties::Types::taxOffice};
    trait.iconId = IconDef::currencyBuilding;
    return trait;
}

constexpr static FixedTrait mine(void) {
    FixedTrait trait;
    trait.description = "Hilly, mountainous regions provide excellent opportunities "
                        "for mining of metals and other minerals.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::currency;

    trait.baseStats = {
        {ProvStatIds::currencyIncome, ProvStatMod(120, 0)}, // value, percentage
        {ProvStatIds::landUsed,       ProvStatMod(12, 0)},
        {ProvStatIds::provinceValue,  ProvStatMod(18, 0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 1500;
    trait.demolishCost[PlayerStatIds::currency] = 1500;
    trait.requiredTraits     = {Properties::Types::mountainous, Properties::Types::hilly};
    trait.costRequiredTraits = {Properties::Types::mountainous, Properties::Types::hilly, Properties::Types::bank};
    trait.blockingTraits     = {Properties::Types::flattenMountain};
    trait.iconId = IconDef::currencyBuilding;
    return trait;
}

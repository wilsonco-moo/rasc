/*
 * GroundFeatures.h
 *
 *  Created on: 18 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for traits which define ground features.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait gravelly(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainGround;
    trait.description = "Patches of small stones pepper the ground here.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -50)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -25)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -5)},
        {ProvStatIds::provinceValue,  ProvStatMod(-38, 0)}
    };
    return trait;
}

constexpr static FixedTrait bouldery(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainGround;
    trait.description = "Regions of large rocks are spread over the surface.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -45)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -23)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 5)},
        {ProvStatIds::provinceValue,  ProvStatMod(-34, 0)}
    };
    return trait;
}

constexpr static FixedTrait rocky(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainGround;
    trait.description = "There is little topsoil here, leaving bare rock.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -15)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 20)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 25)},
        {ProvStatIds::provinceValue,  ProvStatMod(-18, 0)}
    };
    return trait;
}

constexpr static FixedTrait tarSeeping(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainGround;
    trait.description = "Wells of natural tar seep from the ground here.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 7)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 15)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -20)},
        {ProvStatIds::provinceValue,  ProvStatMod(11, 0)}
    };
    return trait;
}

constexpr static FixedTrait peaty(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainGround;
    trait.description = "Thick mud and coarse shrubs cover this region.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -60)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 35)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -20)},
        {ProvStatIds::provinceValue,  ProvStatMod(-13, 0)}
    };
    return trait;
}

constexpr static FixedTrait sandy(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainGround;
    trait.description = "Dry, fine sand forms dunes here.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -20)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -15)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -30)},
        {ProvStatIds::provinceValue,  ProvStatMod(-18, 0)}
    };
    return trait;
}

constexpr static FixedTrait dusty(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainGround;
    trait.description = "Dry air and little rain makes clouds of asphyxiating dust common. "
                        "This is neither nice to breath in, nor is it pleasant when it blows "
                        "into your eyes.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -50)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -45)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 15)},
        {ProvStatIds::provinceValue,  ProvStatMod(-48, 0)}
    };
    return trait;
}

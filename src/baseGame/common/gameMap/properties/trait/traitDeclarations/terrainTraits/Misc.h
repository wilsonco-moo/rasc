/*
 * Misc.h
 *
 *  Created on: 18 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for misc terrain traits.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait geothermal(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrain;
    trait.description = "Hot springs and vents are frequent here.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 15)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 30)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -5)},
        {ProvStatIds::provinceValue,  ProvStatMod(22, 0)}
    };
    return trait;
}

constexpr static FixedTrait forested(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrain;
    trait.description = "A region with a covering of trees.";
    trait.layers = {                                        // Texture IDs and depths for terrain display panel.
        {7.0f, TerDispTex::forested}
    };
    trait.baseStats = {                      // Base stats for (non stackable) trait (value, percentage).
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 25)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 28)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -10)},
        {ProvStatIds::provinceValue,  ProvStatMod(26, 0)}
    };
    return trait;
}

constexpr static FixedTrait volcanic(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrain;
    trait.description = "There is a large amount of volcano activity here.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 5)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 15)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -15)},
        {ProvStatIds::provinceValue,  ProvStatMod(10, 0)}
    };
    return trait;
}

constexpr static FixedTrait seismic(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrain;
    trait.description = "This province is prone to earthquakes and tectonic activity.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -20)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -8)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -25)},
        {ProvStatIds::provinceValue,  ProvStatMod(-14, 0)}
    };
    return trait;
}

constexpr static FixedTrait stinking(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrain;
    trait.description = "This whole province is permeated with a pungent and horrible "
                        "smell, so bad that it drives off any inhabitants";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -75)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -75)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 1)},
        {ProvStatIds::provinceValue,  ProvStatMod(-75, 0)}
    };
    return trait;
}

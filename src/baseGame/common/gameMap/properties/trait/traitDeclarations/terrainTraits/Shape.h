/*
 * Shape.h
 *
 *  Created on: 18 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for terrain traits which define the shape
 * of the land.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait mountainous(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainShape;
    trait.description = "This province contains large landforms which rise above "
                        "the surrounding land.";
    trait.layers = {                                        // Texture IDs and depths for terrain display panel.
        {1.0f, TerDispTex::mountainousBack},
        {4.0f, TerDispTex::mountainousFront}
    };
    trait.baseStats = {                      // Base stats for (non stackable) trait (value, percentage).
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 25)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 30)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -30)},
        {ProvStatIds::provinceValue,  ProvStatMod(27, 0)}
    };
    return trait;
}

constexpr static FixedTrait hilly(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainShape;
    trait.description = "A series of gentle hills are present here.";
    trait.layers = {
        {2.0f, TerDispTex::hillyBack},
        {5.0f, TerDispTex::hillyFront}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 15)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 25)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -20)},
        {ProvStatIds::provinceValue,  ProvStatMod(20, 0)}
    };
    return trait;
}

constexpr static FixedTrait steep(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainShape;
    trait.description = "This region contains many sharp cliffs and drops.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -20)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -8)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -10)},
        {ProvStatIds::provinceValue,  ProvStatMod(-14, 0)}
    };
    return trait;
}

constexpr static FixedTrait valleyComprising(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainShape;
    trait.description = "This region contains many large and enclosed valleys.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 10)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 15)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -15)},
        {ProvStatIds::provinceValue,  ProvStatMod(12, 0)}
    };
    return trait;
}

constexpr static FixedTrait flat(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainShape;
    trait.description = "The terrain here is generally very level and open.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 15)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 7)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 55)},
        {ProvStatIds::provinceValue,  ProvStatMod(20, 0)}
    };
    return trait;
}

/*
 * Water.h
 *
 *  Created on: 18 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for water-based terrain traits.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait riverside(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainWater;
    trait.description = "This province has access to a river, providing considerable trading opportunities.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 35)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 40)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -20)},
        {ProvStatIds::provinceValue,  ProvStatMod(37, 0)}
    };
    return trait;
}

constexpr static FixedTrait coastal(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainWater;
    trait.description = "The nearby sea makes this province a generally nice place to be.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 20)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 40)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -10)},
        {ProvStatIds::provinceValue,  ProvStatMod(30, 0)}
    };
    return trait;
}

constexpr static FixedTrait lakeContaining(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainWater;
    trait.description = "A large lake sits in this province, providing wildlife with a home.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 30)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 12)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -15)},
        {ProvStatIds::provinceValue,  ProvStatMod(21, 0)}
    };
    return trait;
}

constexpr static FixedTrait boggy(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainWater;
    trait.description = "A damp wetland, hindering travel and construction";
    trait.layers = {
        {6.0f, TerDispTex::boggy}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -45)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -45)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -20)},
        {ProvStatIds::provinceValue,  ProvStatMod(-45, 0)}
    };
    return trait;
}

constexpr static FixedTrait swampy(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainWater;
    trait.description = "A moist muddy region, making life difficult.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -70)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -35)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -25)},
        {ProvStatIds::provinceValue,  ProvStatMod(-53, 0)}
    };
    return trait;
}

constexpr static FixedTrait marshy(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainWater;
    trait.description = "A smelly, dank and putrid region, halting "
                        "virtually all civilised existence.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -35)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -70)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -60)},
        {ProvStatIds::provinceValue,  ProvStatMod(-53, 0)}
    };
    return trait;
}

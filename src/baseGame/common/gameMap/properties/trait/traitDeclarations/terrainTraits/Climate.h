/*
 * Climate.h
 *
 *  Created on: 18 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for climate-based terrain traits.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait arable(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainClimate;
    trait.description = "This land takes easily to agriculture.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 38)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 17)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -25)},
        {ProvStatIds::provinceValue,  ProvStatMod(27, 0)}
    };
    return trait;
}

constexpr static FixedTrait boreal(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainClimate;
    trait.description = "A large forest, prone to long winters and short, mild summers.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 12)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 15)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -35)},
        {ProvStatIds::provinceValue,  ProvStatMod(13, 0)}
    };
    return trait;
}

constexpr static FixedTrait arid(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainClimate;
    trait.description = "A dry and low-rainfall region.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -25)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -45)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 30)},
        {ProvStatIds::provinceValue,  ProvStatMod(-35, 0)}
    };
    return trait;
}

constexpr static FixedTrait glacial(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainClimate;
    trait.description = "This province contains large bodies of ice, "
                        "constantly moving through valleys.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -10)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -20)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -35)},
        {ProvStatIds::provinceValue,  ProvStatMod(-15, 0)}
    };
    return trait;
}

constexpr static FixedTrait desolate(void) {
    FixedTrait trait;
    trait.iconId = IconDef::terrainClimate;
    trait.description = "This region is mostly empty, and devoid of many life-allowing environments.";
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -70)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -70)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 50)},
        {ProvStatIds::provinceValue,  ProvStatMod(-70, 0)}
    };
    return trait;
}

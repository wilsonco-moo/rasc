/*
 * ManpowerBuildings.h
 *
 *  Created on: 16 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for buildings in the manpower building
 * category.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait barracks(void) {
    FixedTrait trait;
    trait.description = "A barracks is the bottom tier manpower building, providing a small "
                        "amount of province value and a small amount of manpower. "
                        "Barracks are also very cheap to build.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::manpower;

    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(500, 0)}, // value, percentage
        {ProvStatIds::landUsed,       ProvStatMod(12, 0)},
        {ProvStatIds::provinceValue,  ProvStatMod(10, 0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 700;
    trait.demolishCost[PlayerStatIds::currency] = 700;
    trait.iconId = IconDef::manpowerBuilding;
    return trait;
}

constexpr static FixedTrait recruitmentOffice(void) {
    FixedTrait trait;
    trait.description = "A recruitment office is the mid tier manpower building, "
                        "providing a medium amount of province value and a medium "
                        "amount of manpower. Recruitment offices are more expensive "
                        "to build.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::manpower;

    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(800, 0)}, // value, percentage
        {ProvStatIds::landUsed,       ProvStatMod(14, 0)},
        {ProvStatIds::provinceValue,  ProvStatMod(16, 0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 1500;
    trait.demolishCost[PlayerStatIds::currency] = 1500;
    trait.costRequiredTraits = {Properties::Types::barracks};
    trait.iconId = IconDef::manpowerBuilding;
    return trait;
}

constexpr static FixedTrait armyBase(void) {
    FixedTrait trait;
    trait.description = "An army base is the top tier manpower building, "
                        "providing a large amount of province value and a large "
                        "amount of manpower. Army bases are very expensive "
                        "to build.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::manpower;

    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(1100, 0)}, // value, percentage
        {ProvStatIds::landUsed,       ProvStatMod(16, 0)},
        {ProvStatIds::provinceValue,  ProvStatMod(22, 0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 2500;
    trait.demolishCost[PlayerStatIds::currency] = 2500;
    trait.costRequiredTraits = {Properties::Types::recruitmentOffice};
    trait.iconId = IconDef::manpowerBuilding;
    return trait;
}

constexpr static FixedTrait town(void) {
    FixedTrait trait;
    trait.description = "Towns are where most people probably live, "
                        "providing a high-density source of Manpower for "
                        "a medium land and a high currency cost. ";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::manpower;

    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(1400, 0)}, // value, percentage
        {ProvStatIds::landUsed,       ProvStatMod(18, 0)},
        {ProvStatIds::provinceValue,  ProvStatMod(28, 0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 3500;
    trait.demolishCost[PlayerStatIds::currency] = 3500;
    trait.blockingTraits = {Properties::Types::saltFlat,Properties::Types::iceSheet,Properties::Types::jungle,Properties::Types::saltMarsh,Properties::Types::mudflat,Properties::Types::desert,Properties::Types::mountainous};
    trait.costRequiredTraits = {Properties::Types::recruitmentOffice};
    trait.iconId = IconDef::manpowerBuilding;
    return trait;
}

constexpr static FixedTrait sewagePlant(void) {
    FixedTrait trait;
    trait.description = "Improve your town with actual sewage management, "
                        "allowing for less filth and more manpower, granting "
                        "additional density. ";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::manpower;

    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(700, 0)}, // value, percentage
        {ProvStatIds::landUsed,       ProvStatMod(5, 0)},
        {ProvStatIds::provinceValue,  ProvStatMod(14, 0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 700;
    trait.demolishCost[PlayerStatIds::currency] = 700;
    trait.requiredTraits     = {Properties::Types::town};
    trait.costRequiredTraits = {Properties::Types::town};
    trait.iconId = IconDef::manpowerBuilding;
    return trait;
}

/*
 * Combat.h
 *
 *  Created on: 16 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for combat related buildings and traits.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait fort(void) {
    FixedTrait trait;
    trait.iconId = IconDef::combatBuilding;
    trait.description = "A fort is a building which players can construct in provinces. "
                        "It costs rather a large amount of currency in maintenence, but is "
                        "required for the construction of garrisons.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::combat;

    trait.baseStats = {
        {ProvStatIds::currencyExpenditure, ProvStatMod(160, 0)}, // value, percentage
        {ProvStatIds::landUsed,            ProvStatMod(30,  0)},
        {ProvStatIds::provinceValue,       ProvStatMod(40,  0)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 3200;
    trait.demolishCost[PlayerStatIds::currency] = 3200;
    return trait;
}

constexpr static FixedTrait garrison(void) {
    FixedTrait trait;
    trait.description = "A garrison is a stackable trait representing a standing army "
                        "stationed in a province. Note that the construction of garrisons "
                        "requires a fort to be present. "
                        "Garrisons cost a maintenence proportional to their size, cost "
                        "one manpower to build (per unit), and nothing to demolish.";
    trait.isBuildable = true;
    trait.isStackable = true;

    // Use none as display mode and building category, as to avoid appearing in build menu or any trait lists.
    trait.displayMode = TraitDef::DisplayMode::none;
    trait.buildingCategory = TraitDef::BuildingCategories::none;

    // Currently garrisons cost 50% or normal armies, see UnitsDeployedStat.h.
    trait.provinceStatFunc = [](const StaticTrait & trait, provstat_t count) -> ProvStatModSet {
        return ProvStatModSet({
            {ProvStatIds::currencyExpenditure, ProvStatMod(count/100, 0)} // value, percentage
        });
    };

    // Garrisons are a special case: They cost zero to demolish (always), and one manpower to build.
    trait.buildCost[PlayerStatIds::manpower] = 1;

    // Garrisons require forts.
    trait.requiredTraits     = {Properties::Types::fort};
    trait.costRequiredTraits = {Properties::Types::fort};
    return trait;
}

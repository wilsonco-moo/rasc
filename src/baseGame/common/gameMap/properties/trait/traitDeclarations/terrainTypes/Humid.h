/*
 * Humid.h
 *
 *  Created on: 18 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for humid terrain types. These provinces
 * are too humid to be all that nice to live in.
 *
 * Each province should contain exactly one terrain type, in a special slot
 * within ProvinceProperties.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait moistWoodland(void) {
    FixedTrait trait;
    trait.description = "The moist woodland in this region supports a variety of plants, shrubs and trees.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::moistWoodland}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 20)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 20)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -15)},
        {ProvStatIds::provinceValue,  ProvStatMod(20, 0)}
    };
    return trait;
}

constexpr static FixedTrait rainforest(void) {
    FixedTrait trait;
    trait.description = "This extensive and humid forest exhibits large amounts of rainfall.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::rainforest}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 45)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 5)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -20)},
        {ProvStatIds::provinceValue,  ProvStatMod(25, 0)}
    };
    return trait;
}

constexpr static FixedTrait jungle(void) {
    FixedTrait trait;
    trait.description = "This region contains so much dense and tangled vegetation, that "
                        "development and transportation is significantly hindered.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::jungle}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -20)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -25)},
        {ProvStatIds::provinceValue,  ProvStatMod(-10, 0)}
    };
    return trait;
}

/*
 * Temperate.h
 *
 *  Created on: 18 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for temperate terrain types. These are the
 * nicest and best regions of the map to live in.
 *
 * Each province should contain exactly one terrain type, in a special slot
 * within ProvinceProperties.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait city(void) {
    FixedTrait trait;
    trait.description = "This province is a built up, developed area.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::cityBack},
        {8.0f, TerDispTex::cityFront}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 100)}, // value, percentage
        {ProvStatIds::currencyIncome, ProvStatMod(0, 80)},
        {ProvStatIds::provinceValue,  ProvStatMod(100, 0)}
    };
    return trait;
}

constexpr static FixedTrait woodland(void) {
    FixedTrait trait;
    trait.description = "This region is not only temperate and habitable, but also "
                        "contains plenty of trees, useful as building materials.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::woodland}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 60)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 70)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -20)},
        {ProvStatIds::provinceValue,  ProvStatMod(65, 0)}
    };
    return trait;
}

constexpr static FixedTrait grassland(void) {
    FixedTrait trait;
    trait.description = "This region contains temperate, habitable land.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::grassland}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 50)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 36)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 20)},
        {ProvStatIds::provinceValue,  ProvStatMod(43, 0)}
    };
    return trait;
}

constexpr static FixedTrait plains(void) {
    FixedTrait trait;
    trait.description = "This region contains temperate, easy to develop land.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::plains}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 30)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 10)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 30)},
        {ProvStatIds::provinceValue,  ProvStatMod(20, 0)}
    };
    return trait;
}



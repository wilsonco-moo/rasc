/*
 * Wet.h
 *
 *  Created on: 18 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for wet terrain types. These provinces are
 * inconveniently wet, to their own detriment.
 *
 * Each province should contain exactly one terrain type, in a special slot
 * within ProvinceProperties.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait monsoonRegion(void) {
    FixedTrait trait;
    trait.description = "This region experiences both dry seasons, and seasons of heavy rainfall.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {0.1f, TerDispTex::monsoonRegionBack},
        {3.0f, TerDispTex::monsoonRegionFore}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 30)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -30)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 15)}
    };
    return trait;
}

constexpr static FixedTrait saltMarsh(void) {
    FixedTrait trait;
    trait.description = "This coastal region is regularly flooded by the tide, and populated "
                        "by salt-tolerant grasses and shrubs.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::saltMarsh}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -15)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -20)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -30)},
        {ProvStatIds::provinceValue,  ProvStatMod(-18, 0)}
    };
    return trait;
}

constexpr static FixedTrait mudflat(void) {
    FixedTrait trait;
    trait.description = "This muddy coastal region is regularly flooded by the tide, consisting "
                        "mostly of silt and clay, but little vegetation.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::mudflat}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -20)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -50)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -30)},
        {ProvStatIds::provinceValue,  ProvStatMod(-35, 0)}
    };
    return trait;
}

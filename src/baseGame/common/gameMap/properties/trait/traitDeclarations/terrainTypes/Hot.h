/*
 * Hot.h
 *
 *  Created on: 18 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for dry terrain types. These provinces
 * are unbearably hot and dry.
 *
 * Each province should contain exactly one terrain type, in a special slot
 * within ProvinceProperties.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait savannah(void) {
    FixedTrait trait;
    trait.description = "This region contains hot and dry open woodland, with ground covered "
                        "in grass and small plants.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::savannah}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -20)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -30)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 30)},
        {ProvStatIds::provinceValue,  ProvStatMod(-25, 0)}
    };
    return trait;
}

constexpr static FixedTrait outback(void) {
    FixedTrait trait;
    trait.description = "This region contains hot, arid and dusty terrain, with patchy "
                        "and sparse forests.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::outback}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -50)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -50)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 45)},
        {ProvStatIds::provinceValue,  ProvStatMod(-50, 0)}
    };
    return trait;
}

constexpr static FixedTrait desert(void) {
    FixedTrait trait;
    trait.description = "This region is composed of barren and hostile terrain, made dry "
                        "by the lack of any significant rainfall.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::desert}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -70)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -50)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 35)},
        {ProvStatIds::provinceValue,  ProvStatMod(-60, 0)}
    };
    return trait;
}

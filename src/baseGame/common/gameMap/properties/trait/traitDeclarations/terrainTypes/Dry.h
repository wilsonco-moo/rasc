/*
 * Dry.h
 *
 *  Created on: 18 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for dry terrain types. Life here is
 * possible, but difficult.
 *
 * Each province should contain exactly one terrain type, in a special slot
 * within ProvinceProperties.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait plateau(void) {
    FixedTrait trait;
    trait.description = "This region is dry and flat, but raised far above the surrounding terrain.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::plateau}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 38)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 52)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 20)},
        {ProvStatIds::provinceValue,  ProvStatMod(45, 0)}
    };
    return trait;
}

constexpr static FixedTrait steppe(void) {
    FixedTrait trait;
    trait.description = "This is a vast dry and flat region, covered with grass and small shrubs.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::steppe}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 40)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 30)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 10)},
        {ProvStatIds::provinceValue,  ProvStatMod(35, 0)}
    };
    return trait;
}

constexpr static FixedTrait saltFlat(void) {
    FixedTrait trait;
    trait.description = "This is a flat expanse of ground, covered entirely with salt "
                        "and missing all vegetation.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::saltFlat}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -20)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 56)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -20)},
        {ProvStatIds::provinceValue,  ProvStatMod(18, 0)}
    };
    return trait;
}

/*
 * Cold.h
 *
 *  Created on: 18 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for cold terrain types. These are provinces
 * which are too cold to be particularly hospitable.
 *
 * Each province should contain exactly one terrain type, in a special slot
 * within ProvinceProperties.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif

constexpr static FixedTrait taiga(void) {
    FixedTrait trait;
    trait.description = "This region contains a vast expanse of coniferous forest, "
                        "which experiences cold, harsh winters.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::taiga}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 30)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 15)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -20)},
        {ProvStatIds::provinceValue,  ProvStatMod(22, 0)}
    };
    return trait;
}

constexpr static FixedTrait tundra(void) {
    FixedTrait trait;
    trait.description = "This region's long winters hinder the growth of trees, "
                        "resulting a bare and empty terrain.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::tundra}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 5)}, // value, percentage
        {ProvStatIds::currencyIncome, ProvStatMod(0, -5)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 50)}
    };
    return trait;
}

constexpr static FixedTrait iceSheet(void) {
    FixedTrait trait;
    trait.description = "This region is completely covered in glacial ice, making "
                        "it somewhat uninhabitable.";
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::iceSheet}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -60)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, -60)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -50)},
        {ProvStatIds::provinceValue,  ProvStatMod(-60, 0)}
    };
    return trait;
}

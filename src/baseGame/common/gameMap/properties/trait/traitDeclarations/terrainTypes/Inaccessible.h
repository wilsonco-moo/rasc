/*
 * Inaccessible.h
 *
 *  Created on: 18 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for inaccessible terrain types. These cannot
 * be used by players, and are designed as "filler" for maps.
 *
 * Each province should contain exactly one terrain type, in a special slot
 * within ProvinceProperties.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait sea(void) {
    FixedTrait trait;
    trait.description = "The sea is a large body of salty water. Habitation and land-based "
                        "travel are made troublesome due to its lack of a solid surface. As "
                        "a result, sea provinces cannot be legitimately claimed.";
    trait.isAccessible = false;
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::sea}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -100)}, // value, percentage
        {ProvStatIds::currencyIncome, ProvStatMod(0, -100)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -100)}
    };
    return trait;
}

constexpr static FixedTrait wasteland(void) {
    FixedTrait trait;
    trait.description = "This land is so far away from civilised existence that nobody "
                        "wants to go here. Consequently, wasteland cannot be claimed or used.";
    trait.isAccessible = false;
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::wasteland}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -100)}, // value, percentage
        {ProvStatIds::currencyIncome, ProvStatMod(0, -100)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -100)}
    };
    return trait;
}

constexpr static FixedTrait lake(void) {
    FixedTrait trait;
    trait.description = "This land contains a lake so large, that the other side cannot be "
                        "easily seen. For this reason, despite it being composed of fresh water "
                        "many local people think it is the sea. This means that lakes cannot "
                        "be legitimately claimed.";
    trait.isAccessible = false;
    trait.displayMode = TraitDef::DisplayMode::terrainType;
    trait.layers = {
        {3.0f, TerDispTex::lake}
    };
    trait.baseStats = {
        {ProvStatIds::manpowerIncome, ProvStatMod(0, -100)}, // value, percentage
        {ProvStatIds::currencyIncome, ProvStatMod(0, -100)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, -100)}
    };
    return trait;
}

/*
 * LandShape.h
 *
 *  Created on: 5 Oct 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for province buildings which
 * alter the shape of the land.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif

constexpr static FixedTrait flattenMountain(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Mountains are very economically important, providing rich "
                        "mineral deposits and quarry opportunities, but severely "
                        "restricting available land. However, a small change to planning "
                        "permission and tax regulations, can make this region so attractive "
                        "for quarrying operations that the mountains are eventually flattened. "
                        "No more mountains, no more problems ... right?";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(16, 0)},  // value, percentage
        {ProvStatIds::landAvailable,  ProvStatMod(0, 22)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 979;
    trait.demolishCost[PlayerStatIds::currency] = 979;
    trait.requiredTraits     = {Properties::Types::mountainous};
    trait.costRequiredTraits = {Properties::Types::mountainous};
    return trait;
}

constexpr static FixedTrait bridgeValleys(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Regions comprising of mostly valleys make transportation "
                        "difficult due to their shape. The most sensible solution "
                        "is to build enormous bridges over these valleys. Strangely, "
                        "local people seem to protest this, and claim that enormous "
                        "bridges are unsightly and even dangerous. Bribery can go a "
                        "long way in situations like this, even resulting in local "
                        "people \"no longer complaining\".";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(3, 0)},  // value, percentage
        {ProvStatIds::landAvailable,  ProvStatMod(0, 5)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 225;
    trait.demolishCost[PlayerStatIds::currency] = 225;
    trait.requiredTraits     = {Properties::Types::valleyComprising};
    trait.costRequiredTraits = {Properties::Types::valleyComprising};
    return trait;
}

constexpr static FixedTrait solidifySand(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "We keep hearing reports of construction projects mysteriously "
                        "disappearing into soft sand. Despite concerns about environmental "
                        "impact, safety, long term practicality, smell, weather resistance "
                        "and similarity to popular brands of glue sticks, "
                        "Sandcastle Industries' Sand Stick Plus (tm) can be applied to "
                        "sandy provinces for a very reasonable price.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(18, 0)},  // value, percentage
        {ProvStatIds::currencyIncome, ProvStatMod(0, 25)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 15)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 610;
    trait.demolishCost[PlayerStatIds::currency] = 610;
    trait.requiredTraits     = {Properties::Types::sandy};
    trait.costRequiredTraits = {Properties::Types::sandy};
    return trait;
}

constexpr static FixedTrait digUpSaltFlat(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Modern large scale construction makes it possible to dig up "
                        "salt flats, and dispose of them in the nearest river.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(9, 0)},  // value, percentage
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 12)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 10)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 415;
    trait.demolishCost[PlayerStatIds::currency] = 415;
    trait.requiredTraits     = {Properties::Types::saltFlat};
    trait.costRequiredTraits = {Properties::Types::saltFlat};
    return trait;
}

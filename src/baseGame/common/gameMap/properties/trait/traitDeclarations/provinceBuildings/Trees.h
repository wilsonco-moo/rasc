/*
 * Trees.h
 *
 *  Created on: 5 Oct 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for province buildings which
 * deal with general removal of trees.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif

constexpr static FixedTrait chopTrees(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Why spoil a nice temperate and habitable woodland, with a load "
                        "of trees which get in the way? Removal of these trees is a "
                        "cost-effective way to increase available land for construction "
                        "and other uses.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(15, 0)},  // value, percentage
        {ProvStatIds::landAvailable,  ProvStatMod(0, 20)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 818;
    trait.demolishCost[PlayerStatIds::currency] = 818;
    trait.requiredTraits     = {Properties::Types::woodland};
    trait.costRequiredTraits = {Properties::Types::woodland};
    return trait;
}

constexpr static FixedTrait burnRainforest(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Rainforests are generally favourable places for development: "
                        "plentiful mineral deposits, and large amounts of rainfall making "
                        "water plentifully available. The only thing spoiling it is the "
                        "trees - hindering construction and causing excessive humidity. "
                        "This can be solved very quickly and cheaply by the \"accidental\" "
                        "release of highly flammable fluids.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(7, 0)},  // value, percentage
        {ProvStatIds::landAvailable,  ProvStatMod(0, 10)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 410;
    trait.demolishCost[PlayerStatIds::currency] = 410;
    trait.requiredTraits     = {Properties::Types::rainforest};
    trait.costRequiredTraits = {Properties::Types::rainforest};
    return trait;
}

constexpr static FixedTrait burnJungle(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Dense vegetation and boggy ground makes jungles "
                        "economically very poor. Removal of this vegetation "
                        "by \"wildfires\" can provide a significant opportunity "
                        "for development.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(11, 0)},  // value, percentage
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 10)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 15)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 600;
    trait.demolishCost[PlayerStatIds::currency] = 600;
    trait.requiredTraits     = {Properties::Types::jungle};
    trait.costRequiredTraits = {Properties::Types::jungle};
    return trait;
}

constexpr static FixedTrait cutDownForest(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Trees can often be a problem, when developing infrastructure "
                        "in a region. They cause unwanted wildlife, and generally get "
                        "in the way. Plank global PLC is offering a low cost solution "
                        "to both problems: Trees are \"environmentally reprocessed\" "
                        "into building materials, and unwanted wildlife is \"relocated\".";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(7, 0)},  // value, percentage
        {ProvStatIds::landAvailable,  ProvStatMod(0, 10)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 426;
    trait.demolishCost[PlayerStatIds::currency] = 426;
    trait.requiredTraits     = {Properties::Types::forested};
    trait.costRequiredTraits = {Properties::Types::forested};
    return trait;
}

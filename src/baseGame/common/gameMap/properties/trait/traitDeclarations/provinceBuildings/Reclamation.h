/*
 * Reclamation.h
 *
 *  Created on: 5 Oct 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for province buildings which
 * reclaim land from the sea or other ground features.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif

constexpr static FixedTrait reclaimMudflat(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Large expanses of muddy, tide-washed coastland can be converted "
                        "into prime building land, with the simple addition of a fence "
                        "to keep out the sea. Despite concerns about poor construction "
                        "quality and sinking, such fences can be built very cheaply.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(22, 0)},  // value, percentage
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 10)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 15)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 30)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 1232;
    trait.demolishCost[PlayerStatIds::currency] = 1232;
    trait.requiredTraits     = {Properties::Types::mudflat};
    trait.costRequiredTraits = {Properties::Types::mudflat};
    return trait;
}

constexpr static FixedTrait reclaimSaltMarsh(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Salt marshes are very important for wildlife and defence against "
                        "coastal erosion ... is what you would say if it wasn't possible to "
                        "significantly improve the profitability of the region. Draining and "
                        "keeping the sea out of salt marshes can create prime opportunities "
                        "for overpriced and poor quality coastal holiday parks.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(22, 0)},  // value, percentage
        {ProvStatIds::currencyIncome, ProvStatMod(0, 10)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 30)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 1225;
    trait.demolishCost[PlayerStatIds::currency] = 1225;
    trait.requiredTraits     = {Properties::Types::saltMarsh};
    trait.costRequiredTraits = {Properties::Types::saltMarsh};
    return trait;
}

constexpr static FixedTrait reclaimLand(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Shallow coastal regions provide good opportunities for reclaiming "
                        "land from the sea. Not only does this provide additional "
                        "building land, but waste can now be dumped further out into "
                        "the sea, where nobody will notice it - helping to offset the cost "
                        "of such a large construction project.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(11, 0)},  // value, percentage
        {ProvStatIds::landAvailable,  ProvStatMod(0, 15)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 660;
    trait.demolishCost[PlayerStatIds::currency] = 660;
    trait.requiredTraits     = {Properties::Types::coastal};
    trait.costRequiredTraits = {Properties::Types::coastal};
    return trait;
}

constexpr static FixedTrait drainMarsh(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Civilised existence within marshes can be made "
                        "closer to reality by draining the water. This has "
                        "been proven to reduce smells by up to 65%, and reduce "
                        "ghost sightings by up to 45%.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(30, 0)},  // value, percentage
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 10)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 35)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 40)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 1580;
    trait.demolishCost[PlayerStatIds::currency] = 1580;
    trait.requiredTraits     = {Properties::Types::marshy};
    trait.costRequiredTraits = {Properties::Types::marshy};
    return trait;
}

constexpr static FixedTrait drainBog(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "By reducing the dampness of damp wetland areas, "
                        "travel and construction is hindered significantly less.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(15, 0)},  // value, percentage
        {ProvStatIds::currencyIncome, ProvStatMod(0, 10)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 20)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 875;
    trait.demolishCost[PlayerStatIds::currency] = 875;
    trait.requiredTraits     = {Properties::Types::boggy};
    trait.costRequiredTraits = {Properties::Types::boggy};
    return trait;
}

constexpr static FixedTrait drainSwamp(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Research by the consortium of shed construction, shows that "
                        "moist and muddy regions can make the construction of garden "
                        "sheds up to 35% more difficult. In order to improve the quality "
                        "of life for inhabitants of such regions, long overdue proper "
                        "drainage can be installed.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(18, 0)},  // value, percentage
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 5)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 25)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 25)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 1050;
    trait.demolishCost[PlayerStatIds::currency] = 1050;
    trait.requiredTraits     = {Properties::Types::swampy};
    trait.costRequiredTraits = {Properties::Types::swampy};
    return trait;
}

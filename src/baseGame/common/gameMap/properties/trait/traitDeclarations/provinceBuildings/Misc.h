/*
 * Misc.h
 *
 *  Created on: 5 Oct 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for miscellaneous and other
 * uncategorisable province buildings.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif

constexpr static FixedTrait thawTaiga(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Cold, polar forests are ideal for forestry industry, providing "
                        "a vital source of income for inhabitants. However, during the "
                        "winter, snow and ice significantly hinder construction and logging. "
                        "By adding underground heating, this problem can be avoided.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(7, 0)},  // value, percentage
        {ProvStatIds::landAvailable,  ProvStatMod(0, 10)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 438;
    trait.demolishCost[PlayerStatIds::currency] = 438;
    trait.requiredTraits     = {Properties::Types::taiga};
    trait.costRequiredTraits = {Properties::Types::taiga};
    return trait;
}

constexpr static FixedTrait thawIceSheet(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Ice sheets are a significant problem. Solid ice hundreds of "
                        "metres thick makes access to the minerals and other resources "
                        "below very inconvenient. Trace heating is a cost-effective "
                        "way to solve this problem - the vast quantities of water created "
                        "by melting this ice are now somebody else's problem.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(18, 0)},  // value, percentage
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 17)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 22)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 25)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 1072;
    trait.demolishCost[PlayerStatIds::currency] = 1072;
    trait.requiredTraits     = {Properties::Types::iceSheet};
    trait.costRequiredTraits = {Properties::Types::iceSheet};
    return trait;
}

constexpr static FixedTrait plugVolcanoHoles(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "The High Pressure ltd Volcano-Bung-o-matic is proven to "
                        "delay up to 75% of volcanic eruptions, for time periods "
                        "not quoted by the manufacturer. The perceived additional "
                        "safety of plugged volcanos make the surrounding land more "
                        "desirable, for a very competitive price.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(11, 0)},  // value, percentage
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 15)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 5)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 265;
    trait.demolishCost[PlayerStatIds::currency] = 265;
    trait.requiredTraits     = {Properties::Types::volcanic};
    trait.costRequiredTraits = {Properties::Types::volcanic};
    return trait;
}

constexpr static FixedTrait industrialiseFarmland(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Prime, arable farmland can be alternatively converted into "
                        "prime industrial land, using bribes and edicts. Once large "
                        "and land-consuming farming operations are \"relocated\", "
                        "industry has enough land to flourish.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(18, 0)},  // value, percentage
        {ProvStatIds::manpowerIncome, ProvStatMod(0, 2)},
        {ProvStatIds::currencyIncome, ProvStatMod(0, 5)},
        {ProvStatIds::landAvailable,  ProvStatMod(0, 25)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 1000;
    trait.demolishCost[PlayerStatIds::currency] = 1000;
    trait.requiredTraits     = {Properties::Types::arable};
    trait.costRequiredTraits = {Properties::Types::arable};
    trait.blockingTraits     = {Properties::Types::farm};
    return trait;
}

constexpr static FixedTrait drainTar(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Whereas seeping tar can be a marvelous opportunity for the "
                        "petroleum industry, it can cause subsidence and unsavoury "
                        "smells. Draining it completely, reimbursed by its sale, "
                        "is a cost-effective way to make a region more desirable.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(7, 0)},  // value, percentage
        {ProvStatIds::landAvailable,  ProvStatMod(0, 10)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 436;
    trait.demolishCost[PlayerStatIds::currency] = 436;
    trait.requiredTraits     = {Properties::Types::tarSeeping};
    trait.costRequiredTraits = {Properties::Types::tarSeeping};
    return trait;
}

constexpr static FixedTrait fillInLake(void) {
    FixedTrait trait;
    trait.iconId = IconDef::provinceBuilding;
    trait.description = "Since large lakes provide wildlife with a home, it would "
                        "be such a shame if a dreadful quarrying accident completely "
                        "filled the lake with rubble. At that point, we would have no "
                        "choice but to sell the new and reclaimed land for housing and "
                        "industrial development.";
    trait.isBuildable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::province;

    trait.baseStats = {
        {ProvStatIds::provinceValue,  ProvStatMod(11, 0)},  // value, percentage
        {ProvStatIds::landAvailable,  ProvStatMod(0, 15)}
    };
    trait.buildCost[PlayerStatIds::currency]    = 578;
    trait.demolishCost[PlayerStatIds::currency] = 578;
    trait.requiredTraits     = {Properties::Types::lakeContaining};
    trait.costRequiredTraits = {Properties::Types::lakeContaining};
    return trait;
}

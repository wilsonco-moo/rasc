/*
 * StackableBuildings.h
 *
 *  Created on: 16 Jun 2020
 *      Author: wilson
 */

/**
 * This file contains declarations for stackable buildings.
 *
 * Each trait is defined by providing a static function with the same
 * name as the trait, which returns an FixedTrait instance.
 * InternalTrait.cpp includes this file (putting the contents into its
 * own namespace), and calls each function based on the trait name,
 * using X macros.
 */

#ifndef TRAIT_INTERNAL_DECLARATIONS
    #error "This file must only be #included within InternalTrait.cpp, to provide trait declarations."
#endif


constexpr static FixedTrait housingProject(void) {
    FixedTrait trait;
    trait.description = "Housing project is a stackable building which "
                        "provides manpower. It is the same price but worse "
                        "than high density housing, but can be built anywhere.";
    trait.isBuildable = true;
    trait.isStackable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::manpower;

    trait.provinceStatFunc = [](const StaticTrait & trait, provstat_t count) -> ProvStatModSet {
        return ProvStatModSet({
            {ProvStatIds::manpowerIncome, ProvStatMod(200*count, 0)}, // value, percentage
            {ProvStatIds::landUsed,       ProvStatMod(6*count, 0)},
            {ProvStatIds::provinceValue,  ProvStatMod(4*count, 0)}
        });
    };
    trait.buildCost[PlayerStatIds::currency]    = 700;
    trait.demolishCost[PlayerStatIds::currency] = 700;
    trait.costRequiredTraits = {Properties::Types::recruitmentOffice};
    trait.iconId = IconDef::manpowerBuilding;
    return trait;
}

constexpr static FixedTrait highDensityHousing(void) {
    FixedTrait trait;
    trait.description = "High density housing is a stackable building which "
                        "provides manpower. It is the same price but better "
                        "than housing project, but requires a city to be built.";
    trait.isBuildable = true;
    trait.isStackable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::manpower;

    trait.provinceStatFunc = [](const StaticTrait & trait, provstat_t count) -> ProvStatModSet {
        return ProvStatModSet({
            {ProvStatIds::manpowerIncome, ProvStatMod(350*count, 0)}, // value, percentage
            {ProvStatIds::landUsed,       ProvStatMod(9*count, 0)},
            {ProvStatIds::provinceValue,  ProvStatMod(7*count, 0)}
        });
    };
    trait.buildCost[PlayerStatIds::currency]    = 800;
    trait.demolishCost[PlayerStatIds::currency] = 800;

    trait.requiredTraits     = {Properties::Types::town};
    trait.costRequiredTraits = {Properties::Types::sewagePlant};
    trait.iconId = IconDef::manpowerBuilding;
    return trait;
}

constexpr static FixedTrait roadNetwork(void) {
    FixedTrait trait;
    trait.description = "Build roads to connect buildings and settlements, "
                        "keep in mind that RoadStuff(tm) is very picky as to "
                        "where it actually wants to form into a road.";
    trait.isBuildable = true;
    trait.isStackable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::currency;

    trait.provinceStatFunc = [](const StaticTrait & trait, provstat_t count) -> ProvStatModSet {
        return ProvStatModSet({
            {ProvStatIds::currencyIncome, ProvStatMod(25*count, 0)}, // value, percentage
            {ProvStatIds::landUsed,       ProvStatMod(5*count, 0)},
            {ProvStatIds::provinceValue,  ProvStatMod(4*count, 0)}
        });
    };
    trait.buildCost[PlayerStatIds::currency]    = 525;
    trait.demolishCost[PlayerStatIds::currency] = 525;
    trait.blockingTraits = {Properties::Types::saltFlat,Properties::Types::iceSheet,Properties::Types::jungle,Properties::Types::saltMarsh,Properties::Types::mudflat,Properties::Types::volcanic,Properties::Types::glacial,Properties::Types::tarSeeping};
    trait.costRequiredTraits = {Properties::Types::taxOffice};
    trait.iconId = IconDef::currencyBuilding;
    return trait;
}

constexpr static FixedTrait inn(void) {
    FixedTrait trait;
    trait.description = "Keep some holidaymakers in passable accommodations "
                        "for unacceptable prices. ";
    trait.isBuildable = true;
    trait.isStackable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::currency;

    trait.provinceStatFunc = [](const StaticTrait & trait, provstat_t count) -> ProvStatModSet {
        return ProvStatModSet({
            {ProvStatIds::currencyIncome, ProvStatMod(14*count, 0)}, // value, percentage
            {ProvStatIds::landUsed,       ProvStatMod(3*count, 0)},
            {ProvStatIds::provinceValue,  ProvStatMod(2*count, 0)}
        });
    };
    trait.buildCost[PlayerStatIds::currency]    = 272;
    trait.demolishCost[PlayerStatIds::currency] = 272;
    trait.blockingTraits     = {Properties::Types::armyBase};
    trait.requiredTraits     = {Properties::Types::town};
    trait.costRequiredTraits = {Properties::Types::townHall, Properties::Types::taxOffice};
    trait.iconId = IconDef::currencyBuilding;
    return trait;
}

constexpr static FixedTrait footPaths(void) {
    FixedTrait trait;
    trait.description = "Make walking around easier. Its almost "
                        "as if roads were not in the annual budget. ";
    trait.isBuildable = true;
    trait.isStackable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::currency;

    trait.provinceStatFunc = [](const StaticTrait & trait, provstat_t count) -> ProvStatModSet {
        return ProvStatModSet({
            {ProvStatIds::currencyIncome, ProvStatMod(35*count, 0)}, // value, percentage
            {ProvStatIds::landUsed,       ProvStatMod(8*count, 0)},
            {ProvStatIds::provinceValue,  ProvStatMod(5*count, 0)}
        });
    };
    trait.buildCost[PlayerStatIds::currency]    = 800;
    trait.demolishCost[PlayerStatIds::currency] = 800;
    trait.costRequiredTraits = {Properties::Types::taxOffice};
    trait.iconId = IconDef::currencyBuilding;
    return trait;
}

constexpr static FixedTrait postbox(void) {
    FixedTrait trait;
    trait.description = "Send things to other places via a magical "
                        "sub-surface network of cats and goblins. Sometimes "
                        "rejects items over the stamp band limit. ";
    trait.isBuildable = true;
    trait.isStackable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::currency;

    trait.provinceStatFunc = [](const StaticTrait & trait, provstat_t count) -> ProvStatModSet {
        return ProvStatModSet({
            {ProvStatIds::currencyIncome, ProvStatMod(4*count, 0)}, // value, percentage
            {ProvStatIds::landUsed,       ProvStatMod(1*count, 0)},
            {ProvStatIds::provinceValue,  ProvStatMod(1*count, 0)}
        });
    };
    trait.buildCost[PlayerStatIds::currency]    = 100;
    trait.demolishCost[PlayerStatIds::currency] = 100;
    trait.costRequiredTraits = {Properties::Types::footPaths};
    trait.iconId = IconDef::currencyBuilding;
    return trait;
}

constexpr static FixedTrait publicToilets(void) {
    FixedTrait trait;
    trait.description = "Open-fronted toilet stalls so that you can release "
                        "waste in a natural setting. ";
    trait.isBuildable = true;
    trait.isStackable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::manpower;

    trait.provinceStatFunc = [](const StaticTrait & trait, provstat_t count) -> ProvStatModSet {
        return ProvStatModSet({
            {ProvStatIds::manpowerIncome, ProvStatMod(25*count, 0)}, // value, percentage
            {ProvStatIds::landUsed,       ProvStatMod(1*count, 0)},
            {ProvStatIds::provinceValue,  ProvStatMod(1*count, 0)}
        });
    };
    trait.buildCost[PlayerStatIds::currency]    = 125;
    trait.demolishCost[PlayerStatIds::currency] = 125;
    trait.costRequiredTraits = {Properties::Types::housingProject};
    trait.iconId = IconDef::manpowerBuilding;
    return trait;
}

constexpr static FixedTrait farm(void) {
    FixedTrait trait;
    trait.description = "A large patch of land which contains many green things "
                        "that can be converted into biscuits and bone. Keeps "
                        "people fed at least.";
    trait.isBuildable = true;
    trait.isStackable = true;
    trait.displayMode = TraitDef::DisplayMode::building;
    trait.buildingCategory = TraitDef::BuildingCategories::manpower;

    trait.provinceStatFunc = [](const StaticTrait & trait, provstat_t count) -> ProvStatModSet {
        return ProvStatModSet({
            {ProvStatIds::manpowerIncome, ProvStatMod(90*count, 0)}, // value, percentage
            {ProvStatIds::landUsed,       ProvStatMod(2*count, 0)},
            {ProvStatIds::provinceValue,  ProvStatMod(2*count, 0)}
        });
    };
    trait.buildCost[PlayerStatIds::currency]    = 250;
    trait.demolishCost[PlayerStatIds::currency] = 250;
    trait.costRequiredTraits = {Properties::Types::recruitmentOffice};
    trait.iconId = IconDef::manpowerBuilding;
    return trait;
}

/*
 * InternalTraitDef.inl
 *
 *  Created on: 4 Oct 2020
 *      Author: wilson
 */

/**
 * Provides the definitions for InternalTrait, and provides
 * all the various constexpr logic to generate the array
 * of traits (StaticTrait).
 * This isn't a separate cpp, so the logic in Traits.cpp can
 * use the definitions here. Think of this file as an
 * extension to Traits.cpp.
 */

#ifndef TRAIT_INTERNAL_TRAIT_DEF_INL
    #error "This file must only be #included within Trait.cpp, to provide InternalTrait implementation."
#endif

#include "InternalTrait.h"

#include "../../../util/templateTypes/staticStorage/StaticStorageHelpers.h"
#include "../../../util/definitions/IconDef.h"
#include "../Properties.h"

#include <cstdlib>
#include <GL/gl.h>

namespace rasc {

    namespace internalTraitData {
        // #include all of the header files containing trait declarations.
        // Make sure they go in their own namespace, to avoid naming conflicts.
        // Make sure we define the macro TRAIT_INTERNAL_DECLARATIONS, so the header
        // files know that they are definitely being #included from InternalTrait.cpp.
        namespace internalTraitDeclarations {
            #define TRAIT_INTERNAL_DECLARATIONS
            #include "traitDeclarations/Combat.h"
            #include "traitDeclarations/CurrencyBuildings.h"
            #include "traitDeclarations/ManpowerBuildings.h"
            #include "traitDeclarations/StackableBuildings.h"
            #include "traitDeclarations/terrainTraits/Climate.h"
            #include "traitDeclarations/terrainTraits/GroundFeatures.h"
            #include "traitDeclarations/terrainTraits/Misc.h"
            #include "traitDeclarations/terrainTraits/Shape.h"
            #include "traitDeclarations/terrainTraits/Water.h"
            #include "traitDeclarations/terrainTraits/Weather.h"
            #include "traitDeclarations/terrainTypes/Cold.h"
            #include "traitDeclarations/terrainTypes/Dry.h"
            #include "traitDeclarations/terrainTypes/Hot.h"
            #include "traitDeclarations/terrainTypes/Humid.h"
            #include "traitDeclarations/terrainTypes/Inaccessible.h"
            #include "traitDeclarations/terrainTypes/Temperate.h"
            #include "traitDeclarations/terrainTypes/Wet.h"
            #include "traitDeclarations/provinceBuildings/LandShape.h"
            #include "traitDeclarations/provinceBuildings/Misc.h"
            #include "traitDeclarations/provinceBuildings/Reclamation.h"
            #include "traitDeclarations/provinceBuildings/Trees.h"
            #undef TRAIT_INTERNAL_DECLARATIONS
        }
        
        // Main constexpr method to get the array of trait data as fixed
        // traits, running all the trait declaration functions included
        // in the headers above.
        constexpr static std::array<FixedTrait, TraitRange::COUNT> getFixedTraits(void) {
            // Use X macros to build an array of FixedTrait instances, from the functions
            // defined in the trait declaration header files. Using X macros ensures consistent
            // ordering/count, and ensures that this will fail to compile if trait declaration
            // functions are missing.
            #define X(name, baseType) internalTraitDeclarations::name(),
            std::array<FixedTrait, TraitRange::COUNT> traitsArr = { RASC_TRAITS_LIST };
            #undef X

            // Accesses an internal trait by trait ID. This is required due to the TraitRange offset.
            #define GET_INTERNAL_TRAIT(traitId) \
                traitsArr[(traitId) - TraitRange::MIN]

            for (size_t i = 0; i < traitsArr.size(); i++) {
                const FixedTrait & source = traitsArr[i];
                proptype_t sourceTraitId = i + TraitRange::MIN;

                // Make sure blocking traits are symmetric.
                for (proptype_t blockingTraitId : source.blockingTraits) {
                    GET_INTERNAL_TRAIT(blockingTraitId).addBlocking(sourceTraitId);
                }

                // Make sure each trait listed as required, lists the source trait as allowing.
                for (proptype_t requiredTraitId : source.requiredTraits) {
                    GET_INTERNAL_TRAIT(requiredTraitId).addAllowing(sourceTraitId);
                }

                // Make sure each trait listed as allowing, lists the source trait as required.
                for (proptype_t allowingTraitId : source.allowingTraits) {
                    GET_INTERNAL_TRAIT(allowingTraitId).addRequired(sourceTraitId);
                }

                // Make sure each trait listed as costRequired, lists the source trait as costAllowing.
                for (proptype_t costRequiredTraitId : source.costRequiredTraits) {
                    GET_INTERNAL_TRAIT(costRequiredTraitId).addCostAllowing(sourceTraitId);
                }

                // Make sure each trait listed as costAllowing, lists the source trait as costRequired.
                for (proptype_t costAllowingTraitId : source.costAllowingTraits) {
                    GET_INTERNAL_TRAIT(costAllowingTraitId).addCostRequired(sourceTraitId);
                }
            }

            return traitsArr;
        }
        
        // Define static storage buffers for the trait data.
        STATIC_STORAGE_DEFINE(TerDispLayer, terDispLayerData, getFixedTraits())
        STATIC_STORAGE_DEFINE(proptype_t, propTypeData, getFixedTraits())
        STATIC_STORAGE_DEFINE(ProvStatModPair, provStatModPairData, getFixedTraits())
        
        // Constexpr method to build static traits.
        constexpr static std::array<StaticTrait, TraitRange::COUNT> getStaticTraits(void) {
            std::array<StaticTrait, TraitRange::COUNT> traitsArr;
            
            // Populate static trait vectors from static storage buffers above.
            STATIC_STORAGE_POPULATE(TerDispLayer, traitsArr, terDispLayerData, getFixedTraits())
            STATIC_STORAGE_POPULATE(proptype_t, traitsArr, propTypeData, getFixedTraits())
            STATIC_STORAGE_POPULATE(ProvStatModPair, traitsArr, provStatModPairData, getFixedTraits())
            
            // Copy over other non-vector data (within BaseInternalTrait)
            // for each trait. 
            const std::array<FixedTrait, TraitRange::COUNT> fixedTraits = getFixedTraits();
            for (size_t index = 0; index < TraitRange::COUNT; index++) {
                static_cast<BaseInternalTrait&>(traitsArr[index]) = static_cast<const BaseInternalTrait&>(fixedTraits[index]);
            }

            return traitsArr;
        }
        
        // The traits array with packed StaticVector storage (this one is exposed to the outside).
        constexpr static std::array<StaticTrait, TraitRange::COUNT> staticTraits = getStaticTraits();
    }
    
    bool InternalTraitHelpers::isBetterValueThan(const StaticTrait & self, const StaticTrait & other) {
        // Find (currency) build cost and stat sets for us and them.
        provstat_t ourCost = self.buildCost[PlayerStatIds::currency],
                 theirCost = other.buildCost[PlayerStatIds::currency];
        ProvStatModSet ourStatSet = self.getStatModifierSet(1),
                     theirStatSet = other.getStatModifierSet(1);

        // From these work out stat modifiers, based on building category.
        ProvStatMod ourStats, theirStats;
        if (self.buildingCategory == TraitDef::BuildingCategories::currency) {
            ourStats = ourStatSet.getProvStatMod(ProvStatIds::currencyIncome) - ourStatSet.getProvStatMod(ProvStatIds::currencyExpenditure);
            theirStats = theirStatSet.getProvStatMod(ProvStatIds::currencyIncome) - theirStatSet.getProvStatMod(ProvStatIds::currencyExpenditure);
        } else {
            ourStats = ourStatSet.getProvStatMod(ProvStatIds::manpowerIncome) - ourStatSet.getProvStatMod(ProvStatIds::manpowerExpenditure);
            theirStats = theirStatSet.getProvStatMod(ProvStatIds::manpowerIncome) - theirStatSet.getProvStatMod(ProvStatIds::manpowerExpenditure);
        }
        // First compare percentage value for money. If there's a difference, return it.
        GLfloat ourValuePerCost = ((GLfloat)ourStats.getPercentage()) / ourCost,
              theirValuePerCost = ((GLfloat)theirStats.getPercentage()) / theirCost;
        if (ourValuePerCost != theirValuePerCost) return ourValuePerCost > theirValuePerCost;
        // Then compare "value" value for money, and return it.
        ourValuePerCost = ((GLfloat)ourStats.getValue()) / ourCost,
        theirValuePerCost = ((GLfloat)theirStats.getValue()) / theirCost;
        return ourValuePerCost > theirValuePerCost;
    }
    
    ProvStatModSet InternalTraitHelpers::getBaseStats(const StaticTrait & trait, provstat_t count) {
        return ProvStatModSet(trait.baseStats);
    }
}

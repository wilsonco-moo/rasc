/*
 * InternalTrait.h
 *
 *  Created on: 8 Aug 2023
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROPERTIES_TRAIT_INTERNALTRAIT_INL_
#define BASEGAME_COMMON_GAMEMAP_PROPERTIES_TRAIT_INTERNALTRAIT_INL_

#include "InternalTrait.h"

#include "../../../util/definitions/IconDef.h"

namespace rasc {

    constexpr BaseInternalTrait::BaseInternalTrait(void) :
        displayMode(TraitDef::DisplayMode::terrainTrait),
        provinceStatFunc(&InternalTraitHelpers::getBaseStats),
        buildCost(),
        demolishCost(),
        buildingCategory(TraitDef::BuildingCategories::none),
        isStackable(false),
        isBuildable(false),
        lessThanZeroAllowed(false),
        constructionAllowedDuringBattle(false),
        isAccessible(true),
        description(""),
        iconId(IconDef::DEFAULT) {
    }
        
    template<template<typename, size_t> class VectorType>
    constexpr InternalTrait<VectorType>::InternalTrait(void) :
        BaseInternalTrait(),
        layers(),
        requiredTraits(),
        allowingTraits(),
        costRequiredTraits(),
        costAllowingTraits(),
        blockingTraits(),
        baseStats() {
    }
        
    template<template<typename, size_t> class VectorType>
    bool InternalTrait<VectorType>::isBetterValueThan(const StaticTrait & other) const {
        return InternalTraitHelpers::isBetterValueThan(*this, other);
    }
    
    // At least with g++ 8.3.0, unfortunately a simple loop seems to
    // produce a much smaller and simpler assembly output than an
    // equivalent call to std::find. Either way, std::find isn't
    // constexpr until c++20.
    #define INTERNAL_TRAIT_RETURN_FIND_VECTOR(vec, value) \
        for (size_t i = 0; i < (vec).size(); i++) {       \
            if ((vec)[i] == (value)) {                    \
                return true;                              \
            }                                             \
        }                                                 \
        return false;

    template<template<typename, size_t> class VectorType>
    constexpr bool InternalTrait<VectorType>::hasRequired(proptype_t trait) const {
        INTERNAL_TRAIT_RETURN_FIND_VECTOR(requiredTraits, trait)
    }
    template<template<typename, size_t> class VectorType>
    constexpr bool InternalTrait<VectorType>::hasBlocking(proptype_t trait) const {
        INTERNAL_TRAIT_RETURN_FIND_VECTOR(blockingTraits, trait)
    }
    template<template<typename, size_t> class VectorType>
    constexpr bool InternalTrait<VectorType>::hasAllowing(proptype_t trait) const {
        INTERNAL_TRAIT_RETURN_FIND_VECTOR(allowingTraits, trait)
    }
    template<template<typename, size_t> class VectorType>
    constexpr bool InternalTrait<VectorType>::hasCostRequired(proptype_t trait) const {
        INTERNAL_TRAIT_RETURN_FIND_VECTOR(costRequiredTraits, trait)
    }
    template<template<typename, size_t> class VectorType>
    constexpr bool InternalTrait<VectorType>::hasCostAllowing(proptype_t trait) const {
        INTERNAL_TRAIT_RETURN_FIND_VECTOR(costAllowingTraits, trait)
    }
    
    #undef INTERNAL_TRAIT_RETURN_FIND_VECTOR
    
    template<template<typename, size_t> class VectorType>
    constexpr void InternalTrait<VectorType>::addRequired(proptype_t trait) {
        if (!hasRequired(trait)) requiredTraits.push_back(trait);
    }
    template<template<typename, size_t> class VectorType>
    constexpr void InternalTrait<VectorType>::addBlocking(proptype_t trait) {
        if (!hasBlocking(trait)) blockingTraits.push_back(trait);
    }
    template<template<typename, size_t> class VectorType>
    constexpr void InternalTrait<VectorType>::addAllowing(proptype_t trait) {
        if (!hasAllowing(trait)) allowingTraits.push_back(trait);
    }
    template<template<typename, size_t> class VectorType>
    constexpr void InternalTrait<VectorType>::addCostRequired(proptype_t trait) {
        if (!hasCostRequired(trait)) costRequiredTraits.push_back(trait);
    }
    template<template<typename, size_t> class VectorType>
    constexpr void InternalTrait<VectorType>::addCostAllowing(proptype_t trait) {
        if (!hasCostAllowing(trait)) costAllowingTraits.push_back(trait);
    }
    
    template<template<typename, size_t> class VectorType>
    ProvStatModSet InternalTrait<VectorType>::getStatModifierSet(provstat_t count) const {
        return provinceStatFunc(*this, count);
    }
    
    template<template<typename, size_t> class VectorType>
    template<typename Query>
    constexpr void InternalTrait<VectorType>::query(Query & query) {
        // All vector members must be listed here.
        query.run(layers);
        query.run(requiredTraits);
        query.run(allowingTraits);
        query.run(costRequiredTraits);
        query.run(costAllowingTraits);
        query.run(blockingTraits);
        query.run(baseStats);
    }
}

#endif

/*
 * InternalTrait.h
 *
 *  Created on: 4 Oct 2020
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_PROPERTIES_TRAIT_INTERNALTRAIT_H_
#define BASEGAME_COMMON_GAMEMAP_PROPERTIES_TRAIT_INTERNALTRAIT_H_

#include <functional>
#include <string>
#include <vector>

#include "../../../util/templateTypes/staticStorage/StaticVector.h"
#include "../../../util/templateTypes/staticStorage/FixedVector.h"
#include "../../../util/definitions/TerrainDisplayDef.h"
#include "../../../playerData/PlayerStatArr.h"
#include "../../provinceUtil/ProvStatMod.h"
#include "../PropertyDef.h"
#include "TraitRange.h"
#include "TraitDef.h"

namespace rasc {
    // Forward declares and typedefs.
    
    template<template<typename, size_t> class VectorType>
    class InternalTrait;
    
    /**
     * InternalTrait using FixedVector. Only for use internally for
     * constexpr initialisation.
     */
    using FixedTrait = InternalTrait<FixedVector>;
    
    /**
     * InternalTrait using StaticVector. This is what is exposed
     * for general trait usage.
     */
    using StaticTrait = InternalTrait<SizedStaticVector>;
    
    /**
     * Internal trait base data (i.e: stuff which isn't vectors
     * and doesn't need to be part of the templated version below).
     */
    class BaseInternalTrait {
    public:
        // The display mode for this trait.
        TraitDef::DisplayMode displayMode;

        // This should return an appropriate set of province stat modifiers depending on the provided count value.
        // Note that there is no requirement that this function returns a zero ProvStatMod when the count is zero,
        // because Traits::getProvinceStatModiferSet does that automatically.
        // NOTE: This must ALWAYS return THE SAME province stat modifiers, whenever the SAME count is given.
        //       Randomness is NOT ALLOWED.
        // The default implementation of this returns baseStats. This is helpful (as convenience) for non-stackable traits.
        using ProvinceStatFunc = ProvStatModSet (*)(const StaticTrait & trait, provstat_t count);
        ProvinceStatFunc provinceStatFunc;

        // The cost to BUILD or demolish respectively, ONE of these traits, (if relevant).
        PlayerStatArr buildCost, demolishCost;

        // The category of this building. Note that this is only relevant for traits with the building display mode.
        TraitDef::BuildingCategory buildingCategory;

        // Whether or not this building is stackable. BY DEFAULT BUILDINGS ARE NOT STACKABLE.
        bool isStackable;

        // Whether or not users should be allowed to build this trait.
        bool isBuildable;

        // Whether negative trait values are allowed for this trait. By default this is false.
        bool lessThanZeroAllowed;

        // Whether construction or demolition of this trait is allowed while a battle is ongoing.
        // Note that this is only relevant for traits which are buildings. By default this is false.
        bool constructionAllowedDuringBattle;

        // This is only relevant for terrain types. This specifies whether the province, (containing
        // this type), should be accessible. By default this is true.
        bool isAccessible;

        // A longer description than the name of the trait. By default this is empty.
        const char * description;

        // The icon associated with the trait.
        unsigned int iconId;
        
        /**
         * The default constructor initialises all fields to default values.
         */
        constexpr BaseInternalTrait(void);
    };
    
    /**
     * Templated extension to BaseInternalTrait, containing all
     * vector trait data.
     */
    template<template<typename, size_t> class VectorType>
    class InternalTrait : public BaseInternalTrait {
    public:
        constexpr static size_t ARRAY_SIZE = 20;
        
        // The texture layers for drawing this trait, in the province menu.
        VectorType<TerDispLayer, ARRAY_SIZE> layers;
        
        // All properties specified in this list must be present, before this trait can be built.
        // Note: All of these properties must be traits, not non-trait properties.
        // Note: This is automatically populated with all traits which list this trait as allowing.
        // Note: This is not an unordered_set, since we are unlikely to get any speed advantage considering the
        //       small number of things this is likely to contain.
        VectorType<proptype_t, ARRAY_SIZE> requiredTraits;

        // This stores a list of traits which list this trait as required.
        // Note: This is automatically populated with all traits which list this trait as required.
        // Note: This is not an unordered_set, since we are unlikely to get any speed advantage considering the
        //       small number of things this is likely to contain.
        VectorType<proptype_t, ARRAY_SIZE> allowingTraits;

        // Cost-required and cost-allowing traits are a superset of required and allowing traits.
        // Compared to required, for a trait to be cost-required, a couple of additional conditions must be met:
        //  > Both the source trait and cost-required traits must be regular (not combat or none type) buildings.
        //  > The cost-required trait is "better value" in all circumstances. This means it is never
        //    the case that it makes more financial sense to build this trait BEFORE the cost-required one.
        //  > The cost-required trait's required and blocking traits are a subset of the source trait's required
        //    and blocking traits.
        // Note: It is not always the case that a trait cost-requires another, if it requires it.
        //       The additional cost-required relation may be redundant due to transitivity.
        VectorType<proptype_t, ARRAY_SIZE> costRequiredTraits,
                                           costAllowingTraits;

        // If any of the properties specified in this list are present, the trait cannot be built.
        // Note: All of these properties must be traits, not non-trait properties.
        // Note: Blocking is a symmetric relation - this is automatically populated with all
        //       traits which list this trait as blocking.
        // Note: This is not an unordered_set, since we are unlikely to get any speed advantage considering the
        //       small number of things this is likely to contain.
        VectorType<proptype_t, ARRAY_SIZE> blockingTraits;
        
        // The base province stat modifiers, returned by the default implementation of provinceStatFunc.
        // This is helpful for convenience, for non-stackable traits. By default this is default-constructed.
        VectorType<ProvStatModPair, ARRAY_SIZE> baseStats;

        /**
         * The default constructor initialises all fields to default values.
         */
        constexpr InternalTrait(void);

        /**
         * Returns true if we are better value than the other trait.
         */
        inline bool isBetterValueThan(const StaticTrait & other) const;

        /**
         * Returns true if the specified other trait is listed in our required,
         * blocking or allowing traits respectively.
         */
        constexpr bool hasRequired(proptype_t trait) const;
        constexpr bool hasBlocking(proptype_t trait) const;
        constexpr bool hasAllowing(proptype_t trait) const;
        constexpr bool hasCostRequired(proptype_t trait) const;
        constexpr bool hasCostAllowing(proptype_t trait) const;

        /**
         * Adds the required, blocking or allowing traits, but only if they
         * are not already listed.
         */
        constexpr void addRequired(proptype_t trait);
        constexpr void addBlocking(proptype_t trait);
        constexpr void addAllowing(proptype_t trait);
        constexpr void addCostRequired(proptype_t trait);
        constexpr void addCostAllowing(proptype_t trait);

        /**
         * Helper method to get our province stat modifier set, using the specified trait
         * count. This calls our provinceStatFunc appropriately to get the ProvStatModSet.
         * (More convenient than doing internalTrait.provinceStatFunc(internalTrait, count))
         */
        inline ProvStatModSet getStatModifierSet(provstat_t count) const;
        
        /**
         * Query method for static storage initialisation.
         * All vector members must be listed here.
         */
        template<typename Query>
        constexpr void query(Query & query);
    };
    
    /**
     * Static helper methods for internal trait.
     */
    struct InternalTraitHelpers {
        /**
         * Returns true if the specified trait is better value than the other.
         * Probably more convenient to use InternalTrait::isBetterValueThan.
         */
        static bool isBetterValueThan(const StaticTrait & self, const StaticTrait & other);
        
        /**
         * Gets the raw base stats stored in the static trait, converting
         * it to a ProvStatModSet. This is the default province stat function
         * used by BaseInternalTrait.
         */
        static ProvStatModSet getBaseStats(const StaticTrait & trait, provstat_t count);
    };
}

#include "InternalTrait.inl"

#endif

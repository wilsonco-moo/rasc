/*
 * PropertySet.cpp
 *
 *  Created on: 9 Jun 2020
 *      Author: wilson
 */

#include "PropertySet.h"

#include <sockets/plus/message/Message.h>
#include <iostream>
#include <limits>

#include "Properties.h"

namespace rasc {

    PropertySet::TypeAndStorage::TypeAndStorage(void) {
    }

    PropertySet::TypeAndStorage::TypeAndStorage(proptype_t type, PropertyStorage storage) :
        type(type),
        storage(storage) {
    }

    // -----------------------------------------------------------

    PropertySet::PropertySet(CommonBox & box, Province & province) :
        box(box),
        province(province),
        propertiesByUniqueId(),
        propertiesByType(),
        needUpdatingIds() {
    }

    PropertySet::PropertySet(CommonBox & box, Province & province, simplenetwork::Message & msg) :
        PropertySet(box, province) {
        readFrom(msg);
    }

    PropertySet::~PropertySet(void) {
        clear();
    }

    // ============================== Misc methods and macros ================================

    /**
     * Complains and returns if either the specified property unique ID already
     * exists, or if the specified property type is invalid.
     */
    #define RETURN_IF_CANNOT_BE_ADDED(propertyUniqueId, propType)                                                                         \
        /* Complain if the property type is invalid.                                                                                   */ \
        if (!Properties::isValidProperty(propType)) {                                                                                     \
            std::cerr << "WARNING: PropertySet: " << "Tried to add invalid property type: " << (unsigned int)(propType) << ".\n";         \
            return false;                                                                                                                 \
        }                                                                                                                                 \
                                                                                                                                          \
        /* Complain if the ID is not unique.                                                                                           */ \
        if (propertiesByUniqueId.find(propertyUniqueId) != propertiesByUniqueId.end()) {                                                  \
            std::cerr << "WARNING: PropertySet: " << "Tried to add property ID " << (propertyUniqueId) << " more than once.\n";           \
            return false;                                                                                                                 \
        }

    void PropertySet::addPropertyInternal(uint64_t uniqueId, proptype_t propertyType, PropertyStorage property) {
        propertiesByUniqueId.emplace(uniqueId, TypeAndStorage(propertyType, property));
        propertiesByType[propertyType].insert(uniqueId);
        if (Properties::needsUpdating(box, province, propertyType, property)) {
            needUpdatingIds.insert(uniqueId);
        }
    }

    void PropertySet::deletePropertyInternal(std::unordered_map<uint64_t, TypeAndStorage>::iterator byUniqueIdIter) {
        // Delete the property with the method from Properties.
        Properties::deleteProperty(box, province, byUniqueIdIter->second.getType(), byUniqueIdIter->second.getStorage());

        // Erase from properties by type. If that leaves an empty set for that type, remove it.
        auto byTypeIter = propertiesByType.find(byUniqueIdIter->second.getType());
        byTypeIter->second.erase(byUniqueIdIter->first);
        if (byTypeIter->second.empty()) {
            propertiesByType.erase(byTypeIter);
        }

        // Erase from the other two maps.
        needUpdatingIds.erase(byUniqueIdIter->first);
        propertiesByUniqueId.erase(byUniqueIdIter);
    }

    // ==================== Property modification (from messages) ============================

    bool PropertySet::addPropertyFromMessage(simplenetwork::Message & msg) {
        // Read ID and property type.
        uint64_t uniqueId; proptype_t propertyType;
        Properties::readTypeAndId(msg, propertyType, uniqueId);

        // Ensure that this property is valid and *can* be added.
        RETURN_IF_CANNOT_BE_ADDED(uniqueId, propertyType)

        // If so, create the property using the method from Properties.
        PropertyStorage property = Properties::createProperty(box, province, msg, propertyType);

        // Add it to our internal data structures.
        addPropertyInternal(uniqueId, propertyType, property);
        return true;
    }

    bool PropertySet::addPropertyFromMessage(simplenetwork::Message & msg, ProvStatModSet & addedStatModifier, uint64_t & uniqueIdReturn) {
        // Read ID and property type.
        uint64_t uniqueId; proptype_t propertyType;
        Properties::readTypeAndId(msg, propertyType, uniqueId);

        // Ensure that this property is valid and *can* be added.
        RETURN_IF_CANNOT_BE_ADDED(uniqueId, propertyType)

        // If so, create the property using the method from Properties.
        PropertyStorage property = Properties::createProperty(box, province, msg, propertyType);

        // Add it to our internal data structures.
        addPropertyInternal(uniqueId, propertyType, property);

        // Now that we know adding the property was successful, get/return stat modifier set and unique Id.
        addedStatModifier = Properties::getStatModifierSet(box, province, propertyType, property);
        uniqueIdReturn = uniqueId;
        return true;
    }

    bool PropertySet::readPerTurnUpdate(simplenetwork::Message & msg, ProvStatModSet & addedStatModifier) {
        // First subtract the stat modifier effects of all properties which require updating.
        for (uint64_t uniqueId : needUpdatingIds) {
            TypeAndStorage & ts = propertiesByUniqueId.at(uniqueId);
            addedStatModifier -= Properties::getStatModifierSet(box, province, ts.getType(), ts.getStorage());
        }

        // Get the count specified by the message and complain if it is wrong.
        uint32_t count = msg.read32();
        if (count != needUpdatingIds.size()) {
            std::cerr << "WARNING: PropertySet: " << "received wrong number of property per-turn updates, expecting: " << needUpdatingIds.size() << ", found: " << count << ".\n";
            return false;
        }

        // Make a copy of the IDs which need updating, so we can remove them to ensure ALL got updates.
        std::unordered_set<uint64_t> leftToUpdate = needUpdatingIds;

        // Repeat the specified number of times.
        for (uint32_t i = 0; i < count; i++) {

            // Get the unique ID of the property and erase it from the set of remaining IDs to update.
            // If erasing did not remove an element, then an error has happened so complain and return false.
            uint64_t uniqueId = msg.read64();
            if (leftToUpdate.erase(uniqueId) != 1) {
                std::cerr << "WARNING: PropertySet: " << "property ID " << uniqueId << " found when reading a per-turn update. This either does not exist, does not require updating, or was updated twice.\n";
                return false;
            }

            // Run the properties method to apply the per-turn update. We know that this ID is valid,
            // since it was in the leftToUpdate set.
            TypeAndStorage & ts = propertiesByUniqueId.at(uniqueId);
            Properties::receiveUpdate(box, province, msg, ts.getType(), ts.getStorage());
        }

        // There should no longer be any property IDs left to update. This ought not to happen.
        if (!leftToUpdate.empty()) {
            std::cerr << "WARNING: PropertySet: " << "not all properties were updated by a per-turn update.\n";
            return false;
        }

        // Finally, add the stat modifier effect of all properties which require updating. This will
        // result in addedStatModifier representing the ADDED stat modifier CHANGE.
        for (uint64_t uniqueId : needUpdatingIds) {
            TypeAndStorage & ts = propertiesByUniqueId.at(uniqueId);
            addedStatModifier += Properties::getStatModifierSet(box, province, ts.getType(), ts.getStorage());
        }
        return true;
    }

    bool PropertySet::readDirectUpdate(simplenetwork::Message & msg, ProvStatModSet & addedStatModifier) {
        // Find the property, complain if it doesn't exist.
        uint64_t uniqueId = msg.read64();
        auto iter = propertiesByUniqueId.find(uniqueId);
        if (iter == propertiesByUniqueId.end()) {
            std::cerr << "WARNING: PropertySet: " << "received a direct update for unknown property ID: " << uniqueId << ".\n";
            return false;
        }

        // First subtract the property's old stat modifier, do the direct update, then add the
        // property's new stat modifier. This means addedStatModifier will represent the ADDED
        // stat modifier CHANGE.
        addedStatModifier -= Properties::getStatModifierSet(box, province, iter->second.getType(), iter->second.getStorage());
        Properties::receiveDirectUpdate(box, province, msg, iter->second.getType(), iter->second.getStorage());
        addedStatModifier += Properties::getStatModifierSet(box, province, iter->second.getType(), iter->second.getStorage());
        return true;
    }

    bool PropertySet::deletePropertyFromMessage(simplenetwork::Message & msg, ProvStatModSet & subtractedStatModifier) {
        return deleteProperty(msg.read64(), subtractedStatModifier);
    }

    // ========================== Property modification (direct) =============================


    bool PropertySet::addProperty(uint64_t uniqueId, proptype_t propertyType, PropertyStorage property) {
        // Ensure that this property is valid and *can* be added.
        RETURN_IF_CANNOT_BE_ADDED(uniqueId, propertyType)

        // If so, add to our internal data structures.
        addPropertyInternal(uniqueId, propertyType, property);
        return true;
    }

    bool PropertySet::deleteProperty(uint64_t uniqueId) {
        auto iter = propertiesByUniqueId.find(uniqueId);

        // Complain if the property ID does not seem to exist.
        if (iter == propertiesByUniqueId.end()) {
            std::cerr << "WARNING: PropertySet: " << "tried to delete property ID " << uniqueId << ", which does not seem to exist.\n";
            return false;

        // If it *does* exist, delete using our internal method.
        } else {
            deletePropertyInternal(iter);
            return true;
        }
    }

    bool PropertySet::deleteProperty(uint64_t uniqueId, ProvStatModSet & subtractedStatModifier) {
        auto iter = propertiesByUniqueId.find(uniqueId);

        // Complain if the property ID does not seem to exist.
        if (iter == propertiesByUniqueId.end()) {
            std::cerr << "WARNING: PropertySet: " << "tried to delete property ID " << uniqueId << ", which does not seem to exist.\n";
            return false;

        // If it *does* exist, get the stat modifier then delete using our internal method.
        } else {
            subtractedStatModifier = Properties::getStatModifierSet(box, province, iter->second.getType(), iter->second.getStorage());
            deletePropertyInternal(iter);
            return true;
        }
    }

    void PropertySet::clear(void) {
        // Delete each property with the appropriate method from Properties.
        for (std::pair<const uint64_t, TypeAndStorage> & pair : propertiesByUniqueId) {
            Properties::deleteProperty(box, province, pair.second.getType(), pair.second.getStorage());
        }

        // Clear data structures.
        propertiesByUniqueId.clear();
        propertiesByType.clear();
        needUpdatingIds.clear();
    }


    // ============================== Property lookup methods ================================

    bool PropertySet::propertyUniqueIdExists(uint64_t uniqueId) const {
        return propertiesByUniqueId.find(uniqueId) != propertiesByUniqueId.end();
    }

    PropertySet::TypeAndStorage * PropertySet::getPropertyStorage(uint64_t uniqueId) {
        auto iter = propertiesByUniqueId.find(uniqueId);
        if (iter == propertiesByUniqueId.end()) {
            return NULL;
        } else {
            return &(iter->second);
        }
    }

    const PropertySet::TypeAndStorage * PropertySet::getPropertyStorage(uint64_t uniqueId) const {
        auto iter = propertiesByUniqueId.find(uniqueId);
        if (iter == propertiesByUniqueId.end()) {
            return NULL;
        } else {
            return &(iter->second);
        }
    }

    PropertySet::TypeAndStorage & PropertySet::getPropertyStorageUnchecked(uint64_t uniqueId) {
        return propertiesByUniqueId.at(uniqueId);
    }

    const PropertySet::TypeAndStorage & PropertySet::getPropertyStorageUnchecked(uint64_t uniqueId) const {
        return propertiesByUniqueId.at(uniqueId);
    }

    const std::unordered_set<uint64_t> * PropertySet::getPropertiesByType(proptype_t propertyType) const {
        auto iter = propertiesByType.find(propertyType);
        if (iter == propertiesByType.end()) {
            return NULL;
        } else {
            return &(iter->second);
        }
    }

    bool PropertySet::hasPropertiesOfType(proptype_t propertyType) const {
        return propertiesByType.find(propertyType) != propertiesByType.end();
    }

    proptype_t PropertySet::getPropertyType(uint64_t uniqueId) const {
        auto iter = propertiesByUniqueId.find(uniqueId);
        if (iter == propertiesByUniqueId.end()) {
            return Properties::Types::INVALID;
        } else {
            return iter->second.getType();
        }
    }


    // ============================== Network methods ========================================

    void PropertySet::writeTo(simplenetwork::Message & msg, bool isSaving) {
        // Write the property count. Complain in the exceedingly unlikely
        // event that the size cannot be represented by a uint32_t.
        if (propertiesByUniqueId.size() > (size_t)std::numeric_limits<uint32_t>::max()) {
            std::cerr << "WARNING: PropertySet: " << " cannot write to message as we have more than " << std::numeric_limits<uint32_t>::max() << " properties.\n";
            msg.write32(0);
            return;
        }
        msg.write32((uint32_t)propertiesByUniqueId.size());

        // Write the intial data (along with type and ID), of each property.
        for (std::pair<const uint64_t, TypeAndStorage> & pair : propertiesByUniqueId) {
            Properties::writeInitialDataTypeAndId(box, province, msg, isSaving,
                                                  pair.first, pair.second.getType(), pair.second.getStorage());
        }
    }

    bool PropertySet::readFrom(simplenetwork::Message & msg) {
        clear();
        uint32_t size = msg.read32();
        for (uint32_t i = 0; i < size; i++) {
            if (!addPropertyFromMessage(msg)) return false;
        }
        return true;
    }

    bool PropertySet::readFrom(simplenetwork::Message & msg, ProvStatModSet & statModifier) {
        clear();
        uint32_t size = msg.read32();
        ProvStatModSet newStatModifier;
        for (uint32_t i = 0; i < size; i++) {
            uint64_t uniqueId;
            if (!addPropertyFromMessage(msg, newStatModifier, uniqueId)) return false;
            statModifier += newStatModifier;
        }
        return true;
    }

    void PropertySet::writePerTurnUpdate(simplenetwork::Message & msg) {
        // Write the count of properties which need updating. Complain in the exceedingly unlikely
        // event that the size cannot be represented by a uint32_t.
        if (needUpdatingIds.size() > (size_t)std::numeric_limits<uint32_t>::max()) {
            std::cerr << "WARNING: PropertySet: " << " cannot write to message as we have more than " << std::numeric_limits<uint32_t>::max() << " properties which need updating.\n";
            msg.write32(0);
            return;
        }
        msg.write32((uint32_t)needUpdatingIds.size());

        // Write the basic update data, along with each unique ID.
        for (uint64_t uniqueId : needUpdatingIds) {
            TypeAndStorage & ts = propertiesByUniqueId.at(uniqueId);
            msg.write64(uniqueId);
            Properties::writeUpdate(box, province, msg, ts.getType(), ts.getStorage());
        }

        // Write the additional misc update data (we won't have to read this).
        for (uint64_t uniqueId : needUpdatingIds) {
            TypeAndStorage & ts = propertiesByUniqueId.at(uniqueId);
            Properties::writeMiscUpdate(box, province, msg, ts.getType(), ts.getStorage());
        }
    }


    // ============================== Accessor methods =======================================

    // =================== Wrappers for data access methods from Properties ==================

    void PropertySet::getStatModifierSet(uint64_t uniqueId, ProvStatModSet & statModifier) {
        TypeAndStorage & ts = propertiesByUniqueId.at(uniqueId);
        statModifier = Properties::getStatModifierSet(box, province, ts.getType(), ts.getStorage());
    }

    std::string PropertySet::getFriendlyName(uint64_t uniqueId) {
        TypeAndStorage & ts = propertiesByUniqueId.at(uniqueId);
        return Properties::getFriendlyName(box, province, ts.getType(), ts.getStorage());
    }

    std::string PropertySet::getDescription(uint64_t uniqueId) {
        TypeAndStorage & ts = propertiesByUniqueId.at(uniqueId);
        return Properties::getDescription(box, province, ts.getType(), ts.getStorage());
    }

    bool PropertySet::needsUpdating(uint64_t uniqueId) {
        TypeAndStorage & ts = propertiesByUniqueId.at(uniqueId);
        return Properties::needsUpdating(box, province, ts.getType(), ts.getStorage());
    }

    unsigned int PropertySet::getIconId(uint64_t uniqueId) {
        TypeAndStorage & ts = propertiesByUniqueId.at(uniqueId);
        return Properties::getIconId(box, province, ts.getType(), ts.getStorage());
    }

    void PropertySet::addTerrDispLayers(uint64_t uniqueId, std::vector<TerDispLayer> & layersVec) {
        TypeAndStorage & ts = propertiesByUniqueId.at(uniqueId);
        Properties::addTerrDispLayers(box, province, layersVec, ts.getType(), ts.getStorage());
    }
}

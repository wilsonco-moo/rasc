/*
 * GameMap.h
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_H_
#define BASEGAME_COMMON_GAMEMAP_H_

#include <unordered_map>
#include <GL/gl.h>
#include <cstddef>
#include <cstdint>
#include <utility>
#include <string>
#include <vector>

#include "../update/RascUpdatable.h"
#include "ArmySelectController.h"

namespace tinyxml2 {
    class XMLDocument;
}

namespace rasc {

    class ProvinceBattleController;
    class UniqueIdAssigner;
    class CommonBox;
    class Continent;
    class Province;
    class Area;

    /**
     * GameMap is the class responsible for storing all the Continents, Areas and Provinces,
     * and resultantly any province stats and unit information.
     * It is present on both the server and the client, although each has it's own subclass
     * of GameMap.
     *  - ClientGameMap includes extensions to GameMap for integrating mouse selection,
     *    and drawing the GameMap.
     *  - ServerGameMap includes functionality for setting up server battle controllers,
     *    and any other server specific functionality.
     *
     * Note: As of the mutex redesign of Rasc version 0.503, GameMap no longer has it's own mutex.
     * As a result, GameMap methods no longer have thread-safe synchronised access, so care must be taken outside
     * of GameMap to synchronise access.
     * On the server side:
     *  > This is done since all GameMap accesses are done from within the RascBaseGameServer's killableMutex.
     * On the client side:
     *  > This must be done by synchronising all network operations and drawing operations to RascClientNetIntf's
     *    killableMutex.
     */
    class GameMap : public RascUpdatable {
    private:

        // ----------------------------- MISC -----------------------------------------

        /**
         * Stores the number of playable provinces, areas and continents. These are calculated
         * on the server side when loading map XML file. These are read as initial data by the
         * client when they join, since the client does not load initial map XML province properties.
         */
        uint32_t accessibleProvinceCount,
                 accessibleAreaCount,
                 accessibleContinentCount;

        /**
         * The NAME of the map, read from the XML file. This is set during the loadFromXML method,
         * and is accessible with the getMapName method.
         */
        std::string mapName;

        /**
         * mapRealSize    stores the real size of the map, in pixels, AS IT APPEARS TO THE USER.
         * mapTextureSize stores the size of the TEXTURE SHEETS FOR the map, from which provinces and the minimap are drawn.
         *
         * On the server side, neither of these values are known,
         * so we store dummy values of 32x32, (to avoid any possible divide by zero issues). On the client side,
         * ClientGameMap provides these values to us, which it works out, (in it's constructor),
         * from the size of map textures. These are required to exist, even on the server
         * side, since map elements like provinces need this data, (even if it *is* faked on
         * the server side).
         */
        const std::pair<unsigned int, unsigned int> mapRealSize, mapTextureSize;

        /**
         * The unique ID assigner for use by ProvinceProperties. This should exist only
         * on the server side, and should be assigned to NULL on the client side.
         */
        UniqueIdAssigner * propertiesUniqueIdAssigner;

    protected:
        // ----------------------------- PROPERTIES FROM XML FILE ---------------------

        /**
         * The provinces within this GameMap.
         */
        std::vector<Province *> provinces;
        /**
         * The areas within this GameMap.
         */
        std::vector<Area *> areas;
        /**
         * The continents within this GameMap.
         */
        std::vector<Continent *> continents;

        /**
         * The province id from the colour of the province.
         */
        std::unordered_map<uint32_t, uint32_t> coloursToProvinces;

        /**
         * This controls all army selection.
         */
        ArmySelectController armySelectController;

        /**
         * This allows general access to other parts of the game.
         */
        CommonBox & box;

        // --------------------------------- CONSTRUCTORS ETC -------------------------
    protected:
        GameMap(std::pair<unsigned int, unsigned int> mapRealSize,
                std::pair<unsigned int, unsigned int> mapTextureSize,
                CommonBox & box, UniqueIdAssigner * propertiesUniqueIdAssigner = NULL);
    public:
        GameMap(CommonBox & box, UniqueIdAssigner * propertiesUniqueIdAssigner = NULL);
        virtual ~GameMap(void);

    protected:
        // Initial data saved for the game map is counts of accessible provinces.
        // These must be calculated initially when loading XML on the SERVER, since the client
        // does not load properties from the XML file.
        virtual void onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) override;
        virtual bool onReceiveInitialData(simplenetwork::Message & msg) override;
        
        /**
         * This must create a new province, appropriate to whether this is the client
         * side or server side. This is used during loadFromXML to create provinces of the
         * appropriate type.
         * On the server this should return a ServerProvince.
         * On the client this should return a ClientProvince.
         */
        virtual Province * createNewProvince(void) = 0;
        
    public:
        // --------------------------------- PUBLIC METHODS ---------------------------

        /**
         * This method MUST be called immediately after constructing a GameMap.
         * This loads all province and continent information. This is virtual
         * so ClientGameMap can override this to load client side only additions.
         * TODO: Add a success/failure system, by this method returning a bool value.
         */
        virtual void loadFromXML(tinyxml2::XMLDocument * xml);

        /**
         * Returns how many provinces we have.
         */
        inline uint32_t getProvinceCount(void) const {
            return provinces.size();
        }

        /**
         * Returns the number of accessible provinces, i.e: Those which are not sea, wasteland or similar.
         * On the client side, this will not return a useful value until *after* we have loaded initial data
         * from the server.
         */
        inline uint32_t getAccessibleProvinceCount(void) const {
            return accessibleProvinceCount;
        }

        /**
         * Returns how many areas we have.
         */
        inline uint32_t getAreaCount(void) const {
            return areas.size();
        }

        /**
         * Returns the number of accessible areas, i.e: Those which contain at least one accessible province.
         * On the client side, this will not return a useful value until *after* we have loaded initial data
         * from the server.
         */
        inline uint32_t getAccessibleAreaCount(void) const {
            return accessibleAreaCount;
        }

        /**
         * Returns how many continents we have.
         */
        inline uint32_t getContinentCount(void) const {
            return continents.size();
        }

        /**
         * Returns the number of accessible continents, i.e: Those which contain at least one accessible area.
         * On the client side, this will not return a useful value until *after* we have loaded initial data
         * from the server.
         */
        inline uint32_t getAccessibleContinentCount(void) const {
            return accessibleContinentCount;
        }

        /**
         * These allow read-only access to our provinces, areas an continents. These
         * are provided so that other classes can iterate through them.
         */
        inline const std::vector<Province *> & getProvinces(void) const { return provinces; }
        inline const std::vector<Area *> & getAreas(void) const { return areas; }
        inline const std::vector<Continent *> & getContinents(void) const { return continents; }

        /**
         * This returns a raw pointer to the specified province id.
         * The provided province ID must exist.
         */
        inline Province & getProvince(uint32_t id) const {
            return *provinces[id];
        }

        /**
         * This returns a pointer to the specified area id.
         * The provided area ID must exist.
         */
        inline Area & getAreaRaw(uint32_t id) const {
            return *areas[id];
        }

        /**
         * This returns a raw pointer to the specified continent id.
         * The provided continent ID must exist.
         */
        inline Continent & getContinent(uint32_t id) const {
            return *continents[id];
        }

        /**
         * Returns true if the specified province ID actually exists.
         */
        inline bool provinceExists(uint32_t id) const {
            return id < provinces.size();
        }

        /**
         * Returns true if the specified area ID actually exists.
         */
        inline bool areaExists(uint32_t id) const {
            return id < areas.size();
        }

        /**
         * Returns true if the specified continent ID actually exists.
         */
        inline bool continentExists(uint32_t id) const {
            return id < continents.size();
        }

        /**
         * Returns a reference to our army select controller.
         */
        inline ArmySelectController & getArmySelectController(void) {
            return armySelectController;
        }

        /**
         * Gets the name of the map. This is only defined after calling loadFromXML().
         */
        inline const std::string & getMapName(void) const {
            return mapName;
        }

        /**
         * Returns the REAL WIDTH of the map, in pixels. This returns dummy data on the server side.
         * This is the size of the map, as it appears to the user.
         */
        inline unsigned int getMapRealWidth(void) const {
            return mapRealSize.first;
        }

        /**
         * Returns the REAL HEIGHT of the map, in pixels. This returns dummy data on the server side.
         * This is the size of the map, as it appears to the user.
         */
        inline unsigned int getMapRealHeight(void) const {
            return mapRealSize.second;
        }

        /**
         * Returns the REAL SIZE of the map, in pixels. This returns dummy data on the server side.
         * This is the size of the map, as it appears to the user.
         */
        inline const std::pair<unsigned int, unsigned int> & getMapRealSize(void) const {
            return mapRealSize;
        }

        /**
         * Returns the TEXTURE WIDTH of the map, in pixels. This returns dummy data on the server side.
         * This is the size of the texture sheets for the map, which are used to draw provinces.
         */
        inline unsigned int getMapTextureWidth(void) const {
            return mapTextureSize.first;
        }

        /**
         * Returns the TEXTURE HEIGHT of the map, in pixels. This returns dummy data on the server side.
         * This is the size of the texture sheets for the map, which are used to draw provinces.
         */
        inline unsigned int getMapTextureHeight(void) const {
            return mapTextureSize.second;
        }

        /**
         * Returns the TEXTURE SIZE of the map, in pixels. This returns dummy data on the server side.
         * This is the size of the texture sheets for the map, which are used to draw provinces.
         */
        inline const std::pair<unsigned int, unsigned int> & getMapTextureSize(void) const {
            return mapTextureSize;
        }

        /**
         * Access to the common box, for provinces.
         */
        inline CommonBox & getCommonBox(void) const {
            return box;
        }

        /**
         * Access to the properties unique ID assigner, for province properties.
         */
        inline UniqueIdAssigner * getPropertiesUniqueIdAssigner(void) const {
            return propertiesUniqueIdAssigner;
        }
    };
}

#endif

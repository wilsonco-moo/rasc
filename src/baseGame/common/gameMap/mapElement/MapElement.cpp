/*
 * MapElement.cpp
 *
 *  Created on: 16 Aug 2018
 *      Author: wilson
 */

#include "MapElement.h"

#include <tinyxml2/tinyxml2.h>
#include <wool/font/Font.h>
#include <wool/misc/RGBA.h>
#include <unordered_set>
#include <iostream>
#include <cstdlib>
#include <cstddef>

#include "../../../client/config/MapModeController.h"
#include "../../util/XMLUtil.h"
#include "../GameMap.h"

namespace rasc {

    const uint32_t MapElement::INVALID_MAP_ELEMENT = 0xffffffff;


    MapElement::MapElement(GameMap & gameMap) :
        gameMap(gameMap) {
    }

    void MapElement::loadFromXML(tinyxml2::XMLElement * element) {
        // Read our id
        id = strtol(readAttribute(element, "id"), NULL, 0);

        // Read the properties
        tinyxml2::XMLElement * properties = readElement(element, "properties");


        colour = hexStringToInt(readAttribute(properties, "colour"));


        // Read the province name
        name = std::string(readAttribute(properties, "name"));

        // Read the text rendering element
        tinyxml2::XMLElement * textRendering = readElement(element, "textRendering");

        // Set our text rendering x and y coordinates.
        textRenderingX = (GLfloat)strtol(readAttribute(textRendering, "x"), NULL, 0);
        textRenderingY = (GLfloat)strtol(readAttribute(textRendering, "y"), NULL, 0);
        textRenderingWidth = (GLfloat)strtol(readAttribute(textRendering, "width"), NULL, 0);

        // Read the world element, contained within this is all our texture region coordinates.
        tinyxml2::XMLElement * worldElement = readElement(element, "world");

        // Read the texture element, from within the world element.
        tinyxml2::XMLElement * textureCoords = readElement(worldElement, "texture");

        // Read the texture region's dimensions
        int regionX = strtol(readAttribute(textureCoords, "x"), NULL, 0),
            regionY = strtol(readAttribute(textureCoords, "y"), NULL, 0),
            regionWidth = strtol(readAttribute(textureCoords, "width"), NULL, 0),
            regionHeight = strtol(readAttribute(textureCoords, "height"), NULL, 0);

        // Create the TextureRegion. Since this is related to textures, we must use the map's
        // texture size, NOT the map's real size.
        textureRegion = wool::TextureRegion(
            regionX, regionY,
            regionWidth, regionHeight,
            gameMap.getMapTextureSize()
        );

        // Set our width and height
        width = (GLfloat)regionWidth;
        height = (GLfloat)regionHeight;

        // Read the position element
        tinyxml2::XMLElement * position = readElement(worldElement, "position");

        // Set our x and y coordinates.
        x = (GLfloat)strtol(readAttribute(position, "x"), NULL, 0);
        y = (GLfloat)strtol(readAttribute(position, "y"), NULL, 0);
    }
    
    MapElement::~MapElement(void) {

    }

    bool MapElement::isAdjacent(uint32_t mapElementId) const {
        for (uint32_t id : adjacent) {
            if (id == mapElementId) {
                return true;
            }
        }
        return false;
    }



    #define FONT_CHAR_WIDTH 8
    #define FONT_CHAR_HEIGHT 12

    #define USE_FANCY_TEXT_PLACEMENT

    #ifdef USE_FANCY_TEXT_PLACEMENT
        #define TEXT_CENTREPOINT_X textRenderingX
        #define TEXT_CENTREPOINT_Y textRenderingY
        #define TEXT_WIDTH textRenderingWidth
    #else
        #define TEXT_CENTREPOINT_X (width * 0.5f)
        #define TEXT_CENTREPOINT_Y (height * 0.5f)
        #define TEXT_WIDTH (width * 0.66667f)
    #endif


    void MapElement::drawName(const CameraProperties & cameraProperties, bool drawForeground) const {
        GLfloat scale = (TEXT_WIDTH / (name.size() * FONT_CHAR_WIDTH)) * cameraProperties.xScale;

        // Only draw the text if our scale is greater than or equal to the minimum text scale.
        if (scale >= cameraProperties.minimumTextScale) {
            GLfloat drawX = getCentreViewX(cameraProperties) - (name.length() * FONT_CHAR_WIDTH * scale) * 0.5f,
                    drawY = getCentreViewY(cameraProperties) - (FONT_CHAR_HEIGHT * scale) * 0.5f;

            wool_setColourRGBA(wool::RGBA::GREY);
            wool::Font::font.drawStringScale(name, drawX + scale * 0.5f, drawY + scale * 0.5f, scale, scale);

            if (drawForeground) {
                wool_setColourRGBA(wool::RGBA::BLACK);
                wool::Font::font.drawStringScale(name, drawX, drawY, scale, scale);
            }
        }
    }


    void MapElement::checkMapAdjacencySymmetry(const std::vector<MapElement *> & elements, const char * sourceClassName, const char * mapElementType) {

        // First build an unordered set of adjacent map elements, for each map element.
        // Print a verbose log and exit if any are invalid or repeated.
        std::vector<std::unordered_set<uint32_t>> adjPerElem;
        for (MapElement * element : elements) {
            std::unordered_set<uint32_t> adjacent;
            for (uint32_t adjElem : element->getAdjacent()) {
                if (adjElem >= elements.size()) {
                    std::cerr << "CRITICAL ERROR: " << sourceClassName << ": Unknown adjacent " <<
                                 mapElementType << " ID (" << adjElem << ") listed in " <<
                                 mapElementType << ": " << element->getName() << " (" <<
                                 element->getId() << ").\n";
                    exit(EXIT_FAILURE);

                } else if (adjElem == element->getId()) {
                    std::cerr << "CRITICAL ERROR: " << sourceClassName << ": " << mapElementType <<
                                 ' ' << element->getName() << " (" << element->getId() <<
                                 ") lists itself as being adjacent.\n";
                    exit(EXIT_FAILURE);

                } else if (!adjacent.insert(adjElem).second) {
                    std::cerr << "CRITICAL ERROR: " << sourceClassName << ": Adjacent " <<
                                 mapElementType << " ID (" << adjElem << ") repeated in " <<
                                 mapElementType << ": " << element->getName() << " (" <<
                                 element->getId() << ").\n";
                    exit(EXIT_FAILURE);
                }
            }
            adjPerElem.push_back(std::move(adjacent));
        }

        // Iterate through each map element, and check that the adjacency is symmetric.
        // Note that each time we erase the symmetry pointing the other way, as to avoid
        // checking it twice. Print a verbose log message and exit if there is a problem,
        // so the user can diagnose the problem with their map.
        for (uint32_t elemId = 0; elemId < elements.size(); elemId++) {
            for (uint32_t adjElemId : adjPerElem.at(elemId)) {
                std::unordered_set<uint32_t> & otherSet = adjPerElem.at(adjElemId);
                auto iter = otherSet.find(elemId);
                if (iter == otherSet.end()) {
                    std::cerr << "CRITICAL ERROR: " << sourceClassName << ": " << mapElementType << ' ' <<
                                 elements[elemId]->getName() << " (" << elemId << ") lists " << mapElementType <<
                                 ' ' << elements[adjElemId]->getName() << " (" << adjElemId <<
                                 ") as being adjacent, but " << mapElementType << ' ' <<
                                 elements[adjElemId]->getName() << " (" << adjElemId << ") does not list " <<
                                 mapElementType << ' ' << elements[elemId]->getName() << " (" << elemId <<
                                 ") as being adjacent.\n";
                    exit(EXIT_FAILURE);
                } else {
                    otherSet.erase(iter);
                }
            }
        }
    }
}

/*
 * Area.h
 *
 *  Created on: 14 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_AREA_H_
#define BASEGAME_COMMON_AREA_H_

#include <cstdint>
#include <string>
#include <vector>

#include "../MapElement.h"

namespace rasc {

    class Area : public MapElement {

    protected:

        // ----------------------- GENERAL PROPERTIES, FROM XML FILE -----------------------------

        /**
         * The area we are contained within.
         */
        uint32_t continent;


        /**
         * Stores which provinces are contained within our area.
         */
        std::vector<uint32_t> provinces;

    public:

        Area(GameMap & gameMap);
        virtual ~Area(void);
        
        /**
         * Override this so we can initialise in correct order and load area specific data from XML.
         */
        virtual void loadFromXML(tinyxml2::XMLElement * element) override;

        inline std::vector<uint32_t> & getProvinces(void) {
            return provinces;
        }

        inline uint32_t getContinentId(void) const {
            return continent;
        }

    };

}

#endif

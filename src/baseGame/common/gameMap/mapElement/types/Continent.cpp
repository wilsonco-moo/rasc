/*
 * Continent.cpp
 *
 *  Created on: 16 Aug 2018
 *      Author: wilson
 */

#include "Continent.h"

#include <tinyxml2/tinyxml2.h>
#include <stddef.h>

#include "../../../util/XMLUtil.h"

namespace rasc {

    Continent::Continent(GameMap & gameMap) :
        MapElement(gameMap) {
    }

    Continent::~Continent(void) {
    }

    void Continent::loadFromXML(tinyxml2::XMLElement * element) {
        // Load parent class (MapElement) from XML FIRST.
        MapElement::loadFromXML(element);
        
        // Get the adjacency tag
        tinyxml2::XMLElement * adjacentElement = readElement(element, "adjacent");

        // Loop through adjacent continents.
        loopElements(adjacentContinent, adjacentElement, "continent") {
            // Add all the ids to the list of adjacent continents.
            adjacent.push_back(strtol(readAttribute(adjacentContinent, "id"), NULL, 0));
        }

        // Get our areas tag
        tinyxml2::XMLElement * areasElement = readElement(element, "areas");
        loopElements(containedArea, areasElement, "province") {
            // Add all the ids to the list of contained provinces.
            areas.push_back(strtol(readAttribute(containedArea, "id"), NULL, 0));
        }
    }
}

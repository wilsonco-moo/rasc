/*
 * Continent.h
 *
 *  Created on: 16 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_MAPELEMENT_TYPES_CONTINENT_H_
#define BASEGAME_COMMON_GAMEMAP_MAPELEMENT_TYPES_CONTINENT_H_

#include <cstdint>
#include <vector>

#include "../MapElement.h"

namespace rasc {

    class Continent : public MapElement {
    protected:

        // ----------------------- GENERAL PROPERTIES, FROM XML FILE -----------------------------

        /**
         * Stores which provinces are contained within our area.
         */
        std::vector<uint32_t> areas;

    public:
        Continent(GameMap & gameMap);
        virtual ~Continent(void);

        /**
         * Override this so we can initialise in correct order and load continent specific data from XML.
         */
        virtual void loadFromXML(tinyxml2::XMLElement * element) override;
    };
}

#endif

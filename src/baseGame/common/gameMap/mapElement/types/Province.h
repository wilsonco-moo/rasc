/*
 * Province.h
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_GAMEMAP_MAPELEMENT_TYPES_PROVINCE_H_
#define BASEGAME_COMMON_GAMEMAP_MAPELEMENT_TYPES_PROVINCE_H_

#include <wool/misc/RGBA.h>
#include <cstdint>
#include <string>

#include "../../../update/updateTypes/CheckedUpdate.h"
#include "../../provinceUtil/ProvinceArmyController.h"
#include "../../../update/updateTypes/MiscUpdate.h"
#include "../../properties/ProvinceProperties.h"
#include "../../../update/RascUpdatable.h"
#include "../MapElement.h"

namespace simplenetwork {
    class NetworkInterface;
    class SimpleNetworkServer;
}

namespace rasc {
    class ProvinceBattleController;
    class ArmySelectController;
    class RascClientNetIntf;
    class PrioritisedAI;
    class MiscUpdate;
    class PlayerData;
    class AIPlayer;
    class Province;
    class GameMap;
    class BasicAI;
    class Player;

    class Province : public MapElement, public RascUpdatable {
    private:
        friend class ProvinceProperties;
    public:

        // ----------------------- GENERAL PROPERTIES, FROM XML FILE -----------------------------

        /**
         * Our ProvinceProperties instance. This is public so all outside classes
         * can access our stats.
         */
        ProvinceProperties properties;

        /**
         * Our ProvinceArmyController instance.
         * This stores all army information, and provides all access to army stuff.
         */
        ProvinceArmyController armyControl;

        /**
         * This controls multi-turn battles.
         * On the client side this will be a plain ProvinceBattleController.
         * On the server side, this will be a ServerProvinceBattleController.
         */
        ProvinceBattleController * battleControl;

    protected:

        /**
         * The area we are contained within.
         */
        uint32_t area;

        // ---------------------- IN-GAME PROPERTIES --------------------------------------------

        /**
         * Stores the player id, by default this is PlayerData::NO_PLAYER.
         */
        uint64_t provinceOwner;

        /**
         * The colour to draw this province, as an RGBA int.
         */
        uint32_t ownerColourInt;

        /**
         * The colour to draw this province, as a wool::RGBA.
         * This is generated from ownerColourInt when it is set, for convenience.
         */
        wool::RGBA ownerColour;

        // ------------------------------ OTHER MISC STUFF -------------------------------------

        /**
         * Our army select controller. We must have a pointer to this, so that we can access this during province
         * updates.
         */
        ArmySelectController * armySelectController;

    public:

        Province(GameMap & gameMap, ArmySelectController * armySelectController);
        virtual ~Province(void);

    // --------------------------- ACCESSOR METHODS ---------------------------------------------

        inline uint64_t getProvinceOwner(void) const {
            return provinceOwner;
        }
        inline uint32_t getProvinceOwnerColour(void) const {
            return ownerColourInt;
        }
        inline uint32_t getAreaId(void) const {
            return area;
        }

        /**
         * Override this so we can initialise in correct order and load province specific data from XML.
         * Province subclasses (server/client) further override this to load
         * server/client specific data from XML.
         */
        virtual void loadFromXML(tinyxml2::XMLElement * element) override;

    protected:
        /**
         * This must create a new battle controller, appropriate to whether this is the
         * server side or client side. This is used during loadFromXML to create the appropriate
         * battle controller for this province.
         * On the server this should return a ServerProvinceBattleController.
         * On the client this should return a plain ProvinceBattleController.
         */
        virtual ProvinceBattleController * createNewBattleController(void) = 0;
        
        /**
         * This runs our internal onProvinceChange mechanism, used by Province subclasses
         * to update various data, (like garrison display). See onProvinceChange in ClientProvince.
         * 
         * Must be called internally by Province or any subclasses, (or friend subcomponents
         * like ProvinceProperties), each time something potentially requiring an update to
         * the province menu changes.
         */
        virtual void runOnProvinceChange(void);

    public:

    // =================================== Utility methods for province type ==================================

        /**
         * This returns TRUE if users should be able to place units on this province and claim it,
         * and return FALSE otherwise. Note that this will only return a correct value after loading XML
         * data on the server side, or after loading initial data on the client side.
         */
        inline bool isProvinceAccessible(void) const {
            return properties.isProvinceAccessible();
        }

        /**
         * A convenience method to access the name of our province type,
         * such as plains, tundra etc. This is done by getting the friendly
         * name of our terrain type property.
         * An empty string is returned in the case that the terrain type is
         * unknown, (i.e: not loaded from XML on the server side, or not
         * loaded initial data yet on the client side).
         */
        inline std::string getProvinceTypeName(void) const {
            return properties.getProvinceTypeName();
        }

    // ------------------------- PROVINCE UPDATING ----------------------------------------------

    protected:

        // These are the standard methods by which updating is carried out, from classes outside Province.

        virtual void onGenerateServerUpdate(simplenetwork::Message & msg) override;
        virtual bool onReceiveServerUpdate(simplenetwork::Message & msg) override;
        virtual void onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) override;
        virtual bool onReceiveInitialData(simplenetwork::Message & msg) override;

        // ---------------------------------- MISC UPDATE SENDING ----------------------------

        /**
         * This specifies all the different types of misc updates available.
         */
        class ProvinceMiscUpdateTypes {
        public:
            enum {
                newProvinceClaim,
                changeProvinceColour,

                COUNT_OF_MISC_UPDATE_TYPES
            };
        };
        using ProvinceMiscUpdateType = uint8_t;

        inline void writeMiscUpdateType(simplenetwork::Message & msg, ProvinceMiscUpdateType update) const { msg.write8(update); }
        inline ProvinceMiscUpdateType readMiscUpdateType(simplenetwork::Message & msg) const { return msg.read8(); }

        virtual bool onReceiveMiscUpdate(simplenetwork::Message & msg) override;

    public:
        // -------------------------------- MISC UPDATE METHODS ----------------------------------

        /**
         * Claims ownership of this province, in the name of the specified player.
         */
        void claimByPlayer(MiscUpdate update, const Player * player);

        /**
         * Claims ownership of this province, in the name of the specified player, by player data.
         */
        void claimByPlayerData(MiscUpdate update, const PlayerData * playerData);

        /**
         * Claims ownership of this province, in the name of NO_PLAYER - i.e: Makes the province unowned.
         */
        void claimUnowned(MiscUpdate update);

        /**
         * Changes the colour of the province, without changing the owner.
         * WARNING:
         *  This must only ever be used as part of changing a player colour,
         *  in RascBaseGameServer::changePlayerColour. Applying this misc update
         *  does not actually notify the stat system. That is done after all the
         *  player's provinces and armies have their colours changed, and the misc update
         *  to change the colour in their player data is applied.
         */
        void changeColour(MiscUpdate update, uint32_t newColour);


        // ============================ CHECKED UPDATE STUFF ============================================

    protected:
        /**
         * This specifies all the different types of checked updates available.
         */
        class ProvinceCheckedUpdateTypes {
        public:
            enum {
                testUpdate,

                COUNT_OF_MISC_UPDATE_TYPES
            };
        };
        using ProvinceCheckedUpdateType = uint8_t;
        inline void writeCheckedUpdateType(simplenetwork::Message & msg, ProvinceCheckedUpdateType update) const { msg.write8(update); }
        inline ProvinceCheckedUpdateType readCheckedUpdateType(simplenetwork::Message & msg) const { return msg.read8(); }
        virtual bool onReceiveCheckedUpdate(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) override;

    public:

        // -------------------------------- CHECKED UPDATE METHODS ----------------------------------

        void testUpdate(CheckedUpdate update);
    };
}

#endif

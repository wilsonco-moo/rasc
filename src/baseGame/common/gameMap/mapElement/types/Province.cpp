/*
 * Province.cpp
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#include "Province.h"

#include <tinyxml2/tinyxml2.h>
#include <iostream>
#include <GL/gl.h>
#include <cstdlib>
#include <cstddef>
#include <string>

#include "../../provinceUtil/battle/ProvinceBattleController.h"
#include "../../../../client/config/CameraProperties.h"
#include "../../../stats/UnifiedStatSystem.h"
#include "../../properties/trait/TraitDef.h"
#include "../../../playerData/PlayerData.h"
#include "../../properties/trait/Traits.h"
#include "../../properties/Properties.h"
#include "../../../../server/Player.h"
#include "../../../util/CommonBox.h"
#include "../../../util/XMLUtil.h"
#include "../../GameMap.h"

namespace rasc {

    Province::Province(GameMap & gameMap, ArmySelectController * armySelectController) :
        MapElement(gameMap),
        properties(*this, gameMap.getPropertiesUniqueIdAssigner()),
        armyControl(*this, armySelectController),
        battleControl(NULL),
        provinceOwner(PlayerData::NO_PLAYER),
        ownerColourInt(0xffffffff),
        ownerColour(wool::RGBA::WHITE),
        armySelectController(armySelectController) {
    }

    Province::~Province(void) {
        delete battleControl;
    }
    
    void Province::loadFromXML(tinyxml2::XMLElement * element) {
        // Load parent class (MapElement) from XML FIRST.
        MapElement::loadFromXML(element);
        
        // Now we're constructed properly, create battle controller.
        battleControl = createNewBattleController();
        
        // Initialise province properties from XML too (this loads traits and type).
        properties.loadFromXML(element);
        
        
        // Read the properties element from XML.
        tinyxml2::XMLElement * propertiesElement = readElement(element, "properties");

        // Read the id of the area we are contained within
        area = std::strtol(readAttribute(propertiesElement, "area"), NULL, 0);

        // Get the adjacency tag
        tinyxml2::XMLElement * adjacentElement = readElement(element, "adjacent");

        // Loop through adjacent provinces.
        loopElements(adjacentProvince, adjacentElement, "province") {
            // Add all the ids to the list of adjacent provinces.
            adjacent.push_back(std::strtol(readAttribute(adjacentProvince, "id"), NULL, 0));
        }

        // =========== RascUpdatable children ===========
        registerChild(&properties);
        registerChild(&armyControl);
        registerChild(battleControl);
    }

    void Province::runOnProvinceChange(void) {
    }
    
    // -------------- generic updating --------------------------

    void Province::onGenerateServerUpdate(simplenetwork::Message & msg) {
        writeUpdateHeader(msg);
        msg.write16(id);
    }

    bool Province::onReceiveServerUpdate(simplenetwork::Message & msg) {
        std::cout << "Update to province: " << id << " --> " << msg.read16() << "\n";
        runOnProvinceChange(); // Run this since we got an update.
        return true;
    }

    void Province::onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) {
        writeUpdateHeader(msg);
        // Here we must write all the stats not available in the XML file.
        // Write our owner
        msg.write64(provinceOwner);
        // Write our owner colour.
        msg.write32(ownerColourInt);
    }

    bool Province::onReceiveInitialData(simplenetwork::Message & msg) {
        // Here we must read all the stats not available in the XML file.
        // Read our owner
        provinceOwner = msg.read64();
        // Read our owner colour.
        ownerColourInt = msg.read32();
        ownerColour = wool::RGBA(ownerColourInt);

        runOnProvinceChange(); // Run this since we got an update.
        return true;
    }




    

    // ================================================ PROVINCE MISC UPDATING ===========================================================

    bool Province::onReceiveMiscUpdate(simplenetwork::Message & msg) {
        uint16_t miscUpdateType = readMiscUpdateType(msg);
        switch(miscUpdateType) {
        // ------------------------------------------------------------------------------------
        case ProvinceMiscUpdateTypes::newProvinceClaim:
            {
                uint64_t oldOwner = provinceOwner;
                provinceOwner = msg.read64();
                ownerColourInt = msg.read32();
                ownerColour = wool::RGBA(ownerColourInt);
                // Notify the stat system if ownership actually changed.
                if (oldOwner != provinceOwner) gameMap.getCommonBox().statSystem->onChangeProvinceOwnership(*this, oldOwner, provinceOwner);
                break;
            }
        // ------------------------------------------------------------------------------------
        case ProvinceMiscUpdateTypes::changeProvinceColour:
            {
                // Change only the colour, from the contents of the message.
                // Note that the stat system must not get updated, since a province colour
                // change happens as part of a player colour change. Here, the stat system
                // gets updated at the end, when the PlayerData gets a colour change.
                ownerColourInt = msg.read32();
                ownerColour = wool::RGBA(ownerColourInt);
                break;
            }
        // ------------------------------------------------------------------------------------
        default:
            std::cout << "WARNING: Unknown misc update type for province: ID: " << miscUpdateType << ".\n";
            break;
        }

        runOnProvinceChange(); // Run this since we got an update.
        return true;
    }


    // ---------------------------------- MISC UPDATE SENDING ----------------------------

    void Province::claimByPlayer(MiscUpdate update, const Player * player) {
        writeUpdateHeader(update.msg());
        writeMiscUpdateType(update.msg(), ProvinceMiscUpdateTypes::newProvinceClaim);
        update.msg().write64(player->getData()->getId());
        update.msg().write32(player->getData()->getColourInt());
    }

    void Province::claimByPlayerData(MiscUpdate update, const PlayerData * playerData) {
        writeUpdateHeader(update.msg());
        writeMiscUpdateType(update.msg(), ProvinceMiscUpdateTypes::newProvinceClaim);
        update.msg().write64(playerData->getId());
        update.msg().write32(playerData->getColourInt());
    }

    void Province::claimUnowned(MiscUpdate update) {
        writeUpdateHeader(update.msg());
        writeMiscUpdateType(update.msg(), ProvinceMiscUpdateTypes::newProvinceClaim);
        update.msg().write64(PlayerData::NO_PLAYER);
        update.msg().write32(0xffffffff);
    }

    void Province::changeColour(MiscUpdate update, uint32_t newColour) {
        writeUpdateHeader(update.msg());
        writeMiscUpdateType(update.msg(), ProvinceMiscUpdateTypes::changeProvinceColour);
        update.msg().write32(newColour);
    }

    // ==================================== PROVINCE CHECKED UPDATING =============================================

    bool Province::onReceiveCheckedUpdate(simplenetwork::Message & checkedUpdate, simplenetwork::Message & miscUpdate, PlayerData * contextPlayer) {
        uint16_t checkedUpdateType = readCheckedUpdateType(checkedUpdate);
        switch(checkedUpdateType) {
        // ------------------------------------------------------------------------------------
        case ProvinceCheckedUpdateTypes::testUpdate:
            std::cout << "Received test checked update.\n";
            break;
        // ------------------------------------------------------------------------------------
        default:
            std::cout << "WARNING: Unknown checked update type for province: ID: " << checkedUpdateType << ".\n";
            break;
        }

        // NOTE: Don't run if onProvinceChange, since checked updates are never received on the client-side.

        return true;
    }

    void Province::testUpdate(CheckedUpdate update) {
        writeUpdateHeader(update.msg());
        writeCheckedUpdateType(update.msg(), ProvinceCheckedUpdateTypes::testUpdate);
    }
}









// ======================= OLD PROVINCE ZERO BUG SUCEPTIBLE CODE =============================

/*
// ------------------------------------------------------------------------------------
case ProvinceMiscUpdateTypes::addUnits:
    {
        //
        // This method could also cause the province zero bug: Data is read from the message AFTER checking
        // for the armies existence.
        //
        std::list<Army>::iterator army = findArmy(msg.read64());
        if (army != armies.end()) {
            uint32_t unitsToAdd = msg.read32();
            if (msg.read8()) {
                // If we are supposed to update the turn count
                army->addUnits(unitsToAdd, msg.read64());
            } else {
                // If we are NOT supposed to update the turn count
                army->addUnitsWithoutTurn(unitsToAdd);
            }
            // Update the status of the army select controller, as the number of units in an army has changed.
            armySelectController->updateStatus();
        }

        // Need to remove this, it is only here for debugging the province zero bug.
        else {
            std::cout << "WARNING: We have tried to add units to an army that does not exist, a province zero bug could follow.\n";
        }
    }
    break;
// ------------------------------------------------------------------------------------
case ProvinceMiscUpdateTypes::moveArmy:
    {
        //
        // This army move system may be causing the province colouring issue, as if the army does not exist, not all the
        // message will be read. This would cause the next 8 bytes of the message (the turn count), to be interpreted as
        // a province claim. The province claim would then read the turn count as the most significant part of the player
        // id, causing a very large random (ish) player id (as is seen). It would then read undefined memory, (usually zeroes),
        // giving the zero (transparent black) province colour.
        // Need more testing to verify this.
        // -- Update: It has been proven that this CAN cause a province 0 disappearance bug, but it is still unknown
        //            whether this is the only cause. More testing is still needed.
        // -- Update: It has been demonstrated in game, that the province zero bug is reproducible under specific circumstances,
        //            when the server connection has a high ping.
        //
        Province * otherProvince = gameMap->getProvinceRaw(msg.read32());
        uint64_t armyId = msg.read64();
        std::list<Army>::iterator armyIter = otherProvince->findArmy(armyId);
        // If the army exists,
        if (armyIter != otherProvince->armies.end()) {
            // Run it's onMove method, reading the turn count.
            armyIter->onMove(msg.read64(), MOVES_TO_MOVE);
            // Add it to our province
            armies.push_back(*armyIter);
            // Erase it from it's old province
            otherProvince->armies.erase(armyIter);
            // Notify the army select controller that the army has moved, from the other province to this province.
            armySelectController->onArmyMove(otherProvince->getId(), getId(), armyId);
        }

        // Need to remove this, it is only here for debugging the province zero bug.
        else {
            std::cout << "WARNING: We have tried to move an army that does not exist, a province zero bug could follow.\n";
        }

        // Finally, update the province ownership and stuff.
        updateOwnershipFromBattles();
    }
    break;
*/




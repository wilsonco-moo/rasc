/*
 * Area.cpp
 *
 *  Created on: 14 Aug 2018
 *      Author: wilson
 */

#include "Area.h"

#include <tinyxml2/tinyxml2.h>
#include <cstdlib>

#include "../../../util/XMLUtil.h"

namespace rasc {

    Area::Area(GameMap & gameMap) :
        MapElement(gameMap) {
    }

    Area::~Area(void) {
    }
    
    void Area::loadFromXML(tinyxml2::XMLElement * element) {
        // Load parent class (MapElement) from XML FIRST.
        MapElement::loadFromXML(element);
        
        // Read the properties
        tinyxml2::XMLElement * properties = readElement(element, "properties");

        // Read the id of the continent we are contained within.
        continent = strtol(readAttribute(properties, "continent"), NULL, 0);

        // Get the adjacency tag
        tinyxml2::XMLElement * adjacentElement = readElement(element, "adjacent");

        // Loop through adjacent provinces.
        loopElements(adjacentArea, adjacentElement, "area") {
            // Add all the ids to the list of adjacent areas.
            adjacent.push_back(strtol(readAttribute(adjacentArea, "id"), NULL, 0));
        }

        // Get our provinces tag
        tinyxml2::XMLElement * provincesElement = readElement(element, "provinces");
        loopElements(containedProvince, provincesElement, "province") {
            // Add all the ids to the list of contained provinces.
            provinces.push_back(strtol(readAttribute(containedProvince, "id"), NULL, 0));
        }
    }
}

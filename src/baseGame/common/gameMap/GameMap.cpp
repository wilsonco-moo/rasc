/*
 * GameMap.cpp
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#include "GameMap.h"

#include <sockets/plus/message/Message.h>
#include <tinyxml2/tinyxml2.h>
#include <wool/misc/RGBA.h>
#include <unordered_set>
#include <iostream>
#include <cstdlib>
#include <utility>
#include <vector>
#include <cmath>

#include "mapElement/types/Continent.h"
#include "mapElement/types/Province.h"
#include "../config/StaticConfig.h"
#include "mapElement/MapElement.h"
#include "mapElement/types/Area.h"
#include "../util/XMLUtil.h"
#include "../util/Misc.h"

namespace rasc {

    GameMap::GameMap(std::pair<unsigned int, unsigned int> mapRealSize, std::pair<unsigned int, unsigned int> mapTextureSize, CommonBox & box, UniqueIdAssigner * propertiesUniqueIdAssigner) :
        RascUpdatable(RascUpdatable::Mode::array, 4),

        accessibleProvinceCount(0),
        accessibleAreaCount(0),
        accessibleContinentCount(0),
        mapName(""),
        mapRealSize(mapRealSize),
        mapTextureSize(mapTextureSize),
        propertiesUniqueIdAssigner(propertiesUniqueIdAssigner),
        provinces(),
        areas(),
        continents(),
        coloursToProvinces(),
        armySelectController(),
        box(box) {
    }

    GameMap::GameMap(CommonBox & box, UniqueIdAssigner * propertiesUniqueIdAssigner) :
        GameMap(std::pair<unsigned int, unsigned int>(32, 32),
                std::pair<unsigned int, unsigned int>(32, 32),
                box, propertiesUniqueIdAssigner) {
    }

    GameMap::~GameMap(void) {
        std::cout << "Deleting game map...\n";
        for (Province * prov : provinces) {
            delete prov;
        }
        for (Area * area : areas) {
            delete area;
        }
        for (Continent * continent : continents) {
            delete continent;
        }
    }

    void GameMap::loadFromXML(tinyxml2::XMLDocument * xml) {

        // Read the root element.
        tinyxml2::XMLElement * root = readElement(*xml, "gameMap");

        // First we read the map export version. If it is wrong, exit with a verbose description of the problem.
        tinyxml2::XMLElement * exportElement = readElement(root, "export");
        std::string exportVersion = std::string(readAttribute(exportElement, "mapExportVersion"));
        if (exportVersion != StaticConfig::MAP_VERSION) {
            std::cout << "\nFailed to load map, version mismatch.\n    Map version found: " << exportVersion << "\n    Map version required: " << StaticConfig::MAP_VERSION << "\nPlease re-assemble this map using the latest version of the map converter.\n";
            exit(EXIT_FAILURE);
        }

        // Read the properties element, get the map name.
        tinyxml2::XMLElement * globalProperties = readElement(root, "properties");
            // Read the map element, for properties of the map (rather than the minimap)
            tinyxml2::XMLElement * globalMapProperties = readElement(globalProperties, "map");
            // Read the map name
            mapName = std::string(readAttribute(globalMapProperties, "name"));

        // Store the ids of the accessible areas and continents temporarily in sets, so we can count them.
        std::unordered_set<uint32_t> accessibleAreas, accessibleContinents;

        // First we load our province instances.
        tinyxml2::XMLElement * provincesElement = readElement(root, "provinces");// Loop through the provinces.
        loopElements(province, provincesElement, "province") {
            // Create a new province with our factory method, which returns an appropriate province
            // type on client/server. Immediately afterwards initialise it from XML.
            // Note: this internally passes a pointer to our army select controller, even though it is not initialised yet.
            Province * newProvince = createNewProvince();
            provinces.push_back(newProvince);
            newProvince->loadFromXML(province);
            
            // Provinces are rasc updatables, so register as a child.
            registerChild(newProvince);

            // Count each province and area if it is accessible. Note that this only
            // produces valid results on the server side.
            if (newProvince->isProvinceAccessible()) {
                accessibleProvinceCount++;
                accessibleAreas.insert(newProvince->getAreaId());
            }
            // Add each province to the colours hashmap.
            coloursToProvinces[newProvince->getColour()] = newProvince->getId();
        }

        // Now we know how many provinces we have, we can initialise the army selection controller.
        armySelectController = ArmySelectController(provinces.size(), this);

        // Read the areas element.
        tinyxml2::XMLElement * areasElement = readElement(root, "areas");
        // Loop through the areas.
        loopElements(area, areasElement, "area") {
            // Add each area, then immediately initialise it from XML using the area XML element.
            Area * newArea = new Area(*this);
            areas.push_back(newArea);
            newArea->loadFromXML(area);

            // If each area is accessible, add it's parent continent to the accessible continents set.
            if (accessibleAreas.find(newArea->getId()) != accessibleAreas.end()) {
                accessibleContinents.insert(newArea->getContinentId());
            }
        }

        // Now we know how many accessible areas and continents there are, save these values.
        // This only produces valid results on the server side. These are sent as initial data
        // to the client.
        accessibleAreaCount = accessibleAreas.size();
        accessibleContinentCount = accessibleContinents.size();

        // Read the continents element.
        tinyxml2::XMLElement * continentsElement = readElement(root, "continents");
        // Loop through the continents.
        loopElements(continent, continentsElement, "continent") {
            // Add each continent, then immediately initialise it from XML using the continent XML element.
            Continent * newContinent = new Continent(*this);
            continents.push_back(newContinent);
            newContinent->loadFromXML(continent);            
        }

        // Now we have loaded provinces, areas and continents, check their adjacency.
        MapElement::checkMapAdjacencySymmetry((const std::vector<MapElement *> &)getProvinces(),  "GameMap", "province");
        MapElement::checkMapAdjacencySymmetry((const std::vector<MapElement *> &)getAreas(),      "GameMap", "area");
        MapElement::checkMapAdjacencySymmetry((const std::vector<MapElement *> &)getContinents(), "GameMap", "continent");
    }

    void GameMap::onGenerateInitialData(simplenetwork::Message & msg, bool isSaving) {
        // There is no point saving counts of accessible provinces to the
        // savefile, since we re-calculate these when loading the XML file
        // (before loading savefiles) anyways.
        if (!isSaving) {
            writeUpdateHeader(msg);
            msg.write32(accessibleContinentCount);
            msg.write32(accessibleAreaCount);
            msg.write32(accessibleProvinceCount);
        }
    }

    bool GameMap::onReceiveInitialData(simplenetwork::Message & msg) {
        // Since these values are not written when saving, this will only be called
        // on the client side when a client joins. Simply read the accessible counts.
        accessibleContinentCount = msg.read32();
        accessibleAreaCount = msg.read32();
        accessibleProvinceCount = msg.read32();
        return true;
    }
}

/*
 * RascUpdatableIds.h
 *
 *  Created on: 5 Mar 2019
 *      Author: wilson
 */

#ifndef BASEGAME_COMMON_RASCUPDATABLEBOXIDS_H_
#define BASEGAME_COMMON_RASCUPDATABLEBOXIDS_H_

#include <cstdint>

namespace rasc {

    /**
     * This enum defined the ids that rasc updatable instances, within RascBox and ServerBox are registered to.
     * These ids are not actually used by RascBox and ServerBox, rather they both register the instances in the
     * same order as is described here.
     * The order MUST BE KEPT CONSISTENT, between ServerBox, RascBox and this class, to ensure correct operation
     * of the rasc updatable system.
     */
    class RascUpdatableBoxIds {
        enum {
            gameMap,
            playerDataSystem,

            // Classes which only exist on the server side: (These MUST be defined at the end):
            playerIdAssigner,
            armyIdAssigner,
            propertiesIdAssigner,
        };
    };
    typedef uint8_t RascUpdatableBoxId;

    #define BOX_IDENTIFIER_BYTES 1
}


#endif

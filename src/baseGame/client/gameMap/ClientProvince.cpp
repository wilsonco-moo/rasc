/*
 * ClientProvince.cpp
 *
 *  Created on: 2 May 2021
 *      Author: wilson
 */

#include "ClientProvince.h"

#include <tinyxml2/tinyxml2.h>
#include <wool/font/Font.h>
#include <wool/misc/RGBA.h>
#include <algorithm>
#include <iostream>
#include <cstddef>
#include <cstdlib>
#include <GL/gl.h>

#include "../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../common/gameMap/properties/trait/Traits.h"
#include "../../common/gameMap/properties/Properties.h"
#include "../ui/customComponents/icon/IconDrawer.h"
#include "../../common/util/numeric/NumberFormat.h"
#include "../../common/util/definitions/IconDef.h"
#include "../ui/containers/buildMenu/BuildMenu.h"
#include "../../common/playerData/PlayerData.h"
#include "../../common/gameMap/GameMap.h"
#include "../../common/GameMessages.h"
#include "../../common/util/XMLUtil.h"
#include "../../common/util/Misc.h"
#include "../RascClientNetIntf.h"

namespace rasc {

    ClientProvince::ClientProvince(GameMap & gameMap, ArmySelectController * armySelectController) :
        Province(gameMap, armySelectController),
        minimapTextureRegion(),
        garrisonText{0},
        hasFort(false),
        buildModeColour(),
        buildingDisplayRowCount(0),
        buildingDisplayBoxWidth(0.0f),
        buildingDisplayCanBeBuilt(false),
        isCurrentlyFiltered(false),
        onProvinceChange() {
    }
    
    ClientProvince::~ClientProvince(void) {
    }
    
    ProvinceBattleController * ClientProvince::createNewBattleController(void) {
        return new ProvinceBattleController(*this);
    }
    
    void ClientProvince::runOnProvinceChange(void) {
        onProvinceChange.updateFunctions();
        KShortenedNumber::format(garrisonText, GET_GARRISON_SIZE(properties));
        hasFort = properties.getPropertySet().hasPropertiesOfType(Properties::Types::fort);
    }
    
    void ClientProvince::loadFromXML(tinyxml2::XMLElement * element) {
        // Load parent class (Province) from XML FIRST.
        Province::loadFromXML(element);
        
        // Read the minimap element.
        tinyxml2::XMLElement * minimapElement = readElement(element, "minimap");

        // Read the texture coordinates element for the minimap. Don't use the readElement function for this,
        // as not having minimap coordinates is legal.
        tinyxml2::XMLElement * minimapTextureCoords = minimapElement->FirstChildElement("texture");

        if (minimapTextureCoords == NULL) {
            // If we have no minimap texture coordinates, then initialise all coordinates for the minimap to zero.
            // Don't worry about minimapTextureRegion, as it has already been initialised to zero by default.
            minimapWidth = 0.0f; minimapHeight = 0.0f; minimapX = 0.0f; minimapY = 0.0f;
            // Set this to false, as we should not draw our minimap section, since we do not have coordinates for it.
            drawMinimap = false;
        } else {

            // Read the texture region's dimensions.
            int minimapRegionX = std::strtol(readAttribute(minimapTextureCoords, "x"), NULL, 0),
                minimapRegionY = std::strtol(readAttribute(minimapTextureCoords, "y"), NULL, 0),
                minimapRegionWidth = std::strtol(readAttribute(minimapTextureCoords, "width"), NULL, 0),
                minimapRegionHeight = std::strtol(readAttribute(minimapTextureCoords, "height"), NULL, 0);

            // Create the TextureRegion for the minimap.
            // Since this is related to textures, here we must use the map's texture size NOT the map's real size.
            minimapTextureRegion = wool::TextureRegion(
                minimapRegionX, minimapRegionY,
                minimapRegionWidth, minimapRegionHeight,
                gameMap.getMapTextureSize()
            );

            // Set the width and height of our minimap section.
            minimapWidth = (GLfloat)minimapRegionWidth;
            minimapHeight = (GLfloat)minimapRegionHeight;

            // Read the position element for our minimap.
            tinyxml2::XMLElement * minimapPosition = readElement(minimapElement, "position");

            // Set the coordinates for drawing our minimap section.
            minimapX = (GLfloat)std::strtol(readAttribute(minimapPosition, "x"), NULL, 0);
            minimapY = (GLfloat)std::strtol(readAttribute(minimapPosition, "y"), NULL, 0);

            // Since here we should draw our minimap, as we have coordinates for it, set this to true.
            drawMinimap = true;
        }
    }
    
    
    // ============================================ Province drawing ======================================================
    
    #define SET_PROVINCE_DRAW_COLOUR(r, g, b) \
        glColor4f((r), (g), (b), ((id == cameraProperties.provId && isProvinceAccessible()) ? 0.5f : 1.0f));

    #define GET_PROVINCE_DRAW_BOUNDS                           \
        GLfloat drawWidth = width * cameraProperties.xScale,   \
                drawHeight = height * cameraProperties.yScale, \
                drawX1 = getViewX(cameraProperties),           \
                drawY1 = getViewY(cameraProperties),           \
                drawX2 = drawX1 + drawWidth,                   \
                drawY2 = drawY1 + drawHeight;

    #define PROVINCE_DRAW \
        wool_textureRegionVertex(textureRegion, drawX1, drawY1, drawX2, drawY1, drawX2, drawY2, drawX1, drawY2)

    void ClientProvince::draw(const CameraProperties & cameraProperties) const {
        SET_PROVINCE_DRAW_COLOUR(1.0f, 1.0f, 1.0f)
        GET_PROVINCE_DRAW_BOUNDS
        PROVINCE_DRAW
    }

    void ClientProvince::drawBuildMode(const CameraProperties & cameraProperties) const {
        SET_PROVINCE_DRAW_COLOUR(buildModeColour.red, buildModeColour.green, buildModeColour.blue)
        GET_PROVINCE_DRAW_BOUNDS
        PROVINCE_DRAW
    }

    void ClientProvince::drawWithOwnershipColour(const CameraProperties & cameraProperties) const {
        SET_PROVINCE_DRAW_COLOUR(ownerColour.red, ownerColour.green, ownerColour.blue)
        GET_PROVINCE_DRAW_BOUNDS
        if (battleControl->isBattleOngoing()) {

            GLfloat progressBoundary = 1.0f - battleControl->getSiegeProgress(),
                    uvMid = textureRegion.v1 + (textureRegion.v3 - textureRegion.v1) * progressBoundary,
                    drawYMid = drawY1 + drawHeight * progressBoundary;

            glTexCoord2f(textureRegion.u1, textureRegion.v1);
            glVertex2f(drawX1, drawY1);
            glTexCoord2f(textureRegion.u2, textureRegion.v2);
            glVertex2f(drawX2, drawY1);
            glTexCoord2f(textureRegion.u3, uvMid);
            glVertex2f(drawX2, drawYMid);
            glTexCoord2f(textureRegion.u4, uvMid);
            glVertex2f(drawX1, drawYMid);

            const wool::RGBA & col = battleControl->getMainAttackerColourRGBA();
            SET_PROVINCE_DRAW_COLOUR(col.red, col.green, col.blue)

            glTexCoord2f(textureRegion.u1, uvMid);
            glVertex2f(drawX1, drawYMid);
            glTexCoord2f(textureRegion.u2, uvMid);
            glVertex2f(drawX2, drawYMid);
            glTexCoord2f(textureRegion.u3, textureRegion.v3);
            glVertex2f(drawX2, drawY2);
            glTexCoord2f(textureRegion.u4, textureRegion.v4);
            glVertex2f(drawX1, drawY2);

        } else {
            PROVINCE_DRAW
        }
    }

    void ClientProvince::minimapDraw(const CameraProperties & cameraProperties) const {
        if (drawMinimap) {
            SET_PROVINCE_DRAW_COLOUR(ownerColour.red, ownerColour.green, ownerColour.blue)

            GLfloat drawX = cameraProperties.minimapDrawX + minimapX * cameraProperties.minimapDrawScale,
                    drawY = cameraProperties.minimapDrawY + minimapY * cameraProperties.minimapDrawScale,
                    drawWidth  = minimapWidth  * cameraProperties.minimapDrawScale,
                    drawHeight = minimapHeight * cameraProperties.minimapDrawScale;

            wool_textureRegionRectangle(minimapTextureRegion, drawX, drawY, drawWidth, drawHeight);
        }
    }
    
    
    // ============================================  Army drawing/utility =================================================
    
    /**
     * This internal macro, (used in DEFINE_ARMY_POS), defines all the positioning
     * variables required to work out if an army is visible.
     */
    #define DEFINE_ARMY_SIZE_VARS                                   \
        GLfloat armyIncrement = uiScale * Army::ARMY_DISPLAY_WIDTH, \
                armyDrawWidth = armyIncrement * armies.size();

    /**
     * This internal macro defines all the positioning variables to draw armies,
     * from the provided province view centre point.
     */
    #define DEFINE_ARMY_POS(centrePointX, centrePointY)                          \
        DEFINE_ARMY_SIZE_VARS                                                    \
        GLfloat armyX = (centrePointX) + (armyIncrement - armyDrawWidth) * 0.5f, \
                armyY = (centrePointY);

    /**
     * This internal macro increments the army position when iterating though armies.
     */
    #define INCREMENT_ARMY_POS \
        armyX += armyIncrement;
    
    #define MOVE_PREVIEW_LINE_RADIUS_OUTER 7.0f
    #define MOVE_PREVIEW_LINE_RADIUS_INNER 5.0f
    
    GLfloat ClientProvince::getArmyDrawWidth(const CameraProperties & cameraProperties) const {
        return armyControl.getArmies().size() * Army::ARMY_DISPLAY_WIDTH * (*cameraProperties.uiScale);
    }
    
    bool ClientProvince::armyVisible(const CameraProperties & cameraProperties, GLfloat drawWidth) const {
        return armyControl.getArmies().size() <= 1 || drawWidth <= textRenderingWidth * cameraProperties.xScale;
    }
    
    bool ClientProvince::armyVisible(const CameraProperties & cameraProperties) const {
        return armyControl.getArmies().size() <= 1 || getArmyDrawWidth(cameraProperties) <= textRenderingWidth * cameraProperties.xScale;
    }
    
    void ClientProvince::drawArmies(const CameraProperties & cameraProperties, uint64_t turnCount) const {

        // Do this for convenience.
        const std::list<Army> & armies = armyControl.getArmies();

        // Get the UI scale from the camera properties.
        GLfloat centreX = getCentreViewX(cameraProperties),
                centreY = getCentreViewY(cameraProperties),
                uiScale = *cameraProperties.uiScale;

        // First get selected armies, then draw arrows for selection if we actually *have* selected armies.
        const std::unordered_set<uint64_t> & selectedArmies = armySelectController->getProvinceSelectedArmies(getId());
        if (!selectedArmies.empty()) {

            GLfloat outerRad = MOVE_PREVIEW_LINE_RADIUS_OUTER * uiScale,
                    innerRad = MOVE_PREVIEW_LINE_RADIUS_INNER * uiScale;

            for (uint32_t provId : adjacent) {
                Province & adjProv = gameMap.getProvince(provId);

                // Only draw a preview line to accessible provinces.
                if (adjProv.isProvinceAccessible()) {
                    if (cameraProperties.provId == provId) {
                        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                    } else {
                        glColor4f(1.0f, 1.0f, 1.0f, 0.15f);
                    }
                    Misc::drawThickLine(centreX, centreY, adjProv.getCentreViewX(cameraProperties), adjProv.getCentreViewY(cameraProperties), outerRad);

                    if (cameraProperties.provId == provId) {
                        glColor4f(0.5f, 0.0f, 0.0f, 1.0f);
                    } else {
                        glColor4f(0.5f, 0.0f, 0.0f, 0.15f);
                    }
                    Misc::drawThickLine(centreX, centreY, adjProv.getCentreViewX(cameraProperties), adjProv.getCentreViewY(cameraProperties), innerRad);
                }
            }
        }

        // Draw our garrison/fort if applicable.
        if (hasFort) {
            drawFort(centreX, centreY, uiScale);
        }

        // If we have some armies in the province, draw them.
        if (armies.empty()) return;
        DEFINE_ARMY_POS(centreX, centreY)
        if (armyVisible(cameraProperties, armyDrawWidth)) {
            for (const Army & army : armies) {
                army.draw(cameraProperties, armyX, armyY, selectedArmies, turnCount);
                INCREMENT_ARMY_POS
            }
        } else {
            armies.front().drawPlaceholder(cameraProperties, armies.size(), getCentreViewX(cameraProperties), getCentreViewY(cameraProperties));
        }
    }
    
    
    // ============================================== Garrison drawing ====================================================
    
    #define FORT_OUTLINE_COL     glColor3f(0.0f, 0.0f, 0.0f);
    #define FORT_BOX_COL         glColor3f(0.5f, 0.5f, 0.5f);
    #define FORT_SIDES_COL       glColor3f(0.5f, 0.5f, 0.5f);
    #define FORT_TOP_COL         wool_setColourRGBA(ownerColour);
    #define FORT_BORDER          1.0f

    #define FORT_BOX_WIDTH       38.0f
    #define FORT_BOX_HEIGHT      20.0f
    #define FORT_BOX_Y1          (FORT_TOP_HEIGHT)
    #define FORT_BOX_Y2          (FORT_TOP_HEIGHT + FORT_BOX_HEIGHT)
    #define FORT_BOX_HW          (FORT_BOX_WIDTH * 0.5f)

    #define FORT_SIDES_WIDTH     10.0f
    #define FORT_SIDES_DIST      (FORT_BOX_HW + FORT_SIDES_WIDTH)

    #define FORT_TOP_HEIGHT      8.0f
    #define FORT_TOP_INNER_WIDTH (((FORT_TOP_HEIGHT - FORT_BORDER) / FORT_TOP_HEIGHT) * FORT_BOX_HW)

    #define FORT_TEXT_X          (3.0f - FORT_SIDES_DIST)
    #define FORT_TEXT_Y          (2.0f + FORT_BOX_Y1)
    #define FORT_TEXT_SCALE      (4.0f / 3.0f)
    #define FORT_TEXT_COL        glColor3f(0.8f, 1.0f, 1.0f);

    void ClientProvince::drawFort(GLfloat x, GLfloat y, GLfloat uiScale) const {
        GLfloat bord       = floor(uiScale * FORT_BORDER), // border (scaled by ui scale)
                boxHw      = FORT_BOX_HW * uiScale,        // Box half width (scaled by ui scale)
                boxY1      = y + FORT_BOX_Y1 * uiScale,    // Y1 position for box (scaled by ui scale)
                boxY2      = y + FORT_BOX_Y2 * uiScale,    // Y2 position for box (scaled by ui scale)
                sidesDist  = FORT_SIDES_DIST * uiScale,    // Distance from centre to far edge of sides (scaled by ui scale)
                boxHwL     = x - boxHw,                    // X1 position for box (scaled by ui scale)
                boxHwR     = x + boxHw,                    // X2 position for box (scaled by ui scale)
                sidesDistL = x - sidesDist,                // X1 position for far edge of sides (scaled by ui scale)
                sidesDistR = x + sidesDist;                // X2 position for far edge of sides (scaled by ui scale)

        // Draw outline for fort display.
        FORT_OUTLINE_COL
        glVertex2f(sidesDistL, y); glVertex2f(boxHwL, y);     glVertex2f(boxHwL, boxY2);     glVertex2f(sidesDistL, boxY2); // Left side part.
        glVertex2f(boxHwL, boxY1); glVertex2f(boxHwR, boxY1); glVertex2f(boxHwR, boxY2);     glVertex2f(boxHwL, boxY2);     // Centre (box) section.
        glVertex2f(boxHwR, y);     glVertex2f(sidesDistR, y); glVertex2f(sidesDistR, boxY2); glVertex2f(boxHwR, boxY2);     // Right side part.
        glVertex2f(x, y);          glVertex2f(boxHwR, boxY1); glVertex2f(boxHwL, boxY1);     glVertex2f(x, y);              // Top section.

        // Adjust dimensions for border.
        sidesDistL += bord;
        sidesDistR -= bord;
        boxHwL     -= bord;
        boxHwR     += bord;
        y += bord;
        boxY2 -= bord;

        // Draw fill of sides. Make sure we subtract borders.
        {
            FORT_SIDES_COL
            glVertex2f(sidesDistL, y); glVertex2f(boxHwL, y);     glVertex2f(boxHwL, boxY2);     glVertex2f(sidesDistL, boxY2); // Left side part.
            glVertex2f(boxHwR, y);     glVertex2f(sidesDistR, y); glVertex2f(sidesDistR, boxY2); glVertex2f(boxHwR, boxY2);     // Right side part.
        }

        // Draw the fill of the box. Again, subtract borders.
        {
            GLfloat boxInnerY1 = boxY1 + bord;
            FORT_BOX_COL
            glVertex2f(boxHwL, boxInnerY1); glVertex2f(boxHwR, boxInnerY1); glVertex2f(boxHwR, boxY2); glVertex2f(boxHwL, boxY2); // Centre (box) section.
        }

        // Draw the garrison number text.
        {
            GLfloat textX = x + FORT_TEXT_X * uiScale,
                    textY = y + FORT_TEXT_Y * uiScale,
                    textScale = FORT_TEXT_SCALE * uiScale;
            FORT_TEXT_COL
            wool::Font::font.drawFloorStringScale(garrisonText, textX, textY, textScale, textScale);
        }

        // Draw the fill of the top section. Do this from the inner coordinates to keep consistent bordering.
        {
            GLfloat topInnerW = FORT_TOP_INNER_WIDTH * uiScale,
                    topInnerL = x - topInnerW,
                    topInnerR = x + topInnerW;
            FORT_TOP_COL
            glVertex2f(x, y); glVertex2f(topInnerR, boxY1); glVertex2f(topInnerL, boxY1); glVertex2f(x, y); // Top section.

        }
    }
    
    
    // ========================================== Building display drawing ================================================
    
    #define BUILDING_DISPLAY_OUTLINE_COL       glColor3f(0.0f, 0.0f, 0.0f);
    #define BUILDING_DISPLAY_FILL_COL                      \
        if (buildingDisplayCanBeBuilt) {                   \
            if (isCurrentlyFiltered) { \
                glColor3f(1.0f, 1.0f, 0.0f);               \
            } else {                                       \
                glColor3f(1.0f, 1.0f, 1.0f);               \
            }                                              \
        } else {                                           \
            if (isCurrentlyFiltered) {                     \
                glColor3f(0.5f, 0.5f, 0.0f);               \
            } else {                                       \
                glColor3f(0.4f, 0.4f, 0.4f);               \
            }                                              \
        }
    #define BUILDING_DISPLAY_TRIANGLE_FILL_COL wool_setColourRGBA(ownerColour)
    
    #define BUILDING_DISPLAY_TRIANGLE_WIDTH_RADIUS 10.0f
    #define BUILDING_DISPLAY_TRIANGLE_HEIGHT 20.0f

    #define BUILDING_DISPLAY_ICON_X 1.0f
    #define BUILDING_DISPLAY_ICON_Y 1.0f
    
    #define BUILDING_DISPLAY_TEXT_OFFSETX 2.0f
    #define BUILDING_DISPLAY_TEXT_OFFSETY 1.0f
    #define BUILDING_DISPLAY_TEXT_SCALE (4.0f / 3.0f)
    #define BUILDING_DISPLAY_TEXT_COL if (buildingDisplayCanBeBuilt) { glColor3f(0.0f, 0.0f, 0.0f); } else { glColor3f(0.8f, 1.0f, 1.0f); }
    
    #define BUILDING_DISPLAY_BOX_HEIGHT 21.0f

    #define BUILDING_DISPLAY_BORDER 1.0f
    
    void ClientProvince::drawBuildingDisplay(const CameraProperties & cameraProperties) const {
        // Ignore provinces which are not drawing building displays.
        if (buildingDisplayRowCount == 0) return;
        
        // Get the UI scale from the camera properties.
        GLfloat centreX = getCentreViewX(cameraProperties),
                centreY = getCentreViewY(cameraProperties),
                uiScale = *cameraProperties.uiScale,
                border = std::floor(BUILDING_DISPLAY_BORDER * uiScale);
        
        // Triangle underneath box.
        GLfloat triRadius = BUILDING_DISPLAY_TRIANGLE_WIDTH_RADIUS * uiScale,
                triHeight = BUILDING_DISPLAY_BOX_HEIGHT * uiScale,
                triX1 = centreX - triRadius,
                triX2 = centreX + triRadius,
                triY1 = centreY - triHeight,
                triY2 = centreY;
        
        // Box above triangle.
        // Add "2 * border + 1" to box y position, so it matches up nicely with triangle.
        GLfloat boxWidth = buildingDisplayBoxWidth * uiScale,
                boxHeight = (buildingDisplayRowCount * BUILDING_DISPLAY_BOX_HEIGHT + BUILDING_DISPLAY_ICON_Y + (BUILDING_DISPLAY_BORDER * 2.0f)) * uiScale,
                boxX1 = std::floor(centreX - boxWidth * 0.5f),
                boxY1 = centreY - triHeight - boxHeight + (2.0f * border) + 1.0f,
                boxX2 = std::floor(boxX1 + boxWidth),
                boxY2 = boxY1 + boxHeight;
        
        // Draw outline shapes
        BUILDING_DISPLAY_OUTLINE_COL
        glVertex2f(boxX1, boxY1); glVertex2f(boxX2, boxY1); glVertex2f(boxX2, boxY2); glVertex2f(boxX1, boxY2); // Box background
        glVertex2f(centreX, triY1); glVertex2f(triX1, triY2); glVertex2f(triX2, triY2); glVertex2f(centreX, triY1); // Triangle background
        
        // Set triangle colour, subtract triangle borders, draw triangle.
        BUILDING_DISPLAY_TRIANGLE_FILL_COL
        triX1 += (border * 1.5f); triX2 -= (border * 1.5f); triY1 += (border * 2.0f); triY2 -= border;
        glVertex2f(centreX, triY1); glVertex2f(triX1, triY2); glVertex2f(triX2, triY2); glVertex2f(centreX, triY1); // Triangle background
        
        // Set box colour, subtract box borders, draw box.
        BUILDING_DISPLAY_FILL_COL
        boxX1 += border; boxX2 -= border; boxY1 += border; boxY2 -= border;
        glVertex2f(boxX1, boxY1); glVertex2f(boxX2, boxY1); glVertex2f(boxX2, boxY2); glVertex2f(boxX1, boxY2); // Box background
        
        // Set text colour.
        BUILDING_DISPLAY_TEXT_COL
        
        boxX1 += (BUILDING_DISPLAY_ICON_X * uiScale);
        boxY1 += (BUILDING_DISPLAY_ICON_Y * uiScale);
        GLfloat rowHeight = BUILDING_DISPLAY_BOX_HEIGHT * uiScale,
                textScale = BUILDING_DISPLAY_TEXT_SCALE * uiScale,
                iconSpace = (IconDrawer::BASE_ICON_WIDTH + BUILDING_DISPLAY_TEXT_OFFSETX) * uiScale,
                textYOff = BUILDING_DISPLAY_TEXT_OFFSETY * uiScale;
        
        // For each row, draw icon, draw text, increment.
        for (unsigned int i = 0; i < buildingDisplayRowCount; i++) {
            const BuildingDisplayRow & row = buildingDisplayRows[i];    
            IconDrawer::draw(row.icon, boxX1, boxY1, uiScale, uiScale);
            wool::Font::font.drawFloorStringScale(row.text, boxX1 + iconSpace, boxY1 + textYOff, textScale, textScale);
            boxY1 += rowHeight;
        }
    }
    
    
    // ========================================== Mouse and selection stuff ===============================================    
    
    void ClientProvince::onMouseMove(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY) {

        // Get the UI scale from the camera properties
        GLfloat uiScale = *cameraProperties.uiScale;

        // Do this for convenience.
        const std::list<Army> & armies = armyControl.getArmies();

        cameraProperties.provId = id;
        DEFINE_ARMY_POS(getCentreViewX(cameraProperties), getCentreViewY(cameraProperties))
        if (armyVisible(cameraProperties, armyDrawWidth)) {
            for (const Army & army : armies) {
                if (army.isMouseOver(cameraProperties, armyX, armyY, viewX, viewY)) {
                    cameraProperties.armyId = army.getArmyId();
                    return;
                }
                INCREMENT_ARMY_POS
            }
        }
        cameraProperties.armyId = Army::INVALID_ARMY;
    }

    bool ClientProvince::onMouseLeftClick(const CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY, bool multipleSelectionEnabled, RascClientNetIntf & netIntf, uint64_t turnCount) {

        // Get our player id.
        uint64_t ourPlayerId = netIntf.getData()->getId();

        // Get the UI scale from the camera properties
        GLfloat uiScale = *cameraProperties.uiScale;

        // Do this for convenience.
        const std::list<Army> & armies = armyControl.getArmies();

        DEFINE_ARMY_POS(getCentreViewX(cameraProperties), getCentreViewY(cameraProperties))

        // If armies are shown, then...
        if (armyVisible(cameraProperties, armyDrawWidth)) {
            // Loop through all armies in our province...
            for (const Army & army : armies) {
                // If the army is owned by us, and it has been moused over...
                if (army.getArmyOwner() == ourPlayerId && army.isMouseOver(cameraProperties, armyX, armyY, viewX, viewY)) {

                    // Run the appropriate select method, in the army selection controller, depending on whether multiple
                    // selection is enabled.
                    if (multipleSelectionEnabled) {
                        armySelectController->multipleSelect(getId(), army.getArmyId());
                    } else {
                        armySelectController->switchSelect(getId(), army.getArmyId());
                    }

                    // Return false as we have done something.
                    return false;
                }
                INCREMENT_ARMY_POS
            }
        }


        // --------------- NOW MOVE UNITS, IF WE DO NOTHING WITH SELECTION --------------------

        // If we are not an accessible province, do nothing, as to not allow movement of units.
        if (!isProvinceAccessible()) return true; // Return true, as we have done nothing.

        bool doneMove = false;

        simplenetwork::Message msg(ClientToServer::checkedUpdate);

        // Search all adjacent provinces.
        for (uint32_t adjId : adjacent) {
            Province & adjProv = gameMap.getProvince(adjId);
            for (uint64_t armyId : armySelectController->getProvinceSelectedArmies(adjId)) {
                // Find the army.
                std::list<Army>::const_iterator armyToMove = adjProv.armyControl.findArmyExternal(armyId);
                // If it exists...
                if (armyToMove != adjProv.armyControl.getArmies().end()) {
                    // Then move it.
                    doneMove = true;
                    std::cout << "Moving army " << armyId << " from province " << adjProv.getName() << " to " << name << ".\n";
                    armyControl.requestMoveArmy(CheckedUpdate::client(msg), adjProv.getId(), armyId);
                }
            }
        }

        if (doneMove) {
            netIntf.send(msg);
            return false; // Return false, we have moved a unit (done something).
        }
        return true; // Return true as we have done nothing.
    }
    
    
    // ========================================== Updating stuff (other) ==================================================
    
    void ClientProvince::updateBuildColourAndDisplay(const BuildMenu & buildMenu, uint64_t targetPlayerId) {
        
        // Colours used, defined here for convenience.
        #define NORMAL_COLOUR          wool::RGB::WHITE
        #define UNOWNED_COLOUR         wool::RGB(0.1f, 0.1f, 0.1f)
        #define BLOCKED_COLOUR         wool::RGB::DARK_RED //wool::RGB(0.7f, 0.0f, 0.0f)
        #define NOFILTER_DARKEN_R(col) ((col) * 0.5f)
        #define NOFILTER_DARKEN_G(col) ((col) * 0.5f)
        #define NOFILTER_DARKEN_B(col) ((col) * (0.5f * 1.1f))
        
        // Note whether this province is currently filtered.
        isCurrentlyFiltered = (buildMenu.getFilteredProvince() == this);
        
        // If not accessible, use normal colour, disable building display by setting row count to zero.
        if (!isProvinceAccessible()) {
            buildModeColour = NORMAL_COLOUR;
            buildingDisplayRowCount = 0;

        // If not owned by us, use unowned colour, disable building display by setting row count to zero.
        } else if (getProvinceOwner() != targetPlayerId) {
            buildModeColour = UNOWNED_COLOUR;
            buildingDisplayRowCount = 0;

        } else {
            // Otherwise, the province must be owned by us. (Separated if block since only accessible provinces
            // owned by us need highlighting by filtered province).
            
            // Work out whether the building can be built.
            // It can never be built if invalid or force blocked.
            if (buildMenu.getTargetBuilding() == Properties::Types::INVALID || buildMenu.getIsForceBlocked()) {
                buildingDisplayCanBeBuilt = false;
            } else {
                buildingDisplayCanBeBuilt = Traits::canTraitBeBuilt(*this, buildMenu.getTargetBuilding(), 1);
            }
            
            // If blocked building is forced, always as blocked colour.
            if (buildMenu.getIsForceBlocked()) {
                buildModeColour = BLOCKED_COLOUR;

            // If we don't have the trait, show as normal if buildable, or blocked otherwise.
            } else if (!properties.getPropertySet().hasPropertiesOfType(buildMenu.getTargetBuilding())) {
                buildModeColour = buildingDisplayCanBeBuilt ? NORMAL_COLOUR : BLOCKED_COLOUR;

            // Otherwise, show as a heat map.
            } else {
                provstat_t buildingValue = properties.getTraitCount(buildMenu.getTargetBuilding());
                GLfloat proportion = (GLfloat)(buildingValue - buildMenu.getMinBuildingQuantity()) / (buildMenu.getMaxBuildingQuantity() - buildMenu.getMinBuildingQuantity());
                proportion = std::min(std::max(proportion, 0.0f), 1.0f);
                if (proportion < (1.0f/3.0f)) {
                    proportion *= 3.0f;
                    buildModeColour = wool::RGB(1.0f, 1.0f, 1.0f - proportion);
                } else {
                    proportion = 1.5f * (proportion - (1.0f/3.0f));
                    buildModeColour = wool::RGB(1.0f - proportion, 1.0f - 0.2f * proportion, 0.0f);
                }
            }
            
            // For provinces we own, if there is a filter enabled and we're not the province
            // being filtered, darken the build mode colour by a bit.
            if (buildMenu.getFilteredProvince() != NULL && buildMenu.getFilteredProvince() != this) {
                buildModeColour.red = NOFILTER_DARKEN_R(buildModeColour.red);
                buildModeColour.green = NOFILTER_DARKEN_G(buildModeColour.green);
                buildModeColour.blue = NOFILTER_DARKEN_B(buildModeColour.blue);
            }
            
            // If there is no target building, show nothing.
            if (buildMenu.getTargetBuilding() == Properties::Types::INVALID) {
                buildingDisplayRowCount = 0;
                
            // If there is a target building, construct a display to represent it.
            } else {
                ProvStatModSet statModifier = Traits::getProvinceStatModiferSet(buildMenu.getTargetBuilding(), 1u);
                unsigned int textLength;

                // When filtered, display a more verbose set of information, for each of the stats which the building affects.
                if (buildMenu.getFilteredProvince() == this) {
                    unsigned int rowUpto = 0;
                    textLength = 0;
                    
                    // Display net manpower balance and manpower output percentage, as a modifier, if the building modifies manpower.
                    if (statModifier.doesModify(ProvStatIds::manpowerIncome)) {
                        buildingDisplayRows[rowUpto].icon = IconDef::manpower;
                        ProvStatMod manpower(properties[ProvStatIds::manpowerIncome] - properties[ProvStatIds::manpowerExpenditure],
                            properties[ProvStatIds::manpowerIncome].getAccumulatedStatModifiers().getPercentage() -
                            properties[ProvStatIds::manpowerExpenditure].getAccumulatedStatModifiers().getPercentage());
                        static_assert(15 <= BUILDING_DISPLAY_BUFFER_SIZE, "Buffer must be large enough.");
                        unsigned int len = NumberFormat::manpowerModifier<15>(buildingDisplayRows[rowUpto].text, manpower, false, true);
                        textLength = std::max(textLength, len);
                        rowUpto++;
                    }
                    
                    // Display net currency balance and currency output percentage, as a modifier, if the building modifies currency.
                    if (statModifier.doesModify(ProvStatIds::currencyIncome)) {
                        buildingDisplayRows[rowUpto].icon = IconDef::currency;
                        ProvStatMod currency(properties[ProvStatIds::currencyIncome] - properties[ProvStatIds::currencyExpenditure],
                            properties[ProvStatIds::currencyIncome].getAccumulatedStatModifiers().getPercentage() -
                            properties[ProvStatIds::currencyExpenditure].getAccumulatedStatModifiers().getPercentage());
                        static_assert(15 <= BUILDING_DISPLAY_BUFFER_SIZE, "Buffer must be large enough.");
                        unsigned int len = NumberFormat::currencyModifier<15>(buildingDisplayRows[rowUpto].text, currency, false, true);
                        textLength = std::max(textLength, len);
                        rowUpto++;
                    }
                    
                    // As the last row, always show land use/available.
                    buildingDisplayRows[rowUpto].icon = IconDef::land;
                    static_assert(12 <= BUILDING_DISPLAY_BUFFER_SIZE, "Buffer must be large enough.");
                    unsigned int len = NumberFormat::landUseAvailable<12>(buildingDisplayRows[rowUpto].text,
                        properties[ProvStatIds::landUsed], properties[ProvStatIds::landAvailable]);
                    textLength = std::max(textLength, len);
                    
                    // Store how many rows we generated.
                    buildingDisplayRowCount = rowUpto + 1;
                    
                // When not filtered, show only the most important stat as a single row -
                // a percentage output for manpower/currency buildings, land display for province buildings.
                } else {
                    buildingDisplayRowCount = 1;
                    
                    if (statModifier.getProvStatModValue(ProvStatIds::manpowerIncome) != 0) {
                        buildingDisplayRows[0].icon = IconDef::manpower;
                        static_assert(7 <= BUILDING_DISPLAY_BUFFER_SIZE, "Buffer must be large enough.");
                        textLength = NumberFormat::percentage<7>(buildingDisplayRows[0].text,
                            properties[ProvStatIds::manpowerIncome].getAccumulatedStatModifiers().getPercentage() -
                            properties[ProvStatIds::manpowerExpenditure].getAccumulatedStatModifiers().getPercentage(), true);
                        
                    } else if (statModifier.getProvStatModValue(ProvStatIds::currencyIncome) != 0) {
                        buildingDisplayRows[0].icon = IconDef::currency;
                        static_assert(7 <= BUILDING_DISPLAY_BUFFER_SIZE, "Buffer must be large enough.");
                        textLength = NumberFormat::percentage<7>(buildingDisplayRows[0].text,
                            properties[ProvStatIds::currencyIncome].getAccumulatedStatModifiers().getPercentage() -
                            properties[ProvStatIds::currencyExpenditure].getAccumulatedStatModifiers().getPercentage(), true);                    
                        
                    } else {
                        buildingDisplayRows[0].icon = IconDef::land;
                        static_assert(12 <= BUILDING_DISPLAY_BUFFER_SIZE, "Buffer must be large enough.");
                        textLength = NumberFormat::landUseAvailable<12>(buildingDisplayRows[0].text,
                            properties[ProvStatIds::landUsed], properties[ProvStatIds::landAvailable]);
                    }
                }

                // TODO: Integrate this calculation with definitions of sizes for building display.
                buildingDisplayBoxWidth = textLength * (wool::Font::font.getCharWidth() * (4.0f / 3.0f)) + 28.0f;
            }
        }
    }
    
    void * ClientProvince::addOnProvinceChangeFunc(std::function<void(void)> func) {
        return onProvinceChange.addFunction(func);
    }
    
    void ClientProvince::removeOnProvinceChangeFunc(void * token) {
        onProvinceChange.removeFunction(token);
    }
}

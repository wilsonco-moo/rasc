#include "ClientGameMap.h"

#include <wool/texture/ThreadTexture.h>
#include <tinyxml2/tinyxml2.h>

#include "../../common/gameMap/mapElement/types/Continent.h"
#include "../../common/gameMap/mapElement/types/Province.h"
#include "../../common/gameMap/mapElement/types/Area.h"
#include "../config/MapModeController.h"
#include "../config/CameraProperties.h"
#include "../../common/util/XMLUtil.h"
#include "../../common/util/Misc.h"
#include "../RascClientNetIntf.h"
#include "ClientProvince.h"
#include "../RascBox.h"

namespace rasc {

    ClientGameMap::ClientGameMap(RascBox & rascBox) :
        GameMap(
            // Note that both of the following textures are safe to access, since it is guaranteed that these
            // textures are loaded BEFORE the GameMap is created.
            rascBox.provincesTexture->getInternalTexture()->getSize(), // For the map's REAL SIZE, use the size of the provinces texture.
            rascBox.getCurrentMapModeTexture().getSize(),              // For the map's texture size, us the size of one of the map mode's texture sheets.
            rascBox.common
        ),
        rascBox(rascBox),
        provincesTexture(rascBox.provincesTexture->getInternalTexture()) {
    }

    ClientGameMap::~ClientGameMap(void) {
    }
    
    Province * ClientGameMap::createNewProvince(void) {
        return new ClientProvince(*this, &getArmySelectController());
    }

    void ClientGameMap::loadFromXML(tinyxml2::XMLDocument * xml) {
        GameMap::loadFromXML(xml);

        // Read the root element.
        tinyxml2::XMLElement * root = readElement(*xml, "gameMap");

        // Read the properties element from the root element.
        tinyxml2::XMLElement * globalProperties = readElement(root, "properties");

        // Load camera stuff from this properties element.
        rascBox.cameraProperties->readGlobalProperties(globalProperties);
    }

    // =================================================================================================

    void ClientGameMap::draw(GLfloat elapsed, bool drawMinimap) const {

        const CameraProperties & cameraProperties = *rascBox.cameraProperties;

        uint64_t turnCount = rascBox.common.turnCount;

        glEnable(GL_TEXTURE_2D);
            rascBox.getCurrentMapModeTexture().bind();
            glBegin(GL_QUADS);
                // First we draw the world
                if (rascBox.mapModeController->getCurrentMapMode() == MapModes::geographical) {
                    for (const Province * prov : provinces) {
                        ((const ClientProvince *)prov)->draw(cameraProperties);
                    }
                } else if (rascBox.uiIsBuildMenuOpen()) {
                    for (const Province * prov : provinces) {
                        ((const ClientProvince *)prov)->drawBuildMode(cameraProperties);
                    }
                } else {
                    for (const Province * prov : provinces) {
                        ((const ClientProvince *)prov)->drawWithOwnershipColour(cameraProperties);
                    }
                }
            glEnd();
        glDisable(GL_TEXTURE_2D);

        glBegin(GL_QUADS);
            switch(rascBox.mapModeController->getCurrentMapMode()) {
            case MapModes::geographical: case MapModes::provincial:
                for (const Province * prov : provinces) {
                    prov->drawName(cameraProperties, true);
                }
                break;
            case MapModes::area:
                // Note: In area map mode, we draw the province name labels AND the area name labels.
                for (const Province * prov : provinces) {
                    prov->drawName(cameraProperties, false);
                }
                for (const Area * area : areas) {
                    area->drawName(cameraProperties, true);
                }
                break;
            case MapModes::continental:
                for (const Continent * cont : continents) {
                    cont->drawName(cameraProperties, true);
                }
                break;
            }

            if (rascBox.uiIsBuildMenuOpen()) {
                // Draw building displays otherwise.
                for (const Province * prov : provinces) {
                    ((const ClientProvince *)prov)->drawBuildingDisplay(cameraProperties);
                }
            } else {
                // Draw armies when not in build mode.
                for (const Province * prov : provinces) {
                    ((const ClientProvince *)prov)->drawArmies(cameraProperties, turnCount);
                }
            }

        // Next, we draw the minimap.
        // First draw the background for it.
        if (drawMinimap) {
            MISC_SCALE_BOTTOMLEFT(0, MINIMAP_HEIGHT)
            wool_setColourRGBA(cameraProperties.minimapBackgroundColour);
            MISC_SCALE_RECT(0, 0, MINIMAP_WIDTH, MINIMAP_HEIGHT)
        }

        // Finally, we draw the minimap's textures.
        glEnd();
        if (drawMinimap) {
            glEnable(GL_TEXTURE_2D);
                rascBox.getCurrentMapModeTexture().bind();
                glBegin(GL_QUADS);
                    for (const Province * prov : provinces) {
                        ((const ClientProvince *)prov)->minimapDraw(cameraProperties);
                    }
                glEnd();
            glDisable(GL_TEXTURE_2D);
        }

        // Next we draw the rectangular camera area onto the minimap, using a crap-load of scaling maths.
        if (drawMinimap) {

            GLfloat widthOnMap = (cameraProperties.viewWidth  / cameraProperties.xScale),
                    heightOnMap = (cameraProperties.viewHeight / cameraProperties.yScale);

            // Only draw the camera highlight if our camera view is smaller than the map.
            if (widthOnMap <= provincesTexture->getWidth() || heightOnMap <= provincesTexture->getHeight()) {

                // Work out the scaling factor between the actual world coordinates and the minimap.
                GLfloat minimapScale = (cameraProperties.minimapActualWidth * cameraProperties.minimapDrawScale) / provincesTexture->getWidth();

                // Work out the centre position (on the minimap) that the highlight should appear.
                GLfloat minimapCameraCentreX = cameraProperties.xCamera * minimapScale + cameraProperties.minimapDrawX,
                        minimapCameraCentreY = cameraProperties.yCamera * minimapScale + cameraProperties.minimapDrawY;

                // Work out the radius around this centre position that the edges of the highligh should be
                GLfloat minimapCameraRadiusX = widthOnMap * minimapScale * 0.5f,
                        minimapCameraRadiusY = heightOnMap * minimapScale * 0.5f;

                // Get the UI scale from the camera properties
                GLfloat uiScale = *cameraProperties.uiScale;

                // Get the maximum bounds for drawing the minimap, (so we can do min/max).
                GLfloat mMinX = 0,
                        mMinY = cameraProperties.minimapDrawY,
                        mMaxX = MINIMAP_WIDTH * uiScale,
                        mMaxY = cameraProperties.minimapDrawY + MINIMAP_HEIGHT * uiScale;

                // After that, work out the actual coordinates for the minimap, by using min/max on these bounds, and the radius.
                GLfloat mX1 = fmin(fmax(minimapCameraCentreX - minimapCameraRadiusX, mMinX), mMaxX),
                        mY1 = fmin(fmax(minimapCameraCentreY - minimapCameraRadiusY, mMinY), mMaxY),
                        mX2 = fmin(fmax(minimapCameraCentreX + minimapCameraRadiusX, mMinX), mMaxX),
                        mY2 = fmin(fmax(minimapCameraCentreY + minimapCameraRadiusY, mMinY), mMaxY);

                // Finally, actually draw the highlight.
                glBegin(GL_QUADS);
                glColor4f(1.0f, 1.0f, 1.0f, 0.3f);
                    glVertex2f(mX1, mY1);
                    glVertex2f(mX2, mY1);
                    glVertex2f(mX2, mY2);
                    glVertex2f(mX1, mY2);
                glEnd();

            }
        }
    }

    GLfloat ClientGameMap::viewXToMapX(GLfloat viewX) const {
        return (viewX - rascBox.cameraProperties->x) / rascBox.cameraProperties->xScale;
    }
    GLfloat ClientGameMap::viewYToMapY(GLfloat viewY) const {
        return (viewY - rascBox.cameraProperties->y) / rascBox.cameraProperties->yScale;
    }
    GLfloat ClientGameMap::mapXToViewX(GLfloat mapX) const {
        return mapX * rascBox.cameraProperties->xScale + rascBox.cameraProperties->x;
    }
    GLfloat ClientGameMap::mapYToViewY(GLfloat mapY) const {
        return mapY * rascBox.cameraProperties->yScale + rascBox.cameraProperties->y;
    }

    // =============================== MOUSE POSITION/SELECTION =========================================

    /**
     * This internal macro returns an iterator to a province, at the given mouse position.
     * This is used internally in a few mouse-based methods.
     */
    #define PROVINCE_ITERATOR_AT_MOUSE(viewX, viewY)                                    \
                                                                                        \
        int mapX = (int)viewXToMapX(viewX),                                             \
            mapY = (int)viewYToMapY(viewY);                                             \
                                                                                        \
        std::unordered_map<uint32_t, uint32_t>::const_iterator prov;                    \
                                                                                        \
        if (mapX < 0 || mapX >= (int)provincesTexture->getWidth() ||                    \
            mapY < 0 || mapY >= (int)provincesTexture->getHeight()) {                   \
            prov = coloursToProvinces.end();                                            \
        } else {                                                                        \
            prov = coloursToProvinces.find(provincesTexture->getPixelInt(mapX, mapY));  \
        }

    uint32_t ClientGameMap::getProvinceAtMousePosition(GLfloat viewX, GLfloat viewY) const {
        PROVINCE_ITERATOR_AT_MOUSE(viewX, viewY)
        if (prov == coloursToProvinces.end()) {
            return MapElement::INVALID_MAP_ELEMENT;
        } else {
            return prov->second;
        }
    }

    void ClientGameMap::onMouseMove(GLfloat viewX, GLfloat viewY) {
        PROVINCE_ITERATOR_AT_MOUSE(viewX, viewY)
        if (prov == coloursToProvinces.end()) {
            rascBox.cameraProperties->provId = MapElement::INVALID_MAP_ELEMENT;
            rascBox.cameraProperties->armyId = Army::INVALID_ARMY;
        } else {
            ((ClientProvince *)provinces[prov->second])->onMouseMove(*rascBox.cameraProperties, viewX, viewY);
        }
    }

    Province * ClientGameMap::onMouseLeftClick(GLfloat viewX, GLfloat viewY, bool multipleSelect) {
        uint64_t turnCount = rascBox.common.turnCount;
        bool turnFinished = rascBox.netIntf->haveFinishedTurn();

        PROVINCE_ITERATOR_AT_MOUSE(viewX, viewY)
        if (prov == coloursToProvinces.end()) {
            // If the user clicks outside the map
            rascBox.cameraProperties->provId = MapElement::INVALID_MAP_ELEMENT;
            rascBox.cameraProperties->armyId = Army::INVALID_ARMY;
            // Unselect all armies from the army select controller.
            armySelectController.unselect();
            return NULL;
        } else {

            // Otherwise, automatically call onMouseMove
            ClientProvince * clickedProvince = (ClientProvince *)provinces[prov->second];
            clickedProvince->onMouseMove(*rascBox.cameraProperties, viewX, viewY);

            if (turnFinished) {
                return clickedProvince;
            } else {
                // Only run the province on click method, if we have not finished our turn.
                if (clickedProvince->onMouseLeftClick(*rascBox.cameraProperties, viewX, viewY, multipleSelect, *rascBox.netIntf, turnCount)) {
                    // If the mouse left click method returns true, i.e: it clicked on nothing, then run the
                    // army select controller's unselect method. This means that if the user clicks outside an army,
                    // it will unselect it.
                    bool areAmiesSelected = armySelectController.isAnyArmySelected();
                    armySelectController.unselect();
                    return areAmiesSelected ? NULL : clickedProvince;
                }
                return NULL;
            }
        }
    }

    void ClientGameMap::centreCamera(void) {
        // First centre the view position.
        rascBox.cameraProperties->xCamera = provincesTexture->getWidth()  * 0.5f;
        rascBox.cameraProperties->yCamera = provincesTexture->getHeight() * 0.5f;
    }
}

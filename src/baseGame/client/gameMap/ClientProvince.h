/*
 * ClientProvince.h
 *
 *  Created on: 2 May 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_GAMEMAP_CLIENTPROVINCE_H_
#define BASEGAME_CLIENT_GAMEMAP_CLIENTPROVINCE_H_

#include <wool/texture/TextureRegion.h>
#include <wool/misc/RGB.h>
#include <functional>
#include <GL/gl.h>

#include "../../common/util/templateTypes/TokenFunctionList.h"
#include "../../common/gameMap/mapElement/types/Province.h"
#include "../../common/util/numeric/KShortenedNumber.h"

namespace rasc {
    class CameraProperties;
    class BuildMenu;
    
    /**
     * ClientProvince is a subclass of Province, which is the class responsible for holding
     * all province data, (various stats, armies, etc). See Province.h for more information.
     * On the game client, all provinces are ClientProvince.
     * ClientProvince contains the following extensions:
     *  > Methods for drawing the province, when in-game.
     *  > Methods for drawing armies, when in-game.
     *  > Methods for mouse integration.
     */
    class ClientProvince : public Province {
    private:
        // -------- Loaded from XML ----------
        
        // Whether we can be drawn onto the minimap.
        bool drawMinimap;
        // The texture region for drawing our province onto the minimap.
        wool::TextureRegion minimapTextureRegion;
        // The location within the minimap to draw the minimap section representing our province.
        GLfloat minimapX, minimapY, minimapWidth, minimapHeight;
        
        // ------ Other drawing data ---------
        
        // The text we draw for the garrison display. This is updated
        // each time we get an onProvinceChange.
        char garrisonText[KShortenedNumber::SIZE];
        // Stores whether we should draw the fort/garrison display.
        // This is also updated each time we get an onProvinceChange.
        bool hasFort;
        // This stores the colour used for province display during build mode.
        // This is worked out in updateBuildModeColour.
        wool::RGB buildModeColour;
        
        // Maximum rows for the building display.
        constexpr static unsigned int BUILDING_DISPLAY_MAX_ROWS = 3;
        // Buffer size for text stored on each building display row.
        constexpr static unsigned int BUILDING_DISPLAY_BUFFER_SIZE = 15;
        // Current number of building display rows.
        // This is zero where building display is not being drawn (provinces we don't own).
        unsigned int buildingDisplayRowCount;
        // Width of building display box, resized to fit longest text in any of the rows.
        GLfloat buildingDisplayBoxWidth;
        // Whether the buildings can be built (if not, building display should be greyed out).
        bool buildingDisplayCanBeBuilt;
        // Whether this province is currently filtered (affects colour of building display).
        bool isCurrentlyFiltered;
        
        // Stores each building display row.
        class BuildingDisplayRow {
        public:
            unsigned int icon;
            char text[BUILDING_DISPLAY_BUFFER_SIZE];
        };
        BuildingDisplayRow buildingDisplayRows[BUILDING_DISPLAY_MAX_ROWS];
        
        // ------------ Misc -----------------
        
        // This is run when we get a misc update, server update, or initial data update.
        // Note that this is NOT run when we receive a checked update, since that never happens on the
        // client side.
        // Functions can be added/removed with addOnProvinceChangeFunc and removeOnProvinceChangeFunc.
        // Used by province menu: A function is added for the duration that the province menu is
        // attached to the province.
        TokenFunctionList<void(void)> onProvinceChange;
        
    public:
        ClientProvince(GameMap & gameMap, ArmySelectController * armySelectController);
        virtual ~ClientProvince(void);
        
    private:
        // Internal draw scale stuff.
        GLfloat getArmyDrawWidth(const CameraProperties & cameraProperties) const;
        bool armyVisible(const CameraProperties & cameraProperties, GLfloat drawWidth) const;
        bool armyVisible(const CameraProperties & cameraProperties) const;
        // Draws our fort and garrison. This must be called by drawArmies, if we have a fort.
        void drawFort(GLfloat x, GLfloat y, GLfloat uiScale) const;
        
    protected:
        // Override to return a plain ProvinceBattleController.
        virtual ProvinceBattleController * createNewBattleController(void) override;
        
        // Override to update garrison display, fort status etc. Also runs
        // onProvinceChange (see documentation for it above).
        virtual void runOnProvinceChange(void) override;
        
    public:
        // Override to load client specific province data.
        virtual void loadFromXML(tinyxml2::XMLElement * element) override;
        
        // -------------------------- Public drawing methods -------------------------------
        
        /**
         * Map drawing methods:
         * draw: Draws using white colour, to be used in geographical map mode.
         * drawBuildMode: Draws using our calculated build mode colour, to be
         *                used in build mode.
         * drawWithOwnershipColour: Draws using the colour of our owner player, to be used in
         *                          all regular map modes. This also shows siege "bucket" display.
         * minimapDraw: If minimap drawing is enabled for this province, this draws a
         *              tiny minimap variant of the province, at the appropriate place.
         */
        void draw(const CameraProperties & cameraProperties) const;
        void drawBuildMode(const CameraProperties & cameraProperties) const;
        void drawWithOwnershipColour(const CameraProperties & cameraProperties) const;
        void minimapDraw(const CameraProperties & cameraProperties) const;
        
        /**
         * Must be called after draw when in build mode, draws build mode display.
         */
        void drawBuildingDisplay(const CameraProperties & cameraProperties) const;
        
        /**
         * This must be called after draw, this draws all our associated armies.
         */
        void drawArmies(const CameraProperties & cameraProperties, uint64_t turnCount) const;
        
        
        // -------------------------- Mouse and selection stuff ----------------------------
        
        /**
         * This should be called when it is confirmed that the mouse is over
         * this province, and it has just moved. This method updates the province mouse-over selection, and
         * the army mouse-over selection.
         */
        void onMouseMove(CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY);

        /**
         * This should be called when the user left-clicks on this province.
         * This returns true if the user has clicked on nothing, so something else can be done.
         */
        bool onMouseLeftClick(const CameraProperties & cameraProperties, GLfloat viewX, GLfloat viewY, bool multipleSelectionEnabled, RascClientNetIntf & netIntf, uint64_t turnCount);
        
        
        // -------------------------- Updating stuff (other) -------------------------------
        
        /**
         * This updates, using information from the build menu, both the colour for this
         * province to display, and the building display.
         * The build menu colour is the province colour shown when drawBuildMode
         * is called, and the building display is used to summarise buildings in
         * build mode. This must be called by the build menu, whenever relevant data changes.
         */
        void updateBuildColourAndDisplay(const BuildMenu & buildMenu, uint64_t targetPlayerId);
        
        /**
         * Add and remove functions called on province change.
         * See the documentation for onProvinceChange.
         */
        void * addOnProvinceChangeFunc(std::function<void(void)> func);
        void removeOnProvinceChangeFunc(void * token);
    };
}

#endif

/*
 * ClientGameMap.h
 *
 *  Created on: 11 Jul 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_GAMEMAP_CLIENTGAMEMAP_H_
#define BASEGAME_CLIENT_GAMEMAP_CLIENTGAMEMAP_H_

#include "../../common/gameMap/GameMap.h"

namespace wool {
    class Texture;
}

namespace rasc {

    class RascClientNetIntf;
    class RascBox;

    /**
     * ClientGameMap is a subclass of GameMap, which is the class responsible for holding
     * all map data, (e.g: provinces, areas, continents etc). See GameMap.h for more information.
     * ClientGameMap contains the following extensions:
     *  > Methods for drawing the GameMap's provinces, when in-game.
     *  > Methods for mouse integration.
     *  > Implementation for the method which returns a ProvinceBattleController.
     */
    class ClientGameMap : public GameMap {
    private:
        /**
         * This stores our respective RascBox. This is always defined, as this subclass
         * of GameMap is only used on the client-side.
         */
        RascBox & rascBox;

        /**
         * The texture containing the location and colours of all provinces: provinces.png.
         * This is used mainly for mouse support, so that we know which province the user clicked on
         * given an arbitrary mouse position.
         */
        wool::Texture * provincesTexture;

    public:
        ClientGameMap(RascBox & rascBox);
        virtual ~ClientGameMap(void);

    protected:
        // Override to return a ClientProvince.
        virtual Province * createNewProvince(void) override;
        
    public:
        virtual void loadFromXML(tinyxml2::XMLDocument * xml) override;

        void draw(GLfloat elapsed, bool drawMinimap) const;

        GLfloat viewXToMapX(GLfloat viewX) const;
        GLfloat viewYToMapY(GLfloat viewY) const;
        GLfloat mapXToViewX(GLfloat mapX) const;
        GLfloat mapYToViewY(GLfloat mapY) const;


        uint32_t getProvinceAtMousePosition(GLfloat viewX, GLfloat viewY) const;

        /**
         * This should be called each time the mouse moves.
         * This method updates the CameraProperties' province and army selection, be calling the appropriate
         * Province's onMouseMove method.
         */
        void onMouseMove(GLfloat viewX, GLfloat viewY);

        /**
         * This should be called each time the mouse is left-clicked.
         * (The same stuff as onMouseMove() is automatically called).
         * This will update the selection of armies within provinces.
         * multipleSelect should be whether the CTRL key is pressed.
         *
         * This method returns a province, if the user's click did nothing, (to allow something
         * else to happen).
         * This method returns NULL if the user clicked on an army, if clicking resulting
         * in an army being unselected, or if they clicked outside the map.
         */
        Province * onMouseLeftClick(GLfloat viewX, GLfloat viewY, bool multipleSelect);

        /**
         * Centres the camera. This should be called from the GLUT (UI) thread.
         */
        void centreCamera(void);
    };
}

#endif

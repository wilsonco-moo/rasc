/*
 * LobbyRoom.h
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_LOBBYROOM_H_
#define BASEGAME_CLIENT_LOBBYROOM_H_

#include <string>

#include <sockets/plus/networkinterface/NetworkInterface.h>
#include <wool/room/managed/RoomManaged.h>


namespace rasc {

    class RascBox;
    class CameraProperties;
    class MapModeController;

    class LobbyRoom : public wool::RoomManaged {

    public:
        RascBox * box;

        LobbyRoom(RascBox * net);
        virtual ~LobbyRoom(void);

        virtual void init(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;


    protected:
        virtual void onStep(void) override;

    };
}
#endif

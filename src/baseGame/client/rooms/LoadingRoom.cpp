/*
 * GameRoom.cpp
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#include "LoadingRoom.h"

#include <iostream>

#include <wool/window/base/WindowBase.h>
#include "../../common/config/StaticConfig.h"
#include <wool/room/base/RoomBase.h>
#include "../../server/RascBaseGameServer.h"
#include "../mainMenu/MainMenuBackground.h"
#include "../../server/ServerBox.h"
#include "../RascClientNetIntf.h"
#include "../GlobalProperties.h"
#include "../RascBox.h"
#include "LobbyRoom.h"

// The amount of time to wait between knowing everything has finished,
// and actually changing room.
#define LOAD_FINISHED_TIME 0.2f


namespace rasc {

    // --------------------------- The static method for initialising the game's client --------------------

    LoadingRoom * LoadingRoom::initialiseGameClient(GlobalProperties * globalProperties, std::string & failureReason) {

        // Create the box.
        std::cout << "Creating RascBox...\n";
        RascBox * box = new RascBox(globalProperties);

        // If we are required to start an internal server, then do so, complaining if that failed.
        if (globalProperties->startInternalServer) {
            if (!box->serverBox->server->listenToPort(globalProperties->port)) {
                failureReason = "Failed to bind/listen to port " + std::to_string(globalProperties->port);
                delete box; return NULL;
            }

            // If we are required to load an existing save, do that. If it fails, complain.
            if (!globalProperties->startNew) {
                if (!box->serverBox->server->loadGame(StaticConfig::getFullSaveFilepath(globalProperties->saveName))) {
                    failureReason = "Failed to load savefile.";
                    delete box; return NULL;
                }
            }
        }

        // Create a condition variable, so we can wait for the network interface later.
        std::condition_variable_any variable;
        box->netIntf->setMapNameCondition(&variable);

        // Connect to the server, return NULL if this fails.
        if (!box->netIntf->connectToServer(globalProperties->address, globalProperties->port, globalProperties->connectionTimeout)) {
            std::cout << "Connection failed, deleting RascBox.\n";
            failureReason = "Failed to connect to server.";
            delete box; return NULL;
        }

        // If this succeeds, wait for the client to receive a response about the map name and server version.
        std::cout << "Connection to server succeeded, waiting for map name and server version...\n";
        {
            std::unique_lock<std::recursive_mutex> lock(box->netIntf->getKillableMutex());
            while(!box->netIntf->haveNotifiedAboutMapName()) {
                variable.wait(lock);
            }
        }

        // If the map filename is still empty, then the server has disconnected us before sending us the map
        // filename. In that case, return a failure. Otherwise, we can assume that the server *has* sent us
        // version and map filename data.
        if (box->globalProperties->mapFilename.empty()) {
            std::cout << "Server disconnected us before sending us a version and map filename.\n";
            failureReason = "Server disconnected us.";
            delete box; return NULL;
        }

        // If the server game version is different, complain and fail.
        if (box->globalProperties->serverVersion != StaticConfig::RASC_VERSION_WITHOUT_PLATFORM) {
            std::cout << "Cannot connect to server, server version: " << box->globalProperties->serverVersion << ", our game version: " << StaticConfig::RASC_VERSION_WITHOUT_PLATFORM << ".\n";
            failureReason = "Server is " + box->globalProperties->serverVersion + ".";
            delete box; return NULL;
        }
        std::cout << "Server game version matches ours: " << box->globalProperties->serverVersion << ".\n";

        // If the map the server wants us to use doesn't exist, complain and fail.
        if (!StaticConfig::isValidMapDirectory(StaticConfig::MAPS_LOCATION + box->globalProperties->mapFilename, true)) {
            std::cout << "We do not have a local copy of the map: " << box->globalProperties->mapFilename << ", which the server wants us to use.\n";
            failureReason = "Missing map: " + box->globalProperties->mapFilename + ".";
            delete box; return NULL;
        }
        std::cout << "We do have a local copy of the map: " << box->globalProperties->mapFilename << ", which the server wants us to use.\n";

        return new LoadingRoom(box);
    }

    // -----------------------------------------------------------------------------------------------------

    LoadingRoom::LoadingRoom(RascBox * box) :
        box(box),
        loadingFinishedElapsed(0.0f) {
        viewOptions.viewMode = wool::viewModeExtend;
    }

    LoadingRoom::~LoadingRoom(void) {
        delete box;
    }

    void LoadingRoom::init(void) {
        // Call this in the init function, as it is the first opportunity that our WindowBase instance
        // becomes available.
        box->enterLoadingRoom(getWindowBase());
    }

    void LoadingRoom::end(void) {}

    void LoadingRoom::keyNormal(unsigned char key, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());
        box->onUIKeyPress(key, false);
    }
    void LoadingRoom::keyNormalRelease(unsigned char key, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());
        box->onUIKeyRelease(key, false);
    }
    void LoadingRoom::keySpecial(int key, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());
        box->onUIKeyPress(key, true);
    }
    void LoadingRoom::keySpecialRelease(int key, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());
        box->onUIKeyRelease(key, true);
    }
    void LoadingRoom::mouseEvent(int button, int state, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());
        box->onUIMouseEvent(button, state, windowXToViewX(x), windowYToViewY(y));
    }
    void LoadingRoom::mouseMove(int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());
        box->onUIMouseMove(windowXToViewX(x), windowYToViewY(y));
    }
    void LoadingRoom::mouseDrag(int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());
        box->onUIMouseMove(windowXToViewX(x), windowYToViewY(y));
    }
    bool LoadingRoom::shouldDeleteOnRoomEnd(void) { return true; }

    void LoadingRoom::onStep(void) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        // Update the camera properties, draw the main menu background, then draw the UI.
        box->cameraProperties->step(getWindowBase()->elapsed(), getViewCurrentWidth(), getViewCurrentHeight());
        box->globalProperties->mainMenuBackground->draw(getViewCurrentWidth(), getViewCurrentHeight(), getWindowBase()->elapsed());
        glBegin(GL_QUADS);
            box->drawUI(getWindowBase()->elapsed());
        glEnd();

        // If initialising has finished, start incrementing loadingFinishedElapsed.
        if (box->hasInitialiserJobFinished()) {
            loadingFinishedElapsed += getWindowBase()->elapsed();
        }
        // Once loadingFinishedElapsed reaches LOAD_FINISHED_TIME, change to the lobby room.
        if (loadingFinishedElapsed > LOAD_FINISHED_TIME && box->netIntf->readyToContinueToLobby()) {
            getWindowBase()->changeRoom(new LobbyRoom(box));
            box = NULL;
        }
    }
}

/*
 * GameRoom.h
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_LOADINGROOM_H_
#define BASEGAME_CLIENT_LOADINGROOM_H_

#include <GL/gl.h>
#include <string>

#include <wool/room/managed/RoomManaged.h>
#include "../config/CameraProperties.h"


namespace rasc {
    class RascBox;
    class MapModeController;
    class GlobalProperties;

    class LoadingRoom : public wool::RoomManaged {
    public:
        // --------------------------- The static method for initialising the game's client --------------------

        /**
         * This method:
         *  > Creates a RascBox
         *  > Attempts to connect it to the server, specified by GlobalProperties.
         *  > If successful, creates a LoadingRoom associated with this RascBox, and returns it.
         *  > If unsuccessful, deletes the RascBox and returns NULL.
         *
         * This method is intended to be used by the launcher and the main menu to initialise the game.
         *
         * The user should provide a reference to a std::string. If initialiseGameClient fails for whatever reason,
         * then this string will be populated with a reason for the operation failing.
         */
        static LoadingRoom * initialiseGameClient(GlobalProperties * globalProperties, std::string & failureReason);

        // -----------------------------------------------------------------------------------------------------

    private:
        /**
         * The network interface we are tasked with giving to the GameRoom, when we create it.
         */
        RascBox * box;
        /**
         * Allows us to show the "Loading finished" message for an amount of time.
         */
        GLfloat loadingFinishedElapsed;
    public:

        LoadingRoom(RascBox * box);
        virtual ~LoadingRoom(void);

        virtual void init(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;


    protected:
        virtual void onStep(void) override;

    };
}
#endif

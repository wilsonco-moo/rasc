/*
 * GameRoom.cpp
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#include "GameRoom.h"

#include <GL/freeglut.h>
#include <GL/freeglut_std.h>
#include <GL/gl.h>
#include <iostream>
#include <unordered_map>

#include <wool/window/base/WindowBase.h>
#include <wool/misc/RGBA.h>

#include "../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../common/gameMap/mapElement/types/Province.h"
#include "../../common/gameMap/mapElement/MapElement.h"
#include "../../common/stats/UnifiedStatSystem.h"
#include "../../common/playerData/PlayerData.h"
#include "../config/UnitPlacementController.h"
#include "../../common/gameMap/units/Army.h"
#include "../../common/config/ReleaseMode.h"
#include "../config/MapModeController.h"
#include "../config/CameraProperties.h"
#include "../gameMap/ClientGameMap.h"
#include "../../common/util/Misc.h"
#include "../RascClientNetIntf.h"
#include "../ui/GameUIMenuDef.h"
#include "../GlobalProperties.h"
#include "../RascBox.h"

namespace rasc {

    GameRoom::GameRoom(RascBox * box) :
        #ifdef ENABLE_NAME_MOVER
            nameMover(*box),
        #endif
        multipleSelectEnabled(false),
        unitsPlacedThisTurn(0),
        lastTurnUnitsPlaced(0),
        box(box) {
        box->enterGameRoom();
        background = wool::RGBA::BLACK;
        viewOptions.viewMode = wool::viewModeExtend;
    }

    GameRoom::~GameRoom(void) {
        delete box;
    }

    void GameRoom::init(void) {
    }

    void GameRoom::end(void) {}

    void GameRoom::keyNormal(unsigned char key, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        if (box->onUIKeyPress(key, false)) {
            switch(key) {
            case ' ':
                box->netIntf->finishTurn();
                break;
            case '-':
                box->cameraProperties->zoom(-1);
                break;
            case '=': case '+':
                box->cameraProperties->zoom(1);
                break;
            case 'w': case 'W':
                box->cameraProperties->setMovingUp(true);
                break;
            case 'a': case 'A':
                box->cameraProperties->setMovingLeft(true);
                break;
            case 's': case 'S':
                box->cameraProperties->setMovingDown(true);
                break;
            case 'd': case 'D':
                box->cameraProperties->setMovingRight(true);
                break;
            case '1':
                box->mapModeController->setMapMode(MapModes::geographical);
                break;
            case '2':
                box->mapModeController->setMapMode(MapModes::provincial);
                break;
            case '3':
                box->mapModeController->setMapMode(MapModes::area);
                break;
            case '4':
                box->mapModeController->setMapMode(MapModes::continental);
                break;
            case 'q': case 'Q':
                box->unitPlacementController->setModifierKey1(true);
                break;
            case 'e': case 'E':
                box->unitPlacementController->setModifierKey2(true);
                break;
            case 27: // Esc
                box->uiOpenOverlayMenu(GameUIMenus::gameMenu, false);
                break;
            case 'b': case 'B':
                box->uiOpenOverlayMenu(GameUIMenus::billMenu, false);
                break;
            case 'x': case 'X':
                box->runSplitArmies();
                break;
            case 'c': case 'C':
                box->runMergeArmies();
                break;
            #ifdef RASC_DEVELOPMENT_MODE
                case 'o': case 'O':
                    box->uiOpenOverlayMenu(GameUIMenus::console, false);
                    break;
            #endif
            case 'g': case 'G':
                box->runAbsorbArmiesIntoGarrisons();
                break;
            }

            #ifdef ENABLE_NAME_MOVER
                nameMover.onNormalKeyPress(key);
            #endif
        }
    }

    void GameRoom::keyNormalRelease(unsigned char key, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        if (box->onUIKeyRelease(key, false)) {
            switch(key) {
            case 'w': case 'W':
                box->cameraProperties->setMovingUp(false);
                break;
            case 'a': case 'A':
                box->cameraProperties->setMovingLeft(false);
                break;
            case 's': case 'S':
                box->cameraProperties->setMovingDown(false);
                break;
            case 'd': case 'D':
                box->cameraProperties->setMovingRight(false);
                break;
            case 'q': case 'Q':
                box->unitPlacementController->setModifierKey1(false);
                break;
            case 'e': case 'E':
                box->unitPlacementController->setModifierKey2(false);
                break;
            }

            #ifdef ENABLE_NAME_MOVER
                nameMover.onNormalKeyRelease(key);
            #endif
        }
    }

    void GameRoom::keySpecial(int key, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        if (box->onUIKeyPress(key, true)) {
            switch(key) {
            case 1:
                box->globalProperties->increaseUIScale();
                break;
            case 2:
                box->globalProperties->decreaseUIScale();
                break;
            case 3:
                box->globalProperties->resetUIScale();
                break;
            case 114: case 115: // ctrl
                multipleSelectEnabled = true;
                box->unitPlacementController->setModifierKey1(true);
                break;
            case 112: case 113: // shift
                box->unitPlacementController->setModifierKey2(true);
                break;
            }
        }
    }
    void GameRoom::keySpecialRelease(int key, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        if (box->onUIKeyRelease(key, true)) {
            switch(key) {
            case 114: case 115:
                multipleSelectEnabled = false;
                box->unitPlacementController->setModifierKey1(false);
                break;
            case 112: case 113: // shift
                box->unitPlacementController->setModifierKey2(false);
                break;
            }
        }
    }
    void GameRoom::mouseEvent(int button, int state, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        CameraProperties * cameraProperties = box->cameraProperties;
        GLfloat viewX = windowXToViewX(x), viewY = windowYToViewY(y);

        if (state == 0) {
            switch(button) {

            case 3:
                if (box->onUIMouseEvent(button, state, viewX, viewY)) {
                    cameraProperties->zoom(1);
                }
                break;
            case 4:
                if (box->onUIMouseEvent(button, state, viewX, viewY)) {
                    cameraProperties->zoom(-1);
                }
                break;

            default:
                if (box->onUIMouseEvent(button, state, viewX, viewY)) {

                    if (button == GLUT_LEFT_BUTTON) {
                        if (box->uiIsBuildMenuOpen()) {
                            // If build menu is open, notify build menu if the user clicked on a valid province.
                            if (cameraProperties->provId != MapElement::INVALID_MAP_ELEMENT) {
                                Province & province = box->map->getProvince(cameraProperties->provId);
                                box->uiBuildMenuOnLeftClickProvince(&province);
                                // When build menu is open at the same time as province menu, also notify province menu.
                                if (box->uiIsProvinceMenuOpen()) {
                                    box->uiShowProvinceMenu(&province);
                                }
                            }
                        } else {
                            // Otherwise run map left click, showing province menu if user didn't click on anything.
                            Province * province = box->map->onMouseLeftClick(viewX, viewY, multipleSelectEnabled);
                            if (province != NULL) {
                                box->uiShowProvinceMenu(province);
                            }
                        }

                    } else if (button == GLUT_RIGHT_BUTTON && cameraProperties->provId != MapElement::INVALID_MAP_ELEMENT) {
                        
                        Province & province = box->map->getProvince(cameraProperties->provId);
                        if (box->uiIsBuildMenuOpen()) {
                            // Notify build menu if open.
                            box->uiBuildMenuOnRightClickProvince(&province);
                        } else {
                            // Request placement of units in the appropriate province. We can rely on the
                            // server to do all checks.
                            province.armyControl.requestPlaceUnits(CheckedUpdate::clientAuto(box->netIntf), box->unitPlacementController->getMaximumUnitsToPlace());
                        }
                    }
                }
                break;
            }
        } else {
            if (button != 3 && button != 4) {
                box->onUIMouseEvent(button, state, viewX, viewY);
            }
        }
    }
    void GameRoom::mouseMove(int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        GLfloat viewX = windowXToViewX(x), viewY = windowYToViewY(y);
        if (box->onUIMouseMove(viewX, viewY)) {
            box->map->onMouseMove(viewX, viewY);
        }
    }
    void GameRoom::mouseDrag(int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        box->onUIMouseMove(windowXToViewX(x), windowYToViewY(y));
    }
    bool GameRoom::shouldDeleteOnRoomEnd(void) { return true; }


    void GameRoom::onStep(void) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        #ifdef ENABLE_NAME_MOVER
            nameMover.step(getWindowBase()->elapsed(), getViewCurrentHeight());
        #endif

        box->common.statSystem->runAfterUpdateFunctions();

        box->cameraProperties->step(getWindowBase()->elapsed(), getViewCurrentWidth(), getViewCurrentHeight());

        box->map->draw(getWindowBase()->elapsed(), true);

        glBegin(GL_QUADS);
        box->drawUI(getWindowBase()->elapsed());
        glEnd();
    }


}

/*
 * GameRoom.h
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_GAMEROOM_H_
#define BASEGAME_CLIENT_GAMEROOM_H_

#include <cstdint>
#include <string>

#include <sockets/plus/networkinterface/NetworkInterface.h>
#include <wool/room/managed/RoomManaged.h>

#include "../config/NameMover.h"

namespace rasc {

    class RascBox;


    class GameRoom : public wool::RoomManaged {
    private:
        #ifdef ENABLE_NAME_MOVER
            NameMover nameMover;
        #endif

        bool multipleSelectEnabled;

        int unitsPlacedThisTurn;
        uint64_t lastTurnUnitsPlaced;

    public:
        RascBox * box;

        GameRoom(RascBox * net);
        virtual ~GameRoom(void);

        virtual void init(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;


    protected:
        virtual void onStep(void) override;

    };
}
#endif

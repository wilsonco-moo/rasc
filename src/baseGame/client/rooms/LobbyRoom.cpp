/*
 * LobbyRoom.cpp
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#include "LobbyRoom.h"

#include <sockets/plus/message/Message.h>
#include <wool/window/base/WindowBase.h>
#include <sockets/plus/util/MutexPtr.h>
#include <GL/freeglut.h>
#include <iostream>
#include <GL/gl.h>

#include "../../common/gameMap/mapElement/types/Province.h"
#include "../config/MapModeController.h"
#include "../config/CameraProperties.h"
#include "../../common/GameMessages.h"
#include "../gameMap/ClientGameMap.h"
#include "../RascClientNetIntf.h"
#include "../GlobalProperties.h"
#include "../ui/GameUIMenuDef.h"
#include "../RascBox.h"
#include "GameRoom.h"

namespace rasc {

    LobbyRoom::LobbyRoom(RascBox * box) :
        box(box) {
        box->enterLobbyRoom();
        background = wool::RGBA::BLACK;
        viewOptions.viewMode = wool::viewModeExtend;
        std::cout << "Entering lobby screen.\n";
    }

    LobbyRoom::~LobbyRoom(void) {
        delete box;
    }

    void LobbyRoom::init(void) {
    }

    void LobbyRoom::end(void) {}

    void LobbyRoom::keyNormal(unsigned char key, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        if (box->onUIKeyPress(key, false)) {
            switch(key) {
            case ' ':
                break;
            case '-':
                box->cameraProperties->zoom(-1);
                break;
            case '=': case '+':
                box->cameraProperties->zoom(1);
                break;
            case 'w': case 'W':
                box->cameraProperties->setMovingUp(true);
                break;
            case 'a': case 'A':
                box->cameraProperties->setMovingLeft(true);
                break;
            case 's': case 'S':
                box->cameraProperties->setMovingDown(true);
                break;
            case 'd': case 'D':
                box->cameraProperties->setMovingRight(true);
                break;
            case '1':
                box->mapModeController->setMapMode(MapModes::geographical);
                break;
            case '2':
                box->mapModeController->setMapMode(MapModes::provincial);
                break;
            case '3':
                box->mapModeController->setMapMode(MapModes::area);
                break;
            case '4':
                box->mapModeController->setMapMode(MapModes::continental);
                break;
            case 27: // Esc
                box->uiOpenOverlayMenu(GameUIMenus::gameMenu, false);
                break;
            }
        }
    }

    void LobbyRoom::keyNormalRelease(unsigned char key, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        if (box->onUIKeyRelease(key, false)) {
            switch(key) {
            case 'w': case 'W':
                box->cameraProperties->setMovingUp(false);
                break;
            case 'a': case 'A':
                box->cameraProperties->setMovingLeft(false);
                break;
            case 's': case 'S':
                box->cameraProperties->setMovingDown(false);
                break;
            case 'd': case 'D':
                box->cameraProperties->setMovingRight(false);
                break;
            }
        }
    }

    void LobbyRoom::keySpecial(int key, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        if (box->onUIKeyPress(key, true)) {
            switch(key) {
            case 1:
                box->globalProperties->increaseUIScale();
                break;
            case 2:
                box->globalProperties->decreaseUIScale();
                break;
            case 3:
                box->globalProperties->resetUIScale();
                break;
            }
        }
    }

    void LobbyRoom::keySpecialRelease(int key, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());
        box->onUIKeyRelease(key, true);
    }
    void LobbyRoom::mouseEvent(int button, int state, int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        GLfloat viewX = windowXToViewX(x), viewY = windowYToViewY(y);
        if (box->onUIMouseEvent(button, state, viewX, viewY)) {

            box->map->onMouseMove(viewX, viewY);
            CameraProperties * cameraProperties = box->cameraProperties;

            if (state == 0) {
                switch(button) {

                case 3:
                    if (box->onUIMouseEvent(button, state, viewX, viewY)) {
                        cameraProperties->zoom(1);
                    }
                    break;
                case 4:
                    if (box->onUIMouseEvent(button, state, viewX, viewY)) {
                        cameraProperties->zoom(-1);
                    }
                    break;
                }
            } else {

                // Only select provinces if the user presses the left mouse button.
                // Look out for mouse releases, since otherwise the room change which follows might cause the UI system
                // to miss the mouse release event.
                if (button == GLUT_LEFT_BUTTON) {
                    if (cameraProperties->provId != MapElement::INVALID_MAP_ELEMENT && !box->netIntf->isWaitingForServerResponseInLobby()) {
                        // Show the warning if this province is too bad. Otherwise, we can immediately claim it.s
                        if (!box->uiLobbyProvinceWarnIfNeeded(cameraProperties->provId)) {
                            // We have clicked on a province, so request that we can use it.
                            box->netIntf->lobbyClaimProvince(cameraProperties->provId);
                        }
                    }
                }
            }
        }
    }
    void LobbyRoom::mouseMove(int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        GLfloat viewX = windowXToViewX(x), viewY = windowYToViewY(y);
        if (box->onUIMouseMove(viewX, viewY)) {
            box->map->onMouseMove(windowXToViewX(x), windowYToViewY(y));
        }
    }
    void LobbyRoom::mouseDrag(int x, int y) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());
        box->onUIMouseMove(windowXToViewX(x), windowYToViewY(y));
    }
    bool LobbyRoom::shouldDeleteOnRoomEnd(void) { return true; }


    void LobbyRoom::onStep(void) {
        std::lock_guard<std::recursive_mutex> lock(box->netIntf->getKillableMutex());

        box->cameraProperties->step(getWindowBase()->elapsed(), getViewCurrentWidth(), getViewCurrentHeight());

        box->map->draw(getWindowBase()->elapsed(), false);

        glBegin(GL_QUADS);
        box->drawUI(getWindowBase()->elapsed());
        glEnd();

        if (box->netIntf->readyToContinueToGame()) {
            std::cout << "Exiting lobby room and changing to game room...\n";
            getWindowBase()->changeRoom(new GameRoom(box));
            box = NULL;
        }
    }
}

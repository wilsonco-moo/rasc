/*
 * NewUI.h
 *
 *  Created on: 23 Nov 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_GAMEUI_H_
#define BASEGAME_CLIENT_UI_GAMEUI_H_

#include <GL/gl.h>
#include <mutex>

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/Rectangle.h>

#include "containers/developmentConsole/ClientDevelopmentConsole.h"
#include "containers/modularTopbar/menu/ModularTopbarConfigMenu.h"
#include "containers/clientSidePlayerList/ClientSidePlayerList.h"
#include "containers/lobbyProvincePopup/LobbyProvincePopup.h"
#include "containers/provinceMenu/PropertyDisplayPanel.h"
#include "containers/playerColourBar/PlayerColourBar.h"
#include "containers/modularTopbar/ModularTopbar.h"
#include "containers/provinceMenu/ProvinceMenu.h"
#include "containers/bottomLeft/BottomLeftUI.h"
#include "containers/loadingMenu/LoadingMenu.h"
#include "../../common/config/ReleaseMode.h"
#include "containers/buildMenu/BuildMenu.h"
#include "containers/billMenu/BillMenu.h"
#include "containers/gameMenu/GameMenu.h"

namespace rascUI {
    class Rectangle;
    class Theme;
}

namespace rasc {

    class RascClientNetIntf;
    class CameraProperties;
    class RascBox;

    /**
     * This class contains all of the UI components that are used in the loading room, lobby room and game room.
     */
    class GameUI {
    private:
        std::mutex dataMutex;
        RascBox & box;

        rascUI::Theme * theme; // This is assigned from GlobalProperties, and is here for convenience.

    public: // Make the containers public. It seems silly to implement wrapper methods here for *everything*

        // This is public so outside classes can use the FunctionQueues.
        rascUI::TopLevelContainer topLevel;
        
        // ------------------------ NEW UI COMPONENTS, DECLARE THEM HERE -----------------------------
        // Note: Currently these components will not respond to keyboard/mouse events until a province has been selected.
        
        BottomLeftUI bottomLeft;
        ClientSidePlayerList playerList;
        LobbyProvincePopup lobbyProvincePopup;
        GameMenu gameMenu;
        BillMenu billMenu;
        LoadingMenu loadingMenu;
        #ifdef RASC_DEVELOPMENT_MODE
            ClientDevelopmentConsole console;
        #endif
        ProvinceMenu provinceMenu;
        PropertyDisplayPanel propertyDisplayPanel;
        BuildMenu buildMenu;
        ModularTopbar modularTopbar;
        ModularTopbarConfigMenu modularTopbarConfigMenu;
        PlayerColourBar playerColourBar;

        // -------------------------------------------------------------------------------------------

    public:
        GameUI(RascBox & box);
        virtual ~GameUI(void);

        void drawUI(const rascUI::Rectangle & view, GLfloat elapsed);
        bool onUIMouseEvent(int button, int state, GLfloat viewX, GLfloat viewY);
        bool onUIMouseMove(GLfloat viewX, GLfloat viewY);
        bool onUIKeyPress(int key, bool special);
        bool onUIKeyRelease(int key, bool special);
        void setScalePointers(const GLfloat * xScalePtr, const GLfloat * yScalePtr);

        void onChangeToLoadingRoom(void);
        void onChangeToLobbyRoom(void);
        void onChangeToGameRoom(void);
        
        /**
         * Closes ALL existing overlay menus, OR
         * opens the specified menu ID (see GameUIMenuDef.h).
         * If autoOpen is passed as true, both of the above *can* happen
         * at the same time.
         * If forceOpen is passed as true, the menu is not closed if already
         * open.
         */
        void openOverlayMenu(unsigned int menuId, bool autoOpen, bool forceOpen = false);
    };
}

#endif

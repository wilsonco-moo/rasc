/*
 * NewUI.cpp
 *
 *  Created on: 23 Nov 2018
 *      Author: wilson
 */

#include "GameUI.h"

#include <iostream>

#include "../GlobalProperties.h"
#include "GameUIMenuDef.h"
#include "../RascBox.h"

namespace rasc {

    GameUI::GameUI(RascBox & box) :
        dataMutex(),
        box(box),

        // ---------------------------- DEFINE NEW UI COMPONENTS HERE --------------------------------
        // Note: Currently these components will not respond to keyboard/mouse events until a province has been selected.


        theme(box.globalProperties->themeLoader.getTheme()),
        topLevel(theme, &rascUI::Bindings::defaultOpenGL),

        bottomLeft(box, theme),
        playerList(box, theme),
        lobbyProvincePopup(box, theme),
        gameMenu(box, theme),
        billMenu(box, theme),
        loadingMenu(box, theme),
        #ifdef RASC_DEVELOPMENT_MODE
            console(box, theme),
        #endif
        provinceMenu(box, theme),
        propertyDisplayPanel(box, theme),
        buildMenu(box, theme),
        modularTopbar(box, theme),
        modularTopbarConfigMenu(box, theme),
        playerColourBar(box, theme) {

        // --------------------------- DEFINE LAYOUT OF COMPONENTS HERE ------------------------------

        topLevel.add(&playerList);
        topLevel.add(&playerColourBar);
        topLevel.add(&bottomLeft);
        #ifdef RASC_DEVELOPMENT_MODE
            topLevel.add(&console);
        #endif
        topLevel.add(&provinceMenu);
        topLevel.add(&propertyDisplayPanel);
        topLevel.add(&buildMenu);
        topLevel.add(&modularTopbarConfigMenu);
        topLevel.add(&modularTopbar);
        topLevel.add(&lobbyProvincePopup);
        topLevel.add(&gameMenu);
        topLevel.add(&billMenu);
        topLevel.add(&loadingMenu);

        // -------------------------------------------------------------------------------------------

        // Make the top left UI and bottom left UI initially invisible, to avoid these from being
        // shown in the game room. The visible/invisible approach is simpler than delaying adding them to the
        // top level component, since this way the UI can access it's top level component before being shown in the UI.
        bottomLeft.setVisible(false);
        playerList.setVisible(false);
        billMenu.setVisible(false);
        modularTopbar.setVisible(false);
        playerColourBar.setVisible(false);
    }

    void GameUI::onChangeToLoadingRoom(void) {
        // Notify the loading menu that we entered the loading screen. This sets up it's job status components.
        // This must be done AFTER textures start loading.
        loadingMenu.onChangeToLoadingRoom();
    }

    void GameUI::onChangeToLobbyRoom(void) {
        // Run this for the top left and bottom left UI: This is the first point in time where the game map is available.
        bottomLeft.onChangeToLobbyRoom();
        modularTopbar.onChangeToLobbyRoom();

        // Make the loading menu invisible, as it is no longer needed.
        loadingMenu.setVisible(false);

        // Make the player list visible when we enter the lobby room, since this is the first point in time that
        // the rasc updatable system is initialised, and by extension the first time the player list shows anything useful.
        playerList.setVisible(true);
    }

    void GameUI::onChangeToGameRoom(void) {
        // Make the top left and bottom left UI visible once we enter the game room.
        bottomLeft.setVisible(true);
        modularTopbar.setVisible(true);
        playerColourBar.setVisible(true);
        // Make sure lobby province popup always gets hidden when switching to game room.
        lobbyProvincePopup.hide();
    }

    void GameUI::openOverlayMenu(unsigned int menuId, bool autoOpen, bool forceOpen) {
        // If forceOpen and the menu is already open, do nothing.
        if (forceOpen) {
            switch(menuId) {
                #define X(name, ...)         \
                    case GameUIMenus::name:  \
                        if (name.isOpen()) { \
                            return;          \
                        }                    \
                        break;
                RASC_GAMEUI_MENU_LIST
                #undef X
            }
        }
        
        // Close any existing menus, record what we closed.
        unsigned int closedMenu = GameUIMenus::INVALID;
        #define X(name, ...)                        \
            if (name.isOpen()) {                    \
                name.hide();                        \
                closedMenu = GameUIMenus::name;     \
            }
        RASC_GAMEUI_MENU_LIST
        #undef X

        // If we closed something, and either auto open is false, or we just closed ourself,
        // give up. The second check is needed to stop cases where menus close then immediately open.
        if (closedMenu != GameUIMenus::INVALID && (closedMenu == menuId || !autoOpen)) return;

        // Open new menu.
        switch(menuId) {
            #define X(name, ...)        \
                case GameUIMenus::name: \
                    name.show();        \
                    break;
            RASC_GAMEUI_MENU_LIST
            #undef X
        }
    }


    GameUI::~GameUI(void) {
    }
    void GameUI::drawUI(const rascUI::Rectangle & view, GLfloat elapsed) {
        topLevel.draw(view, elapsed);
    }
    bool GameUI::onUIMouseEvent(int button, int state, GLfloat viewX, GLfloat viewY) {
        return topLevel.mouseEvent(button, state, viewX, viewY);
    }
    bool GameUI::onUIMouseMove(GLfloat viewX, GLfloat viewY) {
        return topLevel.mouseMove(viewX, viewY);
    }
    bool GameUI::onUIKeyPress(int key, bool special) {
        return topLevel.keyPress(key, special);
    }
    bool GameUI::onUIKeyRelease(int key, bool special) {
        return topLevel.keyRelease(key, special);
    }
    void GameUI::setScalePointers(const GLfloat * xScalePtr, const GLfloat * yScalePtr) {
        topLevel.setScalePointers(xScalePtr, yScalePtr);
    }
}

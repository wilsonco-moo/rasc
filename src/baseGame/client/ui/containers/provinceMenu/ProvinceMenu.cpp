/*
 * ProvinceMenu.cpp
 *
 *  Created on: 18 Sep 2019
 *      Author: wilson
 */

#include "ProvinceMenu.h"

#include <rascUI/base/TopLevelContainer.h>
#include <wool/texture/ThreadTexture.h>
#include <rascUI/util/FunctionQueue.h>
#include <threading/ThreadQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <type_traits>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <mutex>

#include "../../../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../../../common/gameMap/mapElement/types/Continent.h"
#include "../../../../common/util/definitions/TerrainDisplayDef.h"
#include "../../../../common/gameMap/mapElement/types/Province.h"
#include "../../../../common/gameMap/properties/trait/TraitDef.h"
#include "../../../../common/gameMap/properties/trait/Traits.h"
#include "../../../../common/gameMap/mapElement/types/Area.h"
#include "../../../../common/gameMap/properties/Properties.h"
#include "../../../../common/playerData/PlayerDataSystem.h"
#include "../../../../common/util/numeric/NumberFormat.h"
#include "../../../../common/playerData/PlayerStatArr.h"
#include "../../../../common/util/definitions/IconDef.h"
#include "../../../../common/stats/UnifiedStatSystem.h"
#include "../../../../common/playerData/PlayerData.h"
#include "../../../../common/stats/types/AreasStat.h"
#include "../../../../common/util/macros/MacroUtil.h"
#include "../../customComponents/icon/IconButton.h"
#include "../../../config/TerrainDisplayManager.h"
#include "../../../../common/GameMessages.h"
#include "../../../gameMap/ClientProvince.h"
#include "../../../gameMap/ClientGameMap.h"
#include "../../../../common/util/Misc.h"
#include "../../../RascClientNetIntf.h"
#include "../../../GlobalProperties.h"
#include "PropertyDisplayPanel.h"
#include "../../../RascBox.h"

#define LAYOUT_THEME theme

// The overall location of the province menu.
#define OVERALL_WIDTH  (500 + 3 * UI_BORDER)
#define OVERALL_HEIGHT (355 + 7 * UI_BORDER)
#define OVERALL_R_BORDER 8
#define OVERALL_B_BORDER 8

#define OVERALL_LOC                                      \
    MOVE_XY(-OVERALL_R_BORDER, -OVERALL_B_BORDER,        \
     PIN_R(OVERALL_WIDTH, PIN_B(OVERALL_HEIGHT,          \
      GEN_FILL                                           \
    )))

// The height of the title bar, excluding borders.
#define TITLE_BAR_HEIGHT UI_BHEIGHT
// The height of the bottom bar, excluding borders.
#define BOTTOM_BAR_HEIGHT UI_BHEIGHT

// The location of the main area of this UI block, i.e: Without the top and bottom bars.
#define MAIN_AREA_LOC                       \
    SUB_BORDERMARGIN_T(TITLE_BAR_HEIGHT,    \
     SUB_BORDERMARGIN_B(BOTTOM_BAR_HEIGHT,  \
      GEN_BORDER                            \
    ))                                      \

// The location of the left half of the area, (with and without a middle border).
#define LEFT_AREA_LOC \
    PIN_L(TERRAIN_DISPLAY_OVERALL_WIDTH, MAIN_AREA_LOC)

// The location of the right half of the area.
#define RIGHT_AREA_LOC \
    SUB_BORDERMARGIN_L(TERRAIN_DISPLAY_OVERALL_WIDTH, MAIN_AREA_LOC)




// For the title label and title panel.
#define TITLE_LOC                               \
    PIN_T(TITLE_BAR_HEIGHT,                     \
     SUB_BORDERMARGIN_R(CLOSE_BUTTON_WIDTH,     \
      GEN_BORDER                                \
    ))

// For the close button.
#define CLOSE_BUTTON_WIDTH 64
#define CLOSE_BUTTON_LOC        \
    PIN_T(TITLE_BAR_HEIGHT,     \
     PIN_R(CLOSE_BUTTON_WIDTH,  \
      GEN_BORDER                \
    ))




// The overall size of the panel containing the terrain display panel.
#define TERRAIN_DISPLAY_OVERALL_WIDTH (TERRAIN_DISPLAY_WIDTH + 2 * UI_BORDER)
#define TERRAIN_DISPLAY_OVERALL_HEIGHT (TERRAIN_DISPLAY_HEIGHT + 2 * UI_BORDER + TERRAIN_TYPE_LABEL_HEIGHT)
// The size of the terrain display panel: This is fixed, and must match the resolution of the textures.
#define TERRAIN_DISPLAY_WIDTH 256
#define TERRAIN_DISPLAY_HEIGHT 128

// The locations of the terrain display panels.
#define TERRAIN_DISPLAY_OVERALL_LOC \
    PIN_T(TERRAIN_DISPLAY_OVERALL_HEIGHT, LEFT_AREA_LOC)

#define TERRAIN_DISPLAY_LOC             \
    MOVE_XY(UI_BORDER, UI_BORDER,       \
     SETSIZE_X(TERRAIN_DISPLAY_WIDTH,   \
      PIN_T(TERRAIN_DISPLAY_HEIGHT,     \
       LEFT_AREA_LOC                    \
    )))

// The location of the label which shows the type of the terrain, (the bit under the terrain display panel).
#define TERRAIN_TYPE_LABEL_HEIGHT UI_BHEIGHT
#define TERRAIN_TYPE_LABEL_LOC \
    PIN_B(TERRAIN_TYPE_LABEL_HEIGHT, TERRAIN_DISPLAY_OVERALL_LOC)




// The location of the trait top back panel. The front panel's location can be worked out by
// subtracting a border from top and bottom from this.
#define TRAIT_TOP_PANEL_LOC                                 \
    PIN_T(COUNT_OUTBORDER_Y(1),                             \
     SUB_MARGIN_T(TERRAIN_DISPLAY_OVERALL_HEIGHT,           \
      LEFT_AREA_LOC                                         \
    ))

// The number of buttons above the trait scroll pane.
#define TRAIT_TITLE_BUTTONS_COUNT 2

// The location of buttons above the tratis scroll pane.
#define TRAIT_TITLE_BUTTON_LOC(id)                  \
    BORDERTABLE_X(id, TRAIT_TITLE_BUTTONS_COUNT,    \
     SUB_BORDER_Y(                                  \
      TRAIT_TOP_PANEL_LOC                           \
    ))

// Note: Subtract negative border from bottom, to hide double border when scrolled to bottom.
#define TRAIT_SCROLLPANE_LOC                                                    \
    SUB_MARGIN_B(-UI_BORDER,                                                    \
     SUB_BORDERMARGIN_T(COUNT_ONEBORDER_Y(1) + TERRAIN_DISPLAY_OVERALL_HEIGHT,  \
     LEFT_AREA_LOC                                                              \
    ))





// The location of the owner panel, (containing owner label, owner player panel).
#define OWNER_PANEL_HEIGHT UI_BHEIGHT
#define OWNER_PANEL_LOC \
    PIN_T(OWNER_PANEL_HEIGHT, RIGHT_AREA_LOC)
#define OWNER_LABEL_WIDTH 65
#define OWNER_LABEL_LOC \
    PIN_L(OWNER_LABEL_WIDTH, OWNER_PANEL_LOC)
#define OWNER_PLAYER_PANEL_LOC \
    SUB_BORDERMARGIN_L(OWNER_LABEL_WIDTH, OWNER_PANEL_LOC)

// The location of the geo panel.
#define GEO_PANEL_HEIGHT COUNT_INBORDER_Y(2)
#define GEO_PANEL_LOC                           \
    PIN_T(GEO_PANEL_HEIGHT,                     \
     SUB_BORDERMARGIN_T(OWNER_PANEL_HEIGHT,     \
      RIGHT_AREA_LOC                            \
    ))

// The location of the text rows within the geo panel.
#define GEO_PANEL_ROW_LOC(rowId) \
    MOVE_BORDERNORMAL_Y(rowId, SETNORMAL_Y(GEO_PANEL_LOC))

// The location of the stat panel.
#define STAT_PANEL_LOC                      \
    SUB_BORDERMARGIN_T(OWNER_PANEL_HEIGHT,  \
     SUB_BORDERMARGIN_T(GEO_PANEL_HEIGHT,   \
      RIGHT_AREA_LOC                        \
    ))

// The location of the rows of text within the stats panel.
#define STAT_PANEL_ROW_LOC(rowId, isBottom)                              \
    MOVE_BORDERNORMAL_Y((rowId) + (isBottom) * STAT_PANEL_BOTTOM_OFFSET, \
     SETNORMAL_Y(                                                        \
      SUB_BORDER_T(                                                      \
       STAT_PANEL_LOC                                                    \
    )))

// Additional offset for bottom stat panels.
#define STAT_PANEL_BOTTOM_OFFSET 0.25f

// The location within the stat panel of the "Advanced stats" button.
#define ADVANCED_STATS_BUTTON_LOC \
    PIN_T(UI_BHEIGHT,             \
     PIN_R(48,                    \
      SUB_BORDER(                 \
       STAT_PANEL_LOC             \
    )))

// -------------------------------- Bottom section --------------------------------

// The location of the bottom back panel. The location of the bottom front panel is given by subtracting a border from this.
#define BOTTOM_BACK_PANEL_LOC \
    PIN_B(BOTTOM_BAR_HEIGHT + 2 * UI_BORDER, GEN_FILL)

// The location of sections within the bottom panel.
#define BOTTOM_PANEL_CONTENTS_LOC(id)       \
    DEFINED_BORDERTABLE_X(BottomPanel, id,  \
     SUB_BORDER(                            \
      BOTTOM_BACK_PANEL_LOC                 \
    ))

// Bottom panel table ratios.
DEFINE_TABLE(BottomPanel, 32, 16, 7, 7, 7, 12)



namespace rasc {

    // ---------------- Custom components ---------------
    #define TRAITBUTTON_CONTENTS_LOC \
        SUB_BORDER_R(SUB_BORDER_B(GEN_FILL))
    ProvinceMenu::TraitButton::TraitButton(rascUI::Theme * theme, rascUI::ToggleButtonSeries * series) :
        IconToggleButton(series, rascUI::Location(TRAITBUTTON_CONTENTS_LOC)),
        currentTrait(Properties::Types::INVALID) {
    }
    ProvinceMenu::TraitButton::~TraitButton(void) {
    }
    void ProvinceMenu::TraitButton::setTrait(proptype_t trait, provstat_t count) {
        currentTrait = trait;
        std::string traitName = Traits::getFriendlyName(trait);
        if (count != 1) {
            NUMBER_FORMAT_DECL(countBuffer, genericSigned, 6, " [", count, false)
            traitName += countBuffer;
            traitName += ']';
        }
        setText(traitName);
        setIconId(Traits::propertyGetIconId(trait));
    }
    proptype_t ProvinceMenu::TraitButton::getTrait(void) const {
        return currentTrait;
    }

    // --------------------------------------------------

    ProvinceMenu::TerrainDisplayPanel::TerrainDisplayPanel(const rascUI::Location & location, RascBox & box) :
        rascUI::Component(location),
        box(box),
        textureSizeBytes(box.globalProperties->terrainDisplayManager->getTextureSizeBytes()),
        texture(box.globalProperties->terrainDisplayManager->getWidth(), box.globalProperties->terrainDisplayManager->getHeight(), true, false),
        token(box.globalProperties->threadQueue->createToken()),
        nextTexture(NULL) {

        // Complain if the texture failed to load (allocate).
        if (!texture.getHasRamCopy()) {
            std::cerr << "CRITICAL ERROR: ProvinceMenu: Terrain display panel texture failed to allocate.\n";
            exit(EXIT_FAILURE);
        }
    }
    ProvinceMenu::TerrainDisplayPanel::~TerrainDisplayPanel(void) {
        // Delete the thread queue token, then the next texture. Do it this order
        // since the next texture is modified from within background jobs run by the thread queue.
        box.globalProperties->threadQueue->deleteToken(token);
        free(nextTexture);
    }
    void ProvinceMenu::TerrainDisplayPanel::onDraw(void) {
        glEnd();
        glEnable(GL_TEXTURE_2D);

        // If the background thread has loaded a next texture, copy it into the texture and update.
        if (nextTexture != NULL) {
            memcpy(texture.getRamCopyOfTexture(), nextTexture, textureSizeBytes);
            free(nextTexture);
            nextTexture = NULL;
            if (texture.getHasVRamCopy()) {
                texture.updateVRamCopy();
            } else {
                texture.loadVRamCopy();
            }
        }

        // If the texture has a VRAM copy, draw it.
        if (texture.getHasVRamCopy()) {
            texture.bind();
            glBegin(GL_QUADS);
            glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            wool_textureRectangle(location.cache.x, location.cache.y, TERRAIN_DISPLAY_WIDTH * location.xScale, TERRAIN_DISPLAY_HEIGHT * location.yScale);
            glEnd();
        }

        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
    }
    bool ProvinceMenu::TerrainDisplayPanel::shouldRespondToKeyboard(void) const {
        return false;
    }
    void ProvinceMenu::TerrainDisplayPanel::updateFromProvince(Province * province) {

        // Build a vector of layers from the properties in the province (with a sky layer).
        std::vector<TerDispLayer> layers;
        PropertySet & propertySet = province->properties.getPropertySet();
        for (const std::pair<const uint64_t, PropertySet::TypeAndStorage> & pair : propertySet.getProperties()) {
            propertySet.addTerrDispLayers(pair.first, layers);
        }
        Traits::addSkyLayer(layers);

        // Build terrain texture in a background thread. Use a barrier job to make sure calls
        // don't end up overlapping.
        box.globalProperties->threadQueue->addBarrier(token, [this, layers = std::move(layers)](void) {
            unsigned char * newTextureData = (unsigned char *)malloc(textureSizeBytes);
            if (newTextureData == NULL) {
                std::cerr << "CRITICAL ERROR: ProvinceMenu: Failed to allocate memory when updating texture.\n";
                exit(EXIT_FAILURE);
            }
            box.globalProperties->terrainDisplayManager->buildTerrainTexture(std::move(layers), newTextureData);

            // Once building the new texture is done, synchronise and set it as next texture (removing any existing one).
            // It is safe to synchronise to this, since deleting (and thus waiting for this to finish)
            // is done outside any mutex.
            std::lock_guard<std::recursive_mutex> lock(box.netIntf->getKillableMutex());
            free(nextTexture);
            nextTexture = newTextureData;
        });
    }

    // ------------------------------------------------
    
    ProvinceMenu::GarrisonDisplayPanel::GarrisonDisplayPanel(const rascUI::Location & location) :
        rascUI::FrontPanel(location),
        value(7, &NumberFormat::manpower<7>, false) {
    }
    
    ProvinceMenu::GarrisonDisplayPanel::~GarrisonDisplayPanel(void) {
    }
    
    void ProvinceMenu::GarrisonDisplayPanel::onDraw(void) {
        rascUI::Theme * theme = getTopLevel()->theme;
        theme->drawFrontPanel(location);
        GLfloat offset = 0.0f;
        IconButton::drawOffsetIcon(theme, location, IconDef::garrison, offset);
        value.drawMouse(location, theme, offset);
    }
    
    void ProvinceMenu::GarrisonDisplayPanel::setValue(provstat_t garrisonSize) {
        value = garrisonSize;
    }
    
    // ------------------------------------------------

    ProvinceMenu::ProvinceMenu(RascBox & box, rascUI::Theme * theme) :
        rascUI::Container(rascUI::Location(OVERALL_LOC)),
        box(box),
        currentProvince(NULL),
        onProvinceChangeToken(NULL),
        mainBackPanel(),
        titlePanel(rascUI::Location(TITLE_LOC)),
        titleLabel(rascUI::Location(TITLE_LOC), IconDef::province),
        closeButton(rascUI::Location(CLOSE_BUTTON_LOC), "Close",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) {
                    getTopLevel()->afterEvent->addFunction([this](void) {
                       hide(); 
                    });
                }
            }
        ),
        terrainDisplayPanelBack(rascUI::Location(TERRAIN_DISPLAY_OVERALL_LOC)),
        terrainDisplayPanel(rascUI::Location(TERRAIN_DISPLAY_LOC), box),
        terrainTypeLabel(rascUI::Location(TERRAIN_TYPE_LABEL_LOC)),
        ownerFrontPanel(rascUI::Location(OWNER_LABEL_LOC)),
        ownerLabel(rascUI::Location(OWNER_LABEL_LOC), "Owner:"),
        ownerPlayerPanel(rascUI::Location(OWNER_PLAYER_PANEL_LOC), box, theme),
        geoPanel(rascUI::Location(GEO_PANEL_LOC)),
        geoAreaLabel(rascUI::Location(GEO_PANEL_ROW_LOC(0))),
        geoContinentLabel(rascUI::Location(GEO_PANEL_ROW_LOC(1))),
        traitTopBackPanel(rascUI::Location(TRAIT_TOP_PANEL_LOC)),
        traitTitleToggleSeries(false),
        terrainTraitsButton(&traitTitleToggleSeries, rascUI::Location(TRAIT_TITLE_BUTTON_LOC(0)), "Traits",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                terrainTraitsScrollPane.setVisible(true);
                buildingTraitsScrollPane.setVisible(false);
            }
        ),
        buildingsButton(&traitTitleToggleSeries, rascUI::Location(TRAIT_TITLE_BUTTON_LOC(1)), "Buildings",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                terrainTraitsScrollPane.setVisible(false);
                buildingTraitsScrollPane.setVisible(true);
            }
        ),
        traitButtonToggleSeries(true,
            [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
                if (toggleButton == NULL) {
                    // Nothing selected now, so close the property display panel (with after event).
                    getTopLevel()->afterEvent->addFunction([this](void) {
                        this->box.uiGetPropertyDisplayPanel().hide(); 
                    });
                } else {
                    // Something is selected now, so get the selected property from the selected
                    // button (cast to relevant type), then show property display panel (with after event).
                    const proptype_t property = ((TraitButton *)toggleButton)->getTrait();
                    Province * const province = currentProvince;
                    getTopLevel()->afterEvent->addFunction([this, property, province](void) {
                        this->box.uiGetPropertyDisplayPanel().show(*province, property); 
                    });
                }
            }
        ),
        terrainTraitsScrollPane(theme, rascUI::Location(TRAIT_SCROLLPANE_LOC), false, true, false, true),
        terrainTraitsButtons(),
        buildingTraitsScrollPane(theme, rascUI::Location(TRAIT_SCROLLPANE_LOC), false, true, false, true),
        buildingTraitsButtons(),
        statPanel(rascUI::Location(STAT_PANEL_LOC)),
        statLabel0StatTitle(rascUI::Location(STAT_PANEL_ROW_LOC(0, false))),
        statLabel0Contents0(rascUI::Location(STAT_PANEL_ROW_LOC(1, false)), IconDef::provinceValue),
        statLabel0Contents1(rascUI::Location(STAT_PANEL_ROW_LOC(2, false)), IconDef::manpower),
        statLabel0Contents2(rascUI::Location(STAT_PANEL_ROW_LOC(3, false)), IconDef::currency),
        statLabel1StatTitle(rascUI::Location(STAT_PANEL_ROW_LOC(4, true))),
        statLabel1Contents0(rascUI::Location(STAT_PANEL_ROW_LOC(5, true)), IconDef::manpower),
        statLabel1Contents1(rascUI::Location(STAT_PANEL_ROW_LOC(6, true)), IconDef::currency),
        statLabel1Contents2(rascUI::Location(STAT_PANEL_ROW_LOC(7, true)), IconDef::land),
        advancedStatsSeries(true,
            [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
                getTopLevel()->afterEvent->addFunction([this](void) {
                    updateProvinceStats();
                });
            }
        ),
        advancedStatsButton(&advancedStatsSeries, rascUI::Location(ADVANCED_STATS_BUTTON_LOC), "Adv"),
        bottomBackPanel(rascUI::Location(BOTTOM_BACK_PANEL_LOC)),
        landFrontPanel(rascUI::Location(BOTTOM_PANEL_CONTENTS_LOC(0))),
        landLabel(rascUI::Location(BOTTOM_PANEL_CONTENTS_LOC(0)), IconDef::land),
        garrisonDisplayPanel(rascUI::Location(BOTTOM_PANEL_CONTENTS_LOC(1))),
        addGarrisonButtons(),
        bottomAbsorbButton(rascUI::Location(BOTTOM_PANEL_CONTENTS_LOC(5)), "Absorb",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) {
                    currentProvince->armyControl.absorbAllArmiesIntoGarrison(CheckedUpdate::clientAuto(this->box.netIntf));
                }
            }
        ),

        /*addMountainButton(rascUI::Location(BOTTOM_BUTTON_POS(0)), "+ Mount",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) currentProvince->properties.addTrait(MiscUpdate::clientAuto((simplenetwork::NetworkInterface *)this->box.netIntf), Trait::mountainous);
            }
        ),
        removeMountainButton(rascUI::Location(BOTTOM_BUTTON_POS(1)), "- Mount",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) currentProvince->properties.removeTrait(MiscUpdate::clientAuto((simplenetwork::NetworkInterface *)this->box.netIntf), Trait::mountainous);
            }
        ),
        addHillButton(rascUI::Location(BOTTOM_BUTTON_POS(2)), "+ Hill",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) currentProvince->properties.addTrait(MiscUpdate::clientAuto((simplenetwork::NetworkInterface *)this->box.netIntf), Trait::hilly);
            }
        ),
        removeHillButton(rascUI::Location(BOTTOM_BUTTON_POS(3)), "- Hill",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) currentProvince->properties.removeTrait(MiscUpdate::clientAuto((simplenetwork::NetworkInterface *)this->box.netIntf), Trait::hilly);
            }
        ),
        addBogButton(rascUI::Location(BOTTOM_BUTTON_POS(4)), "+ Bog",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) currentProvince->properties.addTrait(MiscUpdate::clientAuto((simplenetwork::NetworkInterface *)this->box.netIntf), Trait::boggy);
            }
        ),
        removeBogButton(rascUI::Location(BOTTOM_BUTTON_POS(5)), "- Bog",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) currentProvince->properties.removeTrait(MiscUpdate::clientAuto((simplenetwork::NetworkInterface *)this->box.netIntf), Trait::boggy);
            }
        )*/
        
        // If there is a general stat update during the time that we are open, update the province stats.
        // We cannot solely rely on the onProvinceUpdate mechanism, because that causes us to
        // update our UI during a larger update. So things happen like showing the previous colour of a
        // player during a colour change.
        afterUpdateToken(box.common.statSystem->addAfterUpdateFunction([this](void) {
            if (isVisible()) {
                updateProvinceStats();
            }
        })) {

        // Create buttons to add garrisons.
        for (unsigned int i = 0; i < ADD_GARRISON_COUNT; i++) {
            addGarrisonButtons.emplace(rascUI::Location(BOTTOM_PANEL_CONTENTS_LOC(i + 2)), std::string("+")+std::to_string(i + 1)+'k',
                [this, i](GLfloat viewX, GLfloat viewY, int button) {
                    if (button == getMb()) {
                        // When add garrison button is pressed, request construction of appropriate amount of garrison.
                        currentProvince->properties.requestConstructBuildings(
                            CheckedUpdate::clientAuto(this->box.netIntf),
                            Properties::Types::garrison, (i + 1) * 1000);
                    }
                }
            );
        }

        add(&mainBackPanel);
        add(&titlePanel);
        add(&titleLabel);
        add(&closeButton);
        add(&terrainDisplayPanelBack);
        add(&terrainDisplayPanel);
        add(&terrainTypeLabel);
        add(&ownerFrontPanel);
        add(&ownerLabel);
        add(&ownerPlayerPanel);
        add(&geoPanel);
        add(&geoAreaLabel);
        add(&geoContinentLabel);
        add(&statPanel);
        add(&advancedStatsButton);
        
        add(&statLabel0StatTitle);
        add(&statLabel0Contents0);
        add(&statLabel0Contents1);
        add(&statLabel0Contents2);
        add(&statLabel1StatTitle);
        add(&statLabel1Contents0);
        add(&statLabel1Contents1);
        add(&statLabel1Contents2);
        
        add(&terrainTraitsScrollPane); // ADD SCROLLPANES BEFORE TOP AND BOTTOM PANELS.
        add(&buildingTraitsScrollPane);

        add(&traitTopBackPanel);
        add(&terrainTraitsButton);
        add(&buildingsButton);

        add(&bottomBackPanel);
        add(&landFrontPanel);
        add(&landLabel);
        add(&garrisonDisplayPanel);
        for (rascUI::Button & button : addGarrisonButtons) add(&button);
        add(&bottomAbsorbButton);
        /*add(&addMountainButton);
        add(&removeMountainButton);
        add(&addHillButton);
        add(&removeHillButton);
        add(&addBogButton);
        add(&removeBogButton);*/

        // Make sure the terrain traits are shown by default, and the building traits are hidden by default.
        // Also make sure the terrain traits button is selected by default.
        traitTitleToggleSeries.setSelectedButton(&terrainTraitsButton);
        buildingTraitsScrollPane.setVisible(false);

        // Make sure the province menu is invisible by default.
        setVisible(false);
    }

    ProvinceMenu::~ProvinceMenu(void) {
        box.common.statSystem->removeAfterUpdateFunction(afterUpdateToken);
        
        // Remove our on change function from current province (if existing), by token.
        if (currentProvince != NULL) {
            currentProvince->removeOnProvinceChangeFunc(onProvinceChangeToken);
        }
    }

    void ProvinceMenu::populateScrollPane(rascUI::ScrollPane & scrollPane, std::list<TraitButton> & traitButtonList, const std::vector<std::pair<proptype_t, provstat_t>> & traits) {
        // Synchronise the number of trait buttons with the number of traits.
        if (traits.size() < traitButtonList.size()) {
            for (size_t sizeDiff = traitButtonList.size() - traits.size(); sizeDiff > 0; sizeDiff--) {
                TraitButton * panel = &traitButtonList.back();
                scrollPane.contents.remove(panel);
                traitButtonList.pop_back();
            }
        } else if (traitButtonList.size() < traits.size()) {
            for (size_t sizeDiff = traits.size() - traitButtonList.size(); sizeDiff > 0; sizeDiff--) {
                traitButtonList.emplace_back(getTopLevel()->theme, &traitButtonToggleSeries);
                scrollPane.contents.add(&traitButtonList.back());
            }
        }

        // Set all trait buttons to the correct name.
        std::list<TraitButton>::iterator traitPanelIter = traitButtonList.begin();
        for (const std::pair<proptype_t, provstat_t> & pair : traits) {
            (traitPanelIter++)->setTrait(pair.first, pair.second);
        }
    }

    void ProvinceMenu::updateProvinceStats(void) {
        PlayerData * data = box.netIntf->getData();
        provstat_t manpower = data->getTotalManpower(),
                   currency = data->getTotalCurrency();

        // Set the title to the province name.
        titleLabel.setText(currentProvince->getName());

        // Set the area and continent names. Do a lookup of the area and continent in the gamemap.
        Area & area = box.map->getAreaRaw(currentProvince->getAreaId());
        Continent & continent = box.map->getContinent(area.getContinentId());
        geoAreaLabel.setText(std::string("Area: ") + area.getName());
        geoContinentLabel.setText(std::string("Continent: ") + continent.getName());

        // Set the terrain type label. Every province subclass has a method which returns the name of this.
        terrainTypeLabel.setText(std::string("Terrain type: ") + currentProvince->getProvinceTypeName());

        {
            std::vector<std::pair<proptype_t, provstat_t>> provinceTraits;
            // Populate the scroll pane for terrain traits.
            currentProvince->properties.getSortedTraitsByDisplayMode(provinceTraits, TraitDef::DisplayMode::terrainTrait);
            populateScrollPane(terrainTraitsScrollPane, terrainTraitsButtons, provinceTraits);
            // Populate the scroll pane for building traits.
            currentProvince->properties.getSortedTraitsByDisplayMode(provinceTraits, TraitDef::DisplayMode::building);
            populateScrollPane(buildingTraitsScrollPane, buildingTraitsButtons, provinceTraits);
        }
        
        {
            // If properties display panel is currently open:
            const proptype_t currentProperty = box.uiGetPropertyDisplayPanel().getCurrentProperty();
            if (currentProperty != Properties::Types::INVALID) {
                
                // Search all buttons for the current property, if we find it set selected button.
                bool foundProperty = false;
                for (TraitButton & button : terrainTraitsButtons) {
                    if (button.getTrait() == currentProperty) {
                        traitButtonToggleSeries.setSelectedButton(&button, false, false);
                        foundProperty = true;
                        break;
                    }
                }
                if (!foundProperty) {
                    for (TraitButton & button : buildingTraitsButtons) {
                        if (button.getTrait() == currentProperty) {
                            traitButtonToggleSeries.setSelectedButton(&button, false, false);
                            foundProperty = true;
                            break;
                        }
                    }
                }
                
                // If it can't be found, clear toggle button selection and close property display panel.
                if (!foundProperty) {
                    traitButtonToggleSeries.setSelectedId(rascUI::ToggleButtonSeries::NO_TOGGLE_BUTTON, false, false);
                    box.uiGetPropertyDisplayPanel().hide();
                }
            }
        }

        // Update the owner panel.
        ownerPlayerPanel.setPlayerById(currentProvince->getProvinceOwner());

        // Update the terrain display panel.
        terrainDisplayPanel.updateFromProvince(currentProvince);

        // Update the province value, manpower and currency.
        ProvinceProperties & properties = currentProvince->properties;

        // Buffer for text formatting. Set this to smallest size allowed by format macros and static asserts.
        char textBuffer[21];
        
        // If in advanced stats mode:
        if (advancedStatsButton.isToggleComponentSelected()) {
            
            // Number of digits and buffer size for each advanced stat number. Convert this to a space
            // padded printf format where 3 numbers are displayed next to each other. Note that last
            // number needs no space padding, as it is at the end.
            #define ADV_DIGITS     6
            #define ADV_SIZE       (ADV_DIGITS + 1)
            #define ADV_FORMAT     "%-" MUTIL_STRINGIFY(ADV_DIGITS) "s"
            #define ADV_FORMAT_ALL ADV_FORMAT " " ADV_FORMAT " " "%s"
            
            static_assert(sizeof(textBuffer) >= ADV_DIGITS * 3 + 3, "Text buffer must be big enough");
            
            // Intermediary buffers: these are used by number format methods, then concatenated together
            // with space padding using sprintf.
            char inBuff[ADV_SIZE], outBuf[ADV_SIZE], netBuf[ADV_SIZE];
            
            // Show regular stats.
            statLabel0StatTitle.setText("in, out, net:");
            
            NumberFormat::provinceValue<ADV_SIZE>(textBuffer, properties[ProvStatIds::provinceValue], false);
            statLabel0Contents0.setText(textBuffer);
            
            NumberFormat::manpower<ADV_SIZE>(inBuff, properties[ProvStatIds::manpowerIncome], false);
            NumberFormat::manpower<ADV_SIZE>(outBuf, properties[ProvStatIds::manpowerExpenditure], false);
            NumberFormat::manpower<ADV_SIZE>(netBuf, properties[ProvStatIds::manpowerIncome] - properties[ProvStatIds::manpowerExpenditure], false);
            std::sprintf(textBuffer, ADV_FORMAT_ALL, inBuff, outBuf, netBuf);
            statLabel0Contents1.setText(textBuffer);
            
            NumberFormat::currency<ADV_SIZE>(inBuff, properties[ProvStatIds::currencyIncome], false);
            NumberFormat::currency<ADV_SIZE>(outBuf, properties[ProvStatIds::currencyExpenditure], false);
            NumberFormat::currency<ADV_SIZE>(netBuf, properties[ProvStatIds::currencyIncome] - properties[ProvStatIds::currencyExpenditure], false);
            std::sprintf(textBuffer, ADV_FORMAT_ALL, inBuff, outBuf, netBuf);
            statLabel0Contents2.setText(textBuffer);

            // Show output modifier stats
            statLabel1StatTitle.setText("Output: (in, out, net)");
            
            NumberFormat::percentage<ADV_SIZE>(inBuff, properties[ProvStatIds::manpowerIncome].getAccumulatedStatModifiers().getPercentage(), true);
            NumberFormat::percentage<ADV_SIZE>(outBuf, properties[ProvStatIds::manpowerExpenditure].getAccumulatedStatModifiers().getPercentage(), true);
            NumberFormat::percentage<ADV_SIZE>(netBuf, properties[ProvStatIds::manpowerIncome].getAccumulatedStatModifiers().getPercentage() - properties[ProvStatIds::manpowerExpenditure].getAccumulatedStatModifiers().getPercentage(), true);
            std::sprintf(textBuffer, ADV_FORMAT_ALL, inBuff, outBuf, netBuf);
            statLabel1Contents0.setText(textBuffer);
            
            NumberFormat::percentage<ADV_SIZE>(inBuff, properties[ProvStatIds::currencyIncome].getAccumulatedStatModifiers().getPercentage(), true);
            NumberFormat::percentage<ADV_SIZE>(outBuf, properties[ProvStatIds::currencyExpenditure].getAccumulatedStatModifiers().getPercentage(), true);
            NumberFormat::percentage<ADV_SIZE>(netBuf, properties[ProvStatIds::currencyIncome].getAccumulatedStatModifiers().getPercentage() - properties[ProvStatIds::currencyExpenditure].getAccumulatedStatModifiers().getPercentage(), true);
            std::sprintf(textBuffer, ADV_FORMAT_ALL, inBuff, outBuf, netBuf);
            statLabel1Contents1.setText(textBuffer);
            
            NumberFormat::percentage<ADV_SIZE>(inBuff, properties[ProvStatIds::landAvailable].getAccumulatedStatModifiers().getPercentage(), true);
            NumberFormat::percentage<ADV_SIZE>(outBuf, properties[ProvStatIds::landUsed].getAccumulatedStatModifiers().getPercentage(), true);
            NumberFormat::percentage<ADV_SIZE>(netBuf, properties[ProvStatIds::landAvailable].getAccumulatedStatModifiers().getPercentage() - properties[ProvStatIds::landUsed].getAccumulatedStatModifiers().getPercentage(), true);
            std::sprintf(textBuffer, ADV_FORMAT_ALL, inBuff, outBuf, netBuf);
            statLabel1Contents2.setText(textBuffer);

        // If in basic stats mode:
        } else {
            
            // Show regular stats.
            statLabel0StatTitle.setText("Stats (net):");
            
            NUMBER_FORMAT_FILL(textBuffer, provinceValue, 7, "Prov value:   ",
                properties[ProvStatIds::provinceValue], false)
            statLabel0Contents0.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, manpower, 7, "Manpower:     ",
                properties[ProvStatIds::manpowerIncome] - properties[ProvStatIds::manpowerExpenditure], false)
            statLabel0Contents1.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, currency, 7, "Currency:     ",
                properties[ProvStatIds::currencyIncome] - properties[ProvStatIds::currencyExpenditure], false)
            statLabel0Contents2.setText(textBuffer);

            // Show output modifier stats.
            statLabel1StatTitle.setText("Output modifiers:");
            
            NUMBER_FORMAT_FILL(textBuffer, percentage, 7, "Manpower:     ",
                properties[ProvStatIds::manpowerIncome].getAccumulatedStatModifiers().getPercentage() -
                properties[ProvStatIds::manpowerExpenditure].getAccumulatedStatModifiers().getPercentage(), true)
            statLabel1Contents0.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, percentage, 7, "Currency:     ",
                properties[ProvStatIds::currencyIncome].getAccumulatedStatModifiers().getPercentage() -
                properties[ProvStatIds::currencyExpenditure].getAccumulatedStatModifiers().getPercentage(), true)
            statLabel1Contents1.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, percentage, 7, "Land:         ",
                properties[ProvStatIds::landAvailable].getAccumulatedStatModifiers().getPercentage() -
                properties[ProvStatIds::landUsed].getAccumulatedStatModifiers().getPercentage(), true)
            statLabel1Contents2.setText(textBuffer);
        }

        // Show land display.
        NUMBER_FORMAT_FILL(textBuffer, landUseAvailable, 12,
            "Land: ", properties[ProvStatIds::landUsed], properties[ProvStatIds::landAvailable])
        landLabel.setText(textBuffer);

        // Show garrison display.
        garrisonDisplayPanel.setValue(GET_GARRISON_SIZE(properties));


        // Work out whether we have a fort, and whether we can place new armies into the garrison.
        bool fortOwnedByUs = currentProvince->getProvinceOwner() == data->getId() &&
                             properties.getPropertySet().hasPropertiesOfType(Properties::Types::fort);
        bool canPlaceNewArmies = fortOwnedByUs && !currentProvince->battleControl->isBattleOngoing() && currency >= 0;

        // To place units in the garrison, we also require enough manpower, and some currency.
        for (unsigned int i = 0; i < ADD_GARRISON_COUNT; i++) {
            addGarrisonButtons[i].setActive(canPlaceNewArmies && manpower >= 1000 * (i + 1) * AreasStat::MANPOWER_PER_UNIT);
        }

        // We can absorb into the garrison, if there is a fort owned by us, and we have
        // some armies in the province.
        bottomAbsorbButton.setActive(fortOwnedByUs && currentProvince->armyControl.countUnitsOwnedBy(box.netIntf->getData()->getId()) > 0);
        
        // Also update property display panel (it checks internally if it is open).
        box.uiGetPropertyDisplayPanel().updateStats();
    }

    void ProvinceMenu::show(Province * province) {
        if (province == currentProvince) return;
        if (currentProvince != NULL) {
            currentProvince->removeOnProvinceChangeFunc(onProvinceChangeToken);
        }
        currentProvince = (ClientProvince *)province;
        // Ensure that when *this* province gets an update, a general stat update definitely happens.
        onProvinceChangeToken = currentProvince->addOnProvinceChangeFunc([this](void) {
            box.common.statSystem->forceUpdate(); // Note that this is safe to call from the network side due to mutex design.
        });
        updateProvinceStats();
        setVisible(true);
    }

    void ProvinceMenu::hide(void) {
        currentProvince->removeOnProvinceChangeFunc(onProvinceChangeToken);
        currentProvince = NULL;
        setVisible(false);
        
        // Also hide property display panel, also unselect any trait buttons (since
        // property display panel won't be there when we're shown again).
        box.uiGetPropertyDisplayPanel().hide();
        traitButtonToggleSeries.setSelectedId(rascUI::ToggleButtonSeries::NO_TOGGLE_BUTTON, false, false);
    }
    
    GLfloat ProvinceMenu::getTotalWidth(rascUI::Theme * theme) {
        return OVERALL_WIDTH + OVERALL_R_BORDER;
    }
    
    GLfloat ProvinceMenu::getBottomBorder(rascUI::Theme * theme) {
        return OVERALL_B_BORDER;
    }
    
    void ProvinceMenu::unselectPropertyToggleButtons(void) {
        traitButtonToggleSeries.setSelectedId(rascUI::ToggleButtonSeries::NO_TOGGLE_BUTTON, false, false);
    }
}

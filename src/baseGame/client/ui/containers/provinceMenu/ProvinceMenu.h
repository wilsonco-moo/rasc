/*
 * ProvinceMenu.h
 *
 *  Created on: 18 Sep 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_PROVINCEMENU_PROVINCEMENU_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_PROVINCEMENU_PROVINCEMENU_H_

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/util/EmplaceArray.h>
#include <rascUI/base/Container.h>
#include <wool/texture/Texture.h>
#include <wool/misc/RGBA.h>
#include <string>
#include <list>

#include "../../customComponents/zeroPrefixedNumber/ZeroPrefixedNumberProvstat.h"
#include "../../../../common/gameMap/properties/PropertyDef.h"
#include "../../customComponents/icon/IconToggleButton.h"
#include "../../customComponents/icon/IconLabel.h"
#include "../../customComponents/PlayerPanel.h"

namespace rascUI {
    class Theme;
}

namespace rasc {
    class RascBox;
    class Province;
    class ClientProvince;
    class GlobalProperties;

    /**
     * ProvinceMenu is the menu that shows up when the user clicks on a province.
     *
     * Note that the province menu is NOT by definition a popup menu, nor does it inherit from
     * FloatingPanel, or have a FadePanel behind it. When the province menu appears, it does NOT
     * lock selection, and it allows the user to do other things also while it is open. While it
     * is open, it essentially just becomes yet another piece of UI, like the minimap. There is
     * nothing (other than mild inconvenience), that would stop the user from having this menu
     * open all the time.
     * The province menu can appear at the same time as the bill menu and escape menu, and should
     * be shown behind these other *real* popup menus.
     */
    class ProvinceMenu : public rascUI::Container {
    private:
        // ---------------- Custom components ---------------

        /**
         * These show the traits in the scroll pane.
         */
        class TraitButton : public IconToggleButton {
        private:
            proptype_t currentTrait;
        public:
            TraitButton(rascUI::Theme * theme, rascUI::ToggleButtonSeries * series);
            virtual ~TraitButton(void);
            void setTrait(proptype_t trait, provstat_t count);
            proptype_t getTrait(void) const;
        };

        // ------------------------------------------------

        /**
         * This shows the image view of each province, using the textures
         * associated with the province's traits and types.
         */
        class TerrainDisplayPanel : public rascUI::Component {
        private:
            RascBox & box;
            size_t textureSizeBytes; // For convenience.
            wool::Texture texture;
            void * token;
            unsigned char * nextTexture;
        public:
            TerrainDisplayPanel(const rascUI::Location & location, RascBox & box);
            virtual ~TerrainDisplayPanel(void);
        protected:
            virtual void onDraw(void) override;
            virtual bool shouldRespondToKeyboard(void) const override;
        public:
            void updateFromProvince(Province * province);
        };
        
        // ------------------------------------------------
        
        /**
         * This provides a simple bit of custom drawing for
         * displaying the garrison size.
         */
        class GarrisonDisplayPanel : public rascUI::FrontPanel {
        private:
            ZeroPrefixedNumberProvstat value;
        public:
            GarrisonDisplayPanel(const rascUI::Location & location);
            virtual ~GarrisonDisplayPanel(void);
        protected:
            virtual void onDraw(void) override;
        public:
            void setValue(provstat_t garrisonSize);
        };
        
        // ------------------------------------------------

        RascBox & box;

        // The current province that we are attached to, and an on province change token
        // (see onProvinceChange in ClientProvince).
        ClientProvince * currentProvince;
        void * onProvinceChangeToken;

        // This covers the entire area of the province menu.
        rascUI::BackPanel mainBackPanel;

        // This is where the province name is shown.
        rascUI::FrontPanel titlePanel;
        IconLabel titleLabel;

        // This closes (hides) the province menu.
        rascUI::Button closeButton;

        // This component visually displays, (as a picture), the terrain type and traits.
        rascUI::FrontPanel terrainDisplayPanelBack;
        TerrainDisplayPanel terrainDisplayPanel;
        rascUI::Label terrainTypeLabel;

        // This shows the owner of the province.
        rascUI::FrontPanel ownerFrontPanel;
        rascUI::Label ownerLabel;
        PlayerPanel ownerPlayerPanel;

        // This shows information about the area and continent in which the province is located.
        rascUI::FrontPanel geoPanel;
        rascUI::Label geoAreaLabel,
                      geoContinentLabel;

        // This shows the terrain traits.
        rascUI::BackPanel traitTopBackPanel;
        // The trait title buttons.
        rascUI::ToggleButtonSeries traitTitleToggleSeries;
        rascUI::ToggleButton terrainTraitsButton, buildingsButton;

        // For selecting traits/buildings.
        rascUI::ToggleButtonSeries traitButtonToggleSeries;
        
        // For displaying terrain traits.
        rascUI::ScrollPane terrainTraitsScrollPane;
        std::list<TraitButton> terrainTraitsButtons;

        // For displaying building traits.
        rascUI::ScrollPane buildingTraitsScrollPane;
        std::list<TraitButton> buildingTraitsButtons;

        // These show all of the stats for a province, like manpower and currency.
        rascUI::FrontPanel statPanel;
        rascUI::Label statLabel0StatTitle;
        IconLabel statLabel0Contents0, statLabel0Contents1, statLabel0Contents2;
        rascUI::Label statLabel1StatTitle;
        IconLabel statLabel1Contents0, statLabel1Contents1, statLabel1Contents2;
        
        // Button to enable/disable advanced stats mode.
        rascUI::ToggleButtonSeries advancedStatsSeries;
        rascUI::ToggleButton advancedStatsButton;

        rascUI::BackPanel bottomBackPanel;

        rascUI::FrontPanel landFrontPanel;
        IconLabel landLabel;

        GarrisonDisplayPanel garrisonDisplayPanel;
        constexpr static unsigned int ADD_GARRISON_COUNT = 3;
        rascUI::EmplaceArray<rascUI::Button, ADD_GARRISON_COUNT> addGarrisonButtons;
        rascUI::Button bottomAbsorbButton;
        
        void * afterUpdateToken;
        
        /*rascUI::Button addMountainButton,
                       removeMountainButton,
                       addHillButton,
                       removeHillButton,
                       addBogButton,
                       removeBogButton;*/

    public:
        ProvinceMenu(RascBox & box, rascUI::Theme * theme);
        virtual ~ProvinceMenu(void);

    private:
        /**
         * Updates all of our displayed stats and other data from the current
         * state of the province. This must be called each time our target province
         * gets some kind of update.
         * This can be called from the network side, (e.g: in a misc update), outside
         * UI drawing or events, or in a UI function queue.
         */
        void updateProvinceStats(void);

        /**
         * Populates the specified scrollpane, using the provided list, with trait buttons
         * representing the provided traits.
         */
        void populateScrollPane(rascUI::ScrollPane & scrollPane, std::list<TraitButton> & traitButtonList, const std::vector<std::pair<proptype_t, provstat_t>> & traits);

    public:
        /**
         * Shows the province menu, and attaches it to the specified province.
         * Does not internally use a function queue, so must be called from the
         * network side (e.g: in a misc update), outside UI drawing or events,
         * or in a UI function queue.
         */
        void show(Province * province);

        /**
         * Hides the province menu. This detaches it from the province.
         * Does not internally use a function queue, so must be called from the
         * network side (e.g: in a misc update), outside UI drawing or events,
         * or in a UI function queue.
         */
        void hide(void);
        
        /**
         * Gets total province menu width including the right-border,
         * and gets the bottom border.
         */
        static GLfloat getTotalWidth(rascUI::Theme * theme);
        static GLfloat getBottomBorder(rascUI::Theme * theme);
        
        /**
         * Unselects all property toggle buttons, used by property display panel
         * when it closes itself.
         * Doesn't use function queue internally, so can't be called from UI events.
         */
        void unselectPropertyToggleButtons(void);
    };
}

#endif

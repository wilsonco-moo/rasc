/*
 * PropertyDisplayPanel.h
 *
 *  Created on: 29 Jul 2023
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_PROVINCEMENU_PROPERTYDISPLAYPANEL_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_PROVINCEMENU_PROPERTYDISPLAYPANEL_H_

#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/base/Container.h>

#include "../../../../common/gameMap/properties/PropertyDef.h"
#include "../../customComponents/FancyTextDisplay.h"
#include "../../customComponents/icon/IconButton.h"
#include "../../customComponents/icon/IconLabel.h"

namespace rascUI {
    class Theme;
}

namespace rasc {
    class Province;
    class RascBox;

    /**
     * Extension to the province menu which shows the long description of a property,
     * (typically a building or trait).
     * This exists next to the province menu, and is unhidden when a building/trait
     * is clicked on.
     */
    class PropertyDisplayPanel final : public rascUI::Container {
    private:
        RascBox & box;
        
        Province * currentProvince;
        proptype_t currentProperty;
        
        rascUI::BackPanel backPanel, topBackPanel;
        rascUI::FrontPanel topFrontPanel;
        IconLabel topLabel;
        
        rascUI::Button closeButton;
        
        FancyTextDisplay fancyTextDisplay;
        
        rascUI::BackPanel bottomBackPanel;
        IconButton demolishButton;
        rascUI::FrontPanel bottomFrontPanel;

    public:
        PropertyDisplayPanel(RascBox & box, rascUI::Theme * theme);
        virtual ~PropertyDisplayPanel(void);

    private:
        // Gets whether the demolish button should be active (i.e: whether
        // it is possible to demolish).
        bool canDemolish(void) const;
        
    public:
        /**
         * Show/hide property display panel, for use by province menu.
         * Doesn't use function queue internally, so can't be called from UI events.
         */
        void show(Province & province, proptype_t property);
        void hide(void);
        
        /**
         * Get currently selected property. Will be invalid unless we're open.
         */
        proptype_t getCurrentProperty(void) const;
        
        /**
         * Called by province menu on stat update. If we're open, updates whether
         * demolish button is open.
         */
        void updateStats(void);
    };
}

#endif

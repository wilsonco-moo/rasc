/*
 * PropertyDisplayPanel.cpp
 *
 *  Created on: 29 Jul 2023
 *      Author: wilson
 */

#include "PropertyDisplayPanel.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <sstream>
#include <cstddef>

#include "../../../../common/gameMap/mapElement/types/Province.h"
#include "../../../../common/gameMap/properties/trait/Traits.h"
#include "../../../../common/gameMap/properties/Properties.h"
#include "../../../../common/util/definitions/IconDef.h"
#include "../../../../common/playerData/PlayerData.h"
#include "../../../RascClientNetIntf.h"
#include "../buildMenu/BuildMenu.h"
#include "../../../RascBox.h"
#include "ProvinceMenu.h"

#define LAYOUT_THEME theme

#define OVERALL_WIDTH (800 - RascBox::getProvinceMenuTotalWidth(theme) - UI_BORDER)
#define OVERALL_HEIGHT 300

#define OVERALL_LOC                                             \
    PIN_B(OVERALL_HEIGHT,                                       \
     PIN_R(OVERALL_WIDTH,                                       \
      SUB_MARGIN_B(RascBox::getProvinceMenuBottomBorder(theme), \
       SUB_MARGIN_R(RascBox::getProvinceMenuTotalWidth(theme),  \
        GEN_FILL                                                \
    ))))

#define TOP_BACKPANEL_LOC       \
    PIN_T(COUNT_OUTBORDER_Y(1), \
     GEN_FILL                   \
    )

#define TOP_AREA_LOC        \
    SUB_BORDER_L(           \
     SUB_BORDER_Y(          \
      TOP_BACKPANEL_LOC     \
    ))

#define CLOSE_BUTTON_WIDTH 64
#define CLOSE_BUTTON_LOC      \
    PIN_R(CLOSE_BUTTON_WIDTH, \
     TOP_AREA_LOC             \
    )

#define TOP_FRONTPANEL_LOC                 \
    SUB_BORDERMARGIN_R(CLOSE_BUTTON_WIDTH, \
     TOP_AREA_LOC                          \
    )

#define FANCY_TEXT_LOC                 \
    SUB_MARGIN_Y(COUNT_OUTBORDER_Y(1), \
     GEN_FILL                          \
    )

#define BOTTOM_BACKPANEL_LOC    \
    PIN_B(COUNT_OUTBORDER_Y(1), \
     GEN_FILL                   \
    )

#define BOTTOM_AREA_LOC       \
    SUB_BORDER_L(             \
     SUB_BORDER_Y(            \
      BOTTOM_BACKPANEL_LOC    \
    ))

#define DEMOLISH_BUTTON_WIDTH 128
#define DEMOLISH_BUTTON_LOC      \
    PIN_L(DEMOLISH_BUTTON_WIDTH, \
     BOTTOM_AREA_LOC             \
    )

#define BOTTOM_FRONTPANEL_LOC                 \
    SUB_BORDERMARGIN_L(DEMOLISH_BUTTON_WIDTH, \
     BOTTOM_AREA_LOC                          \
    )

namespace rasc {

    PropertyDisplayPanel::PropertyDisplayPanel(RascBox & box, rascUI::Theme * theme) :
        rascUI::Container(rascUI::Location(OVERALL_LOC)),
        box(box),
        currentProvince(NULL),
        currentProperty(Properties::Types::INVALID),
        topBackPanel(rascUI::Location(TOP_BACKPANEL_LOC)),
        topFrontPanel(rascUI::Location(TOP_FRONTPANEL_LOC)),
        topLabel(rascUI::Location(TOP_FRONTPANEL_LOC)),
        closeButton(rascUI::Location(CLOSE_BUTTON_LOC), "Close",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) {
                    getTopLevel()->afterEvent->addFunction([this](void) {
                        hide();
                        // Note: Also unselect property toggle buttons in province
                        // menu now we're closing, or they'll be stuck open.
                        this->box.uiGetProvinceMenu().unselectPropertyToggleButtons();
                    });
                }
            }
        ),
        fancyTextDisplay(rascUI::Location(FANCY_TEXT_LOC), theme, "", false, false, false),
        bottomBackPanel(rascUI::Location(BOTTOM_BACKPANEL_LOC)),
        demolishButton(rascUI::Location(DEMOLISH_BUTTON_LOC), IconDef::demolish, "Demolish",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) {
                    BuildMenu::requestDemolishBuilding(CheckedUpdate::clientAuto(this->box.netIntf), *currentProvince, currentProperty);
                }
            }
        ),
        bottomFrontPanel(rascUI::Location(BOTTOM_FRONTPANEL_LOC)) {
        
        add(&backPanel);
        add(&fancyTextDisplay);
        add(&topBackPanel);
        add(&topFrontPanel);
        add(&topLabel);
        add(&closeButton);
        add(&bottomBackPanel);
        add(&demolishButton);
        add(&bottomFrontPanel);
        
        // Make sure property display panel is invisible by default.
        setVisible(false);
    }
    
    PropertyDisplayPanel::~PropertyDisplayPanel(void) {
    }
    
    bool PropertyDisplayPanel::canDemolish(void) const {
        // Don't own the province or not a trait.
        PlayerData & playerData = *box.netIntf->getData();
        if (currentProvince->getProvinceOwner() != playerData.getId() ||
            Properties::getBaseType(currentProperty) != Properties::BaseTypes::trait) {
            return false;
        }
        
        // Get demolish cost (ignore errors as count is -1).
        const PlayerStatArr demolishCost = Traits::getTraitBuildingCost(NULL, currentProperty, -1);
        
        // Can't afford to demolish, or can't be demolished.
        if (!playerData.canAffordBuilding(demolishCost) ||
            !Traits::canTraitBeBuilt(*currentProvince, currentProperty, -1)) {
            return false;
        }
        
        return true;
    }
    
    void PropertyDisplayPanel::show(Province & province, proptype_t property) {
        // Update current province.
        currentProvince = &province;
        
        // Nothing to do if already showing that property.
        if (property == currentProperty) {
            return;
        }
        
        // Update current property.
        currentProperty = property;
        
        // Update text display to show long description of the property.
        if (Properties::getBaseType(property) == Properties::BaseTypes::trait) {
            std::stringstream str;
            Traits::getTraitLongDescription(str, property);
            fancyTextDisplay.setText(str.str());
            topLabel.setIconId(Traits::propertyGetIconId(property));
            topLabel.setText(Traits::getFriendlyName(property));
        } else {
            fancyTextDisplay.setText("Add property support in province menu, and for long descriptions!");
        }
        
        // Update whether demolish button should be active.
        demolishButton.setActive(canDemolish());
        
        // Make sure we're visible now.
        setVisible(true);
    }
    
    void PropertyDisplayPanel::hide(void) {
        // Make us invisible, reset current property to invalid.
        setVisible(false);
        currentProperty = Properties::Types::INVALID;
    }
    
    proptype_t PropertyDisplayPanel::getCurrentProperty(void) const {
        return currentProperty;
    }
    
    void PropertyDisplayPanel::updateStats(void) {
        // Only update demolish button if we're actually open.
        if (currentProvince != NULL) {
            demolishButton.setActive(canDemolish());
        }
    }
}

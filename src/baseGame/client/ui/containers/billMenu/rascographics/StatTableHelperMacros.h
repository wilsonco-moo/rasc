/*
 * StatTableHelperMacros.h
 *
 *  Created on: 12 Apr 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_RASCOGRAPHICS_STATTABLEHELPERMACROS_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_RASCOGRAPHICS_STATTABLEHELPERMACROS_H_

#include "../../../../../common/util/macros/MacroList.h"

/**
 * Helper macros which make assembling stat tables more convenient.
 * Each macro takes a list of column definitions, where each column
 * is the following list: (columnName, columnWidth, cellValue)
 * 
 * Note that column widths do not have to add up to one: weights are
 * worked out automatically by these macros.
 */


/**
 * Expands to an initialiser list of StatTable::Column instances, generated
 * from the list. The column weights are worked out automatically from the
 * width proportions of each column.
 */
#define RASC_STATTABLE_COLUMN_INITIALISERS(columnList) {                 \
    LIST_NESTED_EXPAND_1(columnList,                                     \
     LIST_FOR_DATA_NESTED_0(                                             \
      RASC_STATTABLE_INTERNAL_COLUMN_INITIALISER, columnList, columnList \
    ))                                                                   \
}

/**
 * Expands to an initialiser list of cell values, generated from the column list.
 */
#define RASC_STATTABLE_ROW(columnList) {                     \
    LIST_FOR(RASC_STATTABLE_INTERNAL_CELL_VALUE, columnList) \
}


// ------------------------------ Internal macros ----------------------------------

// Gets initialiser for column.
#define RASC_STATTABLE_INTERNAL_COLUMN_INITIALISER(column, columnList) \
    StatTable::Column(                                                 \
        RASC_STATTABLE_INTERNAL_TITLE(column),                         \
        RASC_STATTABLE_INTERNAL_COLUMN_WEIGHT(column, columnList)      \
    ),

// Gets title of column.
#define RASC_STATTABLE_INTERNAL_TITLE(column) LIST_GET(column, 0)

// Gets width of a column.
#define RASC_STATTABLE_INTERNAL_WIDTH(column) LIST_GET(column, 1)

// Gets cell value of a column (with comma appended to separate cells).
#define RASC_STATTABLE_INTERNAL_CELL_VALUE(column) LIST_GET(column, 2),

// Calculates weight of a column from its width.
#define RASC_STATTABLE_INTERNAL_COLUMN_WEIGHT(column, columnList)                      \
    (((GLfloat)(RASC_STATTABLE_INTERNAL_WIDTH(column))) /                              \
     ((GLfloat)(LIST_FOR_NESTED_1(RASC_STATTABLE_INTERNAL_WEIGHT_ADD, columnList) 0)))
#define RASC_STATTABLE_INTERNAL_WEIGHT_ADD(column) \
    (RASC_STATTABLE_INTERNAL_WIDTH(column)) +

#endif

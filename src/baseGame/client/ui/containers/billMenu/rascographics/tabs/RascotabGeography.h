/*
 * RascotabGeography.h
 *
 *  Created on: 17 Nov 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_RASCOGRAPHICS_TABS_RASCOTABGEOGRAPHY_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_RASCOGRAPHICS_TABS_RASCOTABGEOGRAPHY_H_

#include "../StatTable.h"

namespace rasc {

    class RascBox;

    /**
     * The geography Rascographics tab displays stats about the land ownership
     * of each nation.
     */
    class RascotabGeography : public StatTable {
    private:
        RascBox & box;

    public:
        RascotabGeography(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme);
        virtual ~RascotabGeography(void);

        virtual void updateStats(void) override;
    };
}

#endif

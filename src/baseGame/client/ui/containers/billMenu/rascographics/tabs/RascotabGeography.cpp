/*
 * RascotabGeography.cpp
 *
 *  Created on: 17 Nov 2019
 *      Author: wilson
 */

#include "RascotabGeography.h"

#include "../../../../../../common/playerData/PlayerDataSystem.h"
#include "../../../../../../common/stats/types/ContinentsStat.h"
#include "../../../../../../common/stats/types/ProvincesStat.h"
#include "../../../../../../common/util/numeric/NumberFormat.h"
#include "../../../../../../common/stats/types/AreasStat.h"
#include "../../../../../../common/playerData/PlayerData.h"
#include "../../../../../gameMap/ClientGameMap.h"
#include "../../../../../RascClientNetIntf.h"
#include "../StatTableHelperMacros.h"
#include "../../../../../RascBox.h"

#define COLUMNS (                                                                                                                     \
    ("Name", 15,                                                                                                                      \
     new StatTable::PlayerButtonValue(box, playerId)),                                                                                \
                                                                                                                                      \
    ("Provs", 5,                                                                                                                      \
     new StatTable::UnsignedWPercValue(8, &NumberFormat::generic<8>,                                                                  \
                                 box.common.provincesStat->getProvinceCount(playerId), box.map->getAccessibleProvinceCount())),       \
    ("Areas", 5,                                                                                                                      \
     new StatTable::UnsignedWPercValue(8, &NumberFormat::generic<8>,                                                                  \
                                  box.common.areasStat->getAreaCount(playerId), box.map->getAccessibleAreaCount())),                  \
    ("Conts", 5,                                                                                                                      \
     new StatTable::UnsignedWPercValue(8, &NumberFormat::generic<8>,                                                                  \
                                  box.common.continentsStat->getContinentCount(playerId), box.map->getAccessibleContinentCount())),   \
    ("Land used", 9,                                                                                                                  \
     new StatTable::ProvstatWPercValue(8, &NumberFormat::land<8>, false,                                                              \
                                  playerData->getTotalLandUsed(), totalLandUsed)),                                                    \
    ("Land avail", 10,                                                                                                                \
     new StatTable::ProvstatWPercValue(8, &NumberFormat::land<8>, false,                                                              \
                                  playerData->getTotalLandAvailable(), totalLandAvailable))                                           \
)

namespace rasc {

    RascotabGeography::RascotabGeography(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme) :
        StatTable(location, theme, RASC_STATTABLE_COLUMN_INITIALISERS(COLUMNS), true),
        box(box) {
    }

    RascotabGeography::~RascotabGeography(void) {
    }

    void RascotabGeography::updateStats(void) {
        // Note: It is safe to modify the component hierarchy directly here as this is called by a stat system after update.

        // Clear any existing rows.
        clearRows();

        // Add up total land available/used across the map.
        provstat_t totalLandAvailable = 0,
                   totalLandUsed = 0;
        for (const std::pair<const uint64_t, RascUpdatable *> & pair : box.common.playerDataSystem->mapChildren()) {
            PlayerData * data = (PlayerData *)pair.second;
            totalLandAvailable += data->getTotalLandAvailable();
            totalLandUsed += data->getTotalLandUsed();
        }

        // Get our player data and player id.
        PlayerData * ourPlayerData = box.netIntf->getData();
        uint64_t ourPlayerId = ourPlayerData->getId();
        
        // Add each row, set as home row if it is ourself, otherwise add.
        for (const std::pair<const uint64_t, RascUpdatable *> & pair : box.common.playerDataSystem->mapChildren()) {
            PlayerData * playerData = (PlayerData *)pair.second;
            uint64_t playerId = pair.first;
            std::initializer_list<ValueBase *> row = RASC_STATTABLE_ROW(COLUMNS);
            if (playerId == ourPlayerId) {
                setHomeRow(std::move(row));
            } else {
                addRow(std::move(row));
            }
        }
        
        // Re-sort the rows now we have added them all.
        reSort();
    }
}

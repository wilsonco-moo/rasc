/*
 * RascotabEconomy.h
 *
 *  Created on: 17 Nov 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_RASCOGRAPHICS_TABS_RASCOTABECONOMY_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_RASCOGRAPHICS_TABS_RASCOTABECONOMY_H_

#include "../StatTable.h"

namespace rasc {

    class RascBox;

    /**
     * The economy Rascographics tab displays stats about currency income, balance
     * and expenditure for each nation.
     */
    class RascotabEconomy : public StatTable {
    private:
        RascBox & box;

    public:
        RascotabEconomy(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme);
        virtual ~RascotabEconomy(void);

        virtual void updateStats(void) override;
    };
}

#endif

/*
 * RascotabArmies.cpp
 *
 *  Created on: 17 Nov 2019
 *      Author: wilson
 */

#include "RascotabArmies.h"

#include "../../../../../../common/stats/types/UnitsDeployedStat.h"
#include "../../../../../../common/playerData/PlayerDataSystem.h"
#include "../../../../../../common/util/numeric/NumberFormat.h"
#include "../../../../../../common/playerData/PlayerData.h"
#include "../../../../../RascClientNetIntf.h"
#include "../StatTableHelperMacros.h"
#include "../../../../../RascBox.h"

#define COLUMNS (                                                                                                                                          \
    ("Name", 15,                                                                                                                                           \
     new StatTable::PlayerButtonValue(box, playerId)),                                                                                                     \
                                                                                                                                                           \
    ("Manpower/turn", 12,                                                                                                                                  \
     new StatTable::ProvstatWPercValue(8, &NumberFormat::manpower<8>, true,                                                                                \
                                       playerData->getManpowerBalancePerTurn(), totalIncome)),                                                             \
    ("Manpower", 12,                                                                                                                                       \
     new StatTable::ProvstatWPercValue(8, &NumberFormat::manpower<8>, false,                                                                               \
                                       playerData->getTotalManpower(), totalManpower)),                                                                    \
    ("Deployed", 12,                                                                                                                                       \
     new StatTable::UnsignedWPercValue(8, &NumberFormat::armyUnits<8>,                                                                                     \
                                       box.common.unitsDeployedStat->getUnitsDeployed(playerId), box.common.unitsDeployedStat->getTotalUnitsDeployed()))   \
)

namespace rasc {

    RascotabArmies::RascotabArmies(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme) :
        StatTable(location, theme, RASC_STATTABLE_COLUMN_INITIALISERS(COLUMNS), true),
        box(box) {
    }

    RascotabArmies::~RascotabArmies(void) {
    }

    void RascotabArmies::updateStats(void) {
        // Note: It is safe to modify the component hierarchy directly here as this is called by a stat system after update.

        // Clear any existing rows.
        clearRows();

        // First add up all manpower per turn and total manpower, for all players.
        provstat_t totalIncome = 0,
                   totalManpower = 0;
        for (const std::pair<const uint64_t, RascUpdatable *> & pair : box.common.playerDataSystem->mapChildren()) {
            PlayerData * data = (PlayerData *)pair.second;
            totalIncome += data->getManpowerBalancePerTurn();
            totalManpower += data->getTotalManpower();
        }

        // Get our player data and player id.
        PlayerData * ourPlayerData = box.netIntf->getData();
        uint64_t ourPlayerId = ourPlayerData->getId();
        
        // Add each row, set as home row if it is ourself, otherwise add.
        for (const std::pair<const uint64_t, RascUpdatable *> & pair : box.common.playerDataSystem->mapChildren()) {
            PlayerData * playerData = (PlayerData *)pair.second;
            uint64_t playerId = pair.first;
            std::initializer_list<ValueBase *> row = RASC_STATTABLE_ROW(COLUMNS);
            if (playerId == ourPlayerId) {
                setHomeRow(std::move(row));
            } else {
                addRow(std::move(row));
            }
        }
        
        // Re-sort the rows now we have added them all.
        reSort();
    }
}

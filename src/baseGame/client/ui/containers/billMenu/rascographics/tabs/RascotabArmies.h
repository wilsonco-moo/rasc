/*
 * RascotabArmies.h
 *
 *  Created on: 17 Nov 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_RASCOGRAPHICS_TABS_RASCOTABARMIES_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_RASCOGRAPHICS_TABS_RASCOTABARMIES_H_

#include "../StatTable.h"

namespace rasc {

    class RascBox;

    /**
     * The Armies Rascographics tab displays stats about the deployed armies
     * of each nation.
     */
    class RascotabArmies : public StatTable {
    private:
        RascBox & box;

    public:
        RascotabArmies(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme);
        virtual ~RascotabArmies(void);

        virtual void updateStats(void) override;
    };
}

#endif

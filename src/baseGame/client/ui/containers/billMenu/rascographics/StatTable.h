/*
 * StatTable.h
 *
 *  Created on: 27 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_RASCOGRAPHICS_STATTABLE_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_RASCOGRAPHICS_STATTABLE_H_

#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/base/Container.h>
#include <rascUI/util/Rectangle.h>
#include <rascUI/util/Location.h>
#include <wool/misc/RGBA.h>
#include <initializer_list>
#include <GL/gl.h>
#include <cstddef>
#include <string>
#include <vector>

#include "../../../../../common/gameMap/provinceUtil/ProvinceStatTypes.h"

namespace rascUI {
    class Theme;
}

namespace rasc {
    class RascBox;

    /**
     * A StatTable aims to display a table of stats in a coherent and meaningful way.
     * It consists of a title bar, which shows column titles and a home row, and a scroll pane containing
     * the table of numbers.
     * The home row is intended to show stats about the current player, whereas the scroll pane is intended
     * to show stats about other players.
     *
     * Columns must be specified using the StatTable::Column class, which carries a column name and a weight.
     * The weight represents the width of the column, as a proportion of the total width.
     * The weights of all columns must add up to one.
     *
     * Rows are added as a series of dynamically allocated StatTable::ValueBase instances, where each
     * one represents a cell in the table. There are multiple pre-defined types of StatTable::ValueBase, such
     * as StatTable::StringValue. Each type can store a different sort of data for that cell.
     * NOTE: IT IS ASSUMED THAT ALL CELLS IN A PARTICULAR COLUMN WILL USE THE SAME TYPE OF StatTable::ValueBase.
     *       IF THIS IS NOT THE CASE, UNDEFINED BEHAVIOUR WILL RESULT WHEN THE COLUMN IS SORTED.
     *
     * The column labels are buttons, that when clicked sort the table by that column.
     */
    class StatTable : public rascUI::Container {
    public:
        class ValueBase;
        class Column;

    private:
        // ------------------------- Row class ---------------------------------------

        // This is a simple internal wrapper class for RowData, allowing the rows to be sorted. This is
        // separate from the row components, so the component hierarchy does not need (expensive)
        // modifications when sorting.
        // NOTE: This class purposely does not handle memory management of it's internal RowData instance, to
        //       allow easy sorting. Management for that is done by StatTable, using our deleteData method.
        class Row {
        private:
            // Internally stores the data for each row, i.e: The ValueBase instances.
            // This class handles deleting the ValueBase instances associated with it, when we are deleted.
            class RowData {
            private:
                friend class Row;
                RowData(const RowData & other); // No copying.
                RowData & operator = (const RowData & other);
            public:
                StatTable * table;
                std::vector<ValueBase *> values;
                RowData(StatTable * table, std::initializer_list<ValueBase *> values);
                virtual ~RowData(void);
            };
            RowData * data;
        public:
            Row(StatTable * table, std::initializer_list<ValueBase *> values);
            virtual ~Row(void);
            bool operator < (const Row & other) const;
            bool operator > (const Row & other) const;
            inline void deleteData(void) { delete data; }
            inline std::vector<ValueBase *> & values(void) { return data->values; }
        };
        
        // ------------------------- Row component -----------------------------------
        
        // This class is used internally to draw rows. This is distinct from the row data, to make data
        // sorting easier. This class caches the positions of the cells, when recalculated.
        class RowComponent : public rascUI::ResponsiveComponent {
        private:
            StatTable * table; // So we know draw mode.
            std::vector<Row> * rows;
            std::vector<Column> * columns;
            size_t rowId;
            std::vector<rascUI::Rectangle> rectangleCache;
            bool lightColour;
        public:
            RowComponent(const rascUI::Location & location, std::vector<Row> * rows, size_t rowId, std::vector<Column> * columns, bool lightColour, StatTable * table);
            virtual ~RowComponent(void);
        protected:
            virtual bool shouldRespondToKeyboard(void) const override;
            virtual void recalculate(const rascUI::Rectangle & parentSize) override;
            virtual void onDraw(void) override;
            virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;
        };

    public:
        // ------------------------- Base value --------------------------------------
        /**
         * The base value type, all value types inherit from this.
         */
        class ValueBase {
        public:
            ValueBase(void);
            virtual ~ValueBase(void);
            virtual void draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool percentage) = 0;
            virtual void onResize(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect);
            virtual void onClick(void);
            virtual bool operator < (const ValueBase & other) const = 0;
        };
        // ------------------------------------- String value ------------------------
        /**
         * A value type that simply stores some text, and draws it.
         * This draws exactly the same regardless of whether percentage mode is enabled or not.
         */
        class StringValue : public ValueBase {
        private:
            std::string text;
        public:
            StringValue(const std::string & text);
            StringValue(const char * text);
            virtual ~StringValue(void);
            virtual void draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool percentage) override;
            virtual bool operator < (const ValueBase & other) const override;
        };
        // ------------------------------ Player button value ------------------------
        /**
         * A value which displays the player colour and name. This should look and act
         * the same as PlayerPanel, (the universal player button), except not being able
         * to display NO_PLAYER as nicely. This displays the player name and colour, and
         * opens the appropriate tab in the BILL menu when the row is clicked. When
         * sorted, this value sorts aphabetically by player name.
         */
        class PlayerButtonValue : public ValueBase {
        private:
            RascBox & box;
            uint64_t playerId;
            wool::RGBA playerColour;
            std::string playerName;
            rascUI::State internalState;
            rascUI::Location internalLocation;
        public:
            PlayerButtonValue(RascBox & box, uint64_t playerId);
            virtual ~PlayerButtonValue(void);
            virtual void onResize(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect) override;
            virtual void draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool percentage) override;
            virtual void onClick(void) override;
            virtual bool operator < (const ValueBase & other) const override;
        };
        // ----------------------------------- Unsigned value ------------------------
        /**
         * A value type for displaying uint64_t values using a format function.
         */
        class UnsignedValue : public ValueBase {
        public:
            /**
             * Required format function type - example: NumberFormat::generic.
             */
            using FormatFunc = unsigned int (*) (char *, uint64_t);
        private:
            std::vector<char> buffer;
            uint64_t value;
        public:
            UnsignedValue(unsigned int bufferSize, FormatFunc formatFunc, uint64_t value);
            virtual ~UnsignedValue(void);
            virtual void draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool percentage) override;
            virtual bool operator < (const ValueBase & other) const override;
        };
        // ----------------------------------- Provstat value ------------------------
        /**
         * A value type for displaying provstat_t values using a format function.
         */
        class ProvstatValue : public ValueBase {
        public:
            /**
             * Required format function type - example: NumberFormat::currency.
             */
            using FormatFunc = unsigned int (*) (char *, provstat_t, bool);
        private:
            std::vector<char> buffer;
            provstat_t value;
        public:
            ProvstatValue(unsigned int bufferSize, FormatFunc formatFunc, bool prefixPlus, provstat_t value);
            virtual ~ProvstatValue(void);
            virtual void draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool percentage) override;
            virtual bool operator < (const ValueBase & other) const override;
        };
        // ------------------------- Unsigned value with percentage ------------------
        /**
         * A value type for displaying uint64_t values using a format function, with
         * a percentage mode. Note that the percentage mode actually uses NumberFormat::permille
         * to allow an additional decimal place.
         */
        class UnsignedWPercValue : public ValueBase {
        public:
            /**
             * Required format function type - example: NumberFormat::generic.
             */
            using FormatFunc = unsigned int (*) (char *, uint64_t);
        private:
            constexpr static unsigned int PERCENTAGE_BUFFER_SIZE = 7;
            std::vector<char> absoluteBuffer;
            uint64_t value;
            char percentageBuffer[PERCENTAGE_BUFFER_SIZE];
        public:
            UnsignedWPercValue(unsigned int bufferSize, FormatFunc formatFunc, uint64_t value, uint64_t total);
            virtual ~UnsignedWPercValue(void);
            virtual void draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool percentage) override;
            virtual bool operator < (const ValueBase & other) const override;
        };
        // ------------------------ Provstat value with percentage -------------------
        /**
         * A value type for displaying provstat_t values using a format function, with
         * a percentage mode. Note that the percentage mode actually uses NumberFormat::permille
         * to allow an additional decimal place.
         */
        class ProvstatWPercValue : public ValueBase {
        public:
            /**
             * Required format function type - example: NumberFormat::currency.
             */
            using FormatFunc = unsigned int (*) (char *, provstat_t, bool);
        private:
            constexpr static unsigned int PERCENTAGE_BUFFER_SIZE = 7;
            std::vector<char> absoluteBuffer;
            provstat_t value;
            char percentageBuffer[PERCENTAGE_BUFFER_SIZE];
        public:
            ProvstatWPercValue(unsigned int bufferSize, FormatFunc formatFunc, bool prefixPlus, provstat_t value, provstat_t total);
            virtual ~ProvstatWPercValue(void);
            virtual void draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool percentage) override;
            virtual bool operator < (const ValueBase & other) const override;
        };
        // -------------------------- Column class -----------------------------------
        /**
         * This class represents a column, with a name and a weight. Column instances must be provided to
         * StatTable when StatTable is created.
         */
        class Column : public rascUI::Button {
        private:
            friend class StatTable;
            GLfloat weight;
            StatTable * statTable;
            size_t column;
        public:
            Column(const std::string & name, GLfloat weight);
            Column(const char * name, GLfloat weight);
            virtual ~Column(void);
        protected:
            virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;
        };
        // ---------------------------------------------------------------------------

    private:
        // Whether we are drawing in percentage mode or not.
        bool percentage;

        // Whether responsive rows are enabled or disabled. This decides whether our
        // RowComponents should respond to the keyboard or not.
        bool responsiveRows;

        std::vector<Column> columns;
        size_t columnCount;

        ssize_t sortColumn;
        bool ascending;

        std::vector<Row> rows;
        std::vector<RowComponent *> rowComponents;

        // ------ Stuff for home row ------
        rascUI::Theme * theme; // This is saved so we can change home row without needing to be added to a TopLevelContainer.
        std::vector<Row> homeRow; // This will only ever container one component.
        RowComponent * homeRowComponent;
        // --------------------------------


        rascUI::BackPanel topBackPanel;
        rascUI::BackPanel bottomBackPanel;
        rascUI::ScrollPane scrollPane;

        StatTable(const StatTable & other); // Don't allow copying of the entire stat table.
        StatTable & operator = (const StatTable & other);

        // This is called by the column buttons when they are clicked on.
        void sortBy(size_t sortColumn);

    public:
        /**
         * The main constructor for StatTable. Here the user must define the number of columns, and their
         * names/weights, as Column instances.
         */
        StatTable(const rascUI::Location & location, rascUI::Theme * theme, std::initializer_list<Column> columns, bool responsiveRows = false);
        virtual ~StatTable(void);

        /**
         * This method is provided for convenience.
         * A subclass can override this method, to provide an easy way to update stats.
         * By default this method does nothing.
         * 
         * When stat tables are being used for Rascographics tabs, this muse be called when either:
         *  > This tab is opened by the user.
         *  > This tab is active when Rascographics is told to update it's stats.
         * This method updates the displayed stats within this tab.
         */
        virtual void updateStats(void);

        /**
         * Sets the home row for this table, and deletes the old one if it existed.
         * The user must provide the same number of dynamically allocated ValueBase instances as columns.
         *
         * NOTE: This method modifies the component hierarchy, so MUST NOT BE CALLED from within a top level event.
         *
         * NOTE: These pointers must be possible to delete, as responsibility for managing the memory of these
         *       pointers is handed over to us here.
         */
        void setHomeRow(std::initializer_list<ValueBase *> values);

        /**
         * Adds a row to the table.
         * The user must provide the same number of dynamically allocated ValueBase instances as columns.
         *
         * NOTE: This method modifies the component hierarchy, so MUST NOT BE CALLED from within a top level event.
         *
         * NOTE: These pointers must be possible to delete, as responsibility for managing the memory of these
         *       pointers is handed over to us here.
         */
        void addRow(std::initializer_list<ValueBase *> values);

        /**
         * Deletes all rows in the table, including the home row.
         *
         * NOTE: This method modifies the component hierarchy, so MUST NOT BE CALLED from within a top level event.
         */
        void clearRows(void);

        /**
         * This should be called after clearing rows and adding new rows.
         * This re-sorts the rows using the previous sording order.
         */
        void reSort(void);

        /**
         * Returns true if we are drawing in percentage mode, and false otherwise.
         */
        inline bool isPercentageMode(void) const {
            return percentage;
        }

        /**
         * Sets whether we should be drawing in percentage mode or not.
         * True turns on percentage mode, false turns it off.
         */
        inline void setPercentageMode(bool percentageMode) {
            percentage = percentageMode;
        }
    };
}

#endif

/*
 * Rascographics.h
 *
 *  Created on: 27 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_RASCOGRAPHICS_RASCOGRAPHICS_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_RASCOGRAPHICS_RASCOGRAPHICS_H_

#include <array>

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include "../../../../ui/customComponents/icon/IconToggleButton.h"
#include "../BillMenuTab.h"

namespace rascUI {
    class Theme;
}

namespace rasc {
    class RascBox;
    class StatTable;

    /**
     * Rascographics is a menu which provides a series of tabs.
     * Each tab displays a series of stats about the game.
     * Rascographics is part of the BILL menu.
     */
    class Rascographics : public BillMenuTab {
    private:
        // The number of rascographics tabs. This MUST be updated when new tabs are added.
        static constexpr unsigned int TAB_COUNT = 3;

        RascBox & box;

        // This contains the rascographics tabs, like geography, economy and armies.
        std::array<StatTable *, TAB_COUNT> tabs;

        rascUI::BackPanel bottomBackPanel;
        rascUI::ToggleButtonSeries tabSeries;

        // This contains the toggle buttons for each tab.
        std::array<IconToggleButton, TAB_COUNT> tabButtons;

        rascUI::ToggleButtonSeries percentSeries;
        rascUI::ToggleButton percentButton;
        rascUI::FrontPanel bottomFrontPanel;

    public:
        Rascographics(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme);
        virtual ~Rascographics(void);

        /**
         * This must be called by the BILL menu, when either:
         *  > Bill menu is opened, and Rascographics is the active tab.
         *  > Rascographics is switched to from another BILL menu tab.
         *  > A stat update happens while the bill menu is open, and the
         *    Rascographics tab is active.
         * This method requests a stat update for the currently active
         * rascographics tab.
         */
        virtual void updateStats(void) override;
    };
}

#endif

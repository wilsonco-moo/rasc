/*
 * StatTable.cpp
 *
 *  Created on: 27 Dec 2018
 *      Author: wilson
 */

#include "StatTable.h"

#include <algorithm>

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/base/Theme.h>
#include <wool/font/Font.h>
#include <wool/misc/RGB.h>

#include "../../../../../common/util/numeric/KShortenedNumber.h"
#include "../../../../../common/playerData/PlayerDataSystem.h"
#include "../../../../../common/util/numeric/NumberFormat.h"
#include "../../../../../common/playerData/PlayerData.h"
#include "../../../../config/MapModeController.h"
#include "../../../../../common/util/Misc.h"
#include "../../../../RascBox.h"


// Define macros for the theming.
#define UI_BORDER        theme->uiBorder
#define UI_BWIDTH        theme->normalComponentWidth
#define UI_BHEIGHT       theme->normalComponentHeight



#define TOP_BACK_PANEL_LOC     0, 0, 0, UI_BHEIGHT*2 + UI_BORDER*3, 0.0f, 0.0f, 1.0f, 0.0f

#define SCROLL_PANE_LOC        0, UI_BHEIGHT*2 + UI_BORDER*3, -UI_BORDER, -UI_BHEIGHT*2-UI_BORDER*3, 0, 0, 1, 1
#define BOTTOM_BACK_PANEL_LOC  0, UI_BHEIGHT*2 + UI_BORDER*3, 0, -UI_BHEIGHT*2-UI_BORDER*3, 0, 0, 1, 1

#define HOME_ROW_LOC           UI_BORDER, UI_BHEIGHT+UI_BORDER, -UI_BORDER*3-UI_BWIDTH, UI_BHEIGHT + UI_BORDER, 0, 0, 1, 0

#define ROW_LOC                UI_BORDER, 0, -UI_BORDER*2, 0, 0, 0, 1, 1

namespace rasc {

    // ------------------------- Row class ---------------------------------------
        // ----------- RowData class -----------
            StatTable::Row::RowData::RowData(StatTable * table, std::initializer_list<ValueBase *> values) :
                table(table),
                values(values) {
            }
            StatTable::Row::RowData::~RowData(void) {
                for (ValueBase * value : values) {
                    delete value;
                }
            }
        // -------------------------------------
        StatTable::Row::Row(StatTable * table, std::initializer_list<ValueBase *> values) :
            data(new RowData(table, values)) {
        }
        StatTable::Row::~Row(void) {
        }
        bool StatTable::Row::operator < (const Row & other) const {
            size_t sortColumn = data->table->sortColumn;
            return *data->values[sortColumn] < *other.data->values[sortColumn];
        }
        bool StatTable::Row::operator > (const Row & other) const {
            size_t sortColumn = data->table->sortColumn;
            return  *other.data->values[sortColumn] < *data->values[sortColumn];
        }
        
    // ------------------------- Row component -----------------------------------

        StatTable::RowComponent::RowComponent(const rascUI::Location & location, std::vector<Row> * rows, size_t rowId, std::vector<Column> * columns, bool lightColour, StatTable * table) :
            rascUI::ResponsiveComponent(location),
            table(table),
            rows(rows),
            columns(columns),
            rowId(rowId),
            rectangleCache(columns->size()),
            lightColour(lightColour) {
        }
        StatTable::RowComponent::~RowComponent(void) {
        }
        bool StatTable::RowComponent::shouldRespondToKeyboard(void) const {
            return table->responsiveRows;
        }
        void StatTable::RowComponent::recalculate(const rascUI::Rectangle & parentSize) {
            Component::recalculate(parentSize);
            GLfloat weightUpTo = 0.0f;
            size_t columnId = 0;
            GLfloat realWidth = location.cache.width / location.xScale,
                    realHeight = location.cache.height / location.yScale;
            for (Column & column : *columns) {
                rectangleCache[columnId++] = rascUI::Rectangle(weightUpTo * realWidth, 0.0f, column.weight * realWidth, realHeight);
                weightUpTo += column.weight;
            }
            // Ensure onResize gets called for all our values.
            columnId = 0;
            rascUI::Theme * theme = getTopLevel()->theme;
            for (ValueBase * value : (*rows)[rowId].values()) {
                value->onResize(this, location, theme, rectangleCache[columnId++]);
            }
        }
        void StatTable::RowComponent::onDraw(void) {
            // Set the backing colour, from the theme.
            rascUI::Theme * theme = getTopLevel()->theme;
            theme->customSetDrawColour(lightColour ? theme->customColourBackplaneLight : theme->customColourBackplaneDark);

            GLfloat x2 = location.cache.x + location.cache.width,
                    y2 = location.cache.y + location.cache.height;
            glVertex2f(location.cache.x, location.cache.y);
            glVertex2f(x2, location.cache.y);
            glVertex2f(x2, y2);
            glVertex2f(location.cache.x, y2);

            // Draw each of our values
            size_t columnId = 0;
            bool isPercentageMode = table->isPercentageMode();
            for (ValueBase * value : (*rows)[rowId].values()) {
                value->draw(this, location, theme, rectangleCache[columnId++], isPercentageMode);
            }
        }
        void StatTable::RowComponent::onMouseClick(GLfloat viewX, GLfloat viewY, int button) {
            if (table->responsiveRows && button == getTopLevel()->getMb()) {
                for (ValueBase * value : (*rows)[rowId].values()) {
                    value->onClick();
                }
            }
        }

    // ------------------------- Base value --------------------------------------

        StatTable::ValueBase::ValueBase(void) {
        }
        StatTable::ValueBase::~ValueBase(void) {
        }
        void StatTable::ValueBase::onResize(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect) {
        }
        void StatTable::ValueBase::onClick(void) {
        }
    // ------------------------------------- String value ------------------------
        StatTable::StringValue::StringValue(const std::string & text) :
            ValueBase(),
            text(text) {
        }
        StatTable::StringValue::StringValue(const char * text) :
            ValueBase(),
            text(text) {
        }
        StatTable::StringValue::~StringValue(void) {
        }
        void StatTable::StringValue::draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool percentage) {
            theme->customSetDrawColour(theme->customColourMainText);
            theme->customDrawTextNoMouse(location, rect.x, rect.y, 1.0f, 1.0f, text);
        }
        bool StatTable::StringValue::operator < (const ValueBase & other) const { return text < ((StringValue &)other).text; }
    // ----------------------- Player button value -------------------------------
        StatTable::PlayerButtonValue::PlayerButtonValue(RascBox & box, uint64_t playerId) :
            ValueBase(),
            box(box),
            playerId(playerId),
            playerColour(),
            playerName(),
            internalState(rascUI::State::normal),
            internalLocation() {
            // Set name and colour appropriately.
            PlayerData * data = box.common.playerDataSystem->idToDataChecked(playerId);
            if (data == NULL) {
                playerName = (playerId == PlayerData::NO_PLAYER ? "No player" : "Unknown player");
                playerColour = wool::RGBA::GREY;
            } else {
                playerName = data->getName();
                playerColour = data->getColour();
            }
            // Add a state modifier to our internal state which always returns our state field.
            internalLocation.pushStateModifier([this](rascUI::State state, rascUI::Component * component) -> rascUI::State {
                return internalState;
            });
        }
        StatTable::PlayerButtonValue::~PlayerButtonValue(void) {
        }
        void StatTable::PlayerButtonValue::onResize(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect) {
            internalLocation.xScale = location.xScale,
            internalLocation.yScale = location.yScale;
            internalLocation.cache.x = location.cache.x + (rect.x + theme->uiBorder) * internalLocation.xScale;
            internalLocation.cache.y = location.cache.y + (rect.y + theme->uiBorder * 0.5f) * internalLocation.yScale;
            internalLocation.cache.width = (rect.width - 2.0f * theme->uiBorder) * internalLocation.xScale;
            internalLocation.cache.height = theme->normalComponentHeight * internalLocation.yScale;
        }
        void StatTable::PlayerButtonValue::draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool isPercentageMode) {
            // Ensure that our internal location has same state as provided location.
            if (location.getState() != internalState) {
                internalState = location.getState();
                internalLocation.updateState(component);
            }
            // Draw the button background.
            theme->drawButton(internalLocation);
            // Draw the colour box.
            GLfloat size = theme->normalComponentHeight - 2.0f * theme->uiBorder,
                    x1 = internalLocation.cache.x + theme->uiBorder * location.xScale,
                    y1 = internalLocation.cache.y + theme->uiBorder * location.yScale,
                    x2 = x1 + size * location.xScale,
                    y2 = y1 + size * location.yScale;
            wool_setColourRGBA(playerColour)
            glVertex2f(x1, y1); glVertex2f(x2, y1); glVertex2f(x2, y2); glVertex2f(x1, y2);
            // Draw the text over the top, (using custom drawing).
            theme->customSetDrawColour(theme->customColourMainText);
            theme->customDrawTextMouse(internalLocation, size + theme->uiBorder, 0.0f, 1.0f, 1.0f, playerName);
        }
        void StatTable::PlayerButtonValue::onClick(void) {
            box.uiBillMenuShowPlayer(playerId);
        }
        bool StatTable::PlayerButtonValue::operator < (const ValueBase & other) const { return playerName < ((PlayerButtonValue &)other).playerName; }
    // -------------------------------- Unsigned value ---------------------------
        StatTable::UnsignedValue::UnsignedValue(unsigned int bufferSize, FormatFunc formatFunc, uint64_t value) :
            ValueBase(),
            buffer(bufferSize),
            value(value) {
            formatFunc(&buffer[0], value);
        }
        StatTable::UnsignedValue::~UnsignedValue(void) {
        }
        void StatTable::UnsignedValue::draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool percentage) {
            theme->customSetDrawColour(theme->customColourMainText);
            theme->customDrawTextNoMouse(location, rect.x, rect.y, 1.0f, 1.0f, &buffer[0]);
        }
        bool StatTable::UnsignedValue::operator < (const ValueBase & other) const { return value < ((UnsignedValue &)other).value; }
    // -------------------------------- Provstat value ---------------------------
        StatTable::ProvstatValue::ProvstatValue(unsigned int bufferSize, FormatFunc formatFunc, bool prefixPlus, provstat_t value) :
            ValueBase(),
            buffer(bufferSize),
            value(value) {
            formatFunc(&buffer[0], value, prefixPlus);
        }
        StatTable::ProvstatValue::~ProvstatValue(void) {
        }
        void StatTable::ProvstatValue::draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool percentage) {
            theme->customSetDrawColour(theme->customColourMainText);
            theme->customDrawTextNoMouse(location, rect.x, rect.y, 1.0f, 1.0f, &buffer[0]);
        }
        bool StatTable::ProvstatValue::operator < (const ValueBase & other) const { return value < ((ProvstatValue &)other).value; }
    // -------------------------- Unsigned with percentage value -----------------
        StatTable::UnsignedWPercValue::UnsignedWPercValue(unsigned int bufferSize, FormatFunc formatFunc, uint64_t value, uint64_t total) :
            ValueBase(),
            absoluteBuffer(bufferSize),
            value(value) {
            formatFunc(&absoluteBuffer[0], value);
            NumberFormat::permille<PERCENTAGE_BUFFER_SIZE>(percentageBuffer, Misc::valueOutOfTotalUnsigned(value, total, 1000), false);
        }
        StatTable::UnsignedWPercValue::~UnsignedWPercValue(void) {
        }
        void StatTable::UnsignedWPercValue::draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool percentage) {
            theme->customSetDrawColour(theme->customColourMainText);
            theme->customDrawTextNoMouse(location, rect.x, rect.y, 1.0f, 1.0f, percentage ? percentageBuffer : &absoluteBuffer[0]);
        }
        bool StatTable::UnsignedWPercValue::operator < (const ValueBase & other) const { return value < ((UnsignedWPercValue &)other).value; }
    // ---------------------------- Provstat with percentage value ---------------
        StatTable::ProvstatWPercValue::ProvstatWPercValue(unsigned int bufferSize, FormatFunc formatFunc, bool prefixPlus, provstat_t value, provstat_t total) :
            ValueBase(),
            absoluteBuffer(bufferSize),
            value(value) {
            formatFunc(&absoluteBuffer[0], value, prefixPlus);
            // Note: never prefix plus to the percentage: it doesn't make sense, even if the user requested it for the regular number.
            NumberFormat::permille<PERCENTAGE_BUFFER_SIZE>(percentageBuffer, Misc::valueOutOfTotalProvstat(value, total, 1000), false);
        }
        StatTable::ProvstatWPercValue::~ProvstatWPercValue(void) {
        }
        void StatTable::ProvstatWPercValue::draw(RowComponent * component, const rascUI::Location & location, rascUI::Theme * theme, const rascUI::Rectangle & rect, bool percentage) {
            theme->customSetDrawColour(theme->customColourMainText);
            theme->customDrawTextNoMouse(location, rect.x, rect.y, 1.0f, 1.0f, percentage ? percentageBuffer : &absoluteBuffer[0]);
        }
        bool StatTable::ProvstatWPercValue::operator < (const ValueBase & other) const { return value < ((ProvstatWPercValue &)other).value; }
    // -------------------------- Column class -----------------------------------
        StatTable::Column::Column(const std::string & name, GLfloat weight) :
            rascUI::Button(rascUI::Location(), name),
            weight(weight),
            statTable(NULL),
            column(0) {
        }
        StatTable::Column::Column(const char * name, GLfloat weight) :
            rascUI::Button(rascUI::Location(), std::string(name)),
            weight(weight),
            statTable(NULL),
            column(0) {
        }
        StatTable::Column::~Column(void) {
        }
        void StatTable::Column::onMouseClick(GLfloat viewX, GLfloat viewY, int button) {
            if (button == getMb()) {
                statTable->sortBy(column);
            }
        }
    // ---------------------------------------------------------------------------

    void StatTable::sortBy(size_t sortColumn) {
        if (this->sortColumn == (ssize_t)sortColumn) {
            ascending = !ascending;
            std::reverse(rows.begin(), rows.end());
        } else {
            ascending = true;
            this->sortColumn = (ssize_t)sortColumn;
            std::sort(rows.begin(), rows.end());
        }
        // Ensure that the resize method gets called on all row components after sorting,
        // since their locations will have changed.
        childRecalculate(&scrollPane, location.cache);
    }

    StatTable::StatTable(const rascUI::Location & location, rascUI::Theme * theme, std::initializer_list<Column> columns, bool responsiveRows) :
        rascUI::Container(location),
        percentage(false),
        responsiveRows(responsiveRows),
        columns(columns),
        columnCount(columns.size()),
        sortColumn(-1),
        ascending(true),
        rows(),
        rowComponents(),
        theme(theme),
        homeRow(),
        homeRowComponent(NULL),
        topBackPanel(rascUI::Location(TOP_BACK_PANEL_LOC)),
        bottomBackPanel(rascUI::Location(BOTTOM_BACK_PANEL_LOC)),
        scrollPane(theme, rascUI::Location(SCROLL_PANE_LOC), false, true, false, false) {

        add(&bottomBackPanel);
        add(&scrollPane);
        add(&topBackPanel);

        GLfloat weightUpTo = 0.0f;
        size_t columnId = 0;
        for (Column & column : this->columns) {
            column.location.position = rascUI::Rectangle(UI_BORDER - (UI_BORDER*2.0f + UI_BWIDTH) * weightUpTo, 0, -(UI_BORDER*2.0f + UI_BWIDTH) * column.weight - UI_BORDER, UI_BHEIGHT);
            column.location.weight   = rascUI::Rectangle(weightUpTo, 0, column.weight, 0);
            column.column = columnId++;
            column.statTable = this;
            weightUpTo += column.weight;
            add(&column);
        }
    }

    StatTable::~StatTable(void) {
        for (Row & row : rows) {
            row.deleteData();
        }
        for (RowComponent * component : rowComponents) {
            delete component;
        }
        if (homeRowComponent != NULL) {
            homeRow[0].deleteData();
            delete homeRowComponent;
        }
    }

    void StatTable::updateStats(void) {
    }

    void StatTable::setHomeRow(std::initializer_list<ValueBase *> values) {
        if (homeRowComponent != NULL) {
            homeRow[0].deleteData();
            homeRow.clear();
            remove(homeRowComponent);
            delete homeRowComponent;
        }
        homeRow.push_back(Row(this, std::move(values)));
        homeRowComponent = new RowComponent(rascUI::Location(HOME_ROW_LOC), &homeRow, 0, &columns, true, this);
        add(homeRowComponent);
    }
    void StatTable::addRow(std::initializer_list<ValueBase *> values) {
        rows.push_back(Row(this, std::move(values)));
        RowComponent * c = new RowComponent(rascUI::Location(ROW_LOC), &rows, rowComponents.size(), &columns, rowComponents.size() % 2 == 1, this);
        rowComponents.push_back(c);
        scrollPane.contents.add(c);
    }
    void StatTable::clearRows(void) {
        for (Row & row : rows) {
            row.deleteData();
        }
        rows.clear();
        scrollPane.contents.clear();
        for (RowComponent * component : rowComponents) {
            delete component;
        }
        rowComponents.clear();

        if (homeRowComponent != NULL) {
            homeRow[0].deleteData();
            homeRow.clear();
            remove(homeRowComponent);
            delete homeRowComponent;
            homeRowComponent = NULL;
        }
    }

    void StatTable::reSort(void) {
        if (sortColumn != -1) {
            if (ascending) {
                std::sort(rows.begin(), rows.end());
            } else {
                std::sort(rows.begin(), rows.end(), std::greater<Row>());
            }
        }
    }
}

/*
 * Rascographics.cpp
 *
 *  Created on: 27 Dec 2018
 *      Author: wilson
 */

#include "Rascographics.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#include "../../../../../common/util/definitions/IconDef.h"
#include "tabs/RascotabGeography.h"
#include "tabs/RascotabEconomy.h"
#include "tabs/RascotabArmies.h"

// For the layout system.
#define LAYOUT_THEME theme

// Location of bottom panel.
#define BOTTOM_BACK_PANEL_LOC \
    PIN_B(COUNT_OUTBORDER_Y(1), GEN_FILL)

// Size and location of the tab buttons.
#define TAB_BUTTONS_WIDTH 340
#define TAB_BUTTONS_LOC(id)               \
    DEFINED_BORDERTABLE_X(rascotabnew, id,\
     PIN_L(TAB_BUTTONS_WIDTH,             \
      SUB_BORDER(BOTTOM_BACK_PANEL_LOC)   \
    ))

DEFINE_TABLE(rascotabnew, 5, 4, 4)

// The location of the bottom front panel.
#define BOTTOM_FRONT_PANEL_LOC                                   \
    SUB_BORDERMARGIN_LR(TAB_BUTTONS_WIDTH, PERCENT_BUTTON_WIDTH, \
     SUB_BORDER(BOTTOM_BACK_PANEL_LOC)                           \
    )

// Size and location of the percent button.
#define PERCENT_BUTTON_WIDTH UI_BWIDTH  
#define PERCENT_BUTTON_LOC              \
    PIN_R(PERCENT_BUTTON_WIDTH,         \
     SUB_BORDER(BOTTOM_BACK_PANEL_LOC)  \
    )

// Location of main container.
#define MAIN_CONTAINER_LOC \
    SUB_MARGIN_B(COUNT_OUTBORDER_Y(1), GEN_FILL)


// An X macro defining the tabs within rascographics.
#define TABS        \
    X(terrain, Geography, 0) \
    X(currency, Economy,  1) \
    X(army, Armies,       2)


namespace rasc {

    Rascographics::Rascographics(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme) :
        BillMenuTab(location),
        box(box),

        // For each tab, add it to the tabs array.
        #define X(icon, name, id) \
            new Rascotab##name(rascUI::Location(MAIN_CONTAINER_LOC), box, theme),
        tabs({ TABS }),
        #undef X

        bottomBackPanel(rascUI::Location(BOTTOM_BACK_PANEL_LOC)),
        tabSeries(false, [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
            getTopLevel()->afterEvent->addFunction([this, toggleButtonId](void) {
                // When a tab is selected, make it visible, make all others invisible, and
                // update it's stats.
                for (unsigned int i = 0; i < TAB_COUNT; i++) {
                    if (i == toggleButtonId) {
                        tabs[i]->updateStats();
                        tabs[i]->setVisible(true);
                    } else {
                        tabs[i]->setVisible(false);
                    }
                }
            });
        }),

        // For each tab, add a button with some appropriate text.
        #define X(icon, name, id) \
            IconToggleButton(&tabSeries, rascUI::Location(TAB_BUTTONS_LOC(id)), IconDef::icon, #name),
        tabButtons({ TABS }),
        #undef X

        percentSeries(true, [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
            bool percentage = (toggleButton != NULL);
            for (StatTable * tab : tabs) {
                tab->setPercentageMode(percentage);
            }
        }),
        percentButton(&percentSeries, rascUI::Location(PERCENT_BUTTON_LOC), "%"),
        bottomFrontPanel(rascUI::Location(BOTTOM_FRONT_PANEL_LOC)) {

        // Add tabs, make first one visible.
        for (unsigned int i = 0; i < TAB_COUNT; i++) {
            add(tabs[i]);
            tabs[i]->setVisible(i == 0);
        }

        add(&bottomBackPanel);
        for (IconToggleButton & button : tabButtons) {
            add(&button);
        }
        add(&bottomFrontPanel);
        add(&percentButton);

        tabSeries.setSelectedId(0);
    }

    Rascographics::~Rascographics(void) {
        for (StatTable * table : tabs) {
            delete table;
        }
    }

    void Rascographics::updateStats(void) {
        // Only update the stats of the currently visible tab.
        tabs[tabSeries.getSelectedId()]->updateStats();
    }
}

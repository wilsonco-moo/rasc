/*
 * BillMenu.cpp
 *
 *  Created on: 26 Dec 2018
 *      Author: wilson
 */

#include "BillMenu.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/base/Component.h>
#include <rascUI/util/Location.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#include "../../../../common/stats/UnifiedStatSystem.h"
#include "rascographics/Rascographics.h"
#include "diplostatics/Diplostatics.h"
#include "../gameMenu/GameMenu.h"
#include "../../GameUIMenuDef.h"
#include "../../../RascBox.h"

// For the layout system.
#define LAYOUT_THEME theme

// The location of the entire bill menu.
#define BILL_MENU_LOC \
    GEN_CENTRE_FIXEDWEIGHT(240, 0, 0.5, 0.5)

// The location of the top back panel.
#define TOP_BACKPANEL_LOC \
    PIN_T(COUNT_OUTBORDER_Y(1), GEN_FILL)

// The location of the tab buttons
#define TAB_BUTTONS_WIDTH (140 * TAB_COUNT)
#define TAB_BUTTONS_LOC(id)              \
    BORDERTABLE_X(id, TAB_COUNT,         \
     PIN_L(TAB_BUTTONS_WIDTH,            \
      SUB_BORDER(TOP_BACKPANEL_LOC)      \
    ))

// The location of the top front panel.
#define TOP_FRONTPANEL_LOC                                      \
    SUB_BORDERMARGIN_LR(TAB_BUTTONS_WIDTH, CLOSE_BUTTON_WIDTH,  \
     SUB_BORDER(TOP_BACKPANEL_LOC))

// The width and location of the close button.
#define CLOSE_BUTTON_WIDTH 56
#define CLOSE_BUTTON_LOC            \
    PIN_R(CLOSE_BUTTON_WIDTH,       \
     SUB_BORDER(TOP_BACKPANEL_LOC))

// The location of the centre container.
#define CENTRE_CONTAINER_LOC \
    SUB_MARGIN_T(COUNT_OUTBORDER_Y(1), GEN_FILL)

namespace rasc {

    BillMenu::BillMenu(RascBox & box, rascUI::Theme * theme) :
        rascUI::Container(rascUI::Location(BILL_MENU_LOC)),
        box(box),
        topBackPanel(rascUI::Location(TOP_BACKPANEL_LOC)),

        // Change and update tab when toggle buttons are selected.
        tabSeries(false,
            [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
                switchTab(toggleButtonId);
            }
        ),

        // For each tab, add a toggle button representing it.
        #define X(name, id) \
            rascUI::ToggleButton(&tabSeries, rascUI::Location(TAB_BUTTONS_LOC(id)), #name),
        tabButtons{ BILL_MENU_TABS },
        #undef X

        topFrontPanel(rascUI::Location(TOP_FRONTPANEL_LOC)),
        closeButton(rascUI::Location(CLOSE_BUTTON_LOC), "Close",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) hide();
            }
        ),

        // Create an instance of the appropriate class for each tab, add them to the tabs array.
        #define X(name, id) \
            new name(rascUI::Location(CENTRE_CONTAINER_LOC), box, theme),
        tabs{ BILL_MENU_TABS },
        #undef X

        isCurrentlyOpen(false),
        
        afterUpdateToken(box.common.statSystem->addAfterUpdateFunction([this](void) {
            // Note: It is safe to modify the component hierarchy DIRECTLY here, because the stat system's
            //       after update functions are always called outside any UI system events.

            // Only if we are open, on a stat update, update the stats of the currently open BILL menu tab.
            if (isOpen()) {
                tabs[tabSeries.getSelectedId()]->updateStats();
            }
        })) {

        // Add each tab, make them by default not visible.
        for (BillMenuTab * tab : tabs) {
            add(tab);
            tab->setVisible(false);
        }
        add(&topBackPanel);
        for (rascUI::ToggleButton & button : tabButtons) {
            add(&button);
        }
        add(&topFrontPanel);
        add(&closeButton);

        // Set the first tab visible, and select it's toggle button.
        tabSeries.setSelectedId(0);
        tabs[0]->setVisible(true);
    }

    BillMenu::~BillMenu(void) {
        box.common.statSystem->removeAfterUpdateFunction(afterUpdateToken);
        for (BillMenuTab * tab : tabs) {
            delete tab;
        }
    }

    void BillMenu::show(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            if (!isCurrentlyOpen) {
                // When we are opened, update active tab only.
                tabs[tabSeries.getSelectedId()]->updateStats();
                isCurrentlyOpen = true;
                setVisible(true);
                getTopLevel()->setSelectionBound(this);
                getTopLevel()->setKeyboardSelected(&closeButton);
            }
        });
    }

    void BillMenu::hide(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            if (isCurrentlyOpen) {
                isCurrentlyOpen = false;
                setVisible(false);
                getTopLevel()->resetSelectionBound();
                box.uiKeyboardSelectBillButton();
            }
        });
    }

    void BillMenu::switchTab(unsigned int tab) {
        getTopLevel()->afterEvent->addFunction([this, tab](void) {
            for (unsigned int i = 0; i < TAB_COUNT; i++) {
                tabs[i]->setVisible(i == tab);
            }
            if (isCurrentlyOpen) {
                tabs[tab]->updateStats();
            }
        });
    }

    void BillMenu::showPlayer(uint64_t playerId) {
        // First make sure we are open (using UI's overlay menu method, to make sure
        // other menus close). This will run our show method, so *is* in an after event function.
        box.uiOpenOverlayMenu(GameUIMenus::billMenu, true, true);
        
        getTopLevel()->afterEvent->addFunction([this, playerId](void) {
            // Ensure that Diplostatics tab is switched to.
            tabSeries.setSelectedId(tabDiplostatics);
            for (unsigned int i = 0; i < TAB_COUNT; i++) {
                tabs[i]->setVisible(i == tabDiplostatics);
            }
            // Ensure that the Diplostatics menu shows the right player.
            // This will automatically cause Diplostatics to update its stats.
            ((Diplostatics *)tabs[tabDiplostatics])->showPlayer(playerId);
        });
    }
}

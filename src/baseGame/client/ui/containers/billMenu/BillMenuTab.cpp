/*
 * BillMenuTab.cpp
 *
 *  Created on: 20 Dec 2019
 *      Author: wilson
 */

#include "BillMenuTab.h"

namespace rasc {
    BillMenuTab::BillMenuTab(const rascUI::Location & location) :
        rascUI::Container(location) {
    }
    BillMenuTab::~BillMenuTab(void) {
    }
}

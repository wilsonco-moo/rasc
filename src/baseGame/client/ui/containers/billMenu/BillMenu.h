/*
 * BillMenu.h
 *
 *  Created on: 26 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_BILLMENU_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_BILLMENU_H_

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/base/Container.h>

namespace rascUI {
    class Theme;
}

namespace rasc {
    class RascBox;
    class BillMenuTab;

    /**
     * The BILL menu is a popup menu which (will) contain a series of tabs, including
     * Rascographics. The Rascographics tab provides stat information.
     * Other tabs will be added to BILL, which show information about our own country,
     * amongst other things.
     */
    class BillMenu : public rascUI::Container {
    public:
        // An X macro defining the tabs within the BILL menu.
        #define BILL_MENU_TABS  \
            X(Rascographics, 0) \
            X(Diplostatics,  1)

        // An enum defining the ID of each BILL menu tab,
        // accessible like: BillMenu::tabRascographics.
        #define X(name, id) \
            tab##name,
        enum { BILL_MENU_TABS };
        #undef X

    private:
        RascBox & box;
        rascUI::BackPanel topBackPanel;

        // The number of BILL menu tabs. This MUST be updated when new tabs are added.
        constexpr static unsigned int TAB_COUNT = 2;

        // The buttons for the BILL menu tabs.
        rascUI::ToggleButtonSeries tabSeries;
        rascUI::ToggleButton tabButtons[TAB_COUNT];

        rascUI::FrontPanel topFrontPanel;
        rascUI::Button closeButton;

        // The BILL menu tabs, which each inherit from BillMenuTab.
        BillMenuTab * tabs[TAB_COUNT];

        bool isCurrentlyOpen;
        void * afterUpdateToken;

        // Don't allow copying: We hold raw resources.
        BillMenu(const BillMenu & other);
        BillMenu & operator = (const BillMenu & other);

    public:
        BillMenu(RascBox & box, rascUI::Theme * theme);
        virtual ~BillMenu(void);

    public:
        /**
         * Shows the BILL menu, at some point later using an afterEvent function.
         */
        void show(void);
        /**
         * Hides the BILL menu, at some point later using an afterEvent function.
         */
        void hide(void);
        /**
         * Returns whether the BILL menu is currently open.
         */
        inline bool isOpen(void) const {
            return isCurrentlyOpen;
        }
        /**
         * Switches the BILL menu to the specified tab, at some point later using an afterEvent function.
         * The BILL menu does not need to be currently open to do this.
         * Tabs can be specified with the enum, such as BillMenu::tabRascographics.
         */
        void switchTab(unsigned int tab);
        /**
         * Ensures that at some point later, (using an afterEvent function):
         *  The bill menu is opened,
         *  the Diplostatics tab is switched to,
         *  the Diplostatics tab is set to show the specified player id.
         */
        void showPlayer(uint64_t playerId);
    };
}

#endif

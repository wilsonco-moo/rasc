/*
 * Diplostatics.h
 *
 *  Created on: 20 Dec 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_DIPLOSTATICS_DIPLOSTATICS_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_DIPLOSTATICS_DIPLOSTATICS_H_

#include "../BillMenuTab.h"

#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>

#include "../../../../../common/playerData/info/ThreatManagedWeights.h"
#include "../../../customComponents/flag/FlagComponent.h"
#include "../../../customComponents/ColourComponent.h"

namespace rascUI {
    class Theme;
}

namespace rasc {
    class RascBox;

    /**
     * Diplostatics, (diplomacy and stats), is a menu which shows focused
     * information about a specific player. It initially shows information
     * about yourself, but shows information about other players when a
     * universal player button is clicked.
     * Diplostatics is part of the BILL menu.
     */
    class Diplostatics : public BillMenuTab {
    private:
        RascBox & box;

        // The player ID we are showing currently.
        uint64_t currentPlayerId;

        // If this is the first time updating stats.
        bool first;


        rascUI::BackPanel backPanel;

        rascUI::Button showOwnButton;

        FlagComponent flag;

        ColourComponent colourPanel;
        rascUI::FrontPanel playerNamePanel;
        rascUI::Label playerNameLabel;

        rascUI::FrontPanel behaviourPanel;
        rascUI::Label behaviourLabels[ThreatManagedWeights::BEHAVIOUR_VALUES_COUNT];

        rascUI::FrontPanel capitalProvincePanel;
        rascUI::Label capitalProvinceLabel;

        rascUI::FrontPanel leftStatsPanel;
        rascUI::Label provincesLabel,
                      areasLabel,
                      continentsLabel,
                      landUseLabel;

        rascUI::FrontPanel rightManpowerPanel;
        rascUI::Label manpowerBalanceLabel,
                      totalManpowerLabel;

        rascUI::FrontPanel rightCurrencyPanel;
        rascUI::Label currencyIncomeLabel,
                      currencyExpenditureLabel,
                      currencyBalanceLabel,
                      cashLabel;
    public:
        Diplostatics(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme);
        virtual ~Diplostatics(void);

        /**
         * This must be called by the BILL menu, when either:
         *  > Bill menu is opened, and Diplostatics is the active tab.
         *  > Diplostatics is switched to from another BILL menu tab.
         *  > A stat update happens while the bill menu is open, and the
         *    Diplostatics tab is active.
         * This method updates all the displayed stats in Diplostatics.
         */
        virtual void updateStats(void) override;

        /**
         * Switches Diplostatics to display information about the specified player ID.
         * Note that updateStats() is automatically called here.
         * The player does not have to exist: No player or Unknown player will
         * be shown respectively.
         */
        void showPlayer(uint64_t playerId);
    };
}

#endif

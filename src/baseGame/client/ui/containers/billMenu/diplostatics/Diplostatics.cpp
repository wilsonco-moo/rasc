/*
 * Diplostatics.cpp
 *
 *  Created on: 20 Dec 2019
 *      Author: wilson
 */

#include "Diplostatics.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <type_traits>
#include <cstring>

#include "../../../../../common/playerData/PlayerDataSystem.h"
#include "../../../../../common/stats/types/ContinentsStat.h"
#include "../../../../../common/stats/types/ProvincesStat.h"
#include "../../../../../common/util/numeric/NumberFormat.h"
#include "../../../../../common/stats/types/AreasStat.h"
#include "../../../../../common/playerData/PlayerData.h"
#include "../../../../../common/util/Misc.h"
#include "../../../../RascClientNetIntf.h"
#include "../../../../RascBox.h"


#define LAYOUT_THEME theme

// The location within diplostatics of all UI components.
#define ALL_LOC     \
    SUB_BORDER_X(   \
     SUB_BORDER_B(  \
      GEN_FILL      \
    ))

// The height and location of the whole top section, including flag and colour panels etc.
#define TOP_SECTION_HEIGHT \
    (BEHAVIOUR_PANEL_HEIGHT + UI_BORDER*2)
#define TOP_SECTION_LOC \
    PIN_T(TOP_SECTION_HEIGHT, ALL_LOC)

// The height and location of the show own button.
#define SHOW_OWN_WIDTH FLAG_WIDTH
#define SHOW_OWN_HEIGHT COUNT_INBORDER_Y(1)
#define SHOW_OWN_LOC           \
    PIN_L(SHOW_OWN_WIDTH,      \
     PIN_T(SHOW_OWN_HEIGHT,    \
      TOP_SECTION_LOC          \
    ))

// The width, height and location of the flag component.
#define FLAG_WIDTH \
    (Flag::ASPECT_RATIO * FLAG_HEIGHT)
#define FLAG_HEIGHT \
    (TOP_SECTION_HEIGHT - SHOW_OWN_HEIGHT - UI_BORDER)
#define FLAG_LOC                        \
    MOVE_Y(SHOW_OWN_HEIGHT + UI_BORDER, \
     PIN_L(FLAG_WIDTH,                  \
      PIN_T(FLAG_HEIGHT,                \
       TOP_SECTION_LOC                  \
    )))

// The location of the top-right section (colour component)
#define TOP_RIGHT_SECTION_LOC \
    SUB_BORDERMARGIN_L(FLAG_WIDTH, TOP_SECTION_LOC)

// The width, height and location of the player name panel.
#define PLAYER_NAME_PANEL_WIDTH 128
#define PLAYER_NAME_PANEL_HEIGHT COUNT_INBORDER_Y(1)
#define PLAYER_NAME_PANEL_LOC                     \
    PIN_L(PLAYER_NAME_PANEL_WIDTH,                \
     PIN_T(PLAYER_NAME_PANEL_HEIGHT,              \
      SUB_BORDER(                                 \
       TOP_RIGHT_SECTION_LOC                      \
    )))

// The width, height and location of the behaviour panel
#define BEHAVIOUR_PANEL_WIDTH 266
#define BEHAVIOUR_PANEL_HEIGHT (ThreatManagedWeights::BEHAVIOUR_VALUES_COUNT * UI_BHEIGHT)
#define BEHAVIOUR_PANEL_LOC                              \
    PIN_R(BEHAVIOUR_PANEL_WIDTH,                         \
     PIN_T(BEHAVIOUR_PANEL_HEIGHT,                       \
      SUB_BORDER(                                        \
       TOP_RIGHT_SECTION_LOC                             \
    )))
#define BEHAVIOUR_LABEL_LOC(id) \
    TABLE_Y(id, ThreatManagedWeights::BEHAVIOUR_VALUES_COUNT, BEHAVIOUR_PANEL_LOC)

// The location of the entire bottom section.
#define BOTTOM_SECTION_LOC \
    SUB_BORDERMARGIN_T(TOP_SECTION_HEIGHT, ALL_LOC)

// The location of the entire bottom left section.
#define BOTTOM_LEFT_SECTION_LOC \
    BORDERSPLIT_L(0.5f, BOTTOM_SECTION_LOC)

// The height and location of the capital province panel.
#define CAPITAL_PROVINCE_HEIGHT COUNT_INBORDER_Y(1)
#define CAPITAL_PROVINCE_LOC \
    PIN_T(CAPITAL_PROVINCE_HEIGHT, BOTTOM_LEFT_SECTION_LOC)

// The location of the left stats panel.
#define LEFT_STATS_PANEL_LOC \
    SUB_BORDERMARGIN_T(CAPITAL_PROVINCE_HEIGHT, BOTTOM_LEFT_SECTION_LOC)

// The location of the labels within the left stats panel.
#define LEFT_STATS_LABEL_LOC(id) \
    MOVE_NORMAL_Y(id,            \
     SETNORMAL_Y(                \
      LEFT_STATS_PANEL_LOC       \
    ))

// The location of the entire bottom right section.
#define BOTTOM_RIGHT_SECTION_LOC \
    BORDERSPLIT_R(0.5f, BOTTOM_SECTION_LOC)

// The location of the right manpower panel.
#define RIGHT_MANPOWER_PANEL_LOC \
    BORDERSPLIT_T(1.0f / 3.0f, BOTTOM_RIGHT_SECTION_LOC)

// The location of the labels within the right manpower panel.
#define RIGHT_MANPOWER_LABEL_LOC(id) \
    MOVE_NORMAL_Y(id,                \
     SETNORMAL_Y(                    \
      RIGHT_MANPOWER_PANEL_LOC       \
    ))

// The location of the right manpower panel.
#define RIGHT_CURRENCY_PANEL_LOC \
    BORDERSPLIT_B(2.0f / 3.0f, BOTTOM_RIGHT_SECTION_LOC)

// The location of the labels within the right manpower panel.
#define RIGHT_CURRENCY_LABEL_LOC(id) \
    MOVE_NORMAL_Y(id,                \
     SETNORMAL_Y(                    \
      RIGHT_CURRENCY_PANEL_LOC       \
    ))

namespace rasc {

    Diplostatics::Diplostatics(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme) :
        BillMenuTab(location),
        box(box),

        currentPlayerId(PlayerData::NO_PLAYER),
        first(true),

        backPanel(),

        showOwnButton(rascUI::Location(SHOW_OWN_LOC), "Show self",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getTopLevel()->getMb()) {
                    getTopLevel()->afterEvent->addFunction([this](void) {
                        showPlayer(this->box.netIntf->getData()->getId());
                    });
                }
            }
        ),

        flag(rascUI::Location(FLAG_LOC)),

        colourPanel(rascUI::Location(TOP_RIGHT_SECTION_LOC)),
        playerNamePanel(rascUI::Location(PLAYER_NAME_PANEL_LOC)),
        playerNameLabel(rascUI::Location(PLAYER_NAME_PANEL_LOC)),

        behaviourPanel(rascUI::Location(BEHAVIOUR_PANEL_LOC)),
        #define X(id, name, defaultValue, antonym, verb) \
            rascUI::Label(rascUI::Location(BEHAVIOUR_LABEL_LOC(id))),
        behaviourLabels { TM_STORED_BEHAVIOUR_VALUES },
        #undef X

        capitalProvincePanel(rascUI::Location(CAPITAL_PROVINCE_LOC)),
        capitalProvinceLabel(rascUI::Location(CAPITAL_PROVINCE_LOC), "Capital province: None"),

        leftStatsPanel(rascUI::Location(LEFT_STATS_PANEL_LOC)),
        provincesLabel(rascUI::Location(LEFT_STATS_LABEL_LOC(0))),
        areasLabel(rascUI::Location(LEFT_STATS_LABEL_LOC(1))),
        continentsLabel(rascUI::Location(LEFT_STATS_LABEL_LOC(2))),
        landUseLabel(rascUI::Location(LEFT_STATS_LABEL_LOC(3))),

        rightManpowerPanel(rascUI::Location(RIGHT_MANPOWER_PANEL_LOC)),
        manpowerBalanceLabel(rascUI::Location(RIGHT_MANPOWER_LABEL_LOC(0))),
        totalManpowerLabel(rascUI::Location(RIGHT_MANPOWER_LABEL_LOC(1))),

        rightCurrencyPanel(rascUI::Location(RIGHT_CURRENCY_PANEL_LOC)),
        currencyIncomeLabel(rascUI::Location(RIGHT_CURRENCY_LABEL_LOC(0))),
        currencyExpenditureLabel(rascUI::Location(RIGHT_CURRENCY_LABEL_LOC(1))),
        currencyBalanceLabel(rascUI::Location(RIGHT_CURRENCY_LABEL_LOC(2))),
        cashLabel(rascUI::Location(RIGHT_CURRENCY_LABEL_LOC(3))) {

        add(&backPanel);
        add(&showOwnButton);
        add(&flag);
        add(&colourPanel);
        add(&playerNamePanel);
        add(&playerNameLabel);
        add(&behaviourPanel);
        for (rascUI::Label & label : behaviourLabels) {
            add(&label);
        }
        add(&capitalProvincePanel);
        add(&capitalProvinceLabel);
        add(&leftStatsPanel);
        add(&provincesLabel);
        add(&areasLabel);
        add(&continentsLabel);
        add(&landUseLabel);
        add(&rightManpowerPanel);
        add(&manpowerBalanceLabel);
        add(&totalManpowerLabel);
        add(&rightCurrencyPanel);
        add(&currencyIncomeLabel);
        add(&currencyExpenditureLabel);
        add(&currencyBalanceLabel);
        add(&cashLabel);
    }

    Diplostatics::~Diplostatics(void) {
    }

    void Diplostatics::updateStats(void) {
        // Ensure that when Diplostatics first opens, it is showing info about ourself.
        if (first) {
            currentPlayerId = box.netIntf->getData()->getId();
            first = false;
        }

        // Make sure the show self button is only active if we are not showing self.
        showOwnButton.setActive(currentPlayerId != box.netIntf->getData()->getId());

        PlayerData * data = box.common.playerDataSystem->idToDataChecked(currentPlayerId);
        if (data == NULL) {
            // Make the menus capable of displaying no player and unknown player.
            playerNameLabel.setText(currentPlayerId == PlayerData::NO_PLAYER ? "No player" : "Unknown player");
            colourPanel.setColour(wool::RGBA::GREY);
            for (rascUI::Label & label : behaviourLabels) {
                label.setText("");
            }
            provincesLabel.setText("");
            areasLabel.setText("");
            continentsLabel.setText("");
            landUseLabel.setText("");
            manpowerBalanceLabel.setText("");
            totalManpowerLabel.setText("");
            currencyIncomeLabel.setText("");
            currencyExpenditureLabel.setText("");
            currencyBalanceLabel.setText("");
            cashLabel.setText("");
        } else {
            // Display a player which *does* exist.
            playerNameLabel.setText(data->getName());
            colourPanel.setColour(data->getColourInt());
            flag.setFlag(data->getFlag());
            #define X(id, name, defaultValue, antonym, verb) \
                behaviourLabels[id].setText(data->get##name##Str());
            TM_STORED_BEHAVIOUR_VALUES
            #undef X

            char textBuffer[31];
            
            NUMBER_FORMAT_FILL(textBuffer, generic, 7,
                "Provinces: ", box.common.provincesStat->getProvinceCount(currentPlayerId))
            provincesLabel.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, generic, 7,
                "Areas: ", box.common.areasStat->getAreaCount(currentPlayerId));
            areasLabel.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, generic, 7,
                "Continents: ", box.common.continentsStat->getContinentCount(currentPlayerId));
            continentsLabel.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, landUseAvailable, 14,
                "Land use: ", data->getTotalLandUsed(), data->getTotalLandAvailable())
            landUseLabel.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, manpower, 9,
                "Manpower balance: ", data->getManpowerBalancePerTurn(), true)
            manpowerBalanceLabel.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, manpower, 9,
                "Total manpower: ", data->getTotalManpower(), false)
            totalManpowerLabel.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, currency, 9,
                "Currency income: ", data->getCurrencyIncomePerTurn(), false)
            currencyIncomeLabel.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, currency, 9,
                "Currency expenditure: ", data->getCurrencyExpenditurePerTurn(), false)
            currencyExpenditureLabel.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, currency, 9,
                "Currency balance: ", data->getCurrencyBalancePerTurn(), true)
            currencyBalanceLabel.setText(textBuffer);
            
            NUMBER_FORMAT_FILL(textBuffer, currency, 9,
                "Cash: ", data->getTotalCurrency(), false)
            cashLabel.setText(textBuffer);
        }
    }

    void Diplostatics::showPlayer(uint64_t playerId) {
        currentPlayerId = playerId;
        first = false;
        updateStats();
    }
}

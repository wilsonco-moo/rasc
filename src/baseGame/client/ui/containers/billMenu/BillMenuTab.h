/*
 * BillMenuTab.h
 *
 *  Created on: 20 Dec 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_BILLMENUTAB_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_BILLMENU_BILLMENUTAB_H_

#include <rascUI/base/Container.h>

namespace rasc {

    /**
     * A very simple base class which provides a pure virtual
     * updateStats method.
     * All bill menu tabs should inherit from this.
     */
    class BillMenuTab : public rascUI::Container {
    public:
        BillMenuTab(const rascUI::Location & location = rascUI::Location());
        virtual ~BillMenuTab(void);

        /**
         * This must be called by the BILL menu, when either:
         *  > Bill menu is opened, and this is the active tab.
         *  > This tab is switched to from another BILL menu tab.
         *  > A stat update happens while the bill menu is open, and
         *    this tab is active.
         * This method should update all the displayed stats in the
         * respective BILL menu tab.
         */
        virtual void updateStats(void) = 0;
    };
}

#endif

/*
 * PlayerColourBar.h
 *
 *  Created on: 18 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_PLAYERCOLOURBAR_PLAYERCOLOURBAR_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_PLAYERCOLOURBAR_PLAYERCOLOURBAR_H_

#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/base/Container.h>

#include "../../customComponents/ColourComponent.h"

namespace rascUI {
    class Theme;
}

namespace rasc {
    class RascBox;
    
    /**
     *
     */
    class PlayerColourBar : public rascUI::Container {
    private:
        RascBox & box;
        bool recalculateNeeded;
        rascUI::BackPanel backPanel;
        ColourComponent colourPanel;
        void * afterUpdateToken;
        
    public:
        PlayerColourBar(RascBox & box, rascUI::Theme * theme);
        virtual ~PlayerColourBar(void);
        
    protected:
        virtual void recalculate(const rascUI::Rectangle & parentSize) override;
        virtual void onDraw(void) override;
        
    public:
        /**
         * Must be run each time the modular topbar recalculates, since our
         * position depends on its width.
         */
        inline void notifyRecalculate(void) {
            recalculateNeeded = true;
        }
    };
}

#endif

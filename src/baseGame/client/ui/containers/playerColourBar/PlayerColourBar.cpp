/*
 * PlayerColourBar.cpp
 *
 *  Created on: 18 Jan 2021
 *      Author: wilson
 */

#include "PlayerColourBar.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#include "../../../../common/stats/UnifiedStatSystem.h"
#include "../../../../common/playerData/PlayerData.h"
#include "../../../RascClientNetIntf.h"
#include "../../../RascBox.h"

#define LAYOUT_THEME theme

#define OVERALL_LOC                                                 \
    SUB_MARGIN_R(RascBox::getClientSidePlayerListWidth(theme),      \
     SUB_MARGIN_L(box.uiGetModularTopbarWidth(),                    \
      PIN_T(COUNT_OUTBORDER_Y(1),                                   \
       GEN_FILL                                                     \
    )))

#define COLOUR_BAR_LOC  \
    SUB_BORDER_Y(       \
     GEN_FILL           \
    )

namespace rasc {

    PlayerColourBar::PlayerColourBar(RascBox & box, rascUI::Theme * theme) :
        rascUI::Container(),
        box(box),
        recalculateNeeded(true),
        backPanel(),
        colourPanel(rascUI::Location(COLOUR_BAR_LOC)),
        afterUpdateToken(box.common.statSystem->addAfterUpdateFunction([this](void) {
            // Update colour panel's colour on stat updates.
            colourPanel.setColour(this->box.netIntf->getData()->getColour());
        })) {
        add(&backPanel);
        add(&colourPanel);
    }
    
    PlayerColourBar::~PlayerColourBar(void) {
        box.common.statSystem->removeAfterUpdateFunction(afterUpdateToken);
    }
    
    void PlayerColourBar::recalculate(const rascUI::Rectangle & parentSize) {
        // Update location position/weight when recalculating.
        rascUI::Theme * theme = getTopLevel()->theme;
        location.setPositionAndWeight(OVERALL_LOC);
        
        recalculateNeeded = false;
        Container::recalculate(parentSize);
    }
    
    void PlayerColourBar::onDraw(void) {
        if (recalculateNeeded) {
            recalculate(getParent()->location.cache);
        }
        Container::onDraw();
    }
}

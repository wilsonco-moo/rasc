/*
 * NewClientSidePlayerList.cpp
 *
 *  Created on: 30 Nov 2018
 *      Author: wilson
 */

#include "ClientSidePlayerList.h"

#include <rascUI/base/TopLevelContainer.h>
#include <sockets/plus/message/Message.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/base/Component.h>
#include <rascUI/util/Location.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <wool/misc/RGBA.h>
#include <wool/font/Font.h>
#include <wool/misc/RGB.h>
#include <cstddef>
#include <cstdint>

#include "../../../../common/playerData/PlayerDataSystem.h"
#include "../../../../common/util/numeric/NumberFormat.h"
#include "../../../../common/util/definitions/IconDef.h"
#include "../../../../common/stats/UnifiedStatSystem.h"
#include "../../customComponents/PlayerPanel.h"
#include "../../../config/MapModeController.h"
#include "../../GameUIMenuDef.h"
#include "../../../RascBox.h"

// For Layout.h macros.
#define LAYOUT_THEME theme

// The location of the entire player list.
// We have a fixed width, allowing room for a scroll pane which can resize, and a height
// of one third of the screen area, and we attach to the top-right. Additional height is
// then added to allow for the top panel.
#define ENTIRE_CONTAINER_WIDTH (156 + COUNT_OUTBORDER_X(1))
#define ENTIRE_CONTAINER_LOC           \
    SUB_MARGIN_B(-TOP_PANEL_HEIGHT,    \
     SPLIT_T(0.33f,                    \
      PIN_R(ENTIRE_CONTAINER_WIDTH,    \
      GEN_FILL                         \
    )))

// The height and location of the top panel.
#define TOP_PANEL_HEIGHT COUNT_OUTBORDER_Y(1)
#define TOP_BACK_PANEL_LOC  \
    PIN_T(TOP_PANEL_HEIGHT, \
     GEN_FILL               \
    )

// Top panel buttons.
#define ICON_BUTTON_WIDTH (IconButton::getIconOnlyButtonWidth(theme))
#define ICON_BUTTONS_OVERALL_W (ICON_BUTTON_WIDTH * 2 + UI_BORDER * 2)
#define ICON_BUTTON_LOC(id)                            \
    MOVE_X((id - 1) * (ICON_BUTTON_WIDTH + UI_BORDER), \
     PIN_R(ICON_BUTTON_WIDTH,                          \
      SUB_BORDER(                                      \
       TOP_BACK_PANEL_LOC                              \
    )))

#define MODULAR_TOPBAR_BUTTON_LOC        \
    SUB_MARGIN_R(ICON_BUTTONS_OVERALL_W, \
     SUB_BORDER(                         \
      TOP_BACK_PANEL_LOC                 \
    ))

// Location of the scroll pane.
#define SCROLL_PANE_LOC                                         \
    SUB_BORDER_R(                                               \
     SUB_MARGIN_B(-UI_BORDER,                                   \
      SUB_MARGIN_TB(TOP_PANEL_HEIGHT, BOTTOM_BACK_PANEL_HEIGHT, \
       GEN_FILL                                                 \
    )))

// Height and location of the bottom panel.
#define BOTTOM_BACK_PANEL_HEIGHT \
    COUNT_OUTBORDER_Y(1)
#define BOTTOM_PANEL_LOC                \
    PIN_B(BOTTOM_BACK_PANEL_HEIGHT,     \
     GEN_FILL                           \
    )

// Bottom front panel.
#define BOTTOM_FRONT_PANEL_LOC  \
    SUB_BORDER(                 \
     BOTTOM_PANEL_LOC           \
    )

namespace rasc {

    // ---------------------- Player count panel ----------------------------------------
        #define PLAYER_COUNT_BUFFSIZE 5
        ClientSidePlayerList::PlayerCountPanel::PlayerCountPanel(const rascUI::Location & location) :
            rascUI::Component(location),
            playerCount(PLAYER_COUNT_BUFFSIZE, &NumberFormat::generic<PLAYER_COUNT_BUFFSIZE>) {
        }
        ClientSidePlayerList::PlayerCountPanel::~PlayerCountPanel(void) {
        }
        bool ClientSidePlayerList::PlayerCountPanel::shouldRespondToKeyboard(void) const {
            return false;
        }
        void ClientSidePlayerList::PlayerCountPanel::onDraw(void) {
            // Get the theme for convenience.
            rascUI::Theme * theme = getTopLevel()->theme;

            // Draw the FrontPanel background
            theme->drawFrontPanel(location);
            
            // Draw the number of players
            playerCount.draw(location, theme);

            // Draw the words "player" or "players"
            theme->customSetDrawColour(theme->customColourMainText);
            theme->customDrawTextNoMouse(location, theme->customTextCharWidth * PLAYER_COUNT_BUFFSIZE, 0.0f, 1.0f, 1.0f, playerCount == 1 ? "player" : "players");
        }

    // ----------------------------------------------------------------------------------

    ClientSidePlayerList::ClientSidePlayerList(RascBox & box, rascUI::Theme * theme) :
        rascUI::Container(rascUI::Location(ENTIRE_CONTAINER_LOC)),
        box(box),
        playerPanels(),
        shouldReassignPlayerPanels(true),
        backPanel(),
        topBackPanel(rascUI::Location(TOP_BACK_PANEL_LOC)),

        modularTopbarConfigButton(rascUI::Location(MODULAR_TOPBAR_BUTTON_LOC), IconDef::modularTopbar, "Topbar",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) this->box.uiOpenOverlayMenu(GameUIMenus::modularTopbarConfigMenu, true);
            }
        ),

        billButton(rascUI::Location(ICON_BUTTON_LOC(0)), IconDef::bill, "",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) this->box.uiOpenOverlayMenu(GameUIMenus::billMenu, true);
            }
        ),

        menuButton(rascUI::Location(ICON_BUTTON_LOC(1)), IconDef::menu, "",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) this->box.uiOpenOverlayMenu(GameUIMenus::gameMenu, true);
            }
        ),

        scrollPane(theme, rascUI::Location(SCROLL_PANE_LOC), false, true, false, true),
        bottomBackPanel(rascUI::Location(BOTTOM_PANEL_LOC)),
        bottomPanel(rascUI::Location(BOTTOM_FRONT_PANEL_LOC)) {

        add(&backPanel);
        add(&scrollPane);
        add(&topBackPanel);
        add(&modularTopbarConfigButton);
        add(&billButton);
        add(&menuButton);
        add(&bottomBackPanel);
        add(&bottomPanel);

        box.common.playerDataSystem->setOnUpdateFunc([this](void) {
            shouldReassignPlayerPanels = true;
            // Make sure the stat system also gets updated when the player data system gets an update.
            this->box.common.statSystem->forceUpdate();
        });
    }

    ClientSidePlayerList::~ClientSidePlayerList(void) {
    }

    void ClientSidePlayerList::reassignPlayerPanels(void) {
        // First synchronise the number of player panels with the number of players.
        size_t playerCount = box.common.playerDataSystem->getRascUpdatableChildCount();
        GLfloat bord = getTopLevel()->theme->uiBorder;
        if (playerPanels.size() < playerCount) {
            size_t newPlayers = playerCount - playerPanels.size();
            for (size_t i = 0; i < newPlayers; i++) {
                playerPanels.emplace_back(rascUI::Location(bord, 0.0f, -bord*2.0f, -bord, 0.0f, 0.0f, 1.0f, 1.0f), box, getTopLevel()->theme);
                scrollPane.contents.add(&playerPanels.back());
            }
        } else if (playerPanels.size() > playerCount) {
            size_t playerToRemove = playerPanels.size() - playerCount;
            for (size_t i = 0; i < playerToRemove; i++) {
                scrollPane.contents.remove(&playerPanels.back());
                playerPanels.pop_back();
            }
        }

        // Then assign data to all the player panels.
        std::list<PlayerPanel>::iterator panel = playerPanels.begin();
        for (std::pair<const uint64_t, RascUpdatable *> updatable : box.common.playerDataSystem->mapChildren()) {
            panel->setPlayerByData((PlayerData *)updatable.second);
            ++panel;
        }

        // Finally, update the player count panel.
        bottomPanel.playerCount = (int64_t)playerCount;
    }

    void ClientSidePlayerList::onDraw(void) {
        if (shouldReassignPlayerPanels) {
            getTopLevel()->afterEvent->addFunction([this](void) {
                reassignPlayerPanels();
            });
            shouldReassignPlayerPanels = false;
        }
        Container::onDraw();
    }
    
    void ClientSidePlayerList::setModularTopbarButtonSelected(void) {
        getTopLevel()->setKeyboardSelected(&modularTopbarConfigButton);
    }
    
    GLfloat ClientSidePlayerList::getWidth(rascUI::Theme * theme) {
        return ENTIRE_CONTAINER_WIDTH;
    }

    void ClientSidePlayerList::keyboardSelectMenuButton(void) {
        getTopLevel()->setKeyboardSelected(&menuButton);
    }

    void ClientSidePlayerList::keyboardSelectBillButton(void) {
        getTopLevel()->setKeyboardSelected(&billButton);
    }
}

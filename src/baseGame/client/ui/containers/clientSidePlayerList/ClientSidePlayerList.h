/*
 * NewClientSidePlayerList.h
 *
 *  Created on: 30 Nov 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_CLIENTSIDEPLAYERLIST_CLIENTSIDEPLAYERLIST_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_CLIENTSIDEPLAYERLIST_CLIENTSIDEPLAYERLIST_H_

#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/base/Container.h>
#include <list>

#include "../../customComponents/zeroPrefixedNumber/ZeroPrefixedNumberUnsigned.h"
#include "../../../ui/customComponents/icon/IconButton.h"

namespace rascUI {
    class Theme;
}

namespace simplenetwork {
    class Message;
}

namespace rasc {
    class RascBox;
    class PlayerPanel;
    class Location;
    class PlayerPanel;

    class ClientSidePlayerList : public rascUI::Container {
    private:
        // ---------------------- Player count panel ----------------------------------------

        class PlayerCountPanel : public rascUI::Component {
            friend class ClientSidePlayerList;
        private:
            ZeroPrefixedNumberUnsigned playerCount;
        public:
            PlayerCountPanel(const rascUI::Location & location = rascUI::Location());
            virtual ~PlayerCountPanel(void);
        protected:
            virtual bool shouldRespondToKeyboard(void) const override;
            virtual void onDraw(void) override;
        };

        // ----------------------------------------------------------------------------------

        // Our rasc box.
        RascBox & box;

        // This stores all of our player panels. A std::list is used here to avoid breaking container pointers with reallocations.
        std::list<PlayerPanel> playerPanels;

        // When this is set to true, we reassign all player panels from the PlayerDataSystem next frame.
        bool shouldReassignPlayerPanels;

        // --------------- Visual ----------------

        rascUI::BackPanel backPanel;
        rascUI::BackPanel topBackPanel;
        IconButton modularTopbarConfigButton, billButton, menuButton;
        rascUI::ScrollPane scrollPane;
        rascUI::BackPanel bottomBackPanel;
        PlayerCountPanel bottomPanel;

        // ---------------------------------------

        // DON'T ALLOW COPY OR COPY ASSIGNMENT, we own raw pointer data.
        ClientSidePlayerList(const ClientSidePlayerList & other);
        ClientSidePlayerList & operator = (const ClientSidePlayerList & other);

    private:
        // This sets all player panels from the data stored in the player data system.
        void reassignPlayerPanels(void);

    protected:
        // We override this so we can check for reassignment of player panels each frame.
        virtual void onDraw(void) override;

    public:
        ClientSidePlayerList(RascBox & box, rascUI::Theme * theme);
        virtual ~ClientSidePlayerList(void);
        
        void setModularTopbarButtonSelected(void);
        
        /**
         * Gets width of client side player list, used by other components for layout.
         */
        static GLfloat getWidth(rascUI::Theme * theme);
        
        /**
         * This should be called when the game menu closes, within a top level afterEvent function.
         */
        void keyboardSelectMenuButton(void);

        /**
         * This should be called when the bill menu closes, within a top level afterEvent function.
         */
        void keyboardSelectBillButton(void);
    };
}

#endif

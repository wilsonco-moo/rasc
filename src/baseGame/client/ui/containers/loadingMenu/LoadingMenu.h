/*
 * LoadingMenu.h
 *
 *  Created on: 7 Feb 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_LOADINGMENU_LOADINGMENU_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_LOADINGMENU_LOADINGMENU_H_

#include <rascUI/base/Container.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/Label.h>
#include "../../../config/MapModeController.h"
#include "../../customComponents/JobStatusComponent.h"


namespace rascUI {
    class Theme;
}

namespace rasc {

    class RascBox;
    class JobStatusComponent;

    /**
     * This class contains the simple UI shown during the loading room.
     */
    class LoadingMenu : public rascUI::Container {
    private:
        RascBox & box;

        rascUI::BackPanel backPanel;

        rascUI::FrontPanel titlePanel;
        rascUI::Label titleLabel;

        JobStatusComponent * jobStatusComponents[MapModes::MODES_COUNT];
        JobStatusComponent provincesTexture;
        JobStatusComponent gameInitialiser;

        // Don't allow copying: We hold raw resources.
        LoadingMenu(const LoadingMenu & other);
        LoadingMenu & operator = (const LoadingMenu & other);

    public:
        LoadingMenu(RascBox & box, rascUI::Theme * theme);
        virtual ~LoadingMenu(void);

        /**
         * This must be called when we enter the loading room. This sets up the job status components,
         * and it is assumed that by this point the textures are already being loaded.
         */
        void onChangeToLoadingRoom(void);
    };

}

#endif

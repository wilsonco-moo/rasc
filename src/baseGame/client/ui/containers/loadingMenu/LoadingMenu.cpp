/*
 * LoadingMenu.cpp
 *
 *  Created on: 7 Feb 2019
 *      Author: wilson
 */

#include "LoadingMenu.h"

#include <rascUI/base/Theme.h>
#include <rascUI/util/Layout.h>
#include <rascUI/util/Location.h>
#include <wool/texture/ThreadTexture.h>
#include "../../../../common/config/StaticConfig.h"
#include "../../../config/MapModes.h"
#include "../../../GlobalProperties.h"
#include "../../../RascBox.h"

#define LAYOUT_THEME theme

// The number of extra rows, (compared to the number of map modes). This currently includes the title row, provinces texture, and game initialiser.
#define EXTRA_ROWS 3

// The row where the title is, currently 0 = the top.
#define TITLE_ROW 0

// The row at which the map modes start, currently immediately below the title row.
#define MAP_MODES_ROW (TITLE_ROW + 1)

// The row at which the provinces texture status panel is
#define PROVINCES_ROW (MAP_MODES_ROW + MapModes::MODES_COUNT)

// The row at which the game initialiser status panel is
#define GAME_INITIALISER_ROW (PROVINCES_ROW + 1)

// The height of each row, (including and excluding gaps between them respectively).
// This allows extra borders around inside the job status components.
#define ROW_HEIGHT_B (UI_BHEIGHT + 3 * UI_BORDER)
#define ROW_HEIGHT (UI_BHEIGHT + 2 * UI_BORDER)

// The location of the loading menu, specified by its width and height.
#define LOADING_MENU_LOC    GEN_CENTRE_FIXED(600, (MapModes::MODES_COUNT + EXTRA_ROWS) * ROW_HEIGHT_B + UI_BORDER)

#define ROW_LOC(id)                                  \
    MOVE_Y(id * ROW_HEIGHT_B,                        \
     SETSIZE_Y(ROW_HEIGHT,                           \
      SUB_BORDER(                                    \
       LOADING_MENU_LOC                              \
    )))

namespace rasc {

    LoadingMenu::LoadingMenu(RascBox & box, rascUI::Theme * theme) :
        rascUI::Container(rascUI::Location(LOADING_MENU_LOC)),
        box(box),
        backPanel(),
        titlePanel(rascUI::Location(ROW_LOC(0))),
        titleLabel(rascUI::Location(ROW_LOC(TITLE_ROW)), "Loading textures..."),
        provincesTexture(rascUI::Location(ROW_LOC(PROVINCES_ROW)), theme),
        gameInitialiser(rascUI::Location(ROW_LOC(GAME_INITIALISER_ROW)), theme) {

        add(&backPanel);
        add(&titlePanel);
        add(&titleLabel);
        add(&gameInitialiser);

        for (MapMode i = 0; i < MapModes::MODES_COUNT; i++) {
            int rowId = i + MAP_MODES_ROW;
            JobStatusComponent * comp = new JobStatusComponent(rascUI::Location(ROW_LOC(rowId)), theme);
            add(comp);
            jobStatusComponents[i] = comp;
        }

        add(&provincesTexture);
    }

    LoadingMenu::~LoadingMenu(void) {
        for (MapMode i = 0; i < MapModes::MODES_COUNT; i++) {
            delete jobStatusComponents[i];
        }
    }

    void LoadingMenu::onChangeToLoadingRoom(void) {

        // Set the job of each job status component.
        for (MapMode i = 0; i < MapModes::MODES_COUNT; i++) {
            jobStatusComponents[i]->setJob(StaticConfig::MAP_TEXTURES[i],
                    box.globalProperties->threadQueue, box.mapModeTextures[i].getLoadJobId());
        }

        // Set the job for the provinces texture status component.
        provincesTexture.setJob("Provinces texture", box.globalProperties->threadQueue, box.provincesTexture->getLoadJobId());

        // Set the job for the game initialiser component.
        gameInitialiser.setJob("Load map data", box.globalProperties->threadQueue, box.getInitialiserJobId());
    }
}

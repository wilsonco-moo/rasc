/*
 * ClientDevelopmentConsole.h
 *
 *  Created on: 8 Dec 2019
 *      Author: wilson
 */

#include "../../../../common/config/ReleaseMode.h"
#ifdef RASC_DEVELOPMENT_MODE
#ifndef BASEGAME_CLIENT_UI_CONTAINERS_DEVELOPMENTCONSOLE_CLIENTDEVELOPMENTCONSOLE_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_DEVELOPMENTCONSOLE_CLIENTDEVELOPMENTCONSOLE_H_

#include <unordered_map>
#include <functional>
#include <string>
#include <list>
#include <map>

#include <rascUI/components/textEntry/BasicTextEntryBox.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/base/Container.h>

namespace rascUI {
    class Theme;
}
namespace rasc {
    class RascBox;

    /**
     * This class provides a simple console, which can be used during the
     * game for typing in commands which do certain things helpful during
     * development.
     * This entire class is enabled only during development mode.
     */
    class ClientDevelopmentConsole : public rascUI::Container {
    private:

        /**
         * A custom subclass of BasicTextEntryBox which run command when the user presses enter.
         */
        class CommandBox : public rascUI::BasicTextEntryBox {
        private:
            ClientDevelopmentConsole * console;
        public:
            CommandBox(const rascUI::Location & location, ClientDevelopmentConsole * console);
            virtual ~CommandBox(void);
        protected:
            virtual rascUI::NavigationAction onSelectionLockKeyRelease(rascUI::NavigationAction defaultAction, int key, bool special) override;
        };
        
        // Help text for each command.
        const static std::unordered_map<std::string, std::string> COMMAND_HELP;

        RascBox & box;
        bool isCurrentlyOpen, notOpenedYet;

        std::map<std::string, std::function<void(const std::vector<std::string> &)>> commands;

        rascUI::BackPanel backPanel;

        rascUI::BackPanel topBackPanel;
        rascUI::FrontPanel topFrontPanel;
        rascUI::Label topLabel;
        rascUI::Button closeButton;

        std::list<rascUI::Label> scrollLabels;
        rascUI::ScrollPane scrollPane;

        rascUI::BackPanel bottomBackPanel;
        CommandBox commandBox;
        rascUI::Button runButton;

    public:
        ClientDevelopmentConsole(RascBox & box, rascUI::Theme * theme);
        virtual ~ClientDevelopmentConsole(void);

        void toggleOpen(void);
        void show(void);
        void hide(void);
        bool isOpen(void) const;

        void println(const std::string & str);

        void runCommand(const std::string & str);

        void clearConsole(void);
    };
}

#endif
#endif


/*
 * ClientDevelopmentConsole.cpp
 *
 *  Created on: 8 Dec 2019
 *      Author: wilson
 */

#include "ClientDevelopmentConsole.h"
#ifdef RASC_DEVELOPMENT_MODE

#include <rascUI/base/TopLevelContainer.h>
#include <sockets/plus/message/Message.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <algorithm>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <chrono>
#include <vector>
#include <random>
#include <cctype>
#include <limits>

#include "../../../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../../../common/gameMap/mapElement/types/Province.h"
#include "../../../../common/gameMap/properties/trait/TraitDef.h"
#include "../../../../common/gameMap/properties/trait/Traits.h"
#include "../../../../common/update/updateTypes/MiscUpdate.h"
#include "../../../../common/gameMap/mapElement/types/Area.h"
#include "../../../../common/gameMap/properties/Properties.h"
#include "../../../../common/playerData/PlayerDataSystem.h"
#include "../../../../common/playerData/PlayerData.h"
#include "../../../../common/GameMessages.h"
#include "../../../gameMap/ClientGameMap.h"
#include "../../../../common/util/Misc.h"
#include "../../../RascClientNetIntf.h"
#include "../../../RascBox.h"

// A utility macro to convert a string to lowercase
#define MAKE_LOWERCASE(str) \
    std::transform((str).begin(), (str).end(), (str).begin(), [](unsigned char c){return std::tolower(c);});

// Replaces province names with [no name] if the province name is empty.
#define PROV_NAME(str) \
    ((str) == "" ? "[no name]" : (str))

// Joins the command string with spaces.
#define JOIN_ARGS_AFTER(cmdArgs, idAndAfter, outputStr) {           \
    std::stringstream inputStream;                                  \
    bool first = true;                                              \
    for (size_t i = (idAndAfter); i < (cmdArgs).size(); i++) {      \
        if (!first) inputStream << ' ';                             \
        first = false;                                              \
        inputStream << (cmdArgs)[i];                                \
    }                                                               \
    outputStr = inputStream.str();                                  \
}

// From the specified string containing a province ID, finds the province,
// otherwise returns and complains.
#define GET_PROV_FROM_STR(provIdStr, provinceIdVariableName, provinceVariableName)                                                                                                                       \
    uint32_t provinceIdVariableName = (uint32_t)std::strtoul((provIdStr).c_str(), NULL, 0);                                                                                                              \
    if (provinceIdVariableName >= this->box.common.map->getProvinceCount()) {                                                                                                                            \
        println(std::string("\nInvalid province id ")+std::to_string(provinceIdVariableName)+", there are only "+std::to_string(this->box.common.map->getProvinceCount())+" provinces in this map.");    \
        return;                                                                                                                                                                                          \
    }                                                                                                                                                                                                    \
    Province & provinceVariableName = this->box.common.map->getProvince(provinceIdVariableName);

// From the specified string containing a province ID, finds the province,
// otherwise returns and complains.
#define GET_TRAIT_FROM_STR(traitNameStr, traitVariableName)                               \
    proptype_t traitVariableName;                                                         \
    {                                                                                     \
        std::string lowerTraitName = traitNameStr;                                        \
        traitVariableName = Properties::internalNameToType(lowerTraitName);               \
        if (!Properties::isValidProperty(traitVariableName) ||                            \
            Properties::getBaseType(traitVariableName) != Properties::BaseTypes::trait) { \
            println(std::string("\nInvalid trait name: ") + traitNameStr + '.');          \
            return;                                                                       \
        }                                                                                 \
    }

// From the specified string containing a player ID, finds the player,
// otherwise returns and complains.
#define GET_PLAYER_FROM_STR(playerIdStr, playerIdVariableName, playerDataVariableName)                                  \
    uint64_t playerIdVariableName = (uint64_t)std::strtoul((playerIdStr).c_str(), NULL, 0);                             \
    PlayerData * playerDataVariableName = this->box.common.playerDataSystem->idToDataChecked(playerIdVariableName);     \
    if (playerDataVariableName == NULL) {                                                                               \
        println(std::string("\nNo player with an ID of ")+std::to_string(playerIdVariableName)+" currently exists.");   \
        return;                                                                                                         \
    }

// The number of characters wide that the console is.
#define TEXT_LINE_LENGTH 75

#define LAYOUT_THEME theme

#define OVERALL_WIDTH \
    (theme->customTextCharWidth * TEXT_LINE_LENGTH + UI_BORDER + COUNT_OUTBORDER_X(1))

#define OVERALL_LOC                          \
    PIN_R(OVERALL_WIDTH,                     \
     SUB_MARGIN_TBLR(8.0f, 8.0f, 0.0f, 8.0f, \
      SPLIT_B(2.0f / 3.0f,                   \
       GEN_FILL                              \
    )))

#define TOP_SECTION_HEIGHT \
    COUNT_OUTBORDER_Y(1)
#define TOP_SECTION_LOC \
    PIN_T(TOP_SECTION_HEIGHT, GEN_FILL)
#define TOP_SECTION_INNER_LOC \
    SUB_BORDER(TOP_SECTION_LOC)

#define CLOSE_BUTTON_WIDTH 64
#define TOP_PANEL_LOC \
    SUB_BORDERMARGIN_R(CLOSE_BUTTON_WIDTH, TOP_SECTION_INNER_LOC)
#define TOP_CLOSE_BUTTON_LOC \
    PIN_R(CLOSE_BUTTON_WIDTH, TOP_SECTION_INNER_LOC)

#define CENTRE_SECTION_LOC \
    SUB_BORDER_R(SUB_MARGIN_TB(TOP_SECTION_HEIGHT, BOTTOM_SECTION_HEIGHT, GEN_FILL))

#define BOTTOM_SECTION_HEIGHT \
    COUNT_OUTBORDER_Y(1)
#define BOTTOM_SECTION_LOC \
    PIN_B(BOTTOM_SECTION_HEIGHT, GEN_FILL)
#define BOTTOM_SECTION_INNER_LOC \
    SUB_BORDER(BOTTOM_SECTION_LOC)

#define RUN_BUTTON_WIDTH 48
#define CMD_BOX_LOC \
    SUB_BORDERMARGIN_R(RUN_BUTTON_WIDTH, BOTTOM_SECTION_INNER_LOC)
#define RUN_BUTTON_LOC \
    PIN_R(RUN_BUTTON_WIDTH, BOTTOM_SECTION_INNER_LOC)


namespace rasc {

    // ------------------- Command help --------------------------------------
    const std::unordered_map<std::string, std::string> ClientDevelopmentConsole::COMMAND_HELP({
        // 23456789012345678901234567890123456789012345678901234567890123456789012345

        {"help",
         "\n[help (no args)]\n"
         "  Shows a list of available commands.\n"
         "[help command]\n"
         "  Shows the help information about the specified command."
        },

        {"resetStats",
         "\n[resetStats (no args)]\n"
         "  Sets the total manpower and currency of all players to zero."
        },

        {"removeArmies",
         "\n[removeArmies (no args)]\n"
         "  Removes all armies from the map.\n"
         "[removeArmies playerId]\n"
         "  Removes armies from all provinces owned by the specified player (ID)."
        },

        {"removeArmiesProvince",
         "\n[removeArmiesProvince provinceId]\n"
         "  Removes armies from the specified province (ID)."
        },

        {"makeArmy",
         "\n[makeArmy provinceId unitCount]\n"
         "  Creates a new army of specified size, in the specified province. The army\n"
         "  owner is set to the current owner of the province. WARNING: This is a\n"
         "  client side command, and the game client has no way to allocate a truly\n"
         "  unique army ID. Instead it picks a random number, so this may cause an ID\n"
         "  collision extremely occasionally."
         "\n[makeArmy provinceId unitCount playerId]\n"
         "  Creates a new army of specified size, in the specified province. The army\n"
         "  owner is set to the provided player ID. WARNING: This is a client side\n"
         "  command, and the game client has no way to allocate a truly unique army\n"
         "  ID. Instead it picks a random number, so this may cause an ID collision\n"
         "  extremely occasionally."
        },

        {"listPlayers",
         "\n[listPlayers (no args)]\n"
         "  Shows our player ID, and prints a list of player name/ID pairs for all\n"
         "  other players, sorted alphabetically by player name."
        },

        {"searchPlayers",
         "\n[searchPlayers playerName...]\n"
         "  Shows the name and player ID of all players whose names match the\n"
         "  specified player name. The player name can include spaces, (so all\n"
         "  arguments are joined), and is case insensitive."
        },

        {"listProvinces",
         "\n[listProvinces (no args)]\n"
         "  Displays a list of provinces, (sorted alphabetically by their names),\n"
         "  alongside each province ID, and the area it is contained within."
        },

        {"searchProvinces",
         "\n[searchPlayers provinceName...]\n"
         "  Shows the name, province ID and area provinces with names matching the\n"
         "  specified province name. The province name can include spaces, (so all\n"
         "  arguments are joined), and is case insensitive."
        },

        {"provinceInfo",
         "\n[provinceInfo provinceId]\n"
         "  Displays information, (such as the owner player ID and name), for the\n"
         "  specified province (ID)."
        },

        {"claimProvince",
         "\n[claimProvince provinceId]\n"
         "  Changes the ownership of the specified province, so that it becomes owned\n"
         "  by us.\n"
         "[claimProvince provinceId playerId]\n"
         "  Changes the ownership of the specified province, so that it becomes owned\n"
         "  by the specified player."
        },

        {"abandonProvince",
         "\n[abandonProvince provinceId]\n"
         "  Changes the ownership of the specified province, so that it becomes owned\n"
         "  by no player, (i.e: Becomes an unowned province)."
        },

        {"changeColour",
         "\n[changeColour colour]\n"
         "  Changes the colour of our own player, to the one specified. The colour is\n"
         "  specified as a 4 byte number, in the format RGBA (most significant byte\n"
         "  to least significant byte). The colour is most convenient to specify as a\n"
         "  hex value, i.e: 0xaabbccff means red=aa, green=bb, blue=cc, alpha=ff.\n"
         "[changeColour colour playerId]\n"
         "  An alternate form of the command, where a player ID whose colour is\n"
         "  changed can be optionally specified."
        },

        {"addTrait",
         "\n[addTrait provinceId traitName]\n"
         "  Adds the trait, provided by trait name, to the specified province. This\n"
         "  has the same effect as using the \"changeTraitCount\" command with the\n"
         "  value one."
        },

        {"removeTrait",
         "\n[removeTrait provinceId traitName]\n"
         "  Removes the trait, provided by trait name, from the specified province.\n"
         "  This has the same effect as using the \"changeTraitCount\" command with the\n"
         "  value zero."
        },

        {"changeTraitCount",
         "\n[changeTraitCount provinceId traitName count]\n"
         "  Sets the count value for the specified trait, in the specified province.\n"
         "  If the value is set to one, this has the same effect as the \"addTrait\"\n"
         "  command. If the value is set to zero, this has the same effect as the\n"
         "  \"removeTrait\" command."
        },

        {"giveCurrency",
         "\n[giveCurrency playerId amount]\n"
         "  Gives the specified player the specified amount of currency."
        },

        {"giveManpower",
         "\n[giveManpower playerId amount]\n"
         "  Gives the specified player the specified amount of manpower."
        },

        {"clear",
         "\n[clear (no args)]\n"
         "  Empties all previous text from the console."
        }
    });
    
    // ----------------- Command box ------------
    ClientDevelopmentConsole::CommandBox::CommandBox(const rascUI::Location & location, ClientDevelopmentConsole * console) :
        rascUI::BasicTextEntryBox(location),
        console(console) {
    }
    ClientDevelopmentConsole::CommandBox::~CommandBox(void) {
    }
    rascUI::NavigationAction ClientDevelopmentConsole::CommandBox::onSelectionLockKeyRelease(rascUI::NavigationAction defaultAction, int key, bool special) {
        if (defaultAction == rascUI::NavigationAction::enter) {
            console->runCommand(getText());
            setText("");
            repaint();
        }
        return rascUI::BasicTextEntryBox::onSelectionLockKeyRelease(defaultAction, key, special);
    }

    // ------------------------------------------

    ClientDevelopmentConsole::ClientDevelopmentConsole(RascBox & box, rascUI::Theme * theme) :
        rascUI::Container(rascUI::Location(OVERALL_LOC)),
        box(box),
        isCurrentlyOpen(false),
        notOpenedYet(true),

        // ---------------------------------- Commands ---------------------------
        commands({

            {"help", [this](const std::vector<std::string> & args) {
                if (args.size() > 2) {
                    println("\nThe help command requires either no arguments, or a single argument.");
                } else if (args.size() == 2) {
                    std::unordered_map<std::string, std::string>::const_iterator iter = COMMAND_HELP.find(args[1]);
                    if (iter == COMMAND_HELP.end()) {
                        println(std::string("\nCannot find help about command \"")+args[1]+"\".");
                    } else {
                        println(iter->second);
                    }
                } else {
                    std::stringstream str;
                    str << "\nFor help about a specific command, type \"help command\".\nAvailable commands:\n";
                    bool first = true;
                    for (const std::pair<const std::string, std::function<void(const std::vector<std::string> &)>> & pair : commands) {
                        if (!first) str << ", ";
                        first = false;
                        str << pair.first;
                    }
                    str << '.';
                    println(str.str());
                }
            }},

            {"resetStats", [this](const std::vector<std::string> & args) {
                simplenetwork::Message msg(ClientToServer::miscUpdate);
                for (const std::pair<const uint64_t, RascUpdatable *> & pair : this->box.common.playerDataSystem->mapChildren()) {
                    PlayerData * playerData = (PlayerData *)pair.second;
                    playerData->changeTotalStats(MiscUpdate::client(msg), PlayerStatArr(), PlayerStatChangeReasons::developmentConsole);
                }
                this->box.netIntf->send(msg);
                println("\nReset manpower and currency of all players.");
            }},

            {"removeArmies", [this](const std::vector<std::string> & args) {
                if (args.size() == 1) {
                    simplenetwork::Message msg(ClientToServer::miscUpdate);
                    for (Province * province : this->box.map->getProvinces()) {
                        for (const Army & army : province->armyControl.getArmies()) {
                            province->armyControl.removeArmy(MiscUpdate::client(msg), army.getArmyId());
                        }
                    }
                    this->box.netIntf->send(msg);
                    println("\nRemoved all armies from the map.");
                } else if (args.size() == 2) {
                    uint64_t playerId = std::strtoul(args[1].c_str(), NULL, 0);
                    if (this->box.common.playerDataSystem->childForwardLookupChecked(playerId) == NULL) {
                        println(std::string("\nCannot find player ID ")+std::to_string(playerId)+".");
                        return;
                    }
                    simplenetwork::Message msg(ClientToServer::miscUpdate);
                    for (Province * province : this->box.map->getProvinces()) {
                        if (province->getProvinceOwner() == playerId) {
                            for (const Army & army : province->armyControl.getArmies()) {
                                province->armyControl.removeArmy(MiscUpdate::client(msg), army.getArmyId());
                            }
                        }
                    }
                    this->box.netIntf->send(msg);
                    println(std::string("\nRemoved all armies from provinces owned by player ID ")+std::to_string(playerId));
                } else {
                    println("\nToo many arguments to removeArmies command. See help.");
                }
            }},

            {"removeArmiesProvince", [this](const std::vector<std::string> & args) {
                if (args.size() != 2) {
                    println("\nPlease provide one argument: Province id.");
                    return;
                }
                GET_PROV_FROM_STR(args[1], provinceId, province)
                simplenetwork::Message msg(ClientToServer::miscUpdate);
                for (const Army & army : province.armyControl.getArmies()) {
                    province.armyControl.removeArmy(MiscUpdate::client(msg), army.getArmyId());
                }
                this->box.netIntf->send(msg);
                println(std::string("\nRemoved all armies from province id ")+std::to_string(provinceId)+", "+PROV_NAME(province.getName())+".");
            }},

            {"makeArmy", [this](const std::vector<std::string> & args) {
                if (args.size() != 3 && args.size() != 4) {
                    println("\nPlease provide either three of four arguments: Province id, unit count and\noptionally player ID.");
                    return;
                }

                // Read province, unit count and player.
                GET_PROV_FROM_STR(args[1], provinceId, province)
                uint64_t unitCount = (uint64_t)std::strtoul(args[2].c_str(), NULL, 0);
                uint64_t playerId;
                PlayerData * playerData;
                if (args.size() == 4) {
                    GET_PLAYER_FROM_STR(args[3], player, data)
                    playerId = player; playerData = data;
                } else {
                    playerId = province.getProvinceOwner();
                    playerData = this->box.common.playerDataSystem->idToData(playerId);
                }
                
                // Pick ID from top half of available 64 bit IDs, using Misc's time-seeded
                // random (RascBox (client) doesn't have a random number generator).
                // These IDs are very unlikely to be used, and very unlikely to collide.
                // Allocate ID, create army, send misc update, print log message.
                constexpr uint64_t maxInt = std::numeric_limits<uint64_t>::max();
                const uint64_t randomArmyId = Misc::timeSeededRandom(maxInt / 2u, maxInt);
                
                Army army(playerData, unitCount, this->box.common.turnCount, randomArmyId);
                province.armyControl.placeArmy(MiscUpdate::clientAuto(this->box.netIntf), army);
                println(std::string("\nPlaced army in province ")+province.getName()+", with "+std::to_string(unitCount)+" units, ID: "+std::to_string(randomArmyId)+", owner player: "+playerData->getName()+".");
            }},

            {"listPlayers", [this](const std::vector<std::string> & args) {
                std::stringstream str;
                str << "\nYour player ID: " << this->box.netIntf->getData()->getId() << ", other players:\n";
                std::map<std::string, PlayerData *> playerDatas;
                for (const std::pair<const uint64_t, RascUpdatable *> & pair : this->box.common.playerDataSystem->mapChildren()) {
                    std::string name = ((PlayerData *)pair.second)->getName();
                    MAKE_LOWERCASE(name)
                    playerDatas.emplace(std::move(name), (PlayerData *)pair.second);
                }
                bool first = true;
                for (const std::pair<const std::string, PlayerData *> & pair : playerDatas) {
                    if (!first) str << '\n';
                    first = false;
                    str << pair.second->getName() << ", id: " << pair.second->getId();
                }
                println(str.str());
            }},

            {"searchPlayers", [this](const std::vector<std::string> & args) {
                if (args.size() < 2) {
                    println("\nPlease provide (as arguments) player name (case insensitive).");
                    return;
                }
                std::string inputName;
                JOIN_ARGS_AFTER(args, 1, inputName)
                std::string searchName = inputName;
                MAKE_LOWERCASE(searchName)
                std::stringstream str;
                str << "\nPlayers with names matching " << inputName << ":\n";
                bool first = true;
                for (const std::pair<const uint64_t, RascUpdatable *> & pair : this->box.common.playerDataSystem->mapChildren()) {
                    PlayerData * data = (PlayerData *)pair.second;
                    std::string name = data->getName();
                    MAKE_LOWERCASE(name)
                    if (name == searchName) {
                        if (!first) str << '\n';
                        first = false;
                        str << data->getName() << ", id: " << data->getId();
                    }
                }
                if (first) {
                    println(std::string("\nNo players with names matching ")+inputName+'.');
                } else {
                    println(str.str());
                }
            }},

            {"listProvinces", [this](const std::vector<std::string> & args) {
                std::stringstream str;
                str << '\n';
                std::map<std::string, const Province *> provinces;
                for (const Province * prov : this->box.map->getProvinces()) {
                    std::string name = prov->getName();
                    MAKE_LOWERCASE(name)
                    provinces.emplace(std::move(name), prov);
                }
                bool first = true;
                for (const std::pair<const std::string, const Province *> & pair : provinces) {
                    if (!first) str << '\n';
                    first = false;
                    const std::string & areaName = this->box.map->getAreaRaw(pair.second->getAreaId()).getName();
                    str << PROV_NAME(pair.second->getName())
                        << ", id: " << pair.second->getId() << ", area: " << PROV_NAME(areaName);
                }
                println(str.str());
            }},

            {"searchProvinces", [this](const std::vector<std::string> & args) {
                if (args.size() < 2) {
                    println("\nPlease provide (as arguments) province name (case insensitive).");
                    return;
                }
                std::string inputName;
                JOIN_ARGS_AFTER(args, 1, inputName)
                std::string searchName = inputName;
                MAKE_LOWERCASE(searchName)
                std::stringstream str;
                str << "\nProvinces with names matching " << inputName << ":\n";
                bool first = true;
                for (Province * province : this->box.common.map->getProvinces()) {
                    std::string name = province->getName();
                    MAKE_LOWERCASE(name)
                    if (name == searchName) {
                        if (!first) str << '\n';
                        first = false;
                        const std::string & areaName = this->box.map->getAreaRaw(province->getAreaId()).getName();
                        str << PROV_NAME(province->getName())
                        << ", id: " << province->getId() << ", area: " << PROV_NAME(areaName);
                    }
                }
                if (first) {
                    println(std::string("\nNo provinces with names matching ")+inputName+'.');
                } else {
                    println(str.str());
                }
            }},

            {"provinceInfo", [this](const std::vector<std::string> & args) {
                if (args.size() != 2) {
                    println("\nPlease provide one argument: Province id.");
                    return;
                }
                GET_PROV_FROM_STR(args[1], provinceId, province)
                std::string ownerPlayerName;
                if (province.getProvinceOwner() == PlayerData::NO_PLAYER) {
                    ownerPlayerName = "[None]";
                } else {
                    PlayerData * data = this->box.common.playerDataSystem->idToDataChecked(province.getProvinceOwner());
                    if (data == NULL) {
                        ownerPlayerName = "[UNKNOWN?]";
                    } else {
                        ownerPlayerName = data->getName();
                    }
                }
                println(
                    std::string("\nProvince: ")+province.getName()+
                    "\n  Province ID: "+std::to_string(provinceId)+
                    "\n  Owner player: "+ownerPlayerName+
                    "\n  Owner player ID: "+(province.getProvinceOwner() == PlayerData::NO_PLAYER ? "[None]" : std::to_string(province.getProvinceOwner()))+
                    "\n  Battle ongoing: "+(province.battleControl->isBattleOngoing() ? "Yes" : "No")
                );
            }},

            {"playerInfo", [this](const std::vector<std::string> & args) {
                if (args.size() != 2) {
                    println("\nPlease provide one argument: Player id.");
                    return;
                }
                GET_PLAYER_FROM_STR(args[1], playerId, playerData)
                println(
                    std::string("\nPlayer: ")+playerData->getName()+
                    "\n  Player ID: "+std::to_string(playerId)+
                    "\n  Total manpower: "+std::to_string(playerData->getTotalManpower())+
                    "\n  Manpower expenditure / turn: "+std::to_string(playerData->getManpowerExpenditurePerTurn())+
                    "\n  Manpower balance / turn: "+std::to_string(playerData->getManpowerBalancePerTurn())+
                    "\n  Total currency: "+std::to_string(playerData->getTotalCurrency())+
                    "\n  Currency expenditure / turn: "+std::to_string(playerData->getCurrencyExpenditurePerTurn())+
                    "\n  Currency balance / turn: "+std::to_string(playerData->getCurrencyBalancePerTurn())+
                    "\n  Aggressiveness: "+std::to_string(playerData->getWeights().getAggressiveness())+
                    "\n  Extravagance: "+std::to_string(playerData->getWeights().getExtravagance())+
                    "\n  Focus: "+std::to_string(playerData->getWeights().getFocus())+
                    "\n  Cautiousness: "+std::to_string(playerData->getWeights().getCautiousness())
                );
            }},

            {"claimProvince", [this](const std::vector<std::string> & args) {
                PlayerData * data;
                if (args.size() == 2) {
                    data = this->box.netIntf->getData();
                } else if (args.size() == 3) {
                    GET_PLAYER_FROM_STR(args[2], otherPlayerId, otherPlayerData)
                    data = otherPlayerData;
                } else {
                    println("\nThe command claimProvince requires either one or two arguments: Province\nID, and optionally a player ID.");
                    return;
                }
                GET_PROV_FROM_STR(args[1], provinceId, province)
                simplenetwork::Message msg(ClientToServer::miscUpdate);
                province.claimByPlayerData(MiscUpdate::client(msg), data);
                this->box.netIntf->send(msg);
                println(std::string("\nClaimed province ")+province.getName()+" in the name of player "+data->getName()+'.');
            }},

            {"abandonProvince", [this](const std::vector<std::string> & args) {
                if (args.size() != 2) {
                    println("\nPlease provide one argument: Province id.");
                    return;
                }
                GET_PROV_FROM_STR(args[1], provinceId, province)
                simplenetwork::Message msg(ClientToServer::miscUpdate);
                province.claimUnowned(MiscUpdate::client(msg));
                this->box.netIntf->send(msg);
                println(std::string("\nAbandoned province ")+province.getName()+'.');
            }},

            {"changeColour", [this](const std::vector<std::string> & args) {
                PlayerData * data;
                if (args.size() == 2) {
                    data = this->box.netIntf->getData();
                } else if (args.size() == 3) {
                    GET_PLAYER_FROM_STR(args[2], otherPlayerId, otherPlayerData)
                    data = otherPlayerData;
                } else {
                    println("\nThe command claimProvince requires either one or two arguments: Province\nID, and optionally a player ID.");
                    return;
                }
                uint32_t newColour = (uint32_t)std::strtoul(args[1].c_str(), NULL, 0);
                this->box.netIntf->requestColourChange(data->getId(), newColour);
                println(std::string("\nRequested colour change of player ")+data->getName()+'.');
            }},

            {"addTrait", [this](const std::vector<std::string> & args) {
                if (args.size() != 3) {
                    println("\nThe command addTrait requires two arguments: Province ID and trait.");
                    return;
                }
                GET_PROV_FROM_STR(args[1], provinceId, province)
                GET_TRAIT_FROM_STR(args[2], trait)
                province.properties.addTrait(MiscUpdate::clientAuto(this->box.netIntf), trait);
                println(std::string("\nAdded trait ") + args[2] + " to province " + province.getName() + '.');
            }},

            {"removeTrait", [this](const std::vector<std::string> & args) {
                if (args.size() != 3) {
                    println("\nThe command removeTrait requires two arguments: Province ID and trait.");
                    return;
                }
                GET_PROV_FROM_STR(args[1], provinceId, province)
                GET_TRAIT_FROM_STR(args[2], trait)
                province.properties.removeTrait(MiscUpdate::clientAuto(this->box.netIntf), trait);
                println(std::string("\nRemoved trait ") + args[2] + " from province " + province.getName() + '.');
            }},

            {"changeTraitCount", [this](const std::vector<std::string> & args) {
                if (args.size() != 4) {
                    println("\nThe command changeTraitCount requires three arguments: Province ID, trait and count.");
                    return;
                }
                GET_PROV_FROM_STR(args[1], provinceId, province)
                GET_TRAIT_FROM_STR(args[2], trait)
                provstat_t oldCount = province.properties.getTraitCount(trait),
                           newCount = (provstat_t)std::strtol(args[3].c_str(), NULL, 0);
                province.properties.changeTraitCount(MiscUpdate::clientAuto(this->box.netIntf), trait, newCount);
                println(std::string("\nChanged count of trait ") + args[2] + " from " + std::to_string(oldCount) + " to " + std::to_string(newCount) + " in province " + province.getName() + '.');
            }},

            {"giveCurrency", [this](const std::vector<std::string> & args) {
                if (args.size() != 3) {
                    println("\nPlease provide two arguments: Player id and amount of currency.");
                    return;
                }
                GET_PLAYER_FROM_STR(args[1], playerId, playerData)
                provstat_t amount = (provstat_t)std::strtol(args[2].c_str(), NULL, 0);
                // Note: Multiply currency value by 100, since internally currency is stored as hundredths.
                playerData->addTotalCurrency(MiscUpdate::clientAuto(this->box.netIntf), amount * 100, PlayerStatChangeReasons::developmentConsole);
                println(std::string("\nGiven player ")+playerData->getName()+" "+std::to_string(amount)+" currency.");
            }},

            {"giveManpower", [this](const std::vector<std::string> & args) {
                if (args.size() != 3) {
                    println("\nPlease provide two arguments: Player id and amount of manpower.");
                    return;
                }
                GET_PLAYER_FROM_STR(args[1], playerId, playerData)
                provstat_t amount = (provstat_t)std::strtol(args[2].c_str(), NULL, 0);
                playerData->addTotalManpower(MiscUpdate::clientAuto(this->box.netIntf), amount, PlayerStatChangeReasons::developmentConsole);
                println(std::string("\nGiven player ")+playerData->getName()+" "+std::to_string(amount)+" manpower.");
            }},

            {"clear", [this](const std::vector<std::string> & args) {
                clearConsole();
            }}

        }),
        
        backPanel(),

        topBackPanel(rascUI::Location(TOP_SECTION_LOC)),
        topFrontPanel(rascUI::Location(TOP_PANEL_LOC)),
        topLabel(rascUI::Location(TOP_PANEL_LOC), "Rasc Development Console"),
        closeButton(rascUI::Location(TOP_CLOSE_BUTTON_LOC), "Close",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) toggleOpen();
            }
        ),

        scrollLabels(),
        scrollPane(theme, rascUI::Location(CENTRE_SECTION_LOC), false, true, false, false),

        bottomBackPanel(rascUI::Location(BOTTOM_SECTION_LOC)),
        commandBox(rascUI::Location(CMD_BOX_LOC), this),
        runButton(rascUI::Location(RUN_BUTTON_LOC), "Run",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) {
                    runCommand(commandBox.getText());
                    commandBox.setText("");
                }
            }
        ) {

        add(&backPanel);
        add(&scrollPane);

        add(&topBackPanel);
        add(&topFrontPanel);
        add(&topLabel);
        add(&closeButton);

        add(&bottomBackPanel);
        add(&commandBox);
        add(&runButton);

        setVisible(false);
    }

    ClientDevelopmentConsole::~ClientDevelopmentConsole(void) {
    }

    void ClientDevelopmentConsole::toggleOpen(void) {
        if (isCurrentlyOpen) {
            hide();
        } else {
            show();
        }
    }
    
    void ClientDevelopmentConsole::show(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            if (notOpenedYet) {
                println(
                    "640KB OK\n\n"
                    "RascOS version PS/2.1\n"
                    "125752 Bytes free\n\n"
                    "Type \"help\" for a list of available commands."
                );
                notOpenedYet = false;
            }
            setVisible(true);
            isCurrentlyOpen = true;
        });
    }
    
    void ClientDevelopmentConsole::hide(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            setVisible(false);
            isCurrentlyOpen = false;
        });
    }
    
    bool ClientDevelopmentConsole::isOpen(void) const {
        return isCurrentlyOpen;
    }

    void ClientDevelopmentConsole::println(const std::string & printString) {
        getTopLevel()->afterEvent->addFunction([this, printString](void) {
            // Split the string into lines on newline character.
            std::vector<std::string> lineStrings;
            lineStrings.push_back(std::string());
            for (char c : printString) {
                if (c == '\n') {
                    lineStrings.push_back(std::string());
                } else {
                    lineStrings.back().push_back(c);
                }
            }

            // Print each line, with line wrapping.
            for (const std::string & str : lineStrings) {
                size_t lines = ((size_t)std::max((long long)str.length() - 1ll, 0ll)) / TEXT_LINE_LENGTH + 1;
                for (size_t i = 0; i < lines; i++) {
                    size_t upto = i * TEXT_LINE_LENGTH;
                    std::string line = str.substr(upto, std::min(str.length() - upto, (size_t)TEXT_LINE_LENGTH));
                    scrollLabels.emplace_back(rascUI::Location(), std::move(line));
                    scrollPane.contents.add(&scrollLabels.back());
                }
            }

            // Scroll to the bottom.
            scrollPane.contents.scrollSlowlyToEnd();
        });
    }

    void ClientDevelopmentConsole::runCommand(const std::string & str) {
        // Split str into a list of things.
        const std::vector<std::string> args = Misc::splitString(str, ' ');
        
        // Find the command: Run it if it exists.
        auto iter = commands.find(args.front());
        if (iter == commands.end()) {
            println(std::string("\nUnknown command \"")+args.front()+"\".\nType \"help\" for a list of available commands.");
        } else {
            iter->second(args);
        }
    }

    void ClientDevelopmentConsole::clearConsole(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            scrollPane.contents.clear();
            scrollLabels.clear();
            scrollPane.contents.scrollSlowlyToStart();
            scrollPane.contents.scrollToStart();
        });
    }
}

#endif

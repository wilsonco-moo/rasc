/*
 * BuildMenu.h
 *
 *  Created on: 28 Feb 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_BUILDMENU_BUILDMENU_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_BUILDMENU_BUILDMENU_H_

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/util/EmplaceVector.h>
#include <rascUI/util/EmplaceArray.h>
#include <rascUI/base/Container.h>
#include <vector>
#include <array>

#include "../../customComponents/zeroPrefixedNumber/ZeroPrefixedNumberProvstat.h"
#include "../../../../common/gameMap/properties/trait/TraitDef.h"
#include "../../../../common/update/updateTypes/CheckedUpdate.h"
#include "../../../../common/gameMap/properties/PropertyDef.h"
#include "../../customComponents/icon/IconToggleButton.h"
#include "../../customComponents/icon/IconLabel.h"
#include "../../customComponents/FancyTextDisplay.h"

namespace rascUI {
    class Theme;
}

namespace rasc {
    class Province;
    class RascBox;

    /**
     *
     */
    class BuildMenu : public rascUI::Container {
    private:
        class CategoryUI {
        private:
            // Each button, when clicked, shows its specified building, and updates our selected
            // building for when the user clicks back to this tab.
            // Id is required as a toggle button id to provide to the toggle button series.
            class BuildingButton : public rascUI::ToggleButton {
            private:
                CategoryUI * categoryUI;
                proptype_t building;
            public:
                BuildingButton(rascUI::Theme * theme, rascUI::ToggleButtonSeries * series, unsigned int id, CategoryUI * categoryUI, proptype_t building);
                virtual ~BuildingButton(void);
                virtual void onToggleButtonSelect(GLfloat viewX, GLfloat viewY, int button) override;
            };
            
        public:
            BuildMenu * buildMenu;
            TraitDef::BuildingCategory category;
            proptype_t selectedBuilding;

            IconToggleButton titleButton;
            rascUI::ScrollPane scrollPane;
            rascUI::ToggleButtonSeries buildingButtonSeries;
            rascUI::EmplaceVector<BuildingButton> buildingButtons;
            CategoryUI(rascUI::Theme * theme, BuildMenu * buildMenu, TraitDef::BuildingCategory category, unsigned int iconId, unsigned int id, const std::string & title);
        private:
            // Rebuilds our building buttons to show the specified buildings.
            // If enableUnaffordable is set to true, buildings we cannot afford are selectable.
            void rebuildBuildingButtons(const std::vector<proptype_t> & buildings, bool enableUnaffordable);
        public:
            // Re-builds our buildings to show available buildings in the province.
            // If the province is null, this shows all buildings in our category.
            // If enableUnaffordable is set to true, buildings we cannot afford are selectable.
            void rebuildToFilterProvince(Province * province, bool enableUnaffordable);
            // Returns true if we are showing no buildings at all (i.e: not even any buildings we can't afford).
            bool isCompletelyEmpty(void) const;
        };
        
        class CostLabel : public IconLabel {
        private:
            ZeroPrefixedNumberProvstat value;
        public:
            CostLabel(const rascUI::Location & location, unsigned int iconId, std::string text,
                      unsigned int bufferSize, ZeroPrefixedNumberProvstat::FormatFunc formatFunc);
            virtual ~CostLabel(void);
        protected:
            virtual void onDraw(void) override;
        public:
            void setValue(provstat_t newValue);
        };

        RascBox & box;

        // Top section
        rascUI::BackPanel topBackPanel;
        rascUI::FrontPanel titlePanel;
        rascUI::Label titleLabel;
        rascUI::Button clearButton;
        rascUI::Button closeButton;
        rascUI::ToggleButtonSeries categorySeries;

        // Middle section (right)
        rascUI::BackPanel middleBackPanel;
        rascUI::BackPanel buildingNameBackPanel;
        rascUI::FrontPanel buildingNameFrontPanel;
        rascUI::Label buildingNameLabel;
        rascUI::FrontPanel buildingInfoPanel;
        FancyTextDisplay buildingInfoDisplay;

        // Bottom section
        rascUI::BackPanel bottomBackPanel;
        rascUI::ToggleButtonSeries enableUnaffordableSeries;
        IconToggleButton enableUnaffordableButton;
        rascUI::FrontPanel costPanel;
        CostLabel costLabel;
        CostLabel landLabel;
        rascUI::ToggleButtonSeries buildSeries;
        IconToggleButton demolishButton, buildButton;
        
        // Our currently selected building category.
        TraitDef::BuildingCategory currentCategory;
        
        // Whether unaffordable buildings are enabled.
        bool enableUnaffordable;
        
        void * afterUpdateToken;
        
        // Data exchanged with provinces (see accessors at the bottom).
        
        // The current building we're targeting.
        proptype_t targetBuilding;
        // The minimum and maximum values for the building amount, (useful for stackable buildings).
        provstat_t buildingQuantityMin, buildingQuantityMax;
        // If this is set to true, then all provinces should display as blocked.
        // This could be set to true if, for example, we cannot afford the building.
        bool isForceBlocked;
        // The province which is currently being filtered, or NULL if none is being filtered.
        Province * filteredProvince;
        
        // Category UI
        constexpr static unsigned int CATEGORIES_COUNT = 4;
        rascUI::EmplaceArray<CategoryUI, CATEGORIES_COUNT> categoryUIs;
        
    public:
        BuildMenu(RascBox & box, rascUI::Theme * theme);
        virtual ~BuildMenu(void);

    private:
        // Sets "filteredProvince" to specified (if we own it! otherwise NULL),
        // then re-shows current building category to rebuild data, and updates
        // the filter UI (except in the case where we're switching from no filter to no filter).
        // Then, always updates province display.
        void setFilteredProvince(Province * province);
        
        // Switches building category. Must be called outside main event.
        // Sets "currentCategory", updates visibility of categories, tells the
        // appropriate category to rebuild buildings to filter the current filter province,
        // sets selected building to whatever the category decides is currently selected.
        void showBuildingCategory(TraitDef::BuildingCategory category);

        // Updates the build menu display to show the specified building.
        // To update province colour display, use updateProvinceDisplay.
        void setSelectedBuilding(proptype_t trait);

        // Updates the colours of all provinces, based on our currently selected building.
        void updateProvinceDisplay(void);
        
        void internalShow(void);
        void internalHide(void);

    public:
        void show(void);
        void hide(void);
        void toggle(void);
        bool isOpen(void) const;

        /**
         * This should be called whenever the user left clicks on a province, while the build
         * menu is open.
         */
        void onLeftClickProvince(Province * province);
        
        /**
         * This should be called whenever the user right clicks on a province, while the build
         * menu is open.
         */
        void onRightClickProvince(Province * province);
        
        /**
         * Requests to demolish the building, handling the special case for
         * forts/garrisons. Count is how many are demolished (e.g: for stackable traits),
         * and is -1 by default.
         */
        static void requestDemolishBuilding(CheckedUpdate update, Province & province, proptype_t targetBuilding, provstat_t count = -1);
        
        // ---------------------- Accessors for provinces -------------------------
        
        /**
         * Gets the building type which is currently being targeted. Provinces can use this to
         * display whether it can be built there.
         */
        inline proptype_t getTargetBuilding(void) const {
            return targetBuilding;
        }
        
        /**
         * Minimum building quantity across relevant provinces, of this building. Provinces can
         * use this to display stackable building colours.
         */
        inline provstat_t getMinBuildingQuantity(void) const {
            return buildingQuantityMin;
        }
        
        /**
         * Maximum building quantity across relevant provinces, of this building. Provinces can
         * use this to display stackable building colours.
         */
        inline provstat_t getMaxBuildingQuantity(void) const {
            return buildingQuantityMax;
        }
        
        /**
         * If this is set to true, then all provinces should display as blocked.
         */
        inline bool getIsForceBlocked(void) const {
            return isForceBlocked;
        }
        
        /**
         * The province which is currently being filtered, or NULL if none is being filtered.
         * If this is set, provinces should skew colours to draw attention to the filtered province.
         */
        inline Province * getFilteredProvince(void) const {
            return filteredProvince;
        }
    };
}

#endif

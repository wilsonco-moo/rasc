/*
 * BuildMenu.cpp
 *
 *  Created on: 28 Feb 2020
 *      Author: wilson
 */

#include "BuildMenu.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#include "../../../../common/gameMap/provinceUtil/battle/ProvinceBattleController.h"
#include "../../../../common/gameMap/mapElement/types/Province.h"
#include "../../../../common/gameMap/properties/trait/Traits.h"
#include "../../../../common/gameMap/properties/Properties.h"
#include "../../../../common/util/numeric/NumberFormat.h"
#include "../../../../common/stats/types/ProvincesStat.h"
#include "../../../../common/util/definitions/IconDef.h"
#include "../../../../common/stats/UnifiedStatSystem.h"
#include "../../../../common/playerData/PlayerData.h"
#include "../../customComponents/icon/IconButton.h"
#include "../../../../common/gameMap/GameMap.h"
#include "../../../../common/GameMessages.h"
#include "../../../gameMap/ClientProvince.h"
#include "../../../RascClientNetIntf.h"
#include "../../../RascBox.h"

#include <sstream>
#include <limits>
#include <iostream>

#define LAYOUT_THEME theme

// --------------------- Overall location ----------------------
#define PLACEMENT_BORDER 8
#define OVERALL_WIDTH    (800 - 2*PLACEMENT_BORDER - 156 - COUNT_OUTBORDER_X(1))
#define OVERALL_HEIGHT   (600 - OVERALL_Y - RascBox::getBottomLeftUIHeight(theme) - PLACEMENT_BORDER)
#define OVERALL_X        PLACEMENT_BORDER
#define OVERALL_Y        (RascBox::getModularTopbarHeight(theme) + PLACEMENT_BORDER)

#define OVERALL_LOC                                      \
    MOVE_XY(OVERALL_X, OVERALL_Y,                        \
     PIN_L(OVERALL_WIDTH, PIN_T(OVERALL_HEIGHT,          \
      GEN_FILL                                           \
    )))

// ------------------- Top section -----------------------------

// Top back panel (overall).
#define TOP_SECTION_ROWS     2
#define TOP_SECTION_HEIGHT   COUNT_OUTBORDER_Y(TOP_SECTION_ROWS)
#define TOP_SECTION_LOC      PIN_T(TOP_SECTION_HEIGHT, GEN_FILL)

// Things on title row.
DEFINE_TABLE(TitleRow, 7, 1, 1)
#define TITLE_ROW_LOC(row)                  \
    DEFINED_BORDERTABLE_X(TitleRow, row,    \
     BORDERTABLE_Y(0, TOP_SECTION_ROWS,     \
      SUB_BORDER(TOP_SECTION_LOC)           \
    ))

// Title panel, filter panel, clear button, close button.
#define TITLE_PANEL_LOC  TITLE_ROW_LOC(0)
#define CLEAR_BUTTON_LOC TITLE_ROW_LOC(1)
#define CLOSE_BUTTON_LOC TITLE_ROW_LOC(2)

// Category buttons
#define CATEGORY_BUTTONS_COUNT ((GLfloat)CATEGORIES_COUNT)
#define CATEGORY_BUTTONS_LOC(id)                                        \
    BORDERTABLE_XY(id, 1, CATEGORY_BUTTONS_COUNT, TOP_SECTION_ROWS,     \
     SUB_BORDER(TOP_SECTION_LOC)                                        \
    )

// ------------------- Middle (overall) ------------------------

// Weight of left and right sides
#define LEFT_SIDE_WEIGHT  0.43f
#define RIGHT_SIDE_WEIGHT (1.0f - LEFT_SIDE_WEIGHT)

// The location for the whole middle section.
#define MIDDLE_LOC                                              \
    SUB_MARGIN_TB(TOP_SECTION_HEIGHT, BOTTOM_SECTION_HEIGHT,    \
     GEN_FILL                                                   \
    )

// Used for left scroll pane.
#define LEFT_SECTION_LOC                \
    BORDERSPLIT_L(LEFT_SIDE_WEIGHT,     \
     MIDDLE_LOC                         \
    )

// Used for front panels etc in right section.
#define RIGHT_SECTION_LOC               \
    SUB_BORDER_R(                       \
     BORDERSPLIT_R(RIGHT_SIDE_WEIGHT,   \
      MIDDLE_LOC                        \
    ))

// ---------------- Building info ------------------------------

// Back panel behind building name
#define BUILDING_NAME_BACK_PANEL_LOC \
    PIN_T(COUNT_ONEBORDER_Y(1),      \
     RIGHT_SECTION_LOC               \
    )

// Label/panel displaying name of current building.
#define BUILDING_NAME_LOC \
    PIN_NORMAL_T(         \
     RIGHT_SECTION_LOC    \
    )

// Location for fancy text display showing building info.
#define BUILDING_INFO_DISPLAY_LOC   \
    SUB_BORDERMARGIN_T(UI_BHEIGHT,  \
      RIGHT_SECTION_LOC             \
    )

// Location for front panel shown behind fancy text display.
#define BUILDING_INFO_PANEL_LOC   \
    SUB_BORDERMARGIN_R(UI_BWIDTH, \
     BUILDING_INFO_DISPLAY_LOC    \
    )

// ------------------ Bottom section ---------------------------

// Bottom back panel (overall).
#define BOTTOM_SECTION_HEIGHT   (UI_BHEIGHT + (UI_BORDER * 2))
#define BOTTOM_SECTION_LOC       \
    PIN_B(BOTTOM_SECTION_HEIGHT, \
     GEN_FILL                    \
    )

// The part of the bottom section used for panels (i.e: minus unaffordable button).
#define BOTTOM_SECTION_PANEL_LOC                  \
    SUB_BORDERMARGIN_L(UNAFFORDABLE_BUTTON_WIDTH, \
     SUB_BORDER(                                  \
      BOTTOM_SECTION_LOC                          \
    ))

// The location of the unaffordable button.
#define UNAFFORDABLE_BUTTON_WIDTH (IconButton::getIconOnlyButtonWidth(theme))
#define UNAFFORDABLE_BUTTON_LOC      \
    PIN_L(UNAFFORDABLE_BUTTON_WIDTH, \
     SUB_BORDER(                     \
      BOTTOM_SECTION_LOC             \
    ))

DEFINE_TABLE(BottomRow, 13, 13, 10, 8)

// The location of the cost panel, cost label, land label
#define COST_PANEL_LOC    DEFINED_BORDERTABLE_COLSPAN_X(BottomRow, 0, 2, BOTTOM_SECTION_PANEL_LOC)
#define COST_LABEL_LOC    DEFINED_BORDERTABLE_X(BottomRow, 0, BOTTOM_SECTION_PANEL_LOC)
#define LAND_LABEL_LOC    DEFINED_BORDERTABLE_X(BottomRow, 1, BOTTOM_SECTION_PANEL_LOC)

// The locations of the build and demolish buttons.
#define DEMOLISH_BUTTON_LOC DEFINED_BORDERTABLE_X(BottomRow, 2, BOTTOM_SECTION_PANEL_LOC)
#define BUILD_BUTTON_LOC    DEFINED_BORDERTABLE_X(BottomRow, 3, BOTTOM_SECTION_PANEL_LOC)

namespace rasc {

    // ------------------------------------ Category UI building button -----------------------------
    
    BuildMenu::CategoryUI::BuildingButton::BuildingButton(rascUI::Theme * theme, rascUI::ToggleButtonSeries * series, unsigned int id, CategoryUI * categoryUI, proptype_t building) :
        rascUI::ToggleButton(series, id, rascUI::Location(SUB_BORDER_X(SUB_BORDER_B(GEN_FILL))), Traits::getFriendlyName(building)),
        categoryUI(categoryUI),
        building(building) {
    }

    BuildMenu::CategoryUI::BuildingButton::~BuildingButton(void) {
    }
    
    void BuildMenu::CategoryUI::BuildingButton::onToggleButtonSelect(GLfloat viewX, GLfloat viewY, int button) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            categoryUI->buildMenu->setSelectedBuilding(building);
            categoryUI->buildMenu->updateProvinceDisplay();
            categoryUI->selectedBuilding = building;
        });
    }

    // ------------------------------------ Category UI ---------------------------------------------
    
    BuildMenu::CategoryUI::CategoryUI(rascUI::Theme * theme, BuildMenu * buildMenu, TraitDef::BuildingCategory category, unsigned int iconId, unsigned int id, const std::string & title) :
        buildMenu(buildMenu),
        category(category),
        selectedBuilding(Properties::Types::INVALID),
        titleButton(&buildMenu->categorySeries, rascUI::Location(CATEGORY_BUTTONS_LOC(id)), iconId, title,
            [this](GLfloat viewX, GLfloat viewY, int button) {
                titleButton.getTopLevel()->afterEvent->addFunction([this](void) {
                    this->buildMenu->showBuildingCategory(this->category);
                    this->buildMenu->updateProvinceDisplay();
                });
            }
        ),
        scrollPane(theme, rascUI::Location(SUB_MARGIN_B(-UI_BORDER, LEFT_SECTION_LOC)), false, true, false, true),
        buildingButtonSeries(false),
        buildingButtons() {
    }
    
    void BuildMenu::CategoryUI::rebuildBuildingButtons(const std::vector<proptype_t> & buildings, bool enableUnaffordable) {
        // Remove building buttons from scroll pane, delete them.
        scrollPane.contents.clear();
        buildingButtons.clear();
        
        // Make sure buildingButtons can accommodate the number of buildings.
        if (buildingButtons.max_size() < buildings.size()) {
            buildingButtons.resize(buildings.size());
        }
        
        // For access in loop below.
        PlayerData * playerData = buildMenu->box.netIntf->getData();
        
        // Create building buttons.
        bool foundSelected = false;
        for (unsigned int i = 0; i < buildings.size(); i++) {
            proptype_t building = buildings[i];
            buildingButtons.emplace(buildMenu->getTopLevel()->theme, &buildingButtonSeries, i, this, building);
            scrollPane.contents.add(&buildingButtons.back());
            
            // If enableUnaffordable isn't set, disable the button if the building is too expensive for our player to afford.
            bool active = true;
            if (!enableUnaffordable && !playerData->canAffordBuilding(Traits::getTraitBuildingCost(NULL, building, 1))) {
                buildingButtons.back().setActive(false);
                active = false;
            }
            
            // Set active if we find the currently selected building, and it is active.
            if (building == selectedBuilding && active) {
                buildingButtonSeries.setSelectedButton(&buildingButtons.back());
                foundSelected = true;
            }
        }
        
        if (foundSelected) return;
        
        // If we didn't find selected, try to select first active (affordable) building in the list.
        for (unsigned int i = 0; i < buildings.size(); i++) {
            if (buildingButtons[i].isActive()) {
                selectedBuilding = buildings[i];
                buildingButtonSeries.setSelectedId(i);
                return;
            }
        }
        
        // If we *still* haven't found a building to select, give up.
        selectedBuilding = Properties::Types::INVALID;
    }

    void BuildMenu::CategoryUI::rebuildToFilterProvince(Province * province, bool enableUnaffordable) {
        if (province == NULL) {
            rebuildBuildingButtons(Traits::getTraitsByCategory(category), enableUnaffordable);
        } else {
            std::vector<proptype_t> availableBuildings;
            Traits::findBuildableTraits(*province, availableBuildings, category);
            rebuildBuildingButtons(availableBuildings, enableUnaffordable);
        }
    }
    
    bool BuildMenu::CategoryUI::isCompletelyEmpty(void) const {
        return buildingButtons.empty();
    }
        
    // ----------------------------------------------------------------------------------------------
        
    BuildMenu::CostLabel::CostLabel(const rascUI::Location & location, unsigned int iconId, std::string text, unsigned int bufferSize, ZeroPrefixedNumberProvstat::FormatFunc formatFunc) :
        IconLabel(location, iconId, text),
        value(bufferSize, formatFunc, false) {
    }
    
    BuildMenu::CostLabel::~CostLabel(void) {
    }

    void BuildMenu::CostLabel::onDraw(void) {
        GLfloat offset = 0.0f;
        rascUI::Theme * theme = getTopLevel()->theme;
        const std::string & text = getText();
        
        IconButton::drawOffsetIcon(theme, location, getIconId(), offset);
        theme->customDrawTextNoMouse(location, offset, 0.0f, 1.0f, 1.0f, text);
        offset += ((text.length() + 1) * theme->customTextCharWidth);
        value.draw(location, theme, offset);
    }
    
    void BuildMenu::CostLabel::setValue(provstat_t newValue) {
        value = newValue;
    }
        
    // ----------------------------------------------------------------------------------------------

    BuildMenu::BuildMenu(RascBox & box, rascUI::Theme * theme) :
        rascUI::Container(rascUI::Location(OVERALL_LOC)),
        box(box),
        
        // Top section
        topBackPanel(rascUI::Location(TOP_SECTION_LOC)),
        titlePanel(rascUI::Location(TITLE_PANEL_LOC)),
        titleLabel(rascUI::Location(TITLE_PANEL_LOC), "Build menu"),
        clearButton(rascUI::Location(CLEAR_BUTTON_LOC), "Clear", [this](GLfloat viewX, GLfloat viewY, int button){
            if (button == getMb()) {
                getTopLevel()->afterEvent->addFunction([this](void) {
                    // When clear button is pressed, reset filtered province to none.
                    setFilteredProvince(NULL);
                });
            }
        }),
        closeButton(rascUI::Location(CLOSE_BUTTON_LOC), "Close",
            [this](GLfloat viewX, GLfloat viewY, int button){
                if (button == getMb()) hide();
            }
        ),
        categorySeries(false),

        // Middle (right) section
        middleBackPanel(rascUI::Location(MIDDLE_LOC)),
        buildingNameBackPanel(rascUI::Location(BUILDING_NAME_BACK_PANEL_LOC)),
        buildingNameFrontPanel(rascUI::Location(BUILDING_NAME_LOC)),
        buildingNameLabel(rascUI::Location(BUILDING_NAME_LOC)),
        buildingInfoPanel(rascUI::Location(BUILDING_INFO_PANEL_LOC)),
        buildingInfoDisplay(rascUI::Location(BUILDING_INFO_DISPLAY_LOC), theme, std::string(), false, false, false),

        // Bottom section
        bottomBackPanel(rascUI::Location(BOTTOM_SECTION_LOC)),
        enableUnaffordableSeries(true, [this](unsigned long id, rascUI::ToggleButton * button) {
            getTopLevel()->afterEvent->addFunction([this, button](void) {
                // When unaffordable status is changed, re-show current building category and update province display.
                enableUnaffordable = (button != NULL);
                showBuildingCategory(currentCategory);
                updateProvinceDisplay();
            });
        }),
        enableUnaffordableButton(&enableUnaffordableSeries, rascUI::Location(UNAFFORDABLE_BUTTON_LOC), IconDef::currencyLack),
        
        costPanel(rascUI::Location(COST_PANEL_LOC)),
        costLabel(rascUI::Location(COST_LABEL_LOC), IconDef::currency, "Cost:", 8, &NumberFormat::currency<8>),
        landLabel(rascUI::Location(LAND_LABEL_LOC), IconDef::land, "Land:", 6, &NumberFormat::land<6>),
        buildSeries(false),
        demolishButton(&buildSeries, rascUI::Location(DEMOLISH_BUTTON_LOC), IconDef::demolish, "Demolish"),
        buildButton(&buildSeries, rascUI::Location(BUILD_BUTTON_LOC), IconDef::build, "Build"),
        
        // Start by default with with manpower category selected, and unaffordable buildings disabled.
        currentCategory(TraitDef::BuildingCategories::manpower),
        enableUnaffordable(false),

        // If we're open, on stat update re-show current building category, and update the
        // province display.
        afterUpdateToken(box.common.statSystem->addAfterUpdateFunction([this](void) {
            if (isVisible()) {        
                showBuildingCategory(currentCategory);
                updateProvinceDisplay();
            }
        })),

        // Data exchanged with provinces.
        targetBuilding(Properties::Types::INVALID),
        buildingQuantityMin(0),
        buildingQuantityMax(1),
        isForceBlocked(false),
        filteredProvince(NULL),

        // Category UI
        categoryUIs() {
        categoryUIs.emplace(theme, this, TraitDef::BuildingCategories::manpower, IconDef::manpowerBuilding, 0, "Manpower");
        categoryUIs.emplace(theme, this, TraitDef::BuildingCategories::currency, IconDef::currencyBuilding, 1, "Economic");
        categoryUIs.emplace(theme, this, TraitDef::BuildingCategories::province, IconDef::provinceBuilding, 2, "Province");
        categoryUIs.emplace(theme, this, TraitDef::BuildingCategories::combat,   IconDef::combatBuilding,   3, "Combat");

        // Scroll panes must ALWAYS be added first (after back panel though), so it is behind top and bottom panels.
        add(&middleBackPanel);
        for (CategoryUI & ui : categoryUIs) { add(&ui.scrollPane); }
        add(&buildingInfoPanel);
        add(&buildingInfoDisplay);
        // Top section.
        add(&topBackPanel);
        add(&titlePanel);
        add(&titleLabel);
        add(&clearButton);
        add(&closeButton);
        for (CategoryUI & ui : categoryUIs) { add(&ui.titleButton); }
        // Middle (right) section.
        add(&buildingNameBackPanel);
        add(&buildingNameFrontPanel);
        add(&buildingNameLabel);
        // Bottom section.
        add(&bottomBackPanel);
        add(&enableUnaffordableButton);
        add(&costPanel);
        add(&costLabel);
        add(&landLabel);
        add(&demolishButton);
        add(&buildButton);

        // Set category series to show have default category selected (manpower).
        categorySeries.setSelectedId(0);
        
        buildSeries.setSelectedButton(&buildButton);
        setVisible(false);
        
        // Clear button is not initially active, as we default to no filter.
        clearButton.setActive(false);
    }

    BuildMenu::~BuildMenu(void) {
        box.common.statSystem->removeAfterUpdateFunction(afterUpdateToken);
    }

    void BuildMenu::setFilteredProvince(Province * province) {
        // We can't filter provinces not owned by us!
        if (province != NULL && province->getProvinceOwner() != box.netIntf->getData()->getId()) {
            province = NULL;
        }
        
        // Set filtered province.
        filteredProvince = province;

        // Re-show current building category, to re-build data.
        showBuildingCategory(currentCategory);

        // Update filter UI.
        bool hasFilter = (filteredProvince != NULL);
        clearButton.setActive(hasFilter);
        titleLabel.setText(std::string("Build ") + (hasFilter ? (std::string("in ") + filteredProvince->getName()) : "menu"));
        
        // Always update province display - even when not filtering, this is affected
        // by stat updates AND filtered province.
        updateProvinceDisplay();
    }
    
    void BuildMenu::showBuildingCategory(TraitDef::BuildingCategory category) {
        currentCategory = category;
        
        // Set the appropriate scrollpane to visible, rebuild the building category's province filter, set the appropriate selected building.
        for (CategoryUI & ui : categoryUIs) {
            if (ui.category == category) {
                ui.rebuildToFilterProvince(filteredProvince, enableUnaffordable);
                ui.scrollPane.setVisible(true);
                setSelectedBuilding(ui.selectedBuilding);
            } else {
                ui.scrollPane.setVisible(false);
            }
        }
    }

    void BuildMenu::setSelectedBuilding(proptype_t trait) {
        targetBuilding = trait;
        bool isValidBuilding = (targetBuilding != Properties::Types::INVALID);
        
        // Set various things to active only if there is a valid building.
        costLabel.setActive(isValidBuilding);
        landLabel.setActive(isValidBuilding);
        costPanel.setActive(isValidBuilding);
        buildingNameFrontPanel.setActive(isValidBuilding);
        buildingNameLabel.setActive(isValidBuilding);
        
        // Handle the case of an invalid building, (which can only happen with a focused province, or insufficient cash).
        if (!isValidBuilding) {
            // Give a different message depending on whether the category is empty, or all buildings are just disabled/too expensive.
            for (const CategoryUI & ui : categoryUIs) {
                if (ui.category == currentCategory) {
                    if (ui.isCompletelyEmpty()) {
                        buildingInfoDisplay.setText("This focused province currently has no available buildings in the selected category.");
                    } else {
                        buildingInfoDisplay.setText("Your nation lacks sufficient cash for the construction of any buildings in the selected category.");
                    }
                    break;
                }
            }
            buildingNameLabel.setText("No building selected");
            costLabel.setValue(0);
            landLabel.setValue(0);
            return;
        }
        
        buildingNameLabel.setText(Traits::getFriendlyName(targetBuilding));

        // Ignore overflow errors since we are querying a fixed number of buildings. Show cost in currency and land.
        costLabel.setValue(Traits::getTraitBuildingCost(NULL, targetBuilding, 1)[PlayerStatIds::currency]);
        {
            ProvStatModSet stats = Traits::getProvinceStatModiferSet(targetBuilding, 1);
            landLabel.setValue(stats.getProvStatMod(ProvStatIds::landUsed).getValue());
        }
        
        // Get long description of the trait, populate the fancy text display with this description.
        std::stringstream infoStr;
        Traits::getTraitLongDescription(infoStr, targetBuilding);
        buildingInfoDisplay.setText(infoStr.str());
    }

    void BuildMenu::updateProvinceDisplay(void) {
        const uint64_t playerId = box.netIntf->getData()->getId();
        
        // If building is invalid, just make sure all provinces show it as force blocked.
        if (targetBuilding == Properties::Types::INVALID) {
            isForceBlocked = true;
        
        // Otherwise, for non-invalid buildings:
        } else {
        
            // For stackable buildings, work out min and max in owned provinces.
            if (Traits::isTraitStackable(targetBuilding)) {

                buildingQuantityMin = std::numeric_limits<provstat_t>::max(),
                buildingQuantityMax = std::numeric_limits<provstat_t>::min();

                for (Province * province : box.common.provincesStat->getOwnershipInfo(playerId).getOwnedProvinces().vec()) {
                    provstat_t buildingCount = province->properties.getTraitCount(targetBuilding);
                    buildingQuantityMin = std::min(buildingCount, buildingQuantityMin);
                    buildingQuantityMax = std::max(buildingCount, buildingQuantityMax);
                }

                // If we don't have any provinces
                if (buildingQuantityMin > buildingQuantityMax) {
                    buildingQuantityMin = 0; buildingQuantityMax = 1;
                // If all have same value
                } else if (buildingQuantityMin == buildingQuantityMax) {
                    // If same value is bigger than zero, make all buildings the maximum, otherwise make them the minimum.
                    if (buildingQuantityMin > 0) {
                        buildingQuantityMin--;
                    } else {
                        buildingQuantityMax++;
                    }
                }

            // For non stackable buildings, just use values 0-1.
            } else {
                buildingQuantityMin = 0; buildingQuantityMax = 1;
            }

            // Set forceBlocked if we cannot afford the building.
            // Ignore overflow errors since we are querying a fixed number of buildings.
            isForceBlocked = !box.netIntf->getData()->canAffordBuilding(Traits::getTraitBuildingCost(NULL, targetBuilding, 1));
        }
        
        // Update the build mode colour for all provinces now we know all the info.
        for (Province * province : box.common.map->getProvinces()) {
            ((ClientProvince *)province)->updateBuildColourAndDisplay(*this, playerId);
        }
    }

    void BuildMenu::internalShow(void) {
        setVisible(true);
        getTopLevel()->setKeyboardSelected(&closeButton);
        // Show our current building category, to ensure all is updated.
        showBuildingCategory(currentCategory);
        // Update the colours, for all provinces.
        updateProvinceDisplay();
    }

    void BuildMenu::internalHide(void) {
        setVisible(false);
    }

    void BuildMenu::show(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            if (!isVisible()) {
                internalShow();
            }
        });
    }
    void BuildMenu::hide(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            if (isVisible()) {
                internalHide();
            }
        });
    }
    void BuildMenu::toggle(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            if (isVisible()) {
                internalHide();
            } else {
                internalShow();
            }
        });
    }
    
    bool BuildMenu::isOpen(void) const {
        return isVisible();
    }

    void BuildMenu::onLeftClickProvince(Province * province) {
        // If the user clicked on a province we're not already filtering, update filtering.
        if (province != filteredProvince) {
            setFilteredProvince(province);
        }
    }
    
    void BuildMenu::onRightClickProvince(Province * province) {
        // If we actually have a building selected.
        if (targetBuilding != Properties::Types::INVALID) {

            // In build mode, request the construction of ONE building.
            if (buildSeries.getSelectedToggleButton() == &buildButton) {
                province->properties.requestConstructBuildings(CheckedUpdate::clientAuto(box.netIntf), targetBuilding, 1);

            // In demolish mode, use the helper method to request construction of
            // MINUS ONE building, and handle the special case for garrisons.
            } else {
                requestDemolishBuilding(CheckedUpdate::clientAuto(box.netIntf), *province, targetBuilding);
            }
        }
    }
    
    void BuildMenu::requestDemolishBuilding(CheckedUpdate update, Province & province, proptype_t targetBuilding, provstat_t count) {
        // Demolition of forts must be handled slightly differently (special case).
        // FIRST remove a garrison, if present. If we don't do this FIRST, the server will reject
        // our removal of a fort, because the garrison requires it.
        if (targetBuilding == Properties::Types::fort) {
            provstat_t garrisonSize = GET_GARRISON_SIZE(province.properties);
            if (garrisonSize > 0) {
                province.properties.requestConstructBuildings(CheckedUpdate::client(update.msg()), Properties::Types::garrison, -garrisonSize);
            }
        }

        province.properties.requestConstructBuildings(CheckedUpdate::client(update.msg()), targetBuilding, -1);
    }
}

/*
 * ModularTopbarModule.cpp
 *
 *  Created on: 13 Dec 2020
 *      Author: wilson
 */

#include "ModularTopbarModule.h"

#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <tinyxml2/tinyxml2.h>
#include <unordered_map>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <string>
#include <array>

#include "../../../../common/config/RascConfig.h"
#include "../../../../common/util/XMLUtil.h"
#include "menu/ModularTopbarConfigMenu.h"
#include "modules/ArmyFunctionModule.h"
#include "modules/ManpowerModule.h"
#include "modules/CurrencyModule.h"
#include "modules/DividerModule.h"
#include "modules/FlagModule.h"
#include "ModularTopbarDef.h"
#include "../../../RascBox.h"
#include "ModularTopbar.h"

#define LAYOUT_THEME theme

namespace rasc {

    #define X(name, ...) #name,
    const static std::array<std::string, ModuleTypes::COUNT> moduleNames = { RASC_MODULAR_TOPBAR_LIST };
    #undef X

    #define X(name, className, friendlyName, ...) friendlyName,
    const static std::array<std::string, ModuleTypes::COUNT> moduleFriendlyNames = { RASC_MODULAR_TOPBAR_LIST };
    #undef X

    #define X(name, ...) { #name, ModuleTypes::name },
    const static std::unordered_map<std::string, unsigned int> moduleNameToTypeMap({ RASC_MODULAR_TOPBAR_LIST });
    #undef X
    
    
    
    ModularTopbarModule::ModularTopbarModule(RascBox & box, rascUI::Theme * theme, unsigned int moduleType, unsigned int index) :
        rascUI::Container(rascUI::Location(0.0f, UI_BORDER, 0.0f, -2.0f * UI_BORDER, 0.0f, 0.0f, 0.0f, 1.0f)),
        moduleType(moduleType),
        index(index),
        positionOffset(0.0f),
        box(box) {
    }
    
    ModularTopbarModule::~ModularTopbarModule(void) {
    }
    
    void ModularTopbarModule::recalculate(const rascUI::Rectangle & parentSize) {
        // Update location's position and width, *then* recalculate.
        location.position.x = positionOffset;
        location.position.width = getLayoutWidth();
        rascUI::Container::recalculate(parentSize);
    }
    
    tinyxml2::XMLElement * ModularTopbarModule::getOrInitBase(tinyxml2::XMLDocument * document) {
        // Get modular topbar base config element.
        tinyxml2::XMLElement * modularTopbarElem = RascConfig::getOrInitBase(document, ModularTopbar::BASE_CONFIG_NAME.c_str(), &ModularTopbar::initXMLConfig);
        tinyxml2::XMLElement * modulesElem = readElement(modularTopbarElem, ModularTopbar::MODULES_ELEM_NAME.c_str());
        
        // Return modules child element of id index, if it exists already.
        size_t id = 0;
        loopElementsAll(searchElem, modulesElem) {
            if (id == getIndex()) {
                return searchElem;
            }
            id++;
        }
        
        // New modules must be added at the back.
        if (id != getIndex()) {
            std::cerr << "CRITICAL ERROR: ModularTopbarModule: " << "Trying to initialise base module index " << getIndex() << ", when there are only " << id << " XML modules.\n";
            exit(EXIT_FAILURE);
            return NULL;
        }
        
        // Module element does not exist, initialise it.
        return addModuleDefaultXMLConfig(document, modulesElem, getModuleType());
    }
    
    ModularTopbar & ModularTopbarModule::getTopbar(void) const {
        return box.uiGetModularTopbar();
    }
    
    ModularTopbarConfigMenu & ModularTopbarModule::getTopbarConfigMenu(void) const {
        return box.uiGetModularTopbarConfigMenu();
    }
    
    rascUI::Container * ModularTopbarModule::getOptionsMenu(const rascUI::Location & location, bool & shouldDelete) {
        shouldDelete = false;
        return getTopbarConfigMenu().getNoOptionsMenu();
    }

    void ModularTopbarModule::onStatUpdate(void) {
    }

    // ---------------- Static utility methods ------------------------
    
    ModularTopbarModule * ModularTopbarModule::createModule(RascBox & box, rascUI::Theme * theme, unsigned int moduleType, unsigned int index) {
        switch(moduleType) {
            #define X(name, className, ...)                   \
                case ModuleTypes::name:                       \
                    return new className(box, theme, index);
            RASC_MODULAR_TOPBAR_LIST
            #undef X
            default:
                std::cerr << "CRITICAL ERROR: ModularTopbarModule: " << "Tried to create invalid module type: " << moduleType << ".\n";
                exit(EXIT_FAILURE);
                return NULL;
        }
    }
    
    tinyxml2::XMLElement * ModularTopbarModule::addModuleDefaultXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * parentElem, unsigned int moduleType) {
        switch(moduleType) {
            #define X(name, className, ...)                                 \
                case ModuleTypes::name: {                                   \
                    tinyxml2::XMLElement * elem = doc->NewElement(#name);   \
                    className::initXMLConfig(doc, elem);                    \
                    parentElem->InsertEndChild(elem);                       \
                    return elem;                                            \
                }
            RASC_MODULAR_TOPBAR_LIST
            #undef X
            default:
                std::cerr << "CRITICAL ERROR: ModularTopbarModule: " << "Tried to add default XML for invalid module type: " << moduleType << ".\n";
                exit(EXIT_FAILURE);
                return NULL;
        }
    }
    
    const std::string & ModularTopbarModule::getModuleName(unsigned int moduleType) {
        return moduleNames[moduleType];
    }
    
    const std::string & ModularTopbarModule::getModuleFriendlyName(unsigned int moduleType) {
        return moduleFriendlyNames[moduleType];
    }

    unsigned int ModularTopbarModule::moduleNameToType(const std::string & internalName) {
        auto iter = moduleNameToTypeMap.find(internalName);
        if (iter == moduleNameToTypeMap.end()) {
            std::cerr << "WARNING: ModularTopbarModule: " << "Invalid modular topbar module name: \"" << internalName << "\".";
            return 0;
        } else {
            return iter->second;
        }
    }
}

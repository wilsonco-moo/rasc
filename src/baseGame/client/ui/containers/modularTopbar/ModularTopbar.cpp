/*
 * ModularTopbar.cpp
 *
 *  Created on: 13 Dec 2020
 *      Author: wilson
 */

#include "ModularTopbar.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <tinyxml2/tinyxml2.h>
#include <algorithm>

#include "../../../../common/stats/UnifiedStatSystem.h"
#include "../../../../common/config/RascConfig.h"
#include "../../../../common/util/XMLUtil.h"
#include "../../../GlobalProperties.h"
#include "ModularTopbarModule.h"
#include "ModularTopbarDef.h"
#include "../../../RascBox.h"

#define LAYOUT_THEME theme

// The base xml tag for our configuration.
#define GET_BASE simplenetwork::MutexPtr<tinyxml2::XMLDocument> doc = box.globalProperties->rascConfig->getDocument(); \
                 tinyxml2::XMLElement * base = RascConfig::getOrInitBase(&*doc, BASE_CONFIG_NAME.c_str(), &ModularTopbar::initXMLConfig);
                 

namespace rasc {

    constexpr unsigned int ModularTopbar::ROWS;
    
    const std::string ModularTopbar::BASE_CONFIG_NAME  = "modularTopbar",
                      ModularTopbar::MODULES_ELEM_NAME = "modules";
    
    // --------------------------------------------------------------------------
    
    ModularTopbar::ModularTopbar(RascBox & box, rascUI::Theme * theme) :
        rascUI::Container(rascUI::Location(0.0f, 0.0f, 0.0f, COUNT_OUTBORDER_Y(ROWS))),
        box(box),
        backPanel(),
        modules(),
        // Notify all of our modules when a stat update happens.
        afterUpdateToken(box.common.statSystem->addAfterUpdateFunction([this](void) {
            for (ModularTopbarModule * module : modules) {
                module->onStatUpdate();
            }
        })) {
        
        add(&backPanel);
    }
    
    ModularTopbar::~ModularTopbar(void) {
        box.common.statSystem->removeAfterUpdateFunction(afterUpdateToken);
        for (ModularTopbarModule * module : modules) {
            delete module;
        }
    }
    
    void ModularTopbar::initXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
        tinyxml2::XMLElement * modules = doc->NewElement(MODULES_ELEM_NAME.c_str());
        ModularTopbarModule::addModuleDefaultXMLConfig(doc, modules, ModuleTypes::flag);
        ModularTopbarModule::addModuleDefaultXMLConfig(doc, modules, ModuleTypes::manpower);
        ModularTopbarModule::addModuleDefaultXMLConfig(doc, modules, ModuleTypes::armyFunction);
        ModularTopbarModule::addModuleDefaultXMLConfig(doc, modules, ModuleTypes::currency);
        elem->InsertEndChild(modules);
    }
    
    void ModularTopbar::saveModulesToXML(void) {
        {
            GET_BASE
            base->DeleteChildren();
            tinyxml2::XMLElement * modulesElem = doc->NewElement(MODULES_ELEM_NAME.c_str());
            for (ModularTopbarModule * module : modules) {
                const std::string & moduleName = ModularTopbarModule::getModuleName(module->getModuleType());
                tinyxml2::XMLElement * moduleElem = doc->NewElement(moduleName.c_str());
                module->saveToXML(&*doc, moduleElem);
                modulesElem->InsertEndChild(moduleElem);
            }
            base->InsertEndChild(modulesElem);
        }
        box.globalProperties->rascConfig->saveLater();
    }
    
    void ModularTopbar::onChangeToLobbyRoom(void) {
        rascUI::Theme * theme = getTopLevel()->theme;
        
        // Add all modules from XML config.
        GET_BASE
        tinyxml2::XMLElement * modulesElem = readElement(base, MODULES_ELEM_NAME.c_str());
        loopElementsAll(moduleElem, modulesElem) {
            unsigned int moduleType = ModularTopbarModule::moduleNameToType(moduleElem->Name());
            ModularTopbarModule * newModule = ModularTopbarModule::createModule(box, theme, moduleType, modules.size());
            modules.push_back(newModule);
            add(newModule);
        }
    }
    
    void ModularTopbar::recalculate(const rascUI::Rectangle & parentSize) {
        // Resize location's position width to sum of module widths, with borders. Also update
        // modules' position offsets.
        rascUI::Theme * theme = getTopLevel()->theme;
        GLfloat width = UI_BORDER;
        for (ModularTopbarModule * module : modules) {
            module->positionOffset = width;
            width += (module->getLayoutWidth() + UI_BORDER);
        }
        location.position.width = width;
        
        // Notify the player colour bar that it needs to recalculate.
        box.uiPlayerColourBarNotifyRecalculate();
        
        rascUI::Container::recalculate(parentSize);
    }
    
    void ModularTopbar::moduleRunRecalculate(void) {
        recalculate(getParent()->location.cache);
    }
    
    ModularTopbarModule * ModularTopbar::addModule(rascUI::Theme * theme, unsigned int moduleType) {
        // Create module, add to vector, add to this (container).
        ModularTopbarModule * newModule = ModularTopbarModule::createModule(box, theme, moduleType, modules.size());
        modules.push_back(newModule);
        add(newModule);
        
        // Run the module's stat update, now it is in place. This gives it initial data for its display.
        newModule->onStatUpdate();
        
        // Update XML, recalculation is done automatically.
        saveModulesToXML();
        return newModule;
    }

    void ModularTopbar::deleteModule(unsigned int moduleIndex) {
        if (moduleIndex >= modules.size()) return;
        
        // Remove from this (container), delete module, erase from vector, shuffle indices.
        remove(modules[moduleIndex]);
        delete modules[moduleIndex];
        modules.erase(modules.begin() + moduleIndex);
        for (; moduleIndex < modules.size(); moduleIndex++) {
            modules[moduleIndex]->index--;
        }
        
        // Update XML and recalculate.
        saveModulesToXML();
        recalculate(getParent()->location.cache);
    }

    void ModularTopbar::shuffleLeft(unsigned int moduleIndex) {
        if (moduleIndex >= modules.size() || moduleIndex < 1) return;
        
        // Swap modules and swap modules' indices.
        std::swap(modules[moduleIndex], modules[moduleIndex - 1]);
        std::swap(modules[moduleIndex]->index, modules[moduleIndex - 1]->index);
        
        // Update XML and recalculate.
        saveModulesToXML();
        recalculate(getParent()->location.cache);
    }

    void ModularTopbar::shuffleRight(unsigned int moduleIndex) {
        if (modules.size() < 1 || moduleIndex >= modules.size() - 1) return;
        
        // Swap modules and swap modules' indices.
        std::swap(modules[moduleIndex], modules[moduleIndex + 1]);
        std::swap(modules[moduleIndex]->index, modules[moduleIndex + 1]->index);
        
        // Update XML and recalculate.
        saveModulesToXML();
        recalculate(getParent()->location.cache);
    }
}

/*
 * ModularTopbar.h
 *
 *  Created on: 13 Dec 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULARTOPBAR_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULARTOPBAR_H_

#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/base/Container.h>
#include <string>
#include <vector>

namespace rascUI {
    class Theme;
}

namespace tinyxml2 {
    class XMLDocument;
    class XMLElement;
}

namespace rasc {
    class ModularTopbarModule;
    class RascBox;

    /**
     *
     */
    class ModularTopbar : public rascUI::Container {
    public:
        /**
         * The number of rows of UI contained within the modular topbar.
         * This defines the height of all modular topbar modules.
         */
        static constexpr unsigned int ROWS = 4;
        
        /**
         * Name of base config XML element, and modules XML element, for modular topbar.
         */
        static const std::string BASE_CONFIG_NAME,
                                 MODULES_ELEM_NAME;
    private:
        RascBox & box;
        rascUI::BackPanel backPanel;
        std::vector<ModularTopbarModule *> modules;
        void * afterUpdateToken;
        
        // Don't allow copying: we hold raw resources.
        ModularTopbar(const ModularTopbar & other);
        ModularTopbar & operator = (const ModularTopbar & other);
        
    public:
        ModularTopbar(RascBox & box, rascUI::Theme * theme);
        virtual ~ModularTopbar(void);
        
    protected:
        virtual void recalculate(const rascUI::Rectangle & parentSize) override;
        
    public:
        /**
         * Init XML config. Public so child modules can run this.
         */
        static void initXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem);
        
        /**
         * Modules should run this (from a function queue!), when their config
         * changes their size.
         */
        void moduleRunRecalculate(void);
        
        /**
         * Saves contents of all modules to XML, and requests an update of
         * the XML config. This should be called by modules (or us), whenever there is a
         * large change affecting config.
         */
        void saveModulesToXML(void);
        
        /**
         * Here we add modules from XML config.
         * We do not actually create modules, until we enter the lobby room. This ensures
         * that modules are able to access all parts of the game (like the map), when
         * they're created.
         */
        void onChangeToLobbyRoom(void);
        
        /**
         * Allows const access to our modules.
         */
        inline const std::vector<ModularTopbarModule *> & getModules(void) const {
            return modules;
        }
        
        /**
         * Gets the number of modules we currently have.
         */
        inline unsigned int getModuleCount(void) const {
            return modules.size();
        }
        
        
        // ---------------------- Modification methods ------------------------
        // *** NOTE: MUST BE run from function queue. ***
        
        /**
         * Adds a new module to the topbar, returns it for convenience.
         * MUST BE run from function queue.
         */
        ModularTopbarModule * addModule(rascUI::Theme * theme, unsigned int moduleType);
        
        /**
         * Deletes a module by index, if it exists.
         * MUST BE run from function queue.
         */
        void deleteModule(unsigned int index);
        
        /**
         * Swaps module by index, with module to the left, if possible.
         * MUST BE run from function queue.
         */
        void shuffleLeft(unsigned int index);
        
        /**
         * Swaps module by index, with module to the right, if possible.
         * MUST BE run from function queue.
         */
        void shuffleRight(unsigned int index);
    };
}

#endif

/*
 * ModularTopbarModule.h
 *
 *  Created on: 13 Dec 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULARTOPBARMODULE_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULARTOPBARMODULE_H_

#include <rascUI/base/Container.h>

namespace rascUI {
    class Theme;
}

namespace tinyxml2 {
    class XMLDocument;
    class XMLElement;
}

namespace rasc {
    class ModularTopbarConfigMenu;
    class ModularTopbar;
    class RascBox;
    
    /**
     * For use by subclasses: Allows access to XML config,
     * which should have been made by ModularTopbar.
     */
    #define RASC_MODULAR_TOPBAR_MODULE_GET_BASE                                                                 \
        simplenetwork::MutexPtr<tinyxml2::XMLDocument> doc = box.globalProperties->rascConfig->getDocument();   \
        tinyxml2::XMLElement * base = getOrInitBase(&*doc);
    
    /**
     *
     */
    class ModularTopbarModule : public rascUI::Container {
    private:
        // So modular topbar can set our index and position offset.
        friend class ModularTopbar;
        
        unsigned int moduleType, index;
        GLfloat positionOffset; 
        
    protected:
        RascBox & box;
        
    public:
        ModularTopbarModule(RascBox & box, rascUI::Theme * theme, unsigned int moduleType, unsigned int index);
        virtual ~ModularTopbarModule(void);
        
    protected:
        virtual void recalculate(const rascUI::Rectangle & parentSize) override;
        
        /**
         * Gets or initialises config for this module, used in RASC_MODULAR_TOPBAR_MODULE_GET_BASE.
         */
        tinyxml2::XMLElement * getOrInitBase(tinyxml2::XMLDocument * document);
        
    public:
        /**
         * Gets parent modular topbar, which we are included within.
         */
        ModularTopbar & getTopbar(void) const;
        
        /**
         * Gets modular topbar config menu, which is used to configure us.
         */
        ModularTopbarConfigMenu & getTopbarConfigMenu(void) const;
        
        /**
         * Gets module type.
         */
        inline unsigned int getModuleType(void) const {
            return moduleType;
        }
        
        /**
         * This should return preferred module width, required for the modular
         * topbar to arrange modules.
         */
        virtual GLfloat getLayoutWidth(void) const = 0;
        
        /**
         * This should create or return a pointer to a rascUI component, containing options for
         * the modular topbar module. The parameter shouldDelete should be set true if the module
         * options are intended to be deleted after use, and should be set to false otherwise.
         * 
         * By default this returns the modular topbar config menu's "no options" menu - see ModularTopbarConfigMenu::getNoOptionsMenu.
         */
        virtual rascUI::Container * getOptionsMenu(const rascUI::Location & location, bool & shouldDelete);
        
        /**
         * This is run by modular topbar each time the stat system reports an update, by default
         * does nothing. It is also called immediately after we are added to the modular topbar,
         * (from modular topbar config menu), to provide us with initial data.
         * Modules should use this to update their data displays.
         */
        virtual void onStatUpdate(void);

        /**
         * This should save the current contents of the modular topbar module to the XML element
         * and document. The provided element will have no children or attributes.
         */
        virtual void saveToXML(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) = 0;
        
        /**
         * Gets our index within our containing modular topbar. This changes as
         * we are shuffled left and right.
         */
        inline unsigned int getIndex(void) const {
            return index;
        }
        
        // ---------------- Static utility methods ------------------------
        
        /**
         * Creates a modular topbar module.
         */
        static ModularTopbarModule * createModule(RascBox & box, rascUI::Theme * theme, unsigned int moduleType, unsigned int index);
        
        /**
         * Adds, as the last child of parentElem, an element containing default
         * XML config for the specified module type.
         * For convenience, the new child element is returned.
         */
        static tinyxml2::XMLElement * addModuleDefaultXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * parentElem, unsigned int moduleType);
        
        /**
         * Gets the internal name of a modular topbar module.
         */
        static const std::string & getModuleName(unsigned int moduleType);
        
        /**
         * Gets the friendly display name of a modular topbar module.
         */
        static const std::string & getModuleFriendlyName(unsigned int moduleType);
        
        /**
         * Converts module internal name to type.
         */
        static unsigned int moduleNameToType(const std::string & internalName);
    };
}

#endif

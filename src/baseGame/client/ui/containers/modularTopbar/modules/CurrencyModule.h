/*
 * CurrencyModule.h
 *
 *  Created on: 13 Dec 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULES_CURRENCYMODULE_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULES_CURRENCYMODULE_H_

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/util/EmplaceArray.h>

#include "../util/ModularTopbarStatPanel.h"
#include "../ModularTopbarModule.h"
#include "../util/ButtonPanel.h"

namespace rasc {
    
    /**
     *
     */
    class CurrencyModule : public ModularTopbarModule {
    public:
        // Defines available button modes for currency module.
        #define RASC_CURRENCY_AVAILABLE_BUTTON_MODES \
            X(build)
        
        // Defines IDs of available currency module button modes.
        #define X(name, ...) name,
        class AvailableModeIds { public: enum { RASC_CURRENCY_AVAILABLE_BUTTON_MODES COUNT }; };
        #undef X
        constexpr static unsigned int AVAILABLE_MODES_COUNT = AvailableModeIds::COUNT;
        
    private:
        // Internal options menu.
        class CurrencyMenu : public rascUI::Container {
        private:
            CurrencyModule * module;
            rascUI::FrontPanel frontPanel;
            rascUI::Label titleLabel, layoutModeLabel;
            rascUI::ToggleButtonSeries layoutSeries;
            rascUI::ToggleButton compactButton, verboseButton;
            rascUI::ToggleButtonSeries statSeries;
            rascUI::Label statLabel;
            rascUI::ToggleButton twoRowsButton, threeRowsButton;
            rascUI::Label buttonModeLabel;
            rascUI::ToggleButtonSeries buttonModeSeries;
            rascUI::EmplaceArray<rascUI::ToggleButton, AVAILABLE_MODES_COUNT> buttonModeButtons;
        public:
            CurrencyMenu(const rascUI::Location & location, rascUI::Theme * theme, CurrencyModule * module);
            virtual ~CurrencyMenu(void);
        private:
            // Updates active status of buttons based on module config.
            void updateButtonActivity(void);
            // Updates module config from button states.
            void update(void);
        };
        
        ModularTopbarStatPanel statsPanel;
        bool verbose, threeRows;
        unsigned int previousThreeRows, previousVerbose;
        ButtonPanel buttonPanel;

    public:
        CurrencyModule(RascBox & box, rascUI::Theme * theme, unsigned int index);
        virtual ~CurrencyModule(void);
        
    private:
        // Updates location of stat panel, and recalculates it, if different to previous.
        void updateStatPanelLocation(void);
        // Updates text labels for rows.
        void updateLabels(void);
        // Updates zero prefixed formats if needed.
        void updateZeroPrefixFormats(void);
        // Updates stat panel content, from stats.
        void updateStatPanelContent(void);
        
    public:
        virtual GLfloat getLayoutWidth(void) const override;
        virtual rascUI::Container * getOptionsMenu(const rascUI::Location & location, bool & shouldDelete) override;
        virtual void onStatUpdate(void) override;
        
        // For initialising and saving XML config.
        static void initXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem);
        virtual void saveToXML(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) override;
    };
}

#endif

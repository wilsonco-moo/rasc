/*
 * ArmyFunctionModule.h
 *
 *  Created on: 17 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULES_ARMYFUNCTIONMODULE_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULES_ARMYFUNCTIONMODULE_H_

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/util/EmplaceArray.h>

#include "../ModularTopbarModule.h"
#include "../util/ButtonPanel.h"

namespace rasc {

    /**
     *
     */
    class ArmyFunctionModule : public ModularTopbarModule {
    public:
        // Defines available button modes for currency module.
        // Format: name, module width.
        #define RASC_ARMY_FUNCTION_AVAILABLE_BUTTON_MODES \
            X(simpleArmyFunction, 32)                     \
            X(advancedArmyFunction, 120)
        
        // Defines IDs of available currency module button modes.
        #define X(name, ...) name,
        class AvailableModeIds { public: enum { RASC_ARMY_FUNCTION_AVAILABLE_BUTTON_MODES COUNT }; };
        #undef X
        constexpr static unsigned int AVAILABLE_MODES_COUNT = AvailableModeIds::COUNT;
        
    private:
        // Internal options menu.
        class ArmyFunctionMenu : public rascUI::Container {
        private:
            ArmyFunctionModule * module;
            rascUI::FrontPanel frontPanel;
            rascUI::Label titleLabel, buttonModeLabel;
            rascUI::ToggleButtonSeries buttonModeSeries;
            rascUI::EmplaceArray<rascUI::ToggleButton, AVAILABLE_MODES_COUNT> buttonModeButtons;
        public:
            ArmyFunctionMenu(const rascUI::Location & location, rascUI::Theme * theme, ArmyFunctionModule * module);
            virtual ~ArmyFunctionMenu(void);
        private:
            // Updates module config from button states.
            void update(void);
        };
        
        ButtonPanel buttonPanel;
        
    public:
        ArmyFunctionModule(RascBox & box, rascUI::Theme * theme, unsigned int index);
        virtual ~ArmyFunctionModule(void);
        
        virtual GLfloat getLayoutWidth(void) const override;
        virtual rascUI::Container * getOptionsMenu(const rascUI::Location & location, bool & shouldDelete) override;
        
        // For initialising and saving XML config.
        static void initXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem);
        virtual void saveToXML(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) override;
    };
}

#endif

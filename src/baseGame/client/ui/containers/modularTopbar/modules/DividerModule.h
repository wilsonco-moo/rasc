/*
 * DividerModule.h
 *
 *  Created on: 24 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULES_DIVIDERMODULE_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULES_DIVIDERMODULE_H_

#include "../ModularTopbarModule.h"

namespace rasc {

    /**
     *
     */
    class DividerModule : public ModularTopbarModule {
    private:
        const static GLfloat WIDTH;

    public:
        DividerModule(RascBox & box, rascUI::Theme * theme, unsigned int index);
        virtual ~DividerModule(void);
        
    protected:
        virtual void onDraw(void) override;
        
    public:
        virtual GLfloat getLayoutWidth(void) const override;
        
        // For initialising and saving XML config.
        static void initXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem);
        virtual void saveToXML(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) override;
    };
}

#endif

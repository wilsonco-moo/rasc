/*
 * CurrencyModule.cpp
 *
 *  Created on: 13 Dec 2020
 *      Author: wilson
 */

#include "CurrencyModule.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <tinyxml2/tinyxml2.h>
#include <cstddef>
#include <cstdlib>
#include <string>

#include "../../../../../common/gameMap/provinceUtil/ProvinceStatTypes.h"
#include "../../../../../common/util/numeric/NumberFormat.h"
#include "../../../../../common/util/definitions/IconDef.h"
#include "../../../../../common/playerData/PlayerData.h"
#include "../../../../../common/config/RascConfig.h"
#include "../../../../../common/util/XMLUtil.h"
#include "../../../../../common/util/Misc.h"
#include "../../../../RascClientNetIntf.h"
#include "../../../../GlobalProperties.h"
#include "../util/ButtonPanelDef.h"
#include "../../../../RascBox.h"
#include "../ModularTopbarDef.h"
#include "../ModularTopbar.h"

#define LAYOUT_THEME theme

// For currency menu.
#define MENU_ROW_LOC(id)    \
    MOVE_BORDERNORMAL_Y(id, \
     PIN_NORMAL_T(          \
      GEN_BORDER            \
    ))

#define MENU_BUTTON_LOC(rowId, buttonId) \
    BORDERTABLE_X(buttonId, 2, MENU_ROW_LOC(rowId))

// For currency module
#define ROW_LOC(id)         \
    MOVE_BORDERNORMAL_Y(id, \
     PIN_NORMAL_T(          \
      GEN_FILL              \
    ))

#define STATS_PANEL_LOC                         \
    MOVE_BORDERNORMAL_Y(0,                      \
     PIN_T(COUNT_INBORDER_Y(threeRows ? 4 : 3), \
      GEN_FILL                                  \
    ))

namespace rasc {
    
    // Defines available button modes for currency menu.
    #define X(name, ...) ButtonPanelMode::name,
    static const unsigned int availableModes[CurrencyModule::AVAILABLE_MODES_COUNT] = { RASC_CURRENCY_AVAILABLE_BUTTON_MODES };
    #undef X

    // ================================================ Currency menu =====================================================
    
    CurrencyModule::CurrencyMenu::CurrencyMenu(const rascUI::Location & location, rascUI::Theme * theme, CurrencyModule * module) :
        rascUI::Container(location),
        module(module),
        frontPanel(),
        titleLabel(rascUI::Location(MENU_ROW_LOC(0)), "Manpower module options:"),
        layoutModeLabel(rascUI::Location(MENU_ROW_LOC(1)), "Layout mode:"),
        layoutSeries(false, [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
            getTopLevel()->afterEvent->addFunction([this](void) { update(); });
        }),
        compactButton(&layoutSeries, rascUI::Location(MENU_BUTTON_LOC(2, 0)), "Compact"),
        verboseButton(&layoutSeries, rascUI::Location(MENU_BUTTON_LOC(2, 1)), "Verbose"),
        statSeries(false, [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
            getTopLevel()->afterEvent->addFunction([this](void) { update(); });
        }),
        statLabel(rascUI::Location(MENU_ROW_LOC(3)), "Stat panel mode:"),
        twoRowsButton(&statSeries, rascUI::Location(MENU_BUTTON_LOC(4, 0)), "Two rows"),
        threeRowsButton(&statSeries, rascUI::Location(MENU_BUTTON_LOC(4, 1)), "Three rows"),
        buttonModeLabel(rascUI::Location(MENU_ROW_LOC(5)), "Button mode:"),
        buttonModeSeries(false, [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
            getTopLevel()->afterEvent->addFunction([this](void) { update(); });
        }) {

        add(&frontPanel);
        add(&titleLabel);
        add(&layoutModeLabel);
        add(&compactButton);
        add(&verboseButton);
        add(&statLabel);
        add(&twoRowsButton);
        add(&threeRowsButton);
        add(&buttonModeLabel);
        #define X(name, ...)                                                                          \
            buttonModeButtons.emplace(&buttonModeSeries,                                              \
                                      rascUI::Location(MENU_BUTTON_LOC(6, AvailableModeIds::name)),   \
                                      ButtonPanel::getButtonModeFriendlyName(ButtonPanelMode::name)); \
            add(&buttonModeButtons[AvailableModeIds::name]);
        RASC_CURRENCY_AVAILABLE_BUTTON_MODES
        #undef X
        
        layoutSeries.setSelectedId(module->verbose ? 1 : 0);
        statSeries.setSelectedId(module->threeRows ? 1 : 0);
        
        // Set selected ID, to id of current button mode, in available modes array.
        unsigned int selectedMode = module->buttonPanel.getButtonMode();
        for (unsigned int i = 0; i < AVAILABLE_MODES_COUNT; i++) {
            if (availableModes[i] == selectedMode) {
                buttonModeSeries.setSelectedId(i);
                return;
            }
        }
        
        updateButtonActivity();
    }
    
    CurrencyModule::CurrencyMenu::~CurrencyMenu(void) {
    }
    
    void CurrencyModule::CurrencyMenu::updateButtonActivity(void) {
        // Disable button mode toggle if three mode is enabled.
        bool active = !module->threeRows;
        for (rascUI::ToggleButton & toggleButton : buttonModeButtons) {
            toggleButton.setActive(active);
        }
    }
    
    void CurrencyModule::CurrencyMenu::update(void) {
        // Recalculate is only needed if verbose status changed.
        bool recalcNeeded = (module->verbose != layoutSeries.getSelectedId());
        
        // Set module's config from our UI, update module display, recalculate topbar, save XML.
        module->verbose = layoutSeries.getSelectedId();
        module->threeRows = statSeries.getSelectedId();
        module->buttonPanel.setButtonMode(module, module->box, getTopLevel()->theme, !module->threeRows, availableModes[buttonModeSeries.getSelectedId()]);
        module->onStatUpdate();
        if (recalcNeeded) module->getTopbar().moduleRunRecalculate();
        module->getTopbar().saveModulesToXML();
        
        updateButtonActivity();
    }
    
    // ================================================ Currency module ===================================================
    
    CurrencyModule::CurrencyModule(RascBox & box, rascUI::Theme * theme, unsigned int index) :
        ModularTopbarModule(box, theme, ModuleTypes::currency, index),
        statsPanel(rascUI::Location(), IconDef::currency, "Currency", 6, 3),
        previousThreeRows(-1), // Neither true nor false
        previousVerbose(-1),
        buttonPanel(rascUI::Location(ROW_LOC(3))) {
        add(&statsPanel);
        
        // Set the row config used in all formats.
        statsPanel.getRow(2).text = "Total";
        
        // Read default config from XML upon creation.
        RASC_MODULAR_TOPBAR_MODULE_GET_BASE
        tinyxml2::XMLElement * layoutElem = readElement(base, "layout");
        verbose = Misc::strToBool(readAttribute(layoutElem, "verbose"));
        threeRows = Misc::strToBool(readAttribute(layoutElem, "threeRows"));
        buttonPanel.loadFromXML(base, &availableModes[0], AVAILABLE_MODES_COUNT, this, box, theme, !threeRows);
    }
    
    CurrencyModule::~CurrencyModule(void) {
    }
    
    void CurrencyModule::updateZeroPrefixFormats(void) {
        // Note: also run when three row status changes.
        if ((unsigned int)verbose != previousVerbose || (unsigned int)threeRows != previousThreeRows) {
            previousVerbose = verbose;
            
            if (verbose) {
                statsPanel.getRow(0).zeroPrefix = ZeroPrefixedNumberProvstat(9, &NumberFormat::currency<9>, !threeRows);
                statsPanel.getRow(1).zeroPrefix = ZeroPrefixedNumberProvstat(9, &NumberFormat::currency<9>, threeRows);
                statsPanel.getRow(2).zeroPrefix = ZeroPrefixedNumberProvstat(9, &NumberFormat::currency<9>, false);
            } else {
                statsPanel.getRow(0).zeroPrefix = ZeroPrefixedNumberProvstat(6, &NumberFormat::currency<6>, !threeRows);
                statsPanel.getRow(1).zeroPrefix = ZeroPrefixedNumberProvstat(6, &NumberFormat::currency<6>, threeRows);
                statsPanel.getRow(2).zeroPrefix = ZeroPrefixedNumberProvstat(6, &NumberFormat::currency<6>, false);
            }
        }
    }
    
    void CurrencyModule::updateLabels(void) {
        if ((unsigned int)threeRows != previousThreeRows) {
            if (threeRows) {
                statsPanel.getRow(0).text = "Incme";
                statsPanel.getRow(1).text = "Blnce";
            } else {
                statsPanel.getRow(0).text = "/turn";
                statsPanel.getRow(1).text = "Total";
            }
        }
    }
    
    void CurrencyModule::updateStatPanelLocation(void) {
        if ((unsigned int)threeRows != previousThreeRows) {
            previousThreeRows = threeRows;
            
            rascUI::Theme * theme = getTopLevel()->theme;
            statsPanel.location.setPositionAndWeight(STATS_PANEL_LOC);
            childRecalculate(&statsPanel, location.cache);
            statsPanel.setDrawnRows(threeRows ? 3 : 2);
        }
    }
    
    void CurrencyModule::updateStatPanelContent(void) {
        PlayerData * data = box.netIntf->getData();
        provstat_t balance = data->getCurrencyBalancePerTurn(),
                     total = data->getTotalCurrency();

        if (threeRows) {
            provstat_t income = data->getCurrencyIncomePerTurn();
            statsPanel.getRow(0).zeroPrefix = income;
            statsPanel.getRow(1).zeroPrefix = balance;
            statsPanel.getRow(2).zeroPrefix = total;
        } else {
            statsPanel.getRow(0).zeroPrefix = balance;
            statsPanel.getRow(1).zeroPrefix = total;
        }
    }
    
    GLfloat CurrencyModule::getLayoutWidth(void) const {
        return verbose ? 150 : 120;
    }
    
    rascUI::Container * CurrencyModule::getOptionsMenu(const rascUI::Location & location, bool & shouldDelete) {
        shouldDelete = true;
        return new CurrencyMenu(location, getTopLevel()->theme, this);
    }
    
    void CurrencyModule::onStatUpdate(void) {
        updateZeroPrefixFormats();
        updateLabels();
        updateStatPanelLocation();
        updateStatPanelContent();
    }
    
    void CurrencyModule::initXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
        tinyxml2::XMLElement * layoutElem = doc->NewElement("layout");
        layoutElem->SetAttribute("verbose", "true");
        layoutElem->SetAttribute("threeRows", "false");
        elem->InsertEndChild(layoutElem);
        ButtonPanel::initXMLConfig(doc, elem, availableModes[0]);
    }
    
    void CurrencyModule::saveToXML(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
        tinyxml2::XMLElement * layoutElem = doc->NewElement("layout");
        layoutElem->SetAttribute("verbose", Misc::boolToStr(verbose).c_str());
        layoutElem->SetAttribute("threeRows", Misc::boolToStr(threeRows).c_str());
        elem->InsertEndChild(layoutElem);
        buttonPanel.saveToXML(doc, elem);
    }
}

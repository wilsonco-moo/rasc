/*
 * DividerModule.cpp
 *
 *  Created on: 24 Jan 2021
 *      Author: wilson
 */

#include "DividerModule.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/base/Theme.h>
#include <GL/gl.h>

#include "../ModularTopbarDef.h"

namespace rasc {
    
    const GLfloat DividerModule::WIDTH = 2.0f;
    
    DividerModule::DividerModule(RascBox & box, rascUI::Theme * theme, unsigned int index) :
        ModularTopbarModule(box, theme, ModuleTypes::divider, index) {
    }
    
    DividerModule::~DividerModule(void) {
    }

    void DividerModule::onDraw(void) {
        rascUI::Theme * theme = getTopLevel()->theme;
        theme->customSetDrawColour(theme->customColourMainText);
        
        GLfloat x1 = location.cache.x,
                y1 = location.cache.y,
                x2 = location.cache.x + location.cache.width,
                y2 = location.cache.y + location.cache.height;
        
        glVertex2f(x1, y1);
        glVertex2f(x2, y1);
        glVertex2f(x2, y2);
        glVertex2f(x1, y2);
    }
    
    GLfloat DividerModule::getLayoutWidth(void) const {
        return WIDTH;
    }

    void DividerModule::initXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
    }
    
    void DividerModule::saveToXML(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
    }
}

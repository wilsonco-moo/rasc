/*
 * FlagModule.h
 *
 *  Created on: 10 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULES_FLAGMODULE_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULES_FLAGMODULE_H_

#include "../../../customComponents/flag/FlagComponent.h"
#include "../ModularTopbarModule.h"

namespace rasc {

    /**
     *
     */
    class FlagModule : public ModularTopbarModule {
    private:
        FlagComponent flag;

    public:
        FlagModule(RascBox & box, rascUI::Theme * theme, unsigned int index);
        virtual ~FlagModule(void);
        
        virtual GLfloat getLayoutWidth(void) const override;
        virtual void onStatUpdate(void) override;
        
        // For initialising and saving XML config.
        static void initXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem);
        virtual void saveToXML(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) override;
    };
}

#endif

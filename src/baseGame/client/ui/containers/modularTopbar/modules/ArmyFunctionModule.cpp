/*
 * ArmyFunctionModule.cpp
 *
 *  Created on: 17 Jan 2021
 *      Author: wilson
 */

#include "ArmyFunctionModule.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <tinyxml2/tinyxml2.h>
#include <cstddef>
#include <cstdlib>
#include <string>

#include "../../../../../common/config/RascConfig.h"
#include "../../../../../common/config/RascConfig.h"
#include "../../../../../common/util/XMLUtil.h"
#include "../../../../GlobalProperties.h"
#include "../util/ButtonPanelDef.h"
#include "../../../../RascBox.h"
#include "../ModularTopbarDef.h"
#include "../ModularTopbar.h"

#define LAYOUT_THEME theme

// For currency menu.
#define MENU_ROW_LOC(id)    \
    MOVE_BORDERNORMAL_Y(id, \
     PIN_NORMAL_T(          \
      GEN_BORDER            \
    ))

#define MENU_BUTTON_LOC(rowId, buttonId) \
    BORDERTABLE_X(buttonId, 2, MENU_ROW_LOC(rowId))

namespace rasc {

    // Defines available button modes for currency menu.
    #define X(name, ...) ButtonPanelMode::name,
    static const unsigned int availableModes[ArmyFunctionModule::AVAILABLE_MODES_COUNT] = { RASC_ARMY_FUNCTION_AVAILABLE_BUTTON_MODES };
    #undef X
    
    // ================================================ Currency menu =====================================================
    
    ArmyFunctionModule::ArmyFunctionMenu::ArmyFunctionMenu(const rascUI::Location & location, rascUI::Theme * theme, ArmyFunctionModule * module) :
        rascUI::Container(location),
        module(module),
        frontPanel(),
        titleLabel(rascUI::Location(MENU_ROW_LOC(0)), "Army function options:"),
        buttonModeLabel(rascUI::Location(MENU_ROW_LOC(1)), "Button mode:"),
        buttonModeSeries(false, [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
            getTopLevel()->afterEvent->addFunction([this](void) { update(); });
        }) {

        add(&frontPanel);
        add(&titleLabel);
        add(&buttonModeLabel);
        #define X(name, ...)                                                                          \
            buttonModeButtons.emplace(&buttonModeSeries,                                              \
                                      rascUI::Location(MENU_BUTTON_LOC(2, AvailableModeIds::name)),   \
                                      ButtonPanel::getButtonModeFriendlyName(ButtonPanelMode::name)); \
            add(&buttonModeButtons[AvailableModeIds::name]);
        RASC_ARMY_FUNCTION_AVAILABLE_BUTTON_MODES
        #undef X
        
        // Set selected ID, to id of current button mode, in available modes array.
        unsigned int selectedMode = module->buttonPanel.getButtonMode();
        for (unsigned int i = 0; i < AVAILABLE_MODES_COUNT; i++) {
            if (availableModes[i] == selectedMode) {
                buttonModeSeries.setSelectedId(i);
                return;
            }
        }
    }
    
    ArmyFunctionModule::ArmyFunctionMenu::~ArmyFunctionMenu(void) {
    }

    void ArmyFunctionModule::ArmyFunctionMenu::update(void) {
        // Set module's config from our UI, recalculate topbar, save XML.
        module->buttonPanel.setButtonMode(module, module->box, getTopLevel()->theme, true, availableModes[buttonModeSeries.getSelectedId()]);
        module->getTopbar().moduleRunRecalculate();
        module->getTopbar().saveModulesToXML();
    }
    
    // ================================================ Army function module ==============================================
    
    ArmyFunctionModule::ArmyFunctionModule(RascBox & box, rascUI::Theme * theme, unsigned int index) :
        ModularTopbarModule(box, theme, ModuleTypes::armyFunction, index),
        buttonPanel() {
        
        // Read default config from XML upon creation.
        RASC_MODULAR_TOPBAR_MODULE_GET_BASE
        buttonPanel.loadFromXML(base, &availableModes[0], AVAILABLE_MODES_COUNT, this, box, theme, true);
    }
    
    ArmyFunctionModule::~ArmyFunctionModule(void) {
    }
    
    GLfloat ArmyFunctionModule::getLayoutWidth(void) const {
        switch(buttonPanel.getButtonMode()) {
            #define X(name, width, ...)     \
                case ButtonPanelMode::name: \
                    return width;
            RASC_ARMY_FUNCTION_AVAILABLE_BUTTON_MODES
            #undef X
            default:
                // Should not happen.
                return 300.0f;
        }
    }
    
    rascUI::Container * ArmyFunctionModule::getOptionsMenu(const rascUI::Location & location, bool & shouldDelete) {
        shouldDelete = true;
        return new ArmyFunctionMenu(location, getTopLevel()->theme, this);
    }
    
    void ArmyFunctionModule::initXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
        ButtonPanel::initXMLConfig(doc, elem, availableModes[0]);
    }
    
    void ArmyFunctionModule::saveToXML(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
        buttonPanel.saveToXML(doc, elem);
    }
}

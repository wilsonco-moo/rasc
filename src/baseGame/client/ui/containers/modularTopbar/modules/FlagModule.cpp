/*
 * FlagModule.cpp
 *
 *  Created on: 10 Jan 2021
 *      Author: wilson
 */

#include "FlagModule.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#include "../../../../../common/playerData/PlayerData.h"
#include "../../../../../common/playerData/flag/Flag.h"
#include "../../../../RascClientNetIntf.h"
#include "../ModularTopbarDef.h"
#include "../../../../RascBox.h"
#include "../ModularTopbar.h"

#define LAYOUT_THEME theme

namespace rasc {

    FlagModule::FlagModule(RascBox & box, rascUI::Theme * theme, unsigned int index) :
        ModularTopbarModule(box, theme, ModuleTypes::flag, index),
        flag() {
        add(&flag);
    }
    
    FlagModule::~FlagModule(void) {
    }
    
    void FlagModule::onStatUpdate(void) {
        // Note: Always update flag in stat update, it may have changed (e.g: when
        // exiting lobby and selecting an existing AI player).
        flag.setFlag(box.netIntf->getData()->getFlag());
    }
    
    GLfloat FlagModule::getLayoutWidth(void) const {
        rascUI::Theme * theme = getTopLevel()->theme;
        return COUNT_INBORDER_Y(ModularTopbar::ROWS) * Flag::ASPECT_RATIO;
    }
    
    void FlagModule::initXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
    }
    
    void FlagModule::saveToXML(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
    }
}

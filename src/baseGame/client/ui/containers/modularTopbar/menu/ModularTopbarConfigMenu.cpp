/*
 * ModularTopbarConfigMenu.cpp
 *
 *  Created on: 3 Jan 2021
 *      Author: wilson
 */

#include "ModularTopbarConfigMenu.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <iostream>
#include <cstddef>
#include <array>

#include "../ModularTopbarModule.h"
#include "../../../../RascBox.h"
#include "../ModularTopbar.h"

// ------------------ Overall layout ---------------------

#define OVERALL_WIDTH 650
#define OVERALL_HEIGHT 420

#define OVERALL_LOC                                             \
    SETSIZEMOVE_XY(OVERALL_WIDTH, OVERALL_HEIGHT,               \
     SPLIT_B(0.0f, SPLIT_T(0.5f,                                \
      SPLIT_R(0.0f, SPLIT_L(0.5f,                               \
       SUB_MARGIN_T(COUNT_OUTBORDER_Y(ModularTopbar::ROWS),     \
        GEN_FILL                                                \
    ))))))

#define OVERALL_FILL_LOC                             \
    SUB_MARGIN_TB(TOP_BAR_HEIGHT, BOTTOM_BAR_HEIGHT, \
      SUB_BORDER_X(                                  \
       GEN_FILL                                      \
    ))

DEFINE_TABLE(OverallColumns, 3, 3, 4)

// ---------------------- Bars -----------------------------

#define TOP_BAR_HEIGHT COUNT_OUTBORDER_Y(1)
#define TOP_BAR_LOC                                         \
    PIN_T(TOP_BAR_HEIGHT,                                   \
     SUB_BORDER_X(                                          \
      GEN_FILL                                              \
    ))
#define TOP_BAR_CONTENTS_LOC \
    SUB_BORDER_Y(TOP_BAR_LOC)
#define TOP_BAR_BACKPANEL_LOC TOP_BAR_LOC

#define BOTTOM_BAR_HEIGHT (UI_BHEIGHT + 4.0f * UI_BORDER)
#define BOTTOM_BAR_LOC                                      \
    PIN_B(BOTTOM_BAR_HEIGHT,                                \
     SUB_BORDER_X(                                          \
      GEN_FILL                                              \
    ))
#define BOTTOM_BAR_CONTENTS_LOC \
    SUB_BORDER_Y(BOTTOM_BAR_LOC)
#define BOTTOM_BAR_BACKPANEL_LOC \
    DEFINED_BORDERTABLE_COLSPAN_X(OverallColumns, 0, 2, BOTTOM_BAR_LOC)

// ----------------- Bar contents ---------------------------

#define TOP_FRONT_PANEL_LOC(id) \
    DEFINED_BORDERTABLE_X(OverallColumns, id, TOP_BAR_CONTENTS_LOC)

#define CLOSE_BUTTON_WIDTH 64
#define CLOSE_BUTTON_LOC        \
    PIN_R(CLOSE_BUTTON_WIDTH,   \
     TOP_FRONT_PANEL_LOC(2)     \
    )

#define CURRENT_MODULES_TOP_FRONT_PANEL_LOC \
    SUB_BORDERMARGIN_R(CLOSE_BUTTON_WIDTH,  \
     TOP_FRONT_PANEL_LOC(2)                 \
    )

// ----------------------- Module select bar ---------------------

#define BOTTOM_BAR_MODULE_SELECT_LOC \
    DEFINED_BORDERTABLE_X(OverallColumns, 0, BOTTOM_BAR_CONTENTS_LOC)
#define BOTTOM_BAR_MODULE_SELECT_CONTENTS_LOC \
    SUB_BORDER(BOTTOM_BAR_MODULE_SELECT_LOC)
#define MODULE_SELECT_ADD_BUTTON_LOC \
    BOTTOM_BAR_MODULE_SELECT_CONTENTS_LOC

// --------------------- Current modules bar ---------------------

#define BOTTOM_BAR_CURRENT_MODULES_LOC \
    DEFINED_BORDERTABLE_X(OverallColumns, 1, BOTTOM_BAR_CONTENTS_LOC)
#define BOTTOM_BAR_CURRENT_MODULES_CONTENTS_LOC \
    SUB_BORDER(BOTTOM_BAR_CURRENT_MODULES_LOC)
#define CURRENT_MODULES_BUTTON_COUNT 3
#define CURRENT_MODULES_BUTTON_LOC(id)              \
    BORDERTABLE_X(id, CURRENT_MODULES_BUTTON_COUNT, \
     BOTTOM_BAR_CURRENT_MODULES_CONTENTS_LOC        \
    )

// ---------------------- Scrollpanes ----------------------------------

#define SCROLLPANE_LOC(id)                      \
    SUB_MARGIN_B(-UI_BORDER,                    \
     DEFINED_BORDERTABLE_X(OverallColumns, id,  \
      OVERALL_FILL_LOC                          \
    ))

#define SCROLLPANE_TOGGLE_BUTTON_LOC \
    SUB_BORDER_B(                    \
     SUB_BORDER_R(                   \
      GEN_FILL                       \
    ))

// --------------------- Options display and placeholders ------------------

#define OPTIONS_DISPLAY_LOC                      \
    SUB_BORDER_B(                                \
     DEFINED_BORDERTABLE_X(OverallColumns, 2,    \
      SUB_MARGIN_T(TOP_BAR_HEIGHT,               \
       SUB_BORDER_X(                             \
        GEN_FILL                                 \
    ))))

#define PLACEHOLDER_OPTIONS_LABEL_LOC(id)   \
    MOVE_Y(UI_BHEIGHT * id,                 \
     PIN_NORMAL_T(                          \
      GEN_FILL                              \
    ))

#define LAYOUT_THEME theme

namespace rasc {

    ModularTopbarConfigMenu::ModularTopbarConfigMenu(RascBox & box, rascUI::Theme * theme) :
        rascUI::FloatingPanel(rascUI::Location(OVERALL_LOC)),
        box(box),
    
        topBarBackPanel(rascUI::Location(TOP_BAR_LOC)),
        topFrontPanels(),
        topLabels(),
        closeButton(rascUI::Location(CLOSE_BUTTON_LOC), "Close",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) hide();
            }
        ),
        
        moduleSelectScrollPane(theme, rascUI::Location(SCROLLPANE_LOC(0)), false, true, false),
        moduleSelectSeries(true, [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
            getTopLevel()->afterEvent->addFunction([this, toggleButton](void) {
                addButton.setActive(toggleButton != NULL);
            });
        }),
        moduleSelectButtons(),

        currentModulesScrollPane(theme, rascUI::Location(SCROLLPANE_LOC(1)), false, true, false),
        currentModuleSeries(true, [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
            getTopLevel()->afterEvent->addFunction([this, toggleButtonId](void) {
                onCurrentModuleSelect(toggleButtonId);
            });
        }),
        currentModuleButtons(),

        placeholderOptionsMenu(rascUI::Location(OPTIONS_DISPLAY_LOC)),
        placeholderOptionsPanel(),
        placeholderOptionsLabel0(rascUI::Location(PLACEHOLDER_OPTIONS_LABEL_LOC(0)), "Please select a"),
        placeholderOptionsLabel1(rascUI::Location(PLACEHOLDER_OPTIONS_LABEL_LOC(1)), "Modular Topbar module."),
        noOptionsMenu(rascUI::Location(OPTIONS_DISPLAY_LOC)),
        noOptionsPanel(),
        noOptionsLabel0(rascUI::Location(PLACEHOLDER_OPTIONS_LABEL_LOC(0)), "This Modular Topbar"),
        noOptionsLabel1(rascUI::Location(PLACEHOLDER_OPTIONS_LABEL_LOC(1)), "module has no options."),

        currentOptionsModule(NULL),
        currentOptionsContainer(NULL),
        shouldDeleteOptions(false),

        bottomBarBackPanel(rascUI::Location(BOTTOM_BAR_BACKPANEL_LOC)),
        bottomModuleSelectFrontPanel(rascUI::Location(BOTTOM_BAR_MODULE_SELECT_LOC)),
        addButton(rascUI::Location(MODULE_SELECT_ADD_BUTTON_LOC), "Add new module", [this](GLfloat viewX, GLfloat viewY, int button) {
            if (button == getMb()) {
                getTopLevel()->afterEvent->addFunction([this](void) {
                    unsigned int moduleType = moduleSelectSeries.getSelectedId();
                    getTopbar().addModule(getTopLevel()->theme, moduleType);
                    rebuildCurrentModuleButtons();
                    // Make sure the last (just added) module gets selected.
                    currentModuleSeries.setSelectedId(getTopbar().getModuleCount() - 1, true);
                });
            }
        }),
        bottomCurrentModulesFrontPanel(rascUI::Location(BOTTOM_BAR_CURRENT_MODULES_LOC)),
        moveUpButton(rascUI::Location(CURRENT_MODULES_BUTTON_LOC(0)), "Up", [this](GLfloat viewX, GLfloat viewY, int button) {
            if (button == getMb()) {
                getTopLevel()->afterEvent->addFunction([this](void) {
                    unsigned long currentSelectedId = currentModuleSeries.getSelectedId();
                    getTopbar().shuffleLeft(currentSelectedId);
                    rebuildCurrentModuleButtons();
                    currentModuleSeries.setSelectedId(currentSelectedId - 1, true);
                });
            }
        }),
        moveDownButton(rascUI::Location(CURRENT_MODULES_BUTTON_LOC(1)), "Down", [this](GLfloat viewX, GLfloat viewY, int button) {
            if (button == getMb()) {
                getTopLevel()->afterEvent->addFunction([this](void) {
                    unsigned long currentSelectedId = currentModuleSeries.getSelectedId();
                    getTopbar().shuffleRight(currentSelectedId);
                    rebuildCurrentModuleButtons();
                    currentModuleSeries.setSelectedId(currentSelectedId + 1, true);
                });
            }
        }),
        deleteButton(rascUI::Location(CURRENT_MODULES_BUTTON_LOC(2)), "Del", [this](GLfloat viewX, GLfloat viewY, int button) {
            if (button == getMb()) {
                getTopLevel()->afterEvent->addFunction([this](void) {
                    // Always remove options menu FIRST before deleting, since it relates to the module we're deleting.
                    removeOptionsMenu();
                    unsigned long currentSelectedId = currentModuleSeries.getSelectedId();
                    getTopbar().deleteModule(currentSelectedId);
                    rebuildCurrentModuleButtons();
                    // If nothing is now selected (deleted last one), move selection to second-to-last if possible.
                    if (currentModuleSeries.getSelectedId() == rascUI::ToggleButtonSeries::NO_TOGGLE_BUTTON && getTopbar().getModuleCount() > 0) {
                        currentModuleSeries.setSelectedId(getTopbar().getModuleCount() - 1, true);
                    } else {
                        onCurrentModuleSelect(currentModuleSeries.getSelectedId());
                    }
                });
            }
        }) {
        
        addButton.setActive(false);
        moveUpButton.setActive(false);
        moveDownButton.setActive(false);
        deleteButton.setActive(false);
            
        topFrontPanels.emplace(rascUI::Location(TOP_FRONT_PANEL_LOC(0)));
        topLabels.emplace(rascUI::Location(TOP_FRONT_PANEL_LOC(0)), "Select new module");
        topFrontPanels.emplace(rascUI::Location(TOP_FRONT_PANEL_LOC(1)));
        topLabels.emplace(rascUI::Location(TOP_FRONT_PANEL_LOC(1)), "Current modules");
        topFrontPanels.emplace(rascUI::Location(CURRENT_MODULES_TOP_FRONT_PANEL_LOC));
        topLabels.emplace(rascUI::Location(CURRENT_MODULES_TOP_FRONT_PANEL_LOC), "Module options");
        
        static_assert(COLUMN_COUNT == OverallColumns::count, "Overall column count must match.");
        
        placeholderOptionsMenu.add(&placeholderOptionsPanel);
        placeholderOptionsMenu.add(&placeholderOptionsLabel0);
        placeholderOptionsMenu.add(&placeholderOptionsLabel1);
        noOptionsMenu.add(&noOptionsPanel);
        noOptionsMenu.add(&noOptionsLabel0);
        noOptionsMenu.add(&noOptionsLabel1);
        
        contents.add(&moduleSelectScrollPane);
        contents.add(&currentModulesScrollPane);
        contents.add(&placeholderOptionsMenu);
        
        for (unsigned int moduleType = 0; moduleType < ModuleTypes::COUNT; moduleType++) {
            moduleSelectButtons.emplace(&moduleSelectSeries,
                    rascUI::Location(SCROLLPANE_TOGGLE_BUTTON_LOC), ModularTopbarModule::getModuleFriendlyName(moduleType));
            moduleSelectScrollPane.contents.add(&moduleSelectButtons.back());
        }
        
        contents.add(&topBarBackPanel);
        for (unsigned int i = 0; i < COLUMN_COUNT; i++) {
            contents.add(&topFrontPanels[i]);
            contents.add(&topLabels[i]);
        }
        contents.add(&closeButton);
        contents.add(&bottomBarBackPanel);
        contents.add(&bottomModuleSelectFrontPanel);
        contents.add(&addButton);
        contents.add(&bottomCurrentModulesFrontPanel);
        contents.add(&moveUpButton);
        contents.add(&moveDownButton);
        contents.add(&deleteButton);
    }
    
    ModularTopbarConfigMenu::~ModularTopbarConfigMenu(void) {
        if (shouldDeleteOptions) {
            delete currentOptionsContainer;
        }
    }

    ModularTopbar & ModularTopbarConfigMenu::getTopbar(void) const {
        return box.uiGetModularTopbar();
    }
    
    void ModularTopbarConfigMenu::rebuildCurrentModuleButtons(void) {
        ModularTopbar & topbar = getTopbar();
        rascUI::Theme * theme = getTopLevel()->theme;
        
        // If module count is different, clear scrollpane, resize emplace vector, add toggle buttons, restore selected ID.
        if (currentModuleButtons.size() != topbar.getModuleCount()) {
            unsigned long currentSelectedId = currentModuleSeries.getSelectedId();
            currentModulesScrollPane.contents.clear();
            currentModuleButtons.resize(topbar.getModuleCount());
            for (unsigned int i = 0; i < topbar.getModuleCount(); i++) {
                currentModuleButtons.emplace(&currentModuleSeries, i, rascUI::Location(SCROLLPANE_TOGGLE_BUTTON_LOC));
                currentModulesScrollPane.contents.add(&currentModuleButtons.back());
            }
            if (currentSelectedId != rascUI::ToggleButtonSeries::NO_TOGGLE_BUTTON && currentSelectedId < topbar.getModuleCount()) {
                currentModuleSeries.setSelectedId(currentSelectedId);
            }
        }
        
        // Set names of each module.
        for (unsigned int i = 0; i < topbar.getModuleCount(); i++) {
            currentModuleButtons[i].setText(ModularTopbarModule::getModuleFriendlyName(topbar.getModules()[i]->getModuleType()));
        }
    }
    
    void ModularTopbarConfigMenu::onCurrentModuleSelect(unsigned long moduleId) {
        moveUpButton.setActive(moduleId != rascUI::ToggleButtonSeries::NO_TOGGLE_BUTTON && moduleId > 0);
        moveDownButton.setActive(moduleId != rascUI::ToggleButtonSeries::NO_TOGGLE_BUTTON && moduleId < getTopbar().getModuleCount() - 1);
        deleteButton.setActive(moduleId != rascUI::ToggleButtonSeries::NO_TOGGLE_BUTTON);
        updateOptionsMenuFromCurrent();
    }
    
    void ModularTopbarConfigMenu::removeOptionsMenu(void) {
        // Remove and delete options container.
        contents.remove(currentOptionsContainer);
        if (shouldDeleteOptions) {
            delete currentOptionsContainer;
        }
        currentOptionsModule = NULL;
        currentOptionsContainer = NULL;
        
        // Add placeholder container.
        contents.addAfter(&currentModulesScrollPane, &placeholderOptionsMenu);
    }
    
    void ModularTopbarConfigMenu::createOptionsMenuFromCurrent(void) {
        rascUI::Theme * theme = getTopLevel()->theme;
        
        // Remove placeholder container.
        contents.remove(&placeholderOptionsMenu);
        
        // Create and add options container.
        ModularTopbarModule * module = getTopbar().getModules()[currentModuleSeries.getSelectedId()];
        currentOptionsContainer = module->getOptionsMenu(rascUI::Location(OPTIONS_DISPLAY_LOC), shouldDeleteOptions);
        contents.addAfter(&currentModulesScrollPane, currentOptionsContainer);
    }
    
    void ModularTopbarConfigMenu::updateOptionsMenuFromCurrent(void) {
        // Remove options menu if nothing selected, and current options menu still exists.
        if (currentModuleSeries.getSelectedId() == rascUI::ToggleButtonSeries::NO_TOGGLE_BUTTON) {
            if (currentOptionsContainer != NULL) {
                removeOptionsMenu();
            }
        // Otherwise (if something is selected), remove and create from current if it is different to previous.
        } else {
            if (getTopbar().getModules()[currentModuleSeries.getSelectedId()] != currentOptionsContainer) {
                if (currentOptionsContainer != NULL) {
                    removeOptionsMenu();
                }
                createOptionsMenuFromCurrent();
            }
        }
    }
    
    void ModularTopbarConfigMenu::onShow(void) {
        getTopLevel()->setKeyboardSelected(&closeButton);
        rebuildCurrentModuleButtons();
        updateOptionsMenuFromCurrent();
    }
    
    void ModularTopbarConfigMenu::onHide(void) {
        box.uiSetModularTopbarButtonSelected();
        
        // Might as well save memory by deleting our options menu when we close.
        if (currentOptionsContainer != NULL) {
            removeOptionsMenu();
        }
    }
    
    rascUI::Container * ModularTopbarConfigMenu::getPlaceholderOptionsMenu(void) {
        return &placeholderOptionsMenu;
    }
    
    rascUI::Container * ModularTopbarConfigMenu::getNoOptionsMenu(void) {
        return &noOptionsMenu;
    }
}

/*
 * ModularTopbarConfigMenu.h
 *
 *  Created on: 3 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MENU_MODULARTOPBARCONFIGMENU_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MENU_MODULARTOPBARCONFIGMENU_H_

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/floating/FloatingPanel.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/util/EmplaceVector.h>
#include <rascUI/util/EmplaceArray.h>

#include "../ModularTopbarDef.h"

namespace rascUI {
    class Theme;
}

namespace rasc {
    class ModularTopbarModule;
    class ModularTopbar;
    class RascBox;
    
    /**
     *
     */
    class ModularTopbarConfigMenu : public rascUI::FloatingPanel {
    private:
        constexpr static unsigned int COLUMN_COUNT = 3;
        
        RascBox & box;
        
        rascUI::BackPanel topBarBackPanel;
        
        rascUI::EmplaceArray<rascUI::FrontPanel, COLUMN_COUNT> topFrontPanels;
        rascUI::EmplaceArray<rascUI::Label, COLUMN_COUNT> topLabels;
        rascUI::Button closeButton;
        
        rascUI::ScrollPane moduleSelectScrollPane;
        rascUI::ToggleButtonSeries moduleSelectSeries;
        rascUI::EmplaceArray<rascUI::ToggleButton, ModuleTypes::COUNT> moduleSelectButtons;
        
        rascUI::ScrollPane currentModulesScrollPane;
        rascUI::ToggleButtonSeries currentModuleSeries;
        rascUI::EmplaceVector<rascUI::ToggleButton> currentModuleButtons;
        
        rascUI::Container placeholderOptionsMenu;
        rascUI::FrontPanel placeholderOptionsPanel;
        rascUI::Label placeholderOptionsLabel0, placeholderOptionsLabel1;
        rascUI::Container noOptionsMenu;
        rascUI::FrontPanel noOptionsPanel;
        rascUI::Label noOptionsLabel0, noOptionsLabel1;
        
        ModularTopbarModule * currentOptionsModule;
        rascUI::Container * currentOptionsContainer;
        bool shouldDeleteOptions;
        
        rascUI::BackPanel bottomBarBackPanel;
        rascUI::FrontPanel bottomModuleSelectFrontPanel;
        rascUI::Button addButton;
        rascUI::FrontPanel bottomCurrentModulesFrontPanel;
        rascUI::Button moveUpButton, moveDownButton, deleteButton;
        
        // Don't allow copying: we hold raw resources.
        ModularTopbarConfigMenu(const ModularTopbarConfigMenu & other);
        ModularTopbarConfigMenu & operator = (const ModularTopbarConfigMenu & other);
        
    public:
        ModularTopbarConfigMenu(RascBox & box, rascUI::Theme * theme);
        virtual ~ModularTopbarConfigMenu(void);
        
    private:
        ModularTopbar & getTopbar(void) const;
        
        // Run from function queues by buttons and toggle buttons.
        void rebuildCurrentModuleButtons(void);
        void onCurrentModuleSelect(unsigned long moduleId);
        
        // Removes current options menu, assuming it exists.
        void removeOptionsMenu(void);
        // Creates options menu from current selected, assuming current selection is valid, and there is no current options menu.
        void createOptionsMenuFromCurrent(void);
        // Either removes and/or creates options menu, to reflect current selection.
        void updateOptionsMenuFromCurrent(void);
        
    protected:
        virtual void onShow(void) override;
        virtual void onHide(void) override;
        
    public:
        /**
         * Can be used by modules for default options menus. If these are used,
         * shouldDelete must be returned as false in getOptionsMenu.
         */
        rascUI::Container * getPlaceholderOptionsMenu(void);
        rascUI::Container * getNoOptionsMenu(void);
    };
}

#endif

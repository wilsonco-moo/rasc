/*
 * ModularTopbarStatPanel.cpp
 *
 *  Created on: 11 Jan 2021
 *      Author: wilson
 */

#include "ModularTopbarStatPanel.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <GL/gl.h>
#include <cmath>

#include "../../../customComponents/icon/IconButton.h"

#define LAYOUT_THEME theme

// Height (in pixels at 100% scale) of dividing lines.
#define DIVIDER_HEIGHT 1.0f

namespace rasc {

    ModularTopbarStatPanel::Row::Row(void) :
        text(),
        zeroPrefix(),
        showZeroPrefix(true) {
    }
    
    // --------------------------------------------------------------
    
    ModularTopbarStatPanel::ModularTopbarStatPanel(const rascUI::Location & location, unsigned int titleIcon, const std::string & titleText, unsigned int textLength, unsigned int rowCount) :
        rascUI::FrontPanel(location),
        titleIcon(titleIcon),
        textLength(textLength),
        drawnRows(rowCount),
        titleText(titleText),
        rows(rowCount) {
        
        for (unsigned int i = 0; i < rowCount; i++) {
            rows.emplace();
        }
    }
    
    ModularTopbarStatPanel::~ModularTopbarStatPanel(void) {
    }
    
    void ModularTopbarStatPanel::drawDivide(unsigned int id) {
        rascUI::Theme * theme = getTopLevel()->theme;
        GLfloat bord = UI_BORDER * location.xScale,
                x1 = std::floor(location.cache.x + bord),
                x2 = std::floor(location.cache.x + location.cache.width - bord),
                y1 = std::floor(location.cache.y + (COUNT_ONEBORDER_Y(id) - 0.5f * UI_BORDER) * location.yScale),
                y2 = std::floor(y1 + DIVIDER_HEIGHT * location.yScale);
        glVertex2f(x1, y1); glVertex2f(x2, y1); glVertex2f(x2, y2); glVertex2f(x1, y2);
    }

    void ModularTopbarStatPanel::onDraw(void) {
        rascUI::Theme * theme = getTopLevel()->theme;
        theme->drawFrontPanel(location);
        GLfloat rowHeight = UI_BORDER + UI_BHEIGHT,
                xOff = theme->customTextCharWidth * textLength;
        
        IconButton::drawIconAndText(theme, location, titleIcon, titleText);
        
        for (unsigned int i = 0; i < drawnRows; i++) {
            drawDivide(i + 1);
            const Row & row = rows[i];
            theme->customDrawTextNoMouse(location, 0.0f, rowHeight * (i + 1), 1.0f, 1.0f, row.text);
            if (row.showZeroPrefix) {
                row.zeroPrefix.draw(location, theme, xOff, rowHeight * (i + 1), 1.0f, 1.0f);
            }
        }
    }
    
    ModularTopbarStatPanel::Row & ModularTopbarStatPanel::getRow(unsigned int id) {
        return rows[id];
    }
    
    const ModularTopbarStatPanel::Row & ModularTopbarStatPanel::getRow(unsigned int id) const {
        return rows[id];
    }

    void ModularTopbarStatPanel::setDrawnRows(unsigned int newDrawnRows) {
        drawnRows = newDrawnRows;
    }
}

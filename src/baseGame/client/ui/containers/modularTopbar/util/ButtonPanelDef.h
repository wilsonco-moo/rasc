/*
 * ButtonPanelDef.h
 *
 *  Created on: 17 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_UTIL_BUTTONPANELDEF_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_UTIL_BUTTONPANELDEF_H_

namespace rasc {
    
    /**
     * Defines button panels.
     * Format: name, class name, friendly name.
     */
    #define RASC_MODULAR_TOPBAR_BUTTON_PANELS                               \
        X(splitMerge, SplitMergeButtons, "Split/merge")                     \
        X(unitPlace, UnitPlacementButtons, "Unit place")                    \
        X(build, BuildButtons, "Build")                                     \
        X(simpleArmyFunction, SimpleArmyFunctionButtons, "Simple")        \
        X(advancedArmyFunction, AdvancedArmyFunctionButtons, "Advanced")

    /**
     * Enum for button panels.
     */
    #define X(name, ...) name,
    class ButtonPanelMode { public: enum { RASC_MODULAR_TOPBAR_BUTTON_PANELS COUNT, INVALID }; };
    #undef X
}

#endif

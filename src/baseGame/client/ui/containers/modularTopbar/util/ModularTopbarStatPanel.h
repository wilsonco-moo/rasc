/*
 * ModularTopbarStatPanel.h
 *
 *  Created on: 11 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_UTIL_MODULARTOPBARSTATPANEL_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_UTIL_MODULARTOPBARSTATPANEL_H_

#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/util/EmplaceVector.h>
#include <string>

#include "../../../customComponents/zeroPrefixedNumber/ZeroPrefixedNumberProvstat.h"

namespace rasc {

    /**
     *
     */
    class ModularTopbarStatPanel : public rascUI::FrontPanel {
    public:
        class Row {
        public:
            std::string text;
            ZeroPrefixedNumberProvstat zeroPrefix;
            bool showZeroPrefix;
            Row(void);
        };
        
    private:
        unsigned int titleIcon, textLength, drawnRows;
        std::string titleText;
        rascUI::EmplaceVector<Row> rows;
        
    public:
        ModularTopbarStatPanel(const rascUI::Location & location = rascUI::Location(),
                               unsigned int titleIcon = 0, const std::string & titleText = "",
                               unsigned int textLength = 5, unsigned int rowCount = 3);
        virtual ~ModularTopbarStatPanel(void);
        
    private:
        void drawDivide(unsigned int id);
    protected:
        virtual void onDraw(void) override;
            
    public:
        /**
         * Allows access to rows.
         */
        Row & getRow(unsigned int id);
        const Row & getRow(unsigned int id) const;
        
        /**
         * Allows changing count of drawn rows.
         */
        void setDrawnRows(unsigned int drawnRows);
        
        /**
         * Gets the number of rows currently being
         * drawn.
         */
        inline unsigned int getDrawnRows(void) const {
            return drawnRows;
        }
    };
}

#endif

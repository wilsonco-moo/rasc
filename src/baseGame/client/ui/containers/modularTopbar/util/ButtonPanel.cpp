/*
 * ButtonPanel.cpp
 *
 *  Created on: 17 Jan 2021
 *      Author: wilson
 */

#include "ButtonPanel.h"

#include <tinyxml2/tinyxml2.h>
#include <unordered_set>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <string>
#include <array>

#include "../../../../../common/util/XMLUtil.h"
#include "ButtonPanelDef.h"
#include "ButtonPanels.h"

namespace rasc {
    
    // Button mode names.
    #define X(name, ...) #name,
    static const std::array<const char *, ButtonPanelMode::COUNT> buttonModeNames = { RASC_MODULAR_TOPBAR_BUTTON_PANELS };
    #undef X

    // Button mode friendly names.
    #define X(name, className, friendlyName, ...) friendlyName,
    static const std::array<std::string, ButtonPanelMode::COUNT> buttonModeFriendlyNames = { RASC_MODULAR_TOPBAR_BUTTON_PANELS };
    #undef X

    // Button name to mode conversion.
    #define X(name, ...) {#name, ButtonPanelMode::name},
    static const std::unordered_map<std::string, unsigned int> buttonNameToModeMap = { RASC_MODULAR_TOPBAR_BUTTON_PANELS };
    #undef X
    static unsigned int buttonNameToMode(const char * name, const unsigned int * availableModes, unsigned int availableModeCount) {
        // Find button mode in map.
        auto iter = buttonNameToModeMap.find(std::string(name));
        if (iter == buttonNameToModeMap.end()) {
            std::cerr << "CRITICAL ERROR: ButtonPanel: " << "Invalid button mode: " << name << " in XML file.\n";
            exit(EXIT_FAILURE);
        }
        // Only return if it is one of the available modes.
        unsigned int mode = iter->second;
        for (unsigned int i = 0; i < availableModeCount; i++) {
            if (availableModes[i] == mode) {
                return mode;
            }
        }
        std::cerr << "CRITICAL ERROR: ButtonPanel: " << "Button panel mode: " << name << " (from XML file) not currently available.\n";
        return 0;
    }
    
    ButtonPanel::ButtonPanel(const rascUI::Location & location) :
        location(location),
        buttonPanel(NULL),
        buttonMode(ButtonPanelMode::INVALID) {
    }
    
    ButtonPanel::~ButtonPanel(void) {
        if (buttonPanel != NULL) {
            delete buttonPanel;
        }
    }
    
    void ButtonPanel::initXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem, unsigned int defaultButtonPanelMode) {
        tinyxml2::XMLElement * buttonsElem = doc->NewElement("buttons");
        buttonsElem->SetAttribute("mode", buttonModeNames[defaultButtonPanelMode]);
        elem->InsertEndChild(buttonsElem);
    }
    
    void ButtonPanel::saveToXML(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
        tinyxml2::XMLElement * buttonsElem = doc->NewElement("buttons");
        buttonsElem->SetAttribute("mode", buttonModeNames[buttonMode]);
        elem->InsertEndChild(buttonsElem);
    }
    
    void ButtonPanel::loadFromXML(tinyxml2::XMLElement * elem, const unsigned int * availableModes, unsigned int availableModeCount, rascUI::Container * parentContainer, RascBox & box, rascUI::Theme * theme, bool enabled) {
        tinyxml2::XMLElement * buttonsElem = readElement(elem, "buttons");
        setButtonMode(parentContainer, box, theme, enabled,
                      buttonNameToMode(readAttribute(buttonsElem, "mode"), availableModes, availableModeCount));
    }

    void ButtonPanel::setButtonMode(rascUI::Container * parentContainer, RascBox & box, rascUI::Theme * theme, bool enabled, unsigned int newButtonMode) {
        // Only switch mode if needed, or if three enabled differs to button existence.
        if (buttonMode != newButtonMode || (enabled == (buttonPanel == NULL))) {
            buttonMode = newButtonMode;
            
            // Remove/delete old button panel.
            if (buttonPanel != NULL) {
                parentContainer->remove(buttonPanel);
                delete buttonPanel;
                buttonPanel = NULL;
            }
            
            // Create new button panel only if three row mode isn't enabled.
            if (enabled) {
                switch(buttonMode) {
                    #define X(name, className, ...)                                          \
                        case ButtonPanelMode::name:                                          \
                            buttonPanel = new buttonPanels::className(location, box, theme); \
                            break;
                    RASC_MODULAR_TOPBAR_BUTTON_PANELS
                    #undef X
                }
                parentContainer->add(buttonPanel);
            }
        }
    }
    
    const std::string & ButtonPanel::getButtonModeFriendlyName(unsigned int id) {
        return buttonModeFriendlyNames[id];
    }
}

/*
 * ButtonPanels.cpp
 *
 *  Created on: 17 Jan 2021
 *      Author: wilson
 */

#include "ButtonPanels.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#include "../../../../../common/gameMap/ArmySelectController.h"
#include "../../../../../common/util/definitions/IconDef.h"
#include "../../../../config/UnitPlacementController.h"
#include "../../../../gameMap/ClientGameMap.h"
#include "../../../GameUIMenuDef.h"
#include "../../../../RascBox.h"

#define LAYOUT_THEME theme

namespace rasc {
    namespace buttonPanels {
        
        // =============================================== Split merge buttons ====================================================
    
        #define SPLITMERGE_BUTTON_LOC(id) \
            BORDERTABLE_X(id, 2, GEN_FILL)
        
        SplitMergeButtons::SplitMergeButtons(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme) :
            rascUI::Container(location),
            box(box),
            splitButton(rascUI::Location(SPLITMERGE_BUTTON_LOC(0)), "Split",
                [this](GLfloat viewX, GLfloat viewY, int button) {
                    if (button == getMb()) this->box.runSplitArmies();
                }
            ),
            mergeButton(rascUI::Location(SPLITMERGE_BUTTON_LOC(1)), "Merge",
                [this](GLfloat viewX, GLfloat viewY, int button) {
                    if (button == getMb()) this->box.runMergeArmies();
                }
            ),
            splitToken(box.map->getArmySelectController().addOnSplittableStatusChange([this](bool splittable) {
                splitButton.setActive(splittable);
            })),
            mergeToken(box.map->getArmySelectController().addOnMergeableStatusChange([this](bool mergeable) {
                mergeButton.setActive(mergeable);
            })) {
            add(&splitButton);
            add(&mergeButton);
            splitButton.setActive(box.map->getArmySelectController().isSplittable());
            mergeButton.setActive(box.map->getArmySelectController().isMergeable());
        }

        SplitMergeButtons::~SplitMergeButtons(void) {
            box.map->getArmySelectController().removeOnSplittableStatusChange(splitToken);
            box.map->getArmySelectController().removeOnMergeableStatusChange(mergeToken);
        }
        
        // =============================================== Unit placement buttons =================================================
    
        #define UNITPLACEMENT_BUTTON_LOC(id) \
            BORDERTABLE_X(id, 4, GEN_FILL)
        
        UnitPlacementButtons::UnitPlacementButtons(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme) :
            rascUI::Container(location),
            box(box),
            series(false, [this](unsigned long placementMode, rascUI::ToggleButton * toggleButton) {
                getTopLevel()->afterEvent->addFunction([this, placementMode](void) {
                    this->box.unitPlacementController->setMode(placementMode, false);
                });
            }),
            unitButtonX1(&series, rascUI::Location(UNITPLACEMENT_BUTTON_LOC(0)), "x1"),
            unitButtonX2(&series, rascUI::Location(UNITPLACEMENT_BUTTON_LOC(1)), "x2"),
            unitButtonX3(&series, rascUI::Location(UNITPLACEMENT_BUTTON_LOC(2)), "x3"),
            unitButtonAll(&series, rascUI::Location(UNITPLACEMENT_BUTTON_LOC(3)), "A"),
            modeChangeToken(box.unitPlacementController->addOnModeChangeFunc([this](UnitPlacementMode placementMode) {
                series.setSelectedId(placementMode);
            })) {
            add(&unitButtonX1);
            add(&unitButtonX2);
            add(&unitButtonX3);
            add(&unitButtonAll);
            series.setSelectedId(box.unitPlacementController->getMode());
        }

        UnitPlacementButtons::~UnitPlacementButtons(void) {
            box.unitPlacementController->removeOnModeChangeFunc(modeChangeToken);
        }
        
        // =================================================== Build buttons ==================================================
        
        #define BUILD_BUTTON_LOC \
            GEN_FILL
        
        BuildButtons::BuildButtons(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme) :
            rascUI::Container(location),
            box(box),
            buildButton(rascUI::Location(BUILD_BUTTON_LOC), IconDef::build, "Build",
                [this](GLfloat viewX, GLfloat viewY, int button) {
                    if (button == getMb()) this->box.uiOpenOverlayMenu(GameUIMenus::buildMenu, true);
                }
            ) {
            add(&buildButton);
        }

        BuildButtons::~BuildButtons(void) {
        }
        
        // =================================================== Simple army function buttons ===================================
        
        #define SIMPLE_ARMY_FUNCTION_BUTTON_LOC(id) \
            MOVE_BORDERNORMAL_Y(id,                 \
             PIN_NORMAL_T(                          \
              GEN_FILL                              \
            ))
        
        SimpleArmyFunctionButtons::SimpleArmyFunctionButtons(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme) :
            rascUI::Container(location),
            box(box),
            series(false, [this](unsigned long placementMode, rascUI::ToggleButton * toggleButton) {
                getTopLevel()->afterEvent->addFunction([this, placementMode](void) {
                    this->box.unitPlacementController->setMode(placementMode, false);
                });
            }),
            unitButtonX1(&series, rascUI::Location(SIMPLE_ARMY_FUNCTION_BUTTON_LOC(0)), "x1"),
            unitButtonX2(&series, rascUI::Location(SIMPLE_ARMY_FUNCTION_BUTTON_LOC(1)), "x2"),
            unitButtonX3(&series, rascUI::Location(SIMPLE_ARMY_FUNCTION_BUTTON_LOC(2)), "x3"),
            unitButtonAll(&series, rascUI::Location(SIMPLE_ARMY_FUNCTION_BUTTON_LOC(3)), "A"),
            modeChangeToken(box.unitPlacementController->addOnModeChangeFunc([this](UnitPlacementMode placementMode) {
                series.setSelectedId(placementMode);
            })) {
            add(&unitButtonX1);
            add(&unitButtonX2);
            add(&unitButtonX3);
            add(&unitButtonAll);
            series.setSelectedId(box.unitPlacementController->getMode());
        }

        SimpleArmyFunctionButtons::~SimpleArmyFunctionButtons(void) {
            box.unitPlacementController->removeOnModeChangeFunc(modeChangeToken);
        }
        
        // ==================================================Advanced army function buttons ===================================
        
        #define ADVANCED_ARMY_FUNCTION_BUTTON_CELL_LOC(xPos, yPos) \
            BORDERTABLE_X(xPos, 2,                                 \
             ADVANCED_ARMY_FUNCTION_BUTTON_ROW_LOC(yPos)           \
            )

        #define ADVANCED_ARMY_FUNCTION_BUTTON_ROW_LOC(yPos) \
            MOVE_BORDERNORMAL_Y(yPos,                       \
             PIN_NORMAL_T(                                  \
              GEN_FILL                                      \
            ))
            
        AdvancedArmyFunctionButtons::AdvancedArmyFunctionButtons(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme) :
            rascUI::Container(location),
            box(box),
            series(false, [this](unsigned long placementMode, rascUI::ToggleButton * toggleButton) {
                getTopLevel()->afterEvent->addFunction([this, placementMode](void) {
                    this->box.unitPlacementController->setMode(placementMode, false);
                });
            }),
            unitButtonX1(&series, rascUI::Location(ADVANCED_ARMY_FUNCTION_BUTTON_CELL_LOC(0, 0)), "x1"),
            unitButtonX2(&series, rascUI::Location(ADVANCED_ARMY_FUNCTION_BUTTON_CELL_LOC(1, 0)), "x2"),
            unitButtonX3(&series, rascUI::Location(ADVANCED_ARMY_FUNCTION_BUTTON_CELL_LOC(0, 1)), "x3"),
            unitButtonAll(&series, rascUI::Location(ADVANCED_ARMY_FUNCTION_BUTTON_CELL_LOC(1, 1)), "A"),
            splitButton(rascUI::Location(ADVANCED_ARMY_FUNCTION_BUTTON_CELL_LOC(0, 2)), "Split",
                [this](GLfloat viewX, GLfloat viewY, int button) {
                    if (button == getMb()) this->box.runSplitArmies();
                }
            ),
            mergeButton(rascUI::Location(ADVANCED_ARMY_FUNCTION_BUTTON_CELL_LOC(1, 2)), "Merge",
                [this](GLfloat viewX, GLfloat viewY, int button) {
                    if (button == getMb()) this->box.runMergeArmies();
                }
            ),
            statsButton(rascUI::Location(ADVANCED_ARMY_FUNCTION_BUTTON_ROW_LOC(3)), "Stats"),
            modeChangeToken(box.unitPlacementController->addOnModeChangeFunc([this](UnitPlacementMode placementMode) {
                series.setSelectedId(placementMode);
            })),
            splitToken(box.map->getArmySelectController().addOnSplittableStatusChange([this](bool splittable) {
                splitButton.setActive(splittable);
            })),
            mergeToken(box.map->getArmySelectController().addOnMergeableStatusChange([this](bool mergeable) {
                mergeButton.setActive(mergeable);
            })) {
            add(&unitButtonX1);
            add(&unitButtonX2);
            add(&unitButtonX3);
            add(&unitButtonAll);
            add(&splitButton);
            add(&mergeButton);
            add(&statsButton);
            series.setSelectedId(box.unitPlacementController->getMode());
            splitButton.setActive(box.map->getArmySelectController().isSplittable());
            mergeButton.setActive(box.map->getArmySelectController().isMergeable());
        }

        AdvancedArmyFunctionButtons::~AdvancedArmyFunctionButtons(void) {
            box.unitPlacementController->removeOnModeChangeFunc(modeChangeToken);
            box.map->getArmySelectController().removeOnSplittableStatusChange(splitToken);
            box.map->getArmySelectController().removeOnMergeableStatusChange(mergeToken);
        }
    }
}

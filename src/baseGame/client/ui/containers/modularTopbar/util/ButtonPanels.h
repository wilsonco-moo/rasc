/*
 * ButtonPanels.h
 *
 *  Created on: 17 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_UTIL_BUTTONPANELS_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_UTIL_BUTTONPANELS_H_

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/base/Container.h>

#include "../../../customComponents/icon/IconButton.h"

namespace rascUI {
    class Location;
    class Theme;
}

namespace rasc {
    class RascBox;
    
    /**
     * This header file (and this namespace), contains definitions for button
     * panels, used internally by ButtonPanel.
     */
    namespace buttonPanels {
        
        class SplitMergeButtons : public rascUI::Container {
        private:
            RascBox & box;
            rascUI::Button splitButton, mergeButton;
            void * splitToken, * mergeToken;
        public:
            SplitMergeButtons(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme);
            virtual ~SplitMergeButtons(void);
        };
        
        class UnitPlacementButtons : public rascUI::Container {
        private:
            RascBox & box;
            rascUI::ToggleButtonSeries series;
            rascUI::ToggleButton unitButtonX1, unitButtonX2, unitButtonX3, unitButtonAll;
            void * modeChangeToken;
        public:
            UnitPlacementButtons(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme);
            virtual ~UnitPlacementButtons(void);
        };
        
        class BuildButtons : public rascUI::Container {
        private:
            RascBox & box;
            IconButton buildButton;
        public:
            BuildButtons(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme);
            virtual ~BuildButtons(void);
        };
        
        class SimpleArmyFunctionButtons : public rascUI::Container {
        private:
            RascBox & box;
            rascUI::ToggleButtonSeries series;
            rascUI::ToggleButton unitButtonX1, unitButtonX2, unitButtonX3, unitButtonAll;
            void * modeChangeToken;
        public:
            SimpleArmyFunctionButtons(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme);
            virtual ~SimpleArmyFunctionButtons(void);
        };
        
        class AdvancedArmyFunctionButtons : public rascUI::Container {
        private:
            RascBox & box;
            rascUI::ToggleButtonSeries series;
            rascUI::ToggleButton unitButtonX1, unitButtonX2, unitButtonX3, unitButtonAll;
            rascUI::Button splitButton, mergeButton, statsButton;
            void * modeChangeToken, * splitToken, * mergeToken;
        public:
            AdvancedArmyFunctionButtons(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme);
            virtual ~AdvancedArmyFunctionButtons(void);
        };
    }
}

#endif

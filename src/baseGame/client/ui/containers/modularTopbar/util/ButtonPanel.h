/*
 * ButtonPanel.h
 *
 *  Created on: 17 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_UTIL_BUTTONPANEL_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_UTIL_BUTTONPANEL_H_

#include <rascUI/util/Location.h>
#include <string>

namespace tinyxml2 {
    class XMLDocument;
    class XMLElement;
}

namespace rascUI {
    class Container;
    class Theme;
}

namespace rasc {
    class RascBox;
    
    /**
     * This contains a button panel. Mode and enable status can be switched.
     * Doing this adds and removes a relevant button panel to the parent container.
     */
    class ButtonPanel {
    private:
        rascUI::Location location;
        rascUI::Container * buttonPanel;
        unsigned int buttonMode;

    public:
        ButtonPanel(const rascUI::Location & location = rascUI::Location());
        virtual ~ButtonPanel(void);
        
        /**
         * Initialises, saves, and loads XML config for this button panel.
         * It is assumed that either loadXML or updateButtonMode is called, before saveToXML.
         * The loadFromXML method takes an array of available modes. If the mode from
         * XML is not one of these, exit(EXIT_FAILURE) is run.
         */
        static void initXMLConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem, unsigned int defaultButtonPanelMode);
        void saveToXML(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem);
        void loadFromXML(tinyxml2::XMLElement * elem,
                         const unsigned int * availableModes, unsigned int availableModeCount,
                         rascUI::Container * parentContainer, RascBox & box, rascUI::Theme * theme, bool enabled);
        
        /**
         * Sets button mode. Pass false to enabled if the button panel should be removed.
         */
        void setButtonMode(rascUI::Container * parentContainer, RascBox & box, rascUI::Theme * theme, bool enabled,
                           unsigned int buttonMode);

        /**
         * Gets current button mode.
         */
        inline unsigned int getButtonMode(void) const {
            return buttonMode;
        }
        
        /**
         * Gets friendly name of button mode.
         */
        static const std::string & getButtonModeFriendlyName(unsigned int id);
    };
}

#endif

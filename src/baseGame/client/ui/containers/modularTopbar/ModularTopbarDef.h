/*
 * ModularTopbarDef.h
 *
 *  Created on: 13 Dec 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULARTOPBARDEF_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_MODULARTOPBAR_MODULARTOPBARDEF_H_

#include <cstdint>

namespace rasc {
    
    /**
     * X macro defining modular topbar modules.
     * Format: name, class name, friendly name.
     */
    #define RASC_MODULAR_TOPBAR_LIST                         \
        X(manpower, ManpowerModule, "Manpower")              \
        X(currency, CurrencyModule, "Currency")              \
        X(flag, FlagModule, "Flag")                          \
        X(armyFunction, ArmyFunctionModule, "Army function") \
        X(divider, DividerModule, "Divider")
    
    /**
     * Enum of modular topbar module types.
     */
    class ModuleTypes {
    public:
        #define X(name, ...) name,
        enum { RASC_MODULAR_TOPBAR_LIST COUNT };
        #undef X
    };
}

#endif

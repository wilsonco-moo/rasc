/*
 * LobbyProvincePopup.cpp
 *
 *  Created on: 12 Aug 2023
 *      Author: wilson
 */

#include "LobbyProvincePopup.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <cstring>

#include "../../../../common/util/numeric/NumberFormat.h"
#include "../../../../common/util/definitions/IconDef.h"
#include "../../../../common/playerData/PlayerData.h"
#include "../../../gameMap/ClientProvince.h"
#include "../../../gameMap/ClientGameMap.h"
#include "../../../../common/util/Misc.h"
#include "../../../RascClientNetIntf.h"
#include "../../GameUIMenuDef.h"
#include "../../../RascBox.h"

#define LAYOUT_THEME theme

#define TITLE_LOC  \
    PIN_NORMAL_T(  \
     GEN_BORDER    \
    )

#define BUTTONS_LOC \
    PIN_NORMAL_B(   \
     GEN_BORDER     \
    )

#define CANCEL_BUTTON_LOC \
    BORDERTABLE_X(0, 2,   \
     BUTTONS_LOC          \
    )

#define CLAIM_BUTTON_LOC  \
    BORDERTABLE_X(1, 2,   \
     BUTTONS_LOC          \
    )

#define DESCRIPTION_PANEL_HEIGHT (DESCRIPTION_PANEL_ROWS * UI_BHEIGHT)
#define DESCRIPTION_PANEL_LOC      \
    SUB_BORDERMARGIN_Y(UI_BHEIGHT, \
     GEN_BORDER                    \
    )

#define DESCRIPTION_PANEL_ROWS 4
#define DESCRIPTION_LABEL_LOC(row)       \
    TABLE_Y(row, DESCRIPTION_PANEL_ROWS, \
     DESCRIPTION_PANEL_LOC               \
    )

#define OVERALL_WIDTH 366
#define OVERALL_HEIGHT (DESCRIPTION_PANEL_HEIGHT + (2.0f * UI_BHEIGHT) + (4.0f * UI_BORDER))
#define OVERALL_LOC GEN_CENTRE_FIXED(OVERALL_WIDTH, OVERALL_HEIGHT)

namespace rasc {

    LobbyProvincePopup::LobbyProvincePopup(RascBox & box, rascUI::Theme * theme) :
        rascUI::FloatingPanel(rascUI::Location(OVERALL_LOC)),
        box(box),
        selectedProvince(NULL),
        titlePanel(rascUI::Location(TITLE_LOC)),
        titleLabel(rascUI::Location(TITLE_LOC), IconDef::alert),
        
        descriptionPanel(rascUI::Location(DESCRIPTION_PANEL_LOC)),
        descriptionTitle0(rascUI::Location(DESCRIPTION_LABEL_LOC(0)), "Starting here would be difficult or"),
        descriptionTitle1(rascUI::Location(DESCRIPTION_LABEL_LOC(1)), "impossible. Province output:"),
        manpowerLabel(rascUI::Location(DESCRIPTION_LABEL_LOC(2)), IconDef::manpower),
        currencyLabel(rascUI::Location(DESCRIPTION_LABEL_LOC(3)), IconDef::currency),
        
        cancelButton(rascUI::Location(CANCEL_BUTTON_LOC), "Cancel",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) hide();
            }
        ),
        claimButton(rascUI::Location(CLAIM_BUTTON_LOC), "Claim it anyway",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) doClaim();
            }
        ) {
        
        contents.add(&titlePanel);
        contents.add(&titleLabel);
        
        contents.add(&descriptionPanel);
        contents.add(&descriptionTitle0);
        contents.add(&descriptionTitle1);
        contents.add(&manpowerLabel);
        contents.add(&currencyLabel);
        
        contents.add(&cancelButton);
        contents.add(&claimButton);
    }
    
    LobbyProvincePopup::~LobbyProvincePopup(void) {
        // If we've still got a province selected, remove our on change function.
        if (selectedProvince != NULL) {
            selectedProvince->removeOnProvinceChangeFunc(onProvinceChangeToken);
        }
    }
    
    bool LobbyProvincePopup::isProvinceTooBad(ClientProvince & province) {
        // Inaccessible province can't be claimed anyway!
        if (!province.isProvinceAccessible()) {
            return false;
        }
        
        // Somebody already owns it, so either its an AI nation which
        // can be controlled by the player, or another player so the
        // province can't be claimed anyway. Either way, no point showing
        // the warning.
        if (province.getProvinceOwner() != PlayerData::NO_PLAYER) {
            return false;
        }
        
        // Province is "too bad" if its manpower or currency balance
        // is negative. The player's only stats then would be their base
        // stats (which are small).
        const ProvinceProperties & properties = province.properties;
        const provstat_t manpowerBalance = properties[ProvStatIds::manpowerIncome] - properties[ProvStatIds::manpowerExpenditure];
        const provstat_t currencyBalance = properties[ProvStatIds::currencyIncome] - properties[ProvStatIds::currencyExpenditure];
        
        return manpowerBalance < 0 || currencyBalance < 0;
    }
    
    void LobbyProvincePopup::setSelectedProvince(ClientProvince & province) {
        // Set selected province so we know which to claim once the button
        // gets pressed. If we had a selected province already, clear the
        // on change function.
        if (selectedProvince != NULL) {
            selectedProvince->removeOnProvinceChangeFunc(onProvinceChangeToken);
        }
        selectedProvince = &province;
        
        // If our selected province gains an owner while we're open, close the
        // popup as it no longer matters.
        onProvinceChangeToken = province.addOnProvinceChangeFunc([this](void) {
            if (selectedProvince->getProvinceOwner() != PlayerData::NO_PLAYER) {
                hide();
            }
        });
        
        // Update title label.
        titleLabel.setText(province.getName() + " has low output!");
        
        {
            // Get province stats.
            const ProvinceProperties & properties = province.properties;
            const provstat_t manpowerBalance = properties[ProvStatIds::manpowerIncome] - properties[ProvStatIds::manpowerExpenditure];
            const provstat_t currencyBalance = properties[ProvStatIds::currencyIncome] - properties[ProvStatIds::currencyExpenditure];

            // Update manpower and currency labels.
            char textBuffer[17];
            NUMBER_FORMAT_FILL(textBuffer, manpower, 7, "Manpower: ", manpowerBalance, true)
            manpowerLabel.setText(textBuffer);
            NUMBER_FORMAT_FILL(textBuffer, currency, 7, "Currency: ", currencyBalance, true)
            currencyLabel.setText(textBuffer);
        }
        
        // Set keyboard selection to cancel button, as thats the most likely
        // action for the user to take.
        getTopLevel()->setKeyboardSelected(&cancelButton);
    }
    
    void LobbyProvincePopup::doClaim(void) {
        // If we're not already waiting for a lobby province response,
        // request that the province is claimed.
        if (!box.netIntf->isWaitingForServerResponseInLobby()) {
            box.netIntf->lobbyClaimProvince(selectedProvince->getId());
        }
        
        // Always hide after claim button is pressed, even if the claim
        // hasn't succeeded.
        hide();
    }
    
    void LobbyProvincePopup::onHide(void) {
        // Clear the selected province and province change function, when we're hidden.
        // Can always assume that a selected province exists if we're open.
        selectedProvince->removeOnProvinceChangeFunc(onProvinceChangeToken);
        selectedProvince = NULL;
    }

    bool LobbyProvincePopup::warnIfNeeded(const uint32_t provinceId) {
        ClientProvince & province = (ClientProvince &)box.map->getProvince(provinceId);
        
        // Province isn't bad enough to show a warning, so do nothing
        // and return false.
        if (!isProvinceTooBad(province)) {
            return false;
        }
        
        // Open using the GameUI method, to make sure anything else
        // gets closed.
        box.uiOpenOverlayMenu(GameUIMenus::lobbyProvincePopup, true, true);
        
        // After show has run, update our selected province.
        // Note that show runs as an afterEvent, so this will be
        // immediately afterward.
        getTopLevel()->afterEvent->addFunction([this, &province](void) {
            setSelectedProvince(province);
        });
        
        return true;
    }
}

/*
 * LobbyProvincePopup.h
 *
 *  Created on: 12 Aug 2023
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_LOBBYPROVINCEPOPUP_LOBBYPROVINCEPOPUP_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_LOBBYPROVINCEPOPUP_LOBBYPROVINCEPOPUP_H_

#include <rascUI/components/floating/FloatingPanel.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/Button.h>
#include <cstdint>

#include "../../customComponents/icon/IconLabel.h"

namespace rascUI {
    class Theme;
}

namespace rasc {
    class ClientProvince;
    class RascBox;

    /**
     * LobbyProvincePopup provides a warning in the lobby room, if you decide
     * to claim a province so bad it would be difficult or impossible to
     * start there.
     * It gives to the choice to cancel the selection, or continue anyway.
     */
    class LobbyProvincePopup : public rascUI::FloatingPanel {
    private:
        RascBox & box;
        ClientProvince * selectedProvince;
        void * onProvinceChangeToken;
        
        rascUI::FrontPanel titlePanel;
        IconLabel titleLabel;

        rascUI::FrontPanel descriptionPanel;
        rascUI::Label descriptionTitle0, descriptionTitle1;
        IconLabel manpowerLabel, currencyLabel;
        
        rascUI::Button cancelButton, claimButton;
        
    public:
        LobbyProvincePopup(RascBox & box, rascUI::Theme * theme);
        virtual ~LobbyProvincePopup(void);
        
    private:
        // Returns true if the province is bad enough to show the popup.
        static bool isProvinceTooBad(ClientProvince & province);
        // Set our selected province, show info about it. Run by
        // warnIfNeeded after showing the popup.
        void setSelectedProvince(ClientProvince & province);
        // Run by the claim button, sends the request off to RascClientNetIntf.
        void doClaim(void);
        
    protected:
        // Clears selected province on hide.
        virtual void onHide(void) override;
        
    public:
        /**
         * If the province is bad enough, updates our description from
         * the province, shows the popup and returns true.
         * Otherwise, returns false.
         * Safe to call anywhere since all logic is run in a function queue.
         */
        bool warnIfNeeded(const uint32_t provinceId);
    };
}

#endif

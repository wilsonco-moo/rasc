/*
 * BottomLeftNewUI.cpp
 *
 *  Created on: 26 Nov 2018
 *      Author: wilson
 */

#include "BottomLeftUI.h"

#include <rascUI/base/TopLevelContainer.h>
#include <wool/window/base/WindowBase.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/base/Component.h>
#include <rascUI/util/Location.h>
#include <rascUI/base/Theme.h>
#include <GL/gl.h>

#include "../../../../common/util/definitions/IconDef.h"
#include "../../../../common/gameMap/GameMap.h"
#include "../../../config/MapModeController.h"
#include "../../../config/CameraProperties.h"
#include "../../../RascClientNetIntf.h"
#include "../../GameUIMenuDef.h"
#include "../../../RascBox.h"

// -------------------------- GLOBAL PROPERTIES -------------------------------

    // Define macros for the theming.
    #define UI_BORDER        theme->uiBorder
    #define UI_BWIDTH        theme->normalComponentWidth
    #define UI_BHEIGHT       theme->normalComponentHeight

    // The height of the panel above the minimap
    #define TOP_PANEL_HEIGHT   (UI_BHEIGHT + 2 * UI_BORDER)

    // The width of the panel to the right of the minimap
    #define RIGHT_PANEL_WIDTH  (94 + 2 * UI_BORDER)

    // The width and height of the entire bottom left UI area
    #define BOTTOM_LEFT_WIDTH  (MINIMAP_WIDTH + RIGHT_PANEL_WIDTH)
    #define BOTTOM_LEFT_HEIGHT (MINIMAP_HEIGHT + TOP_PANEL_HEIGHT)



// ------------- LOCATIONS FOR TOP AND RIGHT BACK PANELS ----------------------

    #define TOP_PANEL_LOC            0, 0, BOTTOM_LEFT_WIDTH, TOP_PANEL_HEIGHT
    #define BACK_PANEL_LOC           MINIMAP_WIDTH, TOP_PANEL_HEIGHT, RIGHT_PANEL_WIDTH, MINIMAP_HEIGHT



// ----------- CONFIG FOR TOP BUTTONS -----------------------------------------
    
    // The number of buttons above the minimap
    #define TOP_BUTTON_COUNT         4

    // The width of one of the buttons above the minimap, ignoring borders.
    #define TOP_BUTTON_REAL_WIDTH    ((MINIMAP_WIDTH)/TOP_BUTTON_COUNT)

    // The x, y, width, height of the buttons above the minimap
    #define TOP_BUTTON_WIDTH         (TOP_BUTTON_REAL_WIDTH - UI_BORDER)
    #define TOP_BUTTON_HEIGHT        UI_BHEIGHT
    #define TOP_BUTTON_X(buttonId)   (TOP_BUTTON_REAL_WIDTH * (buttonId) + UI_BORDER)
    #define TOP_BUTTON_Y             UI_BORDER

    // The location of each top button.
    #define TOP_BUTTON_LOC(b_id)     TOP_BUTTON_X(b_id), TOP_BUTTON_Y, TOP_BUTTON_WIDTH, TOP_BUTTON_HEIGHT


// --------------- POSITION FOR BUTTONS TO THE RIGHT --------------------------

    // The height of the turn count panel
    #define TURN_COUNT_PANEL_HEIGHT  UI_BHEIGHT

    // The x position of the buttons at the right side of the minimap
    #define RIGHT_BUTTONS_X          (MINIMAP_WIDTH + UI_BORDER)

    // The location for the next turn button and turn count panel
    #define NEXT_TURN_BUTTON_LOC     RIGHT_BUTTONS_X, UI_BORDER, RIGHT_PANEL_WIDTH - 2*UI_BORDER, -TURN_COUNT_PANEL_HEIGHT - 3*UI_BORDER, 0.0f, 0.0f, 0.0f, 1.0f
    #define TURN_COUNT_PANEL_LOC     RIGHT_BUTTONS_X, -TURN_COUNT_PANEL_HEIGHT - UI_BORDER, RIGHT_PANEL_WIDTH - 2*UI_BORDER, TURN_COUNT_PANEL_HEIGHT, 0.0f, 1.0f, 0.0f, 0.0f

// ----------------------------------------------------------------------------




namespace rasc {

    BottomLeftUI::BottomLeftUI(RascBox & box, rascUI::Theme * theme) :
        rascUI::Container(rascUI::Location(0, -BOTTOM_LEFT_HEIGHT, BOTTOM_LEFT_WIDTH, BOTTOM_LEFT_HEIGHT, 0.0f, 1.0f, 0.0f, 0.0f)),
        box(box),

        // ------------------------------ Define components ------------------------------

        topPanel(rascUI::Location(TOP_PANEL_LOC)),
        rightPanel(rascUI::Location(BACK_PANEL_LOC)),

        // NOTE: No synchronisation is needed for changing map mode: Map modes are exclusively
        // controlled by the GLUT (UI) thread.
        mapModeSeries(false, [this](unsigned long mapMode, rascUI::ToggleButton * toggleButton){
            // Send false to the callOnChangeMapModeFunc parameter, as otherwise the UI would get notified
            // about the change it has just caused.
            this->box.mapModeController->setMapMode(mapMode, false);
        }),

        geographicalToggleButton(&mapModeSeries, rascUI::Location(TOP_BUTTON_LOC(0)), "Geo"),
        provincialToggleButton(  &mapModeSeries, rascUI::Location(TOP_BUTTON_LOC(1)), "Pol"),
        areaToggleButton(        &mapModeSeries, rascUI::Location(TOP_BUTTON_LOC(2)), "Reg"),
        continentalToggleButton( &mapModeSeries, rascUI::Location(TOP_BUTTON_LOC(3)), "Con"),


        nextTurnButton(rascUI::Location(NEXT_TURN_BUTTON_LOC), "Next turn",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) {
                    // Run this after event, since it modifies outside the UI.
                    getTopLevel()->afterEvent->addFunction([this](void) {
                        this->box.netIntf->finishTurn();
                    });
                }
            }
        ),

        turnCountPanel(rascUI::Location(TURN_COUNT_PANEL_LOC)) {


        // ---------------------------- Define layout ---------------------------

        add(&topPanel);
        add(&rightPanel);

        add(&geographicalToggleButton);
        add(&provincialToggleButton);
        add(&areaToggleButton);
        add(&continentalToggleButton);

        add(&nextTurnButton);
        add(&turnCountPanel);

        // ----------------------------------------------------------------------

        // Initially make the area map mode toggle button selected, as that is the default
        // map mode.
        mapModeSeries.setSelectedButton(&areaToggleButton);
    }

    BottomLeftUI::~BottomLeftUI(void) {
    }


    void BottomLeftUI::onChangeToLobbyRoom(void) {
        // This must be done when we get to the lobby room, as that is the first opportunity that
        // the GameMap becomes available.

        // Make sure the map mode toggle buttons are updated when the map mode changes. No synchronisation
        // is needed because the map mode is exclusively controlled by the GLUT (UI) thread.
        this->box.mapModeController->setOnChangeMapModeFunc([this](MapMode mapMode) {
            // Note: This won't call onChangeSelection, as otherwise the UI would notify the GameMap
            // about the change the GameMap just caused.
            mapModeSeries.setSelectedId(mapMode);
        });
    }



    #define BUFFER_LEN 20

    void BottomLeftUI::updateTurnCount(uint64_t turnCount) {
        // Run before the next event, since this modifies the user interface.
        getTopLevel()->beforeEvent->addFunction([this, turnCount](void) {
            // Make the next turn button active on the start of turns.
            nextTurnButton.setActive(true);
            turnCountPanel.setTurnCount(turnCount);
        });
    }

    void BottomLeftUI::onFinishTurn(void) {
        // Run before the next event, since this modifies the user interface.
        getTopLevel()->beforeEvent->addFunction([this](void) {
            // Make the next turn button active on the end of turns.
            nextTurnButton.setActive(false);
        });
    }

    GLfloat BottomLeftUI::getHeight(rascUI::Theme * theme) {
        return BOTTOM_LEFT_HEIGHT;
    }
}

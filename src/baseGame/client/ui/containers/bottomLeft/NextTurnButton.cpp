/*
 * NextTurnButton.cpp
 *
 *  Created on: 16 Dec 2018
 *      Author: wilson
 */

#include "../../../ui/containers/bottomLeft/NextTurnButton.h"

#include <GL/gl.h>
#include <functional>
#include <string>

#include <rascUI/base/Component.h>
#include <rascUI/base/Theme.h>
#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/Location.h>
#include <wool/font/Font.h>
#include "../../../config/MapModeController.h"

namespace rasc {

    NextTurnButton::NextTurnButton(const rascUI::Location & location, std::string text, std::function<void(GLfloat, GLfloat, int)> onMouseClickFunc) :
        rascUI::Button(location, text, onMouseClickFunc) {
    }

    NextTurnButton::~NextTurnButton(void) {
    }

    void NextTurnButton::onDraw(void) {
        rascUI::Theme * theme = getTopLevel()->theme;

        // Draw the button background.
        theme->drawButton(location);

        // Set the correct text colour for our current mouse state etc.
        theme->customSetDrawColour(theme->customGetTextColour(location));

        // Draw the text, USING MOUSE STATE.
        theme->customDrawTextTitle(location, -3.0f, 18.0f, 2.8f, 2.8f, "Next");
        theme->customDrawTextTitle(location, -3.0f, 62.0f, 2.8f, 2.8f, "turn");
    }
}

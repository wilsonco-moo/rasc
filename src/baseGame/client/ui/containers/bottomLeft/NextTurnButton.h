/*
 * NextTurnButton.h
 *
 *  Created on: 16 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_BOTTOMLEFT_NEXTTURNBUTTON_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_BOTTOMLEFT_NEXTTURNBUTTON_H_

#include <rascUI/components/generic/Button.h>

namespace rasc {

    class NextTurnButton : public rascUI::Button {
    public:
        NextTurnButton(
            const rascUI::Location & location = rascUI::Location(),
            std::string text = "",
            std::function<void(GLfloat, GLfloat, int)> onMouseClickFunc = [](GLfloat viewX, GLfloat viewY, int button){}
        );
        virtual ~NextTurnButton(void);

    protected:
        virtual void onDraw(void);
    };

}

#endif

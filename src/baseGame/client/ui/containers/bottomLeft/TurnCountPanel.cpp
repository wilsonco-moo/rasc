/*
 * TurnCountPanel.cpp
 *
 *  Created on: 16 Dec 2018
 *      Author: wilson
 */

#include "TurnCountPanel.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/base/Component.h>
#include <rascUI/base/Theme.h>

#include "../../../../common/util/numeric/NumberFormat.h"
#include "../../../config/MapModeController.h"

namespace rasc {

    #define TURN_COUNT_BUFFSIZE 5
    
    TurnCountPanel::TurnCountPanel(const rascUI::Location & location) :
        rascUI::FrontPanel(location),
        turnCount(TURN_COUNT_BUFFSIZE, &NumberFormat::generic<TURN_COUNT_BUFFSIZE>) {
    }

    TurnCountPanel::~TurnCountPanel(void) {
    }

    void TurnCountPanel::onDraw(void) {
        FrontPanel::onDraw();

        // Get our theme, for convenience
        rascUI::Theme * theme = getTopLevel()->theme;

        // Draw the word "Turn"
        theme->customSetDrawColour(theme->customColourMainText);
        theme->customDrawTextNoMouse(location, 0.0f, 0.0f, 1.0f, 1.0f, "Turn");

        // Draw the turn count  (offsetX, offsetY, scaleX, scaleY)
        turnCount.draw(location, theme, theme->customTextCharWidth * 4.5f, 0.0f, 1.0f, 1.0f);
    }

    void TurnCountPanel::setTurnCount(uint64_t turnCount) {
        getTopLevel()->beforeEvent->addFunction([this, turnCount](void) {
            this->turnCount = (int64_t)turnCount;
        });
    }
}

/*
 * BottomLeftNewUI.h
 *
 *  Created on: 26 Nov 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_NEWUI_CONTAINERS_BOTTOMLEFTNEWUI_H_
#define BASEGAME_CLIENT_NEWUI_CONTAINERS_BOTTOMLEFTNEWUI_H_

#include <cstdint>

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/base/Container.h>

#include "NextTurnButton.h"
#include "TurnCountPanel.h"

namespace rascUI {
    class TopLevelContainer;
    class Theme;
}

namespace rasc {

    class RascBox;


    class BottomLeftUI : public rascUI::Container {
    private:
        RascBox & box;

        // Declare components
        rascUI::BackPanel topPanel, rightPanel;

        // Declare the toggle button series for the map modes
        rascUI::ToggleButtonSeries mapModeSeries;

        // Declare the toggle buttons for the map modes
        rascUI::ToggleButton geographicalToggleButton,
                             provincialToggleButton,
                             areaToggleButton,
                             continentalToggleButton;

        // The next turn button
        NextTurnButton nextTurnButton;

        // The turn counter panel
        TurnCountPanel turnCountPanel;

    public:
        BottomLeftUI(RascBox & box, rascUI::Theme * theme);
        virtual ~BottomLeftUI(void);


        /**
         * This should be called by RascClientNetIntf whenever we move to a new turn.
         */
        void updateTurnCount(uint64_t turnCount);

        /**
         * This should be called by RascClientNetIntf when we finish a turn.
         */
        void onFinishTurn(void);


        /**
         * We need to know when we change to the lobby room, so we can connect the map mode switching events.
         *
         * This is the first opportunity that the game map becomes available.
         */
        void onChangeToLobbyRoom(void);

        /**
         * Gets the height of the bottom left UI, given the specified theme.
         */
        static GLfloat getHeight(rascUI::Theme * theme);
    };

}

#endif

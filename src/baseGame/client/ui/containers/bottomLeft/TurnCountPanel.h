/*
 * TurnCountPanel.h
 *
 *  Created on: 16 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_BOTTOMLEFT_TURNCOUNTPANEL_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_BOTTOMLEFT_TURNCOUNTPANEL_H_

#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/util/Location.h>
#include <cstdint>

#include "../../../ui/customComponents/zeroPrefixedNumber/ZeroPrefixedNumberUnsigned.h"

namespace rasc {

    class TurnCountPanel : public rascUI::FrontPanel {
    private:
        ZeroPrefixedNumberUnsigned turnCount;
    public:
        TurnCountPanel(const rascUI::Location & location = rascUI::Location());
        virtual ~TurnCountPanel(void);
    protected:
        virtual void onDraw(void) override;
    public:
        void setTurnCount(uint64_t turnCount);
    };

}

#endif

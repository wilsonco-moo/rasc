/*
 * GameMenu.cpp
 *
 *  Created on: 21 Dec 2018
 *      Author: wilson
 */

#include "GameMenu.h"

#include <rascUI/base/TopLevelContainer.h>
#include <wool/window/base/WindowBase.h>
#include <threading/ThreadQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <iostream>
#include <chrono>
#include <thread>
#include <cmath>

#include "../../customComponents/FancyTextDisplay.h"
#include "../../../../common/config/StaticConfig.h"
#include "../../../../server/RascBaseGameServer.h"
#include "../../../mainMenu/MainMenuRoom.h"
#include "../../../../server/ServerBox.h"
#include "../../../GlobalProperties.h"
#include "../../../RascBox.h"

#ifdef GAME_MENU_ENABLE_DEBUG_FLAGS
    #include "../../../../common/playerData/flag/Flag.h"
    #include "../../../../common/util/numeric/Random.h"
#endif


#define LAYOUT_THEME theme

// Overrall location for game menu. Allow additional space for test component.
#define OVERALL_WIDTH   (196 + UI_BORDER * 2 + TEST_COMP_ADD_WIDTH)
#define OVERALL_HEIGHT  (COUNT_OUTBORDER_Y(box.serverBox == NULL ? 3 : 4) + TEST_COMP_HEIGHT)
#define GAME_MENU_LOC   GEN_CENTRE_FIXED(OVERALL_WIDTH, OVERALL_HEIGHT)

// Locations for each button.
#define BUTTON_LOC(id)          \
    MOVE_BORDERNORMAL_Y(id,     \
     SETNORMAL_Y(               \
      SUB_BORDER(GAME_MENU_LOC) \
    ))

// Size of the save button.
#define SAVE_BUTTON_WIDTH 48

// Define location for test component, if enabled.
#ifdef GAME_MENU_ENABLE_TEST_COMPONENT
    #define TEST_COMP_ADD_WIDTH 150
    #define TEST_COMP_HEIGHT    250
    #define TEST_COMP_LOC                    \
        PIN_B(TEST_COMP_HEIGHT - UI_BORDER,  \
         SUB_BORDER(GAME_MENU_LOC)           \
        )
#else
    #ifdef GAME_MENU_ENABLE_DEBUG_FLAGS
        #define TEST_FLAG_WIDTH 128
        #define TEST_FLAG_HEIGHT 64
        #define TEST_FLAG_COLUMNS 6
        #define TEST_FLAG_ROWS 10
        #define TEST_AREA_WIDTH (TEST_FLAG_COLUMNS * (TEST_FLAG_WIDTH + UI_BORDER) - UI_BORDER)
        #define TEST_AREA_HEIGHT (TEST_FLAG_ROWS * (TEST_FLAG_HEIGHT + UI_BORDER) - UI_BORDER)
        #define TEST_AREA_LOC             \
            PIN_B(TEST_AREA_HEIGHT,       \
                SUB_BORDER(GAME_MENU_LOC) \
            )
        #undef OVERALL_WIDTH
        #define OVERALL_WIDTH (TEST_AREA_WIDTH + UI_BORDER + UI_BORDER)
        #define TEST_COMP_HEIGHT (TEST_AREA_HEIGHT + UI_BORDER)
        #define TEST_FLAG_LOC(x, y)                                 \
            BORDERTABLE_XY(x, y, TEST_FLAG_COLUMNS, TEST_FLAG_ROWS, \
             TEST_AREA_LOC                                          \
            )
    #else
        #define TEST_COMP_ADD_WIDTH 0.0f
        #define TEST_COMP_HEIGHT    0.0f
    #endif
#endif

namespace rasc {

    GameMenu::GameMenu(RascBox & box, rascUI::Theme * theme) :
        rascUI::FloatingPanel(
            rascUI::Location(GAME_MENU_LOC),
            [this](void) {
                // On show
                getTopLevel()->setKeyboardSelected(&continueButton);
            },
            [this](void) {
                // On hide
                this->box.uiKeyboardSelectMenuButton();
            }
        ),
        box(box),
        exitButton(rascUI::Location(BUTTON_LOC(box.serverBox == NULL ? 2 : 3)), "Exit Rasc",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) this->box.window->exitLater(); // No synchronisation is needed here: This can be called from anywhere in the GLUT thread.
            }
        ),
        returnToMainMenuButton(rascUI::Location(BUTTON_LOC(box.serverBox == NULL ? 1 : 2)), "Return to main menu",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb())
                    this->box.window->changeRoom(new MainMenuRoom(this->box.globalProperties));
            }
        ),
        continueButton(rascUI::Location(BUTTON_LOC(0)), "Resume",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) hide();
            }
        ),
        saveButton(rascUI::Location(PIN_L(SAVE_BUTTON_WIDTH, BUTTON_LOC(1))), "Save",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) {
                    saveFloatingPanel.show();
                }
            }
        ),
        saveNameBox(rascUI::Location(SUB_BORDERMARGIN_L(SAVE_BUTTON_WIDTH, BUTTON_LOC(1))), box.globalProperties->saveName,
            [this](const std::string & str){
                this->box.globalProperties->saveName = str;
            }
        ),
        token(box.globalProperties->threadQueue->createToken()),

        #ifdef GAME_MENU_ENABLE_TEST_COMPONENT
            testComp(new FancyTextDisplay(rascUI::Location(TEST_COMP_LOC), theme,
                "This text :manpower: contains icons, :currency: and uses "
                ":currency: real-time:garrison: dynamic :unrest: word-wrapping! "
                "This:manpower:was somewhat of a pain to do, as I am sure you can "
                "imagine.\n\nIt even has basic :manpower:formatting like:unrest: "
                "the inclusion of newline characters.",
                true, false, false
            )),
        #endif
        
                saveFloatingPanel(
            box.globalProperties->threadQueue,
            token,
            theme,
            false,
            rascUI::Location(GEN_CENTRE_FIXED(400, 128)),
            [this](void) { // onProgressShowFunc
                 saveFloatingPanel.topLabel.setText("Saving...");
                 saveFloatingPanel.bottomLabel.setText("");
            },
            [this](void)->bool { // backgroundJobFunc
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                return this->box.serverBox->server->saveGame(StaticConfig::getFullSaveFilepath(this->box.globalProperties->saveName));
            },
            [this](void) { // onWaitFunc
            },
            [this](bool success) { // onEndFunc
                if (success) {
                    saveFloatingPanel.topLabel.setText("Saving succeeded.");
                    saveFloatingPanel.bottomLabel.setText(std::string("Saved as ") + this->box.globalProperties->saveName + StaticConfig::SAVEFILE_EXTENSION);
                } else {
                    saveFloatingPanel.topLabel.setText("Saving failed.");
                    saveFloatingPanel.bottomLabel.setText("Try changing the save name.");
                }
            },
            [this](void){ // onProgressHideFunc
            }
        ) {

        contents.add(&continueButton);
        if (box.serverBox != NULL) {
            contents.add(&saveButton);
            contents.add(&saveNameBox);
            saveFloatingPanel.setSelectWhenHide(&saveButton);
            add(&saveFloatingPanel);
        }
        contents.add(&returnToMainMenuButton);
        contents.add(&exitButton);
        #ifdef GAME_MENU_ENABLE_TEST_COMPONENT
            contents.add(new ColourComponent(rascUI::Location(TEST_COMP_LOC)));
            contents.add(testComp);
        #endif
        #ifdef GAME_MENU_ENABLE_DEBUG_FLAGS
            Random random;
            static_assert(flags.max_size() == TEST_FLAG_COLUMNS * TEST_FLAG_ROWS, "Wrong size of flag array!");
            for (unsigned int iy = 0; iy < TEST_FLAG_ROWS; iy++) {
                for (unsigned int ix = 0; ix < TEST_FLAG_COLUMNS; ix++) {
                    flags.emplace(rascUI::Location(TEST_FLAG_LOC(ix, iy)), Flag(random, Flag::CreateMode::random));
                    add(&flags.back());
                }
            }
        #endif
    }

    GameMenu::~GameMenu(void) {
        box.globalProperties->threadQueue->deleteToken(token);
        #ifdef GAME_MENU_ENABLE_TEST_COMPONENT
            delete testComp;
        #endif
    }

    #ifdef GAME_MENU_ENABLE_TEST_COMPONENT
        void GameMenu::onDraw(void) {
            static GLfloat n = 0.0f;
            n += 0.01;
            rascUI::Theme * theme = getTopLevel()->theme;
            GLfloat widthOff = (sin(n) + 1.0f) * 50;
            rascUI::Location loc(SUB_MARGIN_R(widthOff, PIN_B(TEST_COMP_HEIGHT - UI_BORDER, SUB_BORDER(GAME_MENU_LOC))));
            testComp->location.position = loc.position;
            testComp->location.weight = loc.weight;
            childRecalculate(testComp, location.cache);
            Container::onDraw();
        }
    #endif
}

/*
 * GameMenu.h
 *
 *  Created on: 21 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CONTAINERS_GAMEMENU_GAMEMENU_H_
#define BASEGAME_CLIENT_UI_CONTAINERS_GAMEMENU_GAMEMENU_H_

// Uncomment this to allow space at the bottom for a test component within game menu.
//#define GAME_MENU_ENABLE_TEST_COMPONENT
// Uncomment to draw a load of flags.
//#define GAME_MENU_ENABLE_DEBUG_FLAGS

#include <rascUI/components/textEntry/BasicTextEntryBox.h>
#include <rascUI/components/floating/FloatingPanel.h>
#include <rascUI/components/generic/Button.h>

#include "../../customComponents/ProgressFloatingPanel.h"

#ifdef GAME_MENU_ENABLE_DEBUG_FLAGS
    #include <rascUI/util/EmplaceArray.h>
    #include "../../customComponents/flag/FlagComponent.h"
#endif

namespace rascUI {
    class Component;
    class Theme;
}

namespace rasc {
    class RascBox;
    
    class GameMenu : public rascUI::FloatingPanel {
    private:
        RascBox & box;
        rascUI::Button exitButton, returnToMainMenuButton, continueButton, saveButton;
        rascUI::BasicTextEntryBox saveNameBox;
        void * token;
        #ifdef GAME_MENU_ENABLE_TEST_COMPONENT
            rascUI::Component * testComp;
        #endif
        #ifdef GAME_MENU_ENABLE_DEBUG_FLAGS
            rascUI::EmplaceArray<FlagComponent, 60> flags;
        #endif
        ProgressFloatingPanel saveFloatingPanel;

    public:
        GameMenu(RascBox & box, rascUI::Theme * theme);
        virtual ~GameMenu(void);

    protected:
        #ifdef GAME_MENU_ENABLE_TEST_COMPONENT
            virtual void onDraw(void) override;
        #endif
    };
}

#endif

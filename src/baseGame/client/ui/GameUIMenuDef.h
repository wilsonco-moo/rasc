/*
 * GameUIMenuDef.h
 *
 *  Created on: 19 Jan 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_GAMEUIMENUDEF_H_
#define BASEGAME_CLIENT_UI_GAMEUIMENUDEF_H_

#include "../../common/config/ReleaseMode.h"

namespace rasc {
    
    /**
     * This X macro and related enum defines overlay menus,
     * which can be open. These names correspond to members of GameUI. Use
     * RASC_GAMEUI_MENU_DEV for an overlay menu only used in development mode.
     *
     * By opening and closing menus using the methods
     * from GameUI, we can ensure that only one of these is open at once.
     */
    #define RASC_GAMEUI_MENU_LIST       \
        X(lobbyProvincePopup)           \
        X(gameMenu)                     \
        X(billMenu)                     \
        RASC_GAMEUI_MENU_DEV(console)   \
        X(modularTopbarConfigMenu)      \
        X(buildMenu)
    
    // Ignore development menus in release mode, use them in development mode.
    #ifdef RASC_RELEASE_MODE
        #define RASC_GAMEUI_MENU_DEV(...)
    #endif
    #ifdef RASC_DEVELOPMENT_MODE
        #define RASC_GAMEUI_MENU_DEV(...) X(__VA_ARGS__)
    #endif
    
    /**
     * Enum for game UI menus overlay menus.
     */
    #define X(name, ...) name,
    class GameUIMenus { public: enum { RASC_GAMEUI_MENU_LIST INVALID }; };
    #undef X
}

#endif

/*
 * JobStatusComponent.cpp
 *
 *  Created on: 5 Feb 2019
 *      Author: wilson
 */

#include "JobStatusComponent.h"

#include <GL/gl.h>
#include <string>

#include <rascUI/base/Theme.h>
#include <rascUI/base/TopLevelContainer.h>
#include <threading/JobStatus.h>
#include <threading/ThreadQueue.h>
#include <wool/font/Font.h>
#include <wool/misc/RGB.h>
#include "../../config/MapModeController.h"


// Define macros for the theming.
#define UI_BORDER        theme->uiBorder
#define UI_BWIDTH        theme->normalComponentWidth
#define UI_BHEIGHT       theme->normalComponentHeight

#define GET_COORDINATES                                 \
    GLfloat x1 = location.cache.x,                      \
            y1 = location.cache.y,                      \
            x2 = x1 + location.cache.width,             \
            y2 = y1 + location.cache.height;


// The label should occupy the left half, the display panel the right half (minus borders).
#define LABEL_LOC           0, 0, 0, 0, 0.0f, 0.0f, 0.5f, 1.0f
#define DISPLAY_PANEL_LOC   0, UI_BORDER, -UI_BORDER, -2*UI_BORDER, 0.5f, 0.0f, 0.5f, 1.0f


// The x and y position that the text is drawn, for the display panel.
#define TEXT_X     3.8f
#define TEXT_Y     5.0f

// A convenience macro for the display panel, so it can draw text easily.
#define DISPLAY_TEXT(text) wool::Font::wideSpacedFont.drawFloorStringScale(text, location.cache.x + TEXT_X * location.xScale, location.cache.y + TEXT_Y * location.yScale, location.xScale, location.yScale)


namespace rasc {

    // ==================== Job status display panel inner class ======================

        JobStatusComponent::JobStatusDisplayPanel::JobStatusDisplayPanel(rascUI::Theme * theme, threading::ThreadQueue * threadQueue, uint64_t jobId) :
            rascUI::Component(rascUI::Location(DISPLAY_PANEL_LOC)),
            threadQueue(threadQueue),
            jobId(jobId) {
        }

        JobStatusComponent::JobStatusDisplayPanel::~JobStatusDisplayPanel(void) {

        }

        bool JobStatusComponent::JobStatusDisplayPanel::shouldRespondToKeyboard(void) const {
            return false;
        }

        void JobStatusComponent::JobStatusDisplayPanel::onDraw(void) {
            if (threadQueue == NULL) return;

            GET_COORDINATES

            threading::JobStatus status = threadQueue->getJobStatus(jobId);

            switch(status) {
                case threading::JobStatus::waiting:  wool_setColourRGB(wool::RGB::RED);    break;
                case threading::JobStatus::running:  wool_setColourRGB(wool::RGB::YELLOW); break;
                case threading::JobStatus::complete: wool_setColourRGB(wool::RGB::GREEN);  break;
            }

            glVertex2f(x1, y1);
            glVertex2f(x2, y1);
            glVertex2f(x2, y2);
            glVertex2f(x1, y2);

            switch(status) {
                case threading::JobStatus::waiting:
                    wool_setColourRGB(wool::RGB::YELLOW);
                    DISPLAY_TEXT("Waiting...");
                    break;
                case threading::JobStatus::running:
                    wool_setColourRGB(wool::RGB::RED);
                    DISPLAY_TEXT("Running...");
                    break;
                case threading::JobStatus::complete:
                    wool_setColourRGB(wool::RGB::BLACK);
                    DISPLAY_TEXT("Complete.");
                break;
            }
        }

    // ================================================================================

    JobStatusComponent::JobStatusComponent(const rascUI::Location & location, rascUI::Theme * theme, const std::string & text, threading::ThreadQueue * threadQueue, uint64_t jobId) :
        rascUI::Container(location),
        frontPanel(),
        label(rascUI::Location(LABEL_LOC), text),
        displayPanel(theme, threadQueue, jobId) {

        add(&frontPanel);
        add(&label);
        add(&displayPanel);
    }

    JobStatusComponent::JobStatusComponent(const rascUI::Location & location, rascUI::Theme * theme) :
        JobStatusComponent(location, theme, "", NULL, 0) {
    }

    JobStatusComponent::~JobStatusComponent(void) {
    }

}

/*
 * ColourComponent.h
 *
 *  Created on: 10 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_COLOURCOMPONENT_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_COLOURCOMPONENT_H_

#include <cstdint>

#include <rascUI/base/Component.h>
#include <rascUI/util/Location.h>
#include <wool/misc/RGBA.h>

namespace simplenetwork {
    class Message;
}



namespace rasc {

    /**
     * This class is a component who's filling is one solid colour, defined in the constructor.
     */
    class ColourComponent : public rascUI::Component {
    private:
        uint32_t colourInt;
        wool::RGBA colourRGBA;
    public:
        ColourComponent(const rascUI::Location & location = rascUI::Location(), wool::RGBA colour = wool::RGBA::BLUE);
        ColourComponent(const rascUI::Location & location, uint32_t colour);

        /**
         * A colour component can be created from a message.
         */
        ColourComponent(const rascUI::Location & location, simplenetwork::Message & msg);

        virtual ~ColourComponent(void);

        /**
         * Writes this colour component to the message.
         */
        void writeTo(simplenetwork::Message & msg) const;

        /**
         * Allows the colour to be changed via wool::RGBA.
         */
        void setColour(wool::RGBA colour);

        /**
         * Allows the colour to be changed via a uint32_t.
         */
        void setColour(uint32_t colour);

    protected:
        virtual bool shouldRespondToKeyboard(void) const override;
        virtual void onDraw(void) override;
    };

}

#endif

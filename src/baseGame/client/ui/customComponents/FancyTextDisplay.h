/*
 * FancyTextDisplay.h
 *
 *  Created on: 30 Jun 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_FANCYTEXTDISPLAY_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_FANCYTEXTDISPLAY_H_

#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/base/Container.h>
#include <string>
#include <vector>

namespace rascUI {
    class Theme;
}

namespace rasc {

    /**
     *
     */
    class FancyTextDisplay : public rascUI::Container {
    private:

        // A custom component which only displays a (vertically centre aligned)
        // icon (offsetted in the X direction by the theme's text offsets).
        class IconLabel : public rascUI::Component {
        private:
            unsigned int iconId;
        public:
            IconLabel(const rascUI::Location & location = rascUI::Location(), unsigned int iconId = 0);
            virtual ~IconLabel(void);
        protected:
            virtual bool shouldRespondToKeyboard(void) const override;
            virtual void onDraw(void) override;
        };

        // Our scroll pane. We have not subclassed this, as to not interfere with the
        // scroll pane recalculate system.
        rascUI::ScrollPane scrollPane;
        // Our current text.
        std::string text;
        // Set to true the first time we recalculate. Our setText method only
        // runs recalculate if this is true (to avoid recalculating initially with zero sizes).
        bool hasRecalculated;
        // Stores the last maxWidth - so we don't unnecessarily rebuild the text display.
        GLfloat lastMaxWidth;
        // Set to true when text changes (and true by default) - also so we don't unnecessarily
        // rebuild the text display.
        bool textChanged;
        // The components created as part of text display. These include line containers,
        // labels and icon labels.
        std::vector<rascUI::Component *> textDisplayComponents;
        // The current line (container) which we are upto.
        rascUI::Container * currentLine;
        // The distance along the line (ignoring scale) which we are upto.
        GLfloat currentLinePos;

    public:
        FancyTextDisplay(const rascUI::Location & location, rascUI::Theme * theme,
                         const std::string & text = std::string(),
                         bool drawBackground = true, bool startBorder = true, bool endBorder = true);
        virtual ~FancyTextDisplay(void);

    private:
        // Figures out the maximum width (ignoring scale) available for text.
        // We must rebuild the text display when this changes.
        GLfloat getMaxWidth(void);

        // Rebuilds our text display. This is called by recalculate if either our
        // text or position has changed.
        void rebuildTextDisplay(GLfloat maxWidth);

        // Resets the text display (removes all components) (used by rebuildTextDisplay).
        void resetTextDisplay(void);

        // Starts a text display line (used by rebuildTextDisplay).
        void startLine(void);

        // Writes text to the current text display line (used by rebuildTextDisplay).
        void writeText(const char * text, size_t len, GLfloat width);

        // Writes an icon to the current text display line (used by rebuildTextDisplay).
        void writeIcon(unsigned int iconId);

    protected:
        // This is overridden so that we can update our text display, if
        // either the text or position has changed.
        virtual void recalculate(const rascUI::Rectangle & parentSize) override;

    public:

        /**
         * Allows access to our current text.
         */
        inline const std::string & getText(void) const {
            return text;
        }

        /**
         * This changes our text, sets textChanged to true, and
         * recalculates/rebuilds our text display (if recalculation
         * has happened at least once before).
         *
         * Note that this must NOT be called AFTER removing the text
         * display from a top level component, as the text display will
         * no longer have access to a theme to use for recalculation.
         */
        void setText(const std::string & newText);
    };
}

#endif

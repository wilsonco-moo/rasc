/*
 * JobStatusComponent.h
 *
 *  Created on: 5 Feb 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_JOBSTATUSCOMPONENT_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_JOBSTATUSCOMPONENT_H_

#include <cstdint>

#include <rascUI/base/Container.h>
#include <rascUI/base/Component.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/Label.h>

namespace rascUI {
    class Theme;
}
namespace threading {
    class ThreadQueue;
}

namespace rasc {

    /**
     * This is a component which displays the status of a job, by polling the job's status each
     * frame from the provided thread queue.
     */
    class JobStatusComponent : public rascUI::Container {
    private:

        /**
         * This is the panel internally used for displaying the status.
         */
        class JobStatusDisplayPanel : public rascUI::Component {
        private:
            threading::ThreadQueue * threadQueue;
            uint64_t jobId;
        public:
            JobStatusDisplayPanel(rascUI::Theme * theme, threading::ThreadQueue * threadQueue, uint64_t jobId);
            virtual ~JobStatusDisplayPanel(void);
            inline void setJob(threading::ThreadQueue * threadQueue, uint64_t jobId) {
                this->threadQueue = threadQueue;
                this->jobId = jobId;
            }
        protected:
            virtual bool shouldRespondToKeyboard(void) const override;
            virtual void onDraw(void) override;
        };

        rascUI::FrontPanel frontPanel;
        rascUI::Label label;
        JobStatusDisplayPanel displayPanel;

    public:
        /**
         * We can either be created initially with a ThreadQueue, label and job id,
         * or the ThreadQueue, label and job id can be set later using setJob.
         */
        JobStatusComponent(const rascUI::Location & location, rascUI::Theme * theme, const std::string & text, threading::ThreadQueue * threadQueue, uint64_t jobId);
        JobStatusComponent(const rascUI::Location & location, rascUI::Theme * theme);
        virtual ~JobStatusComponent(void);

        /**
         * This allows the job that we are looking at to be changed at any time.
         */
        inline void setJob(const std::string & text, threading::ThreadQueue * threadQueue, uint64_t jobId) {
            displayPanel.setJob(threadQueue, jobId);
            label.setText(text);
        }

        /**
         * This allows us to no longer be looking at any particular job.
         */
        inline void unsetJob(void) {
            setJob("", NULL, 0);
        }
    };

}

#endif

/*
 * IconToggleButton.h
 *
 *  Created on: 29 Jun 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ICON_ICONTOGGLEBUTTON_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ICON_ICONTOGGLEBUTTON_H_

#include <rascUI/components/toggleButton/ToggleButton.h>

#include "IconObject.h"

namespace rasc {

    /**
     * IconButton is a custom kind of toggle button, which draws an icon to
     * the left of the text. For a regular button variant, see IconButton.
     * 
     * The method which all icon related custom components use for drawing
     * text beside an icon, is located in IconButton.
     */
    class IconToggleButton : public rascUI::ToggleButton, public IconObject {
    public:
        /**
         * The constructor takes the same parameters as the one for rascUI::ToggleButton,
         * but we take an additional parameter for an icon ID.
         * The ID of the icon MUST BE one of the IDs defined by IconDef.
         */
        IconToggleButton(
            rascUI::ToggleButtonSeries * series,
            const rascUI::Location & location = rascUI::Location(),
            unsigned int iconId = 0,
            std::string text = "",
            std::function<void(GLfloat, GLfloat, int)> onSelectFunc = [](GLfloat viewX, GLfloat viewY, int button){}
        );
        virtual ~IconToggleButton(void);

    protected:
        virtual void onDraw(void) override;
    };
}

#endif

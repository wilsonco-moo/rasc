/*
 * IconObject.cpp
 *
 *  Created on: 1 Dec 2020
 *      Author: wilson
 */

#include "IconObject.h"

namespace rasc {

    IconObject::IconObject(unsigned int iconId) :
        iconId(iconId) {
    }
    
    IconObject::~IconObject(void) {
    }
    
    void IconObject::setIconId(unsigned int newIconId) {
        iconId = newIconId;
    }
}

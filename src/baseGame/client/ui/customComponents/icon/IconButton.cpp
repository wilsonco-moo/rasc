/*
 * IconButton.cpp
 *
 *  Created on: 29 Jun 2020
 *      Author: wilson
 */

#include "IconButton.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#include "IconDrawer.h"

#define LAYOUT_THEME theme

namespace rasc {

    IconButton::IconButton(const rascUI::Location & location, unsigned int iconId, std::string text, std::function<void(GLfloat, GLfloat, int)> onMouseClickFunc) :
        rascUI::Button(location, text, onMouseClickFunc),
        IconObject(iconId) {
    }

    IconButton::~IconButton(void) {
    }

    void IconButton::drawOffsetIcon(rascUI::Theme * theme, const rascUI::Location & location, unsigned int iconId, GLfloat & xOffset, GLfloat yOffset) {
        // Set draw colour for text.
        theme->customSetDrawColour(theme->customGetTextColour(location));
        
        // Get y offsets for both default and current state.
        GLfloat defaultXOff, defaultYOff, currentXOff, currentYOff;
        theme->getTextBaseOffsets(rascUI::State::normal, defaultXOff, defaultYOff);
        theme->getTextBaseOffsets(location.getState(), currentXOff, currentYOff);
        
        // Add x offset from user to x offset from theme.
        currentXOff += xOffset;
        
        // Y offset due to location state.
        GLfloat yDiff = currentYOff - defaultYOff;

        // Y position of icon (as to vertically centre align it).
        GLfloat iconYOff = ((UI_BHEIGHT - IconDrawer::BASE_ICON_HEIGHT) * 0.5f +
                            yDiff + yOffset) * location.yScale;

        // Draw the icon.
        IconDrawer::draw(iconId,
            location.cache.x + currentXOff * location.xScale,
            location.cache.y + iconYOff,
            location.xScale, location.yScale);

        // Update user provided x offset.
        xOffset += (defaultXOff + IconDrawer::BASE_ICON_WIDTH);
    }
    
    void IconButton::drawIconAndText(rascUI::Theme * theme, const rascUI::Location & location, unsigned int iconId, const std::string & text, GLfloat xOffset, GLfloat yOffset) {
        // Set draw colour for text.
        theme->customSetDrawColour(theme->customGetTextColour(location));

        // Get y offsets for both default and current state.
        GLfloat defaultXOff, defaultYOff, currentXOff, currentYOff;
        theme->getTextBaseOffsets(rascUI::State::normal, defaultXOff, defaultYOff);
        theme->getTextBaseOffsets(location.getState(), currentXOff, currentYOff);

        // Add x offset from user to x offset from theme.
        currentXOff += xOffset;
        
        // Y offset due to location state.
        GLfloat yDiff = currentYOff - defaultYOff;

        // Y position of icon (as to vertically centre align it).
        GLfloat iconYOff = ((UI_BHEIGHT - IconDrawer::BASE_ICON_HEIGHT) * 0.5f +
                            yDiff + yOffset) * location.yScale;

        // Draw the icon.
        IconDrawer::draw(iconId,
            location.cache.x + currentXOff * location.xScale,
            location.cache.y + iconYOff,
            location.xScale, location.yScale);

        // Draw the text, x offset by icon width and text offset, y offset by diff and text offset.
        GLfloat textX = currentXOff + IconDrawer::BASE_ICON_WIDTH,
                textY = yDiff + yOffset;
        theme->customDrawTextNoMouse(location, textX, textY, 1.0f, 1.0f, text);
    }

    void IconButton::onDraw(void) {
        rascUI::Theme * theme = getTopLevel()->theme;
        theme->drawButton(location);
        drawIconAndText(theme, location, getIconId(), getText());
    }
    
    GLfloat IconButton::getIconOnlyButtonWidth(rascUI::Theme * theme) {
        GLfloat xOff, yOff;
        theme->getTextBaseOffsets(rascUI::State::normal, xOff, yOff);
        return IconDrawer::BASE_ICON_WIDTH + 2.0f * xOff;
    }
}

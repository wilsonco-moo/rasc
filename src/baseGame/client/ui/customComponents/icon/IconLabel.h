/*
 * IconLabel.h
 *
 *  Created on: 1 Dec 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ICON_ICONLABEL_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ICON_ICONLABEL_H_

#include <rascUI/components/generic/Label.h>

#include "IconObject.h"

namespace rasc {

    /**
     * IconLabel is a custom kind of label, which draws an icon to
     * the left of the text.
     * 
     * The method which all icon related custom components use for drawing
     * text beside an icon, is located in IconButton.
     */
    class IconLabel : public rascUI::Label, public IconObject {
    public:
        /**
         * The constructor takes the same parameters as the one for rascUI::Label,
         * but we take an additional parameter for an icon ID.
         * The ID of the icon MUST BE one of the IDs defined by IconDef.
         */
        IconLabel(const rascUI::Location & location = rascUI::Location(),
                  unsigned int iconId = 0,
                  std::string text = "");
        
        virtual ~IconLabel(void);

    protected:
        virtual void onDraw(void) override;
    };
}

#endif

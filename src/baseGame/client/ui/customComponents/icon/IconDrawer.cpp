/*
 * IconDrawer.cpp
 *
 *  Created on: 28 Jun 2020
 *      Author: wilson
 */

#include "IconDrawer.h"

#include <initializer_list>
#include <unordered_map>
#include <algorithm>

#include "../../../../common/util/definitions/IconDef.h"
#include "../../../../common/util/Misc.h"

namespace rasc {

    // =============== Comma separated vertex data for each icon =================

    #define ICON_DATA_garrison                                                            \
        4.88889, 2.05556,    0,       2.05556,    0,       17.9444,    4.88889, 17.9444,  \
        4.88889, 17.9444,    17.1111, 17.9444,    17.1111, 6.94444,    4.88889, 6.94444,  \
        22,      2.05556,    17.1111, 2.05556,    17.1111, 17.9444,    22,      17.9444,  \
        11.1222, 3.27778,    10.8778, 3.27778,    1.22222, 6.94444,    20.7778, 6.94444

    #define ICON_DATA_manpower                                                            \
        10,      14,         8,       14,         8,       20,         10,      20,       \
        8.94821, 8.31866,    6.94821, 8.31866,    12.7711, 8.32418,    14.7711, 8.32418,  \
        14,      14,         12,      14,         12,      20,         14,      20,       \
        14,      6,          8,       6,          8,       14,         14,      14,       \
        14,      5,          8,       5,          9.5,     6,          12.5,    6,        \
        14,      1,          8,       1,          8,       5,          14,      5,        \
        12.5,    0,          9.5,     0,          8,       1,          14,      1,        \
        7,       6,          5,       6,          5,       14,         7,       14,       \
        17,      6,          15,      6,          15,      14,         17,      14,       \
        15,      6,          7,       6,          7,       8,          15,      8

    #define ICON_DATA_currency                          \
        14, 0,       8,  0,      3,  3,      19, 3,     \
        9,  3,       3,  3,      1,  7,      5,  7,     \
        19, 17,      3,  17,     8,  20,     14, 20,    \
        21, 13,      21, 7,      15, 2.5,    15, 17.5,  \
        19, 3,       13, 3,      17, 7,      21, 7,     \
        7,  17.5,    7,  2.5,    1,  7,      1,  13,    \
        12, 7,       12, 5,      8,  6,      8,  8,     \
        3,  17,      9,  17,     5,  13,     1,  13,    \
        21, 13,      17, 13,     13, 17,     19, 17,    \
        8,  15,      14, 15,     14, 13,     8,  13,    \
        12, 6,       10, 6,      10, 14,     12, 14

    #define ICON_DATA_unrest                                                               \
        6.97403, 11.9481,    10.0909, 8.83117,     12.1688, 1.55844,    9.05195, 4.67532,  \
        2.03896, 17.4026,    4.63636, 20,          11.3896, 12.2078,    9.83117, 10.6494,  \
        15.2857, 3.63636,    12.1688, 0.519481,    9.31169, 8.57143,    12.9481, 12.2078,  \
        19.961,  4.15584,    15.8052, 0,           11.6494, 15.5844,    15.8052, 11.4286
    
    #define ICON_DATA_land                                      \
        11,    2.8,     11,    1.2,     0,  9.2,     0,  10.8,  \
        11,    18.8,    11,    17.2,    0,  9.2,     0,  10.8,  \
        11,    1.2,     11,    2.8,     22, 10.8,    22, 9.2,   \
        22,    10.8,    22,    9.2,     11, 17.2,    11, 18.8,  \
        13.5,  5,       12.5,  5,       11, 6,       15, 6,     \
        11.05, 13,      10.95, 13,      9,  15,      13, 15,    \
        8,     6,       8,     13,      11, 12,      13, 12,    \
        10.05, 4,       9.95,  4,       9,  5,       11, 5,     \
        7,     10,      5,     8,       4,  11,      6,  13,    \
        8,     6,       6,     8,       6,  13,      8,  13,    \
        12,    9,       10,    9,       12, 12,      14, 12,    \
        17.5,  9,       16.5,  9,       14, 11,      20, 11,    \
        16.5,  10,      13.5,  10,      14, 11,      16, 11

    #define ICON_DATA_battle                                                                 \
        19.399,  3.2021,      17.2992, 1.10236,     0.501312, 17.9003,    2.60105, 20,       \
        19.399,  20,          21.4987, 17.9003,     4.70079,  1.10236,    2.60105, 3.2021,   \
        1.55118, 10.0262,     1.55118, 13.1759,     7.32546,  18.9501,    10.4751, 18.9501,  \
        20.4488, 13.1759,     20.4488, 10.0262,     11.5249,  18.9501,    14.6745, 18.9501,  \
        1.60367, 0,           1.49869, 0.104987,    2.60105,  3.2021,     4.70079, 1.10236,  \
        20.5013, 0.104987,    20.3963, 0,           17.2992,  1.10236,    19.399,  3.2021

    #define ICON_DATA_province                                  \
        17,  7,       9,   7,       1.5,  13.5,    4.5,  16.5,  \
        4.5, 19.5,    5.5, 18.5,    2,    16,      0,    18,    \
        10,  18,      14,  14,      2,    14,      6,    18,    \
        21,  15,      21,  13,      16,   15,      18,   17,    \
        10,  7,       10,  5,       8,    8,       10,   8,     \
        10,  5,       10,  11,      20.5, 7.5,     15.5, 2.5,   \
        16,  3,       16,  5,       20,   7,       22,   7,     \
        22,  7,       14,  7,       11,   11,      19,   11,    \
        19,  11,      11,  11,      19,   15,      21,   13,    \
        7,   11,      7,   15,      19,   15,      19,   11
    
    #define ICON_DATA_provinceValue                             \
        14,  12,      6,   12,      1.5,  13.5,    4.5,  16.5,  \
        4.5, 19.5,    5.5, 18.5,    2,    16,      0,    18,    \
        10,  18,      14,  14,      2,    14,      6,    18,    \
        21,  15,      21,  13,      16,   15,      18,   17,    \
        13,  4,       13,  10,      20.5, 7.5,     15.5, 2.5,   \
        16,  3,       16,  5,       20,   7,       22,   7,     \
        22,  7,       16,  7,       13,   11,      19,   11,    \
        19,  11,      13,  11,      19,   15,      21,   13,    \
        19,  12,      9,   12,      7,    15,      21,   15,    \
        6,   0,       6,   0,       0,    5,       12,   5,     \
        15,  7,       13,  7,       13,   12,      15,   12,    \
        9,   5,       3,   5,       3,    11,      9,    11

    #define ICON_DATA_manpowerBuilding                              \
        22,    19.5,    22,    13.5,    10,   13.5,    10,   19.5,  \
        6,     19.5,    6,     13.5,    0,    13.5,    0,    19.5,  \
        22,    8.5,     0,     8.5,     0,    13.5,    22,   13.5,  \
        11.05, 0.5,     10.95, 0.5,     0,    8.5,     22,   8.5,   \
        19.5,  1.5,     16.5,  1.5,     16.5, 8.5,     19.5, 8.5
    
    #define ICON_DATA_currencyBuilding                  \
        22,    8,     8,     8,     8,  11,    22, 11,  \
        22,    11,    0,     11,    0,  13,    22, 13,  \
        6,     1,     2,     1,     2,  11,    6,  11,  \
        15.05, 5,     14.95, 5,     8,  8,     22, 8,   \
        22,    16,    0,     16,    0,  19,    22, 19,  \
        2,     13,    0,     13,    0,  16,    2,  16,  \
        22,    13,    20,    13,    20, 16,    22, 16,  \
        6,     13,    4,     13,    4,  16,    6,  16,  \
        14,    13,    12,    13,    12, 16,    14, 16,  \
        10,    13,    8,     13,    8,  16,    10, 16,  \
        18,    13,    16,    13,    16, 16,    18, 16
    
    #define ICON_DATA_provinceBuilding                          \
        14,  12,      6,   12,      1.5,  13.5,    4.5,  16.5,  \
        4.5, 19.5,    5.5, 18.5,    2,    16,      0,    18,    \
        10,  18,      14,  14,      2,    14,      6,    18,    \
        21,  15,      21,  13,      16,   15,      18,   17,    \
        13,  4,       13,  10,      20.5, 7.5,     15.5, 2.5,   \
        16,  3,       16,  5,       20,   7,       22,   7,     \
        22,  7,       16,  7,       13,   11,      19,   11,    \
        19,  11,      13,  11,      19,   15,      21,   13,    \
        19,  12,      9,   12,      7,    15,      21,   15,    \
        12,  9,       2,   9,       2,    11,      12,   11,    \
        15,  7,       13,  7,       13,   12,      15,   12,    \
        4,   1,       2,   1,       2,    9,       4,    9,     \
        12,  1,       10,  1,       10,   9,       12,   9,     \
        8,   2,       6,   2,       6,    9,       8,    9,     \
        10,  6,       4,   6,       4,    7,       10,   7,     \
        10,  3,       4,   3,       4,    4,       10,   4,     \
        7.5, 1,       6.5, 1,       2,    3,       12,   3
    
    #define ICON_DATA_combatBuilding                      \
        19, 12.5,    3,  12.5,    0,  19.5,    22, 19.5,  \
        15, 3.5,     7,  3.5,     6,  12.5,    16, 12.5,  \
        12, 0.5,     10, 0.5,     10, 3.5,     12, 3.5,   \
        9,  0.5,     7,  0.5,     7,  3.5,     9,  3.5,   \
        15, 0.5,     13, 0.5,     13, 3.5,     15, 3.5

    #define ICON_DATA_build                       \
        2,  13,    0,  13,    0,  20,    2,  20,  \
        2,  20,    8,  20,    8,  9,     2,  9,   \
        13, 20,    7,  14,    7,  14,    7,  20,  \
        18, 0,     16, 0,     16, 13,    18, 13,  \
        4,  2,     2,  4,     22, 4,     22, 2,   \
        18, 13,    16, 13,    15, 20,    19, 20,  \
        12, 13,    10, 13,    10, 20,    12, 20
    
    #define ICON_DATA_alert                       \
        22, 0,     14, 0,     14, 20,    22, 20,  \
        8,  0,     0,  0,     0,  20,    8,  20,  \
        3,  0,     3,  2,     19, 2,     19, 0,   \
        18, 20,    18, 18,    3,  18,    3,  20,  \
        18, 14,    18, 12,    3,  12,    3,  14

    #define ICON_DATA_terrain                               \
        22,   18,     22,   16,     0,    16,    0,    18,  \
        0,    15,     10,   15,     8,    9,     2,    9,   \
        10.5, 2.5,    7.5,  5.5,    11.5, 15,    16.5, 15,  \
        20,   10,     12,   10,     12,   15,    22,   15,  \
        17,   5,      11,   5,      16,   14,    20,   10,  \
        13.5, 2,      12.5, 2,      9,    5,     17,   5,   \
        6,    9,      6,    7,      3,    9,     3,    11
    
    #define ICON_DATA_terrainGround                                   \
        22,   19.75,    22,   17.75,    0,    17.75,    0,    19.75,  \
        10,   10.75,    2,    10.75,    0.5,  13.25,    3.5,  16.25,  \
        15.5, 14.25,    12.5, 11.25,    9.5,  14.25,    12.5, 17.25,  \
        5,    4.75,     3,    6.75,     2,    10.75,    10,   10.75,  \
        14.5, 5.25,     9.5,  0.25,     7,    3.75,     9,    5.75,   \
        21,   8.75,     19,   6.75,     10.5, 7.25,     17.5, 14.25
    
    #define ICON_DATA_terrainClimate                      \
        0,    18,    0,    20,    22,   20,    22,   18,  \
        7.5,  14,    4.5,  14,    4.5,  17,    7.5,  17,  \
        8.5,  10,    3.5,  10,    0.5,  14,    11.5, 14,  \
        8.5,  6,     3.5,  6,     1.5,  10,    10.5, 10,  \
        6.5,  0,     5.5,  0,     2.5,  6,     9.5,  6,   \
        17.5, 15,    14.5, 15,    14.5, 17,    17.5, 17,  \
        18.5, 11,    13.5, 11,    10.5, 15,    21.5, 15,  \
        18.5, 7,     13.5, 7,     11.5, 11,    20.5, 11,  \
        16.5, 1,     15.5, 1,     12.5, 7,     19.5, 7
    
    #define ICON_DATA_terrainWater                        \
        1,    8,     11,   8,     6,    0,     6,    0,   \
        22,   20,    22,   18,    0,    18,    0,    20,  \
        16,   3,     16,   3,     11,   11,    21,   11,  \
        3.5,  12,    8.5,  12,    10.5, 10,    1.5,  10,  \
        1.5,  10,    10.5, 10,    11,   8,     1,    8,   \
        8.5,  12,    3.5,  12,    6,    13,    6,    13,  \
        21,   11,    11,   11,    11.5, 13,    20.5, 13,  \
        20.5, 13,    11.5, 13,    13.5, 15,    18.5, 15,  \
        18.5, 15,    13.5, 15,    16,   16,    16,   16
    
    #define ICON_DATA_terrainShape                         \
        22,   20,     22,   18,     0,   18,    0,    20,  \
        2.05, 0,      1.95, 0,      1,   17,    13,   17,  \
        9.5,  9.5,    4.5,  4.5,    11,  17,    13,   17,  \
        9,    9,      9,    15,     17,  14,    19,   14,  \
        21,   17,     21,   13,     10,  11,    10,   17,  \
        12,   9,      10,   7,      7.5, 11,    12.5, 11,  \
        21,   9,      19,   11,     17,  14,    21,   14
    
    #define ICON_DATA_menu                        \
        0,  0,     0,  2,     22, 2,     22, 0,   \
        2,  2,     0,  2,     0,  18,    2,  18,  \
        22, 2,     20, 2,     20, 18,    22, 18,  \
        22, 20,    22, 18,    0,  18,    0,  20,  \
        19, 7,     19, 3,     3,  3,     3,  7,   \
        19, 17,    19, 13,    3,  13,    3,  17,  \
        19, 12,    19, 8,     3,  8,     3,  12

    #define ICON_DATA_bill                        \
        0,  0,     0,  2,     22, 2,     22, 0,   \
        22, 20,    22, 18,    0,  18,    0,  20,  \
        0,  6,     22, 6,     22, 4,     0,  4,   \
        2,  6,     0,  6,     0,  14,    2,  14,  \
        22, 6,     20, 6,     20, 14,    22, 14,  \
        22, 16,    22, 14,    0,  14,    0,  16

    #define ICON_DATA_army                              \
        10.95, 18,    11.05, 18,    17, 12,    5,  12,  \
        22,    2,     0,     2,     0,  12,    22, 12

    #define ICON_DATA_currencyLack                      \
        14, 0,       8,  0,      3,  3,      19, 3,     \
        9,  3,       3,  3,      1,  7,      5,  7,     \
        19, 17,      3,  17,     8,  20,     14, 20,    \
        21, 13,      21, 7,      15, 2.5,    15, 17.5,  \
        19, 3,       13, 3,      17, 7,      21, 7,     \
        7,  17.5,    7,  2.5,    1,  7,      1,  13,    \
        3,  17,      9,  17,     5,  13,     1,  13,    \
        21, 13,      17, 13,     13, 17,     19, 17,    \
        3,  0,       1,  2,      19, 20,     21, 18

    #define ICON_DATA_demolish                            \
        22,   18,    20,   16,    20,   20,    22,   20,  \
        15.2, 9,     16.8, 9,     16.8, 4,     15.2, 4,   \
        20,   20,    20,   14,    15,   16,    15,   20,  \
        6,    0,     4,    0,     4,    13,    6,    13,  \
        0,    2,     0,    4,     20,   4,     18,   2,   \
        6,    13,    4,    13,    3,    20,    7,    20,  \
        16,   15,    12,   19,    10,   20,    16,   20,  \
        17,   9,     15,   9,     13.5, 11,    18.5, 11,  \
        18.5, 11,    13.5, 11,    15,   13,    17,   13

    #define ICON_DATA_modularTopbar               \
        0,  8,     12, 8,     12, 4,     0,  4,   \
        22, 0,     0,  0,     0,  3,     22, 3,   \
        21, 6,     13, 6,     13, 10,    21, 10,  \
        7,  9,     4,  9,     4,  20,    7,  20,  \
        11, 16,    11, 13,    0,  13,    0,  16
    
    // ===========================================================================

    const GLfloat IconDrawer::BASE_ICON_WIDTH = 22.0f,
                  IconDrawer::BASE_ICON_HEIGHT = 20.0f,
                  IconDrawer::BASE_ICON_SPACING = 4.0f;

    // For each icon, use static_assert and an initialiser list to make
    // sure that the number of float values in the data matches the shape
    // size defined in IconDef.
    #define X(previousName, previousShapeCount, name, shapeCount)     \
        static_assert(                                                \
            std::initializer_list<GLfloat>({ICON_DATA_##name}).size() \
                == shapeCount * IconDef::SHAPE_SIZE,                  \
            "Icon data for " #name " is of wrong size."               \
        );
    RASC_ICON_LIST
    #undef X

    // Assert that the maximum icon name length specified in IconDef
    // actually *is* the name length of the longest icon name.
    namespace {
        constexpr static size_t getMaxIconNameLen(void) {
            size_t maxLen = 0;
            #define X(previousName, previousShapeCount, name, shapeCount) \
                maxLen = std::max(maxLen, Misc::constStrlen(#name));
            RASC_ICON_LIST
            #undef X
            return maxLen;
        }
        static_assert(getMaxIconNameLen() == IconDef::MAX_ICON_NAME_LEN,
            "IconDef::MAX_ICON_NAME_LEN must exactly match the length of the longest icon name.");
    }

    /**
     * This defines the data for each icon. The first value stores
     * the size (in shapes), and the rest is the vertex data for each
     * vertex of each shape.
     */
    #define X(previousName, previousShapeCount, name, shapeCount) \
        (GLfloat)shapeCount, ICON_DATA_##name,
    static const GLfloat dataArr[IconDef::SIZE] = { RASC_ICON_LIST };
    #undef X

    /**
     * This contains all icon IDs by their (string) name.
     */
    #define X(previousName, previousShapeCount, name, shapeCount) \
        { #name, IconDef::name },
    static const std::unordered_map<std::string, unsigned int> iconNameMap = { RASC_ICON_LIST };
    #undef X

    void IconDrawer::draw(unsigned int iconId, GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale) {

        // Find position in array, read shape count, work out end position.
        const GLfloat * upTo = dataArr + iconId;
        unsigned int shapeCount = *(upTo++);
        const GLfloat * end = upTo + (IconDef::SHAPE_SIZE * shapeCount);

        // Draw the 4 vertices of each shape.
        while(upTo != end) {

            GLfloat x1 = x + xScale * upTo[0],    y1 = y + yScale * upTo[1],
                    x2 = x + xScale * upTo[2],    y2 = y + yScale * upTo[3],
                    x3 = x + xScale * upTo[4],    y3 = y + yScale * upTo[5],
                    x4 = x + xScale * upTo[6],    y4 = y + yScale * upTo[7];

            glVertex2f(x1, y1);
            glVertex2f(x2, y2);
            glVertex2f(x3, y3);
            glVertex2f(x4, y4);

            upTo += IconDef::SHAPE_SIZE;
        }
    }
    
    void IconDrawer::drawSVG(std::ostream & stream, unsigned int iconId, GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale) {
        
        // Find position in array, read shape count, work out end position.
        const GLfloat * upTo = dataArr + iconId;
        unsigned int shapeCount = *(upTo++);
        const GLfloat * end = upTo + (IconDef::SHAPE_SIZE * shapeCount);

        // Draw the 4 vertices of each shape.
        while(upTo != end) {

            GLfloat x1 = x + xScale * upTo[0],    y1 = y + yScale * upTo[1],
                    x2 = x + xScale * upTo[2],    y2 = y + yScale * upTo[3],
                    x3 = x + xScale * upTo[4],    y3 = y + yScale * upTo[5],
                    x4 = x + xScale * upTo[6],    y4 = y + yScale * upTo[7];

            stream << "    <polygon points=\""
                << x1 << ',' << y1 << ' '
                << x2 << ',' << y2 << ' '
                << x3 << ',' << y3 << ' '
                << x4 << ',' << y4 << "\"/>\n";

            upTo += IconDef::SHAPE_SIZE;
        }
    }

    unsigned int IconDrawer::getIconIdByName(const std::string & name) {
        auto iter = iconNameMap.find(name);
        if (iter == iconNameMap.end()) {
            return IconDef::INVALID;
        }
        return iter->second;
    }
}

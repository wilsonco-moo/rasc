/*
 * IconLabel.cpp
 *
 *  Created on: 1 Dec 2020
 *      Author: wilson
 */

#include "IconLabel.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/base/Theme.h>

#include "IconButton.h"

namespace rasc {

    IconLabel::IconLabel(const rascUI::Location & location, unsigned int iconId, std::string text) :
        rascUI::Label(location, text),
        IconObject(iconId) {
    }
    
    IconLabel::~IconLabel(void) {
    }
    
    void IconLabel::onDraw(void) {
        IconButton::drawIconAndText(getTopLevel()->theme, location, getIconId(), getText());
    }
}

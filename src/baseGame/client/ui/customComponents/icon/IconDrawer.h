/*
 * IconDrawer.h
 *
 *  Created on: 28 Jun 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ICON_ICONDRAWER_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ICON_ICONDRAWER_H_

#include <GL/gl.h>
#include <ostream>
#include <string>

namespace rasc {

    /**
     * This class contains methods for drawing icons. It also provides the
     * storage for all icon data defined by IconDef.
     *
     * See IconDef.h for the icon IDs for use with this.
     */
    class IconDrawer {
    public:
        /**
         * These define the amount of space (at default scale), which
         * icons should (in general) physically take up. Note
         * that this does not include margins around them.
         * This space should be allowed for whenever drawing them.
         * When defined, all icons should be fitted into a
         * BASE_ICON_WIDTH x BASE_ICON_HEIGHT with a 0 pixel margin.
         */
        const static GLfloat BASE_ICON_WIDTH, BASE_ICON_HEIGHT;

        /**
         * Additional space which should be added, if something is being
         * placed immediately next to an icon.
         * Note that this should only be used where text offsets are unknown
         * or not available.
         */
        const static GLfloat BASE_ICON_SPACING;

        /**
         * Draws the icon, specified by iconId, at the specified coordinates,
         * using the specified scale. Note that the icon ID must be one of
         * the icon IDs defined by IconDef.
         */
        static void draw(unsigned int iconId, GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale);
        
        /**
         * Generates SVG polygon data, for the icon specified by iconId, at the
         * specified coordinates, using the specified scale. This is written
         * to the specified output stream.
         * Note: To form a valid SVG file, this must be surrounded in a header
         * and footer, see IconSVGTest.
         * Also note: To avoid issues with small rounding errors (which cause issues
         * with SVG rendering, it is advisable to set slightly lower than default precision
         * in the stream first, like this: stream << std::defaultfloat << std::setprecision(7);
         */
        static void drawSVG(std::ostream & stream, unsigned int iconId, GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale);
        
        /**
         * Returns the icon ID represented by the specified name,
         * or IconDef::INVALID in the case that the name does not represent
         * any valid icon.
         */
        static unsigned int getIconIdByName(const std::string & name);
    };
}

#endif

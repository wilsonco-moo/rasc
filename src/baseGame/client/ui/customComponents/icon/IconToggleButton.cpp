/*
 * IconToggleButton.cpp
 *
 *  Created on: 29 Jun 2020
 *      Author: wilson
 */

#include "IconToggleButton.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/base/Theme.h>

#include "IconButton.h"

namespace rasc {

    IconToggleButton::IconToggleButton(rascUI::ToggleButtonSeries * series, const rascUI::Location & location, unsigned int iconId, std::string text, std::function<void(GLfloat, GLfloat, int)> onSelectFunc) :
        rascUI::ToggleButton(series, location, text, onSelectFunc),
        IconObject(iconId) {
    }

    IconToggleButton::~IconToggleButton(void) {
    }

    void IconToggleButton::onDraw(void) {
        // Call the superclass onDraw method, so the location's state is updated correctly. See ToggleComponent.h.
        ToggleComponent::onDraw();
        
        rascUI::Theme * theme = getTopLevel()->theme;
        theme->drawButton(location);
        IconButton::drawIconAndText(theme, location, getIconId(), getText());
    }
}

/*
 * IconButton.h
 *
 *  Created on: 29 Jun 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ICON_ICONBUTTON_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ICON_ICONBUTTON_H_

#include <rascUI/components/generic/Button.h>

#include "IconObject.h"

namespace rascUI {
    class Theme;
}

namespace rasc {

    /**
     * IconButton is a custom kind of button, which draws an icon to the left
     * of the text. For a toggle button variant, see IconToggleButton.
     * 
     * This class also contains a static method for drawing text beside an icon,
     * which is used in all the icon based custom components.
     */
    class IconButton : public rascUI::Button, public IconObject {
    public:
        /**
         * The constructor takes the same parameters as the one for rascUI::Button,
         * but we take an additional parameter for an icon ID.
         * The ID of the icon MUST BE one of the IDs defined by IconDef.
         */
        IconButton(
            const rascUI::Location & location = rascUI::Location(),
            unsigned int iconId = 0,
            std::string text = "",
            std::function<void(GLfloat, GLfloat, int)> onMouseClickFunc = [](GLfloat viewX, GLfloat viewY, int button){}
        );
        virtual ~IconButton(void);

    protected:
        virtual void onDraw(void) override;

    public:
        /**
         * Sets colour to theme's location state correct text colour, then draws an icon with
         * the specified offset (which is multiplied by location scale). The icon is additionally
         * shifted according the the theme's text offsets, using current location state. The
         * provided x offset is updated to the correct position to draw another icon (or text)
         * beside it. The icon is vertically centred within a UI_BHEIGHT area.
         */
        static void drawOffsetIcon(rascUI::Theme * theme, const rascUI::Location & location,
                                   unsigned int iconId, GLfloat & xOffset, GLfloat yOffset = 0.0f);
        
        /**
         * Sets colour to theme's location state correct text colour, then custom draws an icon
         * and text, with optional offset (the offset is multiplied by location scale).
         * This is used for other components which are displayed in a similar way.
         * The icon is vertically centred within a UI_BHEIGHT area.
         */
        static void drawIconAndText(rascUI::Theme * theme, const rascUI::Location & location, unsigned int iconId,
                                    const std::string & text, GLfloat xOffset = 0.0f, GLfloat yOffset = 0.0f);
        
        /**
         * Returns, for the specified theme, the appropriate width for a button which
         * only contains an icon. This is the width of icons, plus appropriate
         * theme-defined borders on both sides.
         */
        static GLfloat getIconOnlyButtonWidth(rascUI::Theme * theme);
    };
}

#endif

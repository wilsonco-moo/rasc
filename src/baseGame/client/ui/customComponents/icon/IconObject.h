/*
 * IconObject.h
 *
 *  Created on: 1 Dec 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ICON_ICONOBJECT_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ICON_ICONOBJECT_H_

namespace rasc {

    /**
     * Simple class defining the interface for a component which
     * contains an icon, (similar principle to rascUI::TextObject).
     */
    class IconObject {
    private:
        unsigned int iconId;

    public:
        IconObject(unsigned int iconId);
        virtual ~IconObject(void);
        
        /**
         * Allows access to the associated icon.
         */
        inline unsigned int getIconId(void) const {
            return iconId;
        }
        
        /**
         * Allows changing the associated icon.
         */
        void setIconId(unsigned int iconId);
    };
}

#endif

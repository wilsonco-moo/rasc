/*
 * PlayerPanel.cpp
 *
 *  Created on: 7 Dec 2018
 *      Author: wilson
 */

#include "PlayerPanel.h"

#include <GL/gl.h>
#include <cstddef>

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#include "../../../common/playerData/PlayerDataSystem.h"
#include "../../../common/playerData/PlayerData.h"
#include "../../RascBox.h"

#define LAYOUT_THEME theme

// The width and location of the colour box.
#define COLOUR_BOX_WIDTH \
    (UI_BHEIGHT - 2 * UI_BORDER)
#define COLOUR_BOX_LOC \
    PIN_L(COLOUR_BOX_WIDTH, GEN_BORDER)

// The x offset of the text.
#define LABEL_X_OFFSET \
    (UI_BHEIGHT - UI_BORDER)

// The locations of the left and right colour boxes.
#define COLOUR_BOX_LEFT_LOC \
    BORDERTABLE_X(0, 2, COLOUR_BOX_LOC)
#define COLOUR_BOX_RIGHT_LOC \
    BORDERTABLE_X(1, 2, COLOUR_BOX_LOC)

// For convenience
#define DRAW_RECT_LOC(loc) {                        \
    GLfloat x2 = loc.cache.x + loc.cache.width,     \
            y2 = loc.cache.y + loc.cache.height;    \
    glVertex2f(loc.cache.x, loc.cache.y);           \
    glVertex2f(x2, loc.cache.y);                    \
    glVertex2f(x2, y2);                             \
    glVertex2f(loc.cache.x, y2);                    \
}

namespace rasc {

    PlayerPanel::PlayerPanel(const rascUI::Location & providedLoc, RascBox & box, rascUI::Theme * theme) :
        rascUI::Button(providedLoc, "No player"),
        box(box),
        playerId(PlayerData::NO_PLAYER),
        colour(),
        turnStatus(false),
        colourLocation(COLOUR_BOX_LOC),
        leftColourLocation(COLOUR_BOX_LEFT_LOC),
        rightColourLocation(COLOUR_BOX_RIGHT_LOC) {
        setActive(false);
    }

    PlayerPanel::~PlayerPanel(void) {
    }

    void PlayerPanel::recalculate(const rascUI::Rectangle & parentSize) {
        rascUI::Button::recalculate(parentSize);
        // Make sure internal locations get recalculated.
        GLfloat xScale = getTopLevel()->getXScale(),
                yScale = getTopLevel()->getYScale();
        colourLocation.fitCacheInto(location.cache, xScale, yScale);
        leftColourLocation.fitCacheInto(location.cache, xScale, yScale);
        rightColourLocation.fitCacheInto(location.cache, xScale, yScale);
    }

    void PlayerPanel::onDraw(void) {
        rascUI::Theme * theme = getTopLevel()->theme;
        if (playerId == PlayerData::NO_PLAYER) {
            // For non-existing players, only draw button and text.
            theme->drawButton(location);
            theme->drawText(location, getText());
        } else {
            // For existing players, draw colour, button and text.
            theme->drawButton(location);
            theme->customSetDrawColour(theme->customColourMainText);
            theme->customDrawTextMouse(location, LABEL_X_OFFSET, 0.0f, 1.0f, 1.0f, getText());
            if (turnStatus) {
                wool_setColourRGBA(colour)
                DRAW_RECT_LOC(colourLocation)
            } else {
                DRAW_RECT_LOC(rightColourLocation)
                wool_setColourRGBA(colour)
                DRAW_RECT_LOC(leftColourLocation)
            }
        }
    }

    void PlayerPanel::onMouseClick(GLfloat viewX, GLfloat viewY, int button) {
        if (button == getTopLevel()->getMb()) {
            box.uiBillMenuShowPlayer(playerId);
        }
    }

    void PlayerPanel::setPlayerByData(PlayerData * data) {
        // Set to no player if data is NULL.
        if (data == NULL) {
            setText("No player");
            playerId = PlayerData::NO_PLAYER;
            setActive(false);
        } else {
            setText(data->getName());
            playerId = data->getId();
            colour = data->getColour();
            turnStatus = !data->getHaveFinishedTurn();
            setActive(true);
        }
    }

    void PlayerPanel::setPlayerById(uint64_t newPlayerId) {
        // Set to either no player or unknown player depending on whether id is
        // actually NO_PLAYER, or if it just isn't found.
        PlayerData * data = box.common.playerDataSystem->idToDataChecked(newPlayerId);
        if (data == NULL) {
            setText(newPlayerId == PlayerData::NO_PLAYER ? "No player" : "Unknown player");
            playerId = PlayerData::NO_PLAYER;
            setActive(false);
        } else {
            setText(data->getName());
            playerId = newPlayerId;
            colour = data->getColour();
            turnStatus = !data->getHaveFinishedTurn();
            setActive(true);
        }
    }
}

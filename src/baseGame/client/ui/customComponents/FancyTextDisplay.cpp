/*
 * FancyTextDisplay.cpp
 *
 *  Created on: 30 Jun 2020
 *      Author: wilson
 */

#include "FancyTextDisplay.h"

#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <algorithm>
#include <iostream>
#include <cstddef>
#include <cctype>

#include "../../../common/util/definitions/IconDef.h"
#include "icon/IconDrawer.h"

namespace rasc {

    FancyTextDisplay::IconLabel::IconLabel(const rascUI::Location & location, unsigned int iconId) :
        rascUI::Component(location),
        iconId(iconId) {
    }
    FancyTextDisplay::IconLabel::~IconLabel(void) {
    }
    bool FancyTextDisplay::IconLabel::shouldRespondToKeyboard(void) const {
        return false;
    }
    void FancyTextDisplay::IconLabel::onDraw(void) {
        rascUI::Theme * theme = getTopLevel()->theme;
        GLfloat xOff, yOff;
        theme->getTextBaseOffsets(location.getState(), xOff, yOff);
        xOff *= location.xScale;
        yOff = (location.cache.height - IconDrawer::BASE_ICON_HEIGHT * location.yScale) * 0.5f;
        theme->customSetDrawColour(theme->customGetTextColour(location));
        IconDrawer::draw(iconId, location.cache.x + xOff, location.cache.y + yOff, location.xScale, location.yScale);
    }

    // ==========================================================================================================

    #define LAYOUT_THEME theme

    #define LINE_CONTAINER_LOC \
        SUB_BORDER_B(          \
         GEN_FILL              \
        )

    #define LABEL_LOC(linePos, width) \
        MOVE_X(linePos,               \
         PIN_L(width,                 \
          GEN_FILL                    \
        ))

    // The minimum max-width which should be used (15 characters).
    #define MIN_MAXWIDTH (LAYOUT_THEME->customTextCharWidth * 10.0f)

    FancyTextDisplay::FancyTextDisplay(const rascUI::Location & location, rascUI::Theme * theme, const std::string & text, bool drawBackground, bool startBorder, bool endBorder) :
        rascUI::Container(location),

        scrollPane(theme, rascUI::Location(), drawBackground, true, startBorder, endBorder),
        text(text),
        hasRecalculated(false),
        lastMaxWidth(0.0f),
        textChanged(true),
        textDisplayComponents(),
        currentLine(NULL),
        currentLinePos(0.0f) {

        add(&scrollPane);

        IconLabel label;
    }

    FancyTextDisplay::~FancyTextDisplay(void) {
        for (rascUI::Component * component : textDisplayComponents) {
            delete component;
        }
    }

    void FancyTextDisplay::recalculate(const rascUI::Rectangle & parentSize) {
        hasRecalculated = true;

        // Use the component method NOT the Container method: We want to avoid
        // recalculating our child scroll pane until we have done re-building the
        // text components.
        Component::recalculate(parentSize);

        // If either the text or max width has changed, rebuild the text display.
        GLfloat maxWidth = getMaxWidth();
        if (textChanged || lastMaxWidth != maxWidth) {
            textChanged = false;
            lastMaxWidth = maxWidth;
            rebuildTextDisplay(maxWidth);
        }

        // Only recalculate the scroll pane once we have rebuilt the text display.
        childRecalculate(&scrollPane, location.cache);
    }

    GLfloat FancyTextDisplay::getMaxWidth(void) {
        rascUI::Theme * theme = getTopLevel()->theme;
        GLfloat xOff, yOff;
        theme->getTextBaseOffsets(rascUI::State::normal, xOff, yOff);
        return std::max(MIN_MAXWIDTH,
                        (location.cache.width / location.xScale) - UI_BWIDTH - UI_BORDER - xOff * 2.0f);
    }

    void FancyTextDisplay::rebuildTextDisplay(GLfloat maxWidth) {
        resetTextDisplay();
        rascUI::Theme * theme = getTopLevel()->theme;

        // Get character width from theme.
        GLfloat charWidth = theme->customTextCharWidth;

        const char * charUpto = text.c_str(),   // Current character.
                   * lineStart = text.c_str(),  // FIRST character of current line.
                   * lastSpace = NULL;          // Most recent space character (in current line).
        GLfloat widthUpto = 0.0f;               // Current width up to.
        bool continuingLine = false;

        for (; *charUpto != '\0'; charUpto++) {

            // Remember locations of spaces.
            if (*charUpto == ' ') {
                lastSpace = charUpto;
            }

            // Treat newline like a space, but make sure we immediately run out of
            // horizontal space, so the line definately gets broken.
            if (*charUpto == '\n') {
                lastSpace = charUpto;
                widthUpto = maxWidth * 2.0f;
            }

            if (*charUpto == ':') {
                // Find the next colon. Only look IconDef::MAX_ICON_NAME_LEN + 1 characters ahead:
                // any more would be unnecessary. Also stop if we find the end of the string.
                const char * closePos = NULL;
                for (unsigned int offset = 1; offset <= IconDef::MAX_ICON_NAME_LEN + 1; offset++) {
                    char character = charUpto[offset];
                    if (character == ':') {
                        closePos = charUpto + offset;
                        break;
                    } else if (character == '\0') {
                        break;
                    }
                }
                // If we found a closing colon:
                if (closePos != NULL) {
                    // If the icon name is invalid:
                    std::string iconName(charUpto + 1, closePos - (charUpto + 1));
                    unsigned int iconId = IconDrawer::getIconIdByName(iconName);
                    if (iconId != IconDef::INVALID) {

                        // If the icon is not the start of the line, print the part of the line before it.
                        // Notify that we are now continiuing a line.
                        if (lineStart != charUpto) {
                            size_t len = charUpto - lineStart;
                            if (!continuingLine) { startLine(); }
                            writeText(lineStart, len, len * charWidth);
                            continuingLine = true;
                        }

                        // If writing the icon would push across a line boundary, start a new line.
                        if (widthUpto + IconDrawer::BASE_ICON_WIDTH > maxWidth) {
                            startLine();
                            widthUpto = 0.0f;
                        }

                        // Write the icon and update where we are upto. Start a line before it if necessary.
                        if (!continuingLine) { startLine(); }
                        writeIcon(iconId);
                        widthUpto += IconDrawer::BASE_ICON_WIDTH;

                        // We are always continuing a line after drawing an icon.
                        continuingLine = true;

                        // Skip the content of the icon string, start line after close pos,
                        // set last space to close, then continue loop as to avoid doing text printing logic.
                        charUpto = closePos;
                        lineStart = closePos + 1;
                        lastSpace = closePos; // Set this to the close position, even though that is not
                        continue;             // a space. This will allow breaking lines here. Note that
                    }                         // this is BEFORE the start of the line, so nothing will
                }                             // be printed if broken here.
            }

            // Update to include current character.
            widthUpto += charWidth;

            // If the addition of this character made the line too wide, end this line BEFORE current character.
            if (widthUpto > maxWidth) {

                // Find break point (first char of next line). Use either current character or most recent space.
                const char * breakPoint;
                if (lastSpace == NULL) {
                    breakPoint = charUpto;
                } else {
                    breakPoint = lastSpace;
                }

                // Work out len, start line and write text.
                // (As long as the break point is not before the start of the line, which
                // is the case when breaking on the end of an icon).
                // Note that we must still write zero length strings, as otherwise blank
                // (newline) lines will not show.
                if (breakPoint >= lineStart) {
                    size_t len = breakPoint - lineStart;
                    if (!continuingLine) { startLine(); }
                    writeText(lineStart, len, len * charWidth);
                }

                // Set next line to start either on this character, or character after last space.
                if (lastSpace == NULL) {
                    lineStart = charUpto;
                } else {
                    lineStart = lastSpace + 1;
                }

                // Reset variables since we started a new line.
                lastSpace = NULL;
                widthUpto = ((charUpto + 1) - lineStart) * charWidth;
                continuingLine = false;
            }
        }

        // If there is stuff left to write at the end, write it. Don't bother updating
        // any variables since this is the last operation.
        if (lineStart != charUpto) {
            size_t len = charUpto - lineStart;
            if (!continuingLine) { startLine(); }
            writeText(lineStart, len, len * charWidth);
        }
    }

    void FancyTextDisplay::resetTextDisplay(void) {
        scrollPane.contents.clear();
        for (rascUI::Component * component : textDisplayComponents) {
            delete component;
        }
        textDisplayComponents.clear();
        currentLine = NULL;
        currentLinePos = 0.0f;
    }

    void FancyTextDisplay::startLine(void) {
        rascUI::Theme * theme = getTopLevel()->theme;
        currentLine = new rascUI::Container(rascUI::Location(LINE_CONTAINER_LOC));
        textDisplayComponents.push_back(currentLine);
        scrollPane.contents.add(currentLine);
        currentLinePos = 0.0f;
    }

    void FancyTextDisplay::writeText(const char * text, size_t len, GLfloat width) {
        // Create the string, remove any newline characters, trim trailing whitespace.
        std::string lineText(text, len);
        std::replace(lineText.begin(), lineText.end(), '\n', ' ');
        while(!lineText.empty() && std::isspace(lineText.back())) {
            lineText.pop_back();
        }
        // Add a label for this text (as long as the text isn't empty).
        if (!lineText.empty() > 0) {
            lineText.shrink_to_fit();
            rascUI::Label * textLabel = new rascUI::Label(
                    rascUI::Location(LABEL_LOC(currentLinePos, width)), std::move(lineText));
            textDisplayComponents.push_back(textLabel);
            currentLine->add(textLabel);
        }
        // Increment the width even if nothing was added.
        currentLinePos += width;
    }

    void FancyTextDisplay::writeIcon(unsigned int iconId) {
        IconLabel * iconLabel = new IconLabel(
            rascUI::Location(LABEL_LOC(currentLinePos, IconDrawer::BASE_ICON_WIDTH)), iconId);
        textDisplayComponents.push_back(iconLabel);
        currentLine->add(iconLabel);
        currentLinePos += IconDrawer::BASE_ICON_WIDTH;
    }

    void FancyTextDisplay::setText(const std::string & newText) {
        text = newText;
        // Only recalculate if we have recalculated before: This avoids wrongly
        // recaculating many times with zero location size during initialisation.
        if (hasRecalculated) {
            textChanged = true;
            recalculate(getParent()->location.cache);
        }
    }
}

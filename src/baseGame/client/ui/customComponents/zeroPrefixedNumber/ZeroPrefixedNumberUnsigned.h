/*
 * ZeroPrefixedNumberUnsigned.h
 *
 *  Created on: 7 Feb 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ZEROPREFIXEDNUMBER_ZEROPREFIXEDNUMBERUNSIGNED_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ZEROPREFIXEDNUMBER_ZEROPREFIXEDNUMBERUNSIGNED_H_

#include <cstdint>

#include "ZeroPrefixedNumberBase.h"

namespace rasc {

    /**
     * ZeroPrefixedNumberUnsigned displays unsigned 64 bit integers in zero prefixed format.
     */
    class ZeroPrefixedNumberUnsigned : public ZeroPrefixedNumberBase {
    public:
        /**
         * Required format function type - example: NumberFormat::generic.
         */
        using FormatFunc = unsigned int (*) (char *, uint64_t);
        
    private:
        FormatFunc formatFunc;
        uint64_t value;
        
    public:
        /**
         * Format function must not produce an output larger than buffer size.
         * Default constructor uses null format function, zero value, and no buffer.
         * Note: constructor declared explicit to avoid ambiguity between implicit conversion and operator=.
         */
        explicit ZeroPrefixedNumberUnsigned(void);
        explicit ZeroPrefixedNumberUnsigned(unsigned int bufferSize, FormatFunc formatFunc, uint64_t value = 0);
        
        /**
         * This sets the value used in this zero prefixed number.
         */
        ZeroPrefixedNumberUnsigned & operator = (uint64_t newValue);
        
        /**
         * This allows implicit access to our value.
         */
        inline operator uint64_t(void) const {
            return value;
        }
    };
}

#endif

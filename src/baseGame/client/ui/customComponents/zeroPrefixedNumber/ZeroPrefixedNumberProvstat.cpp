/*
 * ZeroPrefixedNumberProvstat.cpp
 *
 *  Created on: 7 Feb 2021
 *      Author: wilson
 */

#include "ZeroPrefixedNumberProvstat.h"

#include <cstring>
#include <cstddef>

namespace rasc {
    
    ZeroPrefixedNumberProvstat::ZeroPrefixedNumberProvstat(void) :
        ZeroPrefixedNumberBase(0),
        formatFunc(NULL),
        prefixPlus(false),
        value(0) {
    }
    
    ZeroPrefixedNumberProvstat::ZeroPrefixedNumberProvstat(unsigned int bufferSize, FormatFunc formatFunc, bool prefixPlus, provstat_t value) :
        ZeroPrefixedNumberBase(bufferSize),
        formatFunc(formatFunc),
        prefixPlus(prefixPlus) {
        operator=(value);
    }

    ZeroPrefixedNumberProvstat & ZeroPrefixedNumberProvstat::operator = (provstat_t newValue) {
        value = newValue;
        
        // Do nothing if we have no buffer.
        if (getBufferSize() != 0) {
            // Write using format function, work out zero count.
            unsigned int written = formatFunc(getNumberStringBuffer(), newValue, prefixPlus),
                         zeroCount = getBufferSize() - 1u - written;

            // Create zero string.
            char * zeroBuffer = getZeroStringBuffer();
            std::memset(zeroBuffer, '0', zeroCount);
            zeroBuffer[zeroCount] = '\0';

            // Notify base class of change to zero string length.
            onSetZeroStringLength(zeroCount);
        }
        return *this;
    }
}

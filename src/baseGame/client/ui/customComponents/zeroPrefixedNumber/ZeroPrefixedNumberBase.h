/*
 * ZeroPrefixedNumberBase.h
 *
 *  Created on: 7 Feb 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ZEROPREFIXEDNUMBER_ZEROPREFIXEDNUMBERBASE_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ZEROPREFIXEDNUMBER_ZEROPREFIXEDNUMBERBASE_H_

#include <GL/gl.h>
#include <vector>

namespace rascUI {
    class Location;
    class Theme;
}

namespace rasc {

    /**
     * Zero prefixed numbers allow displaying numbers in a format which prefixes
     * zeroes (of possibly a different colour), to pad the number to a specified length.
     * This provides a nice and readable way to display numbers, while keeping consistent
     * layout.
     * 
     * Use subclasses for displaying different types.
     */
    class ZeroPrefixedNumberBase {
    private:
        unsigned int bufferSize, zeroStringLength;
        std::vector<char> zeroString, numberString;
    
    protected:
        // Protected constructor and destructor: subclasses must be used instead.
        ZeroPrefixedNumberBase(unsigned int bufferSize);
        ~ZeroPrefixedNumberBase(void);
        
        // Allows subclasses to modify zero and number strings.
        char * getZeroStringBuffer(void);
        char * getNumberStringBuffer(void);
        
        // Should be run by subclasses when they modify string buffers - we
        // need to know this for drawing.
        void onSetZeroStringLength(unsigned int newZeroStringLength);
        
    public:
        /**
         * Allows access to our buffer size.
         */
        inline unsigned int getBufferSize(void) const {
            return bufferSize;
        }
        
        /**
         * This draws this zero prefixed number to the specified location.
         */
        void draw(const rascUI::Location & location, rascUI::Theme * theme,
                  GLfloat offsetX = 0.0f, GLfloat offsetY = 0.0f,
                  GLfloat scaleX = 1.0f, GLfloat scaleY = 1.0f) const;
        
        /**
         * This draws this zero prefixed number to the specified location
         * and two theme custom colours.
         */
        void draw(const rascUI::Location & location, rascUI::Theme * theme,
                  void * zeroColour, void * numberColour,
                  GLfloat offsetX = 0.0f, GLfloat offsetY = 0.0f,
                  GLfloat scaleX = 1.0f, GLfloat scaleY = 1.0f) const;
        
        /**
         * Draw method variants which use mouse offset variants of the theme's
         * text drawing methods.
         */
        void drawMouse(const rascUI::Location & location, rascUI::Theme * theme, GLfloat offsetX = 0.0f, GLfloat offsetY = 0.0f, GLfloat scaleX = 1.0f, GLfloat scaleY = 1.0f) const;
        void drawMouse(const rascUI::Location & location, rascUI::Theme * theme, void * zeroColour, void * numberColour, GLfloat offsetX = 0.0f, GLfloat offsetY = 0.0f, GLfloat scaleX = 1.0f, GLfloat scaleY = 1.0f) const;
    };
}

#endif

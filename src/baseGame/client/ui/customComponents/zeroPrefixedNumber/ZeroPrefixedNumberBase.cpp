/*
 * ZeroPrefixedNumberBase.cpp
 *
 *  Created on: 7 Feb 2021
 *      Author: wilson
 */

#include "ZeroPrefixedNumberBase.h"

#include <rascUI/util/Location.h>
#include <rascUI/base/Theme.h>

namespace rasc {

    ZeroPrefixedNumberBase::ZeroPrefixedNumberBase(unsigned int bufferSize) :
        bufferSize(bufferSize),
        zeroStringLength(0),
        zeroString(bufferSize),
        numberString(bufferSize) {
    }

    ZeroPrefixedNumberBase::~ZeroPrefixedNumberBase(void) {
    }

    char * ZeroPrefixedNumberBase::getZeroStringBuffer(void) {
        return &zeroString[0];
    }
    
    char * ZeroPrefixedNumberBase::getNumberStringBuffer(void) {
        return &numberString[0];
    }
        
    void ZeroPrefixedNumberBase::onSetZeroStringLength(unsigned int newZeroStringLength) {
        zeroStringLength = newZeroStringLength;
    }
    
    void ZeroPrefixedNumberBase::draw(const rascUI::Location & location, rascUI::Theme * theme, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY) const {
        // Get colours from theme: In inactive state use inactive for both, otherwise use secondary
        // for zeroes, and consult theme for main colour.
        void * zeroColour, * numberColour;
        if (location.getState() == rascUI::State::inactive) {
            zeroColour = theme->customColourInactiveText;
            numberColour = zeroColour;
        } else {
            zeroColour = theme->customColourSecondaryText;
            numberColour = theme->customGetTextColour(location);
        }
        
        draw(location, theme, zeroColour, numberColour, offsetX, offsetY, scaleX, scaleY);
    }

    void ZeroPrefixedNumberBase::draw(const rascUI::Location & location, rascUI::Theme * theme, void * zeroColour, void * numberColour, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY) const {
        if (bufferSize != 0) {
            theme->customSetDrawColour(zeroColour);
            theme->customDrawTextNoMouse(location, offsetX, offsetY, scaleX, scaleY, &zeroString[0]);

            theme->customSetDrawColour(numberColour);
            theme->customDrawTextNoMouse(location, offsetX + zeroStringLength * theme->customTextCharWidth * scaleX, offsetY, scaleX, scaleY, &numberString[0]);
        }
    }
    
    void ZeroPrefixedNumberBase::drawMouse(const rascUI::Location & location, rascUI::Theme * theme, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY) const {
        // Get colours from theme: In inactive state use inactive for both, otherwise use secondary
        // for zeroes, and consult theme for main colour.
        void * zeroColour, * numberColour;
        if (location.getState() == rascUI::State::inactive) {
            zeroColour = theme->customColourInactiveText;
            numberColour = zeroColour;
        } else {
            zeroColour = theme->customColourSecondaryText;
            numberColour = theme->customGetTextColour(location);
        }
        
        drawMouse(location, theme, zeroColour, numberColour, offsetX, offsetY, scaleX, scaleY);
    }
    
    void ZeroPrefixedNumberBase::drawMouse(const rascUI::Location & location, rascUI::Theme * theme, void * zeroColour, void * numberColour, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY) const {
        if (bufferSize != 0) {
            theme->customSetDrawColour(zeroColour);
            theme->customDrawTextMouse(location, offsetX, offsetY, scaleX, scaleY, &zeroString[0]);

            theme->customSetDrawColour(numberColour);
            theme->customDrawTextMouse(location, offsetX + zeroStringLength * theme->customTextCharWidth * scaleX, offsetY, scaleX, scaleY, &numberString[0]);
        }
    }
}

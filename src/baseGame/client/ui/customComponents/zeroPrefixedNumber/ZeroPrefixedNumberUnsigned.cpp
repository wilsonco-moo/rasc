/*
 * ZeroPrefixedNumberUnsigned.cpp
 *
 *  Created on: 7 Feb 2021
 *      Author: wilson
 */

#include "ZeroPrefixedNumberUnsigned.h"

#include <cstring>
#include <cstddef>

namespace rasc {

    ZeroPrefixedNumberUnsigned::ZeroPrefixedNumberUnsigned(void) :
        ZeroPrefixedNumberBase(0),
        formatFunc(NULL),
        value(0) {
    }
    
    ZeroPrefixedNumberUnsigned::ZeroPrefixedNumberUnsigned(unsigned int bufferSize, FormatFunc formatFunc, uint64_t value) :
        ZeroPrefixedNumberBase(bufferSize),
        formatFunc(formatFunc) {
        operator=(value);
    }

    ZeroPrefixedNumberUnsigned & ZeroPrefixedNumberUnsigned::operator = (uint64_t newValue) {
        value = newValue;
        
        // Do nothing if we have no buffer.
        if (getBufferSize() != 0) {
            // Write using format function, work out zero count.
            unsigned int written = formatFunc(getNumberStringBuffer(), newValue),
                         zeroCount = getBufferSize() - 1u - written;

            // Create zero string.
            char * zeroBuffer = getZeroStringBuffer();
            std::memset(zeroBuffer, '0', zeroCount);
            zeroBuffer[zeroCount] = '\0';

            // Notify base class of change to zero string length.
            onSetZeroStringLength(zeroCount);
        }
        
        return *this;
    }
}

/*
 * ZeroPrefixedNumberProvstat.h
 *
 *  Created on: 7 Feb 2021
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ZEROPREFIXEDNUMBER_ZEROPREFIXEDNUMBERPROVSTAT_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_ZEROPREFIXEDNUMBER_ZEROPREFIXEDNUMBERPROVSTAT_H_

#include "../../../../common/gameMap/provinceUtil/ProvinceStatTypes.h"
#include "ZeroPrefixedNumberBase.h"

namespace rasc {

    /**
     * ZeroPrefixedNumberUnsigned displays province stat values in zero prefixed format.
     */
    class ZeroPrefixedNumberProvstat : public ZeroPrefixedNumberBase {
    public:
        /**
         * Required format function type - example: NumberFormat::currency.
         */
        using FormatFunc = unsigned int (*) (char *, provstat_t, bool);
        
    private:
        FormatFunc formatFunc;
        bool prefixPlus;
        provstat_t value;
        
    public:
        /**
         * Format function must not produce an output larger than buffer size. The prefixPlus parameter
         * specifies whether positive numbers should have a '+' prefixed to the front.
         * Default constructor uses null format function, zero value, and no buffer.
         * Note: constructor declared explicit to avoid ambiguity between implicit conversion and operator=.
         */
        explicit ZeroPrefixedNumberProvstat(void);
        explicit ZeroPrefixedNumberProvstat(unsigned int bufferSize, FormatFunc formatFunc, bool prefixPlus, provstat_t value = 0);
        
        /**
         * This sets the value used in this zero prefixed number.
         */
        ZeroPrefixedNumberProvstat & operator = (provstat_t newValue);
        
        /**
         * This allows implicit access to our value.
         */
        inline operator provstat_t(void) const {
            return value;
        }
    };
}

#endif

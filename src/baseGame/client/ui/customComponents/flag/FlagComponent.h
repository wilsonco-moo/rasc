/*
 * FlagComponent.h
 *
 *  Created on: 11 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_FLAG_FLAGCOMPONENT_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_FLAG_FLAGCOMPONENT_H_

#include <rascUI/base/Component.h>

#include "../../../../common/playerData/flag/Flag.h"

namespace simplenetwork {
    class Message;
}

namespace rasc {

    /**
     * A FlagComponent is a UI component that contains, and can draw a flag.
     */
    class FlagComponent : public rascUI::Component{
    private:
        // Our internal flag.
        Flag flag;

    public:
        /**
         * This constructor requires a Flag instance to copy.
         */
        FlagComponent(const rascUI::Location & location = rascUI::Location(), Flag flag = Flag());

        /**
         * Since Flag can be created from a message, a flag component can be created using a message.
         */
        FlagComponent(const rascUI::Location & location, simplenetwork::Message & msg);
        virtual ~FlagComponent(void);

    protected:
        // We don't want to respond to selection.
        virtual bool shouldRespondToKeyboard(void) const override;

        /**
         * Here we simply draw our internal Flag instance.
         */
        virtual void onDraw(void) override;
        
    public:
        /**
         * Gets our current flag.
         */
        const Flag & getFlag(void) const;
        
        /**
         * Sets flag to a new one.
         */
        void setFlag(const Flag & flag);
    };

}

#endif

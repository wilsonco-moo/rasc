/*
 * FlagComponent.cpp
 *
 *  Created on: 11 Dec 2018
 *      Author: wilson
 */

#include "../../../ui/customComponents/flag/FlagComponent.h"

namespace rasc {

    FlagComponent::FlagComponent(const rascUI::Location & location, Flag flag) :
        rascUI::Component(location),
        flag(flag) {
    }

    FlagComponent::FlagComponent(const rascUI::Location & location, simplenetwork::Message & msg) :
        rascUI::Component(location),
        flag(msg) {

    }

    FlagComponent::~FlagComponent() {
    }

    bool FlagComponent::shouldRespondToKeyboard(void) const {
        return false;
    }

    void FlagComponent::onDraw(void) {
        flag.draw(location);
    }

    const Flag & FlagComponent::getFlag(void) const {
        return flag;
    }

    void FlagComponent::setFlag(const Flag & flag) {
        this->flag = flag;
    }
}

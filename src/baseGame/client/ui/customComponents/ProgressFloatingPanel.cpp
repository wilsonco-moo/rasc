/*
 * ProgressFloatingPanel.h
 *
 *  Created on: 7 Jul 2019
 *      Author: wilson
 */

#include "ProgressFloatingPanel.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/FunctionQueue.h>
#include <threading/ThreadQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#define LAYOUT_THEME theme
#define CLOSE_BUTTON_WIDTH   128

#define MAIN_SECTION_LOC                       \
    SUB_BORDERMARGIN_B(UI_BHEIGHT,             \
     SUB_BORDER(GEN_FILL))

#define BOTTOM_SECTION_LOC                     \
    PIN_B(UI_BHEIGHT,                          \
     SUB_BORDER(GEN_FILL))

#define BOTTOM_PANEL_LOC                       \
    SUB_BORDERMARGIN_R(CLOSE_BUTTON_WIDTH,     \
     BOTTOM_SECTION_LOC)

#define CLOSE_BUTTON_LOC                       \
    PIN_R(CLOSE_BUTTON_WIDTH,                  \
     BOTTOM_SECTION_LOC)

#define ROW_LOC(id)                            \
    MOVE_BORDERNORMAL_Y(id,                    \
     SETNORMAL_Y(                              \
      MAIN_SECTION_LOC                         \
    ))

namespace rasc {

    // ----------------------- ProgressFloatingPanelBase --------------------------------

    ProgressFloatingPanelBase::ProgressFloatingPanelBase(rascUI::Theme * theme, const rascUI::Location & location, std::function<void(void)> onShowFunc, std::function<void(void)> onHideFunc) :
        rascUI::FloatingPanel(location, onShowFunc, onHideFunc),
        frontPanel(rascUI::Location(MAIN_SECTION_LOC)),
        bottomPanel(rascUI::Location(BOTTOM_PANEL_LOC)),
        closeButton(rascUI::Location(CLOSE_BUTTON_LOC), "Close",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) hide();
            }
        ),
        topLabel(rascUI::Location(ROW_LOC(0))),
        bottomLabel(rascUI::Location(ROW_LOC(1))) {

        contents.add(&frontPanel);
        contents.add(&bottomPanel);
        contents.add(&topLabel);
        contents.add(&bottomLabel);
        contents.add(&closeButton);
    }

    ProgressFloatingPanelBase::~ProgressFloatingPanelBase(void) {
    }

    // ----------------------------- ProgressFloatingPanel ------------------------------

    ProgressFloatingPanel::ProgressFloatingPanel(threading::ThreadQueue * threadQueue, void * token, rascUI::Theme * theme, bool barrier, const rascUI::Location & location, std::function<void(void)> onProgressShowFunc, std::function<bool(void)> backgroundJobFunc, std::function<void(void)> onWaitFunc, std::function<void(bool)> onEndFunc, std::function<void(void)> onProgressHideFunc) :
        ProgressFloatingPanelBase(theme, location, onProgressShowFunc, onProgressHideFunc),
        threadQueue(threadQueue),
        barrier(barrier),
        token(token),
        jobId(0),
        wasJobSuccessful(true),
        backgroundJobFinished(false),
        backgroundJobFunc(backgroundJobFunc),
        onWaitFunc(onWaitFunc),
        onEndFunc(onEndFunc) {

        closeButton.setActive(false);
    }

    ProgressFloatingPanel::~ProgressFloatingPanel(void) {
    }

    // NOTE: We can rely on the fact that onDraw is NEVER called until onShow has ALREADY BEEN CALLED.
    void ProgressFloatingPanel::onDraw(void) {

        // There is nothing to do if the background job has already done, we are just
        // waiting for the user to close.
        if (!backgroundJobFinished) {
            // If the job has JUST finished, since the previous call to onDraw:
            if (threadQueue->getJobStatus(jobId) == threading::JobStatus::complete) {
                // Then set backgroundJobFinished so this isn't called again
                backgroundJobFinished = true;
                // Do all of these things together, after this event. After event is required
                // since we are messing around with the keyboard selection.
                getTopLevel()->afterEvent->addFunction([this](void) {
                    closeButton.setActive(true);
                    onEnd(wasJobSuccessful);
                    getTopLevel()->setKeyboardSelected(&closeButton);
                });
            // If the job has not finished yet, run the user's onWait function.
            } else {
                onWait();
            }
        }
        FloatingPanel::onDraw();
    }

    // NOTE: We can rely on the fact that onDraw is NEVER called until onShow has ALREADY BEEN CALLED.
    void ProgressFloatingPanel::onShow(void) {

        // Reset everything when we get shown, since onDraw relies on this.
        backgroundJobFinished = false;
        closeButton.setActive(false);

        // This is run in the thread queue. It is safe to set wasJobSuccessful
        // in another thread, since we never read this value until after the background
        // job has definately completed.
        std::function<void(void)> func = [this](void) {
            wasJobSuccessful = backgroundJob();
        };

        // Run this just before we start the background job.
        onProgressShow();

        // Run the background job, either (whether it is a barrier is specified by user).
        if (barrier) {
            jobId = threadQueue->addBarrier(token, func);
        } else {
            jobId = threadQueue->add(token, func);
        }
    }

    void ProgressFloatingPanel::onHide(void) {

        // Make sure that the background job has stopped.
        threadQueue->waitForJobToComplete(jobId);

        // If this was called manually while the background job was running, this will
        // not have been called.
        if (!backgroundJobFinished) {
            onEnd(wasJobSuccessful);
        }

        // Run this since we are hiding.
        onProgressHide();
    }

    void ProgressFloatingPanel::onProgressShow(void) { onShowFunc(); }
    bool ProgressFloatingPanel::backgroundJob(void) { return backgroundJobFunc(); }
    void ProgressFloatingPanel::onWait(void) { onWaitFunc(); }
    void ProgressFloatingPanel::onEnd(bool success) { onEndFunc(success); }
    void ProgressFloatingPanel::onProgressHide(void) { onHideFunc(); }

}

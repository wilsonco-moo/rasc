/*
 * PlayerPanel.h
 *
 *  Created on: 7 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_PLAYERPANEL_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_PLAYERPANEL_H_

#include <cstdint>
#include <string>

#include <rascUI/components/generic/Button.h>
#include <wool/misc/RGBA.h>

namespace rascUI {
    class Theme;
}

namespace rasc {
    class PlayerData;
    class RascBox;

    /**
     * PlayerPanel acts as a universal UI representation of a particular player,
     * (or no player). It is a custom kind of button, able to display the player
     * name, and an identifying representation of the player, (namely the
     * player's colour).
     *
     * PlayerPanel must be provided a RascBox, since when clicked, it must lead the
     * user to an appropriate menu about the player. If the PlayerPanel represents
     * no player, or the player no longer exists, clicking must do nothing.
     */
    class PlayerPanel : public rascUI::Button {
    private:
        // The RascBox, to allow us access to the "outside world".
        RascBox & box;

        // The player's internal ID and colour, for reference of which player we
        // are holding info about. The player's name gets stored in our button text.
        uint64_t playerId;
        wool::RGBA colour;
        bool turnStatus;

        // The location of the colour and button. These are updated when we resize
        // or change state.
        rascUI::Location colourLocation,
                         leftColourLocation,
                         rightColourLocation;
    public:
        PlayerPanel(const rascUI::Location & location, RascBox & box, rascUI::Theme * theme);
        virtual ~PlayerPanel(void);
    protected:
        // Overridden so we can update our internal locations.
        virtual void recalculate(const rascUI::Rectangle & parentSize) override;
        // Overridden for custom drawing.
        virtual void onDraw(void) override;
        // To provide player show action.
        virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;
    public:

        /**
         * Changes the player which we are showing info about. This can be NULL, at which
         * point we will show info about no player.
         */
        void setPlayerByData(PlayerData * data);

        /**
         * Changes the player which we are showing info about. This can be NULL, at which
         * point we will show info about no player.
         */
        void setPlayerById(uint64_t playerId);

        /**
         * Accesses the ID of the player that we are currently displaying info about.
         */
        inline uint64_t getPlayerId(void) const {
            return playerId;
        }
    };
}

#endif

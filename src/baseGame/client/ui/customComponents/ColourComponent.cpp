/*
 * ColourComponent.cpp
 *
 *  Created on: 10 Dec 2018
 *      Author: wilson
 */

#include "../../ui/customComponents/ColourComponent.h"

#include <GL/gl.h>

#include <sockets/plus/message/Message.h>

namespace rasc {

    ColourComponent::ColourComponent(const rascUI::Location & location, wool::RGBA colour) :
        rascUI::Component(location),
        colourInt(colour.intValue()),
        colourRGBA(colour) {
    }

    ColourComponent::ColourComponent(const rascUI::Location & location, uint32_t colour) :
        rascUI::Component(location),
        colourInt(colour),
        colourRGBA(colour) {
    }

    ColourComponent::ColourComponent(const rascUI::Location & location, simplenetwork::Message & msg) :
        rascUI::Component(location),
        colourInt(msg.read32()),
        colourRGBA(colourInt) {
    }

    ColourComponent::~ColourComponent() {
    }

    bool ColourComponent::shouldRespondToKeyboard(void) const {
        return false;
    }



    #define GET_COORDINATES                                 \
        GLfloat x1 = location.cache.x,                      \
                y1 = location.cache.y,                      \
                x2 = x1 + location.cache.width,             \
                y2 = y1 + location.cache.height;


    void ColourComponent::onDraw(void) {
        GET_COORDINATES
        wool_setColourRGBA(colourRGBA);
        glVertex2f(x1, y1);
        glVertex2f(x2, y1);
        glVertex2f(x2, y2);
        glVertex2f(x1, y2);
    }

    void ColourComponent::writeTo(simplenetwork::Message & msg) const {
        msg.write32(colourInt);
    }

    void ColourComponent::setColour(wool::RGBA colour) {
        colourRGBA = colour;
        colourInt = colour.intValue();
    }

    void ColourComponent::setColour(uint32_t colour) {
        colourInt = colour;
        colourRGBA = wool::RGBA(colour);
    }
}

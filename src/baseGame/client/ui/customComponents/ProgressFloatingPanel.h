/*
 * ProgressFloatingPanel.h
 *
 *  Created on: 7 Jul 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_PROGRESSFLOATINGPANEL_H_
#define BASEGAME_CLIENT_UI_CUSTOMCOMPONENTS_PROGRESSFLOATINGPANEL_H_

#include <functional>

#include <rascUI/components/floating/FloatingPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/components/generic/FrontPanel.h>

namespace threading {
    class ThreadQueue;
}

namespace rascUI {
    class Theme;
}

namespace rasc {

    /**
     * ProgressFloatingPanelBase is the base class for ProgressFloatingPanel.
     * This simply provides the layout, with two panels, two labels, and a close
     * button which hides the floating panel.
     * This can be used on it's own if the progress floating panel layout is needed,
     * but the threading functionality is not.
     */
    class ProgressFloatingPanelBase : public rascUI::FloatingPanel {
    public:
        rascUI::FrontPanel frontPanel, bottomPanel;
        rascUI::Button closeButton;

        /**
         * The text in either topLabel bottomLabel should be modified by outside classes or
         * subclasses to show the current status.
         */
        rascUI::Label topLabel;
        rascUI::Label bottomLabel;

        ProgressFloatingPanelBase(
            rascUI::Theme * theme,
            const rascUI::Location & location = rascUI::Location(-160, -120, 320, 240, 0.5f, 0.5f, 0.0f, 0.0f),
            std::function<void(void)> onShowFunc = [](void){},
            std::function<void(void)> onHideFunc = [](void){});

        virtual ~ProgressFloatingPanelBase(void);
    };


    /**
     * ProgressFloatingPanel is an extension to FloatingPanel, which provides a text
     * area, and a close button. Upon opening, we start a job on a thread queue.
     * The user cannot close the floating panel until it is finished.
     * Manually calling the hide() method will forcefully wait until the background job
     * is done, then hide the floating panel, (while ensuring that onEnd and onProgressHide
     * are still run.
     * Currently, (at the time of writing this), this class is used in the connection popup
     * (in the main menu), and the save popup (in-game).
     */
    class ProgressFloatingPanel : public ProgressFloatingPanelBase {
    private:
        threading::ThreadQueue * threadQueue;
        bool barrier;
        void * token;
        uint64_t jobId;
        bool wasJobSuccessful,
             backgroundJobFinished;

        std::function<bool(void)> backgroundJobFunc;
        std::function<void(void)> onWaitFunc;
        std::function<void(bool)> onEndFunc;

    public:

        /**
         * The constructor for ProgressFloatingPanel. The following parameters must be supplied:
         *  theme:              The rascUI Theme implementation. This must be supplied so we can build a layout.
         *  barrier:            If set to true, the background job will be run as a barrier job. Otherwise it will be
         *                      run as a normal job.
         *  location:           The location of the main content area, this is passed to FloatingPanel.
         *  onProgressShowFunc: This is called when we are shown, like FloatingPanel's onShowFunc. This is called
         *                      immediately before the background job is added to the ThreadQueue.
         *                      Alternatively, if more convenient, the onProgressShow method can be overridden.
         *  backgroundJobFunc:  The background job to run on the ThreadQueue. This should return true or false
         *                      depending on success or failure.
         *                      Alternatively, if more convenient, the backgroundJob method can be overridden.
         *  onWaitFunc:         This is called every frame, but only while the background job is running, (i.e:
         *                      after onProgressShow and before onEnd and onProgressHide).
         *                      Alternatively, if more convenient, the onWait method can be overridden.
         *  onEndFunc:          This is always called once, after the background job has completed, before onProgressHide,
         *                      even if hide() is called manually while waiting.
         *                      This is called from within the top level component's afterEvent function queue.
         *                      Alternatively, if more convenient, the onEnd method can be overridden.
         *  onProgressHideFunc: This is called when the user presses the close button, from within the top level
         *                      component's afterEvent function queue. When this runs, onEnd will have already been
         *                      called, even if hide() is called manually while waiting.
         *                      Alternatively, if more convenient, the onProgressHide method can be overridden.
         */
        ProgressFloatingPanel(threading::ThreadQueue * threadQueue,
                              void * token,
                              rascUI::Theme * theme,
                              bool barrier = false,
                              const rascUI::Location & location = rascUI::Location(-160, -120, 320, 240, 0.5f, 0.5f, 0.0f, 0.0f),
                              std::function<void(void)> onProgressShowFunc = [](void){},
                              std::function<bool(void)> backgroundJobFunc = [](void)->bool{return false;},
                              std::function<void(void)> onWaitFunc = [](void){},
                              std::function<void(bool)> onEndFunc = [](bool success){},
                              std::function<void(void)> onProgressHideFunc = [](void){});
        virtual ~ProgressFloatingPanel(void);

    protected:
        virtual void onDraw(void) override;
        virtual void onShow(void) override final;
        virtual void onHide(void) override final;

        /**
         * This is called when we are shown, like FloatingPanel's onShowFunc. This is called
         * immediately before the background job is added to the ThreadQueue.
         * Instead of overriding this, a std::function can be provided to the constructor, if more convenient.
         */
        virtual void onProgressShow(void);

        /**
         * The background job to run on the ThreadQueue. This should return true or false
         * depending on success or failure.
         * Instead of overriding this, a std::function can be provided to the constructor, if more convenient.
         */
        virtual bool backgroundJob(void);

        /**
         * This is called every frame, but only while the background job is running, (i.e:
         * after onProgressShow and before onEnd and onProgressHide).
         * Instead of overriding this, a std::function can be provided to the constructor, if more convenient.
         */
        virtual void onWait(void);

        /**
         * This is always called once, after the background job has completed, before onProgressHide,
         * even if hide() is called manually while waiting.
         * This is called from within the top level component's afterEvent function queue.
         * Instead of overriding this, a std::function can be provided to the constructor, if more convenient.
         */
        virtual void onEnd(bool success);

        /**
         * This is called when the user presses the close button, from within the top level
         * component's afterEvent function queue. When this runs, onEnd will have already been
         * called, even if hide() is called manually while waiting.
         * Instead of overriding this, a std::function can be provided to the constructor, if more convenient.
         */
        virtual void onProgressHide(void);
    };
}

#endif

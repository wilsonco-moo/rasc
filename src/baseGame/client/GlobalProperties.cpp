/*
 * GlobalProperties.cpp
 *
 *  Created on: 4 Jan 2019
 *      Author: wilson
 */

#include "GlobalProperties.h"

#include <threading/ThreadQueue.h>
#include <rascUI/base/Theme.h>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cmath>

#include "../common/config/StaticConfig.h"
#include "config/TerrainDisplayManager.h"
#include "../common/config/RascConfig.h"
#include "mainMenu/MainMenuBackground.h"
#include "mainMenu/tabs/OptionsMenu.h"

// Calculates the ui scale from the exponent ui scale.
#define CALC_UI_SCALE ((GLfloat)pow(2.0f, uiScaleExp))
// The increment when increasing/decreasing the UI scale
#define UI_SCALE_INCREMENT 0.125f

namespace rasc {

    // --------------------------- DEFAULT VALUES (CONSTANTS) -----------------

    const std::string GlobalProperties::DEFAULT_ADDRESS                  = "";
    const uint16_t    GlobalProperties::DEFAULT_PORT                     = 64000;
    const GLfloat     GlobalProperties::DEFAULT_CONNECTION_TIMEOUT       = 4.0f;
    const GLfloat     GlobalProperties::DEFAULT_UI_SCALE_EXP             = 0.0f;
    const std::string GlobalProperties::DEFAULT_PLAYER_NAME              = "Bob";
    const std::string GlobalProperties::DEFAULT_SINGLEPLAYER_PLAYER_NAME = "Dave";
    const std::string GlobalProperties::DEFAULT_PORT_STRING              = "64000";

    const size_t      GlobalProperties::MINIMUM_THREADS                  = 1;
    const size_t      GlobalProperties::MAXIMUM_THREADS                  = 5;
    const std::string GlobalProperties::LOCAL_ADDRESS                    = "127.0.0.1";

    // ------------------------------------------------------------------------


    GlobalProperties::GlobalProperties(const std::string & address, const std::string & playerName, uint16_t port, GLfloat connectionTimeout, GLfloat uiScaleExp, bool startInternalServer) :
        uiScaleExp(uiScaleExp),
        uiScale(CALC_UI_SCALE),
        address(address),
        playerName(playerName),
        port(port),
        connectionTimeout(connectionTimeout),
        mapFilename(),
        serverVersion(),
        threadQueue(NULL),
        mainMenuBackground(NULL),
        terrainDisplayManager(NULL),
        rascConfig(NULL),
        themeLoader(),
        currentThemeFriendlyName(""),
        startInternalServer(startInternalServer),
        aisToPlace(0),
        startNew(true),
        saveName("") {
    }

    GlobalProperties::~GlobalProperties(void) {

        delete terrainDisplayManager;
        delete mainMenuBackground;
        delete rascConfig;

        // NOTE: THE THREAD QUEUE MUST BE DELETED AFTER EVERYTHING THAT DEPENDS ON IT.
        std::cout << "Stopping threads...\n";
        delete threadQueue;
        std::cout << "Done.\n";
    }

    void GlobalProperties::increaseUIScale(void) {
        uiScaleExp += UI_SCALE_INCREMENT;
        uiScale = CALC_UI_SCALE;
    }
    void GlobalProperties::decreaseUIScale(void) {
        uiScaleExp -= UI_SCALE_INCREMENT;
        uiScale = CALC_UI_SCALE;
    }

    void GlobalProperties::resetUIScale(void) {
        uiScaleExp = DEFAULT_UI_SCALE_EXP;
        uiScale = CALC_UI_SCALE;
    }

    void GlobalProperties::titleRoomInitialisation(void) {

        std::cout << "Creating thread queue and launching threads...\n";
        threadQueue = new threading::ThreadQueue(MINIMUM_THREADS, MAXIMUM_THREADS);

        // Now we have a thread queue, we can create the config system.
        std::cout << "Done, loading config file...\n";
        rascConfig = new RascConfig(threadQueue);

        // Finally, dynamically load the default theme.
        std::cout << "Done, dynamically loading default theme...\n";
        ThemeSelectionMenu::loadCorrectTheme(this);
        std::cout << "Done.\n";
    }

    void GlobalProperties::mainMenuRoomInitialisation(void) {
        std::cout << "Starting loading of main menu background, and traits/types textures...\n";
        mainMenuBackground = new MainMenuBackground(threadQueue);
        terrainDisplayManager = new TerrainDisplayManager(threadQueue);
    }
}

/*
 * RascBox.cpp
 *
 *  Created on: 19 Dec 2018
 *      Author: wilson
 */

#include "RascBox.h"

#include <iostream>
#include <cstddef>

#include <rascUI/components/floating/FloatingPanel.h>
#include <sockets/plus/message/Message.h>
#include <wool/texture/ThreadTexture.h>
#include <rascUI/util/Rectangle.h>
#include <threading/ThreadQueue.h>
#include <wool/texture/Texture.h>
#include <rascUI/util/Layout.h>
#include <tinyxml2/tinyxml2.h>
#include <rascUI/base/Theme.h>

#include "../common/stats/types/UnitsDeployedStat.h"
#include "../common/playerData/PlayerDataSystem.h"
#include "../common/stats/types/ContinentsStat.h"
#include "../common/stats/types/ProvincesStat.h"
#include "../common/stats/types/AdjacencyStat.h"
#include "../common/stats/types/BuildingStat.h"
#include "../common/stats/UnifiedStatSystem.h"
#include "../common/stats/types/AreasStat.h"
#include "ui/containers/billMenu/BillMenu.h"
#include "ui/containers/gameMenu/GameMenu.h"
#include "../common/playerData/PlayerData.h"
#include "config/UnitPlacementController.h"
#include "../common/update/RascUpdatable.h"
#include "../common/config/StaticConfig.h"
#include "../common/RascUpdatableBoxIds.h"
#include "mainMenu/MainMenuBackground.h"
#include "config/MapModeController.h"
#include "config/CameraProperties.h"
#include "../common/GameMessages.h"
#include "../common/util/XMLUtil.h"
#include "gameMap/ClientGameMap.h"
#include "../server/ServerBox.h"
#include "RascClientNetIntf.h"
#include "GlobalProperties.h"
#include "config/MapModes.h"
#include "ui/GameUI.h"

#define LAYOUT_THEME theme

namespace rasc {


    // ------------------------------- Game initialiser inner class ----------------------------------

        RascBox::GameInitialiser::GameInitialiser(RascBox * box) :
            box(box),
            token(box->globalProperties->threadQueue->createToken()),

            jobId(box->globalProperties->threadQueue->addBarrier(token, [this](void) {

                // It is our job to create the GameMap, once textures are loaded, so do that.

                std::cout << "Completed loading textures, so creating GameMap (ClientGameMap), (loading map XML data).\n";
                this->box->map = new ClientGameMap(*this->box);
                this->box->common.map = this->box->map;
                {
                    tinyxml2::XMLDocument xml;
                    std::string mapConfigFilename = StaticConfig::getMapConfigFile(this->box->globalProperties->mapFilename);
                    loadFile(xml, mapConfigFilename.c_str());
                    this->box->map->loadFromXML(&xml);
                }

                // ------------------------------ Register Rasc updatable children ------------------------------------
                // Register rasc updatables, before we tell the network interface that they exist. Otherwise
                // the network interface's initial map updates will have nowhere to go.
                // We MUST ENSURE that the order is consistent with ServerBox and RascUpdatableBoxIds.
                this->box->registerChild(this->box->map);
                this->box->registerChild(this->box->common.playerDataSystem);
                // ----------------------------------------------------------------------------------------------------

                // Now we have a GameMap instance, tell our network interface that one now exists.
                // This will apply all initial updates for the map, and any other misc updates
                // which happened while we were loading.
                // NOTE: Any calls to the stat system will be ignored here, since initialise has not
                //       yet been called.
                this->box->netIntf->onGameMapCreated();

                // Now we've told network interface that game map has been created, it can apply
                // misc updates at any time. So before we muck about with systems which use game
                // map data, we must lock!
                std::lock_guard<std::recursive_mutex> lock(this->box->netIntf->getKillableMutex());
                
                // Now the map has been loaded, initialise the stat system.
                // This can take a while, so do it here in the GameInitialiser.
                // This must be done AFTER all the initial data has been loaded.
                this->box->common.statSystem->initialise(this->box->common);

                std::cout << "Completed creation of GameMap (ClientGameMap).\n";

            })) {

        }
        RascBox::GameInitialiser::~GameInitialiser(void) {
            box->globalProperties->threadQueue->deleteToken(token);
        }

    // -----------------------------------------------------------------------------------------------





    RascBox::RascBox(GlobalProperties * globalProperties) :
        TopLevelRascUpdatable(RascUpdatable::Mode::array, BOX_IDENTIFIER_BYTES),

        // Use an initialiser list only for stuff we do not yet create, which we initialise
        // to NULL. Everything else is assigned in the constructor, since stuff must be
        // initialised in an order not possible to do in an initialiser list.
        map(NULL),                          // created in GameInitialiser
        mapModeController(NULL),            // created in enterLoadingRoom
        window(NULL),                       // assigned in enterLoadingRoom
        globalProperties(globalProperties),
        mapModeTextures(NULL),              // created in enterLoadingRoom
        provincesTexture(NULL),             // created in enterLoadingRoom
        gameInitialiser(NULL) {             // created in enterLoadingRoom

        // Set the initial turn count, plus other CommonBox members which already exist in GlobalProperties.
        common.turnCount = 0;
        common.rascConfig = globalProperties->rascConfig;

        // If we are supposed to start an internal server, create a ServerBox. Otherwise, set this to NULL.
        serverBox = globalProperties->startInternalServer ? new ServerBox(globalProperties->mapFilename, globalProperties->aisToPlace, false) : NULL;
        unitPlacementController = new UnitPlacementController();
        netIntf = new RascClientNetIntf(*this);

        // <><><><><><><><><><><><><><><><><><><><> Create stats <><><><><><><><><><><><><><><><><><><><>
        common.statSystem = new UnifiedStatSystem();
        common.areasStat = new AreasStat();
        common.continentsStat = new ContinentsStat();
        common.provincesStat = new ProvincesStat();
        common.unitsDeployedStat = new UnitsDeployedStat();
        common.adjacencyStat = new AdjacencyStat();
        common.buildingStat = new BuildingStat();
        // <><><><> Register stats ><><><><><><><>
        common.statSystem->registerStat(common.areasStat);
        common.statSystem->registerStat(common.continentsStat);
        common.statSystem->registerStat(common.provincesStat);
        common.statSystem->registerStat(common.unitsDeployedStat);
        common.statSystem->registerStat(common.adjacencyStat);
        common.statSystem->registerStat(common.buildingStat);
        // <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

        common.random = NULL; // Note: Random number generation isn't actually needed on the client at the moment!
        cameraProperties = new CameraProperties(&(globalProperties->uiScale));
        common.playerDataSystem = new PlayerDataSystem(common);
        ui = new GameUI(*this); // Create the UI AFTER the player data system, and AFTER the network interface.

        // Set the UI scale pointers.
        ui->setScalePointers(&globalProperties->uiScale, &globalProperties->uiScale);
    }

    RascBox::~RascBox(void) {
        delete ui; // Delete UI first, since it registers callbacks with many systems.
        delete unitPlacementController;
        delete netIntf;
        delete cameraProperties;
        delete map;
        delete mapModeController;
        // Don't delete the window: We did not create it.
        // Don't delete the global properties: We did not create it either.

        delete common.areasStat;
        delete common.continentsStat;
        delete common.provincesStat;
        delete common.unitsDeployedStat;
        delete common.adjacencyStat;
        delete common.buildingStat;
        delete common.statSystem; // Delete new stat system, after all stats.

        delete gameInitialiser;
        delete common.playerDataSystem;

        // Delete the server box, AFTER deleting our own network interface.
        delete serverBox;

        // Delete the textures last.
        delete[] mapModeTextures;
        delete provincesTexture;
    }


    void RascBox::enterLoadingRoom(wool::WindowBase * window) {
        this->window = window;
        mapModeController = new MapModeController(*this);

        // NOTE: We no longer send the player name here. This is instead done as a Misc update, in RascClientNetIntf,
        //       when we receive the initialTurnCount message.

        // Create the textures, and start them loading.

        std::string mapPath = StaticConfig::MAPS_LOCATION + globalProperties->mapFilename + "/";

        mapModeTextures = new wool::ThreadTexture[MapModes::MODES_COUNT];
        for (MapMode mapMode = 0; mapMode < MapModes::MODES_COUNT; mapMode++) {
            mapModeTextures[mapMode].load(globalProperties->threadQueue, mapPath+StaticConfig::MAP_TEXTURES[mapMode], false);
        }

        provincesTexture = new wool::ThreadTexture(globalProperties->threadQueue, mapPath+"provinces.png", false);

        // Traits used to be loaded here.

        // Create the game initialiser, AFTER STARTING THE TEXTURES LOADING, AND AFTER CREATING THE MAP
        // MODE CONTROLLER.
        gameInitialiser = new GameInitialiser(this);

        // Notify the UI system, after creating the game initialiser.
        ui->onChangeToLoadingRoom();
    }

    void RascBox::enterLobbyRoom(void) {
        // First delete the game initialiser: It has done it's job now.
        // Since the loading room will wait for the game initialiser to finish, before switching rooms, it must have
        // finished by now.
        delete gameInitialiser;
        gameInitialiser = NULL;

        // NOTE: Due to the GameInitialiser, GameMap is guaranteed to exist by this point in time.

        // Ensure the initial map mode texture is loaded into VRAM.
        mapModeTextures[mapModeController->getCurrentMapMode()].loadVRamCopy();
        // Tell GlobalProperties that we have left the loading room, so it must unload it's texture.
        globalProperties->mainMenuBackground->onLeaveLoadingScreen();
        // Centre the camera, using the gameMap. This can no longer be done in the GameMap's constructor,
        // because it is created in a different thread.
        map->centreCamera();
        // Also notify the UI system.
        ui->onChangeToLobbyRoom();
    }

    void RascBox::enterGameRoom(void) {

        ui->onChangeToGameRoom();
    }


    // =================================== ACCESSOR METHODS ================================================

    // -------------------- The UI system -----------------------

    // ----------------------------- UI -------------------------------------------

    void RascBox::drawUI(GLfloat elapsed) {
        rascUI::Rectangle rect(0.0f, 0.0f, cameraProperties->viewWidth, cameraProperties->viewHeight);
        ui->drawUI(rect, elapsed);
    }
    bool RascBox::onUIMouseEvent(int button, int state, GLfloat viewX, GLfloat viewY) { return ui->onUIMouseEvent(button, state, viewX, viewY); }
    bool RascBox::onUIMouseMove(GLfloat viewX, GLfloat viewY) { return ui->onUIMouseMove(viewX, viewY); }
    bool RascBox::onUIKeyPress(int key, bool special) { return ui->onUIKeyPress(key, special); }
    bool RascBox::onUIKeyRelease(int key, bool special) { return ui->onUIKeyRelease(key, special); }
    void RascBox::uiBillMenuShowPlayer(uint64_t playerId) { ui->billMenu.showPlayer(playerId); }
    void RascBox::uiOpenOverlayMenu(unsigned int menuId, bool autoOpen, bool forceOpen) { ui->openOverlayMenu(menuId, autoOpen, forceOpen); }
    void RascBox::uiShowProvinceMenu(rasc::Province * province) { ui->provinceMenu.show(province); }
    void RascBox::uiHideProvinceMenu(void) { ui->provinceMenu.hide(); }
    GLfloat RascBox::getModularTopbarHeight(rascUI::Theme * theme) { return COUNT_OUTBORDER_Y(ModularTopbar::ROWS); }
    GLfloat RascBox::getBottomLeftUIHeight(rascUI::Theme * theme) { return BottomLeftUI::getHeight(theme); }
    GLfloat RascBox::getProvinceMenuTotalWidth(rascUI::Theme * theme) { return ProvinceMenu::getTotalWidth(theme); }
    GLfloat RascBox::getProvinceMenuBottomBorder(rascUI::Theme * theme) { return ProvinceMenu::getBottomBorder(theme); }
    GLfloat RascBox::getClientSidePlayerListWidth(rascUI::Theme * theme) { return ClientSidePlayerList::getWidth(theme); }
    bool RascBox::uiIsProvinceMenuOpen(void) { return ui->provinceMenu.isVisible(); }
    bool RascBox::uiIsBuildMenuOpen(void) { return ui->buildMenu.isVisible(); }
    bool RascBox::uiLobbyProvinceWarnIfNeeded(const uint32_t provinceId) { return ui->lobbyProvincePopup.warnIfNeeded(provinceId); };
    void RascBox::uiBuildMenuOnLeftClickProvince(Province * province) { ui->buildMenu.onLeftClickProvince(province); }
    void RascBox::uiBuildMenuOnRightClickProvince(Province * province) { ui->buildMenu.onRightClickProvince(province); }
    void RascBox::uiSetModularTopbarButtonSelected(void) { ui->playerList.setModularTopbarButtonSelected(); }
    void RascBox::uiPlayerColourBarNotifyRecalculate(void) { ui->playerColourBar.notifyRecalculate(); }
    GLfloat RascBox::uiGetModularTopbarWidth(void) { return ui->modularTopbar.location.position.width; }
    void RascBox::uiKeyboardSelectBillButton(void) { ui->playerList.keyboardSelectBillButton(); }
    void RascBox::uiKeyboardSelectMenuButton(void) { ui->playerList.keyboardSelectMenuButton(); }
    ModularTopbar & RascBox::uiGetModularTopbar(void) { return ui->modularTopbar; }
    ModularTopbarConfigMenu & RascBox::uiGetModularTopbarConfigMenu(void) { return ui->modularTopbarConfigMenu; }
    PropertyDisplayPanel & RascBox::uiGetPropertyDisplayPanel(void) { return ui->propertyDisplayPanel; }
    BottomLeftUI & RascBox::uiGetBottomLeftUI(void) { return ui->bottomLeft; }
    ProvinceMenu & RascBox::uiGetProvinceMenu(void) { return ui->provinceMenu; }

    void RascBox::runSplitArmies(void) {
        map->getArmySelectController().splitArmies(CheckedUpdate::clientAuto(netIntf));
    }

    void RascBox::runMergeArmies(void) {
        map->getArmySelectController().mergeArmies(CheckedUpdate::clientAuto(netIntf));
    }

    void RascBox::runAbsorbArmiesIntoGarrisons(void) {
        map->getArmySelectController().absorbArmiesIntoGarrisons(CheckedUpdate::clientAuto(netIntf), netIntf->getData()->getId());
    }

    wool::Texture & RascBox::getCurrentMapModeTexture(void) {
        return *mapModeTextures[mapModeController->getCurrentMapMode()].getInternalTexture();
    }

    bool RascBox::hasInitialiserJobFinished(void) {
        return globalProperties->threadQueue->getJobStatus(gameInitialiser->getJobId()) == threading::JobStatus::complete;
    }
}

/*
 * RascClientNetIntf.h
 *
 * RascClientNetIntf controls all the network-side logic for the client.
 * It is the client-side equivalent of the HumanPlayer class, and in fact communicated directly
 * with a HumanPlayer instance.
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_RASCCLIENTNETINTF_H_
#define BASEGAME_CLIENT_RASCCLIENTNETINTF_H_

#include <GL/gl.h>
#include <cstdint>
#include <list>

#include <sockets/plus/message/Message.h>
#include <sockets/plus/networkinterface/NetworkInterface.h>
#include "../server/Player.h"

namespace rasc {

    class CameraProperties;
    class GameMap;
    class GameUI;
    class RascBox;

    /**
     * The main network interface for the client side of the game.
     *
     * NOTE:
     *  Stuff which *is* synchronised to the main mutex:
     *   > Access to internal state, such as the current turn count
     *   > Access to our own player data
     *   > Access to any RascUpdatable class which could be made inconsistent by updates, such as PlayerDataSystem and GameMap.
     *   > Any place where we apply RascUpdatable updates, i.e: Misc updates, server updates and initial data.
     *  Stuff which *is not* synchronised to the main mutex:
     *   > Accesses to the stat system, as the stat system still contains it's own mutex
     *   > Accesses to UI callback methods, as they already use a synchronised "run later" type system.
     */
    class RascClientNetIntf : public simplenetwork::NetworkInterface, public Player {
    private:

        /**
         * The current rasc game box.
         */
        RascBox & box;

        /**
         * This stores any initial map updates we get, before setGameMap is called.
         */
        std::list<simplenetwork::Message *> initialMapUpdates;

        /**
         * This controls queueing of map updates, until the game map is created.
         */
        bool gameMapNotYetCreated;

        /**
         * This stores the player id which the server initially sends us when we join. This is used once we
         * enter the lobby room to assign our PlayerData. This should not be used after that, as it will not
         * be accurate.
         */
        uint64_t prospectivePlayerId;

    private:
        /**
         * Stores the internal ui scale, for the ui system
         */
        GLfloat internalUIScale;

        /**
         * This stores true if we have already notified the condition variable about receiving the map
         * name and game version.
         */
        bool mapNameNotified;

        /**
         * The condition variable to notify when we receive the map filename and game version.
         * This can be set with setOnReceiveMapNameNotify.
         * If we are disconnected before receiving our map name, this is also notified.
         *
         * Note: This MUST be set before connecting, and MUST only ever be notified ONCE.
         */
        std::condition_variable_any * mapNameCondition;

    public:

        RascClientNetIntf(RascBox & box);
        virtual ~RascClientNetIntf(void);

        virtual void onConnected(void) override;
        virtual void receive(simplenetwork::Message & msg) override;
        virtual void onDisconnected(void) override;

        /**
         * Sets the condition variable to notify when we receive our map name and game version.
         * THIS MUST BE CALLED BEFORE WE CONNECT TO THE SERVER.
         */
        void setMapNameCondition(std::condition_variable_any * mapNameCondition);

        /**
         * This returns true when we have received the map name from the server, and notified mapNameCondition.
         */
        bool haveNotifiedAboutMapName(void);

        // ---------------------- TURN STUFF ----------------------

    private:
        bool finishedTurn;
    public:
        void onTurnStarted(void);
        void finishTurn(void);

        virtual bool isHumanPlayer(void) const override;

        // THIS IS NOT USED ON THE CLIENT SIDE.
        virtual bool startTurn(simplenetwork::Message & msg) override;

        // ----------------- GENERAL PURPOSE ACCESSOR METHODS ------------

        // NOTE: THESE MEHTODS ARE NOT SYNCHRONISED, AND MUST ONLY BE USED WHEN IN GAME.
        // WE ARE ONLY SURE THAT OUR PLAYER ID AND COLOUR HAS ACTUALLY ARRIVED, ONCE WE
        // ARE IN GAME.

        virtual bool haveFinishedTurn(void) override;

        // ----------------------- DELAYED INITIAL MAP UPDATE SYSTEM -----------

        /**
         * This should be called by GameRoom once all textures and everything are loaded.
         * Since the RascUpdatable system is not created until we enter the lobby room, we cannot actually apply any misc updates
         * or initial data. So we save them and apply them later, once we have the rasc updatable system.
         * At this point we can now also assign our player data.
         */
        void onGameMapCreated(void);


    private:
        /**
         * If our game map is NULL:
         *     This method adds a duplicate message to the initialMapUpdates list,
         *     then returns false.
         * If we have a NON-NULL game map:
         *     This message does nothing and returns true.
         */
        bool processMapUpdate(simplenetwork::Message & msg);




        // -------------------------- LOBBY SYSTEM -------------------------------

        /**
         * This is set to true when we get our initial player id and colour from the server.
         */
        bool receivedPlayerIdFromServer;

        /**
         * This is set to true when we receive confirmation that our chosen province (in the lobby screen)
         * is valid.
         */
        bool lobbyConfirmed;

        /**
         * This has the value true, between calling lobbyClaimProvince and receiving a response from the server.
         */
        bool waitingForServerResponseInLobby;

        /**
         * This is run internally when we receive a response from the server, during the lobby screen.
         */
        void onLobbyServerResponse(simplenetwork::Message & msg);

    public:

        /**
         * This returns true when we have received our initial player id, colour and turn count.
         * The loading room should not continue to the lobby room until this returns true.
         */
        bool readyToContinueToLobby(void);

        /**
         * This returns true when we have received confirmation from the server that our chosen starting
         * point is valid. The lobby room should not continue to the game room until this returns true.
         */
        bool readyToContinueToGame(void);

        /**
         * This sends a message to the server to claim the specified province.
         */
        void lobbyClaimProvince(uint32_t provinceId);

        /**
         * This has the value true, between calling lobbyClaimProvince and receiving a response from the server.
         */
        bool isWaitingForServerResponseInLobby(void);

        /**
         * Requests, from the server, a colour change of the specified player.
         * This must only be used after we have exited from the lobby.
         * This is the correct way to request a player colour change from the client side.
         */
        void requestColourChange(uint64_t playerId, uint32_t newColour);

        /**
         * This allows access to our main mutex, from our Killable superclass.
         * All client-side code should be synchronised to this, to ensure that the GameMap is not modified while
         * drawing, and the make synchronisation on the client-side significantly simpler.
         */
        inline std::recursive_mutex & getKillableMutex(void) {
            return killableMutex;
        }
    };
}

#endif

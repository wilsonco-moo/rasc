/*
 * MapModes.h
 *
 *  Created on: 20 Mar 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_CONFIG_MAPMODES_H_
#define BASEGAME_CLIENT_CONFIG_MAPMODES_H_


namespace rasc {

    /**
     * This header file simply provides an enum of different map modes.
     */
    namespace MapModes {
        enum {
            /**
             * The geographical map mode, shows the terrain type.
             */
            geographical = 0,
            /**
             * The provincial map mode, shows provinces easily.
             */
            provincial = 1,
            /**
             * The area map mode, shows the borders between areas, with faint
             * borders between provinces.
             */
            area = 2,
            /**
             * The continental map mode, shows the borders between continents,
             * with faint borders between provinces and areas.
             */
            continental = 3,
            /**
             * The total number of map modes.
             */
            MODES_COUNT = 4
        };
    };

    typedef int MapMode;
}

#endif

/*
 * UnitPlacementController.cpp
 *
 *  Created on: 14 Dec 2018
 *      Author: wilson
 */

#include "UnitPlacementController.h"

namespace rasc {

    UnitPlacementController::UnitPlacementController(void) :
        mode(UnitPlacementModes::single),
        modifiedMode(UnitPlacementModes::single),
        modifierKey1(false),
        modifierKey2(false),
        onModeChange() {
    }

    UnitPlacementController::~UnitPlacementController(void) {
    }

    void UnitPlacementController::updateMode(bool updateUI) {
        UnitPlacementMode newMode;

        if (modifierKey1 && modifierKey2) {
            newMode = UnitPlacementModes::all;
        } else if (modifierKey1) {
            newMode = UnitPlacementModes::dual;
        } else if (modifierKey2) {
            newMode = UnitPlacementModes::triple;
        } else {
            newMode = mode;
        }

        if (newMode != modifiedMode) {
            modifiedMode = newMode;
            if (updateUI) onModeChange.updateFunctions(newMode);
        } else {
            // If we are not supposed to update the UI, but the mode has been set to something
            // different, then update the UI anyway to reset the toggle buttons.
            if (!updateUI && mode != newMode) onModeChange.updateFunctions(newMode);
        }
    }
    
    void * UnitPlacementController::addOnModeChangeFunc(std::function<void(UnitPlacementMode)> func) {
        return onModeChange.addFunction(func);
    }
    
    void UnitPlacementController::removeOnModeChangeFunc(void * token) {
        onModeChange.removeFunction(token);
    }

    void UnitPlacementController::setModifierKey1(bool value) {
        if (value != modifierKey1) {
            modifierKey1 = value;
            updateMode(true);
        }
    }

    void UnitPlacementController::setModifierKey2(bool value) {
        if (value != modifierKey2) {
            modifierKey2 = value;
            updateMode(true);
        }
    }

    void UnitPlacementController::setMode(UnitPlacementMode mode, bool updateUI) {
        this->mode = mode;
        updateMode(updateUI);
    }

    int64_t UnitPlacementController::getMaximumUnitsToPlace(void) {
        switch(modifiedMode) {
        case UnitPlacementModes::single:
            return 1000;
        case UnitPlacementModes::dual:
            return 2000;
        case UnitPlacementModes::triple:
            return 3000;
        case UnitPlacementModes::all:
            return -1;
        default:
            return 1;
        }
    }
}

/*
 * UnitPlacementController.h
 *
 *  Created on: 14 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_CONFIG_UNITPLACEMENTCONTROLLER_H_
#define BASEGAME_CLIENT_CONFIG_UNITPLACEMENTCONTROLLER_H_

#include <functional>
#include <cstddef>
#include <cstdint>

#include "../../common/util/templateTypes/TokenFunctionList.h"

namespace rasc {

    /**
     * All the unit placement modes.
     */
    class UnitPlacementModes {
    public:
        enum {
            single = 0,
            dual = 1,
            triple = 2,
            all = 3
        };
    };
    typedef int UnitPlacementMode;


    /**
     * This class controls the current unit placement mode.
     */
    class UnitPlacementController {
    private:
        // Stores the current unit placement mode, as set by setMode.
        UnitPlacementMode mode;

        // How the unit placement mode appears to the outside, after applying modifiers.
        UnitPlacementMode modifiedMode;

        // Stores whether the two modifier keys are pressed.
        bool modifierKey1, modifierKey2;
        
        // This allows the UI to listen for unit placement mode changes.
        TokenFunctionList<void(UnitPlacementMode)> onModeChange;

    public:
        UnitPlacementController(void);
        virtual ~UnitPlacementController(void);

    private:

        // Internally this updates the modifiedMode, and calls setOnModeChangeFunc if necessary.
        // This will call onModeChangeFunc if updateUI is set to true.
        void updateMode(bool updateUI);

    public:
        
        /**
         * This should be used by the UI. Adds and removes mode change functions, called
         * when the unit placement mode changes. All must be removed before we're destroyed.
         */
        void * addOnModeChangeFunc(std::function<void(UnitPlacementMode)> func);
        void removeOnModeChangeFunc(void * token);
        
        /**
         * Sets whether modifier key 1 is pressed, (0 = not pressed, 1 = pressed).
         */
        void setModifierKey1(bool value);

        /**
         * Sets whether modifier key 2 is pressed, (0 = not pressed, 1 = pressed).
         */
        void setModifierKey2(bool value);

        /**
         * Allows the unit placement mode to be set. Modifier keys will affect this.
         * This will call onModeChangeFunc if updateUI is set to true.
         */
        void setMode(UnitPlacementMode mode, bool updateUI);

        /**
         * Gets the current unit placement mode, (after modifiers).
         */
        inline UnitPlacementMode getMode(void) const {
            return modifiedMode;
        }

        /**
         * Returns the maximum number of units we could place in one click, using the current
         * placement mode.
         * This can return -1, which means ALL units possible should be placed.
         */
        int64_t getMaximumUnitsToPlace(void);
    };

}

#endif

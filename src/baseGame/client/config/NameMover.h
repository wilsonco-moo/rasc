/*
 * NameMover.h
 *
 *  Created on: 12 Oct 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_CONFIG_NAMEMOVER_H_
#define BASEGAME_CLIENT_CONFIG_NAMEMOVER_H_

/**
 * Uncomment to enable name moving functionality (for development).
 */
//#define ENABLE_NAME_MOVER


#ifdef ENABLE_NAME_MOVER

#include <GL/gl.h>

namespace rasc {
    class RascBox;

    /**
     * This class provides the ability to conveniently move around province,
     * area and continent labels with the i,j,k,l keys, increase/decrease
     * their size with the h,n keys, and export the changes with the ; key.
     *
     * This is a convenient development tool, for adjusting the positioning
     * of map labels when developing maps. By default it should be disabled.
     */
    class NameMover {
    private:
        RascBox & box;

        bool movingUp, movingDown, movingLeft, movingRight, makingBigger, makingSmaller;

    public:
        NameMover(RascBox & box);

    private:
        void updateXmlFile(void);

    public:
        void onNormalKeyPress(unsigned char key);
        void onNormalKeyRelease(unsigned char key);
        void step(GLfloat elapsed, GLfloat viewHeight);
    };
}
#endif
#endif

/*
 * TerrainDisplayManager.cpp
 *
 *  Created on: 20 Jun 2020
 *      Author: wilson
 */

#include "TerrainDisplayManager.h"

#include <wool/texture/lodepng/lodepng.h>
#include <threading/ThreadQueue.h>
#include <functional>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <utility>
#include <cstring>
#include <string>
#include <cstdio>

#include "../../common/util/numeric/ChArith.h"
#include "../../common/config/StaticConfig.h"
#include "../../common/util/Misc.h"
#include "../GlobalProperties.h"

namespace rasc {

    // Stores the name of each texture ID. Note that this is not the full filename,
    // rather it is just the name (such as "boggy").
    #define X(terrainDisplayName) #terrainDisplayName,
    const static std::string TEXTURE_NAMES[TerDispTex::COUNT] = { RASC_TERRAIN_DISPLAY_TEXTURES };
    #undef X

    // This works out the filename for a texture ID.
    #define GET_FILENAME(textureId) \
        (StaticConfig::TERRAIN_DISPLAY_LOCATION + TEXTURE_NAMES[textureId] + ".png")

    // This gets the pointer within texture data, for the specified texture ID.
    // This assumes that the texture data, width and height have been initialised.
    // It is assumed that the texture ID is a valid texture ID.
    #define GET_TEXTURE(texId) \
        (textureData + ((texId) * textureSizeBytes))

    TerrainDisplayManager::TerrainDisplayManager(threading::ThreadQueue * threadQueue) :
        threadQueue(threadQueue),
        loadMutex(),
        token(threadQueue->createToken()),
        width(0),
        height(0),
        textureSizeBytes(0),
        failedTextureCount(0),
        textureData(NULL) {

        // Start load jobs for all textures.
        for (unsigned int textureId = 0; textureId < TerDispTex::COUNT; textureId++) {
            threadQueue->add(token, [this, textureId](void) {
                loadThread(textureId);
            });
        }
    }

    TerrainDisplayManager::~TerrainDisplayManager(void) {
        // Stop any load operations, then free the texture data (the only thing we possibly allocated).
        threadQueue->deleteToken(token);
        free(textureData);
    }

    bool TerrainDisplayManager::updateWidthHeight(size_t newWidth, size_t newHeight, unsigned int textureId) {
        std::lock_guard<std::mutex> lock(loadMutex);
        bool ret = true;

        // If texture data has not been allocated yet
        if (textureData == NULL) {

            // Complain and use default size if either new size is zero.
            if (newWidth == 0 || newHeight == 0) {
                std::cerr << "WARNING: TerrainDisplayManager: " << "Loaded image with zero size width or height.\n";
                width = 32;
                height = 32;
                ret = false; // Return false since we used default size.

            // Otherwise use the resolution.
            } else {
                width = newWidth;
                height = newHeight;
            }
            bool overflow = false;

            // Work out the size of EACH texture in bytes (check for overflow).
            textureSizeBytes = ChArith::multiply<size_t>(&overflow, 4,
                                   ChArith::multiply<size_t>(&overflow, width, height));

            // Work out the total allocation size (across all textures) (check for overflow).
            size_t allocationSize = ChArith::multiply<size_t>(&overflow, textureSizeBytes, TerDispTex::COUNT);

            printf("Allocating %.2f MiB for terrain display textures.\n", allocationSize / (double)(1024 * 1024));

            // If overflow happened, or attempting to allocate texture data failed, complain and exit.
            if (overflow || (textureData = (unsigned char *)malloc(allocationSize)) == NULL) {
                std::cerr << "WARNING: TerrainDisplayManager: " << "Texture \"" << TEXTURE_NAMES[textureId] <<
                             "\": Failed to allocate " << allocationSize << " for texture, or overflow when calculating texture size.\n";
                exit(EXIT_FAILURE);
            }

        // If texture data *has* already been allocated, check whether the provided image size is the same as the previous.
        } else {
            if (newWidth != width || newHeight != height) {
                std::cerr << "WARNING: TerrainDisplayManager: " << "Texture \"" << TEXTURE_NAMES[textureId] <<
                             "\": Province terrain texture size mismatch: found image with size of " << newWidth <<
                             'x' << newHeight <<", expected: " << width << 'x' << height << ".\n";
                ret = false;
            }
        }

        return ret;
    }

    void TerrainDisplayManager::markFailedTexture(void) {
        std::lock_guard<std::mutex> lock(loadMutex);
        failedTextureCount++;
        if (failedTextureCount == TerDispTex::COUNT) {
            std::cerr << "CRITICAL ERROR: TerrainDisplayManager: All terrain display textures failed to load, so we have no texture allocation.\n";
            exit(EXIT_FAILURE);
        }
    }

    void TerrainDisplayManager::loadThread(unsigned int textureId) {
        // Load the texture using lodepng.
        unsigned char * thisTexData = NULL;
        unsigned int newWidth, newHeight;
        std::string filename = GET_FILENAME(textureId);
        std::cout << "Loading texture " << filename << "...\n";
        unsigned int error = lodepng_decode32_file(&thisTexData, &newWidth, &newHeight, filename.c_str());

        // Complain and mark failed if there is an error reported (free the data anyway).
        if (error || newWidth > SIZE_MAX || newHeight > SIZE_MAX) {
            std::cerr << "WARNING: TerrainDisplayManager: Failed to load texture: " << filename << ", check that the file exists, and is a valid image.\n";
            free(thisTexData);
            markFailedTexture();
            return;
        }

        // Update the width and height. This ensures that the texture data is allocated.
        // If this succeeded, convert to premultiplied alpha then copy our texture data
        // to the main texture data.
        if (updateWidthHeight((size_t)newWidth, (size_t)newHeight, textureId)) {
            Misc::straightToPremultiplied(thisTexData, textureSizeBytes);
            memcpy(GET_TEXTURE(textureId), thisTexData, textureSizeBytes);

        // Mark a failure if this did not succeed.
        } else {
            markFailedTexture();
        }

        // Free our texture, now it is copied into the main texture allocation.
        free(thisTexData);
    }

    // Use to compare terrain display layers, for sorting.
    static bool compareTerrainLayers(const TerDispLayer & layer1, const TerDispLayer & layer2) {
        return layer1.depth < layer2.depth;
    }

    void TerrainDisplayManager::buildTerrainTexture(std::vector<TerDispLayer> layers, unsigned char * image) const {

        // Fill the image with zeroes (transparent by default).
        memset(image, 0, textureSizeBytes);

        // Sort all of the terrain display layers (lowest depth first).
        std::sort(layers.begin(), layers.end(), &compareTerrainLayers);

        // Alpha over the appropriate texture for each layer (note that they all already have premultiplied alpha).
        for (const TerDispLayer & layer : layers) {
            Misc::alphaOver(image, GET_TEXTURE(layer.textureId), textureSizeBytes);
        }

        // Convert the texture back to straight alpha.
        Misc::premultipliedToStraight(image, textureSizeBytes);
    }
}

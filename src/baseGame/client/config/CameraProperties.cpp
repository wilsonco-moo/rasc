/*
 * CameraProperties.cpp
 *
 *  Created on: 15 Aug 2018
 *      Author: wilson
 */

#include "CameraProperties.h"

#include <cmath>
#include <cstdlib>

#include "../../common/gameMap/mapElement/MapElement.h"
#include "../../common/gameMap/units/Army.h"
#include "../../common/util/XMLUtil.h"


namespace rasc {

    /**
     * The minimum text scale if the view size was 768 pixels tall, see the documentation in CameraProperties.h
     * for minimumTextScale.
     */
    #define MINIMUM_TEXT_SCALE_768 0.7f


    /**
     * The factor we make the actual exponent world scale closer to the actual exponent world scale, each second.
     */
    #define WORLD_SCALE_FACTOR_PER_SECOND 0.006f

    /**
     * The minimum target/actual difference that we will ignore and jump the actual exponent world scale straight to the target.
     */
    #define WORLD_SCALE_MIN_DIFF 0.01f


    /**
     * The camera's move speed, measured as:
     *     "The proportion of the height of the display moved per second."
     */
    #define CAMERA_MOVE_SPEED 0.4f



    CameraProperties::CameraProperties(GLfloat * uiScale) :
        movingUp(false),
        movingDown(false),
        movingLeft(false),
        movingRight(false),
        worldScaleExpTarget(0.0f),
        worldScaleExpActual(0.0f),
        x(0.0f),
        y(0.0f),
        xCamera(0.0f),
        yCamera(0.0f),
        xScale(1.0f),
        yScale(1.0f),
        provId(MapElement::INVALID_MAP_ELEMENT),
        armyId(Army::INVALID_ARMY),
        viewWidth(800.0f),
        viewHeight(600.0f),
        worldBackgroundColour(),
        minimumTextScale(0.0f),
        minimapDrawX(0.0f),
        minimapDrawY(0.0f),
        minimapDrawScale(1.0f),
        minimapOffsetX(0.0f),
        minimapOffsetY(0.0f),
        minimapScale(1.0f),
        minimapActualWidth(1.0f),
        minimapActualHeight(1.0f),
        minimapBackgroundColour(),
        uiScale(uiScale) {
    }

    CameraProperties::~CameraProperties(void) {
    }


    void CameraProperties::zoom(int amount) {
        worldScaleExpTarget += amount;

        yScale = xScale;
    }

    void CameraProperties::step(GLfloat elapsed, GLfloat viewWidth, GLfloat viewHeight) {

        // First we set the view width and view height, from the values provided.
        this->viewWidth = viewWidth;
        this->viewHeight = viewHeight;

        // Next we update the smooth camera zooming.
        if (worldScaleExpTarget != worldScaleExpActual) {
            // Get the difference between the target exponent world scale and the actual one.
            double diff = worldScaleExpActual - worldScaleExpTarget;
            if (fabs(diff) < WORLD_SCALE_MIN_DIFF) {
                // If the difference is too small, set the actual value to the target.
                worldScaleExpActual = worldScaleExpTarget;
            } else {
                // Otherwise, move actual closer to target by a factor of WORLD_SCALE_FACTOR_PER_SECOND to the power of the elapsed time. This framerate smooths it.
                worldScaleExpActual = worldScaleExpTarget + diff * pow(WORLD_SCALE_FACTOR_PER_SECOND, elapsed);
            }
            // Finally, set the xscale and yscale to two to the power of worldScaleExpActual.
            xScale = (GLfloat)pow((double)2.0f, worldScaleExpActual);
            yScale = xScale;
        }

        // Then we update the camera movement.
        if (movingUp) {
            yCamera -= (CAMERA_MOVE_SPEED * elapsed * viewHeight) / yScale;
        }
        if (movingDown) {
            yCamera += (CAMERA_MOVE_SPEED * elapsed * viewHeight) / yScale;
        }
        if (movingLeft) {
            xCamera -= (CAMERA_MOVE_SPEED * elapsed * viewHeight) / xScale;
        }
        if (movingRight) {
            xCamera += (CAMERA_MOVE_SPEED * elapsed * viewHeight) / xScale;
        }

        // Now, we work out the origin position to draw the map, from the camera's position on the map.
        x = viewWidth * 0.5f - xCamera * xScale;
        y = viewHeight * 0.5f - yCamera * yScale;

        // After that, we work out the scale and position to draw the minimap, from the current ui scale
        // and the view width/height.
        minimapDrawScale = minimapScale * (*uiScale);
        minimapDrawX = minimapOffsetX * minimapDrawScale;
        minimapDrawY = minimapOffsetY * minimapDrawScale + viewHeight - MINIMAP_HEIGHT * (*uiScale);

        // Finally, we work out the minimum text scale, from the current view height.
        minimumTextScale = (viewHeight / 768.0f) * MINIMUM_TEXT_SCALE_768;
    }

    void CameraProperties::readGlobalProperties(tinyxml2::XMLElement * globalProperties) {

        tinyxml2::XMLElement * globalMapProperties = readElement(globalProperties, "map");
            worldBackgroundColour = wool::RGBA(hexStringToInt(readAttribute(globalMapProperties, "backgroundColour")));


        tinyxml2::XMLElement * globalMinimapProperties = readElement(globalProperties, "minimap");
            minimapBackgroundColour = hexStringToInt(readAttribute(globalMinimapProperties, "backgroundColour"));

            tinyxml2::XMLElement * baseSize = readElement(globalMinimapProperties, "baseSize");
                GLfloat baseWidth  = (GLfloat)strtol(readAttribute(baseSize, "width"), NULL, 0);
                        // Currently the base height is ignored, but this may be used if the minimap system is modified in the future.
                        strtol(readAttribute(baseSize, "height"), NULL, 0);

            tinyxml2::XMLElement * actualSize = readElement(globalMinimapProperties, "actualSize");
                minimapOffsetX = (GLfloat)strtol(readAttribute(actualSize, "xOffset"), NULL, 0);
                minimapOffsetY = (GLfloat)strtol(readAttribute(actualSize, "yOffset"), NULL, 0);

                minimapActualWidth = (GLfloat)strtol(readAttribute(actualSize, "width"), NULL, 0),
                minimapActualHeight = (GLfloat)strtol(readAttribute(actualSize, "height"), NULL, 0);

            // Work out the minimap scale, to fit it into the area.
            minimapScale = MINIMAP_WIDTH / baseWidth;
    }

}























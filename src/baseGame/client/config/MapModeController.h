/*
 * MapModeController.h
 *
 *  Created on: 17 Aug 2018
 *      Author: wilson
 *
 * This class contains all of the textures for different map modes, and also holds which map
 * mode the game is using, as well as an enum for the different map modes.
 */

#ifndef BASEGAME_CLIENT_CONFIG_MAPMODECONTROLLER_H_
#define BASEGAME_CLIENT_CONFIG_MAPMODECONTROLLER_H_

#include <GL/gl.h>
#include <stddef.h>
#include <functional>
#include <string>

#include "MapModes.h"

namespace rasc {

    class RascBox;

    class MapModeController {
    private:
        /**
         * Stores the loading messages for the top of the screen.
         */
        static std::string topLoadingMessages[MapModes::MODES_COUNT + 1];
        /**
         * Stores the loading messages for the bottom of the screen.
         */
        static std::string bottomLoadingMessages[MapModes::MODES_COUNT + 1];
        /**
         * The RascBox.
         */
        RascBox & box;
        /**
         * Stores the current map mode.
         */
        MapMode currentMapMode;
        /**
         * This allows the UI to be updated when the map mode changes.
         */
        std::function<void(MapMode)> onChangeMapModeFunc;

    public:
        /**
         * The map mode controller can be generated if a map name is provided.
         * The map name is needed, since the map mode controller is in charge of storing textures.
         */
        MapModeController(RascBox & box);

        virtual ~MapModeController(void);

        /**
         * Changes the current map mode in use. This calls onChangeMapModeFunc, if callOnChangeMapModeFunc is set to true (the default).
         */
        void setMapMode(MapMode mapMode, bool callOnChangeMapModeFunc = true);

        /**
         * This should be used by the UI, as to receive updates when the map mode changes.
         */
        void setOnChangeMapModeFunc(std::function<void(MapMode)> func);


        inline MapMode getCurrentMapMode(void) {
            return currentMapMode;
        }
    };
}

#endif

/*
 * TerrainDisplayManager.h
 *
 *  Created on: 20 Jun 2020
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_CONFIG_TERRAINDISPLAYMANAGER_H_
#define BASEGAME_CLIENT_CONFIG_TERRAINDISPLAYMANAGER_H_

#include <vector>
#include <mutex>

#include "../../common/util/definitions/TerrainDisplayDef.h"

namespace threading {
    class ThreadQueue;
}

namespace rasc {

    /**
     * This class manages the loading and storage of terrain display textures.
     * Note that most methods of this class MUST NOT BE USED UNTIL ALL TEXTURES
     * HAVE FINISHED LOADING. To know that all textures have finished loading,
     * add a barrier job to the thread queue at some point AFTER creating this,
     * then WAIT FOR IT TO COMPLETE. After that, it is guaranteed that all images
     * have finished loading.
     *
     * Note also that this class converts then stores all terrain display textures
     * using premultiplied alpha, to allow for more convenient blending.
     */
    class TerrainDisplayManager {
    private:
        // For performing asynchronous load operations.
        threading::ThreadQueue * threadQueue;

        // A mutex to control asynchronous load operations.
        std::mutex loadMutex;

        // A thread queue token, to control asynchronous load operations.
        void * token;

        // The width and height of the textures.
        // This is not known until the first image is loaded: until
        // then these are set to zero. The field textureSizeBytes is the
        // size of each texture in bytes.
        size_t width, height, textureSizeBytes;

        // This counts the number of failed loaded textures. This is set by
        // markFailedTexture, (see comment for it).
        unsigned int failedTextureCount;

        // After the first texture loads, this is allocated to something
        // large enough to store all textures. This stores all texture data,
        // using premultiplied alpha.
        unsigned char * textureData;

        // Don't allow copying: we hold raw resources.
        TerrainDisplayManager(const TerrainDisplayManager & other);
        TerrainDisplayManager & operator = (const TerrainDisplayManager & other);

    public:
        /**
         * A TerrainDisplayManager requires a thread queue. When created, asynchronous
         * load operations are immediately added to the thread queue.
         */
        TerrainDisplayManager(threading::ThreadQueue * threadQueue);
        ~TerrainDisplayManager(void);

    private:
        // This should be called by load threads, after they load
        // each image. This updates the width and height, and allocates
        // the texture data if necessary. If widths and heights conflict,
        // complaints are printed. Returns true if successful, i.e: if
        // the thread should write its image data to the texture data array.
        bool updateWidthHeight(size_t newWidth, size_t newHeight, unsigned int textureId);

        // This should be called by loadThread, if any part of loading a texture
        // failed. This increments failedTextureCount. If all textures have failed,
        // an exit(EXIT_FAILURE) is performed as we won't have an allocation.
        void markFailedTexture(void);

        // The load thread function.
        void loadThread(unsigned int textureId);

    public:

        /**
         * Returns true if ALL textures have loaded successfully. Note that this
         * class is still usable as long as at least one texture loaded successfully,
         * but those which didn't will have missing (uninitialised) textures.
         *
         * Note that this must only be called once all textures have *finished* loading,
         * see documentation at the top of the file.
         */
        inline bool hasLoadedSuccessfully(void) const {
            return failedTextureCount == 0;
        }

        /**
         * Returns the width of our textures.
         * Note that this is stored and returned as a size_t for convenience, but
         * will never be larger than can be represented with an unsigned int.
         *
         * Note that this must only be called once all textures have *finished* loading,
         * see documentation at the top of the file.
         */
        inline size_t getWidth(void) const {
            return width;
        }

        /**
         * Returns the height of our textures.
         * Note that this is stored and returned as a size_t for convenience, but
         * will never be larger than can be represented with an unsigned int.
         *
         * Note that this must only be called once all textures have *finished* loading,
         * see documentation at the top of the file.
         */
        inline size_t getHeight(void) const {
            return height;
        }

        /**
         * The size of each texture, in bytes. This is equivalent to 4*width*height.
         *
         * Note that this must only be called once all textures have *finished* loading,
         * see documentation at the top of the file.
         */
        inline size_t getTextureSizeBytes(void) const {
            return textureSizeBytes;
        }

        /**
         * Builds a terrain display texture from "layers", and writes it to "image".
         * Note that "image" must point to an allocation at least as big as textureSizeBytes.
         * Before drawing any texture layers, the image is filled with zeroes (transparent).
         * The "layers" vector is passed by value since internally we sort it.
         *
         * Note that this must only be called once all textures have *finished* loading,
         * see documentation at the top of the file.
         */
        void buildTerrainTexture(std::vector<TerDispLayer> layers, unsigned char * image) const;
    };
}

#endif

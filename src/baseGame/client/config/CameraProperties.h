/*
 * CameraProperties.h
 *
 *  Created on: 15 Aug 2018
 *      Author: wilson
 *
 * This contains all the camera properties, such as it's location.
 * Also controls camera movement.
 */

#ifndef BASEGAME_CLIENT_CONFIG_CAMERAPROPERTIES_H_
#define BASEGAME_CLIENT_CONFIG_CAMERAPROPERTIES_H_

#include <GL/gl.h>
#include <cstdint>

#include <wool/misc/RGBA.h>


namespace tinyxml2 {
    class XMLElement;
}



namespace rasc {

    class CameraProperties {

    private:
        bool movingUp, movingDown, movingLeft, movingRight;

        /**
         * This is the target world scale. This is updated when we zoom in and out.
         */
        double worldScaleExpTarget;

        /**
         * This is the actual exponent world scale. This is made closer to worldScaleExpTarget by a factor
         * of WORLD_SCALE_FACTOR_PER_SECOND each second. When the difference between this and worldScaleExpTarget
         * drops below WORLD_SCALE_MIN_DIFF, this is made equal to worldScaleExpTarget.
         */
        double worldScaleExpActual;

    public:

        // The origin position of where the world map is drawn, calculated from cameraX and cameraY.
        GLfloat x, y;

        /**
         * The position in the world, in non-scaled pixels, that the center of the camera is located.
         * The camera is moved according to the speed defined in the macro CAMERA_MOVE_SPEED, in CameraProperties.cpp.
         * CAMERA_MOVE_SPEED is measured as: "The proportion of the height of the display moved per second."
         */
        GLfloat xCamera, yCamera;

        /**
         * The x and y scale to draw the world.
         * These are calculated as two to the power of worldScaleExpActual.
         */
        GLfloat xScale, yScale;


        // The province we are mousing over
        uint32_t provId;

        // The army we are mousing over
        uint64_t armyId;

        /**
         * The current view width and height.
         */
        GLfloat viewWidth, viewHeight;

        /**
         * The background colour of the world, read from the XML file.
         */
        wool::RGBA worldBackgroundColour;

        /**
         * The minimum scale to draw text on the map. Below this scale, text is not drawn.
         * This provides culling of small and unreadable text. This value is calculated from the view
         * height, in the step method.
         *
         * Note: Text is 14 pixels tall at scale 1. The minimum text scale is calculated from the macro
         * MINIMUM_TEXT_SCALE_768, (in CameraProperties.cpp), which defines the minimum text scale when
         * the view height is 768 pixels, i.e: a standard 1024x768 display.
         */
        GLfloat minimumTextScale;



        // ----------- GLOBAL UI PROPERTIES ----------------

        // The size the UI appears on-screen.
        #define MINIMAP_WIDTH 256.0f
        #define MINIMAP_HEIGHT 128.0f


        /*
             OLD UI CONSTANTS: NO LONGER USED AS OF THE NEW UI SYSTEM.

        // The border for buttons
        #define UI_BORDER !.!.!

        // Remove these macros when the new UI system is in place

        // The colour of standard UI elements
        #define UI_OUTLINE_COLOUR 0.7f, 0.7f, 0.7f, 1.0f
        #define UI_FILL_COLOUR 0.3f, 0.3f, 0.3f, 1.0f
        #define UI_SELECTED_FILL_COLOUR 0.9f, 0.9f, 0.9f, 1.0f
        #define UI_HIGHLIGHTED_FILL_COLOUR 0.0f, 0.0f, 0.0f, 1.0f

        // The standard specification for text for UI elements
        #define UI_TEXT_COLOUR 1.0f, 1.0f, 1.0f, 1.0f
        #define UI_SELECTED_TEXT_COLOUR 0.0f, 0.0f, 0.0f, 1.0f
        #define UI_TEXT_SIZE 1.3f
        #define UI_TEXT_Y 6.0f
        */



        // ------------------------------------------------

        /**
         * These are calculated when step() is called.
         */
        GLfloat minimapDrawX, minimapDrawY, minimapDrawScale;

    private:
        /**
         * These are read from the XML file, and are used to calculate minimapDrawX, minimapDrawY and minimapDrawScale.
         */
        GLfloat minimapOffsetX, minimapOffsetY, minimapScale;

    public:
        /**
         * These are used for calculating the scale to draw the camera highlight.
         */
        GLfloat minimapActualWidth, minimapActualHeight;


        /**
         * The background colour of the minimap, read from the XML file.
         */
        wool::RGBA minimapBackgroundColour;


        /**
         * This allows the ui scale to be accessible from CameraProperties.
         */
        GLfloat * uiScale;

        // -----------------------------------------------



        CameraProperties(GLfloat * uiScale);
        virtual ~CameraProperties(void);

        void zoom(int amount);

        /**
         * Updates the camera's movement, view width and height, and minimap position.
         * This should be called once per step.
         */
        void step(GLfloat elapsed, GLfloat viewWidth, GLfloat viewHeight);

        inline void setMovingUp(bool value) {
            movingUp = value;
        }
        inline void setMovingDown(bool value) {
            movingDown = value;
        }
        inline void setMovingLeft(bool value) {
            movingLeft = value;
        }
        inline void setMovingRight(bool value) {
            movingRight = value;
        }



        void readGlobalProperties(tinyxml2::XMLElement * globalProperties);


    };

}

#endif

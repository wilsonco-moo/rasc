/*
 * NameMover.cpp
 *
 *  Created on: 12 Oct 2020
 *      Author: wilson
 */

#include "NameMover.h"

#ifdef ENABLE_NAME_MOVER

#include <algorithm>
#include <iostream>
#include <fstream>
#include <cctype>
#include <vector>
#include <string>
#include <cmath>

#include "../../common/gameMap/mapElement/types/Continent.h"
#include "../../common/gameMap/mapElement/types/Province.h"
#include "../../common/gameMap/mapElement/types/Area.h"
#include "../../common/gameMap/mapElement/MapElement.h"
#include "../../common/config/StaticConfig.h"
#include "../../common/gameMap/GameMap.h"
#include "../gameMap/ClientGameMap.h"
#include "../GlobalProperties.h"
#include "MapModeController.h"
#include "CameraProperties.h"
#include "../RascBox.h"

namespace rasc {

    #define MOVE_SPEED 0.125f
    #define ZOOM_SPEED 1.6f

    NameMover::NameMover(RascBox & box) :
        box(box),
        movingUp(false),
        movingDown(false),
        movingLeft(false),
        movingRight(false),
        makingBigger(false),
        makingSmaller(false) {
    }

    void NameMover::onNormalKeyPress(unsigned char key) {
        switch(key) {
        case 'i': case 'I':
            movingUp = true;
            break;
        case 'j': case 'J':
            movingLeft = true;
            break;
        case 'k': case 'K':
            movingDown = true;
            break;
        case 'l': case 'L':
            movingRight = true;
            break;
        case 'h': case 'H':
            makingBigger = true;
            break;
        case 'n': case 'N':
            makingSmaller = true;
            break;
        }
    }

    void NameMover::onNormalKeyRelease(unsigned char key) {
        switch(key) {
        case 'i': case 'I':
            movingUp = false;
            break;
        case 'j': case 'J':
            movingLeft = false;
            break;
        case 'k': case 'K':
            movingDown = false;
            break;
        case 'l': case 'L':
            movingRight = false;
            break;
        case 'h': case 'H':
            makingBigger = false;
            break;
        case 'n': case 'N':
            makingSmaller = false;
            break;
        case ':': case ';':
            updateXmlFile();
            break;
        }
    }

    void NameMover::step(GLfloat elapsed, GLfloat viewHeight) {
        uint32_t provinceId = box.cameraProperties->provId;

        // Give up if no province is selected.
        if (provinceId == MapElement::INVALID_MAP_ELEMENT) return;

        // Get relevant map element base on map mode.
        MapElement * mapElement;
        GameMap * map = box.map;
        switch(box.mapModeController->getCurrentMapMode()) {
        case MapModes::geographical: case MapModes::provincial:
            mapElement = &map->getProvince(provinceId);
            break;
        case MapModes::area:
            mapElement = &map->getAreaRaw(map->getProvince(provinceId).getAreaId());
            break;
        default: // continental
            mapElement = &map->getContinent(map->getAreaRaw(map->getProvince(provinceId).getAreaId()).getContinentId());
            break;
        }

        // Find speeds.
        GLfloat moveSpeed = (MOVE_SPEED * elapsed * viewHeight) / box.cameraProperties->xScale,
                zoomSpeed = std::pow(ZOOM_SPEED, elapsed);

        // Move stuff around.
        if (movingUp)      mapElement->setCentreY(mapElement->getCentreY() - moveSpeed);
        if (movingDown)    mapElement->setCentreY(mapElement->getCentreY() + moveSpeed);
        if (movingLeft)    mapElement->setCentreX(mapElement->getCentreX() - moveSpeed);
        if (movingRight)   mapElement->setCentreX(mapElement->getCentreX() + moveSpeed);
        if (makingSmaller) mapElement->setTextWidth(mapElement->getTextWidth() / zoomSpeed);
        if (makingBigger)  mapElement->setTextWidth(mapElement->getTextWidth() * zoomSpeed);
    }

    void NameMover::updateXmlFile(void) {

        // Build vector of map elements.
        std::vector<MapElement *> mapElements;
        for (Province  * province  : box.map->getProvinces())  { mapElements.push_back(province);  }
        for (Area      * area      : box.map->getAreas())      { mapElements.push_back(area);      }
        for (Continent * continent : box.map->getContinents()) { mapElements.push_back(continent); }
        size_t mapElementUpto = 0;

        // Find in and out filenames.
        std::string inFilename = StaticConfig::getMapConfigFile(box.globalProperties->mapFilename),
                    outFilename = inFilename.substr(0, inFilename.size() - 4) + "-adjusted.xml";

        // Open files.
        std::ifstream inFile(inFilename);
        std::ofstream outFile(outFilename);


        std::string line;
        while(inFile) {
            std::getline(inFile, line);

            // Trim whitespace from beginning
            std::string trimmedLine;
            unsigned int firstNonSpace = 0;
            for (; firstNonSpace < line.size(); firstNonSpace++) {
                if (!std::isspace(line[firstNonSpace])) {
                    trimmedLine = line.substr(firstNonSpace);
                    break;
                }
            }

            // Modify text rendering lines
            if (trimmedLine.size() >= 14 && trimmedLine.substr(0, 14) == "<textRendering") {

                // Print correct number of spaces.
                for (unsigned int i = 0; i < firstNonSpace; i++) {
                    outFile << ' ';
                }

                // Get map element (assume xml file is in correct order).
                MapElement * mapElement = mapElements[mapElementUpto];
                mapElementUpto = std::min(mapElementUpto + 1, mapElements.size() - 1);

                // Print modified line using current map element data.
                outFile << "<textRendering width=\"" << (int)std::round(mapElement->getTextWidth()) <<
                                           "\" x=\"" << (int)std::round(mapElement->getCentreX()) <<
                                           "\" y=\"" << (int)std::round(mapElement->getCentreY()) << "\"/>\n";

            // Print all normal lines
            } else {
                outFile << line << '\n';
            }
        }

        std::cout << "Updated names, writing to " << outFilename << ".\n";
    }
}
#endif

/*
 * MapModeController.cpp
 *
 *  Created on: 17 Aug 2018
 *      Author: wilson
 */

#include "MapModeController.h"

#include <iostream>

#include <wool/texture/ThreadTexture.h>
#include "../RascBox.h"

namespace rasc {

    std::string MapModeController::topLoadingMessages[] = {
        "Loading texture sheets for:",
        "Loading texture sheets for:",
        "Loading texture sheets for:",
        "Loading texture sheets for:",
        "Finished."
    };

    std::string MapModeController::bottomLoadingMessages[] = {
        "Geographical map mode...",
        "Provincial map mode...",
        "Area map mode...",
        "Continental map mode...",
        "Launching game..."
    };

    MapModeController::MapModeController(RascBox & box) :
        box(box),
        currentMapMode(MapModes::area),
        onChangeMapModeFunc([](MapMode mapMode){}){
    }

    MapModeController::~MapModeController(void) {
        std::cout << "Deleting map mode controller...\n";
    }

    void MapModeController::setMapMode(MapMode mapMode, bool callOnChangeMapModeFunc) {
        if (mapMode != currentMapMode) {
            box.mapModeTextures[currentMapMode].unloadVRamCopy();
            currentMapMode = mapMode;
            box.mapModeTextures[currentMapMode].loadVRamCopy();
            if (callOnChangeMapModeFunc) {
                onChangeMapModeFunc(mapMode);
            }
        }
    }

    void MapModeController::setOnChangeMapModeFunc(std::function<void(MapMode)> func) {
        onChangeMapModeFunc = func;
    }
}


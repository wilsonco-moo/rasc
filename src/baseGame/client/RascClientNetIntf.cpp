/*
 * RascClientNetIntf.cpp
 *
 *  Created on: 13 Aug 2018
 *      Author: wilson
 */

#include "RascClientNetIntf.h"

#include <iostream>
#include <cstddef>
#include <string>
#include <mutex>

#include <sockets/plus/message/Message.h>

#include "../common/gameMap/mapElement/types/Province.h"
#include "../common/playerData/PlayerDataSystem.h"
#include "ui/containers/bottomLeft/BottomLeftUI.h"
#include "../common/stats/UnifiedStatSystem.h"
#include "../common/playerData/PlayerData.h"
#include "config/CameraProperties.h"
#include "../common/GameMessages.h"
#include "gameMap/ClientGameMap.h"
#include "../server/Player.h"
#include "GlobalProperties.h"
#include "RascBox.h"

namespace rasc {

    RascClientNetIntf::RascClientNetIntf(RascBox & box) : Player(NULL),
        box(box),
        initialMapUpdates(),
        gameMapNotYetCreated(true),
        prospectivePlayerId(0),
        internalUIScale(0.0f), // setting to zero forces a coordinate update on the first frame
        mapNameNotified(false),
        mapNameCondition(NULL),
        finishedTurn(false),
        receivedPlayerIdFromServer(false),
        lobbyConfirmed(false),
        waitingForServerResponseInLobby(false) {
    }

    RascClientNetIntf::~RascClientNetIntf(void) {
        killableDestroy();

        // Delete any remaining initial map update messages, this is required if we somehow
        // are destroyed before loading the game map.
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        while(initialMapUpdates.size() > 0) {
            simplenetwork::Message * msg = initialMapUpdates.front();
            initialMapUpdates.pop_front();
            delete msg;
        }
    }

    void RascClientNetIntf::onConnected(void) {

    }

    void RascClientNetIntf::receive(simplenetwork::Message & msg) {

        // Server --> Client  messages received.

        switch(msg.getId()) {

        // -----------------------------------------------------------------------------------
        case ServerToClient::startTurn:
            // Server changing turns requires *us* to update stats, so can only be called
            // once we actually have a GameMap.
            if (processMapUpdate(msg)) {
                onTurnStarted();
            }
            break;
        // -----------------------------------------------------------------------------------
        case ServerToClient::initialWorldData:
            if (processMapUpdate(msg)) {
                // This is a RascUpdatable update operation, so requires synchronisation.
                std::lock_guard<std::recursive_mutex> lock(killableMutex);
                box.receiveInitialData(msg);
            }
            break;
        // -----------------------------------------------------------------------------------
        case ServerToClient::mapUpdate:
            if (processMapUpdate(msg)) {
                // This is a RascUpdatable update operation, so requires synchronisation.
                std::lock_guard<std::recursive_mutex> lock(killableMutex);
                box.receiveServerUpdate(msg);
            }
            break;
        // -----------------------------------------------------------------------------------
        case ServerToClient::miscUpdate:
            if (processMapUpdate(msg)) {
                // This is a RascUpdatable update operation, so requires synchronisation.
                std::lock_guard<std::recursive_mutex> lock(killableMutex);
                box.receiveMiscUpdate(msg);
            }
            break;
        // -----------------------------------------------------------------------------------
        case ServerToClient::initialTurnCount:
            {
                std::lock_guard<std::recursive_mutex> lock(killableMutex);

                // Do this immediately, since the loading screen should only wait for us
                // to *receive* this message, not *actually apply* this message.
                receivedPlayerIdFromServer = true;

                if (processMapUpdate(msg)) {

                    // Note that since this is now treated as a map update, we don't apply this until
                    // the RascUpdatable system is active, so the player data system *does* exist by this point.

                    prospectivePlayerId = msg.read64();
                    box.common.turnCount = msg.read64();
                    box.uiGetBottomLeftUI().updateTurnCount(box.common.turnCount);
                    std::cout << "Received our prospective player id: " << prospectivePlayerId << ".\n";

                    // At this point, since the server has sent us our ID, they must have already initialised
                    // the player data for our ID. So set our player data from the player data system,
                    // and send back our name.
                    setData(box.common.playerDataSystem->idToData(prospectivePlayerId));
                    getData()->requestChangeName(CheckedUpdate::clientAuto(this), box.globalProperties->playerName);
                }
            }
            break;
        // -----------------------------------------------------------------------------------
        case ServerToClient::lobbyProvinceResponse:
            onLobbyServerResponse(msg);
            break;
        // -----------------------------------------------------------------------------------
        case ServerToClient::mapNameAndGameVersion: {
            std::lock_guard<std::recursive_mutex> lock(killableMutex);
            if (!mapNameNotified) {
                box.globalProperties->mapFilename = msg.readString();
                box.globalProperties->serverVersion = msg.readString();
                mapNameCondition->notify_all();
                mapNameNotified = true;
            }
            break;
        }

        default:
            std::cout << "Warning: Unknown message id: " << msg.getId() << " received.\n";
        }
    }

    void RascClientNetIntf::onDisconnected(void) {
        std::cout << "Note: Disconnected from server.\n";
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        if (!mapNameNotified) {
            mapNameCondition->notify_all();
            mapNameNotified = true;
        }
    }



    void RascClientNetIntf::setMapNameCondition(std::condition_variable_any * mapNameCondition) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        this->mapNameCondition = mapNameCondition;
    }

    bool RascClientNetIntf::haveNotifiedAboutMapName(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        return mapNameNotified;
    }

    // ---------------------- TURN STUFF -------------------------------

    void RascClientNetIntf::onTurnStarted(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);

        finishedTurn = false;
        box.common.turnCount++;
        box.uiGetBottomLeftUI().updateTurnCount(box.common.turnCount);
        box.common.statSystem->forceUpdate(); // Ensure that UI receives at least one stat
                                               // update per turn, even if no stats changed.
        std::cout << "--- TURN: Starting turn " << box.common.turnCount << "...\n";
    }

    void RascClientNetIntf::finishTurn(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);

        if (!finishedTurn) {
            std::cout << "--- TURN: Finishing turn...\n";
            finishedTurn = true;
            box.uiGetBottomLeftUI().onFinishTurn();
            std::cout << "--- TURN: Writing turn finish message.\n";
            send(ClientToServer::finishTurn);
        }
    }

    bool RascClientNetIntf::isHumanPlayer(void) const {
        return true;
    }

    // THIS IS NOT USED ON THE CLIENT SIDE.
    bool RascClientNetIntf::startTurn(simplenetwork::Message & msg) { return true; }


    bool RascClientNetIntf::haveFinishedTurn(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        return finishedTurn;
    }

    // ----------------------------- DELAYED INITIAL MAP UPDATE SYSTEM ------------------------



    void RascClientNetIntf::onGameMapCreated(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);

        // Set this to false so we stop queueing map updates.
        gameMapNotYetCreated = false;
        // Then loop through all the initial map updates we have queued.
        while(initialMapUpdates.size() > 0) {
            simplenetwork::Message * msg = initialMapUpdates.front();
            initialMapUpdates.pop_front();

            // Receive each of them, as we would with a normal message.
            // This will apply them to the game map.
            receive(*msg);

            // Finally, delete the queued message, as we no longer need it.
            delete msg;
        }
    }



    bool RascClientNetIntf::processMapUpdate(simplenetwork::Message & msg) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);

        if (gameMapNotYetCreated) {
            // Create a duplicate dynamically allocated message:
            // Get the old message's length.
            size_t msgLen = msg.getLength();
            // Read the entire contents of the old message's data, into a new dynamically allocated pointer.
            void * data = msg.readData(msgLen);
            // Create a new dynamically allocated message from this pointer. This new message will
            // free the pointer when it is destroyed.
            simplenetwork::Message * newMsg = new simplenetwork::Message(msg.getId(), msgLen, data);

            // Finally, we add our new duplicate message to the initialMapUpdates list.
            initialMapUpdates.push_back(newMsg);

            //std::cout << "Delaying map update message, with ID " << msg.getId() << "\n";

            return false;
        } else {

            //std::cout << "Directly reading map update message, with ID " << msg.getId() << "\n";

            return true;
        }
    }





    // -------------------------- LOBBY SYSTEM -------------------------------


    void RascClientNetIntf::onLobbyServerResponse(simplenetwork::Message & msg) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);

        waitingForServerResponseInLobby = false;
        if (msg.readBool()) {
            std::cout << "Server response: Our request to claim this province has been ACCEPTED.\n";
            // If we have been successful in choosing a province:
            // Then set this to true.
            lobbyConfirmed = true;

            if (msg.readBool()) {
                // If we have been assigned a new player id, then change our player data.
                // We're selecting an already existing player, so by changing our PlayerData,
                // we get all of their properties, like manpower, colour etc.
                uint64_t newPlayerId = msg.read64();
                setData(box.common.playerDataSystem->idToData(newPlayerId));
                std::cout << "Our player id has changed to: " << newPlayerId << ".\n";
            }

            // Ensure the stat system always gets updated, since we just got a new PlayerData.
            box.common.statSystem->forceUpdate();

        } else {
            std::cout << "Server response: Our request to claim this province has been REFUSED.\n    Reason from server: [" << msg.readString() << "].\n";
        }
    }


    bool RascClientNetIntf::readyToContinueToLobby(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        if (receivedPlayerIdFromServer) {
            return true;
        }
        std::cout << "Waiting to start game, not yet received player id...\n";
        return false;
    }

    bool RascClientNetIntf::readyToContinueToGame(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        return lobbyConfirmed;
    }

    void RascClientNetIntf::lobbyClaimProvince(uint32_t provinceId) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        std::cout << "Requesting province: " << provinceId << " (in lobby screen) from server.\n";
        simplenetwork::Message msg(ClientToServer::lobbyProvinceRequest);
        msg.write32(provinceId);
        send(msg);
        waitingForServerResponseInLobby = true;
    }

    bool RascClientNetIntf::isWaitingForServerResponseInLobby(void) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        return waitingForServerResponseInLobby;
    }

    void RascClientNetIntf::requestColourChange(uint64_t playerId, uint32_t newColour) {
        simplenetwork::Message msg(ClientToServer::requestColourChange);
        msg.write64(playerId);
        msg.write32(newColour);
        send(msg);
    }
}

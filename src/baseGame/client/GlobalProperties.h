/*
 * GlobalProperties.h
 *
 *  Created on: 4 Jan 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_GLOBALPROPERTIES_H_
#define BASEGAME_CLIENT_GLOBALPROPERTIES_H_

#include <GL/gl.h>
#include <string>
#include <cstdint>

#include <rascUItheme/utils/DynamicThemeLoader.h>

namespace threading {
    class ThreadQueue;
}

namespace rasc {

    class MainMenuBackground;
    class TerrainDisplayManager;
    class RascConfig;

    /**
     * This class represents the properties shared between the base game and the main menu.
     * This class also includes a thread queue.
     */
    class GlobalProperties {
    public:

        // ----------------------- DEFAULT VALUES (CONSTANTS) ---------------------

        /**
         * The default address to use when connecting to servers.
         */
        static const std::string DEFAULT_ADDRESS;
        /**
         * The default port to use when connecting to servers.
         */
        static const uint16_t DEFAULT_PORT;
        /**
         * The default connection timeout to use when connecting to servers.
         */
        static const GLfloat DEFAULT_CONNECTION_TIMEOUT;
        /**
         * The default value for the (exponent for the) UI scale.
         */
        static const GLfloat DEFAULT_UI_SCALE_EXP;
        /**
         * The default player name to use when connecting to servers.
         */
        static const std::string DEFAULT_PLAYER_NAME;
        /**
         * The default name to use in singleplayer mode.
         */
        static const std::string DEFAULT_SINGLEPLAYER_PLAYER_NAME;
        /**
         * The default port number that stuff should use, (as a string).
         */
        static const std::string DEFAULT_PORT_STRING;

        /**
         * The minimum and maximum threads to start in the thread queue.
         */
        static const size_t MINIMUM_THREADS, MAXIMUM_THREADS;

        /**
         * This is the address for connecting to our own internal server.
         */
        static const std::string LOCAL_ADDRESS;

        // ------------------------------------------------------------------------

    private:

        /**
         * This is the exponent ui scale, from this uiScale is calculated.
         */
        GLfloat uiScaleExp;

        // Don't allow copying, we hold raw resources.
        GlobalProperties(const GlobalProperties & other);
        GlobalProperties & operator = (const GlobalProperties & other);

    public:

        /**
         * The scale to draw the ui, calculated as two to the power of uiScaleExp.
         */
        GLfloat uiScale;

        /**
         * The current server address in use.
         */
        std::string address;
        /**
         * The current player name in use.
         */
        std::string playerName;
        /**
         * The current port that we have connected to.
         */
        uint16_t port;
        /**
         * The current connection timeout in use.
         */
        GLfloat connectionTimeout;
        /**
         * The current map filename in use.
         */
        std::string mapFilename;

        /**
         * This is set when we connect to the server. This can be used to check that the server has
         * the same game version.
         */
        std::string serverVersion;

        /**
         * A thread queue for general use.
         * This is created during titleRoomInitialisation.
         */
        threading::ThreadQueue * threadQueue;

        /**
         * The background for the main menu.
         * This is created during mainMenuRoomInitialisation.
         */
        MainMenuBackground * mainMenuBackground;

        /**
         * The terrain display manager. This stores and manages all
         * terrain display textures.
         * This is created during mainMenuRoomInitialisation.
         */
        TerrainDisplayManager * terrainDisplayManager;

        /**
         * This manages thread-safe access to persistent configuration, backed by an XML file.
         * This is created during the title room, alongside the thread queue.
         */
        RascConfig * rascConfig;

        /**
         * This provides the Theme implementation which should be used both in-game and in the
         * main menu. A theme should be loaded here during the title room.
         */
        rascUItheme::DynamicThemeLoader themeLoader;

        /**
         * This should be set to the friendly name of the currently loaded theme. Unlike the actual theme
         * filename used by themeLoader, this does not change across platforms. This is the name of the theme
         * which is displayed to the user. This should always be updated
         * if a different theme is loaded, and must be set when the theme is initially loaded.
         */
        std::string currentThemeFriendlyName;

        // ----------------------- Options only relevant if we are starting an internal server -----------------------------

        /**
         * Specifies that when creating the RascBox, whether to create an internal server.
         * The server will be created using the given port, and can be connected to via LOCAL_ADDRESS.
         * This is false by default, but should always be set before using the GlobalProperties, as we
         * may have done something different earlier.
         */
        bool startInternalServer;
        /**
         * This represents the number of AI players to place on the map.
         */
        int aisToPlace;
        /**
         * This stores true if we should start a new game, or false if we should load an existing game
         * from saveFilename.
         */
        bool startNew;
        /**
         * The name of the save to load a save from if we are starting a new game,
         * and the name of the save file to save to later.
         * The full filepath can be obtained from this using StaticConfig::getFullSaveFilepath().
         */
        std::string saveName;

        // ------------------------------------------------------------------------------------------------------------------

        /**
         * Note: Default to an empty player name, so we default to loading the one
         *       from the XML file, (which will use the default player name if the XML file does not exist).
         */
        GlobalProperties(const std::string & address             = DEFAULT_ADDRESS,
                         const std::string & playerName          = "",
                         uint16_t            port                = DEFAULT_PORT,
                         GLfloat             connectionTimeout   = DEFAULT_CONNECTION_TIMEOUT,
                         GLfloat             uiScaleExp          = DEFAULT_UI_SCALE_EXP,
                         bool                startInternalServer = false);

        virtual ~GlobalProperties(void);


        void increaseUIScale(void);
        void decreaseUIScale(void);
        void resetUIScale(void);

        /**
         * This MUST be called ONCE and EXACTLY ONCE BEFORE entering the main menu or the game.
         * This does the following (slow/expensive) operations:
         *  > Creates the ThreadQueue, (so starts threads), and starts the main menu background loading.
         *  > Creates the RascConfig instance, (so reads the xml config file).
         *  > Loads and creates the default theme, (so dynamically loads a library).
         */
        void titleRoomInitialisation(void);

        /**
         * This must be called ONCE and EXACTLY ONCE immediately AFTER creating the main menu.
         * This adds background jobs to the thread queue, for the purposes of loading global
         * textures etc. These are background jobs which must be run AFTER the background jobs
         * started by the creation of the main menu room.
         */
        void mainMenuRoomInitialisation(void);
    };

}

#endif

/*
 * RascBox.h
 *
 *  Created on: 19 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_RASCBOX_H_
#define BASEGAME_CLIENT_RASCBOX_H_

#include <GL/gl.h>
#include <string>
#include "../common/update/TopLevelRascUpdatable.h"
#include "../common/config/ReleaseMode.h"
#include "../common/util/CommonBox.h"

namespace rascUI {
    class Theme;
}

namespace wool {
    class WindowBase;
    class Texture;
    class ThreadTexture;
}

namespace rasc {

    class RascClientNetIntf;
    class ClientGameMap;
    class MapModeController;
    class CameraProperties;
    class MapModeController;
    class GameUI;
    class UnitPlacementController;
    class CameraProperties;
    class GlobalProperties;
    class PlayerDataSystem;
    class ServerBox;
    class Province;
    class UnifiedStatSystem;
    class AreasStat;
    class ContinentsStat;
    class ProvincesStat;
    class UnitsDeployedStat;
    class ModularTopbar;
    class ModularTopbarConfigMenu;
    class PropertyDisplayPanel;
    class BottomLeftUI;
    class ProvinceMenu;

    /**
     * The purpose of this class is simply to store, for public access, all components of the rasc client.
     * This class allows the client-side portion of the game to be tidied up.
     */
    class RascBox : public TopLevelRascUpdatable {
    private:

        // ------------------------------- Game initialiser inner class ----------------------------------

        /**
         * This class launched a barrier job, which implicitly won't start until the textures have finished loading.
         * This is used to create the GameMap, (other things might be done in the future).
         * Importantly, WE MUST NOT create the GameMap until AFTER THE TEXTURES FINISH LOADING, because it relies
         * on the existance of the provinces texture.
         *
         * THIS MUST BE CREATED AFTER THE TEXTURES START LOADING.
         */
        class GameInitialiser {
        private:
            RascBox * box;
            void * token;
            uint64_t jobId;

            // Don't allow copying, we hold raw resources.
            GameInitialiser(const GameInitialiser & other);
            GameInitialiser & operator = (const GameInitialiser & other);
        public:
            GameInitialiser(RascBox * box);
            virtual ~GameInitialiser(void);
            inline uint64_t getJobId(void) const {
                return jobId;
            }
        };

        // -----------------------------------------------------------------------------------------------

        // Disable copying: We hold raw pointer resources.
        RascBox(const RascBox & other);
        RascBox & operator = (const RascBox & other);
    public:
        // ---------------- Game stuff stuff -------------

        /**
         * This is used when starting an internal server, for singleplayer mode.
         * This should be deleted after our internal network interface.
         */
        ServerBox * serverBox;

        /**
         * This controls the placement of units, and is created by default, before anything else.
         */
        UnitPlacementController * unitPlacementController;

        /**
         * The client network interface. This is created by default.
         */
        RascClientNetIntf * netIntf;

        /**
         * The camera properties. This is created by default.
         */
        CameraProperties * cameraProperties;

        /**
         * The game map. This is created by a separate thread, (by GameInitialiser), during the loading
         * room, AFTER THE TEXTURES HAVE LOADED.
         * Note that this is the client implementation of the GameMap: ClientGameMap.
         */
        ClientGameMap * map;

        /**
         * The map mode controller. This is created when we enter the loading room.
         * This should only be accessed/modified from the GLUT/UI thread.
         */
        MapModeController * mapModeController;

        /**
         * The wool WindowBase currently in use.
         */
        wool::WindowBase * window;

        /**
         * The global properties shared between the game client and the main menu.
         */
        GlobalProperties * globalProperties;

        /**
         * The map mode textures, there are MapModes::MODES_COUNT textures here.
         * These are created and loaded when we enter the loading room.
         */
        wool::ThreadTexture * mapModeTextures;

        /**
         * The provinces texture, this is used by the game map.
         * This is created and loaded when we enter the loading room.
         */
        wool::ThreadTexture * provincesTexture;

        /**
         * This is created when we enter the loading room, and destroyed when we enter the lobby room.
         * THIS MUST BE CREATED AFTER THE TEXTURES START LOADING.
         */
        GameInitialiser * gameInitialiser;

        /**
         * This stores the games UI system, which is created by default.
         * NOTE: This MUST be created:
         *  > AFTER the network interface
         *  > AFTER the player data system
         */
        GameUI * ui;

        /**
         * The CommonBox. This stores classes common to both the server
         * and the client, like stats, map and player data system.
         */
        CommonBox common;

    public:
        RascBox(GlobalProperties * globalProperties);
        virtual ~RascBox(void);

        /**
         * This should be called when entering the loading room.
         */
        void enterLoadingRoom(wool::WindowBase * window);

        /**
         * This should be called when entering the lobby room.
         */
        void enterLobbyRoom(void);

        /**
         * This should be called when entering the game room.
         */
        void enterGameRoom(void);

    // =============================================== ACCESSOR METHODS ===============================================


        // For the UI system
        void drawUI(GLfloat elapsed);
        bool onUIMouseEvent(int button, int state, GLfloat viewX, GLfloat viewY);
        bool onUIMouseMove(GLfloat viewX, GLfloat viewY);
        bool onUIKeyPress(int key, bool special);
        bool onUIKeyRelease(int key, bool special);
        void uiBillMenuShowPlayer(uint64_t playerId);
        void uiOpenOverlayMenu(unsigned int menuId, bool autoOpen, bool forceOpen = false);
        void uiShowProvinceMenu(rasc::Province * province);
        void uiHideProvinceMenu(void);
        bool uiIsProvinceMenuOpen(void);
        bool uiIsBuildMenuOpen(void);
        bool uiLobbyProvinceWarnIfNeeded(const uint32_t provinceId);
        static GLfloat getModularTopbarHeight(rascUI::Theme * theme);
        static GLfloat getBottomLeftUIHeight(rascUI::Theme * theme);
        static GLfloat getProvinceMenuTotalWidth(rascUI::Theme * theme);
        static GLfloat getProvinceMenuBottomBorder(rascUI::Theme * theme);
        static GLfloat getClientSidePlayerListWidth(rascUI::Theme * theme);
        void uiBuildMenuOnLeftClickProvince(Province * province);
        void uiBuildMenuOnRightClickProvince(Province * province);
        void uiSetModularTopbarButtonSelected(void);
        void uiPlayerColourBarNotifyRecalculate(void);
        GLfloat uiGetModularTopbarWidth(void);
        void uiKeyboardSelectBillButton(void);
        void uiKeyboardSelectMenuButton(void);
        // UI accessors.
        ModularTopbar & uiGetModularTopbar(void);
        ModularTopbarConfigMenu & uiGetModularTopbarConfigMenu(void);
        PropertyDisplayPanel & uiGetPropertyDisplayPanel(void);
        BottomLeftUI & uiGetBottomLeftUI(void);
        ProvinceMenu & uiGetProvinceMenu(void);

        // These methods use the army select controller to split/merge armies, then send the
        // resulting message using the network interface.
        void runSplitArmies(void);
        void runMergeArmies(void);
        void runAbsorbArmiesIntoGarrisons(void);


        wool::Texture & getCurrentMapModeTexture(void);

        /**
         * Returns true if the initialiser job has finished. This should be used by the loading room to know
         * when stuff has finished loading.
         *
         * NOTE: This must only be called AFTER enterLoadingRoom and BEFORE enterLobbyRoom().
         */
        bool hasInitialiserJobFinished(void);

        /**
         * Returns the job id of the game initialiser job. This should be used by the loading UI, so it can
         * show the "initialising game" job.
         *
         * NOTE: This must only be called AFTER enterLoadingRoom and BEFORE enterLobbyRoom().
         */
        inline uint64_t getInitialiserJobId(void) const {
            return gameInitialiser->getJobId();
        }
    };
}

#endif



/*
 * SingleplayerMenu.h
 *
 *  Created on: 22 Mar 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_MAINMENU_TABS_SINGLEPLAYERMENU_H_
#define BASEGAME_CLIENT_MAINMENU_TABS_SINGLEPLAYERMENU_H_

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/textEntry/BasicTextEntryBox.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/base/Container.h>

namespace rascUI {
    class Theme;
}

namespace tinyxml2 {
    class XMLDocument;
    class XMLElement;
}

namespace rasc {

    class MainMenuRoom;

    class SingleplayerMenu : public rascUI::Container {
    private:
        MainMenuRoom * room;
        void * token;

        // ----------------- Top section ------------------

        rascUI::FrontPanel topFrontPanel;
        rascUI::Label playerNameLabel;
        rascUI::BasicTextEntryBox playerNameBox;
        rascUI::Label portLabel;
        rascUI::BasicTextEntryBox portBox;

        // ----------------- Left panel (new game) --------

        rascUI::Container leftContainer;
        rascUI::FrontPanel leftTopFrontPanel;
        rascUI::BackPanel leftTopBackPanel;
        rascUI::Label leftTopLabel;
        rascUI::Label gameNameLabel;
        rascUI::BasicTextEntryBox gameNameBox;
        rascUI::Label aiPlayersLabel;
        rascUI::BasicTextEntryBox aiPlayersBox;
        rascUI::ScrollPane leftScrollPane;
        rascUI::Label leftBottomLabel;
        rascUI::ToggleButtonSeries mapSelectSeries;
        std::vector<rascUI::ToggleButton> mapSelectToggleButtons;
        rascUI::Button leftLaunchButton;

        // ----------------- Right panel (load game) ------

        rascUI::Container rightContainer;
        rascUI::FrontPanel rightTopFrontPanel;
        rascUI::BackPanel rightTopBackPanel;
        rascUI::Label rightTopLabel;
        rascUI::ScrollPane rightScrollPane;
        rascUI::Label rightBottomLabel;
        rascUI::ToggleButtonSeries savefileSeries;
        std::vector<rascUI::ToggleButton> savefileToggleButtons;
        rascUI::Button rightLaunchButton;
        rascUI::Button rightDeleteSaveButton;

        // ------------------ Bottom panel ----------------

        rascUI::BackPanel bottomBackPanel;
        rascUI::FrontPanel bottomFrontPanel;

    public:
        SingleplayerMenu(const rascUI::Location & location, rascUI::Theme * theme, MainMenuRoom * room);
        virtual ~SingleplayerMenu(void);


    private:
        void setButtonActivity(void);

        void loadFromXML(void);
        void saveToXML(void);

        // The initialisation function for xml config.
        static void initConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem);
    };

}

#endif

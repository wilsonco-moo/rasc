/*
 * MultiplayerMenu.cpp
 *
 *  Created on: 1 Jan 2019
 *      Author: wilson
 */

#include "MultiplayerMenu.h"

#include <rascUI/components/abstract/TextObject.h>
#include <rascUI/base/TopLevelContainer.h>
#include <sockets/plus/util/MutexPtr.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/util/Location.h>
#include <rascUI/base/Component.h>
#include <rascUI/base/Theme.h>
#include <tinyxml2/tinyxml2.h>
#include <cstdlib>

#include "../../../common/config/RascConfig.h"
#include "../../../common/util/XMLUtil.h"
#include "../../GlobalProperties.h"
#include "../MainMenuRoom.h"

// Define macros for the theming.
#define UI_BORDER        theme->uiBorder
#define UI_BWIDTH        theme->normalComponentWidth
#define UI_BHEIGHT       theme->normalComponentHeight

#define SCALED_LOC(L_BORD, SEP, R_BORD, ID, COUNT, Y, HEIGHT, Y_WEIGHT, Y_HEIGHT_WEIGHT) \
    (L_BORD) - (((GLfloat)(ID))/((GLfloat)(COUNT))) * ((L_BORD) + (R_BORD) - (SEP)),     \
    (Y),                                                                                 \
    -((L_BORD) + (R_BORD) + (SEP) * ((COUNT) - 1)) / ((GLfloat)(COUNT)),                 \
    (HEIGHT),                                                                            \
    ((GLfloat)(ID))/((GLfloat)(COUNT)),                                                  \
    (Y_WEIGHT),                                                                          \
    1.0f/((GLfloat)(COUNT)),                                                             \
    (Y_HEIGHT_WEIGHT)


// The base xml tag for our configuration.
#define BASE_CONFIG_NAME "multiplayerMenu"
#define GET_BASE simplenetwork::MutexPtr<tinyxml2::XMLDocument> doc = room->globalProperties->rascConfig->getDocument();  \
                 tinyxml2::XMLElement * base = RascConfig::getOrInitBase(&*doc, BASE_CONFIG_NAME, &MultiplayerMenu::initConfig);



// The width of the left column, the x position of the right column.
#define LCOL_W              192
#define RCOL_X              (LCOL_W + UI_BORDER*3)

// The base y position of the text entry stuff
#define COL_BASE_Y          0

// The number of rows, and their height
#define ROW_COUNT           5
#define ROW_HEIGHT          (UI_BHEIGHT + UI_BORDER)

// The y position of each row
#define ROW_Y(id)           (COL_BASE_Y + (id) * ROW_HEIGHT + UI_BORDER*2)

// The locations of stuff in either column
#define LCOL_LOC(id)        UI_BORDER*2, ROW_Y(id), LCOL_W, UI_BHEIGHT
#define RCOL_LOC(id)        RCOL_X, ROW_Y(id), -RCOL_X-UI_BORDER*2, UI_BHEIGHT, 0, 0, 1, 0

// ---------- The buttons --------------

// The row the buttons are on
#define BUTTON_ROW (ROW_COUNT-1)

// The number of buttons there are
#define BUTTON_COUNT 4

// The location of each button
#define BUTTON_LOC(id)   SCALED_LOC(UI_BORDER*2, UI_BORDER, UI_BORDER*2, id, BUTTON_COUNT, ROW_Y(BUTTON_ROW), UI_BHEIGHT, 0, 0)

// -------------------------------------

// Top panel
#define TOP_FRONT_PANEL_HEIGHT   (ROW_COUNT*ROW_HEIGHT+UI_BORDER)
#define TOP_BACK_PANEL_HEIGHT    (ROW_COUNT*ROW_HEIGHT+UI_BORDER*3)
#define TOP_FRONT_PANEL_LOC      UI_BORDER, COL_BASE_Y + UI_BORDER, -2*UI_BORDER, TOP_FRONT_PANEL_HEIGHT, 0, 0, 1, 0
#define TOP_BACK_PANEL_LOC       0, COL_BASE_Y, 0, TOP_BACK_PANEL_HEIGHT, 0, 0, 1, 0

// Scroll pane
#define SCROLL_PANE_LOC          0, TOP_BACK_PANEL_HEIGHT+COL_BASE_Y, -UI_BORDER, -TOP_BACK_PANEL_HEIGHT-COL_BASE_Y-BOTTOM_PANEL_HEIGHT, 0, 0, 1, 1

// Bottom panel
#define BOTTOM_PANEL_HEIGHT      (UI_BORDER*2+UI_BHEIGHT)
#define BOTTOM_BACK_PANEL_LOC    0, -BOTTOM_PANEL_HEIGHT, 0, BOTTOM_PANEL_HEIGHT, 0, 1, 1, 0
#define BOTTOM_FRONT_PANEL_LOC   UI_BORDER, -UI_BORDER-UI_BHEIGHT, -UI_BORDER*2, UI_BHEIGHT, 0, 1, 1, 0

// The location used by server buttons within their scroll segment.
#define SERVER_BUTTON_LOC        UI_BORDER, 0, -UI_BORDER*2, -UI_BORDER, 0, 0, 1, 1



namespace rasc {


    // ------------------ Server button -------------------
        MultiplayerMenu::ServerButton::ServerButton(rascUI::ToggleButtonSeries * series, rascUI::Theme * theme, std::string serverName, std::string address, uint16_t port) :
            rascUI::ToggleButton(series, rascUI::Location(SERVER_BUTTON_LOC), serverName),
            address(address),
            port(port) {
        }

        MultiplayerMenu::ServerButton::~ServerButton(void) {
        }
    // ----------------------------------------------------



    MultiplayerMenu::MultiplayerMenu(const rascUI::Location & location, rascUI::Theme * theme, MainMenuRoom * room) :
        rascUI::Container(location),

        room(room),

        serverButtons(),
        currentServerButton(NULL),

        topBackPanel(rascUI::Location(TOP_BACK_PANEL_LOC)),
        topFrontPanel(rascUI::Location(TOP_FRONT_PANEL_LOC)),

        playerNameLabel(    rascUI::Location(LCOL_LOC(0)), "Player name:"     ),
        serverListNameLabel(rascUI::Location(LCOL_LOC(1)), "Server list name:"),
        addressLabel(       rascUI::Location(LCOL_LOC(2)), "Server address:"  ),
        portLabel(          rascUI::Location(LCOL_LOC(3)), "Server port:"     ),
        playerNameBox(      rascUI::Location(RCOL_LOC(0)), room->globalProperties->playerName, [this](const std::string & str) {
            // When the text in the player name box is modified, ensure the xml file is updated.
            saveServerButtonsToXML();
        }),
        serverListNameBox(  rascUI::Location(RCOL_LOC(1))),
        addressBox(         rascUI::Location(RCOL_LOC(2)), room->globalProperties->address),
        portBox(            rascUI::Location(RCOL_LOC(3)), std::to_string(room->globalProperties->port)),

        saveAsButton(rascUI::Location(BUTTON_LOC(0)), "Save",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) saveServerButton();
            }
        ),
        editButton(  rascUI::Location(BUTTON_LOC(1)), "Edit",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) editServerButton();
            }
        ),
        deleteButton(rascUI::Location(BUTTON_LOC(2)), "Delete",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) deleteServerButton();
            }
        ),
        joinButton(  rascUI::Location(BUTTON_LOC(3)), "Join",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb() && addressBox.getText().size() > 0 && playerNameBox.getText().size() > 0) {
                    this->room->globalProperties->address = addressBox.getText();
                    this->room->globalProperties->port = (uint16_t)strtoul(portBox.getText().c_str(), NULL, 0);
                    this->room->globalProperties->playerName = playerNameBox.getText();
                    this->room->globalProperties->startInternalServer = false; // We don't want to start an internal server, as we are connecting to a server.
                    this->room->connectionPopup.setSelectWhenHide(&joinButton);
                    this->room->connectionPopup.show();
                    /*
                    LoadingRoom * loadingRoom = LoadingRoom::initialiseGameClient(this->room->connectionInfo);
                    if (loadingRoom != NULL) {
                        this->room->getWindowBase()->changeRoom(loadingRoom);
                    }
                    */
                }
            }
        ),

        serverButtonSeries(true, [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
            if (toggleButtonId == rascUI::ToggleButtonSeries::NO_TOGGLE_BUTTON) {
                onUnsetServerButton();
            }
        }),
        scrollPane(theme, rascUI::Location(SCROLL_PANE_LOC), false, true, false, false),

        bottomBackPanel(rascUI::Location(BOTTOM_BACK_PANEL_LOC)),
        bottomFrontPanel(rascUI::Location(BOTTOM_FRONT_PANEL_LOC)) {

        loadServerButtonsFromXML(theme);

        add(&scrollPane);

        add(&topBackPanel);
        add(&topFrontPanel);

        add(&playerNameLabel);
        add(&playerNameBox);

        add(&serverListNameLabel);
        add(&serverListNameBox);

        add(&addressLabel);
        add(&addressBox);

        add(&portLabel);
        add(&portBox);

        add(&saveAsButton);
        add(&editButton);
        add(&deleteButton);
        add(&joinButton);

        add(&bottomBackPanel);
        add(&bottomFrontPanel);

        deleteButton.setActive(false);
        editButton.setActive(false);
    }

    MultiplayerMenu::~MultiplayerMenu(void) {
        for (ServerButton * button : serverButtons) {
            delete button;
        }
    }

    void MultiplayerMenu::saveServerButton(void) {
        if (serverListNameBox.getText().size() > 0 && addressBox.getText().size() > 0) {
            getTopLevel()->afterEvent->addFunction([this](void) {
                saveServerButton(getTopLevel()->theme, serverListNameBox.getText(), addressBox.getText(), (uint16_t)strtoul(portBox.getText().c_str(), NULL, 0));
                saveServerButtonsToXML();
            });
        }
    }

    void MultiplayerMenu::saveServerButton(rascUI::Theme * theme, const std::string & serverName, const std::string & address, uint16_t port) {
        ServerButton * serverButton = new ServerButton(&serverButtonSeries, theme, serverName, address, port);
        serverButton->onSelectFunc = [this, serverButton](GLfloat viewX, GLfloat viewY, int button) {
            onSetServerButton(serverButton);
        };
        serverButtons.push_back(serverButton);
        serverButton->listIter = --serverButtons.end();
        scrollPane.contents.add(serverButton);
    }
    void MultiplayerMenu::editServerButton(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            if (currentServerButton != NULL) {
                currentServerButton->setText(serverListNameBox.getText());
                currentServerButton->setAddress(addressBox.getText());
                currentServerButton->setPort((uint16_t)strtoul(portBox.getText().c_str(), NULL, 0));
                saveServerButtonsToXML();
            }
        });
    }
    void MultiplayerMenu::onSetServerButton(ServerButton * currentServerButton) {
        getTopLevel()->afterEvent->addFunction([this, currentServerButton](void) {
            this->currentServerButton = currentServerButton;
            serverListNameBox.setText(currentServerButton->getText());
            addressBox.setText(currentServerButton->getAddress());
            portBox.setText(std::to_string(currentServerButton->getPort()));
            deleteButton.setActive(true);
            editButton.setActive(true);
        });
    }
    void MultiplayerMenu::onUnsetServerButton(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            currentServerButton = NULL;
            deleteButton.setActive(false);
            editButton.setActive(false);
        });
    }
    void MultiplayerMenu::deleteServerButton(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            if (currentServerButton != NULL) {
                scrollPane.contents.remove(currentServerButton);
                serverButtons.erase(currentServerButton->listIter);
                delete currentServerButton;
                currentServerButton = NULL;
                deleteButton.setActive(false);
                editButton.setActive(false);
                saveServerButtonsToXML();
            }
        });
    }

    void MultiplayerMenu::loadServerButtonsFromXML(rascUI::Theme * theme) {
        GET_BASE

        tinyxml2::XMLElement * name, * servers;

        // Only read the player name if the user has not provided one, i.e: It is non-empty.
        if (playerNameBox.getText().empty()) {
            name = readElement(base, "playerName");
            playerNameBox.setText(readAttribute(name, "name"));
        }

        // Loop through each server element.
        servers = readElement(base, "servers");
        loopElements(buttonElement, servers, "server") {
            // Get the properties and save them as new server buttons.
            const char * serverName = readAttribute(buttonElement, "name"),
                          * address = readAttribute(buttonElement, "address");
            uint16_t           port = buttonElement->IntAttribute("port");
            saveServerButton(theme, serverName, address, port);
        }
    }

    void MultiplayerMenu::saveServerButtonsToXML(void) {
        {
            GET_BASE
            base->DeleteChildren();

            tinyxml2::XMLElement * name, * servers;

            name = doc->NewElement("playerName");
            name->SetAttribute("name", playerNameBox.getText().c_str());
            base->InsertEndChild(name);

            servers = doc->NewElement("servers");
            for (ServerButton * button : serverButtons) {
                tinyxml2::XMLElement * elem = doc->NewElement("server");
                elem->SetAttribute("name", button->getText().c_str());
                elem->SetAttribute("address", button->getAddress().c_str());
                elem->SetAttribute("port", (int)button->getPort());
                servers->InsertEndChild(elem);
            }
            base->InsertEndChild(servers);
        }
        room->globalProperties->rascConfig->saveLater();
    }

    void MultiplayerMenu::initConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
        tinyxml2::XMLElement * server, * servers, * name;

        name = doc->NewElement("playerName");
        name->SetAttribute("name", GlobalProperties::DEFAULT_PLAYER_NAME.c_str());
        elem->InsertEndChild(name);

        servers = doc->NewElement("servers");
            server = doc->NewElement("server");
            server->SetAttribute("name", "Localhost");
            server->SetAttribute("address", "127.0.0.1");
            server->SetAttribute("port", (int)64000);
            servers->InsertEndChild(server);

            server = doc->NewElement("server");
            server->SetAttribute("name", "Wilsonco");
            server->SetAttribute("address", "wilsonco.mooo.com");
            server->SetAttribute("port", (int)64000);
            servers->InsertEndChild(server);
        elem->InsertEndChild(servers);
    }
}

/*
 * OptionsMenu.cpp
 *
 *  Created on: 22 Mar 2019
 *      Author: wilson
 */

#include "OptionsMenu.h"

#include <iostream>
#include <cstdlib>
#include <string>

#include <rascUItheme/utils/DynamicThemeLoader.h>
#include <wool/window/base/WindowBase.h>
#include <sockets/plus/util/MutexPtr.h>
#include <rascUI/util/FunctionQueue.h>
#include <threading/ThreadQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <tinyxml2/tinyxml2.h>

#include "../../../common/util/numeric/MultiCol.h"
#include "../../../common/playerData/flag/Flag.h"
#include "../../../common/config/StaticConfig.h"
#include "../../../common/config/RascConfig.h"
#include "../../../common/util/XMLUtil.h"
#include "../../../common/util/Misc.h"
#include "../../GlobalProperties.h"
#include "../MainMenuRoom.h"

#define LAYOUT_THEME theme

/**
 * Returns true if any of the layout properties of the two provided themes differ.
 */
#define LAYOUT_CHANGED(theme1, theme2) (                                    \
    (theme1)->normalComponentWidth != (theme2)->normalComponentWidth ||     \
    (theme1)->normalComponentHeight != (theme2)->normalComponentHeight ||   \
    (theme1)->uiBorder != (theme2)->uiBorder                                \
)

namespace rasc {

    // ====================== Theme selection menu ======================

    /**
     * This class stores all the info provided to the background thread for loading
     * a theme.
     */
    class ThemeSelectionMenu::ThemeInfo {
    public:
        std::string newThemeFriendlyName;
        rascUItheme::DynamicThemeLoader newThemeLoader;
        ThemeInfo(void) :
            newThemeFriendlyName(""),
            newThemeLoader() {
        }
        ~ThemeInfo(void) {
        }
    };

    #define THSEL_TOP_BACK_PANEL_LOC                \
        PIN_T(COUNT_OUTBORDER_Y(1) + UI_BORDER,     \
         GEN_FILL                                   \
        )

    #define THSEL_TOP_FRONT_PANEL_LOC               \
        PIN_T(COUNT_OUTBORDER_Y(1),                 \
         GEN_FILL                                   \
        )

    #define THSEL_BUTTON_WIDTH 128

    #define THSEL_BUTTON_LOC                        \
        PIN_T(COUNT_INBORDER_Y(1),                  \
         PIN_R(THSEL_BUTTON_WIDTH,                  \
          SUB_BORDER_X(GEN_BORDER)                  \
        ))

    #define THSEL_LABEL_LOC                         \
        PIN_T(COUNT_INBORDER_Y(1),                  \
         SUB_BORDERMARGIN_R(THSEL_BUTTON_WIDTH,     \
          SUB_BORDER_X(GEN_BORDER)                  \
        ))

    #define THSEL_SCROLLPANE_LOC                    \
        SUB_BORDERMARGIN_T(COUNT_OUTBORDER_Y(1),    \
          GEN_FILL                                  \
        )

    // Both the theme selection menu's floating panel and the dummy floating panel use this size.
    #define FLOATING_PANEL_LOC \
        GEN_CENTRE_FIXED(640, 128)

    ThemeSelectionMenu::ThemeSelectionMenu(const rascUI::Location & location, GlobalProperties * glob, rascUI::TopLevelContainer * topLevel, rascUI::Theme * theme, std::function<void*(rascUI::Theme*)> onChangeThemeLayoutInThread, std::function<void(rascUI::Theme*, void*)> onChangeThemeLayoutFinal, std::function<void(void *)> deleteParam) :
        rascUI::Container(location),
        glob(glob),
        topLevel(topLevel),
        topBackPanel(rascUI::Location(THSEL_TOP_BACK_PANEL_LOC)),
        topFrontPanel(rascUI::Location(THSEL_TOP_FRONT_PANEL_LOC)),
        topLabel(rascUI::Location(THSEL_LABEL_LOC), "Select UI theme:"),
        useThemeButton(rascUI::Location(THSEL_BUTTON_LOC), "Apply theme",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) floatingPanel.show();
            }
        ),
        scrollPane(theme, rascUI::Location(THSEL_SCROLLPANE_LOC), false, true, false, false),
        themeSeries(false,
            [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
                useThemeButton.setActive(toggleButton->getText() != this->glob->currentThemeFriendlyName);
            }
        ),
        themeButtons(),
        token(glob->threadQueue->createToken()),
        floatingPanel(
            glob->threadQueue,
            token,
            theme,
            false,
            rascUI::Location(FLOATING_PANEL_LOC),
            [this](void) { // on show
                floatingPanel.setSelectWhenHide(getAppropriateSelectWhenHide());
                themeInfo = new ThemeInfo();
                themeInfo->newThemeFriendlyName = themeSeries.getSelectedToggleButton()->getText();
                floatingPanel.topLabel.setText("Loading theme...");
                floatingPanel.bottomLabel.setText("");
            },
            [this](void)->bool { // background job
                std::cout << "Loading new theme [" << themeInfo->newThemeFriendlyName << "] ...\n";
                // Note: Ensure we convert the friendly name back to a filename BEFORE giving it to DynamicThemeLoader.
                themeInfo->newThemeLoader = rascUItheme::DynamicThemeLoader(StaticConfig::THEMES_DIRECTORY + StaticConfig::themeFriendlyNameToFilename(themeInfo->newThemeFriendlyName));
                bool success = themeInfo->newThemeLoader.loadedSuccessfully();
                if (success) {
                    std::cout << "Successfully loaded new theme [" << themeInfo->newThemeFriendlyName << "].\n";

                    rascUI::Theme * oldTheme = this->glob->themeLoader.getTheme(),
                                  * newTheme = themeInfo->newThemeLoader.getTheme();

                    // If any layout properties have changed, run the custom method.
                    if (LAYOUT_CHANGED(oldTheme, newTheme)) {
                        std::cout << "Note: Theme layout properties have changed.\n";
                        changeThemeParam = this->onChangeThemeLayoutInThread(newTheme);
                    }

                } else {
                    std::cout << "Failed to load theme [" << themeInfo->newThemeFriendlyName << "].\n";
                }
                return success;
            },
            [](void) { // on wait
            },
            [this](bool success) { // on end
                if (success) {

                    // While we still have access to the old theme, work out whether the layout has changed.
                    rascUI::Theme * newTheme = themeInfo->newThemeLoader.getTheme();
                    bool hasLayoutChanged;
                    {
                        rascUI::Theme * oldTheme = this->glob->themeLoader.getTheme();
                        hasLayoutChanged = LAYOUT_CHANGED(oldTheme, newTheme);
                    }

                    std::cout << "Switching over to new theme, and unloading old theme...\n";
                    // NOTE: Set the top level's theme BEFORE doing the move of the theme loader.
                    // This way we do the actual theme change during the short time (before std::move
                    // assigning the new theme into globalProperties), where both themes are loaded.
                    this->topLevel->theme = newTheme;
                    this->glob->currentThemeFriendlyName = themeInfo->newThemeFriendlyName;
                    this->glob->themeLoader = std::move(themeInfo->newThemeLoader);
                    floatingPanel.topLabel.setText("New theme loaded successfully.");
                    std::cout << "Done.\n";

                    // Now all theme switching is done, run the user's custom method if any
                    // layout has changed. Also set the label text.
                    if (hasLayoutChanged) {
                        this->onChangeThemeLayoutFinal(newTheme, changeThemeParam);
                        changeThemeParam = NULL; // Make sure to do this, since we assume that onChangeThemeLayoutFinal has done something with the parameter.

                        floatingPanel.topLabel.setText("New theme loaded successfully. WARNING: This theme");
                        floatingPanel.bottomLabel.setText("changed layout options, please reload the UI.");
                    } else {
                        floatingPanel.topLabel.setText("New theme loaded successfully.");
                        floatingPanel.bottomLabel.setText("");
                    }

                    // If changing theme was successful, we are now using the same theme as
                    // selected in the toggle buttons, do make the "Apply theme" button inactive.
                    useThemeButton.setActive(false);

                    // Then update the configuration system, and tell it to save later, since we successfully changed the theme.
                    simplenetwork::MutexPtr<tinyxml2::XMLDocument> doc = this->glob->rascConfig->getDocument();
                    tinyxml2::XMLElement * base = RascConfig::getOrInitBase(&*doc, "themes", &ThemeSelectionMenu::initConfig);
                    tinyxml2::XMLElement * currentTheme = readElement(base, "currentTheme");
                    currentTheme->SetAttribute("friendlyName", themeInfo->newThemeFriendlyName.c_str());
                    this->glob->rascConfig->saveLater();

                } else {
                    floatingPanel.topLabel.setText("Failed to load new theme. Will continue using current");
                    floatingPanel.bottomLabel.setText(std::string("theme, with name: [") + this->glob->currentThemeFriendlyName + "].");
                }
                delete themeInfo;
                themeInfo = NULL;
            },
            [](void) { // on hide
            }
        ),
        themeInfo(NULL),
        normalComponentWidth(theme->normalComponentWidth),
        normalComponentHeight(theme->normalComponentHeight),
        uiBorder(theme->uiBorder),
        onChangeThemeLayoutInThread(onChangeThemeLayoutInThread),
        onChangeThemeLayoutFinal(onChangeThemeLayoutFinal),
        deleteParam(deleteParam),
        changeThemeParam(NULL) {

        add(&scrollPane);
        add(&topBackPanel);
        add(&topFrontPanel);
        add(&topLabel);
        add(&useThemeButton);

        // Ensure this cannot be clicked, since by default we select the toggle button showing the theme we are currently using.
        useThemeButton.setActive(false);

        // Add the themes from a separate thread, in case the filesystem is slow enough to block the UI.
        Misc::backgroundScrollpaneAdd(theme, topLevel, token, glob->threadQueue, &scrollPane, &themeButtons, &themeSeries, &StaticConfig::getThemeFriendlyNames, false);

        // Then set selection of the correct button, after loading the list of themes is done.
        // Note that we must not set the useThemeButton to active, since that should only be active if we select
        // a different theme to the one currently in use.
        glob->threadQueue->addBarrier(token, [this, topLevel](void) {
            topLevel->beforeEvent->addFunction([this](void) {
                for (rascUI::ToggleButton & button : themeButtons) {
                    if (button.getText() == this->glob->currentThemeFriendlyName) {
                        themeSeries.setSelectedButton(&button);
                        break;
                    }
                }
            });
        });
    }

    ThemeSelectionMenu::~ThemeSelectionMenu(void) {
        glob->threadQueue->deleteToken(token);
        // Do these just in case we are deleted while loading a theme.
        delete themeInfo;
        deleteParam(changeThemeParam);
    }

    void ThemeSelectionMenu::loadCorrectTheme(GlobalProperties * glob) {

        // First load/initialise the config system, and get the current theme's name.
        simplenetwork::MutexPtr<tinyxml2::XMLDocument> doc = glob->rascConfig->getDocument();
        tinyxml2::XMLElement * base = RascConfig::getOrInitBase(&*doc, "themes", &ThemeSelectionMenu::initConfig);
        tinyxml2::XMLElement * currentTheme = readElement(base, "currentTheme");
        const char * currentThemeFriendlyName = readAttribute(currentTheme, "friendlyName");

        // Try to load this theme specified by the config system. If successful, use it and return.
        {
            // Note: Convert the friendly name back to a filename before giving it to DynamicThemeLoader.
            rascUItheme::DynamicThemeLoader newThemeLoader(StaticConfig::THEMES_DIRECTORY + StaticConfig::themeFriendlyNameToFilename(currentThemeFriendlyName));
            if (newThemeLoader.loadedSuccessfully()) {
                glob->themeLoader = std::move(newThemeLoader);
                glob->currentThemeFriendlyName = currentThemeFriendlyName;
                std::cout << "Successfully loaded theme: [" << currentThemeFriendlyName << "].\n";
                return;
            }
        }

        // If the theme that failed to load was not the default theme, then
        // try loading the default theme. If that succeeds, use it AND update the config since
        // we are now using a different theme.
        if (StaticConfig::DEFAULT_THEME_FRIENDLY_NAME != currentThemeFriendlyName) {
            std::cerr << "WARNING: Failed to load theme [" << currentThemeFriendlyName << "], as described by the config file. Maybe it has been deleted since the previous run? Will try the default theme instead.\n";
            rascUItheme::DynamicThemeLoader newThemeLoader(StaticConfig::THEMES_DIRECTORY + StaticConfig::themeFriendlyNameToFilename(StaticConfig::DEFAULT_THEME_FRIENDLY_NAME));
            if (newThemeLoader.loadedSuccessfully()) {
                glob->themeLoader = std::move(newThemeLoader);
                glob->currentThemeFriendlyName = StaticConfig::DEFAULT_THEME_FRIENDLY_NAME;
                currentTheme->SetAttribute("friendlyName", StaticConfig::DEFAULT_THEME_FRIENDLY_NAME.c_str());
                std::cout << "Loading the default theme [" << StaticConfig::DEFAULT_THEME_FRIENDLY_NAME << "] as a fallback was successful. Will now update the configuration to use this next time.\n";
                glob->rascConfig->saveLater();
                return;
            }
        }

        // If loading the default theme failed as well, then we have a failure so exit.
        std::cerr << "CRITICAL ERROR: Failed to load default theme [" << StaticConfig::DEFAULT_THEME_FRIENDLY_NAME << "].\n";
        exit(EXIT_FAILURE);
    }

    void ThemeSelectionMenu::initConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {
        tinyxml2::XMLElement * currentTheme = doc->NewElement("currentTheme");
            currentTheme->SetAttribute("friendlyName", StaticConfig::DEFAULT_THEME_FRIENDLY_NAME.c_str());
        elem->InsertEndChild(currentTheme);
    }

    // ======================== Shared options menu ===============
    
    #define SHARED_OPTIONS_TITLE_LOC \
        PIN_NORMAL_T(                \
         GEN_FILL                    \
        )

    #define SHARED_OPTIONS_ROW_LOC(index) \
        MOVE_BORDERNORMAL_Y(index,        \
         MOVE_NORMAL_Y(1,                 \
          PIN_NORMAL_T(                   \
           GEN_FILL                       \
        )))

    #define SHARED_OPTIONS_LABEL_LOC(rowIndex, labelWidth) \
        PIN_L(labelWidth,                                  \
         SUB_BORDER_R(                                     \
          SHARED_OPTIONS_ROW_LOC(rowIndex)                 \
        ))
    
    #define SHARED_OPTIONS_BUTTONS_LOC(rowIndex, labelWidth) \
        SUB_MARGIN_L(labelWidth,                             \
         SUB_BORDER_R(                                       \
          SHARED_OPTIONS_ROW_LOC(rowIndex)                   \
        ))
    
    #define SHARED_OPTIONS_BUTTON_LOC(rowIndex, labelWidth, buttonTable, buttonId) \
        DEFINED_BORDERTABLE_X(buttonTable, buttonId,                               \
         SHARED_OPTIONS_BUTTONS_LOC(rowIndex, labelWidth)                          \
        )
    
    DEFINE_TABLE(ColourBlindButtons, 5, 9)
    static_assert(ColourBlindButtons::count == MultiCol::ColourBlindMode::COUNT, "Please update layout table for colour blind buttons!");
    
    #define SHARED_OPTIONS_COLOUR_BLIND_LABEL_LOC SHARED_OPTIONS_LABEL_LOC(0, 180)
    #define SHARED_OPTIONS_COLOUR_BLIND_BUTTON_LOC(id) SHARED_OPTIONS_BUTTON_LOC(0, 180, ColourBlindButtons, id)
    
    DEFINE_TABLE(HumanFlagButtons, 7, 6)
    static_assert(HumanFlagButtons::count == Flag::CreateMode::COUNT, "Please update layout table!");
    
    #define SHARED_OPTIONS_HUMAN_FLAG_LABEL_LOC SHARED_OPTIONS_LABEL_LOC(1, 130)
    #define SHARED_OPTIONS_HUMAN_FLAG_BUTTON_LOC(id) SHARED_OPTIONS_BUTTON_LOC(1, 130, HumanFlagButtons, id)

    DEFINE_TABLE(AiFlagButtons, 7, 6)
    static_assert(AiFlagButtons::count == Flag::CreateMode::COUNT, "Please update layout table!");
    
    #define SHARED_OPTIONS_AI_FLAG_LABEL_LOC SHARED_OPTIONS_LABEL_LOC(2, 130)
    #define SHARED_OPTIONS_AI_FLAG_BUTTON_LOC(id) SHARED_OPTIONS_BUTTON_LOC(2, 130, AiFlagButtons, id)
    
    SharedOptionsMenu::SharedOptionsMenu(const rascUI::Location & location, GlobalProperties & globalProperties, rascUI::Theme * theme) :
        rascUI::Container(location),
        globalProperties(globalProperties),
        frontPanel(),
        titleLabel(rascUI::Location(SHARED_OPTIONS_TITLE_LOC), "Shared (server) options"),
        colourBlindLabel(rascUI::Location(SHARED_OPTIONS_COLOUR_BLIND_LABEL_LOC), "Colour blindness:"),
        colourBlindSeries(false,
            [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
                // Update colour-blindness mode in RascConfig when user changes colour-blindness mode.
                updateAndSaveSharedOption("colourBlindness", "mode", MultiCol::colourBlindModeToFriendlyName(toggleButtonId));
            }
        ),
        colourBlindButtons(),
        humanFlagLabel(rascUI::Location(SHARED_OPTIONS_HUMAN_FLAG_LABEL_LOC), "Human flags:"),
        humanFlagSeries(false,
            [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
                updateAndSaveSharedOption("humanFlag", "createMode", Flag::createModeToFriendlyName(toggleButtonId));
            }
        ),
        humanFlagButtons(),
        aiFlagLabel(rascUI::Location(SHARED_OPTIONS_AI_FLAG_LABEL_LOC), "AI flags:"),
        aiFlagSeries(false,
            [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
            updateAndSaveSharedOption("aiFlag", "createMode", Flag::createModeToFriendlyName(toggleButtonId));
            }
        ),
        aiFlagButtons() {
        
        add(&frontPanel);
        add(&titleLabel);
        
        // Add colour blindness label and buttons.
        static_assert(colourBlindButtons.max_size() == MultiCol::ColourBlindMode::COUNT, "Emplace array size must match!");
        add(&colourBlindLabel);
        for (unsigned int colourBlindMode = 0; colourBlindMode < MultiCol::ColourBlindMode::COUNT; colourBlindMode++) {
            colourBlindButtons.emplace(&colourBlindSeries,
                rascUI::Location(SHARED_OPTIONS_COLOUR_BLIND_BUTTON_LOC(colourBlindMode)),
                MultiCol::colourBlindModeToFriendlyName(colourBlindMode));
            add(&colourBlindButtons.back());
        }
        
        // Add human flag label and buttons.
        static_assert(humanFlagButtons.max_size() == Flag::CreateMode::COUNT, "Emplace array size must match!");
        add(&humanFlagLabel);
        for (unsigned int flagCreateMode = 0; flagCreateMode < Flag::CreateMode::COUNT; flagCreateMode++) {
            humanFlagButtons.emplace(&humanFlagSeries,
                rascUI::Location(SHARED_OPTIONS_HUMAN_FLAG_BUTTON_LOC(flagCreateMode)),
                Flag::createModeToFriendlyName(flagCreateMode));
            add(&humanFlagButtons.back());
        }
        
        // Add AI flag label and buttons.
        static_assert(aiFlagButtons.max_size() == Flag::CreateMode::COUNT, "Emplace array size must match!");
        add(&aiFlagLabel);
        for (unsigned int flagCreateMode = 0; flagCreateMode < Flag::CreateMode::COUNT; flagCreateMode++) {
            aiFlagButtons.emplace(&aiFlagSeries,
                rascUI::Location(SHARED_OPTIONS_AI_FLAG_BUTTON_LOC(flagCreateMode)),
                Flag::createModeToFriendlyName(flagCreateMode));
            add(&aiFlagButtons.back());
        }
        
        // Get shared options config from RascConfig.
        simplenetwork::MutexPtr<tinyxml2::XMLDocument> doc = globalProperties.rascConfig->getDocument();
        tinyxml2::XMLElement * sharedOptions = RascConfig::getOrInitBase(&*doc, "sharedOptions", &Misc::initSharedOptionsConfig);
        
        // Set initial data for each option.
        tinyxml2::XMLElement * colourBlindness = readElement(sharedOptions, "colourBlindness");
        colourBlindSeries.setSelectedId(MultiCol::friendlyNameToColourBlindMode(readAttribute(colourBlindness, "mode")));
        
        tinyxml2::XMLElement * humanFlag = readElement(sharedOptions, "humanFlag");
        humanFlagSeries.setSelectedId(Flag::friendlyNameToCreateMode(readAttribute(humanFlag, "createMode")));
        
        tinyxml2::XMLElement * aiFlag = readElement(sharedOptions, "aiFlag");
        aiFlagSeries.setSelectedId(Flag::friendlyNameToCreateMode(readAttribute(aiFlag, "createMode")));
    }
        
    void SharedOptionsMenu::updateAndSaveSharedOption(const char * tag, const char * attribute, const char * value) const {
        simplenetwork::MutexPtr<tinyxml2::XMLDocument> doc = globalProperties.rascConfig->getDocument();
        tinyxml2::XMLElement * sharedOptions = RascConfig::getOrInitBase(&*doc, "sharedOptions", &Misc::initSharedOptionsConfig);
        tinyxml2::XMLElement * colourBlindness = readElement(sharedOptions, tag);
        colourBlindness->SetAttribute(attribute, value);
        globalProperties.rascConfig->saveLater();
    }
    
    // ======================== Rest of options menu ==============

    #define OPTIONS_TOP_LABEL_LOC                       \
        PIN_T(COUNT_INBORDER_Y(1),                      \
         GEN_BORDER                                     \
        )
    #define OPTIONS_MENU_CONTENT_LOC                    \
        SUB_BORDER_X(                                   \
         SUB_BORDERMARGIN_T(COUNT_ONEBORDER_Y(1),       \
          SUB_BORDERMARGIN_B(COUNT_ONEBORDER_Y(1),      \
           GEN_FILL                                     \
        )))
    #define OPTIONS_THEME_SELECT_LOC                    \
        BORDERTABLE_X(0, 2,                             \
         OPTIONS_MENU_CONTENT_LOC                       \
        )
    #define OPTIONS_SHARED_OPTIONS_LOC                  \
        BORDERTABLE_X(1, 2,                             \
         OPTIONS_MENU_CONTENT_LOC                       \
        )
    #define OPTIONS_BOTTOM_PANEL_LOC                    \
        PIN_B(COUNT_OUTBORDER_Y(1),                     \
         GEN_FILL                                       \
        )

    OptionsMenu::OptionsMenu(const rascUI::Location & location, rascUI::Theme * theme, MainMenuRoom * room) :
        rascUI::Container(location),
        room(room),
        topFrontPanel(rascUI::Location(OPTIONS_TOP_LABEL_LOC)),
        label(rascUI::Location(OPTIONS_TOP_LABEL_LOC), "Options menu"),
        themeSelectionMenu(rascUI::Location(OPTIONS_THEME_SELECT_LOC), room->globalProperties, room->getMainMenuTopLevel(), theme,
            [this](rascUI::Theme * newTheme) -> void * {
                std::cout << "Re-creating entire RASC main menu room, since UI needs to be reloaded...\n";
                return new MainMenuRoom(this->room->globalProperties, newTheme, true);
            },
            [this](rascUI::Theme * newTheme, void * param) {
                std::cout << "Switching rooms to newly created main menu, using the new theme.\n";
                this->room->getWindowBase()->changeRoom((MainMenuRoom *)param);
            },
            [](void * param) {
                delete (MainMenuRoom *)param;
            }
        ),
        sharedOptionsMenu(rascUI::Location(OPTIONS_SHARED_OPTIONS_LOC), *room->globalProperties, theme),
        dummyFloatingPanel(theme, rascUI::Location(FLOATING_PANEL_LOC),
            [this](void) { // on show
                // Ensure the appropriate button is keyboard selected when the dummy floating panel is hidden.
                dummyFloatingPanel.setSelectWhenHide(themeSelectionMenu.getAppropriateSelectWhenHide());
                // Also ensure that the dummy floating panel's close button gets keyboard selected when it is shown.
                getTopLevel()->setKeyboardSelected(&dummyFloatingPanel.closeButton);
            }
        ),
        bottomBackPanel(rascUI::Location(OPTIONS_BOTTOM_PANEL_LOC)),
        bottomFrontPanel(rascUI::Location(SUB_BORDER(OPTIONS_BOTTOM_PANEL_LOC))) {

        add(&topFrontPanel);
        add(&label);
        add(&themeSelectionMenu);
        add(&sharedOptionsMenu);
        add(&bottomBackPanel);
        add(&bottomFrontPanel);

        dummyFloatingPanel.topLabel.setText("New theme loaded successfully. UI system reloaded");
        dummyFloatingPanel.bottomLabel.setText("since this theme uses different layout values.");
    }

    OptionsMenu::~OptionsMenu(void) {
    }

}

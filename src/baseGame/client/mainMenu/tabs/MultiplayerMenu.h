/*
 * MultiplayerMenu.h
 *
 *  Created on: 1 Jan 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_MAINMENU_TABS_MULTIPLAYERMENU_H_
#define BASEGAME_CLIENT_MAINMENU_TABS_MULTIPLAYERMENU_H_

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/textEntry/BasicTextEntryBox.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/base/Container.h>
#include <functional>
#include <GL/gl.h>
#include <string>
#include <list>

namespace tinyxml2 {
    class XMLDocument;
    class XMLElement;
}

namespace rascUI {
    class Theme;
}

namespace rasc {

    class MainMenuRoom;


    class MultiplayerMenu : public rascUI::Container {
    private:
        // Don't allow copying
        MultiplayerMenu(const MultiplayerMenu & other);
        MultiplayerMenu & operator = (const MultiplayerMenu & other);


        // ------------------ Server button -------------------
        class ServerButton : public rascUI::ToggleButton {
        private:
            std::string address;
            uint16_t port;
        public:
            std::list<ServerButton *>::iterator listIter;
            ServerButton(rascUI::ToggleButtonSeries * series,
                         rascUI::Theme * theme,
                         std::string serverName,
                         std::string address,
                         uint16_t port);
            virtual ~ServerButton(void);

            const std::string & getAddress(void) const { return address; }
            void setAddress(const std::string & address) { this->address = address; }
            uint16_t getPort(void) const { return port; }
            void setPort(uint16_t port) { this->port = port; }
        };
        // ----------------------------------------------------

        MainMenuRoom * room;

        std::list<ServerButton *> serverButtons;
        ServerButton * currentServerButton;

        rascUI::BackPanel topBackPanel;
        rascUI::FrontPanel topFrontPanel;

        rascUI::Label playerNameLabel, serverListNameLabel, addressLabel, portLabel;
        rascUI::BasicTextEntryBox playerNameBox, serverListNameBox, addressBox, portBox;
        rascUI::Button saveAsButton, editButton, deleteButton, joinButton;

        rascUI::ToggleButtonSeries serverButtonSeries;
        rascUI::ScrollPane scrollPane;

        rascUI::BackPanel bottomBackPanel;
        rascUI::FrontPanel bottomFrontPanel;
    public:
        MultiplayerMenu(const rascUI::Location & location, rascUI::Theme * theme, MainMenuRoom * room);
        virtual ~MultiplayerMenu(void);

    private:
        // Saves the server button as defined by the current values in the text entry boxes, using getTopLevel()->afterEvent.
        // This also runs saveServerButtonsToXML. This call is what should be run when the user
        // presses save.
        void saveServerButton(void);

        // Saves the server button with the specified properties, immediately.
        void saveServerButton(rascUI::Theme * theme, const std::string & serverName, const std::string & address, uint16_t port);

        void editServerButton(void);
        void onSetServerButton(ServerButton * currentServerButton);
        void onUnsetServerButton(void);
        void deleteServerButton(void);

        // Loads all server buttons from the xml config.
        void loadServerButtonsFromXML(rascUI::Theme * theme);

        // Saves all server buttons to the xml config, and requests that RascConfig saves to disk.
        // This should be called when we add, delete or edit a server button.
        void saveServerButtonsToXML(void);

        // The initialisation function for xml config.
        static void initConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem);
    };

}

#endif

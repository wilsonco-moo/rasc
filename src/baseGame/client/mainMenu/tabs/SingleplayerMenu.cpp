/*
 * SingleplayerMenu.cpp
 *
 *  Created on: 22 Mar 2019
 *      Author: wilson
 */

#include "SingleplayerMenu.h"

#include <rascUI/util/FunctionQueue.h>
#include <threading/ThreadQueue.h>
#include <rascUI/util/Layout.h>
#include <tinyxml2/tinyxml2.h>
#include <rascUI/base/Theme.h>

#include "../../../common/config/StaticConfig.h"
#include "../../../common/config/RascConfig.h"
#include "../../../common/util/XMLUtil.h"
#include "../../../common/util/Misc.h"
#include "../../GlobalProperties.h"
#include "../MainMenuRoom.h"

#define LAYOUT_THEME theme

// -------------------- Top section ------------------------

#define LABELS_WIDTH             192
#define TOP_FRONT_PANEL_HEIGHT   COUNT_OUTBORDER_Y(2)

#define TOP_FRONT_PANEL_LOC                       \
    PIN_T(TOP_FRONT_PANEL_HEIGHT,                 \
     SUB_BORDER(GEN_FILL)                         \
    )

#define TOP_LABEL_LOC(id)                         \
    MOVE_BORDERNORMAL_Y(id,                       \
     SETNORMAL_Y(                                 \
     PIN_L(LABELS_WIDTH,                          \
      SUB_BORDER(TOP_FRONT_PANEL_LOC)             \
    )))

#define TOP_BOX_LOC(id)                           \
    MOVE_BORDERNORMAL_Y(id,                       \
     SETNORMAL_Y(                                 \
     SUB_BORDERMARGIN_L(LABELS_WIDTH,             \
      SUB_BORDER(TOP_FRONT_PANEL_LOC)             \
    )))

// --------------------- Left or right panels ------------------

#define LEFT_RIGHT_LABELS_WIDTH  120

#define ROW_LOC(id)                               \
    MOVE_BORDERNORMAL_Y(id,                       \
     SETNORMAL_Y(GEN_BORDER)                      \
    )

#define LABEL_LOC(id)                             \
    PIN_L(LEFT_RIGHT_LABELS_WIDTH,                \
     ROW_LOC(id)                                  \
    )

#define BOX_LOC(id)                               \
    SUB_BORDERMARGIN_L(LEFT_RIGHT_LABELS_WIDTH,   \
     ROW_LOC(id)                                  \
    )

// --------------------- Left panel (new game) -----------------

#define LEFT_TOP_PANEL_HEIGHT     COUNT_OUTBORDER_Y(4)

#define LEFT_AREA_LOC                             \
    BORDERSPLIT_L(0.5f,                           \
     SUB_MARGIN_B(COUNT_ONEBORDER_Y(1),           \
      SUB_BORDERMARGIN_T(TOP_FRONT_PANEL_HEIGHT,  \
       SUB_BORDER(GEN_FILL)                       \
    )))

#define LEFT_TOP_BACK_PANEL_LOC                   \
    PIN_T(LEFT_TOP_PANEL_HEIGHT + UI_BORDER,      \
     GEN_FILL                                     \
    )

#define LEFT_TOP_FRONT_PANEL_LOC                  \
    PIN_T(LEFT_TOP_PANEL_HEIGHT,                  \
     GEN_FILL                                     \
    )

#define LEFT_SCROLL_PANE_LOC                      \
    SUB_BORDERMARGIN_T(LEFT_TOP_PANEL_HEIGHT,     \
     GEN_FILL                                     \
    )

#define LEFT_BUTTON_LOC(id)                       \
    BORDERTABLE_X(id, 2,                          \
     BOX_LOC(3)                                   \
    )

// --------------------- Right panel (load game) ---------------

#define RIGHT_TOP_PANEL_HEIGHT     COUNT_OUTBORDER_Y(2)

#define RIGHT_AREA_LOC                            \
    BORDERSPLIT_R(0.5f,                           \
     SUB_MARGIN_B(COUNT_ONEBORDER_Y(1),           \
      SUB_BORDERMARGIN_T(TOP_FRONT_PANEL_HEIGHT,  \
       SUB_BORDER(GEN_FILL)                       \
    )))

#define RIGHT_TOP_BACK_PANEL_LOC                  \
    PIN_T(RIGHT_TOP_PANEL_HEIGHT + UI_BORDER,     \
     GEN_FILL                                     \
    )

#define RIGHT_TOP_FRONT_PANEL_LOC                 \
    PIN_T(RIGHT_TOP_PANEL_HEIGHT,                 \
     GEN_FILL                                     \
    )

#define RIGHT_SCROLL_PANE_LOC                     \
    SUB_BORDERMARGIN_T(RIGHT_TOP_PANEL_HEIGHT,    \
     GEN_FILL                                     \
    )

#define RIGHT_BUTTON_LOC(id)                      \
    BORDERTABLE_X(id, 3,                          \
     BOX_LOC(1)                                   \
    )

// ------------------ Bottom panel -----------------------------

#define BOTTOM_BACK_PANEL_LOC                     \
    PIN_B(COUNT_OUTBORDER_Y(1), GEN_FILL)


// The base xml tag for our configuration.
#define BASE_CONFIG_NAME "singleplayerMenu"
#define GET_BASE simplenetwork::MutexPtr<tinyxml2::XMLDocument> doc = room->globalProperties->rascConfig->getDocument();  \
                 tinyxml2::XMLElement * base = RascConfig::getOrInitBase(&*doc, BASE_CONFIG_NAME, &SingleplayerMenu::initConfig);

namespace rasc {

    SingleplayerMenu::SingleplayerMenu(const rascUI::Location & location, rascUI::Theme * theme, MainMenuRoom * room) :
        rascUI::Container(location),
        room(room),
        token(room->globalProperties->threadQueue->createToken()),

        // ---------------------------- Top section ---------------------------------

        topFrontPanel(rascUI::Location(TOP_FRONT_PANEL_LOC)),
        playerNameLabel(rascUI::Location(TOP_LABEL_LOC(0)), "Player name:"),
        playerNameBox(rascUI::Location(TOP_BOX_LOC(0)), "", [this](const std::string & newValue) {
            setButtonActivity();
            saveToXML();
        }),
        portLabel(rascUI::Location(TOP_LABEL_LOC(1)), "Server port:"),
        portBox(rascUI::Location(TOP_BOX_LOC(1)), "", [this](const std::string & newValue) {
            portBox.setText(std::to_string(Misc::strtolRange(newValue, 0, 65535)));
            saveToXML();
        }),

        // --------------------------- Left panel (new game) ------------------------

        leftContainer(rascUI::Location(LEFT_AREA_LOC)),
        leftTopFrontPanel(rascUI::Location(LEFT_TOP_FRONT_PANEL_LOC)),
        leftTopBackPanel(rascUI::Location(LEFT_TOP_BACK_PANEL_LOC)),
        leftTopLabel(rascUI::Location(ROW_LOC(0)), "Start a new game"),
        gameNameLabel(rascUI::Location(LABEL_LOC(1)), "Game name:"),
        gameNameBox(rascUI::Location(BOX_LOC(1)), "New Rasc game", [this](const std::string & newValue) {
            setButtonActivity();
        }),
        aiPlayersLabel(rascUI::Location(LABEL_LOC(2)), "AI players:"),
        aiPlayersBox(rascUI::Location(BOX_LOC(2)), "4", [this](const std::string & newValue) {
            aiPlayersBox.setText(std::to_string(Misc::strtolRange(newValue, 0, INT_MAX)));
        }),
        leftScrollPane(theme, rascUI::Location(LEFT_SCROLL_PANE_LOC), false, true, false, false),
        leftBottomLabel(rascUI::Location(ROW_LOC(3)), "Select map:"),
        mapSelectSeries(false),
        mapSelectToggleButtons(),
        leftLaunchButton(rascUI::Location(LEFT_BUTTON_LOC(1)), "Launch new",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getTopLevel()->getMb()) {
                    GlobalProperties * glob = this->room->globalProperties;

                    glob->address = GlobalProperties::LOCAL_ADDRESS;
                    glob->port = (uint16_t)Misc::strtolRange(portBox.getText(), 0, 65535);
                    glob->playerName = playerNameBox.getText();
                    glob->mapFilename = mapSelectToggleButtons[mapSelectSeries.getSelectedId()].getText();
                    glob->aisToPlace = Misc::strtolRange(aiPlayersBox.getText(), 0, INT_MAX);
                    glob->startInternalServer = true; // We want to start an internal server, as we are starting the server ourself.
                    glob->startNew = true;
                    glob->saveName = gameNameBox.getText();

                    this->room->connectionPopup.setSelectWhenHide(&leftLaunchButton);
                    this->room->connectionPopup.show();
                }
            }
        ),

        // ----------------------------- Right panel (new game) ---------------------

        rightContainer(rascUI::Location(RIGHT_AREA_LOC)),
        rightTopFrontPanel(rascUI::Location(RIGHT_TOP_FRONT_PANEL_LOC)),
        rightTopBackPanel(rascUI::Location(RIGHT_TOP_BACK_PANEL_LOC)),
        rightTopLabel(rascUI::Location(ROW_LOC(0)), "Load a saved game"),
        rightScrollPane(theme, rascUI::Location(RIGHT_SCROLL_PANE_LOC), false, true, false, false),
        rightBottomLabel(rascUI::Location(LABEL_LOC(1)), "Select save:"),
        savefileSeries(false),
        savefileToggleButtons(),
        rightLaunchButton(rascUI::Location(RIGHT_BUTTON_LOC(1)), "Launch",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getTopLevel()->getMb()) {
                    GlobalProperties * glob = this->room->globalProperties;

                    glob->address = GlobalProperties::LOCAL_ADDRESS;
                    glob->port = (uint16_t)Misc::strtolRange(portBox.getText(), 0, 65535);
                    glob->playerName = playerNameBox.getText();
                    glob->mapFilename = mapSelectToggleButtons[mapSelectSeries.getSelectedId()].getText();
                    glob->aisToPlace = Misc::strtolRange(aiPlayersBox.getText(), 0, INT_MAX);
                    glob->startInternalServer = true; // We want to start an internal server, as we are starting the server ourself.
                    glob->startNew = false;
                    glob->saveName = savefileToggleButtons[savefileSeries.getSelectedId()].getText();

                    this->room->connectionPopup.setSelectWhenHide(&rightLaunchButton);
                    this->room->connectionPopup.show();
                }
            }
        ),
        rightDeleteSaveButton(rascUI::Location(RIGHT_BUTTON_LOC(2)), "Delete"),

        // ------------------------ Bottom panel ------------------------------------

        bottomBackPanel(rascUI::Location(BOTTOM_BACK_PANEL_LOC)),
        bottomFrontPanel(rascUI::Location(SUB_BORDER(BOTTOM_BACK_PANEL_LOC))) {

        // Add the maps and savefiles from a separate thread, in case the filesystem is slow enough to block the UI.
        Misc::backgroundScrollpaneAdd(theme, room->getMainMenuTopLevel(), token, room->globalProperties->threadQueue, &leftScrollPane, &mapSelectToggleButtons, &mapSelectSeries, &StaticConfig::getMapsClient, true);
        Misc::backgroundScrollpaneAdd(theme, room->getMainMenuTopLevel(), token, room->globalProperties->threadQueue, &rightScrollPane, &savefileToggleButtons, &savefileSeries, &StaticConfig::getSaves, true);

        // Once these have both finished, make sure that we update the activity of the buttons.
        room->globalProperties->threadQueue->addBarrier(token, [this](void) {
            this->room->getMainMenuTopLevel()->beforeEvent->addFunction([this](void) {
                setButtonActivity();
            });
        });

        add(&topFrontPanel);
        add(&playerNameLabel);
        add(&playerNameBox);
        add(&portLabel);
        add(&portBox);

        add(&leftContainer);
        leftContainer.add(&leftScrollPane);
        leftContainer.add(&leftTopBackPanel);
        leftContainer.add(&leftTopFrontPanel);
        leftContainer.add(&leftTopLabel);
        leftContainer.add(&gameNameLabel);
        leftContainer.add(&gameNameBox);
        leftContainer.add(&aiPlayersLabel);
        leftContainer.add(&aiPlayersBox);
        leftContainer.add(&leftBottomLabel);
        leftContainer.add(&leftLaunchButton);
        leftLaunchButton.setActive(false);

        add(&rightContainer);
        rightContainer.add(&rightScrollPane);
        rightContainer.add(&rightTopBackPanel);
        rightContainer.add(&rightTopFrontPanel);
        rightContainer.add(&rightTopLabel);
        rightContainer.add(&rightBottomLabel);
        rightContainer.add(&rightLaunchButton);
        rightContainer.add(&rightDeleteSaveButton);
        rightLaunchButton.setActive(false);
        rightDeleteSaveButton.setActive(false);

        add(&bottomBackPanel);
        add(&bottomFrontPanel);

        loadFromXML();
    }

    SingleplayerMenu::~SingleplayerMenu(void) {
        room->globalProperties->threadQueue->deleteToken(token);
    }

    void SingleplayerMenu::setButtonActivity(void) {
        leftLaunchButton.setActive(!gameNameBox.getText().empty() && !playerNameBox.getText().empty());

        bool savesExist = (rightScrollPane.contents.getComponentCount() > 0);
        rightLaunchButton.setActive(!playerNameBox.getText().empty() && savesExist);
        rightDeleteSaveButton.setActive(savesExist);
    }

    void SingleplayerMenu::loadFromXML(void) {
        GET_BASE

        tinyxml2::XMLElement * name = readElement(base, "singleplayerName");
        playerNameBox.setText(readAttribute(name, "name"));

        tinyxml2::XMLElement * port = readElement(base, "lastUsedPort");
        portBox.setText(readAttribute(port, "value"));
    }

    void SingleplayerMenu::saveToXML(void) {
        {
            GET_BASE
            base->DeleteChildren();

            tinyxml2::XMLElement * name = doc->NewElement("singleplayerName");
            name->SetAttribute("name", playerNameBox.getText().c_str());
            base->InsertEndChild(name);

            tinyxml2::XMLElement * port = doc->NewElement("lastUsedPort");
            port->SetAttribute("value", portBox.getText().c_str());
            base->InsertEndChild(port);
        }
        room->globalProperties->rascConfig->saveLater();
    }

    void SingleplayerMenu::initConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem) {

        tinyxml2::XMLElement * name = doc->NewElement("singleplayerName");
        name->SetAttribute("name", GlobalProperties::DEFAULT_SINGLEPLAYER_PLAYER_NAME.c_str());
        elem->InsertEndChild(name);

        tinyxml2::XMLElement * port = doc->NewElement("lastUsedPort");
        port->SetAttribute("value", GlobalProperties::DEFAULT_PORT_STRING.c_str());
        elem->InsertEndChild(port);
    }
}

/*
 * SingleplayerMenu.h
 *
 *  Created on: 22 Mar 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_MAINMENU_TABS_OPTIONSMENU_H_
#define BASEGAME_CLIENT_MAINMENU_TABS_OPTIONSMENU_H_

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/util/EmplaceArray.h>
#include <rascUI/base/Container.h>
#include <vector>

#include "../../ui/customComponents/ProgressFloatingPanel.h"

namespace rascUI {
    class Theme;
    class TopLevelContainer;
}

namespace tinyxml2 {
    class XMLDocument;
    class XMLElement;
}

namespace rasc {

    class MainMenuRoom;
    class GlobalProperties;

    // ====================== Theme selection menu ======================

    class ThemeSelectionMenu : public rascUI::Container {
    private:
        class ThemeInfo; // This internally stores details when loading a theme.

        GlobalProperties * glob;
        rascUI::TopLevelContainer * topLevel;
        rascUI::BackPanel topBackPanel;
        rascUI::FrontPanel topFrontPanel;
        rascUI::Label topLabel;
        rascUI::Button useThemeButton;
        rascUI::ScrollPane scrollPane;
        rascUI::ToggleButtonSeries themeSeries;
        std::vector<rascUI::ToggleButton> themeButtons;
        void * token;
        ProgressFloatingPanel floatingPanel;
        ThemeInfo * themeInfo;

        // Initial theme layout values. This is so we can provide a warning to the user if a theme changes these.
        GLfloat normalComponentWidth, normalComponentHeight, uiBorder;

        // These std::function instances are called when any layout properties of the theme change.
        // The first is called from within the separate thread, and should be used for any lengthy operations.
        // This is the first opportunity to use the newly loaded theme, but NOTE THAT GlobalProperties is NOT
        // set to actually use the theme yet. Both functions are only ever called if loading the theme was actually successful.
        // The return value from the first is used as the parameter in the second.
        // The deleteParam method will be called when OptionsMenu is deleted. It will be provided with
        // either the return value of onChangeThemeLayoutInThread or NULL, depending on whether
        // onChangeThemeLayoutFinal got called in between or not.
        std::function<void*(rascUI::Theme*)> onChangeThemeLayoutInThread;
        std::function<void(rascUI::Theme*, void*)> onChangeThemeLayoutFinal;
        std::function<void(void *)> deleteParam; // This must delete the returned parameter, in the case that OptionsMenu is deleted before onChangeThemeLayoutFinal gets called.

        // This is used internally to store the return value of onChangeThemeLayoutInThread.
        void * changeThemeParam;

    public:
        ThemeSelectionMenu(
            const rascUI::Location & location,
            GlobalProperties * glob,
            rascUI::TopLevelContainer * topLevel,
            rascUI::Theme * theme,
            std::function<void*(rascUI::Theme*)> onChangeThemeLayoutInThread = [](rascUI::Theme * newTheme) -> void * {return NULL;},
            std::function<void(rascUI::Theme*, void*)> onChangeThemeLayoutFinal = [](rascUI::Theme * newTheme, void * param) {},
            std::function<void(void *)> deleteParam = [](void * param){}
        );

        virtual ~ThemeSelectionMenu(void);

        /**
         * This should be called by GlobalProperties during the title room, after setting
         * up the configuration system. This consults the configuration system to initially
         * load the correct theme. If that fails, the default theme name is tried, and the
         * configuration is updated to use the default theme. If that fails we exit.
         */
        static void loadCorrectTheme(GlobalProperties * glob);

        /**
         * This should be added directly to the top level container where ThemeSelectionMenu
         * is used.
         */
        inline rascUI::Component * getFloatingPanel(void) {
            return &floatingPanel;
        }

        /**
         * This gets the component which is most appropriate to select when a popup is hidden.
         */
        inline rascUI::Component * getAppropriateSelectWhenHide(void) {
            return themeSeries.getSelectedToggleButton();
        }

    private:
        static void initConfig(tinyxml2::XMLDocument * doc, tinyxml2::XMLElement * elem);
    };

    // ======================== Shared options menu ===============
    
    // Provides the options shared between client/server (shared config).
    class SharedOptionsMenu : public rascUI::Container {
    private:
        GlobalProperties & globalProperties;
        
        rascUI::FrontPanel frontPanel;
        rascUI::Label titleLabel;
        rascUI::Label colourBlindLabel;
        rascUI::ToggleButtonSeries colourBlindSeries;
        rascUI::EmplaceArray<rascUI::ToggleButton, 2> colourBlindButtons;
        rascUI::Label humanFlagLabel;
        rascUI::ToggleButtonSeries humanFlagSeries;
        rascUI::EmplaceArray<rascUI::ToggleButton, 2> humanFlagButtons;
        rascUI::Label aiFlagLabel;
        rascUI::ToggleButtonSeries aiFlagSeries;
        rascUI::EmplaceArray<rascUI::ToggleButton, 2> aiFlagButtons;
        
    public:
        SharedOptionsMenu(const rascUI::Location & location, GlobalProperties & globalProperties, rascUI::Theme * theme);
        
    private:
        // Convenience, called each time an option is changed.
        void updateAndSaveSharedOption(const char * tag, const char * attribute, const char * value) const;
    };
    
    // ======================== Rest of options menu ==============

    class OptionsMenu : public rascUI::Container {
    private:
        MainMenuRoom * room;
        rascUI::FrontPanel topFrontPanel;
        rascUI::Label label;
        ThemeSelectionMenu themeSelectionMenu;
        SharedOptionsMenu sharedOptionsMenu;
        ProgressFloatingPanelBase dummyFloatingPanel;
        rascUI::BackPanel bottomBackPanel;
        rascUI::FrontPanel bottomFrontPanel;
    public:
        OptionsMenu(const rascUI::Location & location, rascUI::Theme * theme, MainMenuRoom * room);
        virtual ~OptionsMenu(void);

        // These must be used by main menu room to ensure that the popup menus are added at the right time.
        inline rascUI::Component * getFloatingPanel(void) {
            return themeSelectionMenu.getFloatingPanel();
        }
        inline rascUI::FloatingPanel * getDummyFloatingPanel(void) {
            return &dummyFloatingPanel;
        }
    };

}

#endif

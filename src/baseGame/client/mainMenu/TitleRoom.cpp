/*
 * MainMenuRoom.cpp
 *
 *  Created on: 31 Dec 2018
 *      Author: wilson
 */

#include "TitleRoom.h"

#include <wool/window/base/WindowBase.h>
#include <wool/room/base/RoomBase.h>
#include <wool/font/Font.h>
#include <wool/misc/RGB.h>
#include <GL/gl.h>

#include "../config/MapModeController.h"
#include "../GlobalProperties.h"
#include "MainMenuRoom.h"


#define FRAMES_TO_SHOW_TITLE_ROOM 2



namespace rasc {

    TitleRoom::TitleRoom(GlobalProperties * globalProperties) :
        wool::RoomManaged(),
        globalProperties(globalProperties),
        frameCount(0) {

        background = wool::RGBA::BEIGE;
        viewOptions.viewMode = wool::viewModeAspect;
        viewOptions.width = 400;
        viewOptions.height = 300;
    }
    TitleRoom::~TitleRoom(void) {
    }

    void TitleRoom::init(void) {
    }
    void TitleRoom::end(void) {
    }
    void TitleRoom::keyNormal(unsigned char key, int x, int y) {
    }
    void TitleRoom::keyNormalRelease(unsigned char key, int x, int y) {
    }
    void TitleRoom::keySpecial(int key, int x, int y) {
    }
    void TitleRoom::keySpecialRelease(int key, int x, int y) {
    }
    void TitleRoom::mouseEvent(int button, int state, int x, int y) {
    }
    void TitleRoom::mouseMove(int x, int y) {
    }
    void TitleRoom::mouseDrag(int x, int y) {
    }
    bool TitleRoom::shouldDeleteOnRoomEnd(void) {
        return true;
    }
    void TitleRoom::onStep(void) {
        glBegin(GL_QUADS);
        wool_setColourRGB(wool::RGB::BLUE);
        wool::Font::font.drawCStringScale("Initialising game...", 8.0f, 8.0f, 1.0f, 1.0f);
        glEnd();


        if (frameCount == 1) {
            // Run this once, when drawing the second frame. This way there is already
            // one frame shown in the window.
            globalProperties->titleRoomInitialisation();
        }
        frameCount++;
        if (frameCount >= FRAMES_TO_SHOW_TITLE_ROOM) {
            getWindowBase()->changeRoom(new MainMenuRoom(globalProperties));

            // Immediately after creating the main menu room, run the main menu initialisation.
            globalProperties->mainMenuRoomInitialisation();
        }
    }
}

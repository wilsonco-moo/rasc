/*
 * ConnectionPopup.h
 *
 *  Created on: 4 Jan 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_MAINMENU_CONNECTIONPOPUP_H_
#define BASEGAME_CLIENT_MAINMENU_CONNECTIONPOPUP_H_

#include <GL/gl.h>
#include <string>

#include "../ui/customComponents/ProgressFloatingPanel.h"

namespace rascUI {
    class Theme;
}

namespace rasc {

    class MainMenuRoom;
    class LoadingRoom;

    class ConnectionPopup : public ProgressFloatingPanel {
    private:
        MainMenuRoom * room;

        // This counts time since show() was called, so we can show the countdown.
        GLfloat timeElapsed;

        // Our connection job sets this. If this is NULL after the connection job has
        // ended, the connection must have failed.
        LoadingRoom * newRoom;

        // The reason for failing to launch the game client. This is only set if LoadingRoom::initialiseGameClient fails.
        std::string failureReason;

        // Don't allow copying: We hold raw resources.
        ConnectionPopup(const ConnectionPopup & other);
        ConnectionPopup & operator = (const ConnectionPopup & other);

    public:
        ConnectionPopup(MainMenuRoom * room, rascUI::Theme * theme, void * token);
        virtual ~ConnectionPopup(void);

    protected:
        virtual void onProgressShow(void) override;
        virtual bool backgroundJob(void) override;
        virtual void onWait(void) override;
        virtual void onEnd(bool success) override;
    };
}

#endif

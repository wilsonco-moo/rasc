/*
 * ConnectionPopup.cpp
 *
 *  Created on: 4 Jan 2019
 *      Author: wilson
 */

#include "ConnectionPopup.h"

#include <cmath>
#include <cstdio>
#include <thread>
#include <chrono>

#include <wool/room/base/RoomBase.h>
#include <wool/window/base/WindowBase.h>
#include <rascUI/util/Layout.h>

#include "../GlobalProperties.h"
#include "../rooms/LoadingRoom.h"
#include "MainMenuRoom.h"

// The text to use for timeouts.
// The buffer size MUST BE UPDATED if this text is changed. This should be approximately the length of the timeout text plus 20.
#define TIMEOUT_TEXT "Timeout: %.1f seconds"
#define BUFFER_SIZE 40

namespace rasc {

    ConnectionPopup::ConnectionPopup(MainMenuRoom * room, rascUI::Theme * theme, void * token) :
        ProgressFloatingPanel(room->globalProperties->threadQueue, token, theme, true, rascUI::Location(GEN_CENTRE_FIXED(400, 128))),
        room(room),
        timeElapsed(0.0f),
        newRoom(NULL),
        failureReason() {
    }

    ConnectionPopup::~ConnectionPopup(void) {
        // Delete the new room if it exists.
        delete newRoom;
    }

    void ConnectionPopup::onProgressShow(void) {
        timeElapsed = 0.0f;
        topLabel.setText("Connecting to server...");
    }

    bool ConnectionPopup::backgroundJob(void) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        newRoom = LoadingRoom::initialiseGameClient(this->room->globalProperties, failureReason);
        return newRoom != NULL;
    }

    void ConnectionPopup::onWait(void) {
        char buffer[BUFFER_SIZE];
        GLfloat value = fmax(room->globalProperties->connectionTimeout - timeElapsed, 0.0f);
        snprintf(buffer, BUFFER_SIZE, TIMEOUT_TEXT, value);
        bottomLabel.setText(buffer);
        timeElapsed += getTopLevel()->getElapsed();
    }

    void ConnectionPopup::onEnd(bool success) {
        if (success) {
            // If the new room is not NULL, switch to it. We will be destroyed soon now
            // so don't care about anything else.
            room->getWindowBase()->changeRoom(newRoom);
            // Set this to null so we don't delete the new room immediately in our destructor.
            newRoom = NULL;
        } else {
            // If the connection failed, notify the user of the failure.
            topLabel.setText(failureReason);
            bottomLabel.setText("");
        }
    }
}

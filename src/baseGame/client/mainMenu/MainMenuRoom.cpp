/*
 * MainMenuRoom.cpp
 *
 *  Created on: 31 Dec 2018
 *      Author: wilson
 */

#include "MainMenuRoom.h"

#include <wool/window/base/WindowBase.h>
#include <wool/room/base/RoomBase.h>
#include <threading/ThreadQueue.h>
#include <rascUI/base/Theme.h>
#include <wool/font/Font.h>
#include <wool/misc/RGBA.h>
#include <algorithm>
#include <cstddef>

#include "../../common/util/numeric/MultiCol.h"
#include "../../common/config/StaticConfig.h"
#include "../../common/util/numeric/Random.h"
#include "../config/MapModeController.h"
#include "../../common/util/Misc.h"
#include "../GlobalProperties.h"
#include "MainMenuBackground.h"



// Define macros for the theming.
#define UI_BORDER        theme->uiBorder
#define UI_BWIDTH        theme->normalComponentWidth
#define UI_BHEIGHT       theme->normalComponentHeight

#define GET_COORDINATES                                 \
    GLfloat x1 = location.cache.x,                      \
            y1 = location.cache.y,                      \
            x2 = x1 + location.cache.width,             \
            y2 = y1 + location.cache.height;

#define SCALED_LOC(L_BORD, SEP, R_BORD, ID, COUNT, Y, HEIGHT, Y_WEIGHT, Y_HEIGHT_WEIGHT) \
    (L_BORD) - (((GLfloat)(ID))/((GLfloat)(COUNT))) * ((L_BORD) + (R_BORD) - (SEP)),     \
    (Y),                                                                                 \
    -((L_BORD) + (R_BORD) + (SEP) * ((COUNT) - 1)) / ((GLfloat)(COUNT)),                 \
    (HEIGHT),                                                                            \
    ((GLfloat)(ID))/((GLfloat)(COUNT)),                                                  \
    (Y_WEIGHT),                                                                          \
    1.0f/((GLfloat)(COUNT)),                                                             \
    (Y_HEIGHT_WEIGHT)







#define TOP_PANEL_HEIGHT    128
#define TOP_PANEL_LOC       0, 0, 0, TOP_PANEL_HEIGHT, 0, 0, 1, 0

#define VERSION_LABEL_LOC   0, 0, 128, UI_BHEIGHT, 0, 0, 0, 0
#define AUTHOR_LABEL_LOC(id, len) -((len + 1) * theme->customTextCharWidth), id * UI_BHEIGHT, (len + 1) * theme->customTextCharWidth, UI_BHEIGHT, 1, 0, 0, 0

#define BUTTON_PANEL_HEIGHT (UI_BORDER*2+UI_BHEIGHT)
#define BUTTON_PANEL_LOC    0, TOP_PANEL_HEIGHT, 0, BUTTON_PANEL_HEIGHT, 0, 0, 1, 0

#define BUTTON_COUNT        5
#define BUTTON_Y            TOP_PANEL_HEIGHT + UI_BORDER
#define BUTTON_LOC(id)      SCALED_LOC(MENU_AREA_LEFT_BORDER, UI_BORDER, MENU_AREA_RIGHT_BORDER, id, 4, BUTTON_Y, UI_BHEIGHT, 0, 0)

// ================================= MENU AREA ===========================================

// The fixed width to use for the menu area. Comment this out to use a proportional width, with the left and right borders.
#define MENU_AREA_FIXED_WIDTH   400

// The borders. The left and right borders are only used for menu areas if fixed width is disabled.
#define MENU_AREA_TOP_BORDER          48
#define MENU_AREA_LEFT_BORDER         48
#define MENU_AREA_BOTTOM_BORDER       48
#define MENU_AREA_RIGHT_BORDER        48

// The y position of the menu area
#define MENU_AREA_Y        (TOP_PANEL_HEIGHT + BUTTON_PANEL_HEIGHT + MENU_AREA_TOP_BORDER)

// Define the menu area location
#ifdef MENU_AREA_FIXED_WIDTH
    #define MENU_AREA_LOC      -MENU_AREA_FIXED_WIDTH*0.5f, MENU_AREA_Y, MENU_AREA_FIXED_WIDTH, -MENU_AREA_Y-MENU_AREA_BOTTOM_BORDER, 0.3f, 0, 0.4f, 1
#else
    #define MENU_AREA_LOC      MENU_AREA_LEFT_BORDER, MENU_AREA_Y, -MENU_AREA_LEFT_BORDER-MENU_AREA_RIGHT_BORDER, -MENU_AREA_Y-MENU_AREA_BOTTOM_BORDER, 0, 0, 1, 1
#endif

// =======================================================================================

namespace rasc {

    // --------- Custom title ---------------------
        #define TITLE_SCALE       8.0f
        #define TITLE_CHARS       4
        #define TITLE_CHAR_WIDTH  (theme->customTitleTextCharWidth)
        #define TITLE_CHAR_HEIGHT 11.0f
        #define TITLE_WIDTH       (TITLE_CHAR_WIDTH * TITLE_CHARS * TITLE_SCALE)
        #define TITLE_HEIGHT      (TITLE_CHAR_HEIGHT * TITLE_SCALE)
        #define TITLE_HALF_WIDTH  (TITLE_WIDTH * 0.5f)
        #define TITLE_XOFF        ((location.cache.width/location.xScale) * 0.5f - TITLE_HALF_WIDTH)
        #define TITLE_YOFF        16
        MainMenuRoom::RascTitle::RascTitle(const rascUI::Location & location, MainMenuRoom * room) :
            rascUI::Component(location),
            room(room),
            random(NULL),
            layoutMode(Misc::timeSeededRandomRange(0, 16) == 5 ? ENABLED : DISABLED),
            colour(), xPos(0.0f), yPos(0.0f), xsp(65.0f), ysp(18.0f) {
        }
        MainMenuRoom::RascTitle::~RascTitle(void) {
            delete random;
        }
        void MainMenuRoom::RascTitle::onMouseClick(GLfloat viewX, GLfloat viewY, int button) {
            if (layoutMode != ENABLED || button != getTopLevel()->getMb()) return;
            rascUI::Theme * theme = getTopLevel()->theme;
            GLfloat offX, offY; theme->getTextBaseOffsets(rascUI::State::normal, offX, offY);
            GLfloat x1 = (location.cache.width * 0.5f) + (offX - TITLE_HALF_WIDTH) * location.xScale, y1 = (TITLE_YOFF + offY) * location.yScale,
                    x2 = x1 + TITLE_WIDTH * location.xScale, y2 = y1 + TITLE_HEIGHT * location.yScale;
            if (viewX >= x1 && viewX < x2 && viewY >= y1 && viewY < y2) {
                layoutMode = ACTIVE; xPos = TITLE_XOFF; yPos = TITLE_YOFF; random = new Random();
                room->rascVersionLabel.setVisible(false); room->author1Label.setVisible(false); room->author2Label.setVisible(false); room->author3Label.setVisible(false);
                MultiCol col = MultiCol::perceptUniformRandomCol(*random);
                colour = wool::RGB(col.r() / 255.0f, col.g() / 255.0f, col.b() / 255.0f);
            }
        }
        bool MainMenuRoom::RascTitle::shouldRespondToKeyboard(void) const {
            return false;
        }
        void MainMenuRoom::RascTitle::onDraw(void) {
            rascUI::Theme * theme = getTopLevel()->theme;
            {
                GET_COORDINATES
                if (layoutMode == ACTIVE) {
                    wool_setColourRGB(wool::RGB::BLACK);
                } else {
                    theme->customSetDrawColour(theme->customColourBackplaneMain);
                }
                glVertex2f(x1, y1);
                glVertex2f(x2, y1);
                glVertex2f(x2, y2);
                glVertex2f(x1, y2);
            }
            GLfloat x, y;
            if (layoutMode == ACTIVE) {
                glColor3f(colour.red, colour.green, colour.blue);
                GLfloat elapsed = getTopLevel()->getElapsed(),
                        xspe = xsp * elapsed, yspe = ysp * elapsed, offX, offY;
                getTopLevel()->theme->getTextBaseOffsets(rascUI::State::normal, offX, offY);
                GLfloat minX = -offX, minY = -offY,
                        maxX = (location.cache.width / location.xScale) - TITLE_WIDTH - offX,
                        maxY = (location.cache.height / location.yScale) - TITLE_HEIGHT - offY;
                xPos = Misc::minmax(xPos, minX, maxX); yPos = Misc::minmax(yPos, minY, maxY);
                if (maxX > minX && maxY > minY) {
                    bool switched = false;
                    if (xPos + xspe < minX || xPos + xspe > maxX) { xsp *= -1.0f; xspe = xsp * elapsed; switched = true; }
                    if (yPos + yspe < minY || yPos + yspe > maxY) { ysp *= -1.0f; yspe = ysp * elapsed; switched = true; }
                    if (switched) {
                        MultiCol col = MultiCol::perceptUniformRandomCol(*random);
                        colour = wool::RGB(col.r() / 255.0f, col.g() / 255.0f, col.b() / 255.0f);
                    }
                    xPos += xspe; yPos += yspe;
                }
                x = xPos; y = yPos;
            } else {
                theme->customSetDrawColour(theme->customColourMainText);
                x = TITLE_XOFF; y = TITLE_YOFF;
            }
            theme->customDrawTextTitle(location, x, y, TITLE_SCALE, TITLE_SCALE, "Rasc");
        }
    // --------------------------------------------




    MainMenuRoom::MainMenuRoom(GlobalProperties * globalProperties, rascUI::Theme * prospectiveTheme, bool reloadedFromThemeSwitch) :
        wool::RoomManaged(),
        theme(prospectiveTheme == NULL ? globalProperties->themeLoader.getTheme() : prospectiveTheme), // Only use the provided theme if it is not NULL. Otherwise use the one from GlobalProperties.
        globalProperties(globalProperties),
        token(globalProperties->threadQueue->createToken()),

        // ----------------- UI Stuff ---------------------

        connectionPopup(this, theme, token),

        uiScale(1.0f),
        topLevel(theme, &rascUI::Bindings::defaultOpenGL, &uiScale, &uiScale),

        title(rascUI::Location(TOP_PANEL_LOC), this),
        rascVersionLabel(rascUI::Location(VERSION_LABEL_LOC), StaticConfig::RASC_VERSION_AND_PLATFORM),
        author1Label(rascUI::Location(AUTHOR_LABEL_LOC(0, 19)), "Designed/created by"),
        author2Label(rascUI::Location(AUTHOR_LABEL_LOC(1, 15)), "Andrew Ackerley"),
        author3Label(rascUI::Location(AUTHOR_LABEL_LOC(2, 13)), "Daniel Wilson"),
        buttonPanel(rascUI::Location(BUTTON_PANEL_LOC)),
        backPanel(rascUI::Location(MENU_AREA_LOC)),

        menuSwitchSeries(false),

        singleplayerButton(&menuSwitchSeries, rascUI::Location(BUTTON_LOC(0)), "Singleplayer",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                menuAreaContainer.clear();
                menuAreaContainer.add(&singleplayerMenu);
            }
        ),
        multiplayerButton(&menuSwitchSeries, rascUI::Location(BUTTON_LOC(1)), "Multiplayer",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                menuAreaContainer.clear();
                menuAreaContainer.add(&multiplayerMenu);
            }
        ),
        optionsButton(&menuSwitchSeries, rascUI::Location(BUTTON_LOC(2)), "Options",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                menuAreaContainer.clear();
                menuAreaContainer.add(&optionsMenu);
            }
        ),
        exitButton(rascUI::Location(BUTTON_LOC(3)), "Exit",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == topLevel.getMb()) getWindowBase()->exitLater();
            }
        ),

        menuAreaContainer(rascUI::Location(MENU_AREA_LOC)),
        multiplayerMenu(rascUI::Location(), theme, this),
        singleplayerMenu(rascUI::Location(), theme, this),
        optionsMenu(rascUI::Location(), theme, this) {

        topLevel.setScalePointers(&globalProperties->uiScale, &globalProperties->uiScale);

        topLevel.add(&backPanel);
        topLevel.add(&menuAreaContainer);

        if (reloadedFromThemeSwitch) {
            menuSwitchSeries.setSelectedButton(&optionsButton);
            menuAreaContainer.add(&optionsMenu);
        } else {
            menuSwitchSeries.setSelectedButton(&multiplayerButton);
            menuAreaContainer.add(&multiplayerMenu);
        }



        topLevel.add(&title);
        topLevel.add(&rascVersionLabel);
        topLevel.add(&author1Label);
        topLevel.add(&author2Label);
        topLevel.add(&author3Label);
        topLevel.add(&buttonPanel);
        topLevel.add(&singleplayerButton);
        topLevel.add(&multiplayerButton);
        topLevel.add(&optionsButton);
        topLevel.add(&exitButton);

        // Add all popup menus, at the end.
        topLevel.add(&connectionPopup);
        topLevel.add(optionsMenu.getFloatingPanel());
        topLevel.add(optionsMenu.getDummyFloatingPanel());

        if (reloadedFromThemeSwitch) {
            optionsMenu.getDummyFloatingPanel()->show();
        }

        background = wool::RGBA::BLACK;
        viewOptions.viewMode = wool::viewModeExtend;
    }
    MainMenuRoom::~MainMenuRoom(void) {
        globalProperties->threadQueue->deleteToken(token);
    }

    void MainMenuRoom::init(void) {
    }
    void MainMenuRoom::end(void) {
    }
    void MainMenuRoom::keyNormal(unsigned char key, int x, int y) {
        topLevel.keyPress(key, false);
    }
    void MainMenuRoom::keyNormalRelease(unsigned char key, int x, int y) {
        topLevel.keyRelease(key, false);
    }
    void MainMenuRoom::keySpecial(int key, int x, int y) {
        if (topLevel.keyPress(key, true)) {
            switch(key) {
            case 1:
                globalProperties->increaseUIScale();
                break;
            case 2:
                globalProperties->decreaseUIScale();
                break;
            case 3:
                globalProperties->resetUIScale();
                break;
            }
        }
    }
    void MainMenuRoom::keySpecialRelease(int key, int x, int y) {
        topLevel.keyRelease(key, true);
    }
    void MainMenuRoom::mouseEvent(int button, int state, int x, int y) {
        topLevel.mouseEvent(button, state, windowXToViewX(x), windowYToViewY(y));
    }
    void MainMenuRoom::mouseMove(int x, int y) {
        topLevel.mouseMove(windowXToViewX(x), windowYToViewY(y));
    }
    void MainMenuRoom::mouseDrag(int x, int y) {
        topLevel.mouseMove(windowXToViewX(x), windowYToViewY(y));
    }
    bool MainMenuRoom::shouldDeleteOnRoomEnd(void) {
        return true;
    }
    void MainMenuRoom::onStep(void) {
        rascUI::Rectangle rect(0, 0, getViewCurrentWidth(), getViewCurrentHeight());
        globalProperties->mainMenuBackground->draw(rect.width, rect.height, getWindowBase()->elapsed());
        glBegin(GL_QUADS);
        topLevel.draw(rect, getWindowBase()->elapsed());
        glEnd();
    }
}

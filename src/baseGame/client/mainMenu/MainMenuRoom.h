/*
 * MainMenuRoom.h
 *
 *  Created on: 31 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_MAINMENU_MAINMENUROOM_H_
#define BASEGAME_CLIENT_MAINMENU_MAINMENUROOM_H_

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/base/TopLevelContainer.h>
#include <wool/room/managed/RoomManaged.h>
#include <wool/misc/RGB.h>
#include <GL/gl.h>

#include "tabs/SingleplayerMenu.h"
#include "tabs/MultiplayerMenu.h"
#include "tabs/OptionsMenu.h"
#include "ConnectionPopup.h"

namespace rascUI {
    class Theme;
}

namespace rasc {
    class Random;
    class GlobalProperties;

    class MainMenuRoom : public wool::RoomManaged {
    private:
        // This is taken from GlobalProperties, and stored here for convenience.
        rascUI::Theme * theme;

    public:
        GlobalProperties * globalProperties;
    private:
        // Required for connectionPopup.
        void * token;
    public:
        ConnectionPopup connectionPopup;
    private:

        // ================================ UI stuff =====================================

        // --------- Custom title ---------------------
        class RascTitle : public rascUI::Component {
        private:
            MainMenuRoom * room;
            Random * random;
            enum { DISABLED, ENABLED, ACTIVE };
            unsigned int layoutMode;
            wool::RGB colour;
            GLfloat xPos, yPos, xsp, ysp;
        public:
            RascTitle(const rascUI::Location & location, MainMenuRoom * room);
            virtual ~RascTitle(void);
        protected:
            virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;
            virtual bool shouldRespondToKeyboard(void) const override;
            virtual void onDraw(void);
        };
        // --------------------------------------------

        GLfloat uiScale;

        rascUI::TopLevelContainer topLevel;

        RascTitle title;
        rascUI::Label rascVersionLabel, author1Label, author2Label, author3Label;
        rascUI::BackPanel buttonPanel, backPanel;

        rascUI::ToggleButtonSeries menuSwitchSeries;
        rascUI::ToggleButton singleplayerButton, multiplayerButton, optionsButton;
        rascUI::Button exitButton;

        rascUI::Container menuAreaContainer;

        MultiplayerMenu multiplayerMenu;
        SingleplayerMenu singleplayerMenu;
        OptionsMenu optionsMenu;

        // ===========================================================================

    public:
        /**
         * A theme can be optionally provided so that a second MainMenuRoom, (with a newly
         * loaded and not yet used theme), can be created, after switching themes.
         * Otherwise, the theme from the provided GlobalProperties instance is used.
         * If reloadedFromThemeSwitch is set to true, we automatically show the options menu
         * and show the options menu dummy floating panel to tell the user that we just
         * switched theme.
         */
        MainMenuRoom(GlobalProperties * connectionInfo, rascUI::Theme * theme = NULL, bool reloadedFromThemeSwitch = false);
        virtual ~MainMenuRoom(void);

        virtual void init(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;

        /**
         * This allows easy access to our top level container. This should be used in
         * constructors of tabs, where they are not added yet.
         */
        inline rascUI::TopLevelContainer * getMainMenuTopLevel(void) {
            return &topLevel;
        }

    protected:
        virtual void onStep(void) override;

    };
}
#endif

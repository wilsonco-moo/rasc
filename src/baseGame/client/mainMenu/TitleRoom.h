/*
 * MainMenuRoom.h
 *
 *  Created on: 31 Dec 2018
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_MAINMENU_TITLEROOM_H_
#define BASEGAME_CLIENT_MAINMENU_TITLEROOM_H_

#include <wool/room/managed/RoomManaged.h>

namespace rasc {
    class GlobalProperties;

    class TitleRoom : public wool::RoomManaged {

    private:

        GlobalProperties * globalProperties;
        int frameCount;

    public:
        TitleRoom(GlobalProperties * connectionInfo);
        virtual ~TitleRoom(void);

        virtual void init(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;

    protected:
        virtual void onStep(void) override;

    };
}
#endif

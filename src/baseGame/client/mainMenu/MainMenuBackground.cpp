/*
 * MainMenuBackground.cpp
 *
 *  Created on: 4 Feb 2019
 *      Author: wilson
 */

#include "MainMenuBackground.h"

#include <wool/texture/Texture.h>
#include <iostream>
#include <GL/gl.h>
#include <cmath>

#include "../../common/config/StaticConfig.h"


/**
 * How much of the texture the view shows.
 * A value of 1 shows as much as possible within the aspect ratio, values less than 1 are more "zoomed in".
 */
#define VIEW_FILL 0.5f

/**
 * The speed we fade in, measured in change of alpha per second.
 */
#define FADE_IN_SPEED 1.0f

/**
 * The number of frames after the main menu background texture has loaded that we start increasing the alpha.
 * This avoids problems where the elapsed time might be very long while loading the texture, when running in valgrind.
 */
#define ALPHA_DELAY 3


#define OVERALL_SPEED 0.05f


namespace rasc {

    MainMenuBackground::MainMenuBackground(threading::ThreadQueue * threadQueue) :
        texture(threadQueue, StaticConfig::ASSETS_DIRECTORY + "mainMenuBackground.png", true),
        cameraX(0.5f),
        cameraY(0.5f),
        cameraXSpeed(OVERALL_SPEED),
        cameraYSpeed(OVERALL_SPEED),
        alpha(0.0f),
        alphaCount(0) {
    }

    MainMenuBackground::~MainMenuBackground(void) {
    }


    void MainMenuBackground::updatePosition(GLfloat aspect, GLfloat elapsed) {
        // Set the x and y speed multipliers. This scales the speed uniformly
        // according to the width and height of the texture.
        GLfloat xMult, yMult;
        if (aspect > 0) {
            xMult = 1.0f;
            yMult = aspect;
        } else {
            xMult = 1.0f/aspect;
            yMult = 1.0f;
        }

        // When the camera position gets near the edge, accelerate it away from the edge.
        if (cameraX < 0.1f) cameraXSpeed += OVERALL_SPEED * elapsed;
        else if (cameraX > 0.9f) cameraXSpeed -= OVERALL_SPEED * elapsed;

        if (cameraY < 0.1f) cameraYSpeed += OVERALL_SPEED * elapsed;
        else if (cameraY > 0.9f) cameraYSpeed -= OVERALL_SPEED * elapsed;

        // Add on the camera speed.
        cameraX += xMult * cameraXSpeed * elapsed;
        cameraY += yMult * cameraYSpeed * elapsed;

        // Ensure the camera position cannot go out of range.
        cameraX = fmax(fmin(1.0f, cameraX), 0.0f);
        cameraY = fmax(fmin(1.0f, cameraY), 0.0f);
    }

    void MainMenuBackground::updateAlpha(GLfloat elapsed) {
        if (alphaCount >= ALPHA_DELAY) {
            if (alpha < 1.0f) {
                alpha += elapsed * FADE_IN_SPEED;
                if (alpha > 1.0f) {
                    alpha = 1.0f;
                }
            }
        } else {
            alphaCount++;
        }
    }

    void MainMenuBackground::draw(GLfloat viewWidth, GLfloat viewHeight, GLfloat elapsed) {
        // Do nothing if our texture has not finished loading successfully.
        if (!texture.hasFinishedLoadingSuccessfully()) return;

        // Get the texture width and height.
        GLfloat texWidth = texture.getWidth(),
                texHeight = texture.getHeight();

        // Update the position, telling it the aspect ratio of the texture.
        updatePosition(texWidth/texHeight, elapsed);

        // Update our alpha, for fade-in.
        updateAlpha(elapsed);

        // Work out the scale we need to draw the texture.
        GLfloat scale;
        if (viewWidth/viewHeight > texWidth/texHeight) {
            scale = (viewWidth/texWidth)/VIEW_FILL;
        } else {
            scale = (viewHeight/texHeight)/VIEW_FILL;
        }
        // From this scale and the camera position, work out where to draw the texture.
        GLfloat drawWidth = scale*texWidth,
                drawHeight = scale*texHeight,
                drawX = (viewWidth - drawWidth) * cameraX,
                drawY = (viewHeight - drawHeight) * cameraY;

        // Draw the texture.
        glEnable(GL_TEXTURE_2D);
            texture.bind();
            glBegin(GL_QUADS);
                glColor4f(1.0f, 1.0f, 1.0f, alpha);
                wool_textureRectangle(drawX, drawY, drawWidth, drawHeight);
            glEnd();
        glDisable(GL_TEXTURE_2D);
    }

    void MainMenuBackground::onLeaveLoadingScreen(void) {
        alpha = 0.0f;
        alphaCount = 0;
        texture.unloadVRamCopy();
        std::cout << "Unloading main menu background texture from VRAM.\n";
    }
}

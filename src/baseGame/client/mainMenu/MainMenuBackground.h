/*
 * MainMenuBackground.h
 *
 *  Created on: 4 Feb 2019
 *      Author: wilson
 */

#ifndef BASEGAME_CLIENT_MAINMENU_MAINMENUBACKGROUND_H_
#define BASEGAME_CLIENT_MAINMENU_MAINMENUBACKGROUND_H_

#include <wool/texture/ThreadTexture.h>

namespace threading {
    class ThreadQueue;
}

namespace rasc {

    class MainMenuBackground {
    private:
        /**
         * The background texture for the main menu.
         */
        wool::ThreadTexture texture;

        /**
         * The position of the camera, as it moves around, as values between 0 and 1.
         */
        GLfloat cameraX, cameraY;

        /**
         * The speed the camera is moving,
         * measured in proportion of (the square bounding box of) the texture, per second.
         */
        GLfloat cameraXSpeed, cameraYSpeed;

        /**
         * The alpha we will draw our texture.
         */
        GLfloat alpha;

        /**
         * We only start changing the alpha ALPHA_DELAY frames after the texture has loaded. This avoids problems
         * where the elapsed time might be very long while loading the texture, when running in valgrind.
         */
        int alphaCount;

    private:
        // Performs the position updating, each frame.
        void updatePosition(GLfloat aspect, GLfloat elapsed);

        // Updates the alpha we draw the texture each frame, to allow fading in.
        void updateAlpha(GLfloat elapsed);

    public:
        MainMenuBackground(threading::ThreadQueue * threadQueue);
        virtual ~MainMenuBackground(void);

        /**
         * Draws the main menu background, to fill the specified window area.
         */
        void draw(GLfloat viewWidth, GLfloat viewHeight, GLfloat elapsed);

        /**
         * This must be called when we leave the loading screen. This removes the background
         * texture from VRAM, to avoid excessive VRAM usage during the game.
         * The texture will be automatically loaded in again next time draw is called.
         */
        void onLeaveLoadingScreen(void);

        // Returns the job id we are using to load the texture.
        inline uint64_t getTextureLoadJobId(void) {
            return texture.getLoadJobId();
        }
    };

}

#endif
